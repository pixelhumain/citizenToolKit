<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Yii, Person;
/**
* When a user is invited and click on the link on his invitation email
* Verify email key and redirect to sign in in order to register this user
*/
class ValidateInvitationAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($user, $validationKey, $invitation=null,$redirect=null, $costum=null) {
        if(empty($user) || empty($validationKey) )
            return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));

        $controller=$this->getController();
        $params = array();
        
        //Validate validation key
        $error = "somethingWrong";
        $res = Person::isRightValidationKey($user, $validationKey);
        
        if ($res==true) {
            //Get the invited user in the db
            $account = Person::getById($user, false);
            $error="";

            if(!empty($account)) {
                if(!empty($account["pending"])){
                    $params["email"] = $account["email"];
                    $params["name"] = $account["name"];
                    $params["userValidated"] = 1;
                    $params["pendingUserId"] = $user;
                    $invitedBy = @$account["invitedBy"];
                    if (!empty($invitedBy)) 
                       $params["invitor"] = $invitedBy;
                    else
                        //Something went wrong ! Impossible to retrieve your invitor.
                        $error = "unknwonInvitor";
                    $msg = "";
                } else {
                    //Your account already exists on the plateform : please try to login
                    $error = "accountAlreadyExists";
                    $params["error"] = $error;
                    $urlRedirect=Yii::app()->createUrl("/co2")."?".$this->arrayToUrlParams($params)."#panel.box-login";
                    if(@$redirect){
                        if(strrpos($redirect, "survey") !== false || strrpos($redirect, "costum") !== false){
                            $redirect=str_replace(".", "/", $redirect);
                            $urlRedirect=Yii::app()->createUrl($redirect."?".$this->arrayToUrlParams($params)."#panel.box-login");
                        }else if(strrpos($redirect, "custom") !== false)
                            $urlRedirect=Yii::app()->createUrl($redirect."?el=".$_GET["el"]."&".$this->arrayToUrlParams($params)."#panel.box-login");
                    }else if(isset($costum) && $costum){
                         $controller->redirect(Yii::app()->createAbsoluteUrl("?".$this->arrayToUrlParams($params)."#panel.box-login"));
                    }else{
                        $controller->redirect($urlRedirect);
                    }
                }
            }
        }
        
        $params["error"] = $error;
        $urlRedirect=Yii::app()->createUrl("/co2")."?".$this->arrayToUrlParams($params)."#panel.box-register";
        if(@$redirect){
            if(strrpos($redirect, "survey") !== false || strrpos($redirect, "costum") !== false){
                $redirect=str_replace(".", "/", $redirect);
                $urlRedirect=Yii::app()->createUrl($redirect."?".$this->arrayToUrlParams($params)."#panel.box-register");
            }else if(strrpos($redirect, "custom") !== false){
               $urlRedirect=Yii::app()->createUrl($redirect."?el=".$_GET["el"]."&".$this->arrayToUrlParams($params)."#panel.box-register");
            }
            $controller->redirect($urlRedirect);
           }else if(isset($costum) && $costum){
            $controller->redirect(Yii::app()->createAbsoluteUrl("?".$this->arrayToUrlParams($params)."#panel.box-register"));
        }else{
            $controller->redirect($urlRedirect);
        }
    }

    private function arrayToUrlParams($params) {
        $params = implode('&', array_map(function ($v, $k) { return $k . '=' . urlencode($v); }, 
                                            $params, 
                                            array_keys($params)
                                        ));
        return $params;

    }
}