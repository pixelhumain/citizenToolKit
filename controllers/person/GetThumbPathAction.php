<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;
use CAction;
use Person;
use Rest;
use Yii;

class GetThumbPathAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller=$this->getController();
        $me = Person::getById(Yii::app()->session['userId']);
		return Rest::json(array(
			"profilImageUrl" => $me['profilImageUrl'],
			"profilThumbImageUrl" => $me["profilThumbImageUrl"],
			"profilMarkerImageUrl" => $me["profilMarkerImageUrl"],)); 
    	Yii::app()->end();
    }
}
?>