<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Element, Exception, Rest;

/**
* retreive dynamically 
*/
class GetContactsByMailsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	try {
    		$res = (!empty($_POST["mailsList"] ) ? Element::getContactsByMails($_POST["mailsList"]) : array() );
    	} catch (Exception $e) {
    		$res = array("result" => false, "msg"=> $e->getMessage());
    	}
    	
		return Rest::json($res);
		exit;
    }
}