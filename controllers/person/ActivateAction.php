<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Yii, Person;
/**
* upon Registration a email is send to the new user's email 
* he must click it to activate his account
* This is cleared by removing the tobeactivated field in the pixelactifs collection
*/
class ActivateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($user, $validationKey, $invitedBy=null, $redirect=null, $costum=null, $toredirect = null) {
        if(empty($user) || empty($validationKey) )
            return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));

    	$controller=$this->getController();
    	$params = array();

        //remove logued user to prevent incoherent action
        Person::clearUserSessionData();
    	
        //Validate email
    	$res = Person::validateEmailAccount($user, $validationKey);
    	if ($res["result"]) {
    		$params["userValidated"] = 1;
    		$params["email"] = $res["account"]["email"];
    	} else {
    		$params["msg"] = $res["msg"];
    	}
        
    	//InvitedBy
    
    	if (@$res["account"]["tobeactivated"] == true) {
	    	//echo true;
            $params["tobeactivated"] = true;
            $params["pendingUserId"] = $user;
            if (!empty($invitedBy)) {
        		Yii::app()->session["invitor"] = Person::getSimpleUserById($invitedBy);
        	} else {
                Yii::app()->session["invitor"] = "";
            }
        }
		//print_r($params);
        //var_dump($params);
        $params = implode('&', array_map(function ($v, $k) { return $k . '=' . $v; }, 
                                            $params, 
                                            array_keys($params)
                                        ));
        if(!empty($params) && !empty($toredirect)) {
            $params .= '&toredirect=' . $toredirect;
        } else if(!empty($toredirect) && empty($params)) {
            $params = 'toredirect=' . $toredirect;
        }
        $urlRedirect=Yii::app()->createUrl("/".$controller->module->id)."?".$params."#panel.box-login";

        if(@$redirect){
            if(strrpos($redirect, "survey") !== false || strrpos($redirect, "costum") !== false){
                $redirect=str_replace(".", "/", $redirect);
                $urlRedirect=Yii::app()->createUrl($redirect."?".$params."#panel.box-login");
                //var_dump($urlRedirect);exit;
            }else if(strrpos($redirect, "custom") !== false){
                $urlRedirect=Yii::app()->createUrl($redirect."?el=".$_GET["el"]."&".$params."#panel.box-login");            
            }
            $controller->redirect($urlRedirect);
        }else if(isset($costum) && $costum){
            $controller->redirect(Yii::app()->createAbsoluteUrl("?".$params."#panel.box-login"));
        }else{
	       $controller->redirect($urlRedirect);
        }
    }

    
}