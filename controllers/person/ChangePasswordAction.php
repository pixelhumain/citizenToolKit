<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Person, Yii, Rest;
/**
* Change the password of the current user
*/
class ChangePasswordAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {

    	$controller=$this->getController();
        
        if (isset($_GET["mode"])) {
            $mode = @$_GET["mode"];
            $userId = @$_GET["id"];
        } else {
            $userId = @$_POST["userId"];
            $mode = @$_POST["mode"];
        }

        if (! Person::logguedAndValid() || Yii::app()->session["userId"] != $userId) {
            return Rest::json(array("result" => false, "msg" => "You can not modify a password of this user !"));
            return;
        }

        if ($mode == "initSV") {
            return $controller->renderPartial("changePasswordSV", array(), true);
        } else if ($mode == "changePassword") {
            $res = Person::changePassword(@$_POST["oldPassword"], @$_POST["newPassword"], $userId);
            return Rest::json($res);
        } else {
    		return Rest::json(array("result" => false, "msg" => "Unknown mode !"));
    	}
    }
}