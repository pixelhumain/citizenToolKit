<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Person, Rest;
class RemoveHelpBlockAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id=null) {
		$controller=$this->getController();
		$res = Person::updateNotSeeHelpCo($id); 
		return Rest::json($res);
    }
}