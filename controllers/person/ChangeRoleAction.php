<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Person, Yii, Rest, Role;
/**
* Change the role of a user
*/
class ChangeRoleAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();

    	$userId = @$_POST["id"];
    	$action = @$_POST["action"];

    	if (! (Person::logguedAndValid() && Yii::app()->session["userIsAdmin"])) {
    		return Rest::json(array("result" => false, "msg" => "You are not super admin : you can not modify this role !"));
    		return;
    	}

		$res = Role::updatePersonRole($action, $userId);
    	return Rest::json($res);
    }
}