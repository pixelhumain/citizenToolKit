<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;
use CAction;
use CTKException;
use Mail;
use Person;
use Rest;
use Yii;

/**
* Update an information field for a person
*/
class UpdateFieldAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $res = array("result"=>false, "error"=>"Something went wrong");
        if (!empty($_POST["pk"])) 
        {
            if (! empty($_POST["name"])) 
            {
                try{
                    $res = Person::updatePersonField($_POST["pk"], $_POST["name"], @$_POST["value"], Yii::app()->session["userId"] );
                    if( @$_POST["value"] == "bgCustom" && isset( $_POST["url"] ))
                        Person::updatePersonField($_POST["pk"], "bgUrl", $_POST["url"], Yii::app()->session["userId"] );
                    if(@$_POST["name"] == "professional"){
                        $person=Person::getById(Yii::app()->session["userId"]);
                        Mail::notifAdminNewPro($person);
                    }
                } catch (CTKException $e) {
                    $res = array("result"=>false, "msg"=>$e->getMessage(), $_POST["name"]=>$_POST["value"]);
                }
            }
        } 
        return Rest::json($res);
    }
}