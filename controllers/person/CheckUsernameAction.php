<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, DataValidator, Yii, Rest, Person;
/**
* Check if the username is unique in the db.
*/
class CheckUsernameAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
    	if(DataValidator::missingParamsController($_POST, ["username"]) )
    		return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));

    	$username = $_POST["username"];
     	
        //TODO SBAR - return a list of username available
        return Rest::json(Person::isUniqueUsername($username));
    }
}