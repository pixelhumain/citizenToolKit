<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;
use CAction;
use Yii;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $controller->redirect(Yii::app()->createUrl("/".$controller->module->id."/person/dashboard"));
    }
}