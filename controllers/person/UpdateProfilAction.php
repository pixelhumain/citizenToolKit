<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Yii;
class UpdateProfilAction extends \PixelHumain\PixelHumain\components\Action
{
	/**
	* Dashboard Organization
	*/
    public function run() { 
    	$controller=$this->getController();
        $params = array();
		$page = "updateProfil";
		if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($page,$params,true);
        else 
		return	$controller->render( $page , $params );
    }

    
}
