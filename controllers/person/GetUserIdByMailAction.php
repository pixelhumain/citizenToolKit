<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Person, Rest;
/**
* Check if the username is unique in the db.
*/
class GetUserIdByMailAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
      $params = array();
        $res = Person::getPersonIdByEmail($_POST['mail']);
        
      if($res == false){
        $params["userId"] = "";
      }else
        {
        $params["userId"] = $res;
      }
      
        return Rest::json($params);
    }
}