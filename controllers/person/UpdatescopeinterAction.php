<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Yii, Person, Rest;
/**
* Update an information field for a person
*/
class UpdatescopeinterAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {   
        $controller=$this->getController();
        if(isset(Yii::app()->session["userId"]) && !empty(Yii::app()->session["userId"])){
            $res = Person::updateScopeInter(Yii::app()->session['userId']);
            return Rest::json($res);
        }else{
            return Rest::json(array("result" => false , "msg"=>"You are not connected"));
        }
    }
}

?>