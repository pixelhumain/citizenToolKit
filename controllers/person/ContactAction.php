<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction;
class ContactAction extends \PixelHumain\PixelHumain\components\Action
{
	

    public function run()
    {
    	$controller=$this->getController();
    	$controller->title = "Importer vos contacts";
        $controller->subTitle = "";
        $controller->pageTitle = "Importer vos contacts";
    return	$controller->render("contact");
    }
}