<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Yii, Person, Rest;
class LoggedAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$res = array("userId"=>Yii::app()->session['userId']);
    	if( isset(Yii::app()->session['userId'])){
    		$me = Person::getById(Yii::app()->session['userId']);
            if(!empty($me["address"]["codeInsee"]))
                $address=$me["address"];
    		Person::updateCookieCommunexion(Yii::app()->session['userId'], @$address);
    		$res["profilThumbImageUrl"] = $me['profilThumbImageUrl'];
    	}
        return Rest::json($res);
    }
}