<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Person, Yii, Rest;
/**
* Check if the username is unique in the db.
*/
class CheckLinkMailWithUserAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
      $params = array();
      $res = Person::getPersonFollowsByUser(Yii::app()->session["userId"]);

      if($res != false){
        $params["result"] = false;
      }else
        $params["result"] = true;

        $params["follows"] = $res ;
      
      return Rest::json($params);
    }
}