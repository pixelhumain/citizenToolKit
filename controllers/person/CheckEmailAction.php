<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, DataValidator, Yii, Rest, Person;
/**
* Check if the email is unique in the db.
*/
class CheckEmailAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
    	if(DataValidator::missingParamsController($_POST, ["email"]))
    		return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));
    	$email = $_POST["email"];
        return Rest::json(array("res"=>Person::isUniqueEmail($email)));
    }
}