<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;
use CAction;
use Person;
use Preference;
use Rest;
use Yii;

/**
   * Register a new user for the application
   * Data expected in the post : name, email, postalCode and pwd
   * @return Array as json with result => boolean and msg => String
   */
class UpdateSettingsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        if(isset($_POST["type"])){
          $res=Preference::updateConfidentiality(Yii::app()->session["userId"],Person::COLLECTION,$_POST);
        } else{
        	$res=Preference::updatePreferences(Yii::app()->session["userId"],Person::COLLECTION);
        }
		return Rest::json($res);
		exit;
    }
}