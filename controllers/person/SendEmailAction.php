<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Person, Rest, Yii, PHDB, PHType, MongoRegex, Mail;
use PixelHumain\PixelHumain\models\Application;

/**
 * Send an mail to a user with the email.
 * Depending of the type. Could be
 * - password = to send a new password to the user
 * - validation = to send the validation email to the user
 * @return [json] 
 */
class SendEmailAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $email = trim($_POST["email"]);
        $type = $_POST["type"];
        
        //Check user existance
        $user = Person::getPersonByEmail($email);
        if (!$user) {
            return Rest::json(array("result"=>false, "errId" => "UNKNOWN_ACCOUNT_ID", 
                         "msg"=>Yii::t("common", "This email doesn't exist in our database. Do you want to create an account ?")));
            die();
        }

        //reset users email to lowercase 

        if( $user["email"] != $email ){
            //differnet probably because case sensitive differences
            $email = strtolower($email);
            PHDB::update( PHType::TYPE_CITOYEN,
                            [ "email" => new MongoRegex( '/^'.preg_quote($email).'$/i' ) ], 
                            [ '$set' => ["email" => $email]]);
        }

        // Forgot my password Mail
        if ($type == "password") {
            //reset password 
            $pwd = Person::random_password(8);
            //TODO SBAR : Call the model
            PHDB::update( PHType::TYPE_CITOYEN,
                            [ "email" => new MongoRegex( '/^'.preg_quote($email).'$/i' ) ], 
                            ['$set' => [ "pwd"=>hash('sha256', $email.$pwd)] ]);

            //TODO SBAR : Application - how does it work exactly ?
            //Same user for different application ? Application = Granddir ? Larges ? Communecter ?
            if (empty($_POST["app"])) {
                $app = new Application("");
            } else {
                $app = new Application($_POST["app"]);
            }
            
            //send validation mail
            Mail::passwordRetreive($email, $pwd);
            $res = array("result"=>true, "msg"=>Yii::t("login", "An email with a new password has been sent to your email account. Thanks."));
        
        // Validation Mail
        } else if ($type == "validateEmail") {
            if(isset($user['mailToResend']['pwd']) && $user['mailToResend']['pwd'] != '' && isset($user['mailToResend']['redirectUrl']) && $user['mailToResend']['redirectUrl'] != '' && isset($user['mailToResend']['formTitle'])) {
                Mail::validatePersonWithNewPwd($user, $user['mailToResend']['pwd'], $user['mailToResend']['formTitle'], $user['mailToResend']['redirectUrl']);
            } else {
                Mail::validatePerson($user);
            }
            $res = array("result"=>true,"msg"=>"Un mail de validation vous a été envoyé à votre adresse email.");
        } else {
            $res = array("result"=>true,"msg"=>"Unknow email type : please contact your admin !");
        }

        return Rest::json($res);
    }

}