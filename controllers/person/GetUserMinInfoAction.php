<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use PHDB;
use Rest;

class GetUserMinInfoAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type, $id) {
        $fields=['name', 'username', 'email', 'collection', 'slug'];
        $user = PHDB::findOneById($type, $id, $fields);
        return Rest::json($user);
    }
}