<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Person, Rest, Yii;
class AuthenticateServiceTokenAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
		$controller=$this->getController();
        $tokenName = $_POST["tokenName"];
		$service = $_POST["service"];
		$res = Person::loginAuthServiceToken( $service, $tokenName); 
		return Rest::json($res);
    }
	
}