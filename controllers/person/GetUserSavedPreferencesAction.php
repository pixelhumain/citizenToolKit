<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use Preference, Rest;

class GetUserSavedPreferencesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type, $id) {
        $appKey=(isset($_POST["appKey"])) ? $_POST["appKey"] : "search";
        $preferences = Preference::getAppConfig($appKey, $type, $id);
        return Rest::json($preferences);
    }
}