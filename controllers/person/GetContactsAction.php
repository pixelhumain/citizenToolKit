<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use Person, Rest;

class GetContactsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller = $this->getController();
		$contacts = Person::getPersonLinksByPersonId($_POST["id"]);

		return Rest::json($contacts);
    }
}