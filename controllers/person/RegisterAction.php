<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;
use CAction;
use CTKException;
use Mail;
use Person;
use Rest;
use Yii;
use PHDB;

/**
   * Register a new user for the application
   * Data expected in the post : name, email, postalCode and pwd
   * @return Array as json with result => boolean and msg => String
   */
class RegisterAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();

        $name = (!empty($_POST['name'])) ? $_POST['name'] : "";
        $username = (!empty($_POST['username'])) ? $_POST['username'] : "";
		$email = (!empty($_POST['email'])) ? $_POST['email'] : "";
		$pwd = (!empty($_POST['pwd'])) ? $_POST['pwd'] : "";
		$address = (!empty($_POST['address'])) ? $_POST['address'] : null;
		$pendingUserId = (!empty($_POST['pendingUserId'])) ? $_POST['pendingUserId'] : "";

		$newPerson = array(
			'name'=> $name,
			'username'=> $username,
			'pwd'=>$pwd,
			'address'=> $address
		);

		$inviteCode = (@$_POST['inviteCode']) ? $_POST['inviteCode'] : null;

		if (isset($_POST['mode'])) 
			$mode = $_POST["mode"];
		else 
			$mode = Person::REGISTER_MODE_NORMAL;
	//	if(isset($_POST['loginAfter']))
	//		$newPerson["firstConnect"]=true;
		// Deprecated but keep it for Rest calls.
		// => @Bouboule : I comment to use import process 
		/*if ($mode == Person::REGISTER_MODE_NORMAL) {
			$newPerson['city'] = @$_POST['city'];
			$newPerson['postalCode'] = @$_POST['cp'];
			$newPerson['geoPosLatitude'] = @$_POST['geoPosLatitude'];
			$newPerson['geoPosLongitude'] = @$_POST['geoPosLongitude'];
		}*/

		$pendingUserId = Person::getPendingUserByEmail($email);
		
		//The user already exist in the db (invitation process) : the data should be updated
		if ($pendingUserId != "") {
			$res = Person::updateMinimalData($pendingUserId, $newPerson);
			if (! $res["result"]) {
				return Rest::json($res);
				exit;
			} 
		//New user
		} else {
			try {
				$newPerson['email'] = $email;
				$res = Person::insert($newPerson, $mode,$inviteCode);
				$newPerson["_id"]=$res["id"];
			} catch (CTKException $e) {
				$res = array("result" => false, "msg"=>$e->getMessage());
				return Rest::json($res);
				exit;
			}
		}

		//Try to login with the user
		$res2 = Person::login($email,$pwd,true, @$_POST['loginAfter']);
		if(isset($_POST["isInvitation"]))
			$res["isInvitation"]=true;

		if ($res["result"]) {
			
		//} else if ($res["msg"] == "notValidatedEmail") {
			//send validation mail to the user
			//var_dump("here");
			if(empty($pendingUserId)){
				$newPerson['email'] = $email;
				$newPerson["inviteCode"] = $inviteCode;
				Mail::validatePerson($newPerson);
				$res = array("result"=>true, "msg"=> Yii::t("login","Congratulation your account is created !")."<br>".Yii::t("login","Last step to enter : we sent you an email, click on the link to validate your mail address."), "id"=>(string)$newPerson["_id"], "person"=>$newPerson );
			}else{
				$res = array("result"=>true, "msg"=> Yii::t("login","Congratulation your account is created !"), "isInvitation"=>true );
			}
		} else if ($res["msg"] == "betaTestNotOpen") {
			$newPerson["_id"] = $pendingUserId;
			$newPerson['email'] = $email;
			//send betatest information mail
			Mail::betaTestInformation($newPerson);
			$res = array("result"=>true, 
					"msg"=> Yii::t("login","You are now communnected !")."<br>".Yii::t("login","Our developpers are fighting to open soon ! Check your mail that will happen soon !")."<br>".Yii::t("login","If you really want to start testing the platform before, send us an email and we'll consider your demand :)"), 
					"id"=>$pendingUserId); 
		} 
		return Rest::json($res);
		exit;
    }
}
