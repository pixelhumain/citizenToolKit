<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, CacheHelper, Person, Yii, Rest;
class LogoutAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();
        $costum = CacheHelper::getCostum();
        Person::clearUserSessionData();
        $url = "/#";
        if(@$_GET["network"])
            $url="/network/default/index?src=".$_GET["network"];
            //var_dump($costum);exit;
        if(isset($costum) && !empty($costum) && isset($costum["url"]))
        	$url=$costum["url"];
        $url=Yii::app()->createUrl($url);
        
        if(isset($costum) && isset($costum["host"]))
            $url=$costum["host"];
            //$controller->redirect(Yii::app()->createAbsoluteUrl(""));
//        else 
  //          return $url;
        return Rest::json(array("url"=>$url));
            //$controller->redirect($url);
    }
}