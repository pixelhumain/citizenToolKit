<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Person, Rest;
/**
* Update an information field for a person
*/
class UpdateWithJsonAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $res = Person::updateWithJson($_POST["file"][0]);
        //$res = array();
        return Rest::json($res);
    }
}

?>