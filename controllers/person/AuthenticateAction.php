<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Yii, Person, Rest;
class AuthenticateAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
    	//Rest::json($_POST); exit;
		$controller=$this->getController();
		$email = $_POST["email"];
		if (!Yii::app()->session->isStarted){
			if(  @$_POST["remember"] ){
			ini_set( 'session.cookie_lifetime', 60 * 60 * 24 * 7);
			ini_set( 'session.gc_maxlifetime', 60 * 60 * 24 * 7);
			} else {
			ini_set( 'session.cookie_lifetime', 60 * 30);
			ini_set( 'session.gc_maxlifetime', 60 * 30);
			}
		}

		$res = Person::login( $email , $_POST["pwd"], false);
		if($res["result"] && !empty($_POST["timezone"]))
			Yii::app()->session["timezone"] = $_POST["timezone"];
		if($res["result"] && isset(Yii::app()->request->cookies['invitationLinkRef']))
			$res["goto"] = "/link/connect/ref/".Yii::app()->request->cookies['invitationLinkRef']->value;

		return Rest::json($res);
    }
    
	
	public function getControllerAndActionFromUrl($url) {
		$res = array("controllerId" => "", "actionId" => "");
		if ($url) {
			$controller = $this->getController();
			$res = array();
			$url2 = str_replace(Yii::app()->baseUrl ."/".$controller->moduleId."/", "", $url);

			if (substr_count($url2, '/') == 2) {
				list($controller,$action) = explode("/", $url2);
				$res["controllerId"] = $controller;
				$res["actionId"] = $action;
			}
		}

		return $res;
	}
}