<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use Person, Rest;

class GetUserAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller = $this->getController();
		$contacts = Person::getById($_POST["id"]);

		return Rest::json($contacts);
    }
}