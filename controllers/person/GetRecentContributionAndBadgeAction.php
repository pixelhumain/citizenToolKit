<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use Actions, Rest , PHDB , Badge , DateTime;
use Element;
use Person;
use Project;
use Event;

class GetRecentContributionAndBadgeAction extends \PixelHumain\PixelHumain\components\Action
{
    /**
     * @inheritdoc
     */
    /**
     * Renvoie les contributions récentes et les badges d'un utilisateur.
     * @return array avec deux clés : 'projects' => les projets récents, 'badges' => les badges
     */
    public function run()
    {
        $allChild = getAllChild($_POST['elementId']);
        $recentContribution = Actions::getRecentContributionById($_POST['userId'],$allChild);
        if(isset($_POST["badges"])){
            $badges = $_POST['badges'];
        }else{
            $data = Element::getByTypeAndId($_POST["type"], $_POST['userId']);
            $badges = $data["badges"] ?? [];
        }
        $badges = getBadgeDetails($badges);
        $actionsCreated = getAllActionsCreatedWithStatus($_POST['userId'],$allChild);
        $actionsParticipated = getAllActionsParticipatedWithStatus($_POST['userId'],$allChild);
        $actionsLast12Month = getActionsForLast12MonthsForChart($_POST['userId'],$allChild);
        $countAll = countAllActionsWithStatus($_POST['userId'],$allChild);

        return Rest::json([
            'projects' => $recentContribution,
            'badges' => $badges,
            'actionsCreated' => $actionsCreated,
            'actionsParticipated' => $actionsParticipated,
            'actionsLast12Month' => $actionsLast12Month,
            "countAll" => $countAll
        ]);
    }
}


/**
 * Verifier si l'array est associative ou pas
 *
 * @param array $array
 * @return boolean
 */
function is_associative_array($array) {
    if (array_keys($array) !== range(0, count($array) - 1)) {
        return true; 
    }
    return false; 
}

/**
 * Récupère les détails des badges associés à un utilisateur.
 *
 * @param array $params Les paramètres de l'action, incluant les badges associés à l'utilisateur.
 *
 * @return array Un tableau contenant les détails des badges, incluant l'ID, le nom, l'image.
 */
function getBadgeDetails($badges) {
    $badgeDetails = [];

    if (!empty($badges) && is_associative_array($badges)) {
        uasort($badges, "sortByOrder");

        foreach ($badges as $key => $value) {
            $assertionImage = '';

            if (!isset($value['attenteEmetteur'])) {
                

                if (isset($value["byReference"]) && $value["byReference"] === true) {
                    $assertionImage = PHDB::findOneById(Badge::COLLECTION, $key, ["assertion"])['assertion']["image"] ?? "";
                } else {
                    $assertionImage = PHDB::findOneById(Badge::COLLECTION, $key, ["profilMediumImageUrl"])['profilMediumImageUrl'] ?? "";
                }

                if (is_array($assertionImage)) {
                    $assertionImage = '';
                }

                $badgeDetails[] = [
                    'id' => $key,
                    'name' => $value["name"],
                    'image' => $assertionImage,
                ];
            }
        }
    }

    return $badgeDetails;
}


/**
 * Récupère le nombre d'actions par statut (done ou todo) pour un utilisateur donné.
 *
 * @param string $userId L'ID de l'utilisateur.
 *
 * @return array Un tableau contenant les résultats, incluant :
 *  - byStatus : Un tableau associatif avec les statuts comme clés et le nombre d'actions comme valeurs.
 *  - total : Le nombre total d'actions.
 */
function getAllActionsCreatedWithStatus($userId,$possibleParent) {
    $activities = PHDB::aggregate(
        Actions::COLLECTION,
        [
            [
                '$match' => [
                    'idUserAuthor' => $userId, // Filtrer par l'ID de l'utilisateur
                    "parentId" => ['$in' => $possibleParent], // Filtrer par Id parent rattacher a l'orga
                    'status' => ['$in' => ['done', 'todo']], // Filtrer sur les statuts "done" et "todo"
                ],
            ],
            [
                '$group' => [
                    '_id' => '$status', // Grouper par statut (done ou todo)
                    'count' => ['$sum' => 1], // Compter les actions pour chaque statut
                ],
            ],
            [
                '$project' => [
                    '_id' => 1, 
                    'count' => 1, // Garder les résultats par statut
                ],
            ],
        ]
    );
    
    if (empty($activities['result'])) {
        return [];
    }
    
    $formattedResult = [];
    foreach ($activities['result'] as $activity) {
        $formattedResult[$activity['_id']] = $activity['count'];
    }
    
    return $formattedResult;
}


/**
 * Retourne le nombre d'actions ou l'utilisateur a particip , 
 * group  par statut.
 * 
 * @param string $userId L'ID de l'utilisateur
 * 
 * @return array Un tableau associatif 
 *               - done : Le nombre d'actions realiser par l'utilisateur
 *               - todo : Le nombre d'actions  faire par l'utilisateur
 */
function getAllActionsParticipatedWithStatus($userId,$possibleParent) {
    $activities = PHDB::aggregate(
        Actions::COLLECTION,
        [
            [
                '$match' => [
                    "parentId" => ['$in' => $possibleParent], // Filtrer par Id parent rattacher a l'orga
                    'links.contributors.' . $userId => ['$exists' => true], // Filtrer par l'ID de l'utilisateur
                    'idUserAuthor' => ['$ne' => $userId],
                    'status' => ['$in' => ['done', 'todo']], // Filtrer sur les statuts "done" et "todo"
                ],
            ],
            [
                '$group' => [
                    '_id' => '$status', // Grouper par statut (done ou todo)
                    'count' => ['$sum' => 1], // Compter les actions pour chaque statut
                ],
            ],
            [
                '$project' => [
                    '_id' => 1, 
                    'count' => 1, // Garder les résultats par statut
                ],
            ],
        ]
    );
    
    if (empty($activities['result'])) {
        return [ "done" => 0 , "todo" => 0];
    }
    
    $formattedResult = [];
    foreach ($activities['result'] as $activity) {
        $formattedResult[$activity['_id']] = $activity['count'];
    }
    
    return $formattedResult;
}


/**
 * Renvoie le nombre d'actions terminées (status "done") de l'utilisateur pour les 12 derniers mois.
 * Les résultats sont groupés par mois.
 * @param String $userId L'ID de l'utilisateur
 * @return array Les résultats groupés par mois, avec les clés suivantes :
 *  - _id : Un tableau associatif avec les clés "year" et "month"
 *  - count : Le nombre d'actions terminées pour ce mois
 */
function getActionsForLast12Months($userId,$possibleParent) {
    // Obtenir la date actuelle et celle d'il y a 12 mois
    $currentDate = new DateTime();
    $oneYearAgo = (clone $currentDate)->modify('-12 months');
    
    // Convertir les dates en timestamp Unix (en secondes)
    $startOfPeriod = $oneYearAgo->getTimestamp();
    $endOfPeriod = $currentDate->getTimestamp();

    // Exécuter l'aggregation MongoDB
    $activities = PHDB::aggregate(
        Actions::COLLECTION,
        [
            [
                '$match' => [
                    'parentId' => ['$in' => $possibleParent], // Filtrer par Id parent rattacher a l'orga
                    '$or' => [
                        ['idUserAuthor' => $userId], // L'utilisateur est l'auteur
                        ['links.contributors.' . $userId => ['$exists' => true]] // L'utilisateur est un contributeur
                    ],
                    'status' => 'done', // Statut "done"
                    'updated' => [
                        '$gte' => $startOfPeriod, // Timestamp il y a 12 mois
                        '$lt' => $endOfPeriod     // Timestamp actuel
                    ]
                ]
            ],
            [
                '$group' => [
                    '_id' => [
                        'year' => ['$year' => ['$toDate' => ['$multiply' => ['$updated', 1000]]]], // Extraire l'année
                        'month' => ['$month' => ['$toDate' => ['$multiply' => ['$updated', 1000]]]] // Extraire le mois
                    ],
                    'count' => ['$sum' => 1] // Compter les actions pour chaque mois
                ]
            ],
            [
                '$sort' => [
                    '_id.year' => 1, // Trier par année
                    '_id.month' => 1 // Trier par mois
                ]
            ]
        ]
    );

    // Vérifier si le résultat est vide
    if (empty($activities['result'])) {
        return [];
    }

    // Retourner les résultats groupés par mois
    return $activities['result'];
}

function countAllActionsWithStatus($userId,$possibleParent) {
    $activities = PHDB::aggregate(
        Actions::COLLECTION,
        [
            [
                '$match' => [
                    'parentId' => ['$in' => $possibleParent], // Filtrer par Id parent rattacher a l'orga
                    '$or' => [
                        ['idUserAuthor' => $userId], // Actions created by the user
                        ['links.contributors.' . $userId => ['$exists' => true]] // Actions where the user is a contributor
                    ],
                    'status' => ['$in' => ['done', 'todo']], // Filtrer sur les statuts "done" et "todo"
                ],
            ],
            [
                '$group' => [
                    '_id' => '$status', // Grouper par statut (done ou todo)
                    'count' => ['$sum' => 1], // Compter les actions pour chaque statut
                ],
            ],
            [
                '$project' => [
                    '_id' => 1, 
                    'count' => 1, // Garder les résultats par statut
                ],
            ],
        ]
    );
    
    if (empty($activities['result'])) {
        return [];
    }
    
    $formattedResult = [];
    foreach ($activities['result'] as $activity) {
        $formattedResult[$activity['_id']] = $activity['count'];
    }
    
    return $formattedResult;
}



function getActionsForLast12MonthsForChart($userId,$possibleParent) {
    // Obtenir les actions groupées par mois
    $activities = getActionsForLast12Months($userId,$possibleParent);

    // Tableaux pour les labels et les data
    $labels = [];
    $data = [];

    // Parcourir les résultats et formater les labels et les données
    foreach ($activities as $monthData) {
        $year = $monthData['_id']['year'];
        $month = str_pad($monthData['_id']['month'], 2, '0', STR_PAD_LEFT); // Formater le mois sur 2 chiffres
        $count = $monthData['count'];

        // Ajouter les labels au format "YYYY-MM"
        $labels[] = "$year-$month";
        $data[] = $count;
    }

    // Retourner les labels et les données sous forme de tableau formaté
    return [
        'labels' => $labels,
        'data' => $data
    ];
}

function getAllChild($contextId){

    $projectsAndEventChild = [];

    // Étape 1 : Récupérer les IDs des projets enfants
    $projectsChild = PHDB::aggregate(
        Project::COLLECTION,
        [
            [
                '$match' => [
                    "parent.{$contextId}" => ['$exists' => true]
                ]
            ],
            [
                '$project' => [
                    '_id' => 1
                ]
            ],
            [
                '$group' => [
                    '_id' => null,
                    'ids' => ['$push' => '$_id']
                ]
            ],
            [
                '$project' => [
                    'ids' => [
                        '$map' => [
                            'input' => '$ids',
                            'as' => 'id',
                            'in' => ['$toString' => '$$id']
                        ]
                    ]
                ]
            ]
        ]
    );

    $idsProject = $projectsChild["result"][0]["ids"] ?? [];

    // Étape 2 : Récupérer les IDs des événements enfants
    $eventsChild = PHDB::aggregate(
        Event::COLLECTION,
        [
            [
                '$match' => [
                    "organizerId" => ['$in' => $idsProject]
                ]
            ],
            [
                '$project' => [
                    '_id' => 1
                ]
            ],
            [
                '$group' => [
                    '_id' => null,
                    'ids' => ['$push' => '$_id']
                ]
            ],
            [
                '$project' => [
                    'ids' => [
                        '$map' => [
                            'input' => '$ids',
                            'as' => 'id',
                            'in' => ['$toString' => '$$id']
                        ]
                    ]
                ]
            ]
        ]
    );

    $idsEvents = $eventsChild["result"][0]["ids"] ?? [];

    // Combiner les IDs des projets et événements enfants
    $projectsAndEventChild = array_merge($idsProject, $idsEvents);
    array_unshift($projectsAndEventChild, $contextId);
    
    return $projectsAndEventChild;
}
