<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person;

use CAction, Yii;
class ChooseInviteContactAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();

        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("chooseinviteContact",$params,true);
        else 
           return $controller->render("chooseinviteContact",$params);
    }
}

?>