<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\gamification;
use CAction;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=null, $id=null)
    {
        $controller=$this->getController();

        //TODO type and id 
        //are used to calculate the gamification points of any entity

        $params = array();
       return $controller->render( "index" , $params );
    }

 
}