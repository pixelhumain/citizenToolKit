<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\error;

use CAction, Yii;
class ApiAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $controller->pageTitle = "ERREUR";
        
        
       // $controller->layout = "//layouts/mainSimple";
	    if($error = Yii::app()->errorHandler->error )
	    {
	    	$controller->title = "ERREUR ".$error['code'];
        	$controller->pageTitle = $controller->title;
        	$controller->subTitle = $error['message'];

	      	if(Yii::app()->request->isAjaxRequest)
	        	return $error['message'];
	      	else
	        return	$controller->render('error', array("error"=>$error));
	    }else 
	     return $controller->render( "index");
    }
}