<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment;
use ActStr;
use CAction;
use Comment;
use CTKException;
use Form;
use Notification;
use OrderItem;
use Rest;
use Yii;

class SaveAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();

        if (isset(Yii::app()->session["userId"])) {
            try {
                $res = Comment::insert($_POST, Yii::app()->session["userId"]);

                if(@$_POST["orderId"]){
                    OrderItem::actionRating($_POST,$res["id"]);
                    $res["order"]=OrderItem::getById($_POST["orderId"]);
                }

                if($res["result"] && !empty($res["id"])){
                    $this->notify($_POST,$res["id"]->__toString());
                }
            } catch (CTKException $e) {
                $res = array("result"=>false, "msg"=>$e->getMessage());
            }

            return Rest::json($res);
        } else {
            $res = array("result"=>false, "msg"=>"You must be loggued to create a comment");
            return Rest::json($res);
        }
    }

    /**
     * @param $newComment
     * @return void
     */
    private function notify($newComment,$commentId) : void
    {
        $comment = $newComment;
        $contextId = $comment[ "contextId" ];
        $contextType = $comment[ "contextType" ];
        $objectNotif = array("id" => $commentId, "type" => Comment::COLLECTION);
        $typeAction = Comment::COLLECTION;
        $options = Comment::getCommentOptions($contextId, $contextType);
        if ($comment["contextType"] == Form::COLLECTION && isset($newComment["path"]) && (strpos($newComment["path"], ".comment") !== false || strpos($newComment["path"], ".privateComment") !== false)) {
            // do notihing because the notification for this case is already created in comment models
        } else 
        if($comment["contextType"] != Form::ANSWER_COLLECTION){
            Notification::constructNotification(
                ActStr::VERB_COMMENT,
                array(
                    "id" => Yii::app()->session[ "userId" ],
                    "name" => Yii::app()->session[ "user" ][ "name" ]
                ),
                array(
                    "type" => $comment[ "contextType" ],
                    "id" => $comment[ "contextId" ],
                    "name" => @$options[ "name" ]
                ),
                $objectNotif,
                $typeAction,
                null,
                $comment[ "text" ]
            );
        }
    }
}
