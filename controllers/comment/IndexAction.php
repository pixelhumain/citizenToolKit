<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment;
use Action;
use ActionRoom;
use Authorisation;
use CAction;
use City;
use Classified;
use Comment;
use Crowdfunding;
use CTKException;
use Document;
use Event;
use Form;
use Media;
use News;
use Organization;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use Poi;
use Product;
use Project;
use Proposal;
use Resolution;
use Ressource;
use Room;
use Service;
use Yii;
use Rest;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type, $id, $path = null, $json = null)
    {
        $controller=$this->getController();

        $params = array();
        $res = Comment::buildCommentsTree($id, $type, Yii::app()->session["userId"], @$_POST["filters"], $path);

        $params['comments'] = $res["comments"];
        $params['communitySelectedComments'] = $res["communitySelectedComments"];
        $params['abusedComments'] = $res["abusedComments"];
        
        $params['options'] = $res["options"];
        $params["contextType"] = $type;
        $params["nbComment"] = $res["nbComment"];
        $params['canComment'] = $res["canComment"] ;

        if(@$path)
            $params['path'] = $path ;

        if($type == Event::COLLECTION) {
            $params["context"] = Event::getById($id);
        } else if($type == Project::COLLECTION) {
            $params["context"] = Project::getById($id);
        } else if($type == Organization::COLLECTION) {
            $params["context"] = Organization::getById($id);
        } else if($type == Person::COLLECTION) {
            $params["context"] = Person::getById($id);
        } else if($type == News::COLLECTION) {
            $params["context"] = News::getById($id);
		} else if($type == Poi::COLLECTION) {
            $params["context"] = Poi::getById($id);
        } else if($type == Crowdfunding::COLLECTION) {
            $params["context"] = Crowdfunding::getCampaignAndCounters($id);    
        } else if($type == Classified::COLLECTION) {
            $params["context"] = Classified::getById($id);
        } else if($type == Product::COLLECTION) {
            $params["context"] = Product::getById($id);
        } else if($type == Service::COLLECTION) {
            $params["context"] = Service::getById($id);
        } else if($type == Proposal::COLLECTION) {
            $params["context"] = Proposal::getById($id);
            $params['canComment'] = true;
        } else if($type == Ressource::COLLECTION) {
            $params["context"] = Ressource::getById($id);
        }else if($type == Form::COLLECTION) {
            $params["context"] = Form::getByIdMongo($id);
        }else if($type == Form::ANSWER_COLLECTION) {
            $params["context"] = Form::getAnswerById($id);
        } else if($type == Resolution::COLLECTION) {
            $params["context"] = Resolution::getById($id);


            /*AUTH*/
            //var_dump($params["context"]); exit;
            if(isset($params["context"]["idParentRoom"]))
            $actionRoom = Room::getById($params["context"]["idParentRoom"]);
            $canParticipate = Authorisation::canParticipate(Yii::app()->session["userId"], 
                                $params["context"]["parentType"], $params["context"]["parentId"]);

            $canComment = $params["canComment"] && $canParticipate;
            $params['canComment'] = $canComment;

            $params["parentType"] = $params["context"]["parentType"];
            
        } else if($type == Room::COLLECTION) {
            $actionRoom = Room::getById($id);
            $params["context"] = $actionRoom;
            //Images
			$limit = array(Document::IMG_PROFIL => 1);
			$images = Document::getImagesByKey($id, ActionRoom::COLLECTION, $limit);
			$params["images"] = $images;

            if($actionRoom["parentType"] == Person::CONTROLLER) 
                $params["parent"] = Person::getById($actionRoom["parentId"]);   
            if($actionRoom["parentType"] == Organization::COLLECTION) 
                $params["parent"] = Organization::getById($actionRoom["parentId"]);   
            if($actionRoom["parentType"] == Project::COLLECTION) 
                $params["parent"] = Project::getById($actionRoom["parentId"]);   
            if($actionRoom["parentType"] == City::COLLECTION) {
                $parent = City::getByUnikey($actionRoom["parentId"]);   
                $params["parent"] = array(  "name" => $parent["name"],
                                        "insee" => $parent["insee"],
                                        "cp" => $parent["cp"],
                                        "link" => "urlCtrl.loadByHash('#city.detail.insee.".$parent["insee"].".postalCode.".$parent["cp"]."')");
            }

            if(!isset($params["parent"])) {
                throw new CTKException("Impossible to find this actionRoom");
                //return;
            }
            
            $params["parentType"] = $actionRoom["parentType"];
            $params["parentId"] = $actionRoom["parentId"];
            /*AUTH*/
            $canParticipate = Authorisation::canParticipate(Yii::app()->session["userId"], $actionRoom["parentType"], $actionRoom["parentId"]);
            $canComment = $params["canComment"] && $canParticipate;
            $params['canComment'] = $canComment;

        }else if($type == Action::COLLECTION) {
            $params["context"] = Action::getById($id,["idParentRoom","parentType","parentId"]);
            $params["context"]["actionPath"] = !empty($path) ? $path : "";
            /*AUTH*/
            $limit = array(Document::IMG_PROFIL => 1);
			$images = Document::getImagesByKey($id, Room::COLLECTION_ACTIONS, $limit);
			$params["images"] = $images;
            $canParticipate = Authorisation::canParticipate(Yii::app()->session["userId"], $params["context"]["parentType"], $params["context"]["parentId"]);
            $canComment = $params["canComment"] && $canParticipate;
            $params['canComment'] = $canComment;
            if(!empty($params["context"]["idParentRoom"])){
                $actionRoom = Room::getById($params["context"]["idParentRoom"]);
                $params["parentType"] = $actionRoom["parentType"];
                $params["parentId"] = $actionRoom["parentId"];
            }else{
                $params["parentType"] = $params["context"]["parentType"];
                $params["parentId"] = $params["context"]["parentId"];
            }
            
        } else if($type == Media::COLLECTION) {
            $params["context"] = Media::getById($id);
        } else if($type == Form::ANSWER_COLLECTION) {
            $params["context"] = Media::getById($id);
        } else if($type == "activitypub"){
            $params = [
                "comments" => ActivitypubObject::getNewsComments($id)
            ];
        } else {
        	throw new CTKException("Error : the type is unknown ".$type);
        }

        if(@$params["parentType"] == City::COLLECTION) $params['canComment'] = true;

        $params["idComment"] = $id;
        if ($json == true) {
            return Rest::json($params["comments"]);
        }
        
        if(Yii::app()->request->isAjaxRequest){
            if($type == "activitypub"){
                return $controller->renderPartial("../comment/commentActivitypub" , $params, true);
            }else if($type != Room::COLLECTION && $type != Room::COLLECTION_ACTIONS)
                return $controller->renderPartial("../comment/commentPodSimple" , $params, true);
            else
                return $controller->renderPartial("../comment/commentPodActionRooms" , $params, true);
	    }else{
            if($type == "activitypub"){
                return $controller->renderPartial("../comment/commentActivitypub" , $params, true);
            }else if($type != Room::COLLECTION && $type != Room::COLLECTION_ACTIONS)
                return $controller->renderPartial("../comment/commentPod" , $params, true);
            else
                return $controller->renderPartial("../comment/commentPodActionRooms" , $params, true);
        }
    }

 
}