<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment;
use CAction;
use Comment;
use Person;
use Rest;
use Yii;

class DeleteAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($id= null) {
    	//Check if connected
        if( ! Person::logguedAndValid()) {
            $res = array("result"=>false, "msg"=>"You must be loggued to delete a comment");
        } else {
            $res = Comment::delete($id, Yii::app()->session["userId"]);
        }
        return Rest::json( $res );
    }
}