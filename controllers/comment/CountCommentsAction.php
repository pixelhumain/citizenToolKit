<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment;

use CAction, Yii, Comment, Rest;
class CountCommentsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();

        if ( isset(Yii::app()->session["userId"]) && @$_POST['type'] && @$_POST['id'] ) {
            $res = array( "count" => Comment::countFrom(@$_POST['from'],$_POST['type'],$_POST['id'],@$_POST['path']), "time"=>time() ); 
        } else {
            $res = array("count" => -1 );
        }
        return Rest::json( $res );
    }
}

