<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Authorisation;
use Cms;
use Form;
use MongoDate;
use MongoId;
use Organization;
use PHDB;
use Template;
use Yii;

class CampagneMigrationAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		$user = Yii::app()->session['userId'] ?? '';
		if (empty($user) || !Authorisation::isUserSuperAdmin($user))
			return;
		$this->update_campagne_view();
		$this->update_commun_campagne_config();
		$this->reset_fundings();
		$this->migrate_block();
	}

	private function update_campagne_view() {
		$db_where = ['slug' => 'lesCommunsDesTierslieux'];
		$db_tl = PHDB::findOne(Organization::COLLECTION, $db_where, ['costum']);
		$db_tl['costum']['fUserDashboard'] = 'aactluserdashboard';
		PHDB::update(Organization::COLLECTION, $db_where, ['$set' => $db_tl]);
		echo 'update campagne view<br>';
	}

	private function update_commun_campagne_config() {
		$form = '6438366673d20a0de1533c77';
		$db_config = PHDB::findOneById(Form::COLLECTION, $form, ['config', 'parent']);
		$parent_id = array_keys($db_config['parent']);
		$parent_id = $parent_id[0];
		$db_config = PHDB::findOneById(Form::COLLECTION, $db_config['config'], ['campagne']);
		$campagne_key = array_keys($db_config['campagne']);
		$campagne_key = $campagne_key[0];
		$data = ['$set' => ['costum.campagne' => $campagne_key]];
		// $option = [
		// 	'multi'			=> true,
		// 	'multiple'		=> true,
		// 	'upsert'		=> true,
		// 	'writeConcern'	=> 1,
		// ];
		$criteria = ['_id' => new MongoId($parent_id)];
		$output = PHDB::update(Organization::COLLECTION, $criteria, $data);
		echo 'update campagne config ' . json_encode($output) . '<br>';
	}

	private function reset_fundings() {
		$form = '6438366673d20a0de1533c77';
		$db_where = [
			"form"								=> $form,
			"answers.aapStep1.depense.financer"	=> [
				'$elemMatch'	=> [
					'$or'	=> [
						["bill"				=> ['$exists'	=> 1]],
						["tlid"				=> ['$exists'	=> 1]],
						["status"			=> ['$exists'	=> 0]],
						["status"			=> "pending"],
						["payment_method"	=> ['$exists'	=> 1]],
					]
				]
			]
		];
		$db_answers = PHDB::find(Form::ANSWER_COLLECTION, $db_where, ["answers.aapStep1.depense"]);
		foreach ($db_answers as $id => &$db_answer) {
			$depenses = array_filter($db_answer["answers"]["aapStep1"]["depense"], fn($d) => isset($d["financer"]) && is_array($d["financer"]));
			$depenses = array_values($depenses);
			foreach ($depenses as $i => &$depense) {
				$action = [];
				$financers = array_filter($depense["financer"], fn($f) => isset($f["id"]) && isset($f["amount"]) && empty($f["tlid"]));
				$financers = array_map(function ($f) {
					unset($f["payment_method"]);
					unset($f["finkey"]);
					unset($f["bill"]);
					$f["status"] = "pending";
					return ($f);
				}, $financers);
				$financers = array_values($financers);
				if (count($financers))
					$action['$set'] = ["answers.aapStep1.depense.$i.financer" => $financers];
				else
					$action['$unset'] = ["answers.aapStep1.depense.$i.financer" => null];
				PHDB::update(Form::ANSWER_COLLECTION, ["_id" => new MongoId($id)], $action);
			}
		}
		echo 'update campagne funding ' . count($db_answers) . '<br>';
	}

	private function migrate_block() {
		$messages = [];
		$cms = [
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.bannerTxtAndImg",
				"name" => "Banner text and img",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Utilisation de l'entier long directement
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.campCoFinancBanner",
				"name" => "CO-FINANCEMENT BANNER",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.campCoFinancCount",
				"name" => "CO-FINANCEMENT COMPTE A REBOURS",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.campCoFinancForm",
				"name" => "CO-FINANCEMENT FORMULAIRE",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.campCoFinancText",
				"name" => "CO-FINANCEMENT TEXT",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.campDetail",
				"name" => "CAMPAGNE DETAIL",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.campFinancDesc",
				"name" => "CAMPAGNE FINANCEMENT DESCRIPTION",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.campFinancLine",
				"name" => "FINANCEMENT LINE",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.communSlider",
				"name" => "COMMUN SLIDER",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.deposerCommun",
				"name" => "DEPOSER UN COMMUN",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.faqCollapse",
				"name" => "FAQ COLLAPSE",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.footer",
				"name" => "FOOTER COMMUN DES TIERS LIEUX",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.partCommun",
				"name" => "PARTICIPER COMMUN",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.titleAndDesc",
				"name" => "TITLE AND DESCRIPTION",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020, // Conservé en NumberLong
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582 // Conservé en NumberLong
			],
			[
				"type" => "blockCms",
				"path" => "tpls.blockCms.aap.nombreCard",
				"name" => "NOmbre sur une card",
				"description" => "Bloc text avec titre. Idéal pour décrire quelque chose .",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020,
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582
			],
			[
				"type" => "blockCms",
				"name" => "BUTTON AAC (design AAC)",
				"path" => "tpls.blockCms.aap.btnAac",
				"description" => "",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020,
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => new MongoDate(1640854582),
			],
			[
				"type" => "blockCms",
				"name" => "Project action slide (design AAC)",
				"path" => "tpls.blockCms.coform.projectActionSlide",
				"description" => "",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020,
				"creator" => "5e14a3b3690864e1328b45de",
				"collection" => "cms",
				"originalColor" => "#43C9B7",
				"showbtn" => "false",
				"updated" => new MongoDate(1640854582),
			],
			[
				"type" => "blockCms",
				"collection" => "cms",
				"created" => 1723045525,
				"creator" => "5e14a3b3690864e1328b45de",
				"modified" => new MongoDate(1723045525),
				"name" => "Project Financing ProgressBar (design AAC)",
				"parent" => [
					"669638d265334b37f30207a3" => [
						"type" => "organizations"
					]
				],
				"path" => "tpls.blockCms.coform.projectFinancingProgressBar",
				"source" => [
					"insertOrign" => "costum",
					"key" => "aac",
					"keys" => [
						"aac"
					]
				],
				"updated" => 1723045525,
				"ftl" => "true",
				"originalColor" => "#43C9B7",
				"showtitle" => false,
			],
			[
				"type" => "blockCms",
				"name" => "Search obj",
				"path" => "tpls.blockCms.widget.searchObj",
				"description" => "",
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"created" => 1605854020,
				"creator" => "5fb4d0eb8b509c5d4f8b4667",
				"collection" => "cms",
				"updated" => 1640854582,
			],
			
		];
		$templates = [
			[
				"collection" => "templates",
				"name" => "BANNER TEXT AND IMAGE",
				"path" => "tpls.blockCms.aap.bannerTxtAndImg",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en timestamp
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null,
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "CAMPAGNE CO-FINANCEMENT BANNERR",
				"path" => "tpls.blockCms.aap.campCoFinancBanner",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en NumberLong
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null,
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "CAMPAGNE CO-FINANCEMENT COMPTE A REBOURS",
				"path" => "tpls.blockCms.aap.campCoFinancCount",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "CAMPAGNE CO-FINANCEMENT FORMULAIRE",
				"path" => "tpls.blockCms.aap.campCoFinancForm",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "CAMPAGNE CO-FINANCEMENT TEXT",
				"path" => "tpls.blockCms.aap.campCoFinancText",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "CAMPAGNE DETAIL",
				"path" => "tpls.blockCms.aap.campDetail",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "graph",
				"shared" => true
			],
			[
				"name" => "CAMPAGNE FINANCEMENT",
				"path" => "tpls.blockCms.aap.campFinancDesc",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "FINANCEMENT LINE",
				"path" => "tpls.blockCms.aap.campFinancLine",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "COMMUN SLIDER",
				"path" => "tpls.blockCms.aap.communSlider",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "DEPOSER UN COMMUN",
				"path" => "tpls.blockCms.aap.deposerCommun",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "FAQ COLLAPSE",
				"path" => "tpls.blockCms.aap.faqCollapse",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "graph",
				"shared" => true
			],
			[
				"name" => "FOOTER COMMUN DES TIERS LIEUX",
				"path" => "tpls.blockCms.aap.footer",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "PARTICIPER COMMUN",
				"path" => "tpls.blockCms.aap.partCommun",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "graph",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "TITLE AND DESCRIPTION",
				"path" => "tpls.blockCms.aap.titleAndDesc",
				"type" => "blockCms",
				"updated" => 1709704021, // Conservé en Long
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256, 0), // Transformé en MongoDate
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => null, // Mis à null
				"category" => "aap",
				"shared" => true
			],
			[
				"collection" => "templates",
				"name" => "Nombre sur un card",
				"path" => "tpls.blockCms.aap.nombreCard",
				"type" => "blockCms",
				"updated" => 1709704021,
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256),
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => "66cf034555105f6b4f04aa57",
				"category" => "aap",
				"shared" => true,
			],
			[
				"collection" => "templates",
				"name" => "BUTTON ACC",
				"path" => "tpls.blockCms.aap.btnAac",
				"type" => "blockCms",
				"updated" => 1709704021,
				"costumId" => "6065d18a690864d44a8b49c7",
				"costumType" => "organizations",
				"modified" => new MongoDate(1637649256),
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"source" => [
					"insertOrign" => "costum",
					"key" => "testcmsengin",
					"keys" => [
						"testcmsengin"
					]
				],
				"blocTplId" => "66ebd27ade934a831cb38eac",
				"category" => "aap",
				"shared" => true,
			],
			[
				"collection" => "templates",
				"name" => "Project action slide (design AAC)",
				"path" => "tpls.blockCms.coform.projectActionSlide",
				"type" => "blockCms",
				"updated" => 1709704021,
				"modified" => new MongoDate(1637649256),
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"blocTplId" => "66bb48d255105f6b4ff6de94",
				"category" => "coform",
				"shared" => true,
			],
			[
				"collection" => "templates",
				"name" => "Project Financing ProgressBar (design AAC)",
				"path" => "tpls.blockCms.coform.projectFinancingProgressBar",
				"type" => "blockCms",
				"updated" => 1709704021,
				"modified" => new MongoDate(1637649256),
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"blocTplId" => "66bb477b55105f6b4ff6dd12",
				"category" => "directory",
				"shared" => true,
			],
			[
				"collection" => "templates",
				"name" => "Search obj",
				"path" => "tpls.blockCms.widget.searchObj",
				"type" => "blockCms",
				"updated" => 1709704021,
				"modified" => new MongoDate(1637649256),
				"parent" => [
					"6065d18a690864d44a8b49c7" => [
						"type" => "organizations",
						"name" => "testCmsEngin"
					]
				],
				"blocTplId" => "66bb3f2e55105f6b4ff6be0e",
				"category" => "directory",
				"shared" => true,
			],
		];
		echo "inserted block :<br>";
		$length = count($cms);
		if ($length != count($templates)) {
			echo "Le nombre de block ne correspond pas avec le nombre de template<br>";
			return;
		}
		for ($i = 0; $i < $length; $i++) {
			$exists = PHDB::count(Cms::COLLECTION, ["path" => $cms[$i]["path"]]);
			if ($exists)
				continue;
			PHDB::insert(Cms::COLLECTION, $cms[$i]);
			if (empty($cms[$i]["_id"])) {
				echo "Impossible d'ajouter le block<br>";
				return;
			}
			$templates[$i]["blocTplId"] = (string)$cms[$i]["_id"];
			PHDB::insert(Template::COLLECTION, $templates[$i]);
			$messages[(string)$cms[$i]["_id"]] = $cms[$i]["path"];
		}
		foreach ($messages as $id => $msg)
			echo "${id} => ${msg} <br>";
		echo "<br>";
	}
}
