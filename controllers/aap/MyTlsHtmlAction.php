<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use CacheHelper;
use Form;
use MongoDate;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;
use Yii;

class MyTlsHtmlAction extends  \PixelHumain\PixelHumain\components\Action {
	public function run() {
		$user = Yii::app()->session['userId'] ?? '';
		$form = $_POST['form'] ?? '';
		if (empty($user) || empty($form))
			return (Rest::json([
				'success'	=> 0,
				'content'	=> 'Required field missing'
			]));
		function fn_financer_in_cart(array $financer, string $tl) {
			if (!empty($financer['bill']) || empty($financer['id']) || $financer['id'] !== $tl)
				return (0);
			if (!empty($financer["payment_method"]))
				return (0);
			if (!empty($financer['status']) && $financer['status'] !== 'pending')
				return (0);
			return (1);
		};
		$db_fields = ['name', 'links.members', 'profilImageUrl'];
		$db_tls = Organization::get_tls($db_fields, $user, 1);
		$db_orgas = Organization::get_nontls($db_fields, $user, 1);
		// nombre de financement fait par les tiers-lieux
		$tls_fundings = Aap::funding_by_tls($db_tls, $form);
		$orgas_fundings = Aap::funding_by_tls($db_orgas, $form);
		$tls_fundings = $tls_fundings["funds"];
		$orgas_fundings = $orgas_fundings["funds"];
		$this->_prepare_data($db_tls, $tls_fundings);
		$this->_prepare_data($db_orgas, $orgas_fundings);
		$args = [
			'tls' => $db_tls,
			'orgas' => $db_orgas,
			'selected'		=> $_POST['selected'] ?? null,
			"can_checkout"	=> Aap::camp_can_open_cart($form),
		];
		return (Rest::json([
			'success'	=> 1,
			'content'	=> $this
				->getController()
				->renderPartial('costum.views.custom.aap.cart.tl_list', $args)
		]));
	}
	
	private function _prepare_data(&$organizations, $fundings) {
		foreach ($organizations as $tl_id => &$db_tl) {
			// image par défaut
			if (empty($db_tl['profilImageUrl']))
				$db_tl['profilImageUrl'] = \Yii::app()
				                               ->getModule('co2')
				                               ->getAssetsUrl() . '/images/thumb/default_organizations.png';
			// nombre de financement
			$funding = array_filter($fundings, fn($fun) => $fun["id"] === $tl_id);
			$funding = count($funding) ? end($funding) : [];
			$db_tl['fund_number'] = !empty($funding["communs"]) ? count($funding["communs"]) : 0;
		}
	}
}
