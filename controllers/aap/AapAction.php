<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use PHDB;
use Action;
use DataValidator;
use DateTime;
use MongoId;
use Rest;
use Project;
use Form;
use Person;
use Document;
use MongoDate;
use Notification;
use ActStr;
use Comment;
use Yii;
use CTKException;
use Element;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class AapAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($action){
        $excludeAction = ["getDiscussionSpace"];
        if(!Person::logguedAndAuthorized() && (!empty($action) && !in_array($action, $excludeAction) ))
            return Yii::t("common","Can not update the element : you are not authorized to update that element !");
        else{
            $controller = $this->getController();
            $params = $_POST;
            $params["controller"] = $controller;
            return self::$action($params);
        }
    }
    
    private function notification_history_by_anwerid($params){
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,($params["answerId"]));
        return $params["controller"]->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.NotificationHistory",array(
            "answer" => $answer
        ),true);
    }

    private function update_seen_status($params) {
        $res = [
            'msg' => "You are not logged in",
            'result' => false
        ];
        $params = DataValidator::clearUserInput($params);
        if(Person::logguedAndValid()) {
            if(isset($params["id"]) && isset($params["collection"]) && isset($params["path"]) && isset($params["value"])) {
                if(isset($params["setType"]) && $params["setType"] == "isoDate") {
                    $now = date(DateTime::ATOM, time());
                    $params["value"] = new MongoDate(strtotime($now));
                }
                $where = [];
                $ids = [];
                $options = [];
                if (is_array($params["id"])) {
                    foreach ($params["id"] as $id) {
                        $ids[] = new MongoId($id);
                    }
                    $where["_id"] = ['$in' => $ids];
                    $options = ['multi' => true, 'multiple' => true, 'upsert' => true];
                } else {
                    $where["_id"] = new MongoId($params["id"]);
                }
                if($elt = PHDB::updateWithOptions($params["collection"], $where, ['$set' => [$params["path"] => $params["value"], "updated" => time()]], $options)) {
                    $res['result'] = true;
                    $res['elt'] = $elt;
                    $res['id'] = $params["id"];
                    $res['msg'] = "Element updated";
                } else {
                    $res['result'] = false;
                    $res['msg'] = "Something went wrong";
                }
            } else {
                $res['msg'] = "Something went wrong, call the admin";
            }
        }
        return Rest::json($res);
    }

    private function already_contributors($params) {
        $res = [
            'msg' => "You are not logged in",
            'elt' => [],
            'result' => false
        ];
        $params = DataValidator::clearUserInput($params);
        if(Person::logguedAndValid()) {
            if(isset($params["userId"]) && (isset($params["answerId"]) || isset($params["projectId"]))) {
                if (isset($params["answerId"])) {
                    $answer = Element::getByTypeAndId("answers", $params["answerId"]);
                }
                if (isset($params["projectId"])) {
                    $answer = Element::getByTypeAndId(Project::COLLECTION, $params["projectId"], ["links.contributors"]);
                }
                if(isset($answer["links"]["contributors"][$params["userId"]])) {
                    $res["result"] = true;
                    $res["elt"] = $answer;
                    $res["msg"] = "You are already a contributor";
                } else {
                    !empty($answer) ? $res["elt"] = $answer : "";
                    $res["msg"] = "You are not contributor yet";
                    $res["result"] = false;
                }
            } else {
                $res['msg'] = "Something went wrong, call the admin";
            }
        }
        return Rest::json($res);
    }

    private function notifyUpdateStatus($params){
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$params["answerId"],["answers.aapStep1.titre","context","project","status"]);
        $contextId = array_keys($answer["context"])[0];
        $contextType = $answer["context"][$contextId]["type"];
        $answerName="";
        if(!empty($answer["status"])){
            if(!empty($answer["answers"]["aapStep1"]["titre"])){
				$answerName = $answer["answers"]["aapStep1"]["titre"];
				$res["{what}"] = [$answerName];
			}

            Notification::constructNotification(
                ActStr::VERB_UPDATE_STATUT_AAP,
                array(	"id" => Yii::app()->session["userId"],
                        "name"=> Yii::app()->session["user"]["name"]
                ),
                array(
                    "id"=> $contextId, 
                    "type" => $contextType
                ),
                array(	
                    "type"=> Form::ANSWER_COLLECTION,
                    "id"=> $params["answerId"],
                    "name"=>$answerName,
                    "isAap"=> true,
                    //"isConvertedToProject"=> !empty($answer["project"]["id"]),
                    "url" => Aap::generateNotificationUrl($params["answerId"]),
                    "statut" => $answer["status"]
                ),
                Form::ANSWER_COLLECTION,
                null,
                null
            );
        }
    }

    private function notifyAddDepenseLine($params){
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$params["answerId"],["answers.aapStep1.titre","context","project","status"]);
        $contextId = array_keys($answer["context"])[0];
        $contextType = $answer["context"][$contextId]["type"];
        $answerName="";
        if(!empty($params["depense"])){
            if(!empty($answer["answers"]["aapStep1"]["titre"])){
				$answerName = $answer["answers"]["aapStep1"]["titre"];
				$res["{what}"] = [$answerName];
			}

            Notification::constructNotification(
                ActStr::ADD_DEPENSE_LINE,
                array(	"id" => Yii::app()->session["userId"],
                        "name"=> Yii::app()->session["user"]["name"]
                ),
                array(
                    "id"=> $contextId, 
                    "type" => $contextType
                ),
                array(	
                    "type"=> Form::ANSWER_COLLECTION,
                    "id"=> $params["answerId"],
                    "name"=>$answerName,
                    "isAap"=> true,
                    //"isConvertedToProject"=> !empty($answer["project"]["id"]),
                    "url" => Aap::generateNotificationUrl($params["answerId"]),
                    "statut" => [$params["depense"]]
                ),
                Form::ANSWER_COLLECTION,
                null,
                null
            );
        }
    }

    private function notifyAddEvaluation($params){
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$params["answerId"],["answers.aapStep1.titre","context","project","status"]);
        $contextId = array_keys($answer["context"])[0];
        $contextType = $answer["context"][$contextId]["type"];
        $answerName="";

        if(!empty($answer["answers"]["aapStep1"]["titre"])){
            $answerName = $answer["answers"]["aapStep1"]["titre"];
            $res["{what}"] = [$answerName];
        }

        Notification::constructNotification(
            ActStr::ADD_EVALUATION,
            array(	"id" => Yii::app()->session["userId"],
                    "name"=> Yii::app()->session["user"]["name"]
            ),
            array(
                "id"=> $contextId, 
                "type" => $contextType
            ),
            array(	
                "type"=> Form::ANSWER_COLLECTION,
                "id"=> $params["answerId"],
                "name"=>$answerName,
                "isAap"=> true,
                //"isConvertedToProject"=> !empty($answer["project"]["id"]),
                "url" => Aap::generateNotificationUrl($params["answerId"]),
                "statut" => ""
            ),
            Form::ANSWER_COLLECTION,
            null,
            null
        );
    }

    private function notifyAddFinance($params){
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$params["answerId"],["answers.aapStep1.titre","context","project","status"]);
        $contextId = array_keys($answer["context"])[0];
        $contextType = $answer["context"][$contextId]["type"];
        $answerName="";

        if(!empty($answer["answers"]["aapStep1"]["titre"])){
            $answerName = $answer["answers"]["aapStep1"]["titre"];
            $res["{what}"] = [$answerName];
        }
        if(!empty($params["status"])){
            Notification::constructNotification(
                ActStr::ADD_FINANCE,
                array(	"id" => Yii::app()->session["userId"],
                        "name"=> Yii::app()->session["user"]["name"]
                ),
                array(
                    "id"=> $contextId, 
                    "type" => $contextType
                ),
                array(	
                    "type"=> Form::ANSWER_COLLECTION,
                    "id"=> $params["answerId"],
                    "name"=>$answerName,
                    "isAap"=> true,
                    //"isConvertedToProject"=> !empty($answer["project"]["id"]),
                    "url" => Aap::generateNotificationUrl($params["answerId"]),
                    "statut" => [$params["status"]]
                ),
                Form::ANSWER_COLLECTION,
                null,
                null
            );
        }
    }

    private function notifySwitchToProject($params){
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$params["answerId"],["answers.aapStep1.titre","context","project","status"]);
        $contextId = array_keys($answer["context"])[0];
        $contextType = $answer["context"][$contextId]["type"];
        $answerName="";

        if(!empty($answer["answers"]["aapStep1"]["titre"])){
            $answerName = $answer["answers"]["aapStep1"]["titre"];
            $res["{what}"] = [$answerName];
        }

        Notification::constructNotification(
            ActStr::PROPOSAL_TO_PROJECT,
            array(	"id" => Yii::app()->session["userId"],
                    "name"=> Yii::app()->session["user"]["name"]
            ),
            array(
                "id"=> $contextId, 
                "type" => $contextType
            ),
            array(	
                "type"=> Form::ANSWER_COLLECTION,
                "id"=> $params["answerId"],
                "name"=>$answerName,
                "isAap"=> true,
                //"isConvertedToProject"=> !empty($answer["project"]["id"]),
                "url" => Aap::generateNotificationUrl($params["answerId"]),
                "statut" => [$params["status"]]
            ),
            Form::ANSWER_COLLECTION,
            null,
            null
        );
    }
    private function getDiscussionSpace($params) {
        $res = [];
        $answerId = $params["answerId"] ?? null;
        $formId = $params["formId"] ?? null;
        if(!empty($answerId)) {
            $res = Form::getCommentedQuestionParams($params["answerId"], $formId, true, null, [
                "newCommentId" => $params["newCommentId"] ?? null
            ]);
        }
        if (isset($params["tpl"]) && $params["tpl"] == "json") {
            return Rest::json(["params" => $res]);
        } else {
            return $params["controller"]->renderPartial("co2.views.comment.commentCoform",["params" => $res],true) ;
        }
    }
    private function getDepenseCard($params) {
        $res = [];
        $answerId = $params["answerId"] ?? null;
        if (!empty($answerId)) {
            $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerId, ["form", "user", "links", "answers.aapStep1.depense"]);
            $initAnswerFiles = Document::getListDocumentsWhere(
                [
                    "id" => (string)$answer["_id"],
                    "type" => 'answers'
                ], 
                "image"
            );
            if (!empty($answer["answers"]["aapStep1"]["depense"]) && isset($params["depenseId"]) && isset($answer["answers"]["aapStep1"]["depense"][$params["depenseId"]])) {
                $res["depense"] = $answer["answers"]["aapStep1"]["depense"][$params["depenseId"]];
            }
            foreach ($initAnswerFiles as $key4 => $d4) {
                if(isset($d4["imageKey"])) {
                    $res["depenseImg"][$d4["imageKey"]] = $d4;
                }
            }
            isset($params["depenseId"]) ? $res["depenseId"] = $params["depenseId"] : "";
            $res["answer"] = $answer;
        }
        return $params["controller"]->renderPartial("survey.views.tpls.forms.aap.depenseCard",["params" => $res],true) ;
    }
    private function createNewDiscu($params) {
        $res = [
            'msg' => "You are not logged in",
            'result' => false
        ];
        $params = DataValidator::clearUserInput($params);
        if(Person::logguedAndValid()) {
            if(isset($params["id"]) && isset($params["collection"]) && isset($params["path"]) && isset($params["value"])) {
                $where["_id"] = new MongoId($params["id"]);
                if(
                    $elt = PHDB::update(
                        $params["collection"], 
                        $where, 
                        [
                            '$set' => [
                                $params["path"] => $params["value"], "updated" => time()
                            ]
                        ]
                    )
                ) {
                    $res['result'] = true;
                    $res['elt'] = $elt;
                    $res['id'] = $params["id"];
                    $res['msg'] = "Element updated";
                } else {
                    $res['result'] = false;
                    $res['msg'] = "Something went wrong";
                }
            } else {
                $res['msg'] = "Something went wrong, call the admin";
            }
        }
        return Rest::json($res);
    }
    private function deleteDiscuGroup($params) {
        $res = [
            'msg' => "You are not logged in",
            'result' => false
        ];
        $params = DataValidator::clearUserInput($params);
        if(Person::logguedAndValid()) {
            if(isset($params["id"]) && isset($params["discucommentpath"]) && isset($params["path"])) {
                $where["_id"] = new MongoId($params["id"]);
                if(
                    $elt = PHDB::update(
                        "answers", 
                        $where, 
                        [
                            '$unset' => [
                                $params["path"] => true, "updated" => time()
                            ]
                        ]
                    )
                ) {
                    PHDB::remove(
                        Comment::COLLECTION, 
                        [
                            "path" => $params["discucommentpath"], 
                        ]
                    );
                    $res['result'] = true;
                    $res['elt'] = $elt;
                    $res['id'] = $params["id"];
                    $res['msg'] = "Element deleted";
                } else {
                    $res['result'] = false;
                    $res['msg'] = "Something went wrong";
                }
            } else {
                $res['msg'] = "Something went wrong, call the admin";
            }
        }
        return Rest::json($res);
    }
    private function updateSelection($params) {
        $res = [
           'msg' => "You are not logged in",
           'result' => false
        ];
        $params = DataValidator::clearUserInput($params);
        if (isset($params["id"]) && isset($params["selectionData"])) {
            $values = [];
            $where["_id"] = new MongoId($params["id"]);
            foreach ($params["selectionData"] as $key => $value) {
                $values["answers.aapStep2.choose.$key"] = $value;
            }
            $values["updated"] = time();
            if ($elt = PHDB::update("answers", $where, ['$set' => $values])) {
                $res['result'] = true;
                $res['elt'] = $elt;
                $res['id'] = $params["id"];
                $res['msg'] = "Element updated";
            } else {
                $res['result'] = false;
                $res['msg'] = "Something went wrong";
            }
        }
        return Rest::json($res);
    }
}