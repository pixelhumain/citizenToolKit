<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Answer;
use Form;
use Pdf;
use PHDB;
use Yii;
use MongoId;
use Document;
use ExportToWord;
use Element;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class AttachedFile extends \PixelHumain\PixelHumain\components\Action{
    public function run($answerid,$template){
        $controller = $this->getController();
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$answerid);
        $inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent"=>$answer["form"],"step" => "aapStep1"));
        $params =[
            "formId" => $answer['form']
        ];

        $templateId = $template;
        $templates = Aap::templateEmail($controller,$params);
        $templateHtml = $templates[$templateId]["html"];

        $templateHtml = str_replace("Télécharger le fichier PDF","",$templateHtml);
        $initImage = Document::getListDocumentsWhere(
            array(
                "id"=> $answer["form"],
                "type"=>'form',
                "subKey"=>'imageSignatureEmail',
            ), "image"
        );
        $financors = Aap::getAllFinancorsByFormId($answer["form"],false);
        $answer["signature"] = !empty($initImage["0"]["imagePath"]) ? $initImage["0"]["imagePath"] :"empty" ;
        
        $params["preview"] = false;
        $html = Aap::parseEmailVariable($templateHtml,$answer,$inputs,$financors,$params);

        $paramsPdf = array( 
            "saveOption" => "D" , 
            "header" => false,
            "footer" => false,
            "textShadow" => false,
            "docName" => "notif_".$answerid.".pdf",
        );
        $paramsPdf["html"] =  $html;    
        return Pdf::createPdf($paramsPdf);
    }
}