<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;
use Form;
use Aap;
use PHDB;
use Rest;
use Yii;
use Cms;
use MongoDate;
use MongoId;
use Authorisation;
use CacheHelper;
use Organization;
use Costum;
use DateTime;
use Element;
use Template;

class GenerateFormOcecoAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller = $this->getController();
        $params = $_POST;
        if(empty($params["elId"]) && empty($params["elType"])){
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Invalid request")));
        }elseif(!Authorisation::isElementAdmin($params["elId"],$params["elType"],Yii::app()->session["userId"])){
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","You are not authorized")));
        }else{
            $params["el"] = PHDB::findOneById($params["elType"],$params["elId"]);
        }

        if(!empty($params["createOceco"]) && !empty($params["action"])){
            $res = self::createOceco($controller,$params["elId"],$params["elType"],$params["formName"], $params["action"]);
            return Rest::json($res);
        }

        if(!empty($params["createForm"])){
            $res = null;
            if(!empty($params["inputs"]))//default inputs
                $res = self::createForm($controller,$params["elId"],$params["elType"],$params["inputs"],$params["formName"]);
            else
                $res = self::createForm($controller,$params["elId"],$params["elType"],null,$params["formName"]);
            return Rest::json($res);
        }
    }

    private function createOceco ($controller,$idEl,$typeEl,$formName,$action): array{
        $el = PHDB::findOne($typeEl,array("_id" => new MongoId($idEl)),array("slug","name","collection","costum"));
        if($action == "createOceco") {
            $el = self::createCostumOceco($el,$controller);
        } elseif ($action == "createAac") {
            $el = self::createCostumAac($el,$controller);
        }
        //self::addAapCms($el);
        //begin create form config
        $templateConfig = null;
        $templateConfig = PHDB::findOne(Form::COLLECTION,array(
                "parent.".(string)$el["_id"] => ['$exists' => true],
                "type" => "aapConfig"
            ) 
        );
        if(empty($templateConfig)){
            $templateConfig = json_decode(file_get_contents("../../modules/survey/data/ocecotools/configFormOcecoform.json", FILE_USE_INCLUDE_PATH),true);
            if($action == "createAac") {
                $templateConfig = json_decode(file_get_contents("../../modules/survey/data/ocecotools/configFormAacForm.json", FILE_USE_INCLUDE_PATH),true);
            }
            $parentConfig = [];
            $parentConfig[$idEl] = array(
                "name" => $el["name"],
                "type" => $typeEl
            );
            $templateConfig["parent"] = $parentConfig;
    
            $inputsConfig = self::createInputsOceco(null,$action);
            foreach ($inputsConfig as $keyinp => $valueinp) {
                unset($inputsConfig[$keyinp]["formParent"]);
            }
            Yii::app()->mongodb->selectCollection(Form::INPUTS_COLLECTION)->batchInsert($inputsConfig);
            $templateConfig["subForms"]["aapStep1"]["inputs"] = (string)$inputsConfig[0]["_id"];
            $templateConfig["subForms"]["aapStep2"]["inputs"] = (string)$inputsConfig[1]["_id"];
            $templateConfig["subForms"]["aapStep3"]["inputs"] = (string)$inputsConfig[2]["_id"];
            $templateConfig["subForms"]["aapStep4"]["inputs"] = (string)$inputsConfig[3]["_id"];
    
            Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($templateConfig);
        }


        $configId = (string)$templateConfig["_id"];
        $config = $templateConfig;
        $configElemId = array_keys($config["parent"])[0];
        $configElem = PHDB::findOneById($config["parent"][$configElemId]["type"],$configElemId);

        //begin create form parent
        $times = time();
        $currentDate = new DateTime();
        $endDate = new MongoDate(strtotime($currentDate->modify('+60 days')->format('d-m-Y')));
        $form = json_decode(file_get_contents("../../modules/survey/data/ocecotools/ocecoformtemplate.json", FILE_USE_INCLUDE_PATH),true);
        if($action == "createAac") {
            $form = json_decode(file_get_contents("../../modules/survey/data/ocecotools/aacformtemplate.json", FILE_USE_INCLUDE_PATH),true);
        }
        $form["config"] = $configId;
        $form["name"] = $formName;
        $p = [];
        $p[$idEl] = array(
            "name" => $el["name"],
            "type" => $typeEl
        );
        $form["parent"] = $p;
        $form["creator"] = Yii::app()->session["userId"];
        $form["created"] = $times;
        $form["startDate"] = new MongoDate($times);
        $form["startDateNoconfirmation"] = new MongoDate($times);
        $form["endDate"] =  $endDate;
        $form["endDateNoconfirmation"] = $endDate;
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($form);

        //create inputs
        $inputForm = self::createInputsOceco((string) $form["_id"], $action);
        Yii::app()->mongodb->selectCollection(Form::INPUTS_COLLECTION)->batchInsert($inputForm);
        if($action == "createAac") {
            $contentTitle = "<div style=\"font-family:'Montserrat-Regular';font-size:40px;text-align:left;\">REMPLACER PAR VOTRE TITRE</div>";
            $contentText = "<div style=\"text-align:left;font-size:22px;font-family:'Montserrat-Light';\">Lorem Ipsum is a placeholder text used in graphic design, typography, and publishing to demonstrate the appearance of a layout or design without being distracted by the actual content. It is a scrambled Latin text, often attributed to an unknown typesetter in the 15th century, who allegedly used parts of Cicero’s De Finibus Bonorum et Malorum to create a sample text.<br /></div>";
            $mentionTxt =  "<div style=\"text-align:left;\"><div style=\"font-family:'Montserrat-Light';\">Un Logiciel Libre et [Open Source](https://gitlab.adullact.net/pixelhumain) basé sur [COmmunecter](https://www.communecter.org/). <br />Déployé par Open Atlas.<br /></div></div>";
            PHDB::update(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.widget.searchObj", "page" => "les-communs"],
                ['$set' => ["query.formId" => (string) $form["_id"]]]
            );
            PHDB::update(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.widget.searchObj", "page" => "les-communs"],
                ['$set' => ["filters.filtersPersonal.filterPersonal.1.dynamic" => "dynamicContent.".(string) $form["_id"].".checkboxNewaapStep1m03ot9qymmfgashp7l.list"]]
            );
            PHDB::update(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.widget.searchObj", "page" => "les-communs"],
                ['$set' => ["filters.filtersPersonal.filterPersonal.2.dynamic" => "dynamicContent.".(string) $form["_id"].".tags.list"]]
            );
            PHDB::updateWithOptions(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.aap.btnAac", "typeUrl" => "insideModal"],
                ['$set' => ["button" => "/survey/answer/answer/id/new/form/".(string) $form["_id"]]],
                ['multi' => true, 'multiple' => true, 'upsert' => true]
            );
            PHDB::update($typeEl,
                ["_id" => new MongoId($idEl)],
                ['$set' => ["costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#l-aac.buttonList.openModalBtn.config.button" => "/survey/answer/answer/id/new/form/".(string) $form["_id"]]]
            );
            PHDB::update(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.aap.campFinancDesc"],
                ['$set' => ["form" => (string) $form["_id"]]]
            );
            PHDB::update(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.aap.communSlider"],
                ['$set' => ["form" => (string) $form["_id"]]]
            );
            PHDB::update($el["collection"],
                array("_id" => new MongoId((string)$el["_id"])),
                array(
                    '$set' => ["oceco.webConfig.hideLabelOrganism" => "true"]
                )
            );
            PHDB::updateWithOptions(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.aap.partCommun"],
                ['$set' => ["dataConfig.formId" => (string) $form["_id"]]],
                ['multi' => true, 'multiple' => true, 'upsert' => true]
            );
            PHDB::updateWithOptions(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.superCms.elements.title"],
                ['$set' => ["title.fr" => $contentTitle]],
                ['multi' => true, 'multiple' => true, 'upsert' => true]
            );
            PHDB::updateWithOptions(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.superCms.elements.supertext"],
                ['$set' => ["text.fr" => $contentText]],
                ['multi' => true, 'multiple' => true, 'upsert' => true]
            );
            PHDB::updateWithOptions(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.superCms.elements.supertext", "advanced.persistent" => "footer"],
                ['$set' => ["text.fr" => $mentionTxt]],
                ['multi' => true, 'multiple' => true, 'upsert' => true]
            );
            PHDB::updateWithOptions(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.aap.btnAac"],
                ['$set' => ["css.border" => "", "css.hover.border" => ""]],
                ['multi' => true, 'multiple' => true, 'upsert' => true]
            );
            PHDB::updateWithOptions(cms::COLLECTION,
                ["source.key" => $el["slug"] , "path" => "tpls.blockCms.superCms.elements.video", "page" => "en-savoir-plus"],
                ['$set' => ["link.fr" => "https://www.youtube.com/watch?v=QgzyGkgS_Qo"]],
                ['multi' => true, 'multiple' => true, 'upsert' => true]
            );
        }
        return array(
            "result"=> true,
            "msg"=> "Oceco crée",
            "parentForm" => (string) $form["_id"],
            "aap link" => Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")."/co/index/slug/".$configElem["slug"]."/#proposalAap.context.".$el["slug"].".formid.".(string) $form["_id"].".preconfiguration.true" ,
        );
    }

    private function createInputsOceco($idformParent=null, $action = "createOceco"){
        $inputs = json_decode(file_get_contents("../../modules/survey/data/ocecotools/inputsocecoform.json", FILE_USE_INCLUDE_PATH),true);
        if ($action == "createAac") {
            $inputs = json_decode(file_get_contents("../../modules/survey/data/ocecotools/inputsaacform.json", FILE_USE_INCLUDE_PATH),true);
        }
        if($idformParent != null){
            foreach ($inputs as $keyinp => $vinp) {
                $inputs[$keyinp]["formParent"] = $idformParent;
                if(!empty($vinp["inputs"])){
                    foreach ($vinp["inputs"] as $k => $v) {
                        if(!empty($inputs[$keyinp]["inputs"][$k]["positions"])){
                            $pos = array_values($inputs[$keyinp]["inputs"][$k]["positions"])[0];
                            //$kpos = array_keys($inputs[$keyinp]["inputs"][$k]["positions"])[0];
                            $inputs[$keyinp]["inputs"][$k]["positions"][$idformParent] = $pos;
                        }
                    }
                }
            }
        }
        return $inputs;
    }

    private function createCostumOceco($el,$controller){
        if(!isset($el["costum"]["slug"])){
            $costumTemplate = json_decode(file_get_contents("../../modules/survey/data/ocecotools/costumTemplateOcecoform.json", FILE_USE_INCLUDE_PATH),true);
            PHDB::update($el["collection"],
                array("_id" => new MongoId((string)$el["_id"])),
                array(
                    '$set' => ["costum" => $costumTemplate["costum"]]
                )
            );
            self::createOcecoApp((string)$el["_id"],$el["collection"]);
        }else{
            self::createOcecoApp((string)$el["_id"],$el["collection"]);
        }
        CacheHelper::delete($el["slug"]);
        if(CacheHelper::get($_SERVER['SERVER_NAME']))
            CacheHelper::delete($_SERVER['SERVER_NAME']);
        return $el;
    }

    private function createCostumAac($el,$controller){
        if(!isset($el["costum"]["slug"])){
            $template = Element::getElementByName((string)"AACTEMPLATEGENERIQUE",Template::COLLECTION);
            if(!empty($template))
                $template = array_values($template)[0];
            $costumTemplate = json_decode(file_get_contents("../../modules/survey/data/ocecotools/costumTemplateAac.json", FILE_USE_INCLUDE_PATH),true);
            PHDB::update($el["collection"],
                array("_id" => new MongoId((string)$el["_id"])),
                array(
                    '$set' => ["costum" => $costumTemplate["costum"]]
                )
            );
            self::createOcecoApp((string)$el["_id"],$el["collection"]);
            
            $cssfile = json_decode(file_get_contents("../../modules/survey/data/ocecotools/aactemplatecssfile.json", FILE_USE_INCLUDE_PATH),true);
            $cssfile["contextId"] = (string)$el["_id"];
            PHDB::insert("cssFile", $cssfile);
            $params = [
                "parentType" => $el["collection"],
                "parentId" => (string)$el["_id"],
                "parentSlug" => $el["slug"],
                "newCostum" => true
            ];
            Cms::useTemplate($template,$params);
        }else{
            self::createOcecoApp((string)$el["_id"],$el["collection"]);
        }
        CacheHelper::delete($el["slug"]);
        if(CacheHelper::get($_SERVER['SERVER_NAME']))
            CacheHelper::delete($_SERVER['SERVER_NAME']);
        return $el;
    }

    private function createOcecoApp ($idElt,$typeElt){
        $config = file_get_contents("../../modules/co2/data/aap.json", FILE_USE_INCLUDE_PATH);
        $config = json_decode($config, true);
        $where = array("_id" => new MongoId($idElt));

        $el = PHDB::findOneById($typeElt,$idElt,array("slug"));
        $costum = PHDB::findOne(Costum::COLLECTION,array("slug"=> $el["slug"]));
        $path = "costum.";
        /*if(!empty($costum)){
            $path = "";
        }*/

        $sets = array($path."type" => "aap");
        $unsets = array($path."app.#oceco" => true, $path."htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#oceco" => true);

        foreach ($config["app"] as $k => $v) {
            $u = str_replace("Aap", "", $k);
            $sets[$path."app." . $k] = $v;
            $unsets[$path."app." . $u] = true;
        }
        //echo json_encode($sets);exit;

        foreach ($config["menuTopLeft"] as $k => $v) {
            $u = str_replace("Aap", "", $k);
            $sets[$path."htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList." . $k] = $v;
            $sets[$path."htmlConstruct.header.menuTop.left.buttonList.app.buttonList." . $k] = $v;
            $unsets[$path."htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList." . $u] = true;
            $unsets[$path."htmlConstruct.header.menuTop.left.buttonList.app.buttonList." . $u] = true;
        }
        
        
        PHDB::updateWithOptions(
            $typeElt,
            $where,
        array('$set' => $sets/*, '$unset' => array()*/),
            array('multi' => true, 'multiple' => true, 'upsert' => true)
        );
    }

    private function createForm ($controller,$idEl,$typeEl,$defaultInputs=null,$formName){
        $el = PHDB::findOne($typeEl,array("_id" => new MongoId($idEl)),array("slug","name"));
        $form = json_decode(file_get_contents("../../modules/survey/data/ocecotools/emptyForm.json", FILE_USE_INCLUDE_PATH),true);

        $p = [];
        $p[$idEl] = array(
            "name" => $el["name"],
            "type" => $typeEl
        );
        $times = time();
        $currentDate = new DateTime();
        $endDate = new MongoDate(strtotime($currentDate->modify('+60 days')->format('d-m-Y')));
        $form["id"] = $el["slug"].$times;
        $form["name"] = $formName;
        $form["parent"] = $p;
        $form["creator"] = Yii::app()->session["userId"];
        $form["created"] = time();
        $form["startDate"] = new MongoDate($times);
        $form["startDateNoconfirmation"] = new MongoDate($times);
        $form["endDate"] = $endDate;
        $form["endDateNoconfirmation"] =  $endDate;
        $form["subForms"] = [$el["slug"].$times."_0"];
        $subForms = array();;
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($form);

        if(!empty($defaultInputs) && is_array($defaultInputs))
            $subForms = self::createSubForms((string) $form["_id"],$el["slug"].$times."_0",$el,$defaultInputs);
        else
            self::createSubForms((string) $form["_id"],$el["slug"].$times."_0",$el);

        $configLink = Yii::app()->getRequest()->getBaseUrl(true)."/#@".$el["slug"].".view.forms.dir.form.".(string) $form["_id"];
        $standaloneLink =  Yii::app()->getRequest()->getBaseUrl(true)."/#answer.index.id.new.form.".(string) $form["_id"].".mode.w.standalone.true";

        if(!empty($el["costum"]["slug"])){
            $standaloneLink =  Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")."#answer.index.id.new.form.".(string) $form["_id"].".mode.w.standalone.true";
        }
        return array(
            "result"=> true,
            "msg"=> "Oceco crée",
            "parentForm" => (string) $form["_id"],
            "config link" => $configLink,
            "standalone link" => $standaloneLink,
            "contextSlug" => $el["slug"],
            "keyUnik" => isset($subForms["inputs"]) ? array_keys($subForms["inputs"])[0] : ""
        );
    }
    private function createSubForms($parentForm,$subFormKeys,$el,$defaultInputs = null){
        $subform = array(
            "id" => $subFormKeys,
            "name" => "Etape 1",
            "type" => "openForm",
            "created" => time(),
            "creator" => Yii::app()->session["userId"],
            "updated" => time()
        );

        if(!empty($defaultInputs) && is_array($defaultInputs)){
            $subform["inputs"] = [];
            foreach ($defaultInputs as $key => $value) {
                $rand = random_int(1, 200); 
                $key = $el["slug"].time()."_".$rand;
                $paramsId = $value["id"];
                $subform["inputs"][$key] = [
                    "label" => $value["label"],
                    "type" => $value["type"]
                ];
                if($paramsId == "timeEvaluation"){
                    self::timeEvaluationParams($parentForm,$key);
                }
            }
        }
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($subform);
        return $subform;
    }

    private function timeEvaluationParams($formId ,$keyInput){
        $defaultConfig = json_decode(file_get_contents("../../modules/survey/data/ocecotools/timeEvaluation.json", FILE_USE_INCLUDE_PATH),true);
        foreach ($defaultConfig as $key => $value) {
            $defaultConfig[$key.$keyInput] = $value;
            unset($defaultConfig[$key]);
        }
        return PHDB::update(Form::COLLECTION,array("_id"=> new MongoId($formId)),array(
            '$set' => ["params" => $defaultConfig]
        ));
    }
}