<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Answer;
use Form;
use Person;
use PHDB;
use PixelHumain\PixelHumain\components\Action;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Yii;

class ExcelTableAction extends Action {
	public function run() {
		$userSessionId = Yii::app()->session['userId'] ?? '';
		$controller = $this->getController();
		$to_exclude = [
			'tpls.forms.titleSeparator'
		];
		$escape_response = [];
		$map_status = [
			'progress'          => 'En cours',
			'underconstruction' => 'En construction',
			'vote'              => 'En evaluation',
			'finance'           => 'En financement',
			'call'              => 'Appel à participation',
			'newaction'         => 'nouvelle proposition',
			'projectstate'      => 'En projet',
			'prevalided'        => 'Pré validé',
			'validproject'      => 'Projet validé',
			'voted'             => 'Voté',
			'finish'            => 'Términé',
			'suspend'           => 'Suspendu',
		];
		$map_status_reverse = [
			'En cours'              => 'progress',
			'En construction'       => 'underconstruction',
			'En evaluation'         => 'vote',
			'En financement'        => 'finance',
			'Appel à participation' => 'call',
			'nouvelle proposition'  => 'newaction',
			'En projet'             => 'projectstate',
			'Pré validé'            => 'prevalided',
			'Projet validé'         => 'validproject',
			'Voté'                  => 'voted',
			'Términé'               => 'finish',
			'Suspendu'              => 'suspend',
		];

		$db_inputs = PHDB::findByIds(Form::INPUTS_COLLECTION, $_POST['inputs']);
		$db_answers = PHDB::findByIds(Answer::COLLECTION, $_POST['answers'], ['status', 'user', 'answers.aapStep1']);
		$db_form = PHDB::findOneById(Form::COLLECTION, $_POST['form'], ['params.tags.list', 'config', 'parent', 'name']);
		$db_authors = PHDB::findByIds(Person::COLLECTION, array_values(array_map(fn($ans) => $ans["user"], $db_answers)), ["name", "email"]);
		$dbConfig = PHDB::findOneById(Form::COLLECTION, $db_form['config'], ['subForms.aapStep1']);
		$isSuperAdmin = Person::isSuperAdmin();
		$thisParent = ['id' => array_keys($db_form['parent'])[0], 'type' => $db_form['parent'][array_keys($db_form['parent'])[0]]['type']];
		$dbParent = PHDB::findOneById($thisParent['type'], $thisParent['id'], ['links.members']);

		$members = $dbParent['links']['members'] ?? [];
		$isUserParentAdmin = !empty($members[$userSessionId]['isAdmin']) && filter_var($members[$userSessionId]['isAdmin'], FILTER_VALIDATE_BOOLEAN);
		$userParentFormRoles = $members[$userSessionId]['roles'] ?? [];
		$reorderedAnswers = [];
		foreach ($_POST['answers'] as $anAnswerId) {
			$reorderedAnswers[$anAnswerId] = $db_answers[$anAnswerId];
		}

		$parameter = [
			'headers'   => [
				'ID'		=> 'ID',
				"author"	=> "Auteur",
				'status'	=> 'Status',
			],
			'bodies'    => [],
			'sources'   => [
				'status'    => $map_status,
				'status_reverse' => $map_status_reverse,
				'tag_lists' => $db_form['params']['tags']['list'] ?? []
			],
			'canEdit'   => [],
			'canRead'   => [],
			"readonly_cols"	=> ["ID", "author"],
			'formTitle' => $db_form['name'] ?? '',
		];

		$extendables = [
			'answers.aapStep1.depense' => 0,
			'financers'                => []
		];

		foreach ($db_inputs as $db_input) {
			$fields = $db_input['inputs'] ?? [];
			foreach ($fields as $field_id => $field) {
				if (!isset($escape_response[$db_input['step']])) $escape_response[$db_input['step']] = [];
				if (empty($field['type']) || in_array($field['type'], $to_exclude)) {
					$escape_response[$db_input['step']][] = $field_id;
					continue;
				}
				$parameter['headers']['answers.' . $db_input['step'] . '.' . $field_id] = $field['label'] ?? ($field['info'] ?? $field_id);
			}
		}

		foreach ($reorderedAnswers as $answer_id => $answer_value) {
			$isCreator = !empty($answer_value['user']) && $answer_value['user'] === $userSessionId;
			$status = !empty($answer_value['status']) && is_array($answer_value['status']) ? $answer_value['status'] : [];
			$status = array_filter($status, function ($status_value) use ($map_status) {
				return is_string($status_value) && array_key_exists($status_value, $map_status);
			});

			$auth = $db_authors[$answer_value["user"]]["name"];
			if (!empty($db_authors[$answer_value["user"]]["email"]))
				$auth .= "\n(" . $db_authors[$answer_value["user"]]["email"] . ")";
			$data_line = [
				'ID'		=> $answer_id,
				"author"	=> $auth,
				'status'	=> implode(' ; ', array_map(function ($_status) use ($map_status) {
					return $map_status[$_status];
				}, $status)),
			];
			$data_line['answers'] = [];
			foreach ($answer_value['answers'] as $step => $answers) {
				$data_line['answers'][$step] = [];
				foreach ($answers as $header => $answer) {
					if (!empty($escape_response[$step]) && in_array($header, $escape_response[$step])) continue;
					$value = "";
					if (is_string($answer) || is_numeric($answer))
						$value = $answer;
					else if (is_array($answer) && $this->is_elementary_content($answer))
						$value = implode(' ; ', $answer);
					else if ($step === 'aapStep1' && empty($value)) {
						if ($header === 'depense' && !empty($answer)) {
							$value = [];
							foreach ($answer as $expense_index => $expense_value) {
								$output = [
									'poste' => $expense_value['poste'] ?? '',
									'price' => $expense_value['price'] ?? 0
								];
								$extendables['answers.aapStep1.depense'] = max($extendables['answers.aapStep1.depense'], $expense_index);
								$financers = $expense_value['financer'] ?? [];
								$financer_length = count($financers);

								// traitement stockage nombre de colonnes dans la variable temporaire pour stocker les finances
								$temp_finance_array_length = count($extendables['financers']);
								$max = max($temp_finance_array_length, $expense_index);

								for ($i = 0; $i <= $max; $i++) {
									if (!isset($extendables['financers'][$i])) $extendables['financers'][$i] = -1;
								}

								$extendables['financers'][$expense_index] = max($extendables['financers'][$expense_index], $financer_length);
								if ($financer_length > 0) $output['financer'] = [];
								foreach ($financers as $financer_value) {
									$output['financer'][] = [
										'line'   => $financer_value['line'] ?? '',
										'amount' => $financer_value['amount'] ?? 0
									];
								}
								$value[] = $output;
							}
						} else if (strpos($header, "finder") === 0) {
							$header = substr($header, 6);
							UtilsHelper::push_array_if_not_exists("answers.$step.$header", $parameter["readonly_cols"]);
							$value = [];
							foreach ($answer as $ans)
								$value[] .= '"' . $ans["name"] . '"; "'
									. $ans["id"] . '"; "'
									. $ans["type"] . '".';
							$value = implode("\n", $value);
						}
					}
					$data_line['answers'][$step][$header] = $value;
				}
			}
			$parameter['bodies'][] = $data_line;
			$aapStep1ConfigParams = $dbConfig['subForms']['aapStep1']['params'] ?? ['haveEditingRules' => false, 'haveReadingRules' => false];
			$aapStep1ConfigParams['haveEditingRules'] = filter_var($aapStep1ConfigParams['haveEditingRules'] ?? false, FILTER_VALIDATE_BOOLEAN);
			$aapStep1ConfigParams['haveReadingRules'] = filter_var($aapStep1ConfigParams['haveReadingRules'] ?? false, FILTER_VALIDATE_BOOLEAN);

			if ($isSuperAdmin || $isCreator) {
				$parameter['canEdit'][$answer_id] = true;
				$parameter['canRead'][$answer_id] = true;
			} else if (!empty($members[$userSessionId])) {
				if ($aapStep1ConfigParams['haveEditingRules']) {
					$canEditRoles = $aapStep1ConfigParams['canEdit'] ?? [];
					$emptyEditRole = empty($canEditRoles) || count($canEditRoles) === 1 && empty($canEditRoles[0]);
					$matchRoles = array_filter($userParentFormRoles, function ($userParentFormRole) use ($canEditRoles) {
						return in_array($userParentFormRole, $canEditRoles);
					});
					$parameter['canEdit'][$answer_id] = $emptyEditRole && $isUserParentAdmin || count($matchRoles) > 0;
				} else $parameter['canEdit'][$answer_id] = true;
				if ($aapStep1ConfigParams['haveReadingRules']) {
					$canReadRoles = $aapStep1ConfigParams['canRead'] ?? [];
					$emptyReadRole = empty($canReadRoles) || count($canReadRoles) === 1 && empty($canReadRoles[0]);
					$matchRoles = array_filter($userParentFormRoles, function ($userParentFormRole) use ($canReadRoles) {
						return in_array($userParentFormRole, $canReadRoles);
					});
					$parameter['canRead'][$answer_id] = $emptyReadRole && $isUserParentAdmin || count($matchRoles) > 0;
				} else $parameter['canRead'][$answer_id] = true;
			} else {
				$parameter['canEdit'][$answer_id] = !$aapStep1ConfigParams['haveEditingRules'];
				$parameter['canRead'][$answer_id] = !$aapStep1ConfigParams['haveReadingRules'];
			}
		}

		// Trairement des entêtes
		$header_keys = array_keys($parameter['headers']);

		// Ligne des dépenses et des finances
		if (!empty($extendables['answers.aapStep1.depense'])) {
			$insert = [];
			$repetitions = $extendables['answers.aapStep1.depense'];
			$current_header = $parameter['headers']['answers.aapStep1.depense'];

			// indéxer la nouvelle entête
			for ($i = 0; $i < $repetitions; $i++) {
				$insert["answers.aapStep1.depense.$i.poste"] = ($i + 1) . ") $current_header - Poste";
				$insert["answers.aapStep1.depense.$i.price"] = ($i + 1) . ") $current_header - Montant";
				$financer_length = $extendables['financers'][$i] ?? -1;
				for ($j = 0; $j <= $financer_length; $j++) {
					$insert["answers.aapStep1.depense.$i.financer.$j.line"] = ($i + 1) . ") $current_header - Financement (" . ($j + 1) . ")";
					$insert["answers.aapStep1.depense.$i.financer.$j.amount"] = ($i + 1) . ") $current_header - Financement (" . ($j + 1) . ") Montant";
				}
			}
			$index = array_search('answers.aapStep1.depense', $header_keys);
			// insérer le nouvel index dans la liste de l'entête existante
			array_splice($header_keys, $index, 1, array_merge(array_keys($insert)));

			// Recoudre le tout
			$temporaire = [];
			foreach ($header_keys as $header) {
				$temporaire[$header] = $parameter['headers'][$header] ?? $insert[$header];
			}
			$parameter['headers'] = $temporaire;
		}
		return $controller->renderPartial('co2.views.aap.partials.excel_table_view', $parameter);
	}

	private function is_elementary_content(array &$array): bool {
		foreach ($array as $content) {
			if (!is_string($content) & !is_numeric($content)) return false;
		}
		return true;
	}
}
