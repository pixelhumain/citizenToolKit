<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Answer;
use Yii;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;

class IndexAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($page=null,$context=null,$formid=null,$aapview=null, $answerId=null,$projectId = null, $isForAap = true, $step = null) {
        $controller=Yii::app()->getController();
        $entries = array(
            "page" => $page,
            "context" => $context,
            "formid" => $formid,
            "aapview" => $aapview,
            'answerId' => $answerId,
            'projectId' => $projectId,
            'renderThisStep' => $step
        );
        $params = null;
		
        if($isForAap === true || $isForAap == 'true') {
            if(empty($entries["context"]) || empty($entries["formid"])){
                $params = Aap::generateDefautlUrl($controller->costum["slug"]);
                return  $controller->renderPartial("co2.views.aap.partials.formList",array("params"=>$params)); 
            }else{
                $params = Aap::prepareAapGlobalData($entries);
                return $controller->renderPartial("co2.views.aap.index",$params);
            }
        } else {
            return Rest::json(Aap::prepareAapGlobalData($entries));
        }
        
    }
}