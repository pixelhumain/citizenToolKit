<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Answer, PHDB, Authorisation, CAction, Form, Rest, type, Yii, Project, Slug, Action, Element, Organization, Exception;
use Citoyen;
use MongoId;
use DateTime;
use MongoDate;
use DateTimeZone;
use DateInterval;
use DatePeriod;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class ObservatoryAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($request)
    {
        $controller = $this->getController();
        $results = $this->$request();
        return Rest::json($results);
        //return $results;
    }
    private function getTimezone()
    {
        return Yii::app()->session["timezone"];
    }

    private function getProjectId($formId, $contexts)
    {
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers, $projectIdFromContext);
        return $projectId;
    }

    private function userHistories()
    {

        /*$formId = $_POST["formId"];
        $personneId = $_POST["personneId"];
        
        $projectId = $this->getProjectId($formId, $_POST["context"]);

        if(!empty($projectId )){
            $query = array(
                '$and' => array(
                    array('tasks.userId' => new MongoId($personneId)),
                    array("created" => ['$exists' => true]),
                    array("parentId" => ['$in' => $projectId]),
                )
            );

           

            $actions = PHDB::find(Action:: COLLECTION,$query);
            
        }

        return $actions;*/



        $formId = $_POST["personneId"];
        $citoyen = PHDB::find(Citoyen::COLLECTION, array("_id" => new MongoId($formId)), array("name",));
        // [creator] à creer une action [name] le [created]
        $action  = PHDB::find(
            Action::COLLECTION,
            array(
                '$and' => array(
                    array('parentId' => $formId),
                    array('tasks' => ['$exists' => true])
                )
            ),
            array('name', 'creator', 'created')
        );

        // [user] à créer une nouvelle proposition [answers.aapStep1.titre] le [created]  
        $proposition = PHDB::find(
            Form::ANSWER_COLLECTION,
            array(
                '$and' => array(
                    array('user' => $formId),
                    array('answers.aapStep1.titre' => ['$exists' => true]),
                    array('project.id' => ['$exists' => true])
                )
            ),
            array('user', 'answers.aapStep1.titre', 'created')
        );
        //[links.contributors.58453a7840bdda445dflg39d] a contribué à l'actions
        $contribution  = PHDB::find(
            Action::COLLECTION,
            array(
                '$and' => array(
                    array('links.contributors.' . $formId => ['$exists' => true]),
                    array('tasks' => ['$exists' => true])
                )
            ),
            array('created', 'name')
        );
        //[tasks.userId] à créer une tache [tasks.task] le [tasks.createdAt] dans l'action [name] et
        //[tasks.userId] terminé la [tasks.task]  dans l'action [name]    
        $tache = PHDB::find(
            Action::COLLECTION,
            array(
                '$and' => array(
                    array('tasks.userId' => $formId),
                    array('tasks' => ['$exists' => true])
                )
            ),
            array('name', 'created', 'status', 'tasks')
        );


        return ['citoyen' => $citoyen[$formId]['name'], 'action' => $action, 'proposition' => $proposition, 'contribution' => $contribution, 'tache' => $tache];
    }

    /********************************************* METRICS ****************************************************/
    private function projectMetrics()
    {
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        // $projectIdFromContext = [];
        $projectId = array_merge($projectIdFromAnswers, $projectIdFromContext);
        $projectId = array_values(array_unique($projectId));

        $projectsMongoId = [];
        foreach ($projectId as $key => $value)
        {
            array_push($projectsMongoId, new MongoId($value));
        }
        $finished = PHDB::findAndSort(
            Project::COLLECTION,
            [
                '$and' => [
                    [
                        "_id" => ['$in' => $projectsMongoId]
                    ],
                    [
                        '$or' => [
                            ["parent" => ['$exists' => true]],
                            ["parentId" => ['$exists' => true]]
                        ]
                    ],
                    // [
                    //     "properties.avancement" => ['$exists' => true]
                    // ],
                    [
                        "properties.avancement" => ['$in' => ['finished', 'abandoned']]
                    ],
                ]
            ],
            [
                "updated" => -1
            ],
            0,
            ["_id"]
        );
        $inprogress = PHDB::findAndSort(
            Project::COLLECTION,
            [
                '$and' => [
                    [
                        "_id" => ['$in' => $projectsMongoId]
                    ],
                    [
                        '$or' => [
                            ["parent" => ['$exists' => true]],
                            ["parentId" => ['$exists' => true]]
                        ]
                    ],
                    [
                        '$or' => [
                            ["properties.avancement" => ['$in' => ['idea', 'concept', 'started', 'development', 'testing', 'mature']]],
                            ["properties.avancement" => ['$exists' => false]]
                        ]
                    ]
                ]
            ],
            [
                "updated" => -1
            ],
            0,
            ["_id"]
        );
        $results = [
            // "total" => PHDB::count(
            //                 Project:: COLLECTION,
            //                 [
            //                     '$and' => [
            //                         [
            //                             "_id" => ['$in' => $projectsMongoId]
            //                         ],
            //                         // [
            //                         //     "parent" => ['$exists' => true]
            //                         // ]
            //                     ]
            //                 ]
            //             ),
            "total" => count($finished) + count($inprogress),
            "finished" => count($finished),
            "inprogress" => count($inprogress),
            "finishedData" => $finished,
            "inprogressData" => $inprogress
        ];

        return  $results;
    }
    private function actionMetrics()
    {
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $currentDateTime = (new DateTime())->getTimestamp();
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers, $projectIdFromContext);
        $projectId = array_values(array_unique($projectId));

        $results = [
            "total" => 0,
            "todo" => 0,
            "running" => 0,
            "done" => 0,
            "deadlinePassed" => 0,
        ];

        if (!empty($projectId))
        {
            $results["total"] = PHDB::count(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => ['$ne' => 'disabled']),
                    )
                )
            );
            $results["todo"] = PHDB::count(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => "todo"),
                        array(
                            '$or' => [
                                array("tracking" => false),
                                array("tracking" => ['$exists' => false])
                            ]
                        ),
                    )
                )
            );
            $results["running"] = PHDB::count(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => "todo"),
                        array("tracking" => true),
                    )
                )
            );
            $results["done"] = PHDB::count(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => "done"),
                    )
                )
            );
            $results["deadlinePassed"] = PHDB::count(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => "todo"),
                        array(
                            '$or' => [
                                array("endDate" => ['$lt' => new MongoDate($currentDateTime)]),
                                /*array('$expr' => [
                                    '$lt' => [
                                      ['$toDate'=> "endDate" ],
                                      $currentDateTime
                                    ]
                                ])*/
                            ]
                        )
                    )
                )
            );
        }

        return  $results;
    }
    private function proposalMetrics()
    {
        $formId = $_POST["formId"];
        $context = Slug::getBySlug($_POST["context"]);

        $results = [
            "total" => 0,
            "vote" => 0,
            "toBefinanced" => 0,
            "inFinancing" => 0
        ];
        $proposalData = [
            "vote" => PHDB::findAndSort(
                Form::ANSWER_COLLECTION,
                array(
                    '$and' => array(
                        array("form" => $formId),
                        array('status' => "vote"),
                        array('answers.aapStep1.titre' => ['$exists' => true]),
                        array('project.id' => ['$exists' => false])
                    )
                ),
                ['updated' => -1],
                0,
                ["_id"]
            ),
            "toBefinanced" => PHDB::findAndSort(
                Form::ANSWER_COLLECTION,
                array(
                    '$and' => array(
                        array("form" => $formId),
                        array('answers.aapStep1.titre' => ['$exists' => true]),
                        array('answers.aapStep1.depense' => ['$exists' => true]),
                        array('answers.aapStep1.depense.financer' => ['$exists' => false])
                    )
                ),
                ["updated" => -1],
                0,
                ["_id"]
            ),
            "inFinancing" => PHDB::findAndSort(
                Form::ANSWER_COLLECTION,
                array(
                    '$and' => array(
                        array("form" => $formId),
                        array('answers.aapStep1.titre' => ['$exists' => true]),
                        array('answers.aapStep1.depense.financer' => ['$exists' => true])
                    )
                ),
                ["updated" => -1],
                0,
                ["_id"]
            )
        ];

        if (!empty($formId))
        {
            // $results["total"] = PHDB::count(Form:: ANSWER_COLLECTION,
            //     array(
            //         '$and' => array(
            //             array("form" => $formId),
            //             array('status'=> ['$ne'=>"finished"]),
            //             array('answers.aapStep1.titre' => ['$exists'=> true]),
            //             array('project.id' => ['$exists' => false])
            //         )
            //     )
            // );
            $results["total"] = count($proposalData["vote"]) + count($proposalData["toBefinanced"]) + count($proposalData["inFinancing"]);
            $results["vote"] = count($proposalData["vote"]);

            $results["toBefinanced"] = count($proposalData["toBefinanced"]);

            $results["inFinancing"] = count($proposalData["inFinancing"]);

            $results["tobevotedData"] = $proposalData["vote"];
            $results["tobefinancedData"] = $proposalData["toBefinanced"];
            $results["infinancingData"] = $proposalData["inFinancing"];
        }

        return  $results;
    }
    private function financingMetrics()
    {
        $formId = $_POST["formId"];
        $context = Slug::getBySlug($_POST["context"]);

        $results = [
            "amountRequested" => 0,
            "amountFinanced" => 0,
            "amountSpent" => 0,
            "amountRemaining" => 0,
        ];

        if (!empty($formId))
        {
            $results["amountRequested"] = PHDB::aggregate(Form::ANSWER_COLLECTION, [
                [
                    '$match' => [
                        'form' => $formId
                    ]
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense'
                ],
                [
                    '$match' => [
                        "answers.aapStep1.depense.price" => ['$type' => "number"] // Filter out non-numeric "price" values
                    ]
                ],
                [
                    '$addFields' => [
                        "answers.aapStep1.depense.price" => [
                            '$cond' => [
                                'if' => ['$eq' => [['$type' => '$answers.aapStep1.depense.price'], "string"]],
                                'then' => ['$toInt' => '$answers.aapStep1.depense.price'],
                                'else' => '$answers.aapStep1.depense.price'
                            ]
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => null,
                        'totalDepense' => ['$sum' => '$answers.aapStep1.depense.price']
                    ]
                ]
            ]);
            $results["amountRequested"] = $results["amountRequested"]["result"][0]["totalDepense"];

            $results["amountFinanced"] = PHDB::aggregate(Form::ANSWER_COLLECTION, [
                [
                    '$match' => [
                        'form' => $formId
                    ]
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense'
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense.financer'
                ],
                [
                    '$addFields' => [
                        'answers.aapStep1.depense.financer.amount' => [
                            '$cond' => [
                                'if' => [
                                    '$eq' => [
                                        ['$type' => '$answers.aapStep1.depense.financer.amount'],
                                        'string'
                                    ]
                                ],
                                'then' => ['$toInt' => '$answers.aapStep1.depense.financer.amount'],
                                'else' => '$answers.aapStep1.depense.financer.amount'
                            ]
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => null,
                        'totalAmountFinanced' => ['$sum' => '$answers.aapStep1.depense.financer.amount']
                    ]
                ]
            ]);
            $results["amountFinanced"] = $results["amountFinanced"]["result"][0]["totalAmountFinanced"];

            $results["amountSpent"] = PHDB::aggregate(Form::ANSWER_COLLECTION, [
                [
                    '$match' => [
                        'form' => $formId
                    ]
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense'
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense.payement'
                ],
                [
                    '$addFields' => [
                        'answers.aapStep1.depense.payement.amount' => [
                            '$cond' => [
                                'if' => [
                                    '$eq' => [
                                        ['$type' => '$answers.aapStep1.depense.payement.amount'],
                                        'string'
                                    ]
                                ],
                                'then' => ['$toInt' => '$answers.aapStep1.depense.payement.amount'],
                                'else' => '$answers.aapStep1.depense.payement.amount'
                            ]
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => null,
                        'totalAmountSpent' => ['$sum' => '$answers.aapStep1.depense.payement.amount']
                    ]
                ]
            ]);
            $results["amountSpent"] = isset($results["amountSpent"]["result"][0]["totalAmountSpent"]) ? $results["amountSpent"]["result"][0]["totalAmountSpent"] : 0;

            $results["amountRemaining"] = ($results["amountRequested"] - $results["amountFinanced"]);
        }

        return  $results;
    }
    private function combineMetrics()
    {
        $results = [
            "projectMetrics" => $this->projectMetrics(),
            "actionMetrics" => $this->actionMetrics(),
            "proposalMetrics" => $this->proposalMetrics(),
            "financingMetrics" => $this->financingMetrics(),
        ];
        return $results;
    }
    /********************************************* CHARTS ****************************************************/
    private function taskPerPerson()
    {
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers, $projectIdFromContext);
        $status = ["todo", "running", "done"];
        $actions = [];
        foreach ($status as $vs) $actions[$vs] = [];

        if (!empty($projectId))
        {
            $actions["todo"] = PHDB::find(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array(
                            '$or' => [
                                array("links.contributors" => ['$exists' => true]),
                                array("links.contributors" => [array('$not' => ['$type' => 3])]),
                            ]
                        ),
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array(
                            '$or' => [
                                array("tracking" => ['$exists' => false]),
                                array("tracking" => false)
                            ]
                        ),
                    )
                )
            );
            $actions["running"] = PHDB::find(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array(
                            '$or' => [
                                array("links.contributors" => ['$exists' => true]),
                                array("links.contributors" => [array('$not' => ['$type' => 3])]),
                            ]
                        ),
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array("tracking" => true),
                    )
                )
            );
            $actions["done"] = PHDB::find(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array(
                            '$or' => [
                                array("links.contributors" => ['$exists' => true]),
                                array("links.contributors" => [array('$not' => ['$type' => 3])]),
                            ]
                        ),
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'done'),
                    )
                )
            );
        }

        $members = $this->getMembersContextIds($contexts);
        $membersCounter = array();

        foreach ($members as $kuser => $vuser)
        {
            foreach ($status as $vs)
            {
                $membersCounter[$vs][$kuser] = array("name" => $vuser["name"], "id" => $kuser, "actionValue" => 0, "subTaskValue" => 0);
            }
        }

        foreach ($status as $vs)
        {
            foreach ($actions[$vs] as $kact => $vact)
            {
                if (!empty($vact["links"]["contributors"]))
                {
                    foreach ($vact["links"]["contributors"] as $kuser => $vuser)
                    {
                        if (!empty($membersCounter[$vs][$kuser])) $membersCounter[$vs][$kuser]["actionValue"]++;
                    }
                }
                if (!empty($vact["tasks"]))
                {
                    foreach ($vact["tasks"] as $ktask => $vtask)
                    {
                        if (($vs === "todo" || $vs === "running") && empty($vtask["checked"]))
                        {
                            if (!empty($vtask["contributors"]))
                            {
                                foreach ($vtask["contributors"] as $kuser => $vuser)
                                {
                                    if (!empty($membersCounter[$vs][$kuser])) $membersCounter[$vs][$kuser]["subTaskValue"]++;
                                }
                            }
                        }
                        elseif ($vs === "done" && !empty($vtask["checked"]))
                        {
                            if (!empty($vtask["contributors"]))
                            {
                                foreach ($vtask["contributors"] as $kuser => $vuser)
                                {
                                    if (!empty($membersCounter[$vs][$kuser])) $membersCounter[$vs][$kuser]["subTaskValue"]++;
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($status as $vs)
        {
            usort($membersCounter[$vs], function ($a, $b)
            {
                return $b['actionValue'] - $a['actionValue'];
            });
        }
        return  $membersCounter;
    }

    private function taskPerProject()
    {
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_values(array_unique(array_merge($projectIdFromAnswers, $projectIdFromContext)));
        $projectId2 = array_map(function ($v)
        {
            return new MongoId($v);
        }, $projectId);
        $projects = PHDB::findAndSort(Project::COLLECTION, array('_id' => ['$in' => $projectId2]), array("name" => 1), 0, array("name"));

        $status = ["todo", "running", "done"];
        $projectLabels = [];
        foreach ($status as $vs) $projectLabels[$vs] = array();
        foreach ($status as $vs)
        {
            foreach ($projects as $key => $value)
            {
                $projectLabels[$vs][$key] = array("name" => substr($value["name"], 0, 30), "id" => $key, "actionValue" => 0, "subTaskValue" => 0);
            }
        }

        $actions = [];
        if (!empty($projectId))
        {
            $actions["todo"] = PHDB::find(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array(
                            '$or' => [
                                array("tracking" => ['$exists' => false]),
                                array("tracking" => false)
                            ]
                        ),
                    )
                )
            );
            $actions["running"] = PHDB::find(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array("tracking" => true),
                    )
                )
            );
            $actions["done"] = PHDB::find(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'done'),
                    )
                )
            );
        }
        foreach ($status as $vs)
        {
            if (isset($actions[$vs]))
                foreach ($actions[$vs] as $kact => $vact)
                {
                    if (empty($projectLabels[$vs][$vact["parentId"]]))
                        continue;
                    if (!empty($vact["parentId"]))
                        $projectLabels[$vs][$vact["parentId"]]["actionValue"]++;
                    if (!empty($vact["tasks"]))
                    {
                        foreach ($vact["tasks"] as $ktask => $vtask)
                        {
                            if (($vs === "todo" || $vs === "running") && empty($vtask["checked"]))
                            {
                                $projectLabels[$vs][$vact["parentId"]]["subTaskValue"]++;
                            }
                            elseif ($vs === "done" && !empty($vtask["checked"]))
                            {
                                $projectLabels[$vs][$vact["parentId"]]["subTaskValue"]++;
                            }
                        }
                    }
                }
        }
        foreach ($status as $vs)
        {
            usort($projectLabels[$vs], function ($a, $b)
            {
                return $b['actionValue'] - $a['actionValue'];
            });
        }
        return $projectLabels;
    }

    private function detailKanbanPerPerson()
    {

        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $personneId = $_POST["personneId"];

        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge(/*$projectIdFromAnswers,*/$projectIdFromContext);


        $listpProjectUserParticipated = [];
        if (!empty($projectId))
        {
            $listpProjectUserParticipated = PHDB::find(
                Action::COLLECTION,
                array(
                    '$and' => array(
                        array(
                            '$or' => [
                                array("links.contributors." . $personneId => array('$exists' => true)),
                                array("creator" => $personneId),
                            ]
                        ),
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array("tracking" => true),
                    )
                ),
                array("parentId")
            );
        }

        $list = array();
        foreach ($listpProjectUserParticipated as $key => $value)
        {
            array_push($list, $value["parentId"]);
        }
        $list = array_values(array_unique($list));


        $liste = PHDB::find(
            Form::ANSWER_COLLECTION,
            array(
                "project.id" => array('$in' => $list)
            ),
            array("project.id")
        );

        $list = array();
        foreach ($liste  as $key => $value)
        {
            array_push($list, $value["project"]["id"]);
        }

        $list = array_values(array_unique($list));
        return array_values(array_unique($list));
    }

    private function testProjetWithFinancement()
    {
        $projectId = $_POST['projectId'];

        return ["nombre" => PHDB::count(
            Form::ANSWER_COLLECTION,
            array(
                "project.id" => array('$in' => array($projectId)),

            ),
            array("project.id")
        )];
    }

    private function  actionPerPersonByYear()
    {
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $projectId = $this->getProjectId($formId, $contexts);

        if (!empty($projectId))
        {
            $query = array(
                '$and' => array(
                    array("created" => ['$exists' => true]),
                    array("parentId" => ['$in' => $projectId]),
                )
            );

            $years = $this->getCreatedYears(Action::COLLECTION, $query);

            $creators = PHDB::distinct(Action::COLLECTION, "idUserAuthor", $query);
            $creators2 = PHDB::distinct(Action::COLLECTION, "creator", $query);
            $creators = array_values(array_unique(array_merge($creators, $creators2)));
            $creators = array_map(function ($val)
            {
                return new MongoId($val);
            }, $creators);

            $members = PHDB::find(Citoyen::COLLECTION, array("_id" => ['$in' => $creators]), array("name"));
            $membersData = array();

            foreach ($members as $kmember => $vmember)
            {
                $membersData[$kmember] = array(
                    "label" => /*$vmember["name"]*/ $kmember,
                    "name" => $vmember["name"],
                    "backgroundColor" => sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255)),
                    "data" => $years,
                    "stack" => 'Stack 0',
                    "_id" => $kmember
                );
            }

            $query['created'] = ['$gte' => mktime(0, 0, 0, 0, 0, array_keys($years)[0])];
            $actions = PHDB::find(Action::COLLECTION, $query);
            foreach ($actions as $kact => $vact)
            {
                if (!empty($vact["idUserAuthor"]))
                {
                    $creator = $vact["idUserAuthor"];
                    $createdYear = date('Y', $vact["created"]);
                    $membersData[$creator]["data"][$createdYear]++;
                }
                elseif (!empty($vact["creator"]))
                {
                    $creator = $vact["creator"];
                    $createdYear = date('Y', $vact["created"]);
                    $membersData[$creator]["data"][$createdYear]++;
                }
            }

            foreach ($membersData as $key => $value)
                $membersData[$key]["data"] = array_values($value["data"]);

            $results = [
                "years" => array_keys($years),
                "membersData" => $membersData
            ];
            return $results;
        }
    }
    private function financingFlows()
    {
        $formId = $_POST["formId"];
        $year =  $_POST["year"] ?? null;
        $context = Slug::getBySlug($_POST["context"]);

        $answersAndFinancers = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => [
                    "form" => $formId,
                    "answers.aapStep1.depense.financer.name" => ['$exists' => true],
                    "answers.aapStep1.depense.financer.amount" => ['$exists' => true],
                    "answers.aapStep1.titre" => ['$exists' => true]
                ]
            ],
            [
                '$unwind' => '$answers.aapStep1.depense'
            ],
            [
                '$unwind' => '$answers.aapStep1.depense.financer'
            ],
            [
                '$project' => [
                    //_id => 0, Exclure le champ "_id" du résultat si nécessaire
                    'name' => '$answers.aapStep1.depense.financer.name',
                    'id' => '$answers.aapStep1.depense.financer.id',
                    'amount' => '$answers.aapStep1.depense.financer.amount',
                    'proposal' => '$answers.aapStep1.titre',
                ]
            ]
        ]);

        $answersAndPayement = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => [
                    "form" => $formId,
                    "answers.aapStep1.depense.payement" => ['$exists' => true],
                    "answers.aapStep1.titre" => ['$exists' => true]
                ]
            ],
            [
                '$unwind' => '$answers.aapStep1.depense'
            ],
            [
                '$unwind' => '$answers.aapStep1.depense.payement'
            ],
            [
                '$project' => [
                    //_id => 0, Exclure le champ "_id" du résultat si nécessaire
                    'name' => '$answers.aapStep1.depense.payement.beneficiary.name',
                    'id' => '$answers.aapStep1.depense.payement.beneficiary.id',
                    'amount' => '$answers.aapStep1.depense.payement.amount',
                    'proposal' => '$answers.aapStep1.titre',
                ]
            ]
        ]);

        $results = [
            "colors" => array(),
            "labels" => array(),
            "flows" => array()
        ];

        if (!empty($answersAndFinancers["result"]))
        {
            foreach ($answersAndFinancers["result"] as $key => $val)
            {
                if (!empty($val["name"]) && $val["proposal"] && $val["amount"])
                {
                    $val["name"] = ucfirst($val["name"]) . " (FINANCEUR)";
                    $val["proposal"] = ucfirst($val["proposal"]) . " (PROPOSITION)";

                    $index = $val["name"] . "---" . $val["proposal"];
                    if (!isset($results["flows"][$index]["flow"]))
                    {
                        $results["flows"][$index] = array('from' => $val["name"], 'to' => $val["proposal"], 'flow' => (int)$val["amount"]);
                    }
                    else
                    {
                        $results["flows"][$index]["flow"] = $results["flows"][$index]["flow"] + (int)$val["amount"];
                    }

                    if (!in_array($val["name"], $results["labels"]))
                    {
                        $results["labels"][] = $val["name"];
                        $results["colors"][$val["name"]] = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
                    }

                    if (!in_array($val["proposal"], $results["labels"]))
                    {
                        $results["labels"][] = $val["proposal"];
                        $results["colors"][$val["proposal"]] = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
                    }
                }
            }
        }

        if (!empty($answersAndPayement["result"]))
        {
            foreach ($answersAndPayement["result"] as $key => $val)
            {
                if (!empty($val["name"]) && $val["proposal"] && $val["amount"])
                {
                    $val["name"] = ucfirst($val["name"]) . " (EXECUTANT)";
                    $val["proposal"] = ucfirst($val["proposal"]) . " (PROPOSITION)";

                    $index = $val["proposal"] . "---" . $val["name"];
                    if (!isset($results["flows"][$index]["flow"]))
                    {
                        $results["flows"][$index] = array('from' => $val["proposal"], 'to' => $val["name"], 'flow' => (int)$val["amount"]);
                    }
                    else
                    {
                        $results["flows"][$index]["flow"] = $results["flows"][$index]["flow"] + (int)$val["amount"];
                    }

                    if (!in_array($val["name"], $results["labels"]))
                    {
                        $results["labels"][] = $val["name"];
                        $results["colors"][$val["name"]] = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
                    }

                    if (!in_array($val["proposal"], $results["labels"]))
                    {
                        $results["labels"][] = $val["proposal"];
                        $results["colors"][$val["proposal"]] = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
                    }
                }
            }
        }



        return $results;
    }
    private function expenditureFinancingPaymentHistory()
    {
        $formId = $_POST["formId"];
        $context = Slug::getBySlug($_POST["context"]);
        $key = $_POST["filter"]; // allTime : year, months, this week, today, given two date
        $query = array();
        $labels = array();
        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        $results = [
            "depense" => [],
            "financement" => [],
            "payment" => [],
        ];

        $depense = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => array(
                    'form' => $formId,
                    'answers.aapStep1.depense' => array('$exists' => true)
                )
            ],
            [
                '$unwind' => '$answers.aapStep1.depense'
            ],
            [
                '$project' => [
                    '_id' => 0,
                    'date' => '$answers.aapStep1.depense.date',
                    'amount' => '$answers.aapStep1.depense.price'
                ]
            ]
        ]);

        $financer = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => array(
                    'form' => $formId,
                    'answers.aapStep1.depense.financer' => array('$exists' => true)
                )
            ],
            [
                '$unwind' => '$answers.aapStep1.depense'
            ],
            [
                '$unwind' => '$answers.aapStep1.depense.financer'
            ],
            [
                '$project' => [
                    '_id' => 0,
                    'date' => '$answers.aapStep1.depense.financer.date',
                    'amount' => '$answers.aapStep1.depense.financer.amount'
                ]
            ]
        ]);

        $payement = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => array(
                    'form' => $formId,
                    'answers.aapStep1.depense.payement' => array('$exists' => true)
                )
            ],
            [
                '$unwind' => '$answers.aapStep1.depense'
            ],
            [
                '$unwind' => '$answers.aapStep1.depense.payement'
            ],
            [
                '$project' => [
                    '_id' => 0,
                    'date' => '$answers.aapStep1.depense.payement.date',
                    'amount' => '$answers.aapStep1.depense.payement.amount'
                ]
            ]
        ]);

        if ($key == "year")
        {
            $labels = $this->getCreatedYears(Form::ANSWER_COLLECTION, $query);
            $query = array(
                "form" => $formId,
                "answers.aapStep1.titre" => ['$exists' => true],
                '$or' => [
                    array("answers.aapStep1.depense" => ['$exists' => true]),
                    array("answers.aapStep1.depense.financer" => ['$exists' => true]),
                    array("answers.aapStep1.depense.payment" => ['$exists' => true]),
                ]
            );
            $labels = $this->getCreatedYears(Form::ANSWER_COLLECTION, $query);
            $results = [
                "depense" => $labels,
                "financement" => $labels,
                "payment" => $labels,
            ];

            foreach ($depense["result"] as $k => $v)
            {
                if (!empty($v["date"]) && !empty($v["amount"]))
                {
                    if (!empty($v["date"]->sec))
                    {
                        $y = date('Y', $v["date"]->sec);
                    }
                    elseif (is_string($v["date"]))
                    {
                        $y = DateTime::createFromFormat('d/m/Y', $v["date"])->format('Y');
                    }
                    $results["depense"][$y] += (int)$v["amount"];
                }
            }

            foreach ($financer["result"] as $k => $v)
            {
                if (!empty($v["date"]) && !empty($v["amount"]))
                {
                    if (!empty($v["date"]->sec))
                    {
                        $y = date('Y', $v["date"]->sec);
                    }
                    elseif (is_string($v["date"]))
                    {
                        $y = DateTime::createFromFormat('d/m/Y', $v["date"])->format('Y');
                    }
                    $results["financement"][$y] += (int)$v["amount"];
                }
            }

            foreach ($payement["result"] as $k => $v)
            {
                if (!empty($v["date"]) && !empty($v["amount"]))
                {
                    if (!empty($v["date"]->sec))
                    {
                        $y = date('Y', $v["date"]->sec);
                    }
                    elseif (is_string($v["date"]))
                    {
                        $y = DateTime::createFromFormat('d/m/Y', $v["date"])->format('Y');
                    }
                    $results["payment"][$y] += (int)$v["amount"];
                }
            }
        }

        if ($key == "all")
        {
            $depenseDate = PHDB::distinct(Form::ANSWER_COLLECTION, "answers.aapStep1.depense.date", array(
                'form' => $formId,
                'answers.aapStep1.depense' => array('$exists' => true)
            ));
            $financerDate = PHDB::distinct(Form::ANSWER_COLLECTION, "answers.aapStep1.depense.financer.date", array(
                'form' => $formId,
                'answers.aapStep1.depense.financer' => array('$exists' => true)
            ));
            $payementDate = PHDB::distinct(Form::ANSWER_COLLECTION, "answers.aapStep1.depense.payement.date", array(
                'form' => $formId,
                'answers.aapStep1.depense.payement' => array('$exists' => true)
            ));
            $labels = array_merge($depenseDate, $financerDate, $payementDate);
            $labels2 =  array();
            foreach ($labels as $k => $v)
            {
                if (!empty($v->sec))
                {
                    $labels[$k] = date('d/m/Y', (int)$v->sec);
                }
                elseif (empty($v) || (!empty($v->sec) && $v->sec == 0))
                {
                    unset($labels[$k]);
                }
                elseif (is_string($v))
                {
                    $labels[$k] = DateTime::createFromFormat('d/m/Y', $v)->format('d/m/Y');
                }
                else
                {
                    unset($labels[$k]);
                }
            }
            $labels = array_values($labels);
            usort($labels, function ($a, $b)
            {
                $dateA = DateTime::createFromFormat('d/m/Y', $a);
                $dateB = DateTime::createFromFormat('d/m/Y', $b);
                if ($dateA === false || $dateB === false)
                {
                    throw new Exception("Invalid date format");
                }
                return $dateA <=> $dateB;
            });
            $labels2 = array();
            foreach ($labels as $kl => $vl)
            {
                $labels2[$vl] = 0;
            }
            $labels = $labels2;
            unset($labels2);

            $results = [
                "depense" => $labels,
                "financement" => $labels,
                "payment" => $labels,
            ];

            foreach ($depense["result"] as $k => $v)
            {
                if (!empty($v["date"]) && !empty($v["amount"]))
                {
                    if (!empty($v["date"]->sec))
                    {
                        $d = date('d/m/Y', $v["date"]->sec);
                    }
                    elseif (is_string($v["date"]))
                    {
                        $d = DateTime::createFromFormat('d/m/Y', $v["date"])->format('d/m/Y');
                    }
                    $results["depense"][$d] += (int)$v["amount"];
                }
            }

            foreach ($financer["result"] as $k => $v)
            {
                if (!empty($v["date"]) && !empty($v["amount"]))
                {
                    if (!empty($v["date"]->sec))
                    {
                        $d = date('d/m/Y', $v["date"]->sec);
                    }
                    elseif (is_string($v["date"]))
                    {
                        $d = DateTime::createFromFormat('d/m/Y', $v["date"])->format('d/m/Y');
                    }
                    $results["financement"][$d] += (int)$v["amount"];
                }
            }

            foreach ($payement["result"] as $k => $v)
            {
                if (!empty($v["date"]) && !empty($v["amount"]))
                {
                    if (!empty($v["date"]->sec))
                    {
                        $d = date('d/m/Y', $v["date"]->sec);
                    }
                    elseif (is_string($v["date"]))
                    {
                        $d = DateTime::createFromFormat('d/m/Y', $v["date"])->format('d/m/Y');
                    }
                    $results["payment"][$d] += (int)$v["amount"];
                }
            }
        }
        return $results;
    }

    private function weekActionActivity($timestamp = null)
    {
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $id_user = $_POST["id_user"];
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers, $projectIdFromContext);

        $currentDate = date('Y-m-d');
        if (!empty($timestamp))
            $currentDate = date('Y-m-d', $timestamp);

        $firstDayOfWeek = date('Y-m-d', strtotime('last monday', strtotime($currentDate)));
        $lastDayOfWeek = date('Y-m-d', strtotime('next sunday', strtotime($currentDate)));
        $startWeeksTimestamp = strtotime($firstDayOfWeek . ' 00:00:00');
        $endWeeksTimestamp = strtotime($lastDayOfWeek . ' 23:59:59');

        $startWeeksDateTime = (new DateTime("@$startWeeksTimestamp"))->format(DateTime::ATOM);
        $endWeeksDateTime = (new DateTime("@$endWeeksTimestamp"))->format(DateTime::ATOM);

        $weekDaysTimestamp = $this->timestampOfEachDayOnCurrentWeek(strtotime($currentDate));
        $weekDaysLabels = [
            "todo" => [],
            "running" => [],
            "done" => [],
        ];
        foreach ($weekDaysTimestamp as $key => $value)
        {
            $weekDaysLabels["todo"][$key] = 0;
            $weekDaysLabels["running"][$key] = 0;
            $weekDaysLabels["done"][$key] = 0;
        }

        $todoActionNoTasks = [];
        $todoActionWithTasks = [];

        $runningActionNoTasks = [];
        $runningActionWithTasks = [];

        $doneActionNoTasks = [];
        $doneActionWithTasks = [];

        if (!empty($projectId))
        {
            $array_todoActionNoTasks = array(
                '$and' => array(
                    array(
                        '$or' => [
                            array("tasks" => ['$exists' => false]),
                            array("tasks" => ['$size' => 0])
                        ]
                    ),

                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array(
                        '$or' => [
                            array("tracking" => ['$exists' => false]),
                            array("tracking" => false)
                        ]
                    ),
                    array(
                        '$or' => [
                            array(
                                "startDate" => array(
                                    '$gte' => new MongoDate($startWeeksTimestamp),
                                    '$lte' => new MongoDate($endWeeksTimestamp)
                                )
                            ),
                            array(
                                "created" => array(
                                    '$gte' => $startWeeksTimestamp,
                                    '$lte' => $endWeeksTimestamp
                                )
                            )
                        ]
                    )
                )
            );
            $array_todoActionWithTasks = array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array(
                        '$or' => [
                            array("tracking" => ['$exists' => false]),
                            array("tracking" => false)
                        ]
                    ),
                    array(
                        "tasks.createdAt" => array(
                            '$gte' => new MongoDate($startWeeksTimestamp),
                            '$lte' => new MongoDate($endWeeksTimestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );
            $array_runningActionNoTasks = array(
                '$and' => array(
                    array(
                        '$or' => [
                            array("tasks" => ['$exists' => false]),
                            array("tasks" => ['$size' => 0])
                        ]
                    ),

                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array("tracking" => true),
                    array(
                        '$or' => [
                            array(
                                "startDate" => array(
                                    '$gte' => new MongoDate($startWeeksTimestamp),
                                    '$lte' => new MongoDate($endWeeksTimestamp)
                                )
                            ),
                            array(
                                "created" => array(
                                    '$gte' => $startWeeksTimestamp,
                                    '$lte' => $endWeeksTimestamp
                                )
                            )
                        ]
                    )
                )
            );
            $array_runningActionWithTasks =  array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array("tracking" => true),
                    array(
                        "tasks.createdAt" => array(
                            '$gte' => new MongoDate($startWeeksTimestamp),
                            '$lte' => new MongoDate($endWeeksTimestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );

            $array_doneActionNoTasks = array(
                '$and' => array(
                    array(
                        '$or' => [
                            array("tasks" => ['$exists' => false]),
                            array("tasks" => ['$size' => 0])
                        ]
                    ),

                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'done'),
                    array(
                        '$or' => [
                            array(
                                "startDate" => array(
                                    '$gte' => new MongoDate($startWeeksTimestamp),
                                    '$lte' => new MongoDate($endWeeksTimestamp)
                                )
                            ),
                            array(
                                "created" => array(
                                    '$gte' => $startWeeksTimestamp,
                                    '$lte' => $endWeeksTimestamp
                                )
                            )
                        ]
                    )
                )
            );
            $array_doneActionWithTasks = array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'done'),
                    array(
                        "tasks.createdAt" => array(
                            '$gte' => new MongoDate($startWeeksTimestamp),
                            '$lte' => new MongoDate($endWeeksTimestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );
            if ($id_user != null)
            {
                $array_contributor = ["links.contributors." . $id_user =>  ['$exists' => true]];
                array_push($array_todoActionNoTasks['$and'], $array_contributor);
                array_push($array_todoActionWithTasks['$and'], $array_contributor);
                array_push($array_runningActionNoTasks['$and'], $array_contributor);
                array_push($array_runningActionWithTasks['$and'], $array_contributor);
                array_push($array_doneActionNoTasks['$and'], $array_contributor);
                array_push($array_doneActionWithTasks['$and'], $array_contributor);
            }
            $todoActionNoTasks = PHDB::find(Action::COLLECTION, $array_todoActionNoTasks);
            $todoActionWithTasks = PHDB::find(Action::COLLECTION, $array_todoActionWithTasks);
            $runningActionNoTasks = PHDB::find(Action::COLLECTION, $array_runningActionNoTasks);
            $runningActionWithTasks = PHDB::find(Action::COLLECTION, $array_runningActionWithTasks);
            $doneActionNoTasks = PHDB::find(Action::COLLECTION, $array_doneActionNoTasks);
            $doneActionWithTasks = PHDB::find(Action::COLLECTION, $array_doneActionWithTasks);
        }

        foreach ($todoActionNoTasks as $kact => $vact)
        {
            foreach ($weekDaysTimestamp as $day => $vday)
            {
                if (!empty($vact['startDate']->sec) &&  $this->isBetween($vact['startDate']->sec, $vday['begin'], $vday['end']))
                    $weekDaysLabels["todo"][$day]++;
                elseif (!empty($vact["created"]) && $this->isBetween($vact["created"], $vday['begin'], $vday['end']))
                    $weekDaysLabels["todo"][$day]++;
            }
        }

        foreach ($todoActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                foreach ($weekDaysTimestamp as $day => $vday)
                {
                    if (!empty($vtask['createdAt']) &&
                        $vtask['createdAt'] instanceof MongoDate && 
                        $this->isBetween($vact['created'], $vday['begin'], $vday['end']))
                        $weekDaysLabels["todo"][$day]++;
                }
            }
        }

        foreach ($runningActionNoTasks as $kact => $vact)
        {
            foreach ($weekDaysTimestamp as $day => $vday)
            {
                if (!empty($vact['startDate']->sec) &&  $this->isBetween($vact['startDate']->sec, $vday['begin'], $vday['end']))
                    $weekDaysLabels["running"][$day]++;
                elseif (!empty($vact["created"]) && $this->isBetween($vact["created"], $vday['begin'], $vday['end']))
                    $weekDaysLabels["running"][$day]++;
            }
        }

        foreach ($runningActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                foreach ($weekDaysTimestamp as $day => $vday)
                {
                    if (!empty($vtask['createdAt']->sec) &&  $this->isBetween($vtask['createdAt']->sec, $vday['begin'], $vday['end']))
                        $weekDaysLabels["running"][$day]++;
                }
            }
        }

        foreach ($doneActionNoTasks as $kact => $vact)
        {
            foreach ($weekDaysTimestamp as $day => $vday)
            {
                /*if(!empty($vact['startDate']->sec) &&  $this->isBetween($vact['startDate']->sec,$startWeeksTimestamp,$vday['end'])){
                    $weekDaysLabels["done"][$day]++;
                }else*/
                if (!empty($vact["updated"]) && $this->isBetween($vact["updated"], $vday['begin'], $vday['end']))
                {
                    $weekDaysLabels["done"][$day]++;
                }
            }
        }

        foreach ($doneActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                foreach ($weekDaysTimestamp as $day => $vday)
                {
                    if (!empty($vtask['checkedAt']->sec) &&  $this->isBetween($vtask['checkedAt']->sec, $vday['begin'], $vday['end']))
                    {
                        $weekDaysLabels["done"][$day]++;
                    }
                }
            }
        }

        return  $weekDaysLabels;
    }

    private function oneDateActivitylist()
    {
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $thedate = $_POST["date"];
        $status = $_POST['status'];
        $id_user = $_POST['id_user'];
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers, $projectIdFromContext);
        $datetoTimestamp = $this->getDayStartAndEndTimestamps($thedate);
        $date1Timestamp = $datetoTimestamp['start'];
        $date2Timestamp = $datetoTimestamp['end'];

        $query = array(
            "parentId" => ['$in' => $projectId],
            '$or' => [
                array(
                    "startDate" => array(
                        '$gte' => new MongoDate($date1Timestamp),
                        '$lte' => new MongoDate($date2Timestamp)
                    )
                ),
                array(
                    "createdAt" => array(
                        '$gte' => new MongoDate($date1Timestamp),
                        '$lte' => new MongoDate($date2Timestamp)
                    )
                ),
                array(
                    "created" => array(
                        '$gte' => $date1Timestamp,
                        '$lte' => $date2Timestamp
                    )
                )
            ]
        );

        $days = PHDB::distinct(Action::COLLECTION, "startDate", $query);
        $days2 = PHDB::distinct(Action::COLLECTION, "created", $query);
        $days3 = PHDB::distinct(Action::COLLECTION, "createdAt", $query);
        $days = array_values(array_unique(array_merge($days, $days2, $days3)));
        $days = array_map(function ($v)
        {
            if (!empty($v->sec))
                return date("d-m-Y", $v->sec);
            elseif (is_int($v))
                return date("d-m-Y", $v);
        }, $days);

        $ActionNoTasks = [];
        $ActionWithTasks = [];


        if (!empty($projectId))
        {
            if ($id_user != null)
            {
                $array_contributor = ["links.contributors." . $id_user => ['$exists' => true]];
            }

            if ($status == "todo")
            {
                $Array_ActionNoTasks = array(
                    '$and' => array(
                        array(
                            '$or' => [
                                array("tasks" => ['$exists' => false]),
                                array("tasks" => ['$size' => 0])
                            ]
                        ),

                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array(
                            '$or' => [
                                array("tracking" => ['$exists' => false]),
                                array("tracking" => false)
                            ]
                        ),
                        array(
                            '$or' => [
                                array(
                                    "startDate" => array(
                                        '$gte' => new MongoDate($date1Timestamp),
                                        '$lte' => new MongoDate($date2Timestamp)
                                    )
                                ),
                                array(
                                    "created" => array(
                                        '$gte' => $date1Timestamp,
                                        '$lte' => $date2Timestamp
                                    )
                                )
                            ]
                        )
                    )
                );


                $Array_ActionWithTatsk = array(
                    '$and' => array(
                        array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array(
                            '$or' => [
                                array("tracking" => ['$exists' => false]),
                                array("tracking" => false)
                            ]
                        ),
                        array(
                            "tasks.createdAt" => array(
                                '$gte' => new MongoDate($date1Timestamp),
                                '$lte' => new MongoDate($date2Timestamp)
                            )
                        ),
                        array(
                            '$or' => [
                                array("tasks.checked" => ['$exists' => false]),
                                array("tasks.checked" => false)
                            ]
                        )
                    )
                );

                if ($id_user != null)
                {
                    array_push($Array_ActionNoTasks['$and'], $array_contributor);
                    array_push($Array_ActionWithTatsk['$and'], $array_contributor);
                }
                $ActionNoTasks = PHDB::find(Action::COLLECTION, $Array_ActionNoTasks, array("startDate", "name", "created", "creator", "links.contributors"));
                $ActionWithTasks = PHDB::find(Action::COLLECTION, $Array_ActionWithTatsk, array("startDate", "name", "created", "creator", "links.contributors"));
            }
            else if ($status == "running")
            {
                $Array_ActionNoTasks = array(
                    '$and' => array(
                        array(
                            '$or' => [
                                array("tasks" => ['$exists' => false]),
                                array("tasks" => ['$size' => 0])
                            ]
                        ),

                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array("tracking" => true),
                        array(
                            '$or' => [
                                array(
                                    "startDate" => array(
                                        '$gte' => new MongoDate($date1Timestamp),
                                        '$lte' => new MongoDate($date2Timestamp)
                                    )
                                ),
                                array(
                                    "created" => array(
                                        '$gte' => $date1Timestamp,
                                        '$lte' => $date2Timestamp
                                    )
                                )
                            ]
                        )
                    )
                );


                $Array_ActionWithTatsk =  array(
                    '$and' => array(
                        array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'todo'),
                        array("tracking" => true),
                        array(
                            "tasks.createdAt" => array(
                                '$gte' => new MongoDate($date1Timestamp),
                                '$lte' => new MongoDate($date2Timestamp)
                            )
                        ),
                        array(
                            '$or' => [
                                array("tasks.checked" => ['$exists' => false]),
                                array("tasks.checked" => false)
                            ]
                        )
                    )
                );
                if ($id_user != null)
                {
                    array_push($Array_ActionNoTasks['$and'], $array_contributor);
                    array_push($Array_ActionWithTatsk['$and'], $array_contributor);
                }
                $ActionNoTasks = PHDB::find(Action::COLLECTION, $Array_ActionNoTasks, array("startDate", "name", "created", "creator", "links.contributors"));
                $ActionWithTasks = PHDB::find(Action::COLLECTION, $Array_ActionWithTatsk, array("startDate", "name", "created", "creator", "links.contributors"));
            }
            else if ($status == "done")
            {
                $Array_ActionNoTasks = array(
                    '$and' => array(
                        array(
                            '$or' => [
                                array("tasks" => ['$exists' => false]),
                                array("tasks" => ['$size' => 0])
                            ]
                        ),

                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'done'),
                        array(
                            '$or' => [
                                array(
                                    "startDate" => array(
                                        '$gte' => new MongoDate($date1Timestamp),
                                        '$lte' => new MongoDate($date2Timestamp)
                                    )
                                ),
                                array(
                                    "created" => array(
                                        '$gte' => $date1Timestamp,
                                        '$lte' => $date2Timestamp
                                    )
                                )
                            ]
                        )
                    )
                );
                $Array_ActionWithTatsk = array(
                    '$and' => array(
                        array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => 'done'),
                        array(
                            "tasks.createdAt" => array(
                                '$gte' => new MongoDate($date1Timestamp),
                                '$lte' => new MongoDate($date2Timestamp)
                            )
                        ),
                        array(
                            '$or' => [
                                array("tasks.checked" => ['$exists' => false]),
                                array("tasks.checked" => false)
                            ]
                        )
                    )
                );
                if ($id_user != null)
                {
                    array_push($Array_ActionNoTasks['$and'], $array_contributor);
                    array_push($Array_ActionWithTatsk['$and'], $array_contributor);
                }
                $ActionNoTasks = PHDB::find(Action::COLLECTION, $Array_ActionNoTasks, array("startDate", "name", "created", "creator", "links.contributors"));
                $ActionWithTasks = PHDB::find(Action::COLLECTION, $Array_ActionWithTatsk, array("startDate", "name", "created", "creator", "links.contributors"),);
            }
        }


        $creators = array();
        $list_creator = array();

        $tasks = array_merge($ActionNoTasks, $ActionWithTasks);

        foreach ($tasks as $key => $value)
        {
            array_push($list_creator, $value["creator"]);
        }

        $list_creator = array_map(function ($val)
        {
            return new MongoId($val);
        }, array_values(array_unique($list_creator)));

        $members = PHDB::find(Citoyen::COLLECTION, array("_id" => ['$in' => $list_creator]), array("name"));

        $creators  = array_map(function ($val)
        {

            if (!empty($val['startDate']->sec))
            {
                $d = $val['startDate']->sec;
            }
            elseif (!empty($val["created"]))
            {
                $d = $val["created"];
            }


            return array(
                "id" => $val["_id"],
                "name" => $val["name"],
                "creator" => $val["creator"],
                "created" => $d
            );
        }, $tasks);

        $tasks = array();

        foreach ($creators as $key => $value)
        {
            $creator = array(
                "id" => $key,
                "name" => $value["name"],
                "creator" => $value["creator"],
                "created" => $value["created"],
                "name_creator" => $members[$value["creator"]]["name"],
            );
            if (date("d-m-Y", $value["created"]) == $thedate)
            {
                array_push($tasks, $creator);
            }
        }

        return array_values($tasks);
    }

    private function betweenTwoDateActivity()
    {
        $id_user = $_POST['id_user'];
        $date1 = $_POST["date1"] ?? date('Y-m-01');
        $date2 = $_POST["date2"] ?? date('Y-m-t');

        if (!empty($_POST["filter"]))
        {
            if ($_POST["filter"] === "thismonth")
            {
                $date1 = date('Y-m-01');
                $date2 = date('Y-m-t');
            }
        }

        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers, $projectIdFromContext);

        $date1Timestamp = strtotime($date1);
        $date2Timestamp = strtotime($date2);

        if ($date1Timestamp > $date2Timestamp)
        {
            $temp =  $date1Timestamp;
            $date1Timestamp = $date2Timestamp;
            $date2Timestamp = $temp;
        }
        $commonQuery = array(
            "parentId" => ['$in' => $projectId],
            '$or' => [
                array(
                    "startDate" => array(
                        '$gte' => new MongoDate($date1Timestamp),
                        '$lte' => new MongoDate($date2Timestamp)
                    )
                ),
                array(
                    "createdAt" => array(
                        '$gte' => new MongoDate($date1Timestamp),
                        '$lte' => new MongoDate($date2Timestamp)
                    )
                ),
                array(
                    "created" => array(
                        '$gte' => $date1Timestamp,
                        '$lte' => $date2Timestamp
                    )
                )
            ]
        );
        $days = PHDB::distinct(Action::COLLECTION, "startDate", $commonQuery);
        $days2 = PHDB::distinct(Action::COLLECTION, "created", $commonQuery);
        $days3 = PHDB::distinct(Action::COLLECTION, "createdAt", $commonQuery);
        $days = array_values(array_unique(array_merge($days, $days2, $days3)));

        $days = array_map(function ($v)
        {
            if (!empty($v->sec))
                return date("d-m-Y", $v->sec);
            elseif (is_int($v))
                return date("d-m-Y", $v);
        }, $days);
        //return $days;
        $days = array_values(array_unique($days));
        $daysLabel = [
            "todo" => [],
            "running" => [],
            "done" => [],
        ];
        foreach ($days as $key => $value)
        {
            $daysLabel["todo"][$value] = 0;
            $daysLabel["running"][$value] = 0;
            $daysLabel["done"][$value] = 0;
        }

        $todoActionNoTasks = [];
        $todoActionWithTasks = [];

        $runningActionNoTasks = [];
        $runningActionWithTasks = [];

        $doneActionNoTasks = [];
        $doneActionWithTasks = [];

        if (!empty($projectId))
        {

            $array_todoActionNoTasks = array(
                '$and' => array(
                    array(
                        '$or' => [
                            array("tasks" => ['$exists' => false]),
                            array("tasks" => ['$size' => 0])
                        ]
                    ),

                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array(
                        '$or' => [
                            array("tracking" => ['$exists' => false]),
                            array("tracking" => false)
                        ]
                    ),
                    array(
                        '$or' => [
                            array(
                                "startDate" => array(
                                    '$gte' => new MongoDate($date1Timestamp),
                                    '$lte' => new MongoDate($date2Timestamp)
                                )
                            ),
                            array(
                                "created" => array(
                                    '$gte' => $date1Timestamp,
                                    '$lte' => $date2Timestamp
                                )
                            )
                        ]
                    )
                )
            );
            $array_todoActionWithTasks = array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array(
                        '$or' => [
                            array("tracking" => ['$exists' => false]),
                            array("tracking" => false)
                        ]
                    ),
                    array(
                        "tasks.createdAt" => array(
                            '$gte' => new MongoDate($date1Timestamp),
                            '$lte' => new MongoDate($date2Timestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );
            $array_runningActionNoTasks =  array(
                '$and' => array(
                    array(
                        '$or' => [
                            array("tasks" => ['$exists' => false]),
                            array("tasks" => ['$size' => 0])
                        ]
                    ),

                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array("tracking" => true),
                    array(
                        '$or' => [
                            array(
                                "startDate" => array(
                                    '$gte' => new MongoDate($date1Timestamp),
                                    '$lte' => new MongoDate($date2Timestamp)
                                )
                            ),
                            array(
                                "created" => array(
                                    '$gte' => $date1Timestamp,
                                    '$lte' => $date2Timestamp
                                )
                            )
                        ]
                    )
                )
            );
            $array_runningActionWithTasks = array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array("tracking" => true),
                    array(
                        "tasks.createdAt" => array(
                            '$gte' => new MongoDate($date1Timestamp),
                            '$lte' => new MongoDate($date2Timestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );
            $array_doneActionNoTasks = array(
                '$and' => array(
                    array(
                        '$or' => [
                            array("tasks" => ['$exists' => false]),
                            array("tasks" => ['$size' => 0])
                        ]
                    ),

                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'done'),
                    array(
                        '$or' => [
                            array(
                                "startDate" => array(
                                    '$gte' => new MongoDate($date1Timestamp),
                                    '$lte' => new MongoDate($date2Timestamp)
                                )
                            ),
                            array(
                                "created" => array(
                                    '$gte' => $date1Timestamp,
                                    '$lte' => $date2Timestamp
                                )
                            )
                        ]
                    )
                )
            );

            $array_doneActionWithTasks = array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'done'),
                    array(
                        "tasks.createdAt" => array(
                            '$gte' => new MongoDate($date1Timestamp),
                            '$lte' => new MongoDate($date2Timestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );

            if ($id_user != null)
            {
                $array_contributor = ["links.contributors." . $id_user => ['$exists' => true]];
                array_push($array_todoActionNoTasks['$and'], $array_contributor);
                array_push($array_todoActionWithTasks['$and'], $array_contributor);
                array_push($array_runningActionNoTasks['$and'], $array_contributor);
                array_push($array_runningActionWithTasks['$and'], $array_contributor);
                array_push($array_doneActionNoTasks['$and'], $array_contributor);
                array_push($array_doneActionWithTasks['$and'], $array_contributor);
            }

            $todoActionNoTasks = PHDB::find(Action::COLLECTION, $array_todoActionNoTasks);
            $todoActionWithTasks = PHDB::find(Action::COLLECTION, $array_todoActionWithTasks);
            $runningActionNoTasks = PHDB::find(Action::COLLECTION, $array_runningActionNoTasks);
            $runningActionWithTasks = PHDB::find(Action::COLLECTION, $array_runningActionWithTasks);
            $doneActionNoTasks = PHDB::find(Action::COLLECTION, $array_doneActionNoTasks);
            $doneActionWithTasks = PHDB::find(Action::COLLECTION, $array_doneActionWithTasks);
        }

        foreach ($todoActionNoTasks as $kact => $vact)
        {
            if (!empty($vact['startDate']->sec))
            {
                $d = date("d-m-Y", $vact['startDate']->sec);
                $daysLabel["todo"][$d]++;
            }
            elseif (!empty($vact["created"]))
            {
                $d = date("d-m-Y", $vact["created"]);
                $daysLabel["todo"][$d]++;
            }
        }

        foreach ($todoActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                if (!empty($vact['createdAt']->sec))
                {
                    $d = date("d-m-Y", $vact['createdAt']->sec);
                    $daysLabel["todo"][$d]++;
                }
            }
        }

        foreach ($runningActionNoTasks as $kact => $vact)
        {
            if (!empty($vact['startDate']->sec))
            {
                $d = date("d-m-Y", $vact['startDate']->sec);
                $daysLabel["running"][$d]++;
            }
            elseif (!empty($vact["created"]))
            {
                $d = date("d-m-Y", $vact["created"]);
                $daysLabel["running"][$d]++;
            }
        }

        foreach ($runningActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                if (!empty($vact['createdAt']->sec))
                {
                    $d = date("d-m-Y", $vact['createdAt']->sec);
                    $daysLabel["running"][$d]++;
                }
            }
        }

        foreach ($doneActionNoTasks as $kact => $vact)
        {
            if (!empty($vact['startDate']->sec))
            {
                $d = date("d-m-Y", $vact['startDate']->sec);
                $daysLabel["done"][$d]++;
            }
            elseif (!empty($vact["created"]))
            {
                $d = date("d-m-Y", $vact["created"]);
                $daysLabel["done"][$d]++;
            }
        }

        foreach ($doneActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                if (!empty($vact['createdAt']->sec))
                {
                    $d = date("d-m-Y", $vact['createdAt']->sec);
                    $daysLabel["done"][$d]++;
                }
            }
        }

        foreach ($daysLabel["done"] as $k => $v)
        {
            $date = date("Y-m-d", strtotime($k));
            $daysLabel['calendrier'][$date] = array(
                "x" => $date,
                "y" => date("N", strtotime($k)),
                "d" => $date,
                "v" => $v
            );
        }
        return $daysLabel;
    }


    private function oneDayActionActivity()
    {
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $filter = $_POST["filter"];
        $currentTimezone = new DateTimeZone($this->getTimezone());
        $currentDate = new DateTime("now", $currentTimezone);
        if ($filter == "yesterday")
        {
            $currentDate->modify('-1 day');
        }
        $currentDate = $currentDate->format('Y/m/d');

        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers, $projectIdFromContext);

        $hours = [];
        $hoursStart = [];
        $hoursStart[] = sprintf("%02d:%02d", 00, 00);
        for ($hour = 0; $hour < 24; $hour++)
        {
            for ($minute = 0; $minute < 60; $minute += 30)
            {
                if (isset($hours[count($hours) - 1]))
                    $hoursStart[] = $hours[count($hours) - 1];
                $hours[] = sprintf("%02d:%02d", $hour, $minute);
            }
        }
        $hours[] = sprintf("%02d:%02d", 23, 59);
        $hoursStart[] = sprintf("%02d:%02d", 23, 30);

        $hours2 = [];
        $hoursSt = [];
        foreach ($hours as $key => $value)
        {
            $hours2[$value] = (new DateTime($currentDate . $value, $currentTimezone))->getTimestamp();
            if (isset($hoursStart[$key]))
                $hoursSt[] = (new DateTime($currentDate . $hoursStart[$key], $currentTimezone))->getTimestamp();
        }
        $hours = $hours2;
        unset($hours2);
        $hoursStart = $hoursSt;
        unset($hoursSt);

        $beginEndTimestamp = $this->timestampOfDay($currentDate);
        $startTimestamp = $beginEndTimestamp["startTimestamp"];
        $endTimestamp = $beginEndTimestamp["endTimestamp"];

        $daysLabels = [
            "todo" => [],
            "running" => [],
            "done" => [],
        ];

        foreach ($hours as $label => $tmsp)
        {
            $daysLabels["todo"][$tmsp] = array("label" => $label, "value" => 0);
            $daysLabels["running"][$tmsp] = array("label" => $label, "value" => 0);
            $daysLabels["done"][$tmsp] = array("label" => $label, "value" => 0);
        }

        $todoActionNoTasks = [];
        $todoActionWithTasks = [];

        $runningActionNoTasks = [];
        $runningActionWithTasks = [];

        $doneActionNoTasks = [];
        $doneActionWithTasks = [];
        $id_user = $_POST['id_user'];
        if (!empty($projectId))
        {
            $array_todoActionNoTasks = [
                '$and' => [
                    [
                        '$or' => [
                            ["tasks" => ['$exists' => false]],
                            ["tasks" => ['$size' => 0]]
                        ]
                    ],
                    ["parentId" => ['$in' => $projectId]],
                    ["status" => 'todo'],
                    [
                        '$or' => [
                            ["tracking" => ['$exists' => false]],
                            ["tracking" => false]
                        ]
                    ],
                    [
                        '$or' => [

                            [
                                "startDate" =>
                                [
                                    '$gte' => new MongoDate($startTimestamp),
                                    '$lte' => new MongoDate($endTimestamp)
                                ]
                            ],
                            [
                                "created" =>
                                [
                                    '$gte' => $startTimestamp,
                                    '$lte' => $endTimestamp
                                ]
                            ]

                        ]
                    ]
                ]
            ];
            $array_todoActionWithTasks =  array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array(
                        '$or' => [
                            array("tracking" => ['$exists' => false]),
                            array("tracking" => false)
                        ]
                    ),
                    array(
                        "tasks.createdAt" => array(
                            '$gte' => new MongoDate($startTimestamp),
                            '$lte' => new MongoDate($endTimestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );
            $array_runningActionNoTasks = array(
                '$and' => array(
                    array(
                        '$or' => [
                            array("tasks" => ['$exists' => false]),
                            array("tasks" => ['$size' => 0])
                        ]
                    ),

                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array("tracking" => true),
                    array(
                        '$or' => [
                            array(
                                "startDate" => array(
                                    '$gte' => new MongoDate($startTimestamp),
                                    '$lte' => new MongoDate($endTimestamp)
                                )
                            ),
                            array(
                                "created" => array(
                                    '$gte' => $startTimestamp,
                                    '$lte' => $endTimestamp
                                )
                            )
                        ]
                    )
                )
            );
            $array_runningActionWithTasks =   array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'todo'),
                    array("tracking" => true),
                    array(
                        "tasks.createdAt" => array(
                            '$gte' => new MongoDate($startTimestamp),
                            '$lte' => new MongoDate($endTimestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );
            $array_doneActionNoTasks = array(
                '$and' => array(
                    array(
                        '$or' => [
                            array("tasks" => ['$exists' => false]),
                            array("tasks" => ['$size' => 0])
                        ]
                    ),

                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'done'),
                    array(
                        '$or' => [
                            array(
                                "modified" => array(
                                    '$gte' => new MongoDate($startTimestamp),
                                    '$lte' => new MongoDate($endTimestamp)
                                )
                            ),
                            array(
                                "updated" => array(
                                    '$gte' => $startTimestamp,
                                    '$lte' => $endTimestamp
                                )
                            )
                        ]
                    )
                )
            );
            $array_doneActionWithTasks = array(
                '$and' => array(
                    array("tasks" => ['$exists' => true, '$not' => ['$size' => 0]]),
                    array("parentId" => ['$in' => $projectId]),
                    array("status" => 'done'),
                    array(
                        "modified" => array(
                            '$gte' => new MongoDate($startTimestamp),
                            '$lte' => new MongoDate($endTimestamp)
                        )
                    ),
                    array(
                        '$or' => [
                            array("tasks.checked" => ['$exists' => false]),
                            array("tasks.checked" => false)
                        ]
                    )
                )
            );
            if ($id_user != null)
            {
                $array_contributor = ["links.contributors." . $id_user => ['$exists' => true]];
                array_push($array_todoActionNoTasks['$and'], $array_contributor);
                array_push($array_todoActionWithTasks['$and'], $array_contributor);
                array_push($array_runningActionNoTasks['$and'], $array_contributor);
                array_push($array_runningActionWithTasks['$and'], $array_contributor);
                array_push($array_doneActionNoTasks['$and'], $array_contributor);
                array_push($array_doneActionWithTasks['$and'], $array_contributor);
            }

            $todoActionNoTasks = PHDB::find(Action::COLLECTION, $array_todoActionNoTasks);
            $todoActionWithTasks = PHDB::find(Action::COLLECTION, $array_todoActionWithTasks);
            $runningActionNoTasks = PHDB::find(Action::COLLECTION, $array_runningActionNoTasks);
            $runningActionWithTasks = PHDB::find(Action::COLLECTION, $array_runningActionWithTasks);
            $doneActionNoTasks = PHDB::find(Action::COLLECTION, $array_doneActionNoTasks);
            $doneActionWithTasks = PHDB::find(Action::COLLECTION, $array_doneActionWithTasks);
        }

        foreach ($todoActionNoTasks as $kact => $vact)
        {
            $keyIteration = 0;
            foreach ($hours as $k => $v)
            {
                if (!empty($vact['startDate']->sec) &&  $this->isBetween($vact['startDate']->sec, isset($hoursStart[$keyIteration]) ? $hoursStart[$keyIteration] : $startTimestamp, $v))
                {
                    $daysLabels["todo"][$v]["value"]++;
                }
                elseif (!empty($vact["created"]) && $this->isBetween($vact["created"], isset($hoursStart[$keyIteration]) ? $hoursStart[$keyIteration] : $startTimestamp, $v))
                {
                    $daysLabels["todo"][$v]["value"]++;
                }
                $keyIteration++;
            }
        }

        foreach ($todoActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                $keyIteration = 0;
                foreach ($hours as $k => $v)
                {
                    if (!empty($vtask['createdAt']->sec) &&  $this->isBetween($vtask['createdAt']->sec, isset($hoursStart[$keyIteration]) ? $hoursStart[$keyIteration] : $startTimestamp, $v))
                    {
                        $daysLabels["todo"][$v]["value"]++;
                    }
                    $keyIteration++;
                }
            }
        }

        foreach ($runningActionNoTasks as $kact => $vact)
        {
            $keyIteration = 0;
            foreach ($hours as $k => $v)
            {
                if (!empty($vact['startDate']->sec) &&  $this->isBetween($vact['startDate']->sec, isset($hoursStart[$keyIteration]) ? $hoursStart[$keyIteration] : $startTimestamp, $v))
                {
                    $daysLabels["running"][$v]["value"]++;
                }
                elseif (!empty($vact["created"]) && $this->isBetween($vact["created"], isset($hoursStart[$keyIteration]) ? $hoursStart[$keyIteration] : $startTimestamp, $v))
                {
                    $daysLabels["running"][$v]["value"]++;
                }
                $keyIteration++;
            }
        }

        foreach ($runningActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                $keyIteration = 0;
                foreach ($hours as $k => $v)
                {
                    if (!empty($vtask['createdAt']->sec) &&  $this->isBetween($vtask['createdAt']->sec, isset($hoursStart[$keyIteration]) ? $hoursStart[$keyIteration] : $startTimestamp, $v))
                    {
                        $daysLabels["running"][$v]["value"]++;
                    }
                    $keyIteration++;
                }
            }
        }

        foreach ($doneActionNoTasks as $kact => $vact)
        {
            $keyIteration = 0;
            foreach ($hours as $k => $v)
            {
                /*if(!empty($vact['startDate']->sec) &&  $this->isBetween($vact['startDate']->sec,$startTimestamp,$v)){
                    $daysLabels["done"][$v]["value"]++;
                }else*/
                if (!empty($vact["updated"]) && $this->isBetween($vact["updated"], isset($hoursStart[$keyIteration]) ? $hoursStart[$keyIteration] : $startTimestamp, $v))
                {
                    $daysLabels["done"][$v]["value"]++;
                    $keyIteration++;
                }
            }
        }

        foreach ($doneActionWithTasks as $kact => $vact)
        {
            foreach ($vact['tasks'] as $ktask => $vtask)
            {
                $keyIteration = 0;
                foreach ($hours as $k => $v)
                {
                    if (!empty($vtask['checkedAt']->sec) &&  $this->isBetween($vtask['checkedAt']->sec, isset($hoursStart[$keyIteration]) ? $hoursStart[$keyIteration] : $startTimestamp, $v))
                    {
                        $daysLabels["done"][$v]["value"]++;
                    }
                    $keyIteration++;
                }
            }
        }

        return  $daysLabels;
    }

    /******************************************** UTILS ***********************************************************/
    private function getProjectByFormId($formId)
    {
        $projectId = [];
        $answers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => $formId, "answers.aapStep1.titre" => ['$exists' => true], "project.id" => ['$exists' => true]), array("project"));
        foreach ($answers as $kans => $vans)
        {
            $projectId[] = $vans["project"]['id'];
        }
        return $projectId;
    }

    private function getProjectByContextId($contextIds)
    {
        $projectIds = [];
        $or = array();
        foreach ($contextIds as $key => $value)
        {
            $or[] = array("parent." . $value => ['$exists' => true]);
        }
        $projects = PHDB::find(Project::COLLECTION, array('$or' => $or), array("_id"));
        if (!empty($projects)) $projectIds = array_keys($projects);
        return $projectIds;
    }

    private function getMembersContextIds($contextIds)
    {
        $members = [];
        $contextIds =  array_map(function ($v)
        {
            return new MongoId($v);
        }, $contextIds);

        $elementOrg = PHDB::find(Organization::COLLECTION, array("_id" => ['$in' => $contextIds]), array("links"));
        $elementProject = PHDB::find(Project::COLLECTION, array("_id" => ['$in' => $contextIds]), array("links"));
        $elements = array_merge($elementOrg, $elementProject);
        foreach ($elements as $key => $value)
        {
            if (!empty($value["links"]["members"]))
            {
                foreach ($value["links"]["members"] as $kl => $vl)
                {
                    if (!isset($vl["toBeValidated"]))
                    {
                        $members[] = new MongoId($kl);
                    }
                }
            }
            if (!empty($value["links"]["contributors"]))
            {
                foreach ($value["links"]["contributors"] as $kl => $vl)
                {
                    if (!isset($vl["toBeValidated"]))
                    {
                        $members[] = new MongoId($kl);
                    }
                }
            }
        }

        $members = PHDB::findAndSort(Citoyen::COLLECTION, array("_id" => ['$in' => $members]), array("name" => 1), null, array("name", "slug"));
        return $members;
    }

    private function getCreatedYears($collection, $query)
    {
        $minimumYear = PHDB::findAndLimitAndIndex($collection, $query, array("created" => 1), array("limit" => 1));
        if (count($minimumYear) > 0)
        {
            $minimumYear = array_values($minimumYear)[0]["created"];
            $minimumYear = (int)date('Y', $minimumYear);
        }
        else
            $minimumYear = '2017';

        $maximumYear =  (int)date('Y');
        $years = [];
        for ($i = $minimumYear; $i <= $maximumYear; $i++)
            $years[$i] = 0;
        return $years;
    }
    private function compareDates($a, $b)
    {
        $dateA = DateTime::createFromFormat('d/m/Y', $a);
        $dateB = DateTime::createFromFormat('d/m/Y', $b);

        if ($dateA === false || $dateB === false)
        {
            throw new Exception("Invalid date format");
        }

        return $dateA <=> $dateB;
    }
    private function timestampOfEachDayOnCurrentWeek($tmsp)
    {
        $today = date('Y-m-d', $tmsp); // Get the current date
        $dayOfWeek = date('N', strtotime($today)); // Get the numeric representation of the day of the week (1 = Monday, 7 = Sunday)

        $timestamps = [];

        for ($i = 1; $i <= 7; $i++)
        {
            $beginTimestamp = strtotime("$today -" . ($dayOfWeek - $i) . " days 00:00:00");
            $endTimestamp = strtotime("$today -" . ($dayOfWeek - $i) . " days 23:59:59");

            $timestamps[date('l', $beginTimestamp)] = [
                'begin' => $beginTimestamp,
                'end' => $endTimestamp,
            ];
        }
        return $timestamps;
    }
    private function isBetween($number, $min, $max)
    {
        return ((int)$number >= (int)$min && (int)$number <= (int)$max);
    }
    /* Y/m/d H:i */
    private function timestampOfDay($dateStr = null)
    {
        $timezone = new DateTimeZone($this->getTimezone());
        if (empty($dateStr))
        {
            $now = new DateTime('now', $timezone);
            $dateStr = $now->format('Y/m/d H:i');
        }

        $currentDate = new DateTime($dateStr, $timezone);
        $currentDateTime = $currentDate;
        $currentDate->setTime(0, 0, 0);

        $endDate = clone $currentDate;
        $endDate->setTime(23, 59, 59);

        $startTimestamp = $currentDate->getTimestamp();
        $endTimestamp = $endDate->getTimestamp();
        $currentDateTime = $currentDateTime->getTimestamp();
        return array(
            "startTimestamp" => $startTimestamp,
            "endTimestamp" => $endTimestamp,
            "currentTimestamp" => $currentDateTime
        );
    }

    function getDayStartAndEndTimestamps($date)
    {
        $dateTime = new DateTime($date);
        $dateTime->setTime(0, 0, 0);
        $startTimestamp = $dateTime->getTimestamp();
        $dateTime->setTime(23, 59, 59);
        $endTimestamp = $dateTime->getTimestamp();
        return [
            'start' => $startTimestamp,
            'end' => $endTimestamp
        ];
    }

    private function dayOffPerPersonne()
    {
        $formId = $_POST["formId"];
        $projectId = $this->getProjectId($formId, $_POST["context"]);
        $where = array(
            '$and' => array(
                array("parentId" => ['$in' => $projectId]),
                array("tags" => 'off'),
            ),
            '$or' => [
                ['startDate' => ['$exists' => 1]],
                ['created' => ['$exists' => 1]]
            ]
        );

        if (isset($_POST["id_user"]) && $_POST["id_user"] != '')
        {
            $id_user = $_POST["id_user"];
            $array_contributor = ["links.contributors." . $id_user =>  ['$exists' => true]];
            array_push($where['$and'], $array_contributor);
        }

        $daysoffs = PHDB::find(Action::COLLECTION, $where, array('name', 'startDate', 'created', 'endDate', 'idUserAuthor', "links.contributors"));
        $members = array();
        $years = array();
        foreach ($daysoffs as $key)
        {
            if (isset($key['links']))
                array_push($members, array_keys($key['links']['contributors'])[0]);
            if (isset($key['endDate']->sec))
                $years[date("Y", $key['endDate']->sec)] = 0;
        }

        $members = array_values(array_unique($members));

        $creators = array_map(function ($val)
        {
            return new MongoId($val);
        }, $members);
        $name_user = PHDB::find(Citoyen::COLLECTION, array("_id" => ['$in' => $creators]), array("name"));

        $daysoffs = array_map(function ($v)   use ($years)
        {
            $start_date = $v['created'];
            if (!empty($v['startDate']) && $v['startDate'] instanceof MongoDate)
                $start_date = $v['startDate']->sec;

            if (!isset($v['endDate']->sec))
                $end_date = $start_date;
            else
                $end_date = $v['endDate']->sec;

            if (isset($v['links']))
                $id_user = array_keys($v["links"]["contributors"])[0];
            else
                $id_user = $v['idUserAuthor'];

            $startDate = date("Y-m-d", $start_date);
            $endDate =  date("Y-m-d", $end_date);

            $dateDebut = new DateTime($startDate);
            $dateFin = new DateTime($endDate);
            $interval = new DateInterval('P1D');
            $periode = new DatePeriod($dateDebut, $interval, $dateFin);

            //Ne pas compte les weeks ends

            $jours = $years;
            $list_jours = array();
            foreach ($periode as $jour)
            {
                $jourSemaine = $jour->format('N'); // 1 (lundi) à 7 (dimanche)
                if ($jourSemaine < 6)
                { // Si le jour n'est pas un samedi (6) ou dimanche (7)
                    $jours[$jour->format('Y')]++;
                    $list_jours[$jour->format('Y-m-d')] = array(
                        'x' => $jour->format('Y-m-d'),
                        'y' => $jourSemaine,
                        'd' => $jour->format('Y-m-d'),
                        'v' => 1

                    );
                }
            }
            if (date("N", $end_date)  < 6)
            {
                $list_jours[date("Y-m-d", $end_date)] = array(
                    'x' => date("Y-m-d", $end_date),
                    'y' => date("N", $end_date),
                    'd' => date("Y-m-d", $end_date),
                    'v' => 1
                );
            }
            if (date("Y-m-d", $start_date) == date("Y-m-d", $end_date))
                $jours[date("Y", $start_date)]++;
            else
                $jours[date("Y", $end_date)]++;


            $daysoff = array(
                "name" => $v['name'],
                "startDate" => $startDate,
                "endDate" => $endDate,
                "idUserAuthor" => $v['idUserAuthor'],
                "id_user" => $id_user,
                "total" => $jours,
                "list_jour" => $list_jours,
            );
            return $daysoff;
        }, $daysoffs);




        $list_jours = array();
        foreach (array_values($daysoffs) as $key)
        {
            foreach ($key["list_jour"] as $k => $value)
            {

                if (isset($list_jours[$k]))
                    $list_jours[$k]['v']++;
                else
                    $list_jours[$k] = $value;
            }
        }




        $finale = array();

        foreach ($members as $member)
        {
            $total = $years;

            foreach (array_values($daysoffs) as $daysoff)
            {

                if ($daysoff['id_user'] == $member)
                {
                    foreach (array_keys($total) as $k)
                    {
                        $total[$k] += $daysoff['total'][$k];
                    }
                }
            }
            $data = array(

                "label" => $member,
                "name" => $name_user[$member]['name'],
                "backgroundColor" => sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255)),
                "data" => array_values($total),
                "stack" => 'Stack 0',
                "id_" => $member

            );
            $finale[$member] = $data;
        }

        return array("years" => array_keys($years), "membersData" => array_values($finale), "calendrier" => $list_jours, "details" =>  $daysoffs);
    }
}
