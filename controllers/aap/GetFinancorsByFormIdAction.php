<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use MongoId;
use Person;
use PHDB;
use Rest;
use Ctenat;
use Form;
use Element;
use Link;
use Organization;
use Project;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class GetFinancorsByFormIdAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id, $getAllItems = false) {
        $orgs = Aap::getAllFinancorsByFormId($id , $getAllItems);
        return Rest::json($orgs);
    }
}
