<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;
use Rest;
use type;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class DirectoryProjectAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($context = null, $form = null) {
        $searchParams = $_POST;
        $output = Aap::globalAutocompleteProject($form, $context, $searchParams);
        return Rest::json($output);
    }
}
