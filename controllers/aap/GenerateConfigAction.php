<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use CAction, Aap, Rest;
class GenerateConfigAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $result = Aap::generateConfig($_POST);
        return Rest::json($result);
    }
}