<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;
use Answer;
use Authorisation;
use CAction;
use Form;
use Rest;
use type;
use Yii;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class DirectoryOrganismChooserAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($slug=null){
		$controller = $this->getController();
		$searchParams = $_POST;
		$organism = Aap::globalAutocompleteOrganismChooser2($slug, $searchParams);
		return Rest::json($organism);
	}
}
