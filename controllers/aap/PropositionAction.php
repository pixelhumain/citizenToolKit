<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Answer;
use Authorisation;
use cebe\markdown\Markdown;
use Form;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Rest;
use Comment;
use Markdown_Parser;
use MongoId;
use Organization;
use Parsedown;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Project;
use Yii;

class PropositionAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($request = null) {
		$output = '';
		$user = Yii::app()->session['userId'] ?? '';
		switch ($request) {
			case 'user_have_answer':
				$timezone = !empty(Yii::app()->session["timezone"]) ? Yii::app()->session["timezone"] : "UTC";
				date_default_timezone_set($timezone);
				$now = date('d-m-Y H:i', time());
				$form = PHDB::findOneById(Form::COLLECTION, $_POST["form"], ["startDate", "endDate"]);
				$startDate = !empty($form["startDate"]) ? $form["startDate"]->toDateTime()->format('c') : null;
				$endDate = !empty($form["endDate"]) ? $form["endDate"]->toDateTime()->format('c') : null;
				$checkDate = Api::checkDateStatus($now, $startDate, $endDate);
				if (empty($user)) $output = ["hasAnswer" => Rest::json(false), "session" => $checkDate];
				else $output = ["hasAnswer" => Rest::json(Aap::countUserAnswer($_POST['form'], $user) > 0), "session" => $checkDate];
				break;
			case 'get_summary':
				//$db_ans = PHDB::findOneById(Answer::COLLECTION, $_POST['id']);
                $db_ans =  Aap::filterdepense($_POST['id'],null);
				$params = Aap::parsePropositionData(['results' => [$_POST['id'] => $db_ans]]);
				$output = $this->getController()->renderPartial('co2.views.aap.partials.proposition_summary', $params);
				break;
			case 'get_authorization':
				if (strlen($user))
					$output = Rest::json(Aap::getAnswerAuthorization($_POST['answer'], $user));
				else {
					$output = Rest::json([
						'reason' => 'logged out',
						'canEdit' => false,
						'canRead' => false
					]);
				}
				break;
			case 'get_one_answer':
                //$db_ans = PHDB::findOneById(Answer::COLLECTION, $_POST['id']);
                $db_ans =  Aap::filterdepense($_POST['id'],null);
				$db_ans["commentCount"] = PHDB::count(Comment::COLLECTION, array("contextId" => $_POST['id']));
				$output = Rest::json(Aap::parsePropositionData(['results' => [$_POST['id'] => $db_ans]]));
				break;
			case 'get_aboutelem':
				$elementId = isset($_POST["elementId"]) ? $_POST["elementId"] : "";
				$elementType = isset($_POST["elementType"]) ? $_POST["elementType"] : "";
				$db_element = !empty($elementId) && !empty($elementType) ? PHDB::findOneById($elementType, $elementId) : [];
				$params["elementId"] = $elementId;
				$params["elementType"] = $elementType;
				$params["elemContext"] = $db_element;
				$output = $this->getController()->renderPartial('co2.views.aap.aboutElement', $params);
				break;
			case 'random':
				$limit = empty($_POST['limit']) ? 1 : intval($_POST['limit']);
				$output = [
					'success'	=> 1,
					'content'	=> null
				];
				$aggregation = [
					[
						'$match'   => [
							// 'form'                      => $_POST['form'],
							'answers.aapStep1.titre'     => ['$exists' => 1],
						]
					],
					['$sample'  => ['size' => $limit]],
					['$project' => [
						'answers.aapStep1.titre'		              => 1,
                        'answers.aapStep1.depense' => [
                            '$filter' => [
                                'input' => '$answers.aapStep1.depense',
                                'as' => 'item',
                                'cond' => [
                                    '$ne' => [
                                        '$$item.include',
                                        false
                                    ]
                                ]
                            ]
                        ],
						'answers.aapStep1.description'	              => 1,
						'answers.aapStep1.tags'			              => 1,
						'answers.aapStep1.aapStep1m03ot9qymmfgashp7l' => 1,
						'answers.aapStep1.aapStep1lzi62x3etw49gyc424d' => 1,
					]]
				];
				if (isset($_POST["filters"])) {
					foreach ($_POST["filters"] as $fKey => $fValue) {
						if (strpos($fKey, '$or') !== false) {
							if (is_array($fValue) && Api::isAssociativeArray($fValue)) {
								foreach ($fValue as $valKey => $valValue) {
									$aggregation[0]['$match'][$fKey][] = [$valKey => $valValue];
								}
							} else {
								$aggregation[0]['$match'][$fKey] = $fValue;
							}
						} else {
							$aggregation[0]['$match'][$fKey] = $fValue;
						}
					}
				} else {
					$aggregation[0]['$match']["form"] = $_POST['form'];
				}
				$db_ans = PHDB::aggregate(Form::ANSWER_COLLECTION, $aggregation);
				if (!empty($db_ans['result']))
					$db_ans = $db_ans['result'];
				else
					$db_ans = null;
				if (!empty($db_ans)) {
					$output['content'] = [];
					$image_list = Aap::getPropositionThumbnail(array_map(fn($item) => (string)$item['_id'], $db_ans));
					foreach ($db_ans as $db_answer) {
						$id = (string)$db_answer['_id'];
						$as1 = $db_answer['answers']['aapStep1'];
						$description = [
							'full'		=> !empty($as1['description']) ? $as1['description'] : '',
							'preview'	=> ''
						];
						$description['full'] = $description['full'];
						$description['preview'] = mb_convert_encoding(substr($description['full'], 0, 100), 'UTF-8');
						if (!empty($description['preview']) && strlen($description['preview']) < strlen($description['full']))
							$description['preview'] .= ' ...';
						$answer = [
							'id' 					=>  $id,
							'image'					=>	$image_list[$id],
							'titre'					=>	htmlspecialchars_decode($as1['titre']),
							'description'			=>	[
								'long'	=> $description['full'],
								'short'	=> $description['preview']
							],
							'tags'					=>	!empty($as1['tags']) && is_array($as1['tags']) ? $as1['tags'] : [],
							'tagsbesoin'            =>  !empty($as1["aapStep1m03ot9qymmfgashp7l"]) && is_array($as1["aapStep1m03ot9qymmfgashp7l"]) ? $as1["aapStep1m03ot9qymmfgashp7l"] : [],
							'depense'				=>	[],
							'shortDesc'             =>  !empty($as1["aapStep1lzi62x3etw49gyc424d"]) ? $as1["aapStep1lzi62x3etw49gyc424d"] : "",
						];
						$depenses = !empty($as1['depense']) && is_array($as1['depense']) ? $as1['depense'] : [];
						$answer['depense'] = array_map(function ($depense) {
							$ret = [
								'price'		=>	isset($depense['price']) ? intval($depense['price']) : 0,
								'financer'	=>	!empty($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : []
							];
							$ret['financer'] = array_map(fn($financer) => isset($financer['amount']) ? intval($financer['amount']) : 0, $ret['financer']);
							return ($ret);
						}, $depenses);
						$output['content'][] = $answer;
					}
					if ($limit === 1)
						$output['content'] = end($output['content']);
				} else
					$output = [
						'success'	=> 0,
						'content'	=> 'Empty answer'
					];
				$output = Rest::json($output);
				break;
			case 'get_answer_v2':
				$output = [
					'success'	=> 1,
					'content'	=> null
				];
				if (!empty($_POST['id'])) {
					//$db_answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $_POST['id']);
                    // $db_answer = PHDB::findOneById(Answer::COLLECTION, $_POST['id']);
                    $db_answer =  Aap::filterdepense($_POST['id'],null);
					$as1 = !empty($db_answer['answers']['aapStep1']) ? $db_answer['answers']['aapStep1'] : [];
					$desc = $as1['description'] ?? '';
					$contribs = !empty($db_answer['links']['contributors']) && is_array($db_answer['links']['contributors']) ? $db_answer['links']['contributors'] : [];
					$votes = !empty($db_answer['vote']) && is_array($db_answer['vote']) ? $db_answer['vote'] : [];
					$tags = !empty($as1['tags']) && is_array($as1['tags']) ? $as1['tags'] : [];
					$author = !empty($db_answer['user']) ? PHDB::findOneById(Person::COLLECTION, $db_answer['user'], ['name', 'profilImageUrl', 'profilMediumImageUrl']) : null;
					$form = !empty($db_answer["form"]) ? PHDB::findOneById(Form::COLLECTION, $db_answer["form"], ["params", "contributorsCanEdit"]) : [];
					$author = !empty($author) ? [
						'id' => $db_answer['user'],
						'name' => $author['name'],
						'image_medium'	=> !empty($author['profilMediumImageUrl']) ? $author['profilMediumImageUrl'] : Yii::app()
							->getModule('co2')
							->getAssetsUrl() . '/images/thumb/default_persons.png',
						'image_default'	=> !empty($author['profilImageUrl']) ? $author['profilImageUrl'] : Yii::app()
							->getModule('co2')
							->getAssetsUrl() . '/images/thumb/default_persons.png'
					] : null;
					$linkedAnswers = [];
					// input référant du commun
					if (!empty($form["params"]["finderaapStep1m0w402djpsq3i44017"]["addToLinks"]["value"])) {
						$linksData = $form["params"]["finderaapStep1m0w402djpsq3i44017"]["addToLinks"];
						$linkedAnswers[$linksData["links"]] = "answers.aapStep1.finderaapStep1m0w402djpsq3i44017";
					}
					$depenses = !empty($as1['depense']) && is_array($as1['depense']) ? $as1['depense'] : [];
					$depenses = array_map(function ($depense) {
						$ret = [
							'price'		=>	isset($depense['price']) ? intval($depense['price']) : 0,
							'financer'	=>	!empty($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : []
						];
						$ret['financer'] = array_map(fn($financer) => [
							'id'		=> $financer['id'] ?? null,
							'amount'	=> (isset($financer['amount']) ? intval($financer['amount']) : 0),
							'user'		=> $financer['user'] ?? null,
							'date'		=> $financer['date'] ?? null,
							'name'		=> $financer['name'] ?? null,
							'email'		=> $financer['email'] ?? null
						], $ret['financer']);
						return ($ret);
					}, $depenses);
					// special aactl
					$desc_summary = '';
					$context_link = '';
					$authCheckParams = [];
					if (!empty($form["contributorsCanEdit"])) {
						$authCheckParams["checkContributors"] = true;
					}
					$isAnswerEditable = Authorisation::canEdit(Yii::app()->session['userId'], $_POST['id'], Form::ANSWER_COLLECTION, $authCheckParams);
					if (isset($db_answer["user"]) && $db_answer["user"] == Yii::app()->session["userId"]) {
						$isAnswerEditable = true;
					}
					if (!empty($as1['aapStep1lzi62x3etw49gyc424d']))
						$desc_summary = $as1['aapStep1lzi62x3etw49gyc424d'];
					if (!empty($as1['aapStep1m1esnlbwekc71uotpf4']))
						$context_link = $as1['aapStep1m1esnlbwekc71uotpf4'];
					$canEditForm = null;
					if (!empty($db_answer["links"]["canEdit"]))
						$canEditForm = $db_answer["links"]["canEdit"];
					$statusProject = null;
					if (!empty($db_answer["status"]))
						$statusProject = $db_answer["status"];
					$project_data = null;
					if (!empty($db_answer["project"]["id"]))
						$project_data = PHDB::findOneById(Project::COLLECTION, $db_answer["project"]["id"], ['_id', 'name', 'slug', 'profilImageUrl', 'parent']);
					$aacForms = [];
					if (isset($db_answer["answers"]["aapStep2"]["choose"])) {
						$aacParentData = [
							"projectIds" => [],
							"orgaIds" => [],
							"aac" => [],
						];
						foreach ($db_answer["answers"]["aapStep2"]["choose"] as $contextKey => $contextValue) {
							if ($contextValue["value"] == "selected") {
								$aacParentData["aac"]['$or'][] = [
									"parent.$contextKey" => [
										'$exists' => true
									],
									"aapType" => "aac"
								];
								if (isset($contextValue["type"])) {
									if ($contextValue["type"] == Organization::COLLECTION) {
										UtilsHelper::push_array_if_not_exists($contextKey, $aacParentData["orgaIds"]);
									}
									if ($contextValue["type"] == Project::COLLECTION) {
										UtilsHelper::push_array_if_not_exists($contextKey, $aacParentData["projectIds"]);
									}
								}
							}
						}
						if (!empty($aacParentData["aac"])) {
							$aacForms = PHDB::find(Form::COLLECTION, $aacParentData["aac"], ["name", "parent"]);
						}
						if (!empty($aacParentData["projectIds"])) {
							$aacProjectsParent = PHDB::findByIds(Project::COLLECTION, $aacParentData["projectIds"], ["name", "slug", "profilImageUrl", "profilMediumImageUrl"]);
						}
						if (!empty($aacParentData["orgaIds"])) {
							$aacOrgasParent = PHDB::findByIds(Organization::COLLECTION, $aacParentData["orgaIds"], ["name", "slug", "profilImageUrl", "profilMediumImageUrl"]);
						}
						foreach ($aacForms as $aacKey => $aacValue) {
							if (isset($aacValue["parent"])) {
								$parentKey = array_keys($aacValue["parent"])[0];
								if (isset($aacValue["parent"][$parentKey]["type"])) {
									$parentData = [];
									if ($aacValue["parent"][$parentKey]["type"] == Organization::COLLECTION && isset($aacOrgasParent[$parentKey])) {
										$parentData = $aacOrgasParent[$parentKey];
									}
									if ($aacValue["parent"][$parentKey]["type"] == Project::COLLECTION && isset($aacProjectsParent[$parentKey])) {
										$parentData = $aacProjectsParent[$parentKey];
									}
									if (!empty($parentData["profilImageUrl"])) {
										$aacForms[$aacKey]["profilImageUrl"] = $parentData["profilImageUrl"];
									}
									if (!empty($parentData["profilMediumImageUrl"])) {
										$aacForms[$aacKey]["profilMediumImageUrl"] = $parentData["profilMediumImageUrl"];
									}
									if (isset($parentData["name"])) {
										$aacForms[$aacKey]["parentData"]["name"] = $parentData["name"];
									}
									$aacForms[$aacKey]["parentData"]["id"] = $parentKey;
									$aacForms[$aacKey]["parentData"]["type"] = $aacValue["parent"][$parentKey]["type"];
									$aacForms[$aacKey]["parentData"]["slug"] = $parentData["slug"];
								}
							}
						}
					}
					$images = Aap::getPropositionImages($_POST['id']);
					if (empty($images))
						$images = [
							Yii::app()
								->getModule('co2')
								->getAssetsUrl() . '/images/thumbnail-default.jpg'
						];
					$answer = [
						'id'			=> $_POST['id'],
						'titre'			=> htmlspecialchars_decode($as1['titre'] ?? ''),
						'tags'			=> $tags,
						'image'			=> end($images),
						'images'		=> $images,
						'desc_full'		=> $desc,
						'desc_summary'	=> $desc_summary,
						'contributors'	=> $contribs,
						'votes'			=> $votes,
						'author'		=> $author,
						'depenses'		=> $depenses,
						'steps'			=> [],
						'infos'			=> [
							'form'	=> $db_answer['form'],
						],
						'editable'		=> $isAnswerEditable,
						"project"		=> $project_data,
						"aacParents"	=> $aacForms,
						"canEditForm"	=> $canEditForm,
						"context_link"	=> $context_link,
						"statusProject"	=> $statusProject,
						"answers_links" => $linkedAnswers
					];
					foreach (['aapStep1', 'aapStep2', 'aapStep3', 'aapStep4'] as $step)
						$answer['steps'][$step] = $db_answer['answers'][$step] ?? null;
					$output['content'] = $answer;
				} else
					$output = [
						'success'	=> 0,
						'content'	=> 'Required id is missing'
					];
				$output = Rest::json($output);
				break;
			case 'contributev2':
				$output = [
					'success'	=> 1,
					'content'	=> 0
				];
				if (!empty($_POST['answer']) && !empty($_POST['contribs']) && isset($_POST['action'])) {
					$where = ['_id' => new MongoId($_POST['answer'])];
					if (intval($_POST['action'])) {
						$action = ['$set' => []];
						foreach ($_POST['contribs'] as $contrib)
							$action['$set']['links.contributors.' . $contrib . '.type'] = Person::COLLECTION;
					} else {
						$action = ['$unset' => []];
						foreach ($_POST['contribs'] as $contrib)
							$action['$unset']['links.contributors.' . $contrib] = 1;
					}
					PHDB::update(Form::ANSWER_COLLECTION, $where, $action);
					$output['content'] = $action;
				} else
					$output = [
						'success'	=> 1,
						'content'	=> 'Required params are missing'
					];
				$output = Rest::json($output);
				break;
			case 'checkElement':
				$output = [
					'success'	=> 1,
					'content'	=> 0
				];
				if (!empty($_POST['id']) && !empty($_POST['type'])) {
					$where = ['_id' => new MongoId($_POST['id'])];
					$response =  PHDB::findOneById($_POST['type'], $_POST['id'], ['_id', 'oceco', 'type', 'name']);
					$oceco = null;
					if (isset($response['oceco']['convertCommunToProject']) && ($response['oceco']['convertCommunToProject'] == true || $response['oceco']['convertCommunToProject'] == "true")) {
						$oceco = $response['oceco']['convertCommunToProject'];
					}
					$output['content'] = $response;
					$output['oceco'] = $oceco;
				} else
					$output = [
						'success'	=> 1,
						'content'	=> 'Required params are missing'
					];
				$output = Rest::json($output);
				break;
			case 'tl_use':
				$output = [
					'success'	=> 1,
					'content'	=> null
				];
				$answer = $_POST['answer'] ?? '';
				$tls = $_POST['tls'] ?? [];
				if (empty($answer) || empty($tls))
					return (Rest::json([
						'success'	=> 0,
						'content'	=> 'Requires fields are missing'
					]));
				$db_answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answer, ['links.tls']);
				$tls_link = isset($db_answer['links']['tls']) && is_array($db_answer['links']['tls']) ? $db_answer['links']['tls'] : [];
				$tls_link = array_merge($tls_link, $tls);
				$db_where = [
					'$or'	=> [
						['source.keys'		=> 'franceTierslieux'],
						['reference.costum'	=> 'franceTierslieux']
					],
					"links.members.$user.isAdmin"	=> true
				];
				$db_organizations = PHDB::find(Organization::COLLECTION, $db_where, ['_id']);
				$db_organizations = array_keys($db_organizations);
				foreach ($db_organizations as $key) {
					if (empty($tls[$key]))
						unset($tls_link[$key]);
				}
				PHDB::update(Form::ANSWER_COLLECTION, ['_id' => $db_answer['_id']], ['$set' => ['links.tls' => $tls_link]]);
				$output['content'] = $tls_link;
				$output = Rest::json($output);
				break;
			case 'tl_user':
				$output = [
					'success'	=> 1,
					'content'	=> null
				];
				$answer = $_POST['answer'] ?? '';
				$user = $_POST['user'] ?? '';
				$user_session = Yii::app()->session['userId'] ?? '';
				if (empty($answer))
					return ([
						'success'	=> 0,
						'content'	=> 'Requires fields are missing'
					]);
				$db_where = [
					'$or'	=> [
						['source.keys'		=> 'franceTierslieux'],
						['reference.costum'	=> 'franceTierslieux']
					],
				];
				if (!empty($user))
					$db_where["links.members.$user.type"] = Person::COLLECTION;
				$db_organizations = PHDB::find(Organization::COLLECTION, $db_where, ['_id', 'links.members', "profilImageUrl"]);
				$db_answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answer, ['links.tls']);
				$tls_link = isset($db_answer['links']['tls']) && is_array($db_answer['links']['tls']) ? $db_answer['links']['tls'] : [];
				$output['content'] = [
					'is_user'	=> 0,
					'tls'		=> []
				];
				foreach ($db_organizations as $i => $db_organization) {
					if (!empty($tls_link[$i])) {
						$tl = $tls_link[$i];
						$tl['id'] = $i;
						$tl["image"] = !empty($db_organization["profilImageUrl"]) ? $db_organization["profilImageUrl"] : Yii::app()->getModule("co2")->getAssetsUrl() . "/images/thumb/default_organizations.png";
						$output['content']['tls'][] = $tl;
						if (!empty($user_session) && !empty($db_organization['links']['members'][$user_session]) && $db_organization['links']['members'][$user_session]['type'] === Person::COLLECTION)
							$output['content']['is_user'] = 1;
					}
				}
				$output = Rest::json($output);
				break;
			case "get_answer_id_next":
				if (empty($_POST["id"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Empty answer id or bad format (string only)"
					]));
				//$db_this = PHDB::findOneById(Form::ANSWER_COLLECTION, $_POST["id"], ["form", "created"]);
                //$db_ans = PHDB::findOneById(Answer::COLLECTION, $_POST['id']);
                $db_this =  Aap::filterdepense($_POST['id'],null);
                $db_where = [
					"form"	=> $db_this["form"],
					"answers.aapStep1.titre"	=> ['$exists' => 1],
					"created"	=> ['$gt' => $db_this["created"]]
				];
				if (!empty($_POST["criteria"]))
					$db_where = array_merge($db_where, $_POST["criteria"]);
				$db_next = PHDB::findAndFieldsAndSortAndLimitAndIndex(
					Form::ANSWER_COLLECTION,
					$db_where,
					["_id"],
					["created"	=> 1],
					1,
					0
				);
				if (count($db_next)) {
					$db_next = end($db_next);
					$db_next = (string)$db_next["_id"];
				} else
					$db_next = null;
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $db_next
				]));
				break;
			case "get_answer_id_prev":
				if (empty($_POST["id"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Empty answer id or bad format (string only)"
					]));
				$db_this = PHDB::findOneById(Form::ANSWER_COLLECTION, $_POST["id"], ["form", "created"]);
				$db_where = [
					"form"	=> $db_this["form"],
					"answers.aapStep1.titre"	=> ['$exists' => 1],
					"created"	=> ['$lt' => $db_this["created"]]
				];
				if (!empty($_POST["criteria"]))
					$db_where = array_merge($db_where, $_POST["criteria"]);
				$db_prev = PHDB::findAndFieldsAndSortAndLimitAndIndex(
					Form::ANSWER_COLLECTION,
					$db_where,
					["_id"],
					["created"	=> -1],
					1,
					0
				);
				if (count($db_prev)) {
					$db_prev = end($db_prev);
					$db_prev = (string)$db_prev["_id"];
				} else
					$db_prev = null;
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $db_prev
				]));
				break;
		}
		return $output;
	}
}
