<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Answer;
use Form;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Rest;
use SearchNew;
use Yii;

class setFtlInputAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($form = null) {
        $res = array();
        if(!empty($form) && Form::canAdmin($form)){
            $res =  Form::setFtlInput($form);
        }
        return Rest::json($res);
    }
}
