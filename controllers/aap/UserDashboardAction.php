<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use cebe\markdown\Markdown;
use Citoyen;
use Document;
use Form;
use Organization;
use Person;
use PHDB;
use PixelHumain\PixelHumain\components\Action;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Project;
use Rest;
use Yii;

class UserDashboardAction extends Action {
	const DESCRIPTION_LENGTH = 100;
	public function run($function = null) {
		$form = null;
		$user = null;

		if (!empty($_POST['user']))
			$user = $_POST['user'];
		elseif (!empty(Yii::app()->session['userId']))
			$user = Yii::app()->session['userId'];
		if (!empty($_POST['form']))
			$form = $_POST['form'];
		else {
			$form = Aap::generateDefautlUrl($_POST['costumSlug']);
			$form = (string) $form['_id'];
		}
		if (!empty($function) && gettype($function)) {
			$dashboard = $function;
			$function = '_' . sha1($function);
			if (!method_exists($this, $function))
				return (Rest::json([
					'success'	=> 0,
					'data'		=> 'You are trying to call a non existing method'
				]));
			if (empty($user))
				return (Rest::json([
					'success'	=> 0,
					'data'		=> 'Empty user id'
				]));
			return ($this->$function($_POST['costumSlug'], $form, $dashboard, $user));
		}
		$parameters = [
			'roles'        => [],
			'rolesMessage' => [
				'admin'      => [
					'title'       => 'Vous êtes administrateur',
					'description' => 'Ce rôle vous offre un accès intégral, y compris en édition, à la plateforme'
				],
				'Partenaire' => [
					'title'       => 'Vous êtes partenaire',
					'description' => 'Ce rôle vous permet de visualiser l’ensemble de la plateforme et son contenu. Il permet notamment de commenter tous les dispositifs et leurs actions en construction'
				]
			],
			'form'         => $form
		];
		if (!empty(Yii::app()->session['user']['roles']['superAdmin']) && filter_var(Yii::app()->session['user']['roles']['superAdmin'], FILTER_VALIDATE_BOOLEAN))
			$parameters['roles'][] = 'admin';
		$form = PHDB::findOneById(Form::COLLECTION, $_POST['form'], ['parent']);
		$parentKey = array_keys($form['parent'])[0];
		$formParent = PHDB::findOneById($form['parent'][$parentKey]['type'], $parentKey, ['slug', 'links']);

		$linkTypes = ['contributors', 'members'];
		$isAdmin = array_filter($linkTypes, function ($linkType) use ($formParent) {
			return !empty($formParent['links'][$linkType][Yii::app()->session['userId']]['isAdmin']) && filter_var($formParent['links'][$linkType][Yii::app()->session['userId']]['isAdmin']);
		});
		if (count($isAdmin) > 0)
			UtilsHelper::push_array_if_not_exists('admin', $parameters['roles']);
		foreach ($linkTypes as $linkType) {
			if (!empty($formParent['links'][$linkType][Yii::app()->session['userId']]['roles']) && is_array($formParent['links'][$linkType][Yii::app()->session['userId']]['roles'])) {
				foreach ($formParent['links'][$linkType][Yii::app()->session['userId']]['roles'] as $role)
					UtilsHelper::push_array_if_not_exists($role, $parameters['roles']);
			}
		}
		$dbProposalListliked =  PHDB::find(Form::ANSWER_COLLECTION, [
			'vote.' . Yii::app()->session['userId'] => ['$exists' => 1],
			'answers.aapStep1.titre' => ['$exists' => 1]
		], ['answers.aapStep1.titre', 'answers.aapStep1.description', 'project.id']);
		$parameters['proposalsliked'] = $this->getImage($dbProposalListliked);
		$dbProposalList = PHDB::find(Form::ANSWER_COLLECTION, [
			'form'                   => $_POST['form'],
			'$or'                    => [
				['links.contributors.' . Yii::app()->session['userId'] => ['$exists' => 1]],
				['user' => Yii::app()->session['userId']],
			],
			'answers.aapStep1.titre' => ['$exists' => 1]
		], ['answers.aapStep1.titre', 'answers.aapStep1.description', 'project.id']);
		$parameters['proposals'] = $this->getImage($dbProposalList);
		$dbProjectList = PHDB::find(Project::COLLECTION, [
			'$and' => [
				[
					'$or' => [
						["parentId" => $parentKey, 'parentType' => $form['parent'][$parentKey]['type']],
						["parent.$parentKey.type" => $form['parent'][$parentKey]['type']]
					]
				],
				[
					'$or' => [
						['links.contributors.' . Yii::app()->session['userId'] => ['$exists' => 1]],
						['creator' => Yii::app()->session['userId']]
					]
				]
			]
		], ['name', 'description', 'profilImageUrl']);
		$parameters['projects'] = array_map(function ($project) {
			$markdown = new Markdown();
			$markdown->html5 = true;
			$description = substr($project['description'] ?? '', 0, self::DESCRIPTION_LENGTH);
			$description = !empty($description) ? "$description..." : '';
			return [
				'id'          => (string)$project['_id'],
				'name'        => $project['name'],
				'image'       => !empty($project['profilImageUrl']) ? Yii::app()->createUrl($project['profilImageUrl']) : Yii::app()
					->getModule('co2')
					->getAssetsUrl() . "/images/thumb/default_" . Project::COLLECTION . ".png",
				'description' => $markdown->parse($description)
			];
		}, array_values($dbProjectList));
		$parameters['context'] = $formParent['slug'];
		return ($this->getController()->renderPartial('co2.views.aap.partials.dashboard', $parameters));
	}

	private function getImage($dbProposalList) {
		$dbProposalImageList = PHDB::find(\Document::COLLECTION, [
			'id'     => ['$in' => array_keys($dbProposalList)],
			'type'   => Form::ANSWER_COLLECTION,
			'subKey' => 'aapStep1.image',
			'folder' => ['$exists' => 1]
		], ['id', 'name', 'folder']);
		return  array_map(function ($proposal) use ($dbProposalImageList) {
			$markdown = new Markdown();
			$markdown->html5 = true;
			$description = substr($proposal['answers']['aapStep1']['description'] ?? '', 0, self::DESCRIPTION_LENGTH);
			$description = !empty($description) ? "$description..." : '';
			$images = array_filter($dbProposalImageList, function ($proposalImage) use ($proposal) {
				return $proposalImage['id'] === (string)$proposal['_id'];
			});
			$images = array_values($images);
			return [
				'id'          => (string)$proposal['_id'],
				'name'        => $proposal['answers']['aapStep1']['titre'] ?? 'no title',
				'image'       => count($images) > 0 ? Yii::app()
					->createUrl('/upload/communecter/' . $images[0]['folder'] . '/' . $images[0]['name']) : Yii::app()
					->getModule("co2")->assetsUrl . "/images/thumbnail-default.jpg",
				'description' => $markdown->parse($description)
			];
		}, array_values($dbProposalList));
	}

	private function _552e2905405eec9c8dcd0fe0353b625a31db7275($context, $form, $dashboard, $user) {
		if ($user) {
			$userProfil = PHDB::findOneById(Citoyen::COLLECTION, $user, ['profilImageUrl', '_id', 'slug', 'name']);
		}
		$commun_page = "#detail-un-commun";
		$view = 'co2.views.aap.partials.' . $dashboard;
		$ft = [
			'profile'	=> [
				'mode'  => isset($userProfil) && isset($userProfil["name"]) && $userProfil["name"] == Yii::app()->session["user"]["name"] ? false : true,
				'image'	=> isset($userProfil) && isset($userProfil["profilImageUrl"]) ? $userProfil["profilImageUrl"] : (!empty(Yii::app()->session["user"]["profilMediumImageUrl"]) ? Yii::app()->session["user"]["profilMediumImageUrl"] : Yii::app()->getModule('co2')->getParentAssetsUrl() . '/images/thumb/default_citoyens.png'),
				'slug'	=> isset($userProfil) && isset($userProfil["slug"]) ? $userProfil["slug"] : Yii::app()->session['user']['slug'],
				'name'	=> isset($userProfil) && isset($userProfil["name"]) ? $userProfil["name"] : Yii::app()->session["user"]["name"]
			],
			'context'	=> $context,
			'form'		=> $form,
			'bills'		=> [],
			'proposal_list'	=> [
				'created' => [
					'title'	=> Yii::t("common", "The commons I've created"),
					'data'	=> [],
					'tab'	=> 'aactl-commun',
					'type'	=> 'proposal'
				],
				'fund' => [
					'title'	=> Yii::t("common", "The commons I co-fund"),
					'data'	=> [],
					'tab'	=> 'aactl-commun',
					'type'	=> 'proposal'
				],
				'contribute' => [
					'title'	=> Yii::t("common", "The commons to which I contribute"),
					'data'	=> [],
					'tab'	=> 'aactl-commun',
					'type'	=> 'proposal'
				],
				'liked' => [
					'title'	=> Yii::t("common", "The commons that interest me"),
					'data'	=> [],
					'tab'	=> 'aactl-commun',
					'type'	=> 'proposal'
				],
				'tladmin' => [
					'title'	=> Yii::t("common", "The “tiers-lieux” of which I am admin"),
					'data'	=> [],
					'tab'	=> 'aactl-tl',
					'type'	=> 'tl'
				],
				'tlnetworkadmin' => [
					'title'	=> Yii::t("common", "The networks"),
					'data'	=> [],
					'tab'	=> 'aactl-tl',
					'type'	=> 'tl'
				],
			],
		];
		$orga_is_admin = [
			["links.members.$user.type"		=> Person::COLLECTION],
			["links.members.$user.isAdmin"	=> true],
			[
				'$or' => [
					["links.members.$user.toBeValidated" => ['$exists' => 0]],
					["links.members.$user.toBeValidated" => false],
				]
			],
			[
				'$or' => [
					["links.members.$user.isAdminPending" => ['$exists' => 0]],
					["links.members.$user.isAdminPending" => false],
				]
			]
		];
		$answer_id_list = [];
		$markdown_parser = new Markdown();
		$markdown_parser->html5 = true;
		// Les communs que j'ai liké
		$commun_where = ['form' => $form, "vote.$user.status" => 'love', 'answers.aapStep1.titre' => ['$exists' => 1]];
		$db_commun_list = PHDB::find(Form::ANSWER_COLLECTION, $commun_where, ['answers.aapStep1']);
		$answer_id_list = array_unique(array_merge($answer_id_list, array_keys($db_commun_list)));
		$db_commun_list = array_map(fn($db_commun_item) => $db_commun_item['answers']['aapStep1'], $db_commun_list);
		foreach ($db_commun_list as $id => $db_commun_item) :
			$description = substr($db_commun_item['description'] ?? '', 0, self::DESCRIPTION_LENGTH);
			$description = !empty($description) ? "$description..." : '';
			$commun = [
				'id'			=> $id,
				'name'			=> $db_commun_item['titre'],
				'url'			=> "$commun_page.communId.$id",
				'description'	=> [],
				'image'			=> Yii::app()
					->getModule('co2')
					->getAssetsUrl() . "/images/thumb/default_proposals.png"
			];
			$ft['proposal_list']['liked']['data'][] = $commun;
		endforeach;
		// Les communs que j'ai créé
		$commun_where = ['form' => $form, 'user' => $user, 'answers.aapStep1.titre' => ['$exists' => 1]];
		$db_commun_list = PHDB::find(Form::ANSWER_COLLECTION, $commun_where, ['answers.aapStep1']);
		$answer_id_list = array_unique(array_merge($answer_id_list, array_keys($db_commun_list)));
		$db_commun_list = array_map(fn($db_commun_item) => $db_commun_item['answers']['aapStep1'], $db_commun_list);
		foreach ($db_commun_list as $id => $db_commun_item) :
			$description = substr($db_commun_item['description'] ?? '', 0, self::DESCRIPTION_LENGTH);
			$description = !empty($description) ? "$description..." : '';
			$commun = [
				'id'			=> $id,
				'name'			=> $db_commun_item['titre'],
				'url'			=> "$commun_page.communId.$id",
				'description'	=> [],
				'image'			=> Yii::app()
					->getModule('co2')
					->getAssetsUrl() . "/images/thumb/default_proposals.png"
			];
			$ft['proposal_list']['created']['data'][] = $commun;
		endforeach;
		// Les communs auxquels je contribue
		$commun_where = ['form' => $form, "links.contributors.$user.type" => Person::COLLECTION, 'answers.aapStep1.titre' => ['$exists' => 1]];
		$db_commun_list = PHDB::find(Form::ANSWER_COLLECTION, $commun_where, ['answers.aapStep1']);
		$answer_id_list = array_unique(array_merge($answer_id_list, array_keys($db_commun_list)));
		$db_commun_list = array_map(fn($db_commun_item) => $db_commun_item['answers']['aapStep1'], $db_commun_list);
		foreach ($db_commun_list as $id => $db_commun_item) :
			$description = substr($db_commun_item['description'] ?? '', 0, self::DESCRIPTION_LENGTH);
			$description = !empty($description) ? "$description..." : '';
			$commun = [
				'id'			=> $id,
				'name'			=> $db_commun_item['titre'],
				'url'			=> "$commun_page.communId.$id",
				'description'	=> [],
				'image'			=> Yii::app()
					->getModule('co2')
					->getAssetsUrl() . "/images/thumb/default_proposals.png"
			];
			$ft['proposal_list']['contribute']['data'][] = $commun;
		endforeach;
		// Les tl où je suis admin
		$tl_where = [
			'$or'	=> [
				['source.keys'		=> 'franceTierslieux'],
				['reference.costum'	=> 'franceTierslieux']
			],
			'$and' => $orga_is_admin,
		];
		$aggregation = [
			['$match'	=> $tl_where],
			[
				'$group'	=> [
					'_id'	=> '$slug',
					'doc'	=> ['$first' => '$$ROOT']
				]
			],
			['$replaceRoot' => ['newRoot' => '$doc']],
			[
				'$project'	=> [
					'name'					=> 1,
					'slug'					=> 1,
					'description'			=> 1,
					'profilMediumImageUrl'	=> 1,
					'address'				=> 1,
					"links.members.$user"	=> 1
				]
			]
		];
		$db_tl_list = PHDB::aggregate(Organization::COLLECTION, $aggregation);
		$db_tl_list = !empty($db_tl_list['result']) ? $db_tl_list['result'] : [];
		$tmp = [];
		foreach ($db_tl_list as &$tl_item)
			$tmp[(string)$tl_item["_id"]] = $tl_item;
		$db_tl_list = &$tmp;
		unset($tmp);
		$default_organizations = Yii::app()
			->getModule('co2')
			->getAssetsUrl() . "/images/thumb/default_" . Organization::COLLECTION . ".png";
		$subventions = Aap::funding_by_tls(array_map(fn($tl) => (string)$tl['_id'], $db_tl_list), $form);
		foreach ($db_tl_list as $db_commun_item) :
			$description = substr($db_commun_item['description'] ?? '', 0, self::DESCRIPTION_LENGTH);
			$description = !empty($description) ? "$description..." : '';
			$id = (string)$db_commun_item['_id'];
			$commun = [
				'id'			=> $id,
				'slug'			=> $db_commun_item['slug'],
				'name'			=> $db_commun_item['name'],
				'className'		=> "btn-tl-link",
				'is_admin'		=> !empty($db_commun_item['links']['members'][$user]['isAdmin']) && $db_commun_item['links']['members'][$user]['isAdmin'] && (empty($db_commun_item['links']['members'][$user]['toBeValidated']) || !$db_commun_item['links']['members'][$user]['toBeValidated']),
				'url'			=> '#page.type.' . Organization::COLLECTION . '.id.' . $id,
				'description'	=> [],
				'image'			=> !empty($db_commun_item['profilMediumImageUrl']) ? $db_commun_item['profilMediumImageUrl'] : $default_organizations,
				'tl_address'	=> $db_commun_item['address']['level3'],
				"other"			=> "<small>Subvention restante : " . $subventions["campagne"][$id] . " <i class=\"fa fa-eur\"></i></small>"
			];
			$ft['proposal_list']['tladmin']['data'][] = $commun;
		endforeach;
		// Les communs que je finance
		$tl_admin_ids = array_map(fn($tl) => $tl['id'], $ft['proposal_list']['tladmin']['data']);
		$commun_where = [
			'form' => $form,
			'answers.aapStep1.titre' => ['$exists' => 1],
			'answers.aapStep1.depense.financer' => [
				'$elemMatch' => [
					'id'	=> ['$in' => $tl_admin_ids]
				]
			]
		];
		$db_commun_list = PHDB::find(Form::ANSWER_COLLECTION, $commun_where, ['answers.aapStep1']);
		$answer_id_list = array_unique(array_merge($answer_id_list, array_keys($db_commun_list)));
		$db_commun_list = array_map(fn($db_commun_item) => $db_commun_item['answers']['aapStep1'], $db_commun_list);
		$db_bills = [];
		foreach ($db_commun_list as $id => $db_commun_item) :
			$depenses = $db_commun_item['depense'];
			$i_fund = 0;
			foreach ($depenses as $depense) {
				$tl_financers = $depense['financer'] ?? [];
				$tl_financers = is_array($tl_financers) ? $tl_financers : [];
				$tl_financers = array_filter($tl_financers, fn($f) => !empty($f['id']) && in_array($f['id'], $tl_admin_ids) && !empty($f['bill']));
				foreach ($tl_financers as $tl_financer) {
					$db_bills[$tl_financer['bill']] = $tl_financer['id'];
					// if (!$i_fund && !empty($tl_financer['user']) && $tl_financer['user'] === $user)
					// 	$i_fund = 1;
				}
			}
			// if (!$i_fund)
			// 	continue;
			$description = substr($db_commun_item['description'] ?? '', 0, self::DESCRIPTION_LENGTH);
			$description = !empty($description) ? "$description..." : '';
			$commun = [
				'id'			=> $id,
				'name'			=> $db_commun_item['titre'],
				'url'			=> "$commun_page.communId.$id",
				'description'	=> [],
				'image'			=> Yii::app()
					->getModule('co2')
					->getAssetsUrl() . "/images/thumb/default_proposals.png"
			];
			$ft['proposal_list']['fund']['data'][] = $commun;
		endforeach;
		// Les réseaux de tl où je suis admin
		$tl_where = [
			'tags'				=> 'RéseauTiersLieux',
			'address.level3'	=> ['$in'	=> array_map(fn($tl) => $tl['tl_address'], $ft['proposal_list']['tladmin']['data'])],
			'$and'	=> $orga_is_admin
		];
		$aggregation = [
			['$match'	=> $tl_where],
			[
				'$group'	=> [
					'_id'	=> '$slug',
					'doc'	=> ['$first' => '$$ROOT']
				]
			],
			['$replaceRoot' => ['newRoot' => '$doc']],
			[
				'$project'	=> [
					'name'					=> 1,
					'description'			=> 1,
					'profilMediumImageUrl'	=> 1,
					'address'				=> 1,
					'slug'					=> 1
				]
			]
		];
		$db_tl_list = PHDB::aggregate(Organization::COLLECTION, $aggregation);
		$db_tl_list = !empty($db_tl_list['result']) ? $db_tl_list['result'] : [];
		$default_organizations = Yii::app()
			->getModule('co2')
			->getAssetsUrl() . "/images/thumb/default_" . Organization::COLLECTION . ".png";
		foreach ($db_tl_list as $db_commun_item) :
			$description = substr($db_commun_item['description'] ?? '', 0, self::DESCRIPTION_LENGTH);
			$description = !empty($description) ? "$description..." : '';
			$id = (string)$db_commun_item['_id'];
			$commun = [
				'id'			=> $id,
				'name'			=> $db_commun_item['name'],
				'url'			=> '#page.type.' . Organization::COLLECTION . '.id.' . $id,
				'description'	=> [],
				'slug'			=> @$db_commun_item['slug'],
				"level3"		=> @$db_commun_item['address']['level3'],
				'className'		=> "btn-network-link",
				'image'			=> !empty($db_commun_item['profilMediumImageUrl']) ? $db_commun_item['profilMediumImageUrl'] : $default_organizations
			];
			$ft['proposal_list']['tladmin']['data'] = $this->addNetworkToTL($commun, $ft['proposal_list']['tladmin']['data']);
			$ft['proposal_list']['tlnetworkadmin']['data'][] = $commun;
		endforeach;
		$db_image_where = [
			'moduleId'	=> ['$exists' => 1],
			'folder'	=> ['$exists' => 1],
			'name'		=> ['$exists' => 1],
			'type'		=> Form::ANSWER_COLLECTION,
			'id'		=> ['$in' => array_values($answer_id_list)],
			'subKey'	=> 'aapStep1.image',
			'$or'		=> [
				['doctype' => 'image'],
				['docType' => 'image']
			]
		];
		// financements effectués sous forme d'array de description
		$db_where = [
			'form'								=> $form,
			'answers.aapStep1.depense.financer'	=> [
				'$elemMatch'	=> [
					'id'	=> ['$in' => array_map(fn($item) => $item['id'], $ft['proposal_list']['tladmin']['data'])]
				]
			]
		];
		$db_answers = PHDB::find(Form::ANSWER_COLLECTION, $db_where, ['answers.aapStep1.depense']);
		foreach ($db_answers as $answer_id => $db_answer) {
			$depenses = $db_answer['answers']['aapStep1']['depense'];

			foreach ($ft['proposal_list'] as &$proposal_item) {
				if ($proposal_item['type'] !== 'proposal')
					continue;
				foreach ($proposal_item['data'] as &$proposal_data) {
					if ($answer_id !== $proposal_data['id'])
						continue;
					foreach ($depenses as $depense) {
						$financers = isset($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : [];
						$financers = array_filter($financers, function ($financer) use ($ft) {
							if (empty($financer['id']))
								return (0);
							foreach ($ft['proposal_list']['tladmin']['data'] as $tl) {
								if ($tl['id'] === $financer['id'])
									return (1);
							}
							return (0);
						});
						foreach ($financers as $financer) {
							$amount = 0;
							if (!isset($proposal_data['description'][$financer['id']]))
								$proposal_data['description'][$financer['id']] = ["pending" => 0, "paid" => 0];
							if (isset($financer['amount']))
								$amount = intval($financer['amount']);
							if (empty($financer["status"]) || $financer["status"] === "pending")
								$proposal_data['description'][$financer['id']]["pending"] += $amount;
							else
								$proposal_data['description'][$financer['id']]["paid"] += $amount;
						}
					}
				}
			}
		}
		foreach ($ft['proposal_list'] as &$proposal_item) {
			foreach ($ft['proposal_list']['tladmin']['data'] as &$tl) {
				// récupération des factures
				$bills = array_filter($db_bills, fn($b) => $b === $tl['id']);
				$bills = array_keys($bills);
				$bills = PHDB::findByIds('bills', $bills, ['created']);
				$tl['description'] = array_map(function ($b) use ($tl) {
					$name = $b['created'] ?? time();
					$name = "Facture " . date('d/m/Y H:i', $name);
					$link = Yii::app()->baseUrl . '/co2/aap/camp-tl-bill/tl/' . $tl['id'] . '/bill/' . $b['_id'];
					return ("<a class=\"btn-dash-link\" href=\"$link\"><i class=\"fa fa-file\"></i> $name</a>");
				}, $bills);
				$tl['description'] = '<p>' . implode(' ', $tl['description']) . '</p>';
				// assignation des tls aux communs
				foreach ($proposal_item['data'] as &$proposal_line) {
					if (isset($proposal_line['description'][$tl['id']])) {
						$proposal_line['description'][$tl['id']] = [
							'id'		=> $tl['id'],
							'name'		=> $tl['name'],
							'amount'	=> $proposal_line['description'][$tl['id']]
						];
					}
				}
			}
			foreach ($proposal_item['data'] as &$proposal_line) {
				$proposal_line['description'] = array_values($proposal_line['description']);
				$proposal_line['description'] = array_map(function ($desc) {
					$out = '<li><b>' .
						$desc['name'] . '</b> : ';
					if ($desc["amount"]["pending"] && $desc["amount"]["paid"])
						$out .= $desc['amount']["paid"] . ' <i class="fa fa-eur"></i> Payé / ' . $desc['amount']["pending"] . ' <i class="fa fa-eur"></i> Impayé';
					else if ($desc["amount"]["pending"])
						$out .= $desc['amount']["pending"] . ' <i class="fa fa-eur"></i> Impayé';
					else if ($desc["amount"]["paid"])
						$out .= $desc['amount']["paid"] . ' <i class="fa fa-eur"></i> Payé';
					$out .= "<br></li>";
					return ($out);
				}, $proposal_line['description']);
				if (count($proposal_line['description']))
					$proposal_line['description'] = '<ul><b>Financement par mes tiers-lieu:</b>' . implode('', $proposal_line['description']) . '</ul>';
				else
					$proposal_line['description'] = '';
			}
		}
		$db_image_list = PHDB::find(Document::COLLECTION, $db_image_where, ['id', 'moduleId', 'folder', 'name']);
		$this->filter_commun_image($db_image_list, $ft['proposal_list']['liked']['data']);
		$this->filter_commun_image($db_image_list, $ft['proposal_list']['created']['data']);
		$this->filter_commun_image($db_image_list, $ft['proposal_list']['contribute']['data']);
		$this->filter_commun_image($db_image_list, $ft['proposal_list']['fund']['data']);
		$ft['proposal_list']['tladmin']['data'] = array_filter($ft['proposal_list']['tladmin']['data'], fn($admin) => $admin['is_admin']);
		return ($this->getController()->renderPartial($view, $ft));
	}

	private function addNetworkToTL($network, $tls){
		foreach ($tls as $keyTl => $tl) {
			if($network["level3"] == $tl["tl_address"]){
				$tls[$keyTl]["network"] = $network["name"];
			}
		}
		return $tls;
	}

	private function filter_commun_image(&$document_list, &$commun_list) {
		foreach ($commun_list as $id => $commun) {
			$filtered = array_filter($document_list, fn($document_item) => $document_item['id'] === $commun['id']);
			if (count($filtered) > 0) {
				$filtered = end($filtered);
				$commun['image'] = Yii::app()->getBaseUrl() . '/upload/' . $filtered['moduleId'] . '/' . $filtered['folder'] . '/' . $filtered['name'];
			}
			$commun_list[$id] = $commun;
		}
	}
}
