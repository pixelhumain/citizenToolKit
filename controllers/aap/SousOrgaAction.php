<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;
use Form;
use PHDB;
use Rest;
use Yii;
use MongoId;
use Organization;
use Slug;
use SearchNew;
use Document;
use MongoRegex;
use Zone;

class SousOrgaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($action,$slug=null) {
    	$controller = $this->getController();
        $params= $_POST;
        $params["controller"] = $controller;

        //$action = strtolower($action);
        return self::$action($params,$slug);
    }

    private function add($params,$slug){
        $elementHaveConfigSlug = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug));
        
        $newElement = $elementHaveConfigSlug;
        $newElement["name"] = $params["name"];
        $newElement["shortDescription"] = $params["shortDescription"];
        $newElement["parentId"] = (string)$elementHaveConfigSlug["_id"];
        $newElement["parentType"] = $elementHaveConfigSlug["collection"];
        $newElement["parent"] = [
            (string)$elementHaveConfigSlug["_id"] => [
                "type" => "organizations"
            ]
        ];
        $newElement["slug"] = Slug::checkAndCreateSlug($newElement["name"]);
        $newElement["updated"] = time();
        $newElement["created"] = time();
        $newElement["creator"] = Yii::app()->session["userId"];
        unset($newElement["_id"]);
        Yii::app()->mongodb->selectCollection(Organization::COLLECTION)->insert($newElement);
        $newSlug = Slug::checkAndCreateSlug($params["name"],$newElement["collection"],(string)$newElement["_id"]);
	    Slug::save($newElement["collection"],(string)$newElement["_id"],$newSlug);

        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementHaveConfigSlug["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent"));
        if(empty($config)){
            //get parent element if this not have config
            $parentIdElementHaveConfigSlug = array_keys($elementHaveConfigSlug["parent"])[0];
            $config = PHDB::findOne(Form::COLLECTION,array("parent.".$parentIdElementHaveConfigSlug => ['$exists'=>true],"type" => "aapConfig"),array("parent"));
        }
        $formParents = PHDB::find(Form::COLLECTION,
            array(
                "parent.".(string)$elementHaveConfigSlug["_id"] => ['$exists'=>true],
                "type" => "aap",
                "config"=>(string)$config["_id"]
            )
        );
        $formParent = null;
        $formParentInputs = [];
        foreach ($formParents as $key => $value) {
            $inputs = PHDB::find(Form::INPUTS_COLLECTION,array("formParent" => $key));
            if(!empty($inputs)){
                $formParent = $value;
                $formParentInputs = $inputs;
                break;
            }
        }
        
        $newFormParent = $formParent;
        $newFormParent["name"] = $params["name"];
        $newFormParent["shortDescription"] = $params["name"];
        $newFormParent["parent"] = [
            (string)$newElement["_id"] => [
                "type" => $newElement["collection"],
                "name" => $newElement["name"]
            ]
        ];
        $newFormParent["updated"] = time();
        $newFormParent["created"] = time();
        $newFormParent["creator"] = Yii::app()->session["userId"];
        unset($newFormParent["_id"]);
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($newFormParent);
        
        foreach ($formParentInputs as $key => $value) {
            unset($formParentInputs[$key]["_id"]);
            $formParentInputs[$key]['formParent'] = (string)$newFormParent["_id"];
        }
        
        Yii::app()->mongodb->selectCollection(Form::INPUTS_COLLECTION)->batchInsert($formParentInputs);
        return Rest::json(
            array(
                "result" => true,
                "msg" => Yii::t("common","Created Successfully"),
                "data" => [
                    "formId" => (string) $newFormParent["_id"],
                    "formParentSlug" => $newSlug ,
                    "configSlug" => $slug,
                ]
            )
        );
    }

    private function globalautocompletequartier($params,$slug){
        $query = array();
        $params["isZones"] = true;
        $answers = self::globalautocompleteaction($params,$slug);

        $quarties = [];
        $counterActions = [];
        $counterCacs = [];
        foreach ($answers["results"] as $key => $value) {
            if(!empty($value["answers"]["aapStep1"]["quartiers"])){
                foreach ($value["answers"]["aapStep1"]["quartiers"] as $qt) {
                    if(!isset($counterActions[$qt])){
                        $counterActions[$qt] = [];
                        $counterActions[$qt][] = $key;
                    }else{
                        if(!in_array($key,$counterActions[$qt]))
                            $counterActions[$qt][] = $key;
                    }

                    $contextkey = array_keys($value["context"])[0];
                    //var_dump($contextkey);
                    if(!isset($counterCacs[$qt])){
                        $counterCacs[$qt]=[];
                        $counterCacs[$qt][] = $contextkey;
                    }else{
                        if(!in_array($contextkey,$counterCacs[$qt]))
                            $counterCacs[$qt][] = $contextkey;
                    }
                                        
                }
                $quarties = array_merge($quarties,$value["answers"]["aapStep1"]["quartiers"]);
            }
        } 
        //var_dump($counterCacs);exit;
        $quarties = array_unique($quarties);
        $quarties = array_values($quarties);
        $quarties = array_map(function($val){
            return new MongoId($val);
        },$quarties);

        $query = SearchNew::addQuery($query,array("_id" => ['$in' => $quarties]));
        if(!empty($params["name"])){
            $q = array("name" => new MongoRegex("/.*{$params["name"]}.*/i"));
            $query = SearchNew::addQuery( $query ,$q);
        }

        if(!empty($quarties)){
            //$quarties = PHDB::find(Zone::COLLECTION,$query);
            $quarties = PHDB::findAndFieldsAndSortAndLimitAndIndex(Zone::COLLECTION,$query,array("name"),array("updated" => -1), @$params["indexStep"], @$params["indexMin"]);
            $quariesCount = PHDB::count(Zone::COLLECTION,$query);
            foreach ($quarties as $k => $v) {
                $quarties[$k]["actions"] = $counterActions[$k];
                $quarties[$k]["cacs"] = $counterCacs[$k];
            }
        }
        //***************get actions by quartier*/

        return Rest::json(array(
            "results" => $quarties,
            "count" => [
                "answers" => $quariesCount
            ]
        ));
    }

    private function listSousOrga($params,$slug){
        $BASE_URL = Yii::app()->getRequest()->getBaseUrl(true);
        $COSTUM_HOST = isset($params["controller"]->costum["host"])? $params["controller"]->costum["host"] : "";
        $elemtsIds = [];
        $query = [];
        $elementHaveConfigSlug = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug),array("name","slug","collection"));
        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementHaveConfigSlug["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent","hiddenAap"));

        $COSTUM_BASE_URL = ((parse_url($BASE_URL, PHP_URL_HOST))==$COSTUM_HOST)?$BASE_URL:$BASE_URL."/costum/co/index/slug/".$elementHaveConfigSlug["slug"];

        $parentForms  = PHDB::find(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name"));
        
        
        if(!empty($params["filters"]["idcacs"])){
            $idCacs = explode(",",$params["filters"]["idcacs"]);
            $idCacs = array_map(function($val){
                return new MongoId($val);
            },$idCacs);
            $elemtsIds = $idCacs;
        }else{
            foreach($parentForms as $id => $val){
                if(!empty($val["parent"])){
                    $tempId = array_keys($val["parent"])[0];
                    $elemtsIds[] = new MongoId($tempId);
                }
            }
        }

        if(!empty($params["filters"]["context"])){

        }else{
            $q = array('_id' => ['$in' => $elemtsIds]);
            $query = SearchNew::addQuery( $query ,$q);
        }

        if(!empty($params["name"]))
            $q = array("name" => new MongoRegex("/.*{$params["name"]}.*/i"));
            $query = SearchNew::addQuery( $query ,$q);
            $elements = PHDB::findAndFieldsAndSortAndLimitAndIndex(Organization::COLLECTION,$query,array("name","slug","profilImageUrl","collection","shortDescription"),array("updated" => -1), $params["indexStep"], @$params["indexMin"]);
            $countElements = PHDB::count(Organization::COLLECTION,$query);
        foreach($parentForms as $id => $val){
            if(!empty($val["parent"])){
                $tempId = array_keys($val["parent"])[0];
                if(!empty($elements[$tempId])){
                    $elements[$tempId]["aapForm"] =  $id;
                }
            }
        }

        return Rest::json(
            array(
            "result" => true,
            /*"data" => [
                "parentForms" => $parentForms,
                "elements" => $elements,
                "costumUrl" => $COSTUM_BASE_URL,
                "elementHaveConfigSlug" => $elementHaveConfigSlug,
                "config" => $config,
                "cacs" => $cacs
            ],*/
            "results" => $elements,
            "count" => ["organizations" => $countElements]
        ));
    }

    private function getParentForms($slug){
        $elementHaveConfigSlug = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug),array("name","slug","collection"));
        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementHaveConfigSlug["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent","hiddenAap"));
        $parentForms  = PHDB::find(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name"));
        return $parentForms;
    }

    private function idsToMongoIds($arrayIds){
        $ids = [];
        foreach ($arrayIds as $key => $value) {
            $ids[] = new MongoId($value);
        }
        return $ids;
    }

    private function globalautocompleteaction($p,$slug){
        unset($p["controller"]);
        $p["searchType"] = $p["searchType"][0];
        $parentForms =  self::getParentForms($slug);
        $parentFormsIds = array_keys($parentForms);

        $query = array();
        $query = array("form"=> ['$in' =>$parentFormsIds],"answers.aapStep1.titre" => ['$exists' => true]);

        if(!empty($p["filters"]["context"])){
            $q = array('$or' => []);
            foreach ($p["filters"]["context"] as $kc => $vc) {
                $q['$or'][] = array("context.".$vc => ['$exists'=>true]);
            }
            $query = SearchNew::addQuery($query,$q);
        }
        if(!empty($p["filters"]["quartier"])){
            $q = array('$or' => []);
            foreach ($p["filters"]["quartier"] as $kc => $vc) {
                $q['$or'][] = array("answers.aapStep1.quartiers" => ['$in'=> $p["filters"]["quartier"]]);
            }
            $query = SearchNew::addQuery($query,$q);
        }
        //search by name
        if(!isset($p["isZones"])){ 
			if(!empty($p["name"]) && !empty($p["textPath"]))
				$query = SearchNew::searchText($p["name"], $query, array("textPath"=>$p["textPath"]));
            $res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex($p["searchType"],$query,array(),array("updated" => -1), $p["indexStep"], $p["indexMin"]);
		}else{
            $res["results"] = PHDB::find($p["searchType"],$query);
        }
        $res["query"] = $query;
        $res["params"] = $p;

        
        $res["results"] = self::prepareGlobalautocompleteActionResult($res["results"]);
        $res["count"][$p["searchType"]] = PHDB::count($p["searchType"], $query);
        return Rest::json( $res );
    }

    private function prepareGlobalautocompleteActionResult($result){
        $allImages = [
			"ansId" => [],
			"data" => []
		];

        $allContexts = [
			"contextIds" => [],
			"data" => []
		];

        foreach ($result as $key => $value) {
            $allImages["ansId"][] = $key;
            $allContexts["contextIds"][] = new MongoId(array_keys($value["context"])[0]);
        }

        $allImages["data"] = Document::getListDocumentsWhere(array(
			"id"=> ['$in'=>$allImages["ansId"]],  
			"type"=> Form::ANSWER_COLLECTION,
			"subKey"=>"aapStep1.image"), "file");

        $allContexts["data"] = PHDB::find(Organization::COLLECTION,array("_id" => ['$in' => $allContexts["contextIds"]]),array("name","slug"));

        foreach ($allImages["data"] as $kimg => $img) {
            if(!isset($result[$img["id"]]["answers"]["aapStep1"]["images"]))
                $result[$img["id"]]["answers"]["aapStep1"]["images"] = [];
            if(Document::urlExists(Yii::app()->getRequest()->getBaseUrl(true).$img["docPath"]))
                $result[$img["id"]]["answers"]["aapStep1"]["images"][] = $img["docPath"];
        }

        foreach($result as $kr => $vr){
            $cid =  array_keys($result[$kr]["context"])[0];
            //var_dump(@$allContexts["data"]);
            $result[$kr]["context"][$cid]["slug"] = $allContexts["data"][$cid]["slug"];
        }
        return $result;
    }

    private function openCacsListByQuartier($params){
        return $params["controller"]->renderPartial("costum.views.tpls.blockCms.projects.specifics.cacsList",array(
            "idquartier" => $params["idquartier"],
            "idcacs" => $params["idcacs"],
            "quartiername" => $params["quartiername"]
        ), true);
    }
    private function openActionListByCacs($params){
        return $params["controller"]->renderPartial("costum.views.tpls.blockCms.projects.specifics.cacsAction",array(
            /*"idquartier" => $params["idquartier"],
            "quartiername" => $params["quartiername"],*/
            "idcacs" => $params["idcacs"],
            "cacsname" => $params["cacsname"],
        ), true);
    }

    public function listSubOrga($params,$slug){
        $orga = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug),array("name","slug","collection"));
        $sousOrga = PHDB::find(Organization::COLLECTION,array(
            '$or' =>[
                array("parentId" => (string)$orga["_id"]),
                array("parent.".(string)$orga["_id"] => ['$exists'=> true])
            ],
            "slug" => ['$ne' => "proceco"]
        ),array("parent","slug","name","collection","profilMediumImageUrl"));

        if(!empty($sousOrga)){
            $whereForm = ['$or' => array()];
            foreach ($sousOrga as $key => $value) {
                $whereForm['$or'][] = array("parent.".$key => ['$exists'=> true],"type"=> "aap");
            }

            $formAap = PHDB::find(Form::COLLECTION,$whereForm,array("config","parent","collection","name"));

            foreach ($formAap as $kf => $vf) { 
                $parentId = array_keys($vf["parent"])[0];
                $parentType = $vf["parent"][$parentId]["type"];

                if(!isset($sousOrga[$parentId]["aap"]))
                    $sousOrga[$parentId]["aap"] = array();

                $sousOrga[$parentId]["aap"][] = array(
                    "formId" => $kf,
                    "formName" => $vf["name"],
                    "context" => $sousOrga[$parentId]["slug"],
                );
            }
        }
        return Rest::json($sousOrga);
    }

    public function listParentOrga($params,$slug){
        $orga = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug),array("name","slug","collection","parentId","profilMediumImageUrl"));
        $parentOrga = [];
        if(isset($orga["parentId"])){
            $parentOrga = PHDB::find(Organization::COLLECTION,array("_id" => new MongoId($orga["parentId"])),array("parent","slug","name","collection","profilMediumImageUrl"));
            $whereForm = ['$or' => array()];
            foreach ($parentOrga as $key => $value) {
                $whereForm['$or'][] = array("parent.".$key => ['$exists'=> true]);
            }
    
            $formAap = PHDB::find(Form::COLLECTION,$whereForm,array("config","parent","collection","name"));
    
            foreach ($formAap as $kf => $vf) {
                $parentId = array_keys($vf["parent"])[0];
                $parentType = $vf["parent"][$parentId]["type"];
    
                if(!isset($parentOrga[$parentId]["aap"]))
                    $parentOrga[$parentId]["aap"] = array();
    
                $parentOrga[$parentId]["aap"][] = array(
                    "formId" => $kf,
                    "formName" => $vf["name"],
                    "context" => $parentOrga[$parentId]["slug"]
                );
            }
        }
        return Rest::json($parentOrga);
    }
}