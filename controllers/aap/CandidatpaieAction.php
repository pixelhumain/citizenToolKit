<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use CacheHelper;
use Citoyen;
use Element;
use Form;
use Link;
use MongoDate;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;
use Yii;

class CandidatpaieAction extends  \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $user = Yii::app()->session['userId'] ?? '';
        $form = $_POST['form'] ?? '';
        $view = $_POST['view'] ?? '';
        $view = !empty($_POST['selected']) ? 'costum.views.custom.aap.genericpayment.candidat_list' : $view;
        $financer = $_POST['financer'] ?? '';
        $financer = $_POST['selected'] ?? $financer;
        $candidat = $_POST['candidat'] ?? '';
        $page = $_POST['page'] ?? '';
        $page = !empty($_POST['selected']) ? 'candidatdepense' : $page;


        if (empty($user) || empty($form))
            return (Rest::json([
                'success'	=> 0,
                'content'	=> 'Required field missing'
            ]));

        $candidatInfo = Element::getByTypeAndId(Citoyen::COLLECTION, $candidat);
        $parentForm = Element::getByTypeAndId(Form::COLLECTION, $form);

        switch ($page) {
            case 'financer' :
                $parentForm = Element::getByTypeAndId(Form::COLLECTION, $form);
                $contextId = array_key_first($parentForm["parent"]);

                //$community = Element::getCommunityByTypeAndId($parentForm["parent"][$contextId]["type"] , $contextId);
                //$organizations = Link::groupFindByType( Organization::COLLECTION,$community , ["name" , "links" , "creator"] );

                /*$myfinanceurorga = array_filter($organizations, function($item) use ($user) {
                    $ret = false;
                    if($item["creator"] == $user || !empty($item["links"]["members"][$user]["isAdmin"])){
                        $ret = true;
                    }
                    return $ret;
                });*/

                $fundings = Aap::funding_by_financer($form);
                $funding = [];
                $myfinanceurorga =  [];

                foreach ($fundings as $tl_id => $db_or) {
                    if(!empty($db_or["financeurId"])){
                        $myfin = Organization::getById($db_or["financeurId"]);
                        if(empty($myfin)){
                            $myfin = Person::getById($db_or["financeurId"]);
                        }
                        if(!empty($myfin)){
                            array_push($myfinanceurorga,$myfin);
                        }
                    }
                }

                foreach ($myfinanceurorga as $tl_id => &$db_or) {
                    // image par défaut
                    if (empty($db_or['profilImageUrl']))
                        $db_or['profilImageUrl'] = Yii::app()
                                ->getModule('co2')
                                ->getAssetsUrl() . '/images/thumb/default_organizations.png';
                    // nombre de financement
                    $funding = array_filter($fundings, fn($fun) => !empty($fun["financeurId"])
                        && $fun["financeurId"] === $tl_id
                    );
                    $funding = count($funding) ? end($funding) : [];
                }

                $args = [
                    "myfinanceurorga" => $myfinanceurorga,
                    "fundings" => $fundings,
                    "funding" => $fundings,
                    "candidatInfo" => $candidatInfo
                ];
                break;

            case 'candidatdepense':
                $fundings = Aap::funding_by_candidat($form,$financer,$candidat);
                //return Rest::json($fundings);
                $financer = PHDB::findOneById(Organization::COLLECTION, $financer, ['name']);

                $args = [
                    'financer'		=> $financer,
                    'fundings'		=> $fundings,
                    'candidatInfo' => $candidatInfo
                ];
                break;

            case 'paymenttypepage':
                $paymentMethods = [];
                if(isset($parentForm["params"]["paymentConfig"]["userconfig"][$candidat])){
                    $paymentMethods = $parentForm["params"]["paymentConfig"]["userconfig"][$candidat]["paymentMethods"];
                    $paymentMethods = array_filter($paymentconfig, function($pc) {
                        return isset($pc["type"]) ? $pc["type"] : false;
                    });
                }
                $args = [
                    'paymentMethods' => $paymentMethods,
                ];
                //return (Rest::json([$view,$args]));
                break;
        }

        function fn_financer_in_cart(array $financer, string $tl) {
            if (!empty($financer['bill']) || empty($financer['id']) || $financer['id'] !== $tl)
                return (0);
            if (!empty($financer["payment_method"]))
                return (0);
            if (!empty($financer['status']) && $financer['status'] !== 'pending')
                return (0);
            return (1);
        };

        return (Rest::json([
            'success'	=> 1,
            'content'	=> $this
                ->getController()
                ->renderPartial($view, $args)
        ]));
    }
}
