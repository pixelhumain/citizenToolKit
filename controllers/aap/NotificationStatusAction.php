<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Answer;
use Form;
use Pdf;
use PHDB;
use Yii;
use MongoId;
use Document;
use ExportToWord;
use Element;
use Aap;
use Citoyen;
use MongoDate;

class NotificationStatusAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($answerid,$notificationid=null,$template){
        $controller = $this->getController();
        $answerUserId = PHDB::findOneById(Form::ANSWER_COLLECTION,$answerid,array("user"));
        $params=[];
        $params['$addToSet'] = ["status"=>"notified"];
        if(!empty($notificationid)){
            $params['$set'] = [
                "statusInfo.".$template.".updated" => time(),
                "statusInfo.".$template.".user" => "",
            ];

            $params['$addToSet'] = [
                "status" => $template
            ];
            
        }
        PHDB::update(Form::ANSWER_COLLECTION,
            array("_id" => new MongoId($answerid)),$params);
        
        $params = [];
        $params['$pull'] = [
                "status" => $template."Sent"
            ];
        PHDB::update(Form::ANSWER_COLLECTION,
            array("_id" => new MongoId($answerid)),$params);
        
        return  Yii::app()->getRequest()->getBaseUrl(true)."/".Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg";
    }
}