<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;
use Rest;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class DirectoryProposalAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($form=null,$newcounter=false,$countonly=false){
		$newcounter = (boolean)$newcounter;
		$searchParams = $_POST;
		$answers= Aap::globalAutocompleteProposal($form, $searchParams,$newcounter, $countonly);
		return Rest::json( $answers );
	}
}
