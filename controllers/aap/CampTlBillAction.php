<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Form;
use Organization;
use Pdf;
use PHDB;

class CampTlBillAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($tl, $bill) {
		$bill_collection = 'bills';
		$pdf_arg = [
			'header'		=> false,
			'footer'		=> false,
			'textShadow'	=> false,
			'saveOption'	=> 'D',
			'docName'		=> 'Facture campagne',
		];
		$db_tls = PHDB::findByIds(Organization::COLLECTION, [$tl], ['name', 'profilMediumImageUrl']);
		$db_bill = PHDB::findOneById($bill_collection, $bill);
		$created = $db_bill['created'] ?? time();
		$file_args = [
			'created_at'	=> date('d/m/Y', $created),
			'image'			=> $db_tls[$tl]['profilMediumImageUrl'] ?? '',
			'number'		=> PHDB::count($bill_collection, ['created' => ['$lt' => $created]]),
			'name'			=> $db_bill['name'],
			'address'		=> $db_bill['address'],
			'lines'			=> [],
		];
		$pdf_arg['docName'] .= ' ' . $db_tls[$tl]['name'];
		$db_where = [
			'answers.aapStep1.depense.financer'	=> [
				'$elemMatch'	=> [
					'bill'	=> $bill
				]
			]
		];
		$db_communs = PHDB::find(Form::ANSWER_COLLECTION, $db_where, ['answers.aapStep1.depense']);
		foreach ($db_communs as $db_commun) {
			$depenses = array_filter($db_commun['answers']['aapStep1']['depense'], function ($depense) use ($bill) {
				$financers = $depense['financer'] ?? [];
				$financers = array_filter($financers, fn($financer) => !empty($financer['bill']) && $financer['bill'] === $bill);
				return (count($financers));
			});
			$depenses = array_values($depenses);
			foreach ($depenses as $depense) {
				$one = [
					'line'		=> $depense['poste'],
					'amount'	=> 0,
					'double'	=> 0,
				];
				$financers = array_filter($depense['financer'], fn($financer) => !empty($financer['bill']) && $financer['bill'] === $bill);
				$financers = array_values($financers);
				foreach ($financers as $financer) {
					if ($financer['id'] === $tl)
						$one['amount'] += $financer['amount'];
					else
						$one['double'] += $financer['amount'];
				}
				$file_args['lines'][] = $one;
			}
		}
		$pdf_arg['html'] = $this->getController()->renderPartial('costum.views.custom.aap.cart.pdf_bill', $file_args);
		return (Pdf::createPdf($pdf_arg));
	}
}
