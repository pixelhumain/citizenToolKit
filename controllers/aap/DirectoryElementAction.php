<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;
use Rest;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class DirectoryElementAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($form=null,$newcounter=false,$elttype=null){
		$newcounter = (boolean)$newcounter;
		$searchParams = $_POST;
		$answers= Aap::globalAutocompleteElement($form, $searchParams,$newcounter, $elttype);
		return Rest::json( $answers );
	}
}
