<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Document;
use Form;
use PHDB;
use Rest;

class DepenseImageAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		if (empty($_POST["keys"]))
			return (Rest::json([
				"success"	=> 0,
				"content"	=> "Nothing to request"
			]));
		$db_where = [
			"answers.aapStep1.depense"	=> [
				'$elemMatch'	=> [
					"imageKey"	=> ['$in' => $_POST["keys"]]
				]
			]
		];
		$db_answers = PHDB::find(Form::ANSWER_COLLECTION, $db_where, ["_id"]);
		$db_answers = array_keys($db_answers);
		$db_files = Document::getListDocumentsWhere(array(
			"id"		=> ['$in'		=> $db_answers],
			"imageKey"	=> ['$exists'	=> 1],
			"type"		=> 'answers'
		), "image");
		$out = [];
		foreach ($db_files as $db_file)
			$out[$db_file["imageKey"]] = $db_file["imagePath"];
		return (Rest::json([
			"success"	=> 1,
			"content"	=> $out
		]));
	}
}
