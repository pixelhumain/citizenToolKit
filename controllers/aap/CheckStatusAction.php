<?php
	
	namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;
	
	class CheckStatusAction extends \PixelHumain\PixelHumain\components\Action {
		public function run($bill) {
			$db_bill = \PHDB::findOneById(\Bill::COLLECTION, $bill);
			$db_answers = \PHDB::findByIds(\Form::ANSWER_COLLECTION, array_map(fn($c) => $c["ans"], $_POST["communs"]), ["answers.aapStep1.depense"]);
			$pending_count = 0;
			foreach ($db_answers as $db_answer) {
				$depense = $db_answer["answers"]["aapStep1"]["depense"];
				
				foreach ($depense as $d) {
					$financer = isset($d['financer']) && is_array($d["financer"]) ? $d["financer"] : [];
					$financer = array_filter($financer, fn($f) => !empty($f["id"]) && $f["id"] === $db_bill["parent"]["id"] && !empty($f["status"]) && $f["status"] === "pending");
					$pending_count += count($financer);
				}
			}
			return (\Rest::json([
				"success"	=>	true,
				"content" => $pending_count > 0 ? "pending" : "paid",
			]));
		}
	}
