<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;
use Yii;

class TlFundHtmlAction extends  \PixelHumain\PixelHumain\components\Action {
	public function run($tl, $form) {
		$user = Yii::app()->session['userId'] ?? '';
		if (empty($user))
			return (Rest::json([
				'success'	=> 0,
				'content'	=> 'You are not allowed to do this action'
			]));
		$db_tls = PHDB::findByIds(Organization::COLLECTION, [$tl], ['name']);
		$fundings = Aap::funding_by_tls($db_tls, $form);
		$tl = [
			'id' => $tl,
			'name' => $db_tls[$tl]['name']
		];
		$args = [
			'fundings' => $fundings,
			'tl' => $tl,
			"unique_tl" => isset($_POST["unique_tl"]) ? $_POST["unique_tl"] : false,
			"camp_conf" => Aap::get_campagne_config($form),
			"can_checkout" => Aap::camp_can_open_cart($form),
		];
		return (Rest::json([
			'success'	=> 1,
			'content'	=> $this->getController()->renderPartial('costum.views.custom.aap.cart.commun_list', $args)
		]));
	}
}
