<?php
    
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Action;
use MongoId;
    use PixelHumain\PixelHumain\components\Action as CoAction;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    use Project;
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
    use Rest;
    use PHDB;
    use Yii;
    
    class ProjectAction extends CoAction {
        public function run($request = '') {
            switch ($request) {
                case 'project_detail':
                    $parameter = [
                        'readonly_content'    => true,
                        'show_filter'         => false,
                        'content_type'        => 'projects',
                        'default_filter_keys' => [$_POST['project_id']],
                        'predefinedColor'     => [
                            'background' => '#9BC125',
                            'foreground' => 'white'
                        ],
                        'activeSearchFilter'  => true
                    ];
                    if (!empty($_POST['user'])) $parameter['memberFilters'] = [$_POST['user']];
                    if (!empty($_POST['name'])) $parameter['contentName'] = $_POST['name'];
                    $contributorsList = [];
                    $dbActionsList = PHDB::find(\Action::COLLECTION, ['parentId' => $_POST['project_id']], ['links.contributors']);
                    foreach ($dbActionsList as $dbAction) {
                        $actionContributorsList = $dbAction['links']['contributors'] ?? [];
                        foreach ($actionContributorsList as $contributorId => $contributor) {
                            if (!empty($contributor['type']) && $contributor['type'] === \Person::COLLECTION) {
                                UtilsHelper::push_array_if_not_exists($contributorId, $contributorsList);
                            }
                        }
                    }
                    $dbContributorsList = PHDB::findByIds(\Person::COLLECTION, $contributorsList, ['name', 'profilImageUrl']);
                    $contributorsList = [];
                    foreach ($dbContributorsList as $contributorId => $dbContributor) {
                        $contributorsList[$contributorId] = [
                            'name'  => $dbContributor['name'],
                            'image' => $dbContributor['profilImageUrl'] ?? Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumbnail-default.jpg'
                        ];
                    }
                    $html = $this->getController()->renderPartial('costum.views.tpls.blockCms.events.generic_agenda', $parameter);
                    $response = [
                        'inputFilter'  => $_POST['name'] ?? '',
                        'html'         => $html,
                        'contributors' => $contributorsList
                    ];
                    $output = Rest::json($response);
                    break;
                case 'get_one_project':
                    $project = [];
                    $project["results"] = PHDB::find(Project::COLLECTION, array("_id" => new MongoId($_POST['id'])));
                    
                    $project['data'] = Aap::parse_project_data($project);
                    $output = Rest::json($project);
                    break;
                case 'action_count':
                    $status = 'totest';
                    $output = Rest::json(Action::countByStatus(['parentId' => $_POST['project'], 'parentType' => Project::COLLECTION], $status));
                    break;
                default:
                    $output = Rest::json(['status' => false, 'msg' => 'Bad request']);
                    break;
            }
            return $output;
        }
    }