<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Form;
use MongoId;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Ramsey\Uuid\Uuid;
use Rest;
use RocketChat;
use Yii;

class FundingAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($request = '') {
		switch ($request) {
			case "delete":
				$requireds = ["form", "communs"];
				foreach ($requireds as $field) {
					if (!isset($_POST[$field]))
						return (Rest::json([
							"success"	=> 0,
							"content"	=> "Required field is missing"
						]));
				}
				$communs = $_POST["communs"];
				$db_where = [
					"form"	=> $_POST["form"],
					"_id"	=> ['$in'	=> array_map(fn($c) => new MongoId($c["ans"]), $communs)]
				];
				$db_communs = PHDB::find(Form::ANSWER_COLLECTION, $db_where, ["answers.aapStep1.depense"]);
				$db_communs = array_combine(
					array_keys($db_communs),
					array_values(array_map(fn($c) => $c["answers"]["aapStep1"]["depense"], $db_communs))
				);
				$messages = [];
				foreach ($communs as $commun) {
					$ans = $commun["ans"];
					$depenses = &$db_communs[$ans];
					if (!isset($messages[$commun["tlname"]]))
						$messages[$commun["tlname"]] = [];
					$messages[$commun["tlname"]][] = "**". $commun["name"] ."**";
					foreach ($depenses as &$depense) {
						$financers = isset($depense["financer"]) && is_array($depense["financer"]) ? $depense["financer"] : [];
						$financers = array_filter($financers, function ($f) use ($commun) {
							$tl = $commun["tl"];
							$is_paid = !empty($f["payment_method"]) || (!empty($f["status"]) && $f["status"] === "paid");
							$is_tl = !empty($f["id"]) && $f["id"] === $tl;
							$is_ftl = !empty($f["tlid"]) && $f["tlid"] === $tl;

							if ($is_paid)
								return (1);
							if ($is_tl && !$is_paid)
								return (0);
							if ($is_ftl && !$is_paid)
								return (0);
							return (1);
						});
						$financers = array_values($financers);
						if (empty($financers))
							unset($depense["financer"]);
						else
							$depense["financer"] = $financers;
					}
					// Test inside modal tib
					PHDB::update(Form::ANSWER_COLLECTION, ["_id" => new MongoId($ans)], [
						'$set' => [
							"answers.aapStep1.depense"	=> $depenses
						]
					]);
				}
				$message = array_keys($messages);
				$message = "**".end($message)."**";
				$message .= " a supprimé ses lignes de financement dans : ". implode(" , ", end($messages)) . " .";
				// RocketChat::post($_POST["costumSlug"], $message, "", true);
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $db_communs
				]));
				break;
			case 'cart':
				$output = [
					'success'	=> 1,
					'content'	=> null
				];
				$form = $_POST['form'] ?? '';
				if (empty($form))
					return (Rest::json([
						'success'	=> 0,
						'content'	=> 'Empty form id ' . $form
					]));
				$user = Yii::app()->session['userId'] ?? '';
				if (empty($user))
					return (Rest::json([
						'success'	=> 0,
						'content'	=> 'You are not allowed to do this action'
					]));
				// tl where I'm admin
				$db_tls = Organization::get_tls(['name'], $user, 1);
				$db_orgas = Organization::get_nontls(["name"], $user, 1);
				$db_tls = array_merge($db_tls, $db_orgas);
				$output['content'] = Aap::funding_by_tls($db_tls, $form);
				return (Rest::json($output));
				break;
			case 'camp_bank_payment':
				$output = [
					'success'	=> 1,
					'content'	=> null
				];
				$required = ["communs", "infos"];
				foreach ($required as $require) {
					if (empty($_POST[$require]))
						return (Rest::json([
							"success"	=> 0,
							"content"	=> "Missing required field ($require)"
						]));
				}
				$funds = $_POST['communs'] ?? [];
				$bill_info = !empty($_POST['infos']) && is_array($_POST['infos']) ? $_POST['infos'] : [];
				if (empty($funds))
					return (Rest::json([
						'success'	=> 0,
						'content'	=> 'Empty values'
					]));
				// Answers for tls
				$db_answers = PHDB::findByIds(Form::ANSWER_COLLECTION, array_map(fn($map) => $map['ans'], $funds), ['answers.aapStep1.depense', 'context']);
				$depenses = array_combine(
					array_keys($db_answers),
					array_values(array_map(fn($map) => $map['answers']['aapStep1']['depense'], $db_answers))
				);
				foreach ($funds as $fund) {
					$ans_depense = &$depenses[$fund['ans']];
					foreach ($ans_depense as &$depense) {
						$financers = isset($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : [];
						foreach ($financers as &$financer) {
							// sauter si pas d'id
							if (empty($financer['id']))
								continue;
							// sauter si déjà payé
							if (!empty($financer['status']) && $financer['status'] === 'paid')
								continue;
							// parser la valeur à avoir un int
							if (!isset($financer['amount']))
								$financer['amount'] = 0;
							else
								$financer['amount'] = floatval($financer['amount']);
							if ($financer['id'] === $fund['tl']) {
								$financer['amount'] = floatval($financer['amount']);
								$financer['status'] = 'pending';
								$financer["payment_method"] = "bank";
								$financer['bill'] = $bill_info['id'];
								if (!empty($financer["finkey"])) {
									foreach ($financers as &$ftl) {
										if (!empty($ftl["finkey"]) && $ftl["finkey"] === $financer["finkey"]) {
											$ftl["status"] = "pending";
											$ftl["payment_method"] = "bank";
											$ftl["bill"] = $bill_info['id'];
										}
									}
								}
							}
						}
						if (empty($financers))
							unset($depense['financer']);
						else
							$depense['financer'] = $financers;
					}
				}
				foreach ($depenses as $ans => $depense)
					PHDB::update(Form::ANSWER_COLLECTION, ['_id' => new MongoId($ans)], ['$set'	=> ['answers.aapStep1.depense' => array_values($depense)]]);
				$output['content'] = $depenses;
				return (Rest::json($output));
				break;
			case 'camp_double':
				$output = [
					'success'	=> 1,
					'content'	=> null
				];
				$required = ["form", "communs", "infos", "method"];
				foreach ($required as $require) {
					if (empty($_POST[$require]))
						return (Rest::json([
							'success'	=> 0,
							'content'	=> "Missing required field ($require)"
						]));
				}
				$funds = $_POST['communs'];
				$form = $_POST['form'];
				$bill_info = $_POST['infos'];
				$payment_method = $_POST["method"];
				$status = isset($_POST["wait_for_validation"]) && intval($_POST["wait_for_validation"]) ? "pending" : "paid";
				// Answers for tls
				$db_answers = PHDB::findByIds(Form::ANSWER_COLLECTION, array_map(fn($map) => $map['ans'], $funds), ['answers.aapStep1.depense', 'context']);
				$depenses = array_combine(
					array_keys($db_answers),
					array_values(array_map(fn($map) => $map['answers']['aapStep1']['depense'], $db_answers))
				);
				foreach ($funds as $fund) {
					$ans_depense = &$depenses[$fund['ans']];
					foreach ($ans_depense as &$depense) {
						$financers = isset($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : [];
						foreach ($financers as &$financer) {
							// sauter si pas d'id
							if (empty($financer['id']))
								continue;
							// sauter si déjà payé
							if (!empty($financer['status']) && $financer['status'] === 'paid')
								continue;
							if (!isset($financer['amount']))
								$financer['amount'] = 0;
							else
								$financer['amount'] = floatval($financer['amount']);
							if ($financer['id'] === $fund['tl']) {
								$financer["status"] = $status;
								$financer["payment_method"] = $payment_method;
								$financer["bill"] = $bill_info['id'];
								foreach ($financers as &$ftl) {
									if (!empty($ftl["finkey"]) && $ftl["finkey"] === $financer["finkey"]) {
										$ftl["status"] = $status;
										$ftl["payment_method"] = $payment_method;
										$ftl["bill"] = $bill_info['id'];
									}
								}
								unset($ftl);
							}
						}
						unset($financer);
						if (empty($financers))
							unset($depense['financer']);
						else
							$depense['financer'] = $financers;
					}
					unset($depense);
				}
				foreach ($depenses as $ans => $depense)
					PHDB::update(Form::ANSWER_COLLECTION, ['_id' => new MongoId($ans)], [
						'$set'	=> [
							'answers.aapStep1.depense' => array_values($depense)
						]
					]);
				$output['content'] = $depenses;
				return (Rest::json($output));
				break;
			default:
				return (Rest::json([
					'success'	=> 0,
					'content'	=> 'Undefined request'
				]));
				break;
		}
	}

	public static function req_validate_fund(array $communs, string $form, array $bill, string $method, $wait = 0): array {
		// $url = "http://localhost:5080";
		$url = Yii::app()->getBaseUrl(1);
		$url .= "/co2/aap/funding/request/camp_double";
		$payload = [
			"communs"				=> $communs,
			"form"					=> $form,
			"infos"					=> $bill,
			"method"				=> $method,
			"wait_for_validation"	=> $wait
		];
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/x-www-form-urlencoded',
			"accept: application/json"
		]);
		$resp = json_decode(curl_exec($ch), 1);
		$err = curl_error($ch);
		$info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if (!empty($err) || $info !== 200)
			return ([
				"success"	=> 0,
				"content"	=> "$err status code $info"
			]);
		return ($resp);
	}
}
