<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Organization;
use PHDB;
use Rest;

class CartBillInfoHtmlAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		$tl = $_POST['tl'] ?? '';
		if (empty($tl))
			return (Rest::json([
				'success'	=> 0,
				'content'	=> 'Required field is missing'
			]));
		$db_tl = PHDB::findOneById(Organization::COLLECTION, $tl, ['name', 'email', 'address']);
		$args = [
			'tl'	=> [
				'name'		=> $db_tl['name'],
				'email'		=> $db_tl['email'] ?? '',
				'address'	=> ''
			]
		];
		$address = ['streetAddress', 'postalCode', 'addressLocality', 'addressCountry'];
		foreach ($address as $field) {
			if (!empty($db_tl['address'][$field]))
				$args['tl']['address'] .= $db_tl['address'][$field] . ' ';
		}
		$args['tl']['address'] = trim($args['tl']['address']);
		return (Rest::json([
			'success'	=> 1,
			'content'	=> $this->getController()->renderPartial('costum.views.custom.aap.cart.bill_info', $args)
		]));
	}
}
