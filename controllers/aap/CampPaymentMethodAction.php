<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use CacheHelper;
use Form;
use PaymentMethod;
use PHDB;
use Rest;
use Slug;

class CampPaymentMethodAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($method) {
		$cnt = $this->getController();
		switch ($method) {
			case 'get':
				$required = ['form'];
				foreach ($required as $req) {
					if (!array_key_exists($req, $_POST))
						return (Rest::json([
							'success'	=> 0,
							'content'	=> 'Missing reuquired fields'
						]));
				}
				$db_config = Form::getConfig($_POST['form']);
				$db_campagne = Slug::getBySlug($_POST['costumSlug']);
				$db_campagne = PHDB::findOneById($db_campagne['type'], $db_campagne['id'], ['costum.campagne']);
				$db_campagne = $db_campagne['costum']['campagne'] ?? '';
				$db_campagne = $db_config['campagne'][$db_campagne] ?? [];

				$costum = CacheHelper::getCostum(null, null, $_POST["costumSlug"]);
				$paymentMethods = PaymentMethod::getByElement(["id" => $costum["contextId"], "type" => $costum["contextType"]], "helloasso");
				$args = [
					'iban'		=> '',
					'config'	=> $db_campagne,
					'paymentMethods'	=> $paymentMethods
				];
				return (Rest::json([
					'success'	=> 1,
					'content'	=> $cnt->renderPartial('costum.views.custom.aap.cart.payment_method', $args)
				]));
				break;
			case 'post':

				break;
			default:
				return (Rest::json([
					'success'	=> 0,
					'content'	=> 'Unrecognized method'
				]));
				break;
		}
	}
}
