<?php
    
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Form;
use Person;
use PHDB;
use Rest;

class RefreshAnswerAction extends \PixelHumain\PixelHumain\components\Action 
{
    public function run() {
        if ( Person::logguedAndValid() ) {
            isset($_POST["id"]) ? $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$_POST["id"],["answers", "context", "created", "user", "status", "statusInfo", 'updated']) : "";
            return Rest::json($answer);
        }
        return Rest::json("You are not logged");
    }
}