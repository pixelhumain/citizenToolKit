<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Answer;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Rest;
use SearchNew;
use Yii;

class ChangeSessionAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $res = array();
        if(!empty($_POST["formid"]) && !empty($_POST["year"])){
            Yii::app()->session["aapSession-".$_POST["formid"]] = $_POST["year"];
            $res = array(
                "session" => $_POST["year"],
                "form" =>$_POST["formid"],
                "key" =>"aapSession-".$_POST["formid"],  
                "msg" => "Session changé en ".$_POST["year"]
            );
        }
        return Rest::json($res);
    }
}
