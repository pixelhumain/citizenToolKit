<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use CacheHelper;
use Citoyen;
use Element;
use Form;
use Link;
use MongoDate;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;
use Yii;

class ConfigPaymentAction extends  \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $debug = false;
        //$debug = true;
        $return = [];
        $result = [];
        $action = $_POST["action"];
        //return (Rest::json($_POST));
        unset($_POST["action"]);

        if($action == "favorite") {
            $result = Form::savePayementFavorite($_POST["type"]);
        }elseif($action == "save"){
            $result = Form::savePayementPartialConfig($_POST);
        }elseif($action == "delete"){
            $result = Form::deletePayementConfig($_POST["type"]);
        }
        $return = Form::getPayementPartialConfig();
        $return["result"] = $result;

        return (Rest::json($return));
    }

    
}
