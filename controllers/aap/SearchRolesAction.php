<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Form;
use MongoId;
use MongoRegex;
use Organization;
use PHDB;
use Rest;
use SearchNew;

class SearchRolesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($q,$contextId,$contextType,$key="tags") {
        $controller=$this->getController();
        $regex = SearchNew::accentToRegex($q);
        $element = PHDB::findOneById($contextType,$contextId,array("links.members","links.contributors"));
        $contextLink = "members";
        if($contextType=== "Projects")
            $contextLink = "contributors";
        $roles = [];
        foreach($element["links"][$contextLink] as $k=> $vl){
            if(!empty($vl["roles"])){
                $roles = array_merge($roles,$vl["roles"]);
            }
        }
        $roles = array_unique($roles);
        $filteredRoles =[];
        foreach ($roles as $kr => $vr) {
            if(strpos(strtolower($vr), strtolower($q)) !== false)
                $filteredRoles[] = ['tag'=>$vr];
        }
        return Rest::json($filteredRoles);
    }
}