<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Rest;

class AacAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($method){
        $controller = $this->getController();
        $params = $_POST;
        $params["controller"] = $controller;
        return self::$method($params);
    }

    private function aac_directory($params) {
        $queryParams = Aap::aacQuery($params);
        $query = $queryParams['query'];
        $searchParams = $queryParams['searchParams'];

        $res = [];
        $res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex(
            $searchParams["searchType"][0],
            $query,
            $searchParams["fields"],
            $searchParams['sortBy'],
            $searchParams["indexStep"],
            $searchParams["indexMin"]
        );

        if (isset($searchParams["count"])) {
            $res["count"][$searchParams["searchType"][0]] = PHDB::count($searchParams["searchType"][0], $query);
        }

        return Rest::json(Aap::parseAacData($res));
    }

    private function communs_directory($params) {
        $queryParams = Aap::communsQuery($params);
        $query = $queryParams['query'];
        $searchParams = $queryParams['searchParams'];

        $res = [];
        $res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex(
            $searchParams["searchType"][0],
            $query,
            $searchParams["fields"],
            $searchParams['sortBy'],
            $searchParams["indexStep"],
            $searchParams["indexMin"]
        );

        if (isset($searchParams["count"])) {
            $res["count"][$searchParams["searchType"][0]] = PHDB::count($searchParams["searchType"][0], $query);
        }

        Return Rest::json(Aap::parseCommunData($res));
    }

    private function aac_globalautocomplete($params) {
        $params["filters"]["aapType"][] = "aac";
        return Rest::json(Aap::globaleAutocompleteMyAac($params));
    }
}