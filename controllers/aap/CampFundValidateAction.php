<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Bill;
use CacheHelper;
use Form;
use MongoId;
use Organization;
use PaymentMethod;
use PHDB;
use Rest;
use Stripe\Checkout\Session as StrSess;
use Stripe\Exception\SignatureVerificationException;
use Stripe\Stripe;
use Stripe\Webhook;
use UnexpectedValueException;
use Yii;

class CampFundValidateAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($webhook = 0, $method = 'stripe') {
		switch ($method) {
			case 'stripe':
				return ($this->stripe_payment($webhook));
				break;
			case 'bank':
				return ($this->bank_payment());
				break;
		}
	}

	private function bank_payment() {
		$costum = CacheHelper::getCostum(null, null, $_POST["costumSlug"]);
		$required = ["form", "communs", "infos", "method"];
		foreach ($required as $require) {
			if (empty($_POST[$require]))
				return (Rest::json([
					'success'	=> 0,
					'content'	=> "Missing required field ($require)"
				]));
		}
		$db_info = $_POST['infos'];
		$db_communs = $_POST["communs"];
		$db_info['parent'] = [
			'type'	=> Organization::COLLECTION,
			'id'	=> $_POST['tl']
		];
		$db_info["context"] = [
			"id"	=> $costum["contextId"],
			"type"	=> $costum["contextType"]
		];
		$db_info['created'] = time();
		Bill::create($db_info);
		$db_info["id"] = (string) $db_info['_id'];
		$resp = FundingAction::req_validate_fund(
			$db_communs,
			$_POST["form"],
			$db_info,
			$_POST["method"],
			1
		);
		if (!$resp["success"])
			return (Rest::json($resp));
		return (Rest::json([
			'success'	=> 1,
			'content'	=> $this->getController()->renderPartial('costum.views.custom.aap.cart.bank_pay', [
				'communs'	=> $db_communs,
				'bill'		=> $db_info
			])
		]));
	}

	private function stripe_payment($webhook = 0) {
		if ($webhook) {
			$php_input = file_get_contents('php://input');
			$inputs = json_decode($php_input, 1);
			if ($inputs["type"] === "checkout.session.completed") {
				$bill = json_decode($inputs["data"]["object"]["metadata"]["bill"], 1);
				$paymentMethod = PaymentMethod::getByElementAndType(
					[
						"id"	=> $bill["context"]["id"],
						"type"	=> $bill["context"]["type"]
					],
					"stripe"
				);
			}
		} else {
			$costum = CacheHelper::getCostum(null, null, $_POST["costumSlug"]);
			$paymentMethod = PaymentMethod::getByElementAndType(["id" => $costum["contextId"], "type" => $costum["contextType"]], "stripe");
		}
		if (!empty($paymentMethod)) {
			Stripe::setApiKey($paymentMethod['config']["secretKey"]);
			if ($webhook) {
				$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
				try {
					$event = Webhook::constructEvent($php_input, $sig_header, $paymentMethod['config']["webhookKey"]);
					if ($event->type == 'checkout.session.completed') {
						$session = $event->data->object; // Le contenu de checkout.session.completed
						$meta = $session->metadata;
						$validation = FundingAction::req_validate_fund(
							json_decode($meta["communs"], 1),
							$meta["form"],
							json_decode($meta["bill"], 1),
							"stripe"
						);
						if ($validation["success"])
							return (self::card_payment_socket('credit-card-pay-success', $validation["content"]));
						else
							return (self::card_payment_socket('credit-card-pay-error', $validation["content"]));
					};
				} catch (UnexpectedValueException $e) {
					return (self::card_payment_socket('credit-card-pay-error', $e));
				} catch (SignatureVerificationException $e) {
					return (self::card_payment_socket('credit-card-pay-error', $e));
				}
				return (Rest::json(false));
			}
			$required = ['tl', 'communs', "form", "infos"];
			foreach ($required as $require) {
				if (!isset($_POST[$require]))
					return (Rest::json([
						'success'	=> 0,
						'content'	=> "Missing required field ($require)"
					]));
			}
			$output = [
				'success'	=> 1,
				'content'	=> null
			];
			$db_info = $_POST['infos'];
			$db_info['parent'] = [
				'type'	=> Organization::COLLECTION,
				'id'	=> $_POST['tl']
			];
			$db_info["context"] = [
				"id" => $costum["contextId"],
				"type" => $costum["contextType"]
			];
			$db_info['created'] = time();
			Bill::create($db_info);
			$db_info["id"] = (string) $db_info['_id'];
			// $db_tl = PHDB::findOneById(Organization::COLLECTION, $_POST['tl'], ['name']);
			$checkout_session = StrSess::create([
				'mode'			=> 'payment',
				'success_url'	=> Yii::app()->getBaseUrl(1) . "/co2/helloAsso/return",
				'line_items'	=> [
					array_map(fn($commun) => [
						'quantity'		=> 1,
						'price_data'	=> [
							'currency'		=> 'EUR',
							'unit_amount'	=> $commun['amount'] * 100,
							'product_data'	=> [
								'name'	=> $commun['name']
							]
						]
					], $_POST['communs'])
				],
				"metadata"		=> [
					"form"		=> $_POST["form"],
					"communs"	=> json_encode($_POST["communs"]),
					"bill"		=> json_encode($db_info),
				],
			]);
			$args = [
				'link' => [
					'tl'	=> $_POST['tl']
				]
			];
			$output['content'] = [
				'url'		=> $checkout_session->url
			];
			if (!empty($db_info['id'])) {
				$output['content']['bill_id'] = $db_info['id'];
				$args['link']['bill'] = $db_info['id'];
			}
			$output['content']['render'] = $this->getController()->renderPartial('costum.views.custom.aap.cart.cart_processing', $args);
			return (Rest::json($output));
		} else {
			return (Rest::json([
				'success'	=> 0,
				'content'	=> 'No payment method found'
			]));
		}
	}

	public static function card_payment_socket($event, $data) {
		$coWs = Yii::app()->params['cows'];

		if (empty($coWs['pingStripeCheckout']))
			return (false);
		$payload = [
			'event'	=> $event,
			'data'	=> $data
		];
		$curl = curl_init($coWs['pingStripeCheckout']);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		return (curl_exec($curl));
	}
}
