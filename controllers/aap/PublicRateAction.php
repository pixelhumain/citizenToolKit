<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap;

use Form;
use MongoId;
use MongoDate;
use Person;
use PHDB;
use Rest;
use Yii;

class PublicRateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        if(Person::logguedAndValid()){
            $me = Yii::app()->session['userId'];
            $updatedElem = [];
            if(!empty($_POST["id"]) && !empty($_POST["value"])){
                if($_POST["value"] == "set"){
                    PHDB::update( Form::ANSWER_COLLECTION,
                        [ "_id" => new MongoId($_POST["id"]) ], 
                        [ '$set' => [ "vote.".$me => array("status" => "love","date" => new MongoDate(time())) ] ]
                    );
                }elseif ($_POST["value"] == "unset") {
                    PHDB::update( Form::ANSWER_COLLECTION,
                        [ "_id" => new MongoId($_POST["id"]) ], 
                        [ '$unset' => [ "vote.".$me => true] ]
                    );
                }
                $updatedElem = PHDB::findOneById( Form::ANSWER_COLLECTION, $_POST["id"], ["vote"]);
                if (!empty($updatedElem)) {
                    $loveCount = 0;
                    foreach ($updatedElem["vote"] as $key => $value) {
                        if($value["status"] == "love") {
                            $loveCount++;
                        }
                    }
                    PHDB::update( Form::ANSWER_COLLECTION,
                        [ "_id" => new MongoId($_POST["id"]) ], 
                        [ '$set' => [ "allVoteCount.love" => $loveCount ] ]
                    );
                }

                $msg=Yii::t("common","Everything is allRight");
                return Rest::json(array("result"=>true, "msg"=>$msg,"params"=> $_POST["value"]));
            }
        }else
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Please Login First")));
    }
}