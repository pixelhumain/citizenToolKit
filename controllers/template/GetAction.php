<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\template;

use CAction, Element, Cms, PHDB, MongoId, Yii, Rest, CacheHelper, Template, Document, Authorisation, Costum, DataValidator;
class GetAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id=null){
    	if(!empty($id)){
    		$res=PHDB::findOne(Template::COLLECTION, array("_id"=>new MongoId($id)));
            $res["images"] = Document::getListDocumentsWhere(
                array("id"=>$id, "type"=>Template::COLLECTION, "doctype"=>"image", 
                    "contentKey"=>"profil", "current"=>array('$exists'=>true)
                ), "image");
        	$res["tplAlbum"] = Document::getListDocumentsWhere(
	            array( "id"=> $id,
	                "type"=>Template::COLLECTION,
	                "subKey"=>"tplAlbum",
	      		), "image"
	        );
        	if (isset($res["path"]))
        	    $res["usesTpl"] = PHDB::find(Cms::COLLECTION, array("path"=> $res["path"]));

        	// $tplsJson = PHDB::find("versionning", ['type' => "tplJson"]);
        	// $res = array("result"=>true, "items" => $tplsJson);
        	if (isset($res["siteParams"]["app"])) {
        		foreach ($res["siteParams"]["app"] as $keyApp => $valueApp) {        			
        			$tplsJson = PHDB::find("versionning", ["pages.".trim($keyApp, "#").".templateOrigin" => $id]);
        			$res["tplJson"] = $tplsJson;
        		}
        	}


    		return Rest::json($res);
    	}
    }
}