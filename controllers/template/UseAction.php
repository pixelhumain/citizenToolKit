<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\template;
use CAction, Element, Cms, PHDB, MongoId, Yii, Rest, CacheHelper, Template, Document, Authorisation, Costum, DataValidator,Log,Person;
class UseAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id=null){
    	$controller=$this->getController();
        $params = DataValidator::clearUserInput($_POST);
        $template = Element::getElementById($_POST["id"],Template::COLLECTION);
        // $contextCostum = CacheHelper::getCostum();
        // $allCmsId = isset($template["cmsList"]) ? $template["cmsList"] : [];

        // $cms = PHDB::findByIds("templates",$allCmsId);

        // echo "<pre>";
        // var_dump($cms);
        // echo "</pre>";exit();

        // $contextCostum = [];
        // if(isset($_POST['newCostum'])){
        //     $contextCostum = [
        //         "contextType" => $_POST["parentType"],
        //         "contextId" => $_POST["parentId"],
        //         "app" => []
        //     ];
        // }else{
        //     $contextCostum = CacheHelper::getCostum();
        // }
        // $tplSelectedType = $template["subtype"] ?? "page";
        /**Duplicate block**/

            // var_dump(Person::logguedAndAuthorized());exit();
        if(Yii::app()->session["userIsAdmin"] || Authorisation::isInterfaceAdmin()){
            // Save action Logs
            Cms::useTemplate($template,$params);
            // $dataSend =  array(
            //     "userId"         =>  Yii::app()->session["userId"],
            //     "userName"       =>  Yii::app()->session["user"]["name"],
            //     "costumId"       =>  @$_POST["parentId"],
            //     "costumSlug"     =>  @$_POST["parentSlug"],
            //     "costumType"     =>  @$_POST["parentType"],
            //     "costumEditMode" =>  @$_POST["costumEditMode"],
            //     "browser"        =>  @$_SERVER["HTTP_USER_AGENT"],
            //     "ipAddress"      =>  @$_SERVER["REMOTE_ADDR"],
            //     "created"        =>  time(),
            //     "action"         => "costum/addBlock",
            //     "blockPath"      =>  @$template["path"],
            //     "blockName"      =>  @$template["name"] ?? substr($template["path"], strrpos($template["path"], '.') + 1),
            //     "collection"     =>  @$template["collection"],
            //     "page"           =>  str_replace('#', '', $_POST["page"]),
            //     "type"           =>  @$template["type"],
            //     "idBlock"        =>  (string)$template["_id"],
            //     "subtype"        =>  @$template["subtype"]
            // );

            // /* WE HAVE 2 TYPE OF TEMPLATES. FOR WHOLE COSTUM (type site) AND DEDICATED FOR ONE "Page" */
            // // DUPLICATE ALL PAGE FROM TEMPLATE AND CHANGE WHOLE COSTUM CONFIG ACCORDIND TO TEMPLATE IF IT IS A SITE
            // if (!empty($template["siteParams"])) { 
            //     $template["siteParams"]["app"] = array_unique(array_merge($template["siteParams"]["app"], @$contextCostum["app"]), SORT_REGULAR); 
            //     if (!isset($_POST["action"]) || empty($_POST["action"])) { 
            //     // If action is empty string, it means THE TEMPLATE CHOOSED IS NEVER USED, then we duplicate all blocks
            //         Cms::duplicateBlock($allCmsId, array(
            //             "parentId"   => $_POST['parentId'], 
            //             "parentType" => $_POST["parentType"],
            //             "parentSlug" => $_POST['parentSlug'],
            //             "useTemplate"=>true
            //         ),null,false);
            //     // NOW, WE INCREMENT THE NUMBRE OF TEMPLATES USER
            //         Element:: updatepathvalue(Template::COLLECTION,$tplSelectedId,"userCounter", isset($template["userCounter"]) ? $template["userCounter"]+1 : 1);
            //     }elseif ($_POST["action"] == "using") {
            //         // If action is using, THE TEMPLATE CHOOSED IS USING (Bouton reinitialiser est cliqué), Then we reset the template as default. 
            //         // Remove the old block from each page then duplicate the template again 
            //         // $cmsEachPage = array();
            //         // $allCmsToReset = array();
            //         $i = 0;
            //         foreach (array_keys($template["siteParams"]["app"]) as $pageHash) {                 
            //             PHDB::remove("cms",[
            //                 "parent.".$contextCostum["contextId"] => array('$exists'=>1),
            //                 "page" => trim($pageHash, "#")]
            //             );
            //             $i++;
            //             if (count(array_keys($template["siteParams"]["app"])) == $i) {                                    
            //                 // Reduplicate the Template
            //                 Cms::duplicateBlock($allCmsId, array(
            //                     "parentId"   => $_POST['parentId'], 
            //                     "parentType" => $_POST["parentType"],
            //                     "parentSlug" => $_POST['parentSlug'],
            //                     "useTemplate"=>true
            //                 ),null,false);
            //             }

            //         }
            //     }
            //     foreach ($template["siteParams"]["app"] as $keyPage => $Page) {
            //         $template["siteParams"]["app"][$keyPage]["templateOrigin"] = $tplSelectedId;
            //     }              
            //     foreach (array_keys($template["siteParams"]) as $key => $value) {
            //         Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.".$value, $template["siteParams"][$value]);
            //     }
            //     Costum::forceCacheReset();
            // // JUST DUPLICATE THE BLOCK FROM ONE PAGE IF TEMPLATE IS DEDICATED FOR ONE PAGE 
            // }else{     
            //     Cms::duplicateBlock($allCmsId, array(
            //         "parentId"   => $_POST['parentId'], 
            //         "parentType" => $_POST["parentType"],
            //         "parentSlug" => $_POST['parentSlug'],
            //         "page"       => trim($_POST["page"], "#"),
            //         "useTemplate"=>true),null,false);

            //     $dataSend["page"] =  str_replace('#', '', $_POST["page"]);
            // }

            // if ($template["type"] == "page") {
            //     Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.app.".$_POST['page'].".templateOrigin", $tplSelectedId);
            // }

            // Log::saveAndClean(
            //     $dataSend
            // );

            // THEN SAVE THE TEMPLATES INTO COSTUM "used" AND "using" 
            /*if (!empty($contextCostum["tplUsed"])) {
                foreach ($contextCostum["tplUsed"] as $tplUsedId => $valueTplUsed) {  
                    if ($tplUsedId != $tplSelectedId) {
                        Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplUsedId, "used");
                    }else{
                        Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplUsedId, "using");
                    }                
                }
                if (!in_array($tplSelectedId,$contextCostum["tplUsed"])) {
                    Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplSelectedId, "using");
                }
            }else{             
                Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplSelectedId, "using");
            }*/
            Costum::forceCacheReset();
            $res = array("result" => true, "msg" => Yii::t("common", "Template has been well activated"));   
        }else
        $res = array("result" => false, "msg" => Yii::t("common", "You are not allowed to do this action"));
        return Rest::json($res);
    }
}