<?php
    
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\migration;

use Cms;
use MongoId;
use PHDB;
use Rest;
use Yii;

class BlockCMSAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $cms = PHDB::findOne(Cms::COLLECTION, array('type' => 'blockCms', 'name' => 'Super bloc'));
        $page = trim($_POST['page'], "#");
        $parentCMS = array($_POST['costumId'] => array("type" => $_POST["costumType"], "name" => $_POST['costumSlug']));
        $allId = array();
        $allChildId = array();
        $parentID = "";
        foreach ($_POST['css'] as $key => $value) {
            $id = new \MongoDB\BSON\ObjectID();
            $arrayInsertSuperCms = array(
                "page" => $page,
                "type" => "blockChild",
                "parent" => $parentCMS,
                "creator" => $cms["creator"],
                "collection" => "cms",
                "subtype" => "supercms",
                "_id" => $id,
                "order" => (isset($value['ordre']) ? $value['ordre'] : 0),
                "class" => [
                    "width" => "sp-cms-std"
                ],
            );
            if(strpos($key, 'sp-text') > -1){
                $arrayInsertSuperCms['path'] = "tpls.blockCms.superCms.elements.supertext";
                $arrayInsertSuperCms['name'] = "Super text";
                $arrayInsertSuperCms['text'] = $value['text'];
            }else if(strpos($key, 'img') > -1){
                $arrayInsertSuperCms['path'] = "tpls.blockCms.superCms.elements.image";
                $arrayInsertSuperCms['name'] = "Super Image";
                $arrayInsertSuperCms["imagePath"] = $value["src"];
            }else{
                $arrayInsertSuperCms['name'] = $cms["name"];
                $arrayInsertSuperCms["path"] = "tpls.blockCms.superCms.container";
            }
            if(isset($value['css'])){
                if(isset($value['css']['class']['other'])){
                    $arrayInsertSuperCms['class']['other'] = $value['css']['class']['other'];
                    unset($value['css']['class']);
                }
                $arrayInsertSuperCms['css'] = $value['css'];
            }
            if(isset($value['parent'])){
                $arrayInsertSuperCms["blockParent"] = $this->getParent($allId,$value['parent']);
                array_push($allChildId, array("parent" => $value['parent'],"ordre" => (isset($value['ordre']) ? $value['ordre'] : 0), '_id' => (String)$id));
                $this->updateCMSList($allChildId, $arrayInsertSuperCms["blockParent"], $value['parent']);
            }else{
                $parentID = (string) $id;
            }
            Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsertSuperCms);
            array_push($allId, array("name" => $key,"ordre" => (isset($value['ordre']) ? $value['ordre'] : 0), '_id' => (String)$id));
        }
        return Rest::json($parentID);
    }

    private function getParent($allParents, $child)
    {
        $parent = "";
        foreach ($allParents as $key => $value) {
            if($value['name'] == $child){
                $parent = $value['_id'];
            }
        }
        return $parent;
    }

    private function updateCMSList($allChild,$id, $parent)
    {
        $childs = [];
        foreach ($allChild as $key => $value) {
            if($value['parent'] == $parent){
               $childs[intval($value['ordre'])] = $value['_id'];
            }
        }
        PHDB::update(Cms::COLLECTION,["_id" => new MongoId($value['_id'])],['$set'=> [
            "cmsList" => $childs,
        ]]);
    }
}
