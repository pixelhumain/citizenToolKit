<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding;

use CAction, Authorisation, Rest, Crowdfunding, Yii;
class ValidatePledgeAction extends \PixelHumain\PixelHumain\components\Action {
	
	public function run($type, $id) {
		$controller=$this->getController();
        
        if ( ! Authorisation::isInterfaceAdmin()) {
            return Rest::json(array( "result" => false, "msg" => "You must be a super admin to delete something" ));
            return;
        }
       
        $res = Crowdfunding::validatePledge($id, Yii::app()->session["userId"]);
        return Rest::json($res);

	}
}