<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding;
use Crowdfunding;
use Rest;
class getCampaignAndCountersByParentAction extends \PixelHumain\PixelHumain\components\Action {
	
	public function run() {
		if (isset($_POST["results"])){
            foreach ($_POST["results"] as $key=>$value){
                           $camp=Crowdfunding::getCampaignAndCountersByParentId($key);
                           //var_dump($camp);
                           $camp["idCamp"] = (string)$camp["_id"];
                            unset($camp["_id"]);
                            $camp["nameCamp"] = isset($camp["name"]) ? $camp["name"] : null;
                            unset($camp["name"]);
                            $camp["descriptionCamp"]= isset($camp["description"]) ? $camp["description"] : null;
                            unset($camp["description"]);
                            $_POST["results"][$key] = array_merge($_POST["results"][$key],$camp);                 
                    }
            
            $res = $_POST;
            return Rest::json($res);
        }    

	}
}