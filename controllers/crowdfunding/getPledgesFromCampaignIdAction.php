<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding;

use Crowdfunding;
use Rest;

class getPledgesFromCampaignIdAction extends \PixelHumain\PixelHumain\components\Action {
	
	public function run($type, $id) {
		$controller=$this->getController();
        
        
       
        $res = Crowdfunding::getPledgesFromCampaignId($id);
        return Rest::json($res);

	}
}