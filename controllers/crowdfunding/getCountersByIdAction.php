<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding;
use Crowdfunding;
use Rest;

class getCountersByIdAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type, $id) {
        $controller = $this->getController();


        $res = Crowdfunding::getCounters($id);
        return Rest::json($res);

    }
}