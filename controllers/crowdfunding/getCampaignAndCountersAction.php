<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding;

use CAction, Crowdfunding, Rest;
class getCampaignAndCountersAction extends \PixelHumain\PixelHumain\components\Action {
	
	public function run() {
		if (isset($_POST["results"])){
            foreach ($_POST["results"] as $key=>$value){
                        $camp=Crowdfunding::getCampaignById($key);
                        //var_dump($camp);exit;   
                        $_POST["results"][$key] = $camp;               
                    }
            
            $res = $_POST;
            return Rest::json($res);
        }    

	}
}