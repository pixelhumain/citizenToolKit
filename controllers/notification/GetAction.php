<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\notification;
use ActivityStream;
use ActivityStreamReference;
use ActStr;
use CAction;
use MongoDate;
use Notification;
use Person;
use Rest;
use Translate;
use Yii;

/**
 * a notification has been read by a user
 * remove it's entry in the notify node on an activity Stream for the current user
 * @return [json] 
 */
class GetAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run($type, $id){
      $res = array(); $datas = array();
      if(Yii::app()->session["userId"]){
        if($type != Person::COLLECTION){
          $params = [
            '$and'=>[
              [
                "userId"=>Yii::app()->session["userId"],
                "verb"=>[
                  '$ne'=>ActStr::VERB_ASK
                ],
                "type"=>[
                  '$ne'=>"oceco"
                ]
              ],
              [
                '$or'=>[
                  [
                    "targetId"=>$id,
                    "targetType"=>$type
                  ],
                  [
                    "targetParentId"=>$id,
                    "targetParentType"=>$type
                  ]
                ]
              ]
            ]   
          ];
        }else{
          $params = [
            "userId"=>Yii::app()->session["userId"],
            'type'=>['$ne'=> "oceco"]
          ];
        }

        if(!empty($_POST["refreshTimestamp"])){
          $params = array_merge(
            $params,
            [
              'updated'=>[
                '$gt' => new MongoDate($_POST["refreshTimestamp"])
              ]
            ]
          );
          $res = ActivityStreamReference::getNotificationsByTimeLimit($params);
        }else
          $res = ActivityStreamReference::getNotificationsByStep($params, $_POST["indexMin"] ?? 0);
        if(!empty($res)){
          $timezone="";
            foreach($res as $key => $data){
              if(@$data["notify"]["labelAuthorObject"] || @$data["notify"]["labelArray"]){
                if(@$data["notify"]["labelAuthorObject"] && $data["notify"]["labelAuthorObject"]=="mentions")
                  $res[$key]["notify"]["displayName"]=Notification::translateMentions($data);
                else
                  $res[$key]["notify"]["displayName"]=Notification::translateLabel($data);
              }
              $res[$key]["timeAgo"]=Translate::pastTime(date(@$data["updated"]->sec), "timestamp", $timezone);
              $res[$key]["timestamp"]=$data["updated"]->sec;
            } 
        }

        //$data["notif"] = $res;
        $datas["notif"] = $res;
        if(@$_POST["refreshTimestamp"]){
            //$datas["countNotif"] = ActivityStream::countUnseenNotifications(Yii::app()->session["userId"], $type, $id);
            $datas["countNotif"] = ActivityStreamReference::countUnseenNotifications(Yii::app()->session["userId"], $type, $id);
        }
        //$datas["coop"] = Cooperation::getCountNotif();

      }else
        $datas = array('result' => false , 'msg'=>'something somewhere went terribly wrong');
      return Rest::json($datas,false);
    }
}