<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action;

use Action;
use Lists;
use MongoId;
use PHDB;
use PixelHumain\PixelHumain\modules\costum\controllers\actions\project\ActionAction;
use Rest;

class ReorderAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($action, $after = null) {
		$ids = [$action];
		if (!empty($after))
			$ids[] = $after;
		$db_action = PHDB::findOneById(Action::COLLECTION, $action, ["parent", "parentId", "parentType", "tags", "status", "tracking"]);
		$parent_id = !empty($db_action["parentId"]) ? $db_action["parentId"] : null;
		$parent_type = !empty($db_action["parentType"]) ? $db_action["parentType"] : null;
		if ((empty($parent_id) || empty($parent_type)) && !empty($db_action["parent"])) {
			$first = array_keys($db_action["parent"])[0];
			$parent_id = $first;
			$parent_type = $db_action["parent"][$first]["type"];
		}
		$db_list = PHDB::findOne(Lists::COLLECTION, ["parent.$parent_id.type" => $parent_type, "type" => Action::COLLECTION], ["list"]);
		$list = $db_list["list"] ?? [];
		// créer la liste
		$mongo_ids = array_keys($list);
		$mongo_ids = array_filter($mongo_ids, fn($k) => (!empty($k)));
		$mongo_ids = array_map(fn($id) => new MongoId($id), $mongo_ids);
		$db_actions = Action::selectOrderedByStatus([
			"id"	=>	$parent_id,
			"type"	=>	$parent_type
		], [
			"_id"	=>	[
				'$nin'	=>	$mongo_ids
				]
			]);
		$length = count($db_actions);
		for ($i = 0; $i < $length - 1; $i++) {
			$key0 = (string)$db_actions[$i]["_id"];
			$key1 = (string)$db_actions[$i + 1]["_id"];
			if (empty($list[$key0]))
				$list[$key0] = $key1;
			if (empty($list[$key1]))
				$list[$key1] = null;
		}
		$old_before = array_search($action, $list);
		// rejoindre les deux anciens bouts
		$duplicate_null = array_keys(array_filter($list, fn($l) => $l === null));
		for ($i = 0; $i < (count($duplicate_null) - 1); $i++)
			$list[$duplicate_null[$i]] = $duplicate_null[$i + 1];
		$old_after = $list[$action] ?? null;
		if (!empty($old_before))
			$list[$old_before] = $old_after;
		// intégrer dans la nouvelle liste
		if (empty($after)) {
			$null_index = array_search(null, $list);
			while (array_search($null_index, $list) !== false)
				$null_index = array_search($null_index, $list);
			if (!empty($null_index) && $null_index != $action)
				$list[$action] = $null_index;
		} else {
			$after_before = array_search($after, $list);
			if (!empty($after_before))
				$list[$after_before] = $action;
			$list[$action] = $after;
		}

		if (empty($db_list)) {
			$create_list = [
				"type"		=>	Action::COLLECTION,
				"name"		=>	"",
				"parent"	=>	[
					$parent_id	=> ["type"	=>	$parent_type]
				],
				"list"		=>	$list
			];
			PHDB::insert(Lists::COLLECTION, $create_list);
		} else {
			PHDB::update(Lists::COLLECTION, ["_id" => $db_list["_id"]], ['$set' => [
				"list" => $list
			]]);
		}
		$socket = [
			"moved"		=>	$action,
			"group"		=>	$_POST['group'],
			"after"		=>	$after === "null" ? null : $after,
			"emiter"	=>	$_POST["emiter"],
			"component"	=>	$_POST["component"],
		];
		ActionAction::emitActionEvent(".action-order", $socket);
		return (Rest::json($list));
	}
}
