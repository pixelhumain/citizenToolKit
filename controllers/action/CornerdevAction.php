<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action;

use Action;
use Lists;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Project;
use Rest;

class CornerdevAction extends  \PixelHumain\PixelHumain\components\Action {
	public function run($method) {
		switch ($method) {
			case "url-exists":
				if (empty($_POST["url"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Url is empty"
					]));
				$db_where = [
					'urls'		=> $_POST["url"],
					'status'	=> [
						'$nin'	=> [
							'disabled',
							'closed',
						]
					]
				];
				return (Rest::json([
					"success"	=> 1,
					"content"	=> PHDB::count(Action::COLLECTION, $db_where)
				]));
				break;
			case "get-by-url":
				if (empty($_POST["url"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Url is empty"
					]));
				$db_where = [
					"tracking"		=> true,
					"tags"			=> [
						'$nin'	=> [
							"totest",
							'next',
						]
					],
					"status"		=> "todo",
					'urls'			=> $_POST['url']
				];
				if (is_array($_POST['url']))
					$db_where['urls'] = ['$in' => $_POST['url']];
				if (!empty($_POST["status"])) {
					switch ($_POST['status']) {
						case 'done':
							$db_where['status'] = 'done';
							unset($db_where['tracking']);
							break;
						case 'totest':
							$db_where['status'] = 'todo';
							$db_where['tags'] = 'totest';
							unset($db_where['tracking']);
							break;
						case 'todo':
							$db_where['status'] = 'todo';
							$db_where['$or'] = [
								['tracking'	=> false],
								['tracking'	=> ['$exists' => 0]],
							];
							unset($db_where['tracking']);
							break;
						case 'discuter':
							$db_where['status'] = 'todo';
							$db_where['tags'] = 'discuter';
							unset($db_where['tracking']);
							break;
					}
				}
				if (!empty($_POST['project']))
					$db_where = array_merge($db_where, [
						"parentId"		=> $_POST["project"],
						"parentType"	=> Project::COLLECTION,
					]);
				$db_actions = PHDB::find(Action::COLLECTION, $db_where);
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $db_actions,
				]));
				break;
			case "get-by-costum":
				if (empty($_POST['costumSlug']))
					return (Rest::json([
						'success'	=> false,
						'content'	=> 'Missing costum slug'
					]));
				$db_where = [
					'$and'		=> [
						[
							'$or'	=> [
								['source.key'	=> $_POST['costumSlug']],
								['urls'			=> UtilsHelper::mongo_regex('/slug/'.$_POST['costumSlug'])]
							],
						],
						[
							'$or'	=> [
								['created'		=> ['$exists'	=> 1]],
								['startDate'	=> ['$exists'	=> 1]],
							],
						]
					],
					'status'	=> [
						'$nin'	=> ['closed', 'disabled']
					],
					'name'		=> ['$exists'	=> 1]
				];
				$db_actions = PHDB::findAndSort(Action::COLLECTION, $db_where, ['startDate' => 1, 'created' => 1]);
				return (Rest::json([
					'success'	=> true,
					'content'	=> $db_actions
				]));
				break;
				case 'get-action':
				if (empty($_POST["action"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Action id is empty"
					]));
				return (Rest::json([
					'success'	=> 1,
					'content'	=> PHDB::findOneById(Action::COLLECTION, $_POST["action"]),
				]));
				break;
			default:
				return (Rest::json([
					"success"	=> 0,
					"content"	=> "Bad request"
				]));
				break;
		}
	}
}
