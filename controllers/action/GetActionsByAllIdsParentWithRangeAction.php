<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action;

use Actions;
use Action;
use Lists;
use PHDB;
use Project;
use Rest;
use DateTime;
use MongoDB\BSON;
use MongoDB\BSON\UTCDateTime;
use MongoId;
use Event;


class GetActionsByAllIdsParentWithRangeAction extends  \PixelHumain\PixelHumain\components\Action {
    public function run(){
        ini_set('memory_limit', '20M');
        set_time_limit(5000);  

        $range = isset($_POST["range"]) ? $_POST["range"] : "-7";
        $data = getAllActionsCreatedWithStatusForUsers($_POST["parentType"],$_POST["costumId"],$range);
        return Rest::json([
            'data' => $data,
        ]);
        
    }
}


/**
 * Renvoie les actions créées avec un statut "done" ou "todo" et 
 * créées ou mises à jour dans les 7 derniers jours pour chaque utilisateur 
 * dans le tableau $userIds.
 * 
 * Les résultats sont stockés dans un tableau associatif avec les clés 
 * correspondant aux IDs des utilisateurs.
 * 
 * @param array $userIds Les IDs des utilisateurs pour lesquels on souhaite 
 *                        récupérer les actions.
 * 
 * @return array Un tableau associatif contenant les résultats pour chaque 
 *               utilisateur.
 */
function getAllActionsCreatedWithStatusForUsers($parentTypeToFind, $contextId,$range) {

    $currentDate = new DateTime();
    $daysAgo = (clone $currentDate)->modify('-' . $range . ' days');
    
    $startOfPeriod = new UTCDateTime($daysAgo->getTimestamp() * 1000);

    $startOfPeriodTimestamp = $daysAgo->getTimestamp();
    $timestampAujourdhui = $currentDate->getTimestamp();

    $projectsChild = PHDB::aggregate(
        Project::COLLECTION,
        [
            [
                '$match' => [
                    "parent.{$contextId}" => ['$exists' => true]
                ]
            ],
            [
                '$project' => [
                    '_id' => 1 // Inclure uniquement l'_id du projet
                ]
            ],
            [
                '$group' => [
                    '_id' => null, // Aucun regroupement spécifique, tous les projets ensemble
                    'ids' => ['$push' => '$_id'] // Crée un tableau avec tous les _id des projets
                ]
                ],
                [
                    '$project' => [
                        'ids' => [
                            '$map' => [
                                'input' => '$ids', // L'array d'_id
                                'as' => 'id', // Alias pour chaque élément de l'array
                                'in' => [
                                    '$toString' => '$$id' // Convertir chaque _id en string
                                ]
                            ]
                        ]
                    ]
                ]
        ]
    );
    
    $idsProject = [];
    if (isset($projectsChild["result"][0]["ids"])) $idsProject = $projectsChild["result"][0]["ids"];

    $eventsChild = PHDB::aggregate(
        Event::COLLECTION,
        [
            [
                '$match' => [
                    "organizerId" => ['$in' => $idsProject]
                ]
            ],
            [
                '$project' => [
                    '_id' => 1 // Inclure uniquement l'_id du projet
                ]
            ],
            [
                '$group' => [
                    '_id' => null, // Aucun regroupement spécifique, tous les projets ensemble
                    'ids' => ['$push' => '$_id'] // Crée un tableau avec tous les _id des projets
                ]
                ],
                [
                    '$project' => [
                        'ids' => [
                            '$map' => [
                                'input' => '$ids', // L'array d'_id
                                'as' => 'id', // Alias pour chaque élément de l'array
                                'in' => [
                                    '$toString' => '$$id' // Convertir chaque _id en string
                                ]
                            ]
                        ]
                    ]
                ]
        ]
    );

    $idsEvents = [];
    if (isset($eventsChild["result"][0]["ids"])) $idsEvents = $eventsChild["result"][0]["ids"];
    
    $projectsChild = $idsProject;
    $eventsChild = $idsEvents;
    $projectsAndEventChild = array_merge($idsProject, $idsEvents);
    array_unshift($projectsAndEventChild, $contextId);

    $activities = PHDB::aggregate(
        Actions::COLLECTION,
        [
            [
                '$match' => [
                    'parentId' => ['$in' => $projectsAndEventChild ],
                    '$or' => [
                        ['startDate' => ['$gte' => $startOfPeriod]],
                        ['endDate' => ['$gte' => $startOfPeriod]],
                        [
                            'created' => [
                                '$gte' => $startOfPeriodTimestamp,
                                '$lte' => $timestampAujourdhui
                            ],
                        ]
                    ]
                ]
            ],
            [
                '$group' => [
                    '_id' => '$idUserAuthor',
                    'actions' => [
                        '$push' => [
                            '_id' => '$_id',
                            'status' => '$status',
                            'name' => '$name',
                            'createdAt' => '$createdAt',
                            'updatedAt' => '$updatedAt',
                            'parentId' => '$parentId',
                            'authorId' => '$idUserAuthor',
                            'parentType' => '$parentType',
                            'tracking' => '$tracking',
                            'tags' => '$tags',  
                            'tasks' => '$tasks'
                        ]
                    ],
                    'totalCount' => [
                        '$sum' => 1
                    ],
                    'doneCount' => [
                        '$sum' => ['$cond' => [['$eq' => ['$status', 'done']], 1, 0]]
                    ],
                    'distinctParentIds' => [
                        '$addToSet' => [
                            'parentId' => '$parentId',
                            'parentType' => '$parentType'
                        ]
                    ],
                ]

            ],
            [
                '$project' => [
                    '_id' => 0,
                    'userId' => '$_id',
                    'actions' => 1,
                    'totalCount' => 1,
                    'doneCount' => 1,
                    'distinctParentIds' => 1
                ]
            ]
        ]
    );
    
    if (empty($activities['result'])) {
        return [];
    } else {
        $result = [];
        $allProject = [];
        foreach ($activities['result'] as $activity) {
            $parentDetails = [];
            $activityPerProject = [];
            foreach ($activity['actions'] as $action) {

                if(!isset($activityPerProject[$action['parentId']])){
                    $activityPerProject[$action['parentId']] = [];
                }
                $activityPerProject[$action['parentId']][] = $action;

                $parent = PHDB::findOne($action['parentType'],['_id' => new MongoId($action['parentId'])]);
                if ($parent) {
                    $parentData = [
                        'id' => $action['parentId'],
                        'parentType' => $action['parentType'],
                        'name' => $parent['name'],
                        'slug' => $parent['slug'],
                        'profilImageUrl' => $parent['profilImageUrl'] ?? null
                    ];
        
                    if (!isset($allProject[$action['parentId']])) {
                        $allProject[$action['parentId']] = $parentData;
        
                        $doneCount = PHDB::count(Actions::COLLECTION, ["status" => "done", "parentId" => $action['parentId']]);
                        $allCount = PHDB::count(Actions::COLLECTION, ["parentId" => $action['parentId']]);
                        $allProject[$action['parentId']]['progression'] = $allCount > 0 ? round($doneCount / $allCount * 100) : 0;
                    }
        
                    $parentDetails[] = $parentData;
                }

            }
        
            $result[] = [
                'userId' => $activity['userId'],
                'totalCount' => $activity['totalCount'],
                'doneCount' => $activity['doneCount'],
                'distinctParentIds' => $parentDetails,
                'actions' => $activity['actions'],
                'actionsPerProject' => $activityPerProject,
            ];
        }
        
        $allProjectClassed = [
            "projects" => [],
            "events" => [],
            "organizations" => [],
            "others" => []
        ];
        
        foreach ($allProject as $project) {
            if (isset($project['parentType']) && isset($allProjectClassed[$project['parentType']])) {
                $allProjectClassed[$project['parentType']][] = $project;
            } else {
                $allProjectClassed["others"][] = $project;
            }
        };


        return [
            'allActions' => $result,
            'allProjects' => $allProjectClassed, 
        ];
        
    }
}



