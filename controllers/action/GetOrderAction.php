<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action;

use Lists;
use PHDB;
use Rest;

class GetOrderAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($parent, $type) {
		$db_list = PHDB::findOne(Lists::COLLECTION, ["parent.$parent.type" => $type], ["list"]);
		$attr_list = $db_list["list"] ?? [];
		$list = [];
		$search = null;
		while (($search = array_search($search, $attr_list)) !== false) {
			array_unshift($list, $search);
		}
		return Rest::json($list);
	}
}
