<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action;

use Actions;
use Action;
use Lists;
use PHDB;
use Project;
use Rest;
use DateTime;
use MongoId;
use Event;


class GetByPoleAndParentIdAction extends  \PixelHumain\PixelHumain\components\Action {
    public function run(){
        $data = getAllProjectsByPole($_POST["parentId"],$_POST["pole"]);
        return Rest::json([
            'data' => $data,
        ]); 
    }
}


function getAllProjectsByPole($parentId,$pole) {

    $projectsChild = PHDB::find(Project::COLLECTION,["parent.{$parentId}" => ['$exists' => true], "tags" => $pole],["name", "slug", "profilImageUrl"]);

    if ( !empty($projectsChild) ) {
        $allProject = $projectsChild;
    } else {
        $allProject = [];
    }

    foreach($allProject as $key => $project) {
        $allActions = PHDB::findAndSortAndLimitAndIndex(Actions::COLLECTION , ['parentId' => $key ], array("startDate" => 1), 100, 0);
        $allProject[$key]["allActions"] = $allActions;

        $allEvents = PHDB::find(Event::COLLECTION , ["organizer." . $key => ['$exists' => true]],["name", "slug", "profilImageUrl"]);
        $allProject[$key]["allEvents"] = $allEvents;
    }
    
    return [
        'allProjects' => $allProject,
    ];

}


