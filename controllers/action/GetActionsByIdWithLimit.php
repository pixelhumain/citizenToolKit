<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action;

use Actions;
use Action;
use Lists;
use PHDB;
use Project;
use Rest;
use DateTime;
use MongoId;
use Event;


class GetActionsByIdWithLimit extends  \PixelHumain\PixelHumain\components\Action {
    public function run(){

        if(isset($_POST["parentId"]) && isset($_POST["limit"])){
            $allActions = PHDB::findAndSortAndLimitAndIndex(Actions::COLLECTION , ['parentId' => $_POST["parentId"] ], array("created" => 1), $_POST["limit"], 0);
            return Rest::json([
                'data' => $allActions,
            ]);
        } else {
            return "Error: missing parameters";
        }
    }
}


