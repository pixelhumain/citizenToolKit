<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action;

use Actions;
use Lists;
use PHDB;
use Project;
use Rest;
use DateTime;
use MongoDB\BSON;
use MongoDB\BSON\UTCDateTime;
use MongoId;
use Event;


class GetActionsByIdParentWithRangeAction extends  \PixelHumain\PixelHumain\components\Action {
    public function run(){
        // $parentType = !empty($_POST["parentType"]) ? $_POST["parentType"] : ["organizations","events","projects"];
        $range = isset($_POST["range"]) ? $_POST["range"] : "-7";
        $totest = isset($_POST["status"]) ? $_POST["status"] : "false";
        $data = getAllActionsById($_POST["parentId"],$range,$totest);
        return Rest::json([
            'data' => $data,
        ]);
        
    }
}


function getAllActionsById($parentIdToFind,$range,$totest) {
    $currentDate = new DateTime();
    $daysAgo = (clone $currentDate)->modify('-' . $range . ' days');
    
    $startOfPeriod = new UTCDateTime($daysAgo->getTimestamp() * 1000);

    $startOfPeriodTimestamp = $daysAgo->getTimestamp();
    $timestampAujourdhui = $currentDate->getTimestamp();
    
    $matchCriteria = [
        'parentId' => $parentIdToFind,
        '$or' => [
            ['startDate' => ['$gte' => $startOfPeriod]],
            ['endDate' => ['$gte' => $startOfPeriod]],
            [
                'created' => [
                    '$gte' => $startOfPeriodTimestamp,
                    '$lte' => $timestampAujourdhui
                ],
            ]
        ]
    ];

    if ($totest === "true") {
        $matchCriteria['tags'] = 'totest';
    }

    $activities = PHDB::aggregate(
        Actions::COLLECTION,
        [
            ['$match' => $matchCriteria],
            [
                '$group' => [
                    '_id' => '$idUserAuthor',
                    'actions' => [
                        '$push' => [
                            '_id' => '$_id',
                            'status' => '$status',
                            'name' => '$name',
                            'createdAt' => '$createdAt',
                            'updatedAt' => '$updatedAt',
                            'parentId' => '$parentId',
                            'authorId' => '$idUserAuthor',
                            'parentType' => '$parentType',
                            'tasks' => '$tasks',
                            'tags' => '$tags'
                        ]
                    ],
                    'totalCount' => [
                        '$sum' => 1
                    ],
                    'doneCount' => [
                        '$sum' => ['$cond' => [['$eq' => ['$status', 'done']], 1, 0]]
                    ],
                    'distinctParentIds' => [
                        '$addToSet' => [
                            'parentId' => '$parentId',
                            'parentType' => '$parentType'
                        ]
                    ],
                ]

            ],
            [
                '$project' => [
                    '_id' => 0, 
                    'userId' => '$_id',
                    'actions' => 1,
                    'totalCount' => 1,
                    'doneCount' => 1,
                    'distinctParentIds' => 1
                ]
            ]
        ]
    );

    if (empty($activities['result'])) {
        return [];
    } else {
        $result = [];
        $allProject = [];
        foreach ($activities['result'] as $activity) {
            $parentDetails = [];
            $activityPerProject = [];
            foreach ($activity['distinctParentIds'] as $parent) {
                $parentId = $parent['parentId'];
                $parentType = $parent['parentType'];
                $parent = PHDB::findOne( $parentType , ['_id' => new MongoId($parentId)]);
                if ($parent) {
                    $parentData = [
                        'parentId' => $parentId,
                        'parentType' => $parentType,
                        'name' => $parent['name'],
                        'profilImageUrl' => $parent['profilImageUrl'] ?? null
                    ];
        
                    $existsInAllProjects = array_filter(
                        $allProject,
                        fn($proj) => $proj['parentId'] === $parentId
                    );
                    if (empty($existsInAllProjects)) {
                        $allProject[] = $parentData;
                    }
        
                    $parentDetails[] = $parentData;
                    $activityPerProject[$parentData['parentId']] = [];
                }
            }

            foreach ($activity['actions'] as $actions) {
                $activityPerProject[$actions['parentId']][] = $actions;
            }

            $result[] = [
                'userId' => $activity['userId'],
                'totalCount' => $activity['totalCount'],
                    'doneCount' => $activity['doneCount'],
                'distinctParentIds' => $parentDetails,
                'actions' => $activity['actions'],
                'actionsPerProject' => $activityPerProject
            ];
        }
        return [
            'allActions' => $result,
            'allProjects' => $allProject
        ];
    }
}



