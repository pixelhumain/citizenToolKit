<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\export;

use CTKAction, CacheHelper, Costum, Document, Form, Yii, Rest, PHDB, Answer, Pdf;
class PdfElementAction extends CTKAction{
	public function run($id, $type){

		$controller=$this->getController();
		ini_set('max_execution_time',1000);
		$costum = CacheHelper::getCostum();
		if(!empty($costum))
			$controller->costum = $costum;
		$paramsCostum["controller"] = $controller;
		$paramsCostum["slug"] = null; 
		$paramsCostum["admin"] = null; 
		$paramsCostum["id"] = null; 
		$paramsCostum["idElt"] = $id;

		

		if(Costum::isSameFunction("pdfElement")){
			if(isset($_POST["save"])){
				$res=Document::checkFileRequirements([], null, 
					array(
						"dir"=>"communecter",
						"typeEltFolder"=> Form::ANSWER_COLLECTION,
						"idEltFolder"=> $id,
						"docType"=> "file",
						"nameUrl"=>"/pdf.pdf",
						"sizeUrl"=>1000
					) ) ;
				$paramsCostum["comment"]=true;
				$paramsCostum["saveOption"]="F";
				$paramsCostum["urlPath"]=$res["uploadDir"];
				// $paramsCostum["docName"]=$_POST["title"].date("d-m-Y",strtotime($_POST["date"])).".pdf";
				//  if(isset($_POST["subKey"]))
				$paramsCostum["docName"]=$_POST["subKey"].date("d-m-Y",strtotime($_POST["date"])).".pdf";
			}
			
			$params = Costum::sameFunction("pdfElement", $paramsCostum);

			if(isset($_POST["save"])){	
				$kparams = [
	                "id" => $id,
	                "type" => Form::ANSWER_COLLECTION,
	                "folder" => Form::ANSWER_COLLECTION."/".$id."/".Document::GENERATED_FILE_FOLDER,
	                "moduleId" => "communecter",
	                "name" => $paramsCostum["docName"],
	                "size" => "",
	                "contentKey" => "",
	                "doctype"=> "file",
	                "author" => Yii::app()->session["userId"]
	            ];
	            if(isset($_POST["subKey"]))
					$kparams["subKey"]=$_POST["subKey"];

		        $res2 = Document::save($kparams);
		        return Rest::json( [ "res"=>true, "docPath"=>"/upload/".$kparams["moduleId"]."/".$kparams["folder"]."/".$params["docName"], "fileName"=>$params["docName"] ] );
		    }
		} 
		else 
		{
			$elt = PHDB::findOneById( $type , $id);
			$params = [ "elt" => $elt, "type" => $type , "id" => $id ];
			//Rest::json($elt); exit;
			if(!empty($controller->costum) && 
				!empty($controller->costum["pdf"]) && 
				!empty($controller->costum["pdf"][$type]) && 
				!empty($controller->costum["pdf"][$type])){
				$html = $controller->renderPartial($controller->costum["pdf"][$type], $params, true);
			} else if ($type == Answer::COLLECTION ) {
				$paramsAnswer = array("answerId" => $id ) ;
				$paramsAnswer["answer"] = $elt;
				$paramsAnswer["form"] = PHDB::findOneById( Form::COLLECTION, $elt["form"]  );
		        $paramsAnswer = Form::getDataForm($paramsAnswer);
		        $paramsAnswer["mode"] = "pdf";
		        $paramsAnswer["canEdit"] = false;
		        $paramsAnswer["canEditForm"] = false;
		        $paramsAnswer["canAdminAnswer"] = false;
		        $paramsAnswer["canSee"] = true;
		        $paramsAnswer["canAnswer"] = ( Yii::app()->session["userId"] == $paramsAnswer["answer"]["user"] || !empty($paramsAnswer["parentForm"]["anyOnewithLinkCanAnswer"])) ? true :false;
		        // Rest::json($paramsAnswer); exit;
				$html = $controller->renderPartial('application.views.pdf.answer', $paramsAnswer, true);
			} else {
				$html = $controller->renderPartial('application.views.pdf.element', $params, true);
			}
			$params["html"] = $html ;
			Pdf::createPdf($params);
		}
		
		

		
		
	}
}