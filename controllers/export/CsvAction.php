<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\export;

use CTKAction, Rest, Csv;
class CsvAction extends CTKAction{
	public function run(){

		$controller=$this->getController();
		ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '512M');
        $_POST["indexStep"] = "";
        $_POST["indexMin"] = "";
		return Rest::json(Csv::getCsvAdmin($_POST));
		
	}
}