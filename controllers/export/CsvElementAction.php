<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\export;

use CTKAction, CacheHelper, Costum, SearchNew, Rest;
class CsvElementAction extends CTKAction{
	public function run($type, $limit=50, $index=0){

		$controller=$this->getController();
		ini_set('max_execution_time',1000);
		$costum = CacheHelper::getCostum();
		if(!empty($costum))
			$controller->costum = $costum;
		//Rest::json($costum); exit;
		if(Costum::isSameFunction("csvElement")){
			$params = Costum::sameFunction("csvElement", $params);
		} else {
			
			// if(	!empty($controller->costum) ){
			// 	if($type == Answer::COLLECTION && !empty($controller->costum["form"]) && !empty($controller->costum["form"]["ids"])){
			// 		$where["formId"] = implode("|", $controller->costum["form"]["ids"]);
			// 		$forms = PHDB::find(Form::COLLECTION, array("id" => array('$in' => $controller->costum["form"]["ids"])));
			// 	} else if( !empty($controller->costum["slug"]) ) {
			// 		$where["source.key"] = $controller->costum["slug"];
			// 		$where["source.keys"] = array('$in' => $controller->costum["slug"] );
			// 	}
			// }
			
			// if($type == Answer::COLLECTION){
			// 	$queryForm = array("parent.".$_POST["id"] => array('$exists' => 1 ) );
			// 	if(!empty($controller->costum) && !empty($controller->costum["slug"]))
			// 		$queryForm = array('$and' => array( $queryForm , array("source.key" => $controller->costum["slug"]) ) ) ;
			// 	$forms = PHDB::find(Form::COLLECTION, $queryForm , array("name", "mapping") );
			// 	$idFs = array();
			// 	foreach ($forms as $keyF => $valF) {
			// 		$idFs[] = $keyF;
			// 	}
			// 	$res = Form::getBySourceAndId($controller->costum["slug"]);
			// 	$searchParams = $_POST;
			// 	$searchParams["count"] = true;
			// 	$resGA=Answer::globalAutocomplete($res["form"], $searchParams);
			// 	$subForms = $res["forms"];
			// 	$elts=$resGA["results"];
			// } else {
				// $where=array();
				// $sortCriteria=array();
				// $elts = PHDB::findAndSortAndLimitAndIndex( $type, $where, $sortCriteria, $limit, $index );
				//Rest::json($_POST); exit;
				$resS = SearchNew::globalAutoComplete($_POST);
				return Rest::json($resS); exit;
				$elts = $resS["results"];
				//Rest::json($elts); exit;
			//}
			
			$elements =  array();
			foreach ($elts as $keyElt => $valElt) {
				// if($type == Answer::COLLECTION){
				// 	$elements = Answer::csv($elements, $keyElt, $valElt, $subForms);
				// }else{
					$elements[$keyElt] = $valElt;
				//}
			}

			// $params =  array(
			// 	"elts" => $elements,
				
			// 	"type" => $type , 
			// 	"where" => @$where , 
			// 	"sortCriteria" => @$sortCriteria , 
			// 	"limit" => $limit , 
			// 	"index" => $index
			// );

			// if(!empty($forms))
			// 	$params["forms"] = $forms;
		}
		
		//Rest::json($elements); exit;
		Rest::csv($elements, false, false);
		
		
	}
}