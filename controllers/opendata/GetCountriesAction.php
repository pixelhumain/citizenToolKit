<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\opendata;

use CAction, Zone, Rest, Yii;
/**
 * Retrieve all the Countries 
 * @return [json] {value : "theValue", text : "the Text"}
 */
class GetCountriesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($hasCity = null)
    {
    	if(!is_bool($hasCity))
    		$hasCity = ($hasCity == "true" ? true : null);
    	
        $countries = Zone::getListCountry($hasCity);
        return Rest::json($countries,false);
    }
}