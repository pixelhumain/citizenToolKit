<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\answer;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use CAction;
use News;
use Answer;
use Rest;
use Translate;
use TranslateCommunecter;
use TranslateGeoJson;
use TranslateGogoCarto;
use TranslateJsonFeed;
use TranslateKml;
use TranslateSchema;
use Yii;
use PHDB;
use Form;
use MongoId;

class GetAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null, $limit=50, $form=null, $index=0, $key = null, $format=null, $user=null, $token=null, $tokenName=null) {
      $controller=$this->getController();
		// Get format
		header("Access-Control-Allow-Origin: *");
		
		
	    $bindMap = TranslateCommunecter::$dataBinding_allAnswer;

	    $params = [];
      $answer= (!empty($id)) ? PHDB::findOne( Answer::COLLECTION, [ "_id"=>new MongoId($id) ] ) : null ;  
      $form= (!empty($answer) && isset($answer["form"])) ? $answer["form"] : $form;
      
       
	    $params["form"] = PHDB::findOne( Form::COLLECTION, [ "_id"=>new MongoId($form) ] );
      $params["token"] = (!empty($token)) ? $token : "" ;
      $params["tokenName"] = (!empty($tokenName)) ? $tokenName : "" ;
      $params["user"] = (!empty($user)) ? $user : "" ; 
    // if(isset($params["form"]["preferences"]) && isset($params["form"]["preferences"]["isOpenData"]) && ($params["form"]["preferences"]["isOpenData"]=="true" || $params["form"]["preferences"]["isOpenData"]==true)){
	    $params["forms"] = [];
        if(isset($params["form"]["subForms"])){
	        foreach ($params["form"]["subForms"] as $ix => $formId) {
                $collec=Form::COLLECTION;
                $query=["id"=>$formId];
                if(isset($params["form"]["type"]) && $params["form"]["type"]=="aap"){
                    $collec=Form::INPUTS_COLLECTION;
                    $query=["formParent" => (string)$params["form"]["_id"],"step"=>$formId];
                }
	    	    $f = PHDB::findOne($collec, $query);
	    	    $params["forms"][$formId] = $f;
 	        }
        }

        if(!empty($id) && !empty($answer)){
            $singleAnswer=[];
            $singleAnswer[(string)$answer["_id"]]=$answer;
        }
      	$params["answers"] = (isset($singleAnswer)) ? $singleAnswer : PHDB::findAndLimitAndIndex( Answer::COLLECTION, ["form"=> $form], $limit, $index );
      	$result = (!empty($params) && !empty($bindMap) )? Translate::convert_answer($params , $bindMap):$params;
    // }
    // else{
    //     $result["msg"]="No open data to show";
    // }

    if ($format == Translate::FORMAT_KML) {
		$strucKml = News::getStrucKml();		
		Rest::xml($result, $strucKml,$format);	
    } else if ($format == "csv") {
		Rest::csv($result["entities"], false, false);
    } else {
		return Rest::json($result);
    }
    }

    
}



?>