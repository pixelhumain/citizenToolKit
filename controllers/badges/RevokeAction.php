<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use ActStr;
use Authorisation;
use Badge;
use MongoId;
use Notification;
use PHDB;
use Rest;
use Yii;

class RevokeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
		if(!(isset($_POST) && isset($_POST["awardId"]) && isset($_POST["awardType"]) && isset($_POST["badgeId"]))){
			return Rest::json(array("result"=> false, "error"=>"400", "msg" => "Bad Request : Check you parameters"));
		}
		if(Authorisation::badgeIsAdminEmetteur($_POST["badgeId"])){
			$setArray = [
				"\$set" => [
					"badges." . $_POST["badgeId"] . ".revoke" => "true",
					"badges." .  $_POST["badgeId"] . ".revokeAuthor" => Yii::app()->session["userId"]
				]
			];
			if(isset($_POST["revokeReason"])){
				$setArray["\$set"]["badges." . $_POST["badgeId"] . ".revokeReason"] = $_POST["revokeReason"];
			}
			PHDB::update($_POST["awardType"], ["_id" => new MongoId($_POST["awardId"])], $setArray);

			$this->constructNotification($_POST["badgeId"], $_POST["awardId"], $_POST["awardType"]);

			return Rest::json(array("result"=> true, "error"=>"200", "msg" => "OK"));
		}else{
			return Rest::json(array("result"=> false, "error"=>"400", "msg" => "Bad Request : Check you parameters"));
		}
	}

	private function constructNotification($badgeId, $awardId, $awardType){
		$badge = Badge::getById($badgeId);
		$award = PHDB::findOneById($awardType, $awardId);

		//send notif to award
		Notification::constructNotification(
			ActStr::VERB_REVOKE,
			[
				"id" => Yii::app()->session["userId"],
				"name" => Yii::app()->session["name"]
			],
			[
				"id" => $awardId,
				"type" => $awardType
			],
			[
				"name" => $badge["name"],
				"type" => Badge::COLLECTION,
				"id" => (string) $badge["_id"]
			],
			Badge::COLLECTION
		);

		//send notif to admin
		Notification::constructNotification(
			ActStr::VERB_REVOKE,
			[
				"id" => Yii::app()->session["userId"],
				"name" => Yii::app()->session["name"]
			],
			[
				"type" => Badge::COLLECTION,
				"id" => (string) $badge["_id"],
				"name" => $badge["name"],
			],
			[
				"id" => $awardId,
				"type" => $awardType,
				"name" => $award["name"]
			],
			Badge::COLLECTION."admin"
		);
	} 
}
