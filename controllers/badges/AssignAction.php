<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use ActStr;
use Authorisation;
use Badge;
use Cron;
use MongoDate;
use MongoId;
use Notification;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\PNGMetaDataHandler;
use Rest;
use Yii;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

class AssignAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
		if(!(isset($_POST) && isset($_POST["award"]) && isset($_POST["badgeId"]))){
			return Rest::json(array("result"=> false, "error"=>"400", "msg" => "Bad Request : Check you parameters"));
		}
		$badge = $_POST["badgeId"];
		$userId = Yii::app()->session["userId"];
		if(isset($userId)){
			$badgeElement = PHDB::findOneById(Badge::COLLECTION, $badge);
			if(Authorisation::badgeIsAdminEmetteur(null, $badgeElement)){
				$this->assignAll($badgeElement, $_POST);
				return Rest::json(['result' => true, 'msg' => 'Badge assigned with success1']);
			}else if(Authorisation::isPublicBadge(null, $badgeElement)){
				$this->assignAll($badgeElement, $_POST, true);
				return Rest::json(['result' => true, 'msg' => 'Badge assigned with success2']);
			}else{
				return Rest::json(['result' => false, 'msg' => 'user not know1']);
			}
		}else{
			return Rest::json(['result' => false, 'msg' => 'user not know2']);
		}
	}
	private function assignAll($badgeElement, $post, $attenteEmetteur = false)
	{
		foreach($post['award'] as $id => $value){
			$this->assign($badgeElement, $id, $value["type"], $post, $attenteEmetteur);
			$award = PHDB::findOneById($value["type"], $id);
			self::constructNotification($badgeElement,$award);
		}

		return;
	}
	private function assign($badgeElement, $award, $awardType, $post, $attenteEmetteur = false, $attenteRecepteur = true)
	{
		$element = PHDB::findOneById($awardType, $award);
		if($awardType != Person::COLLECTION && Authorisation::isElementAdmin($award, $awardType, Yii::app()->session["userId"], false, false)){
			$attenteRecepteur = false;
		}
		if($awardType == Person::COLLECTION && $award == Yii::app()->session["userId"]){
			$attenteRecepteur = false;
		}
		if(!$this->containsBadge($element, $badgeElement)){
			$badgeElementId = (string) $badgeElement['_id'];
			$setArray = ['badges.'.$badgeElementId.'.name' => $badgeElement['name'],'badges.'.$badgeElementId.'.isParcours' => $badgeElement['isParcours'], 'badges.'.$badgeElementId.'.type' => 'badges', 'badges.'.$badgeElementId.'.issuedOn' => new MongoDate(time())];
			if($attenteRecepteur){
				$setArray['badges.'.$badgeElementId.'.attenteRecepteur'] = true;
			}
			if($attenteEmetteur){
				$setArray['badges.'.$badgeElementId.'.attenteEmetteur'] = true;
			}
			if(isset($post["narative"])){
				$setArray['badges.'.$badgeElementId.'.narative'] = $post["narative"];
			}
			if(isset($post["evidences"]) && is_array($post["evidences"]) && count($post["evidences"]) > 0){
				$setArray['badges.'.$badgeElementId.'.evidences'] = $post["evidences"];
			}
			if(isset($post["expiredOn"])){
				$badgeToRevoke = PHDB::find(Cron::COLLECTION, ["awardId" => $award,"awardCollection" => $awardType,"badgeId" => $badgeElementId]);
				if(!($badgeToRevoke && !empty($badgeToRevoke))){
					$expiredOn = strtotime($post["expiredOn"]);
					$setArray['badges.'.$badgeElementId.'.expiredOn'] = $expiredOn;
					$cronData = array(
						"status" => Cron::STATUS_FUTURE,
						"type" => Cron::TYPE_BADGE,
						"sendOn" => $expiredOn,
						"awardId" => $award,
						"awardCollection" => $awardType,
						"badgeId" => $badgeElementId
					);
					Cron::save($cronData);
				}
			}
			$setArray['badges.'.$badgeElementId.'.show'] = "true";

			if(!$attenteRecepteur && !$attenteEmetteur){
				$baseDir = dirname(__FILE__) . "/../../../../pixelhumain/ph/";
				if(isset($badgeElement["profilMediumImageUrl"])){
					$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
					$image = file_get_contents( $baseDir . $badgeElement["profilMediumImageUrl"]);
					$meta = new PNGMetaDataHandler($image);
					$image_with_meta = $meta->add_chunks('iTXt', 'openbadges', $baseUrl . "/co2/badges/assertions/badge/" . (string) $badgeElement["_id"] . "/award/" . $award);
					$first = substr($badgeElement["profilMediumImageUrl"], 0, strripos($badgeElement["profilMediumImageUrl"],"/"));
					$saveDir = $baseDir . substr($first, 0 , strripos($first,"/")) . "/". "assertions/";
					if(!is_dir($saveDir)){
						mkdir($saveDir, 0755, true);
					}
					file_put_contents($saveDir . $award . ".png", $image_with_meta);
				}
			}
			$element = PHDB::findOneById($awardType, $award);
			if(isset($element["badges"]) && is_array($element["badges"])){
				$keys = array_keys($element["badges"]);
				if(count($keys) == 0 || $keys[0] == 0){
					PHDB::update($awardType,['_id' => new MongoId($award)], ['$unset' => ["badges" => true]]);
				}
			}
			PHDB::update($awardType,['_id' => new MongoId($award)], ['$set' => $setArray]);
		}
	}
	private function containsBadge($element, $badgeElement)
	{
		$badgeId = (string) $badgeElement['_id'];
		return isset($element) && isset($element['badges']) && key_exists($badgeId, $element['badges']);
	}

	private function getAssignState($badge, $award){
		$attenteEmetteur = !Authorisation::badgeIsAdminEmetteur(null, $badge);
		$attenteRecepteur = !(
			($award["type"] != Person::COLLECTION && Authorisation::isElementAdmin($award["id"],$award["type"], Yii::app()->session["userId"])) ||
			($award["type"] == Person::COLLECTION && $award["id"] == Yii::app()->session["userId"])
		);
		
		return [
			"attenteEmetteur" => $attenteEmetteur,
			"attenteRecepteur" => $attenteRecepteur
		];
	}

	private function constructNotification($badge, $award){ 
		$awardId = (string)$award["_id"];
		$awardType = $award["collection"];
		$awardName = $award["name"];

		$assignState = $this->getAssignState($badge, [ "id" => $awardId, "type" => $awardType]);

		if(!$assignState["attenteEmetteur"] && !$assignState["attenteRecepteur"]){
			//send assign notification
			Notification::constructNotification(
				ActStr::VERB_ASSIGN,
				[
					"id" => Yii::app()->session["userId"],
					"type" => Person::COLLECTION,
					"name" => Yii::app()->session["name"]
				],
				[
					"id" => (string) $badge["_id"],
					"type" => $badge["collection"],
					"name" => $badge["name"]
				],
				null,
				ActStr::SELF_ASSIGN
			);
		}else if($assignState["attenteEmetteur"]){
			Notification::constructNotification(
				ActStr::VERB_BADGE_ASK,
				[
					"id" => Yii::app()->session["userId"],
					"type" => Person::COLLECTION,
					"name" => Yii::app()->session["name"]
				],
				[
					"id" => (string)$badge["_id"],
					"type" => Badge::COLLECTION,
					"name" => $badge["name"]
				],
				[
					"id" => $awardId,
					"type" => $awardType,
					"name" => $awardName
				],
				ActStr::ASK_ASSIGN_EMITER
			);
		}else if($assignState["attenteRecepteur"]){
			//send notification to award
			Notification::constructNotification(
				ActStr::VERB_BADGE_ASK,
				[
					"id" => Yii::app()->session["userId"],
					"type" => Person::COLLECTION,
					"name" => Yii::app()->session["name"]
				],
				[
					"id" => $awardId,
					"type" => $awardType,
					"name" => $awardName
				],
				[
					"id" => (string) $badge["_id"],
					"type" => $badge["collection"],
					"name" => $badge["name"]
				],
				ActStr::ASK_ASSIGN_RECEIVER
			);
			//send notification to admin badge
			Notification::constructNotification(
				ActStr::VERB_ASSIGN,
				[
					"id" => Yii::app()->session["userId"],
					"type" => Person::COLLECTION,
					"name" => Yii::app()->session["name"]
				],
				[
					"id" => (string) $badge["_id"],
					"type" => $badge["collection"],
					"name" => $badge["name"]
				],
				[
					"id" => $awardId,
					"type" => $awardType,
					"name" => $awardName
				],
				Badge::COLLECTION
			);
		}
	}
}
