<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use PHDB;
use yii\web\NotFoundHttpException;

class ViewsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($badge, $view){
		return $this->getController()->renderPartial('co2.views.badges.' . $view,["element" => PHDB::findOneById(Badge::COLLECTION, $badge)]);
	}
}