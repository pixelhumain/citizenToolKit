<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\BadgeClass;
use Rest;
use Yii;
use yii\web\NotFoundHttpException;

class BadgesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id){
		try {
			$res = PHDB::findOneById(Badge::COLLECTION, $id);
			if(empty($res)){
				return $this->send404();
			}else{
				return Rest::json(BadgeClass::fromBadgeElement($res)->getAsAssociativeArray(),null, false, "application/ld+json");
			}
		} catch (\Throwable $th) {
			echo $th->getMessage();
			$this->send404();
		}
    }
	private function send404()
	{
		http_response_code(404);
		Rest::json(['result' => false, 'msg' => 'Not FOund']);
		exit();
		Yii::app()->end();
	}
}