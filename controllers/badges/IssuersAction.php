<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Person;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Profile;
use Project;
use Rest;
use Yii;
use yii\web\NotFoundHttpException;

class IssuersAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id){
		try {
			$res = PHDB::findOneById(Person::COLLECTION, $id);
			if(empty($res)){
				$res = PHDB::findOneById(Organization::COLLECTION, $id);
			}
			if(empty($res)){	
				$res = PHDB::findOneById(Project::COLLECTION, $id);
			}
			if(empty($res)){
				return $this->send404();
			}else{
				return Rest::json($this->preprocessRes($res),null, false, "application/ld+json");
			}
		} catch (\Throwable $th) {
			return $this->send404();
		}
    }
	private function send404($msg = 'Not Found')
	{
		http_response_code(404);
		return Rest::json(['result' => false, 'msg' => $msg]);
	}
	private function preprocessRes($res){
		if(!isset($res["email"])){
			$this->send404('This element must have an email to be an issuer');
		}
		$profile = Profile::fromElement($res);
		$profile->type = 'Issuer';
		$array = $profile->getAsAssociativeArray();
		return $array;
	}
}