<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use MongoId;
use PHDB;
use Rest;

class ParcoursadderAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
		$controller = $this->getController();
		$controller->layout = "//layouts/empty";
		$post = $_POST;
		if(!isset($post["badge"]) || !isset($post["parent"])){
			return Rest::json(["result" => false, "message" => "Data incomplet"]);
		}
		$badge = PHDB::findOneById(Badge::COLLECTION, $post["badge"]);
		$badgeExistingParents = isset($badge["parent"]) ? $badge["parent"] : [];
		$badgeParent = PHDB::findOneById(Badge::COLLECTION, $post["parent"]);
		$badgeExistingParents = array_merge($badgeExistingParents, [(string) $badgeParent["_id"] => [
			"name" => $badgeParent["name"],
			"type" => Badge::COLLECTION
		]]);
		PHDB::update(Badge::COLLECTION, ["_id" => new MongoId($post["badge"])], ["\$set" => [
			"parent" => $badgeExistingParents
		]]);
		return Rest::json(["result" => true, "message" => "OK"]);
	}
}
