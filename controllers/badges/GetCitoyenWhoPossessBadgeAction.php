<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Element;
use MongoId;
use Yii, Rest, Badge, Citoyen, Person, Organization, Event, Project, PHDB;

class GetCitoyenWhoPossessBadgeAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();

        if(isset($_POST["parent"]) && isset($_POST["contextId"])  && isset($_POST["contextType"]) ){
            $contextType = $_POST["contextType"];
            $contextId = $_POST["contextId"];
            $id = [];

            $where = array();
            $citoyen = array();

            foreach($_POST["parent"] as $parentId){
                array_push($where, array("badges.".$parentId  => array('$exists'=>1)));
            }

            $result = Element::getCommunityByTypeAndId($contextType, $contextId, "all", null, null, null, null); //PHDB::find(Citoyen::COLLECTION,  $where);
            foreach($result as $key => $val) {
                $resultat = PHDB::find(Citoyen::COLLECTION,  array(
                    '$and' => array(
                        array("badges.".$parentId  => array('$exists'=>1)),
                        array("_id" => new MongoId($key))
                    )
                ));
                if(isset($resultat[$key])){
                    $citoyen[$key] = $resultat[$key];
                    if(isset($val["roles"]))
                        $citoyen[$key]["roles"] = $val["roles"];
                }
            }    

            return Rest::json(array("res" => true, "data" => $citoyen, "where" => $where, "id" => $id));
        }
        
        return Rest::json(array("res" => false, "data" => array()));
    }
}
