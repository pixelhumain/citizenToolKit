<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use PHDB;
use Rest;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class ParcourseditAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($badge, $format = 'html'){
		$controller = $this->getController();
		$controller->layout = "//layouts/empty";
		$tree = Badge::getBadgeChildrenTree($badge);
		$params = ["tree" => $tree];
		if($format == 'html'){
			if(Yii::app()->request->isAjaxRequest){
        		return $controller->renderPartial("parcours-editor",$params,true);
			}
			else if( !Yii::app()->request->isAjaxRequest ){
				return $controller->render("parcours-editor", $params);
			}
		}else{
			return Rest::json($params);
		}
		
	}
}
