<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Element;
use yii\web\NotFoundHttpException;

class ElementbadgeAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($type,$id) {
		$controller=$this->getController();
		$element = Element::getElementById($id, $type);
		if($element)
			return $controller->renderPartial("badge",array("el" => $element));
		else
			throw new NotFoundHttpException("Element id".$id."type".$type." not found");
    }
}