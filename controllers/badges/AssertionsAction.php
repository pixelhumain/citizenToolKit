<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use CAction;
use Person;
use OpenBadgeException;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Assertion;
use Project;
use Rest;
use Yii;
use yii\web\NotFoundHttpException;
use Zend\Stdlib\DateTime;

class AssertionsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($badge, $award){
		try {
			$res = PHDB::findOneById(Person::COLLECTION, $award);
			if(empty($res)){
				$res = PHDB::findOneById(Organization::COLLECTION, $award);
			}
			if(empty($res)){
				$res = PHDB::findOneById(Project::COLLECTION, $award);
			}
			if(empty($res)){
				return $this->send404();
			}else{
				$b = PHDB::findOneById(Badge::COLLECTION, $badge);
				if(empty(@$res["badges"][$badge]) || empty($b)){
					return $this->send404();
				}else{
					$badgeElement = PHDB::findOneById(Badge::COLLECTION, $badge);
					return Rest::json(Assertion::fromBadgeAndEarnerElement($badgeElement, $res)->getAsAssociativeArray(),null, false, "application/ld+json");
				}
			}
		} catch (\Throwable $th) {
			if($th instanceof OpenBadgeException){
				$this->send404($th->getMessage());
			}else{
				$this->send404();
			}
		}
    }
	private function send404($msg = 'Not Found')
	{
		http_response_code(404);
		Rest::json(['result' => false, 'msg' => $msg]);
		exit();
		Yii::app()->end();
	}
}