<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use BadgeValidator;
use Element;
use MongoDate;
use MongoId;
use OpenBadgeException;
use PHDB;
use Rest;
use Yii;
use yii\web\NotFoundHttpException;

class ValidatorAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $id = NULL;
        try {
            if(!isset($_POST) || !isset($_POST["type"]) || !isset($_POST["contextId"]) || !isset($_POST["contextCollection"])){
                
               throw new OpenBadgeException('Some value not provided');
            }
            $element = PHDB::findOneById($_POST["contextCollection"], $_POST["contextId"]);
            if(empty($element["email"])){
                throw new OpenBadgeException("No valid email on the element which receive the badge");
            }
            $type = $_POST["type"];
            $response = NULL;
            switch ($type) {
                case 'url':
                    if(!isset($_POST["url"])){
                       throw new OpenBadgeException('URL not found');
                    }
                    $response = BadgeValidator::checkData($_POST["url"], $element["email"]);
                    break;
                case 'json':
                    if(!isset($_POST["json"])){
                        throw new OpenBadgeException('JSON not found');
                    }
                    $response = BadgeValidator::checkData($_POST["json"], $element["email"]);
                    break;
                case 'image':
                    if(!isset($_FILES) || !isset($_FILES["image"])){
                       throw new OpenBadgeException('No file found');
                    }
                    $image = $_FILES["image"];
                    $response = BadgeValidator::checkImage($image, $element["email"]);
                    break;
                default:
                   throw new OpenBadgeException('Type not valid');
                    break;
            }
            $input = $response["input"];
            $assertion = NULL;
            $issuer = NULL;
            $badgeClass = NULL;
            foreach ($response["graph"] as $value) {
                if(isset($value["type"])){
                    $type = strtolower($value["type"]);
                    if($type == "badgeclass"){
                        $badgeClass = $value;
                    }else if($type == "assertion"){
                        $assertion = $value;
                    }else if($type == "issuer"){
                        $issuer = $value;
                    }
                }
            }
            //CHECK IF BADGE ALREADY EXISTS
            $existingBadge = PHDB::findOne(Badge::COLLECTION, ["badgeClass.id" => $badgeClass["id"]]);
            if(isset($existingBadge)){
                $id = (string) $existingBadge["_id"];
            }else{
                $elementSave = Element::save([
                    'collection' => Badge::COLLECTION,
                    'creator' => Yii::app()->session['userId'],
                    'name' => $badgeClass["name"],
                    'type' => 'byReference'
                ]);
                $id = $elementSave["id"];
            }
            PHDB::update($_POST["contextCollection"], ["_id" => new MongoId($_POST["contextId"])], ["\$set" => [
                "badges.".$id.".issuedOn" => new MongoDate(strtotime($assertion["issuedOn"])),
                "badges.".$id.".type" => "badges",
                "badges.".$id.".name" => $badgeClass["name"],
                "badges.".$id.".byReference" => true,
                "badges.".$id.".lastVerification" => new MongoDate()
            ]]);

            PHDB::update(Badge::COLLECTION, ["_id" => new MongoId($id)], ["\$set" => [
                'name' => $badgeClass["name"],
                'issuer' => $issuer,
                'badgeClass' => $badgeClass,
                'assertion.' . $_POST["contextId"] => $assertion
            ]]);
            
            return Rest::json(['result' => true, 'msg' => 'OK', 'response' => $response]);
            
        } catch (\Throwable $th) {
            if($th instanceof OpenBadgeException){
                if(isset($_POST["badgeId"])){
                    PHDB::update($_POST["contextCollection"], ["_id" => new MongoId($_POST["contextId"])], ["\$unset" => ["badges.".$_POST["badgeId"] => ""]]);
                }
                return Rest::json(['result' => false, 'msg' => $th->getMessage()]);
                
            }
            return Rest::json(['result' => false, 'msg' => 'UNKNOW ERROR']);
            
        }
            
    }
}