<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use MongoId;
use PHDB;
use Rest;

class ParcoursdeleterAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
		$controller = $this->getController();
		$controller->layout = "//layouts/empty";
		$post = $_POST;
		if(!isset($post["child"]) || !isset($post["parent"])){
			return Rest::json(["result" => false, "message" => "Data incomplet"]);
		}
		$badgeChild = PHDB::findOneById(Badge::COLLECTION, $post["child"]);
		$badgeExistingParents = isset($badgeChild["parent"]) ? $badgeChild["parent"] : [];
		unset($badgeExistingParents[$post["parent"]]);
		PHDB::update(Badge::COLLECTION, ["_id" => new MongoId($post["child"])], ["\$set" => [
			"parent" => $badgeExistingParents
		]]);
		return Rest::json(["result" => true, "message" => "OK"]);
	}
}
