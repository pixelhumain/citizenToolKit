<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use MongoId;
use PHDB;
use Yii, Rest, Cms, Badge;

class SetBadgeParentInBlockCmsAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();

        if( isset($_POST["parent"]) && isset($_POST["id"]) ){
            $parentId = array();
            $where = array(
                "_id" => new MongoId($_POST["id"])
            );

            $action = array(
                '$set' => array("badge.parent" => $_POST["parent"])
            );

            if(isset($_POST["template"]))
                $action['$set'] = array_merge($action['$set'], array("template" => $_POST["template"]));

            foreach($_POST["parent"] as $idP => $nameP) {
                $parentId[$idP] = $idP;
            }

            $badges = Badge::getChild($parentId);

            $res = PHDB::update(Cms::COLLECTION, $where, $action);
            return Rest::json(($res));
        }
        
        return Rest::json(array("res" => false));
    }
}