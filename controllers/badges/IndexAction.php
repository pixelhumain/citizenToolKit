<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use CAction;
use Citoyen;
use Organization;
use PHDB;
use Project;
use Yii;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=null, $id=null, $tree=null){

    	$controller=$this->getController();
    	$this->getController()->layout = "//layouts/empty";
		if($tree == null || $id == null){
			$badges = PHDB::find(Badge::COLLECTION);
			$category = array();
			foreach ($badges as $k => $v) {
				$count = 0;
				// $count += PHDB::count(Organization::COLLECTION, array('badges.'.$k => array('$exists' => true)));
				// $count += PHDB::count(Project::COLLECTION, array('badges.'.$k => array('$exists' => true)));
				// $count += PHDB::count(Citoyen::COLLECTION, array('badges.'.$k => array('$exists' => true)));
				$badges[$k]["count"] = $count;
				if(!isset($v["category"])){
					$v["category"] = "other";
					$badges[$k]["category"] = "other";
				}
				if(!in_array($v["category"], $category)){
					array_push($category, $v["category"]);
				}
			}
			$params = array( 
				"badges" =>  $badges,
				"category" => $category
			);
			
			if( Yii::app()->request->isAjaxRequest )
				return $controller->renderPartial("index", $params, true);
			else 
				return $controller->render("index", $params, true);
		}
		else{
			$badges = Badge::getBadgeChildrenTree($id);
			$params = array(
				"badges" =>  $badges
			);
			if( Yii::app()->request->isAjaxRequest )
				return $controller->renderPartial("treeview", $params, true);
			else 
				return $controller->render("treeview", $params, true);
		}
    }
}