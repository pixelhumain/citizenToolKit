<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use ActStr;
use Authorisation;
use Badge;
use MongoId;
use Notification;
use PHDB;
use Rest;
use Yii;

class CancelRevokeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type, $award, $badge){
		if(Authorisation::badgeIsAdminEmetteur($badge)){
			$unsetArray = [
				"\$unset" => [
					"badges." . $badge . ".revoke" => true,
					"badges." .  $badge . ".revokeAuthor" => true,
					"badges." . $badge . ".revokeReason" => true
				]
			];
			PHDB::update($type, ["_id" => new MongoId($award)], $unsetArray);
			return Rest::json(array("result"=> true, "error"=>"200", "msg" => "OK"));
		}else{
			return Rest::json(array("result"=> false, "error"=>"400", "msg" => "Bad Request : Check you parameters"));
		}
	}
}
