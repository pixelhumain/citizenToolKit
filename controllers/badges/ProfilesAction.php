<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Person;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Profile;
use Project;
use Rest;
use Yii;
use yii\web\NotFoundHttpException;

class ProfilesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id){
		try {
			$res = PHDB::findOneById(Person::COLLECTION, $id);
			if(empty($res)){
				$res = PHDB::findOneById(Organization::COLLECTION, $id);
			}
			if(empty($res)){	
				$res = PHDB::findOneById(Project::COLLECTION, $id);
			}
			if(empty($res)){
				return $this->send404();
			}else{
				return Rest::json(Profile::fromElement($res)->getAsAssociativeArray(),null, false, "application/ld+json");
			}
		} catch (\Throwable $th) {
			$this->send404();
		}
    }
	private function send404()
	{
		http_response_code(404);
		Rest::json(['result' => false, 'msg' => 'Not FOund']);
		exit();
		Yii::app()->end();
	}
}