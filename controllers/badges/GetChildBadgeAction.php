<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;
use Yii, Rest, Badge;

class GetChildBadgeAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();

        if(isset($_POST["parent"])){
            $badges = Badge::getChild($_POST["parent"]);
            return Rest::json(array("res" => true, "data" => $badges));
        }
        
        return Rest::json(array("res" => false, "data" => array()));
    }
}
