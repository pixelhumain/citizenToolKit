<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Authorisation;
use Badge;
use Organization;
use Person;
use PHDB;
use Project;
use Rest;
use yii\web\NotFoundHttpException;

class CountAttenteAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($badge){
		if(!Authorisation::badgeIsAdminEmetteur($badge)){
			$personCountRecepteur = PHDB::count(Person::COLLECTION, ['$or' => ["badges.".$badge.".attenteRecepteur" => ['$exists' => true], "badges.".$badge.".attenteEmetteur" => ['$exists' => true]]]);
			$orgaCountRecepteur = PHDB::count(Organization::COLLECTION, ['$or' => ["badges.".$badge.".attenteRecepteur" => ['$exists' => true], "badges.".$badge.".attenteEmetteur" => ['$exists' => true]]]);
			$projectCountRecepteur = PHDB::count(Project::COLLECTION, ['$or' => ["badges.".$badge.".attenteRecepteur" => ['$exists' => true], "badges.".$badge.".attenteEmetteur" => ['$exists' => true]]]);			
		}else{ 
			$personCountRecepteur = PHDB::count(Person::COLLECTION, ["badges.".$badge.".attenteRecepteur" => ['$exists' => true], "badges.".$badge.".attenteEmetteur" => ['$exists' => false]]);
			$orgaCountRecepteur = PHDB::count(Organization::COLLECTION, ["badges.".$badge.".attenteRecepteur" => ['$exists' => true], "badges.".$badge.".attenteEmetteur" => ['$exists' => false]]);
			$projectCountRecepteur = PHDB::count(Project::COLLECTION, ["badges.".$badge.".attenteRecepteur" => ['$exists' => true], "badges.".$badge.".attenteEmetteur" => ['$exists' => false]]);
		}
		
		$attente = $personCountRecepteur + $orgaCountRecepteur + $projectCountRecepteur;

		$personCountEmetteur = PHDB::count(Person::COLLECTION, ["badges.".$badge.".attenteEmetteur" => ['$exists' => true]]);
		$orgaCountEmetteur = PHDB::count(Organization::COLLECTION, ["badges.".$badge.".attenteEmetteur" => ['$exists' => true]]);
		$projectCountEmetteur = PHDB::count(Project::COLLECTION, ["badges.".$badge.".attenteEmetteur" => ['$exists' => true]]);
		$toConfirm =  $personCountEmetteur + $orgaCountEmetteur + $projectCountEmetteur;
		$res = ["to-confirm" => $toConfirm, "attente" => $attente];

		return Rest::json($res);
	}
}