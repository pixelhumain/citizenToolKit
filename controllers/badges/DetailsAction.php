<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Assertion;
use Yii;

class DetailsAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($badge, $element, $collection, $narrative = false) {
        $controller = $this->getController();
        $awardElement = PHDB::findOneById($collection, $element);
        $badgeElement = PHDB::findOneById(Badge::COLLECTION, $badge);
        $assertion = Assertion::fromBadgeAndEarnerElement($badgeElement, $awardElement)->getAsAssociativeArray();
        $revokeAuthor = '';
        if(isset($awardElement["badges"][$badge]["revokeAuthor"])){
            $author = PHDB::findOneById(Person::COLLECTION, $awardElement["badges"][$badge]["revokeAuthor"], ["name"]);
            if(!empty($author)){
                $revokeAuthor = $author["name"];
            }
        }
        return $controller->renderPartial("details", [
            'assertion' => $assertion,
            'badgeElement' => $badgeElement,
            'awardElement' => $awardElement,
            'revokeAuthor' => $revokeAuthor,
            'narrative' => $narrative
        ]);
    }
}