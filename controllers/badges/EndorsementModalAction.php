<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use Endorsement;
use PHDB;
use Rest;

class EndorsementModalAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type, $id, $badge){
		$endorsements = Endorsement::getEndorsementByElementByBadge($type, $id, $badge);
		self::injectImagesIntoEndorsement($endorsements);
		$controller = $this->getController();
        return $controller->renderPartial("endorsement-modal", ["endorsements" => $endorsements]);
	}
	private static function injectImagesIntoEndorsement(&$endorsements){
		$issuers = [];
		// store all $id and element to take images
		foreach($endorsements as $idEndorsement => $value){
			$collection = array_values($value["issuer"])[0]["type"];
			$id = array_keys($value["issuer"])[0];
			if(!isset($issuers[$collection])){
				$issuers[$collection] = [];
			}
			$issuers[$collection][$idEndorsement] = $id;
		}
		// get elements and image attributes
		$elements = [];
		foreach(array_keys($issuers) as $collection){
			$currentElements = PHDB::findByIds( $collection , array_values($issuers[$collection]),['profilMediumImageUrl']);
			$elements = array_merge($currentElements, $elements);
		}
		// Copy value image of elements into endorsements;
		foreach($endorsements as $idEndorsement => $value){
			$id = array_keys($value["issuer"])[0];
			$collection = array_values($value["issuer"])[0]["type"];
			if(!isset($elements[$issuers[$collection][$idEndorsement]]['profilMediumImageUrl'])){
				continue;
			}
			$endorsements[$idEndorsement]["issuer"][$id]["image"] = $elements[$issuers[$collection][$idEndorsement]]["profilMediumImageUrl"];
		}
	}
}