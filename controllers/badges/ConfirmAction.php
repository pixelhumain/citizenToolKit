<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use ActStr;
use Authorisation;
use Badge;
use MongoId;
use Notification;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\PNGMetaDataHandler;
use VARIANT;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class ConfirmAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($badge, $award, $type,$confirmType = 'recepteur',  $confirm = "true"){
		if(!in_array($confirmType, ["recepteur", "emetteur"]))
			throw new BadRequestHttpException('confirmType not recognized');

		//check if user can Change current element
		if($confirmType == 'recepteur'){
			$this->recepteur($award, $badge, $type, $confirm);
		}else if($confirmType == 'emetteur'){
			$this->emetteur($award, $badge, $type, $confirm);
		}

		$this->constructNotification($confirm, $confirmType, $badge, $award, $type);
	}
	private function emetteur($award, $badge, $type, $confirm){
		if(Authorisation::badgeIsAdminEmetteur($badge)){
			$this->makeChanges($badge,$award, $type, $confirm, 'emetteur');
		}
	}
	private function recepteur($award, $badge, $type, $confirm)
	{
		$userId = Yii::app()->session["userId"];
		if($type == Person::COLLECTION){
			if($userId == $award){
				// MAKE CHANGES
				$this->makeChanges($badge,$award, $type, $confirm);
			}
		}
		//check if user admin of element
		if(Authorisation::isElementAdmin($award, $type, $userId)){
			//make changes
			$this->makeChanges($badge,$award, $type, $confirm);
		}
	}
	private function makeChanges($badge, $award, $type, $confirm, $confirmType = 'recepteur')
	{
		if($confirm == "true"){
			$elt = PHDB::findOneById(Badge::COLLECTION, $badge);
			if($confirmType == 'recepteur'){
				$baseDir = dirname(__FILE__) . "/../../../../pixelhumain/ph/";
				if(isset($elt["profilMediumImageUrl"])){
					$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
					$image = file_get_contents( $baseDir . $elt["profilMediumImageUrl"]);
					$meta = new PNGMetaDataHandler($image);
					$image_with_meta = $meta->add_chunks('iTXt', 'openbadges', $baseUrl . "/co2/badges/assertions/badge/" . $badge . "/award/" . $award);
					$first = substr($elt["profilMediumImageUrl"], 0, strripos($elt["profilMediumImageUrl"],"/"));
					$saveDir = $baseDir . substr($first, 0 , strripos($first,"/")) . "/". "assertions/";
					if(!is_dir($saveDir)){
						mkdir($saveDir, 0755, true);
					}
					file_put_contents($saveDir . $award . ".png", $image_with_meta);
				}
				PHDB::update($type, ['_id' => new MongoId($award)], ['$unset' => ['badges.'. $badge . '.attenteRecepteur' => 1]]);
				$awardElement = PHDB::findOneById($type, $award);
			}else if($confirmType == 'emetteur'){
				PHDB::update($type, ['_id' => new MongoId($award)], ['$unset' => ['badges.'. $badge . '.attenteEmetteur' => 1]]);
				$awardElement = PHDB::findOneById($type, $award);
				
				if(!(isset($awardElement['badges'][$badge]['attenteRecepteur']))){
					$baseDir = dirname(__FILE__) . "/../../../../pixelhumain/ph/";
					if(isset($elt["profilMediumImageUrl"])){
						$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
						$image = file_get_contents( $baseDir . $elt["profilMediumImageUrl"]);
						$meta = new PNGMetaDataHandler($image);
						$image_with_meta = $meta->add_chunks('iTXt', 'openbadges', $baseUrl . "/co2/badges/assertions/badge/" . $badge . "/award/" . $award);
						$first = substr($elt["profilMediumImageUrl"], 0, strripos($elt["profilMediumImageUrl"],"/"));
						$saveDir = $baseDir . substr($first, 0 , strripos($first,"/")) . "/". "assertions/";
						if(!is_dir($saveDir)){
							mkdir($saveDir, 0755, true);
						}
						file_put_contents($saveDir . $award . ".png", $image_with_meta);
					}
				}
			}
		}else{
			PHDB::update($type, ['_id' => new MongoId($award)], ['$unset' => ['badges.'. $badge => 1]]);
		}
	}

	private function constructNotification($confirm, $confirmType, $badgeId, $awardId, $awardType){
		$badge = Badge::getById($badgeId);
		$award = PHDB::findOneById($awardType, $awardId);
		

		if($badge && $award){
			if($confirm == "true"){
				if($confirmType == "emetteur"){
					//notification for award
					Notification::constructNotification(
						ActStr::VERB_BADGE_CONFIRM,
						[
							"id" => Yii::app()->session["userId"],
							"type" => Person::COLLECTION,
							"name" => Yii::app()->session["name"]
						],
						[
							"id" => (string) $award["_id"],
							"type" => $award["collection"],
							"name" => $award["name"]
						],
						[
							"id" => (string) $badge["_id"],
							"type" => $badge["collection"],
							"name" => $badge["name"]
						],
						ActStr::VERB_EMETTEUR_CONFIRM
					);
					//notification for badge admin
					Notification::constructNotification(
						ActStr::VERB_BADGE_CONFIRM,
						[
							"id" => Yii::app()->session["userId"],
							"type" => Person::COLLECTION,
							"name" => Yii::app()->session["name"]
						],
						[
							"id" => (string) $badge["_id"],
							"type" => $badge["collection"],
							"name" => $badge["name"]
						],
						[
							"id" => (string) $award["_id"],
							"type" => $award["collection"],
							"name" => $award["name"]
						],
						ActStr::VERB_EMETTEUR_CONFIRM."admin"
					);
				}else if($confirmType == "recepteur"){
					Notification::constructNotification(
						ActStr::VERB_BADGE_CONFIRM,
						[
							"id" => Yii::app()->session["userId"],
							"type" => Person::COLLECTION,
							"name" => Yii::app()->session["name"]
						],
						[
							"id" => (string) $badge["_id"],
							"type" => $badge["collection"],
							"name" => $badge["name"]
						],
						[
							"id" => (string) $award["_id"],
							"type" => $award["collection"],
							"name" => $award["name"]
						],
						ActStr::VERB_RECEPTEUR_CONFIRM
					);
				}
			}else if($confirm == "false"){
				//send notif to award
				Notification::constructNotification(
					ActStr::VERB_REVOKE,
					[
						"id" => Yii::app()->session["userId"],
						"name" => Yii::app()->session["name"]
					],
					[
						"id" => $awardId,
						"type" => $awardType
					],
					[
						"name" => $badge["name"],
						"type" => Badge::COLLECTION,
						"id" => (string) $badge["_id"]
					],
					Badge::COLLECTION
				);

				//send notif to admin
				Notification::constructNotification(
					ActStr::VERB_REVOKE,
					[
						"id" => Yii::app()->session["userId"],
						"name" => Yii::app()->session["name"]
					],
					[
						"type" => Badge::COLLECTION,
						"id" => (string) $badge["_id"],
						"name" => $badge["name"],
					],
					[
						"id" => $awardId,
						"type" => $awardType,
						"name" => $award["name"]
					],
					Badge::COLLECTION."admin"
				);
			}
		}
	}
}
