<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use Person;
use PHDB;
use Rest;


class GetBadgeAction extends  \PixelHumain\PixelHumain\components\Action
{
    public function run($id)
    {
        // Check if the ID is provided
        if (!$id) {
            return Rest::json([
                'status' => 'error',
                'message' => 'ID cannot be null',
                'data' => new \stdClass()
            ]);
        }

        // Fetch the badge using the provided ID
        $badge = PHDB::findOneById(Badge::COLLECTION, $id);

        // Check if the badge exists
        if (!$badge) {
            return Rest::json([
                'status' => 'error',
                'message' => 'Badge not found',
                'data' => new \stdClass()
            ]);
        }

        // Initialize the creator data
        $creatorData = new \stdClass();

        // Check if the badge has a creator
        if (!empty($badge['creator'])) {
            // Fetch the creator's full document
            $creator = PHDB::findOneById(Person::COLLECTION, $badge['creator']);

            if ($creator) {
                // Extract only the specified fields
                $creatorData = [
                    'name' => isset($creator['name']) ? $creator['name'] : null,
                    'email' => isset($creator['email']) ? $creator['email'] : null,
                    'username' => isset($creator['username']) ? $creator['username'] : null,
                    'profilImageUrl' => isset($creator['profilImageUrl']) ? $creator['profilImageUrl'] : null,
                    'profilMarkerImageUrl' => isset($creator['profilMarkerImageUrl']) ? $creator['profilMarkerImageUrl'] : null,
                    'profilMediumImageUrl' => isset($creator['profilMediumImageUrl']) ? $creator['profilMediumImageUrl'] : null,
                    'profilThumbImageUrl' => isset($creator['profilThumbImageUrl']) ? $creator['profilThumbImageUrl'] : null,
                    'badges' => isset($creator['badges']) ? $creator['badges'] : []
                ];

                // Optionally, remove keys with null values
                $creatorData = array_filter($creatorData, function($value) {
                    return !is_null($value);
                });
            }
        }

        // Combine badge data with the filtered creator information
        $data = [
            'badge' => $badge,
            'creator' => (object)$creatorData // Convert to object for consistency
        ];

        return Rest::json([
            'status' => 'success',
            'data' => $data
        ]);
    }
}