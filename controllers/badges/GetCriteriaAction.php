<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use PHDB;
use Rest;

class GetCriteriaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($badge){
		$badge = PHDB::findOneById(Badge::COLLECTION, $badge, ['criteria']);
		return Rest::json(['criteria' => $badge['criteria']['narrative']]);
	}
}