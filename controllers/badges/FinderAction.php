<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Yii;

class FinderAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($email = false, $source=null,$parentId=null) {
        $controller = $this->getController();
        return $controller->renderPartial("assign-badge-finder-in-element", ["email" => $email, "source" => $source, "parentId" => $parentId ]);
    }
}