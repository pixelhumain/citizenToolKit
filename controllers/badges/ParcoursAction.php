<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use PHDB;
use Rest;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class ParcoursAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($badge, $element, $collection, $status = "false"){
		$controller = $this->getController();
		$controller->layout = "//layouts/empty";
		$badgeAssigned = null;
		$element = PHDB::findOneById($collection, $element);
		if(isset($element["badges"])){
			foreach($element["badges"] as $idBadge => $badgeValue){
				if(!isset($badgeValue["attenteEmetteur"]) && !isset($badgeValue["attenteRecepteur"])){
					if($badgeAssigned == null){
						$badgeAssigned = [];
					}
					array_push($badgeAssigned, $idBadge);
				}
			}
		}
		$badgeElement = PHDB::findOneById(Badge::COLLECTION, $badge);
		if($status == "false"){
			if( Yii::app()->request->isAjaxRequest )
				return $controller->renderPartial("parcours-for-element",["badge" => $badgeElement, "badgeAssigned" => $badgeAssigned], true);
			else 
				return $controller->render("parcours-for-element",["badge" => $badgeElement, "badgeAssigned" => $badgeAssigned], true);
		}else{
			$tree = Badge::getBadgeChildrenTree($badgeElement["_id"], $badgeAssigned);
			$childrenLocked = $tree["childrenLocked"];
			$childrenCount = $tree["childrenCount"];
			$progress = (($childrenCount - $childrenLocked) / $childrenCount) * 100;
			$progress = round($progress, 2);
			return Rest::json(["progress" => $progress]);
		}
	}
}
