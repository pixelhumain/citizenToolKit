<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use MongoId;
use PHDB;
use Rest;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class SettingsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($element, $type){
		try {
			$setValue = [];
			foreach ($_POST["badges"] as $id => $value) {
				$setValue['badges.' . $id . ".order"] = $value["order"];
				$setValue['badges.' . $id . ".show"] = $value["show"];
				$setValue['badges.' . $id . ".showBanner"] = $value["showBanner"];
			}
			PHDB::update($type, ["_id" => new MongoId($element)], ["\$set" => $setValue]);
			return Rest::json(["result" => true, "msg" => "updated successfully"]);
		} catch (\Throwable $th) {
			return Rest::json(["result" => false, "msg" => "Error"]);
		}
	}

}
