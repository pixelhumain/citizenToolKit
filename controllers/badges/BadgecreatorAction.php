<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class BadgecreatorAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
		echo $this->getController()->renderPartial("badge-creator",[], true);
	}

}
