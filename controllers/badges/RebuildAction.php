<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges;

use Badge;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\PNGMetaDataHandler;
use Rest;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class RebuildAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
		try {
			$award = $_POST["awardId"];
			$badgeElement = PHDB::findOneById(Badge::COLLECTION, $_POST["badgeId"]);
			$baseDir = dirname(__FILE__) . "/../../../../pixelhumain/ph/";
			if(isset($badgeElement["profilMediumImageUrl"])){
				$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
				$image = file_get_contents( $baseDir . $badgeElement["profilMediumImageUrl"]);
				$meta = new PNGMetaDataHandler($image);
				$image_with_meta = $meta->add_chunks('iTXt', 'openbadges', $baseUrl . "/co2/badges/assertions/badge/" . (string) $badgeElement["_id"] . "/award/" . $award);
				$first = substr($badgeElement["profilMediumImageUrl"], 0, strripos($badgeElement["profilMediumImageUrl"],"/"));
				$saveDir = $baseDir . substr($first, 0 , strripos($first,"/")) . "/". "assertions/";
				if(!is_dir($saveDir)){
					mkdir($saveDir, 0755, true);
				}
				file_put_contents($saveDir . $award . ".png", $image_with_meta);
			}
			return Rest::json(["result" => true, "msg" => "updated successfully"]);
			
		} catch (\Throwable $th) {
			return Rest::json(["result" => false, "msg" => "Error"]);
		}
	}

}
