<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;
use CAction;
use DataValidator;
use Element;
use ElephantIO\Client;
use Link;
use Rest;
use Yii;

/**
 * Action used to validate a link between a child and a parent
 * Ex : 
 * - member need to be validate by an admin of the organization/projet
 * - new admin demand must be validated by a current admin of the organization/projet
 * 
 */
class ValidateAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
    	if(DataValidator::missingParamsController($_POST, ["childId", "childType","parentId","parentType", "linkOption"]) )
            return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));
    
		$res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!" ));
		
		$linkOption = $_POST["linkOption"];
		if ($linkOption != Link::IS_ADMIN_PENDING && $linkOption != Link::TO_BE_VALIDATED && $linkOption != Link::IS_INVITING && $linkOption != Link::IS_ADMIN_INVITING ) {
			return array( "result" => false , "msg" => "Unknown link option : ".$linkOption);
		}

		$res = Link::validateLink($_POST["parentId"], $_POST["parentType"], $_POST["childId"], 
									$_POST["childType"], $linkOption, Yii::app()->session["userId"]);
		if($res["result"] == true && Yii::app()->params['cows']['enable']){
			$client = Client::create(str_replace("wss:", "https:", Yii::app()->params['cows']['serverUrl']), ["client" => Client::CLIENT_4X]);
        	$client->connect();
			$client->emit('directory_event', [
				'name' => "join_user",
				"id" => $_POST["parentId"],
				'data' => Element::getByTypeAndId($_POST["childType"], $_POST["childId"])
			]);
			$client->disconnect();
		}
		return Rest::json($res);
	}
}