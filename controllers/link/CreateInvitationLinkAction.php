<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use Authorisation;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use Rest;
use Yii;
use CacheHelper;

class CreateInvitationLinkAction extends \PixelHumain\PixelHumain\components\Action
{
    function run(){
        $data = $_POST;

        if(isset($data["targetId"]) && isset($data["targetType"])){
            if(Authorisation::isElementAdmin($data["targetId"], $data["targetType"], Yii::app()->session["userId"])){
                $data["invitorId"] =  Yii::app()->session["userId"];
                $data["ref"] = uniqid();
                $data["createOn"] = time();
                $costum = CacheHelper::getCostum();
                if(isset($costum) && isset($costum["slug"])){
                    $data["source"]=array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]]);
                }    
                PHDB::insert(InvitationLink::COLLECTION, $data);
                
                return Rest::json([
                    "result"=>[
                        "link"=> Yii::app()->getBaseUrl(true)."/co2/link/connect/ref/".$data["ref"]
                    ]
                ]);
            }else{
                return Rest::json( array("result"=>false,"error"=>"You have not a permission to create a link for this element."));
            }
        }else{
            return Rest::json( array("result"=>false,"error"=>"Missing parameters."));
        }
    }
}