<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;
use MongoId;
use PHDB;
use Rest;

class LoadContextDataLinksAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $elementLink =  PHDB::findOne($_POST["type"],array("_id"=>new MongoId($_POST["id"])),array("links"));
        $res["links"] = $elementLink["links"];
        // $res["counts"] = [];
        // foreach($elementLink["links"] as $kl => $vl){
        //     $res["counts"][$kl] = count($elementLink["links"][$kl]);
        // } 
        return Rest::json($res);
    }
}