<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;
use ActStr;
use CAction;
use CTKException;
use Element;
use ElephantIO\Client;
use Link;
use MongoId;
use Notification;
use Organization;
use Person;
use PHDB;
use Rest;
use Yii;

class DisconnectAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!" ));
        if(@$_POST){
	        $childId=$_POST["childId"];
	        $childType=$_POST["childType"];
	        $parentId=$_POST["parentId"];
	        $parentType=$_POST["parentType"];
	        $connectType=$_POST["connectType"];
	        $linkOption=null;
			
			
	        if(@$_POST["linkOption"])
	        	$linkOption=$_POST["linkOption"];
	        $removeMeAsAdmin=false;
			try {

				if($parentType==Organization::COLLECTION)
					$parentConnect="memberOf";
				else
					$parentConnect = Link::$linksTypes[$childType][$parentType];
					//$parentConnect=$parentType; 

				if($connectType == "followers")
					$parentConnect="follows";
				if($connectType == "friends")
					$parentConnect="friends";
				if($parentType == Person::COLLECTION && $childType == Organization::COLLECTION && $connectType!="follows"){
					$parentConnect="members";
					$connectType="memberOf";
				}
				//if($parentType == Person::COLLECTION && $childType == Organization::COLLECTION && $connectType == "members")
				//	$connectType="memberOf";

				$whereParent = array(	"_id" => new MongoId($parentId),
										"parent.".$childId => array('$exists' => true ) );
				$isParent = PHDB::findOne( $parentType, 
					                       $whereParent );

				//Rest::json($isParent); exit;
				if(empty($isParent))
					$data=Link::disconnect($childId, $childType, $parentId, $parentType,Yii::app()->session['userId'], $parentConnect,$linkOption);
				else
					$data["parentEntity"] = $isParent;
				
				Link::disconnect($parentId, $parentType, $childId, $childType,Yii::app()->session['userId'], $connectType,$linkOption);
				if($childId == Yii::app()->session["userId"] && !@$_POST["fromMyDirectory"]){
					$removeMeAsAdmin=true;
				}
				if(@$_POST["fromMyDirectory"])
					$collection = $parentType;
				else
					$collection = $childType;
				$res = array( "result" => true , "msg" => Yii::t("",$connectType." successfully removed"), "collection" => $collection,"removeMeAsAdmin"=> $removeMeAsAdmin,"parentId"=>$parentId,"parentType"=>$parentId,"parent"=>$data["parentEntity"]);			

				if(Yii::app()->params['cows']['enable']){
					$client = Client::create(str_replace("wss:", "https:", Yii::app()->params['cows']['serverUrl']), ["client" => Client::CLIENT_4X]);
					$client->connect();
					$client->emit('directory_event', [
						'name' => "remove_user",
						"id" => $parentId,
						'data' => Element::getByTypeAndId($childType, $childId)
					]);
					$client->disconnect();
				}

				$this->saveNotification($parentId, $parentType);
			} catch (CTKException $e) {
				$res = array( "result" => false , "msg" => $e->getMessage() );
			}
		} 
		return Rest::json($res);
    }

	private function saveNotification($parentId, $parentType){
		//send notification
		Notification::constructNotification(
			//verb
			ActStr::VERB_LEAVE,
			//author
			[
				"id" => Yii::app()->session["userId"],
				"name" => Yii::app()->session["name"]
			],
			//target
			[
				"id" =>$parentId,
				"type" =>$parentType
			],
			//obejct
			NULL,
			//levelType
			$parentType
		);
	}
}