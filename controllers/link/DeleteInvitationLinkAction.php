<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use Rest;

class DeleteInvitationLinkAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($ref)
    {
        $res = InvitationLink::delete($ref);
        return Rest::json($res);
    }
}