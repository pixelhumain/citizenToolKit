<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use CAction, Yii, CTKException, Rest, Link;
class LinkChildParentAction extends \PixelHumain\PixelHumain\components\Action
{
	 /**
	 * 
	 */
    public function run() {
    	$res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!") );
		try {
			if (isset($_POST["parent"])) {
				foreach ($_POST["parent"] as $key => $value) {
					$isConnectingAdmin = false;
					$parentId = $key;
			    	$parentType = $value["type"];
			    	$child = array(
						"childId" => $_POST["childId"],
				    	"childType" => $_POST["childType"],
				    	"childName" => $_POST["childName"],
			            "childEmail" => $_POST["childEmail"]
				    );
				    if ($_POST["connectType"] == "admin"){
				    	 $isConnectingAdmin == true;
				    }
				    $roles = [];
			    	$res = Link::connectParentToChild($parentId, $parentType, $child, $isConnectingAdmin, Yii::app()->session["userId"], $roles);
				}
			}

		} catch (CTKException $e) {
			$res = array( "result" => false , "msg" => $e->getMessage() );
		}

		return Rest::json($res);;

    }



}