<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use CAction, DataValidator, Yii, Person, Rest, Link;
class FollowAction extends \PixelHumain\PixelHumain\components\Action
{
	 /**
	 * TODO Clement : La PHPDOC
	 */
    public function run() {
    	if(DataValidator::missingParamsController($_POST, ["childType","parentId","parentType"]) )
    	    return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));
     
	    $result = array("result"=>false, "msg"=>Yii::t("common", "Incorrect request"));
		
		if (! Person::logguedAndValid()) {
			return $result;
		}

	    $roles="";
	    $child = array(
			"childId" => @$_POST["childId"],
	    	"childType" => $_POST["childType"],
	    	"childName" => @$_POST["childName"],
            "childEmail" => @$_POST["childEmail"]
	    );
    	$parentId = $_POST["parentId"];
    	$parentType = $_POST["parentType"];
    	$result = Link::follow($parentId, $parentType, $child, Yii::app()->session["userId"]);
		return Rest::json($result);
    }

}