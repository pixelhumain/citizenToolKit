<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use MongoId;
use Organization;
use PHDB;
use Rest;
use Role;
use Yii;

class EkitiaLinkAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $ekitia = PHDB::findOne(Organization::COLLECTION, array("slug" => "ekisphere"), array("links" => 1));
            if($ekitia){
                $ekitiaId = ((string)$ekitia["_id"]);
                $links = PHDB::find(Organization::COLLECTION, array("links.memberOf.$ekitiaId" => array('$exists' => false),"slug" => array('$ne' => "ekisphere"),'$or' => array(array("source.keys" => "ekisphere"), array("reference.costum" => "ekisphere"))), array("links" => 1));
                $i = 0;
                foreach ($links as $key => $orga){
                    $ekitia["links"]["members"][$key] = array("type" => "organizations");
                    $orga["links"]["memberOf"][$ekitiaId] = array("type" => "organizations");
                    PHDB::update(Organization::COLLECTION, array("_id" => new MongoId($key)), array('$set' => $orga));
                    $i++;
                }
                PHDB::update(Organization::COLLECTION, array("_id" => new MongoId($ekitiaId)), array('$set' => $ekitia));
                return Rest::sendResponse(200, "<h2>$i organisations ajoutés comme membre d'Ekitia</h2>");
            }else{
                return Rest::sendResponse(404, "<h2>Organisation Ekitia non trouvé</h2>");
            }
        }else{
            return Rest::sendResponse(401, "<h2>Vous n'êtes pas autorisé</h2>");
        }
    }
}