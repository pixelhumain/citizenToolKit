<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use Link;
use Yii;
use MongoId;
use PHDB;
use Rest;

class VerifyAttendeeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
		$res = array( "result" => false, "isParticipant" => false , "msg" => Yii::t("common","Something went wrong!" ));
		if (isset($_POST["verifyAttendee"]) && ($_POST["verifyAttendee"] === "true" || $_POST["verifyAttendee"] === true)) {
			$where = array(
				"_id" => new MongoId($_POST["parentId"]),
				"links.attendees." . $_POST["childId"] =>  array('$exists' => 1)
			);
			$originLinksKnows = PHDB::findOne($_POST["parentType"], $where);
			$verify = isset($originLinksKnows);	
			if ($verify) {
				$res = array( "result" => true, "isParticipant" => true);
				return Rest::json($res);
			} 
		}
		return Rest::json($res);
    }

}