<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use CAction, Rest, CTKException, Yii,Link;
class MultiConnectAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run() {
		$controller=$this->getController();
		if(!empty($_POST["parentId"]) && !empty($_POST["parentType"]) && !empty($_POST["listInvite"])) {
			try {
				$list = $_POST["listInvite"] ;
				$parent = $_POST;
				$res = Link::invite($list, $parent);
				return Rest::json($res);
			} catch (CTKException $e) {
				return Rest::json(array("result"=>false, "msg"=>$e->getMessage(), "data"=>$_POST));
			}
		}
		return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Invalid request")));
	}

}