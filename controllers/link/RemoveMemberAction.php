<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use CAction, Yii, ActivityStream, Organization, CTKException, Rest, Link;
class RemoveMemberAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($memberId, $memberType, $memberOfId, $memberOfType)
    {
        $res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!" ));
		try {
			$res = ActivityStream::removeObject($memberId, $memberType, $memberOfId, $memberOfType, "join");
			$res = Link::removeMember($memberOfId, Organization::COLLECTION, $memberId, $memberType, Yii::app()->session['userId']);
			
		} catch (CTKException $e) {
			$res = array( "result" => false , "msg" => $e->getMessage() );
		}

		return Rest::json($res);
    }
}