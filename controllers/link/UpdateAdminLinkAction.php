<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link;

use Yii, PHDB, MongoId, Rest, Link;
class UpdateAdminLinkAction extends \PixelHumain\PixelHumain\components\Action{
	public function run(){
		//Rest::json($_POST); exit ;
		$form = PHDB::findOne( $_POST["parentType"] , 
								array("_id"=> new MongoId($_POST["parentId"]) ), 
								array("links","name" ) );
		
		//var_dump($_POST["$form "]);
		if(!empty($form) && 
			!empty($form["links"]) && 
			!empty($form["links"][Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]]][$_POST["childId"]])){
			if($_POST["isAdmin"] == "true"){
				$form["links"][ Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]] ][ $_POST["childId"] ]["isAdmin"] = true ;
			} else {
				unset($form["links"][ Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]] ][ $_POST["childId"] ]["isAdmin"]);
				if(isset($form["links"][ Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]] ][ $_POST["childId"] ]["isAdminInviting"]))
					unset($form["links"][ Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]] ][ $_POST["childId"] ]["isAdminInviting"]);
				if(isset($form["links"][ Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]] ][ $_POST["childId"] ]["isAdminPending"]))
					unset($form["links"][ Link::$linksTypes[$_POST["parentType"]][$_POST["childType"]] ][ $_POST["childId"] ]["isAdminPending"]);
			}
			//var_dump("update parentType");
			$res = PHDB::update( $_POST["parentType"], 
										array("_id" => new MongoId($_POST["parentId"])), 
										array('$set' => array("links" => $form["links"])));
		}

		$formChild = PHDB::findOne( $_POST["childType"] , 
								array("_id"=> new MongoId($_POST["childId"]) ), 
								array("links","name" ) );
		//var_dump($formChild);
		if(!empty($formChild) && 
			!empty($formChild["links"]) && 
			!empty($formChild["links"][Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]]][$_POST["parentId"]])){
			if($_POST["isAdmin"] == "true"){
				$formChild["links"][ Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]] ][ $_POST["parentId"] ]["isAdmin"] = true ;
			} else {
				unset($formChild["links"][ Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]] ][ $_POST["parentId"] ]["isAdmin"]);
				if(isset($formChild["links"][ Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]] ][ $_POST["parentId"] ]["isAdminInviting"]))
					unset($formChild["links"][ Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]] ][ $_POST["parentId"] ]["isAdminInviting"]);
				if(isset($formChild["links"][ Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]] ][ $_POST["parentId"] ]["isAdminPending"]))
					unset($formChild["links"][ Link::$linksTypes[$_POST["childType"]][$_POST["parentType"]] ][ $_POST["parentId"] ]["isAdminPending"]);

			}
			//var_dump("update childType");
			$res = PHDB::update( $_POST["childType"], 
										array("_id" => new MongoId($_POST["childId"])), 
										array('$set' => array("links" => $formChild["links"])));
		}
		if($_POST["isAdmin"] == "false"){
			$form["msg"] = Yii::t("common","{who} is well remove to administrate {what}",array("{who}"=>$formChild["name"],"{what}"=>$form["name"]));
		}else{
			$form["msg"] = Yii::t("common","{who} is well added to administrate {what}",array("{who}"=>$formChild["name"],"{what}"=>$form["name"]));
		}

		return Rest::json($form); exit ;
	}
}