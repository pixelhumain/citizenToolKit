<?php
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

    use CAction, Convert, Rest, Yii;
    class IdWikidataAction extends \PixelHumain\PixelHumain\components\Action {
        
        public function run() {

            // header("Access-Control-Allow-Origin: *");
            // header("Content-Type: application/json");
    
            // $pageTitle = $_GET['title'];
            // $language = "fr";
            // $apiUrl = "https://${language}.wikipedia.org/w/api.php";
            // $params = [
            //     'action' => 'query',
            //     'format' => 'json',
            //     'titles' => urlencode($pageTitle),
            //     'prop' => 'pageprops',
            //     'ppprop' => 'wikibase_item',
            // ];

            // $url = $apiUrl . '?' . http_build_query($params);
            
            // $curl = curl_init($url);
    
            // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            // $response = curl_exec($curl);
    
            // if (curl_errno($curl)) {
            //     echo json_encode(['error' => 'Erreur cURL : ' . curl_error($curl)]);
            //     http_response_code(500);
            //     exit();
            // }
    
            // // Fermez la connexion cURL
            // curl_close($curl);
    
            // // Retournez la réponse JSON
            // echo $response;

            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json");
        
            if (!isset($_GET['title']) || empty($_GET['title'])) {
                echo json_encode(['error' => 'Le paramètre "title" est requis.']);
                http_response_code(400);
                exit();
            }
        
            $pageTitle = $_GET['title'];

            $pageTitleWithDashes = str_replace(' ', '-', $pageTitle);
            $language = "fr";
            $apiUrl = "https://www.wikidata.org/w/api.php";
            $params = [
                'action' => 'wbsearchentities',
                'search' => $pageTitleWithDashes,
                'language' => $language,
                'format' => 'json',
            ];
            $url = $apiUrl . '?' . http_build_query($params);
        
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
            $response = curl_exec($curl);
            if (curl_errno($curl)) {
                echo json_encode(['error' => 'Erreur cURL : ' . curl_error($curl)]);
                http_response_code(500);
                exit();
            }
            curl_close($curl);
        
            $data = json_decode($response, true);
        
            if (isset($data['search']) && !empty($data['search'])) {
                $result = $data['search'][0];
                $wikidataId = $result['id'];
                $label = $result['label'];
                $description = isset($result['description']) ? $result['description'] : '';
        
                echo json_encode([
                    'wikidataId' => $wikidataId,
                    'label' => $label,
                    'description' => $description,
                ]);
            } else {
                echo json_encode(['error' => 'Aucun résultat trouvé pour "' . htmlspecialchars($pageTitleWithDashes) . '".']);
                http_response_code(404);
            }
        }
    }
?>