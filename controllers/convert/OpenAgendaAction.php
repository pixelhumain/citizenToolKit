<?php
    
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;
    
    use Convert, Rest;
    
    class OpenAgendaAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $response = Convert::ConvertOpenAgendaToPh($_GET);
            if (!empty($response['error'])) $output = [
                'success' => false,
                'total'   => 0,
                'after'   => 0,
                'items'   => [],
                'message' => $response['error']
            ];
            else {
                $output = $response;
                $output['message'] = '';
            }
            return Rest::json($output);
        }
    }