<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

class OsmContribution extends \PixelHumain\PixelHumain\components\Action
{
	private $defaultUrl = "https://overpass-api.de/api/interpreter";

	public function run()
	{
		$username = 'fanilontsoadinah@gmail.com';
        $password = 'd!d!nah05';

        // ID de l'élément à mettre à jour
        $id = 11010203810;

        // Nouvelle description à ajouter
        $new_description = "Description de l'université IFP";

        // Construire la requête de mise à jour
        $data = [
            'type' => 'node',
            'id' => $id,
            'tags' => [
                'description' => $new_description
            ]
        ];

        // Convertir les données en format XML pour OSM API
        $xml_data = '<osmChange version="0.6" generator="YourApp">';
        $xml_data .= '<modify>';
        $xml_data .= '<node id="'.$id.'" version="1">';
        foreach ($data['tags'] as $key => $value) {
            $xml_data .= '<tag k="'.$key.'" v="'.$value.'"/>';
        }
        $xml_data .= '</node>';
        $xml_data .= '</modify>';
        $xml_data .= '</osmChange>';

        // Initialiser cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.openstreetmap.org/api/0.6/changeset/create');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

        // Exécuter la requête
        $response = curl_exec($ch);

        // Vérifier la réponse de l'API
        if ($response !== false) {
            return "Mise à jour réussie.";
        } else {
            return "Erreur lors de la mise à jour.";
        }
        curl_close($ch);
	}
}
