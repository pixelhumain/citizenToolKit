<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class DatagouvAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($file=null, $type=null, $url=null) {

    	$res = Convert::convertDatagouvToPh($url);

    	if (isset($res)) {
			return Rest::json($res);
		}

		Yii::app()->end();
	}
}

?>