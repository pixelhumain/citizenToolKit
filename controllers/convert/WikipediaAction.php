<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class WikipediaAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($text_filter = null) {

		$url = $_GET['url'];
		$title = $_GET['title'];
		$type = $_GET['type'];
		$wikidataID = $_GET['wikidataID'];
		$res = "";

		$res = Convert::convertWikiToPath($url, $type, $title, $wikidataID, $text_filter);

  		if (isset($res)) {
			return Rest::json($res);
		}

		Yii::app()->end();
	}
}

?>