<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class FabMobAction extends \PixelHumain\PixelHumain\components\Action {

	public function run($data, $categorie) {
    $res = Convert::convertFabMobToPh($data, $categorie);

	if (isset($res)) {
			return Rest::json($res);
		}

        Yii::app()->end();

	}
}

?>
