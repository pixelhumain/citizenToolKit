<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class PoleEmploiAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($url, $rome_activity = null) {
    	$res = Convert::convertPoleEmploiToPh($url, $_POST, $rome_activity);

  		if (isset($res)) {
			return Rest::json($res);
		}

		Yii::app()->end();
	}
}

?>