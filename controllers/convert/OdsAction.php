<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class OdsAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($url) {

    	$res = Convert::convertOdsToPh($url);

  		if (isset($res)) {
			return Rest::json($res);
		}

		Yii::app()->end();
	}
}

?>