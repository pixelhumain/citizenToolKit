<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use  Rest, Cms, MongoId, PHDB, Element, Osm, Costum;

class OsmAction extends \PixelHumain\PixelHumain\components\Action
{
	private $defaultUrl = "https://overpass-api.de/api/interpreter";

	private $osmLevel = [
		"MG" => [
			"provinces" => "3",
			"region" => "4",
			"district" => "6",
			"ville" => "8"
		],
		"FR" => [
			"region" => "4",
			"departement" => "6",
			"epci" => "7",
			"cities" => "8"
		],
	];

	public function run()
	{
		$url = $this->defaultUrl;
		$res = array(
			'element' => array(),
			'count' => array(),
			'query' => array(),
		);
		
		
		if ($_POST != null) {
			if(isset($_POST["setUser"]) && isset($_POST["user"]) && isset($_POST["password"]) )
				return Osm::setAccountUser($_POST["user"], $_POST["password"]);
			
			if(isset($_POST["contribution"]))
				return $this->contribution($_POST["contribution"], (isset($_POST["comment"])) ? $_POST["comment"] : "", (isset($_POST["contextType"])) ? $_POST["contextType"] : "", (isset($_POST["contextId"])) ? $_POST["contextId"] : "" );
			
			if (isset($_POST["location"]))
				$params["location"] = $_POST["location"];
			if (isset($_POST["longitude"]))
				$params["longitude"] = $_POST["longitude"];
			if (isset($_POST["latitude"]))
				$params["latitude"] = $_POST["latitude"];
			
			if (isset($_POST["types"])  && isset($_POST["osmAllTYpe"])) {
				$types = array();
				$this->getTypeAndTag($_POST["types"], $_POST["osmAllTYpe"], $types);
				if(isset($_POST["typeCocity"]) && isset($_POST["parentId"]) && isset($_POST["parentType"])) {
					$parent = Element::getByTypeAndId($_POST["parentType"], $_POST["parentId"], ["address.addressCountry"]);
					if($parent && isset($parent["address"]["addressCountry"])){
						$params["levelLocation"] = $this->osmLevel[$parent["address"]["addressCountry"]][$_POST["typeCocity"]];
						$params["countryLocation"] = $parent["address"]["addressCountry"];
						if($parent['address']["addressCountry"] == "MG")
							$params["prefixLocation"] = $_POST["typeCocity"] == "district" ? "District de " : null;
					}
				}
				foreach($types as $key => $type) {
					$params["tag"] = $key;
					for($i = 0; $i < count($type) ; $i++) {
						$params["type"] = $type[$i]["type"];
						
						$request = $this->getQuerry($params);
						$res["query"][] = $request;
						$query = $request["query"];
						$element = Osm::getData($query, $type[$i], true, false);
						if($element != null) {
							$count = array(
								"name" => $type[$i]["name"], 
								"count" => count($element), 
								"type" => $type[$i]["type"],
							);

							if(isset($type[$i]["image"]))
								$count["image"] = $type[$i]["image"];
								
							$count["icon"] = (isset($type[$i]["icon"])) ? $type[$i]["icon"] : "fa fa-paperclip";

							$res["element"] = array_replace($res["element"], $element);
							$res["count"][$type[$i]["type"]] = $count;
						}	
							
					}
				}
				
				if (isset($_POST["idBlock"]) && (!isset($_POST["changeFiltreCity"]) || (isset($_POST["changeFiltreCity"]) && ( $_POST["changeFiltreCity"] == "false" || $_POST["changeFiltreCity"] == false || !$_POST["changeFiltreCity"]))))  {
					$block = Element::getByTypeAndId(Cms::COLLECTION, $_POST["idBlock"]);
					$unset = [];
					if(isset($block["typeTag"]) && $block["typeTag"] != null) {
						foreach( $block["typeTag"] as $key => $value) {
							if($value != null) {
								foreach( $value as $kk => $vvv) {
									if(!$vvv)
										$unset["typeTag.".$key.".".$kk] = "";
								}
							} 
						}
					}
					
					if(count($unset) > 0)
						PHDB::update(Cms::COLLECTION, array("_id" => new MongoId($_POST["idBlock"])), array('$unset' => $unset));

					if(count($res["element"]) > 0 && $block != null && isset($block["parent"]) && $block["parent"] != null) {
						foreach($block["parent"] as $idParent => $parent ) {
							if($parent != null && isset($parent["type"])) {
								$unset = [];
								$result = Element::getByTypeAndId($parent["type"], $idParent);
								if($result != null && isset($result["costum"]) && $result["costum"] != null)
									$costum = $result["costum"] ;
								else
									$costum = Costum::getCostumCache();
								
								$where = array(
									"_id" => new MongoId($idParent)
								);

								$setInCostum = [];
								if(!isset($setInCostum["element"]))
									$setInCostum["element"] = array();
								$res["test"] = [
									"exist" => [],
									"non" => [],
								];
								$setInCostum["count"] = array();
								if($costum != null && isset($costum["osm"]) && $costum["osm"] != null && isset($costum["osm"]["elts"]) && $costum["osm"]["elts"] != null && isset($costum["osm"]["elts"]["element"]) && $costum["osm"]["elts"]["element"] != null) {
									
									foreach( $costum["osm"]["elts"]["element"] as $key => $data) {
										if(isset($data["osmId"]))  {
											$setInCostum["element"][$data["osmId"]] = $data;
										}
									}

									if(isset($costum["osm"]["elts"]["count"]) && $costum["osm"]["elts"]["count"] != null) {
										foreach( $costum["osm"]["elts"]["count"] as $key => $c) {
											if(isset($c["type"])) 
												$setInCostum["count"][$c["type"]] = $c;
										}
									}
									
									if(isset($costum["osm"]["typeTag"]) && $costum["osm"]["typeTag"] != null) {
										foreach( $costum["osm"]["typeTag"] as $key => $value) {
											if($value != null) {
												foreach( $value as $kk => $vvv) {
													if(!$vvv)
														$unset["osm.typeTag.".$key.".".$kk] = "";
												}
											} 
										}
									}
								}
								
								foreach($res["element"] as $osmId => $v) {
									if(isset($setInCostum["element"][$osmId]))
										$res["test"]["exist"][] = $osmId;
									else
										$res["test"]["non"][] = $osmId;
									$setInCostum["element"][$osmId] = $v;
								}
								
								foreach($res["count"] as $key => $vv) {
									$setInCostum["count"][$key] = $vv;
								}
								
								$action = array(
									'$set' => array(
										"costum.osm.elts" => $setInCostum
									),
								);
								$res["unset"] = $unset;
								if(count($unset) > 0)
									$action['$unset'] = $unset;

								if(isset($_POST["types"]))
									$action['$set']["costum.osm.typeTag"] = $_POST["types"];

								if(isset($_POST["location"]))
									$action['$set']["costum.osm.location"] = $_POST["location"];

								if(isset($_POST["selectedElement"]))
									$action['$set']["costum.osm.selectedElement"] = $_POST["selectedElement"];

						
								PHDB::update($parent["type"], $where, $action);

								if($setInCostum != null && isset($setInCostum["element"]) && isset($setInCostum["count"])) {
									$res["element"] = $setInCostum["element"];
									$res["count"] = $setInCostum["count"];
								}
							}
						}
					}
				}
			}
			else if(isset($_POST["location"])) {
				$loca = $params["location"];
				if (gettype($loca) == "string") $loca = explode(",", $loca);
				
				foreach ($loca as $key => $loc) {
					$location = $this->getGeoOfLocation($loc, $this->defaultUrl);
					$res["element"] = array_merge($res["element"], $location);
				}
				
			}
		}
		return Rest::json($res);
	}

	private function getQuerry($params)
	{
		$query = "[out:json][timeout:25];(";
		$geo = [];
		$arround = 4000;
		$coordonate = [];

		if (isset($params) && $params != null) {
			if (isset($params["location"])) {
				$loca = $params["location"];
				if (gettype($loca) == "string") $loca = explode(",", $loca);

				$level = isset($params["levelLocation"]) ? $params["levelLocation"] : null;

				foreach ($loca as $key => $loc) {
					if($level){
						if(isset($params["prefixLocation"]) && $params["prefixLocation"] != null){
							$loc = $params["prefixLocation"].$loc;
						}
						$queryText = "";
						$queryText .= 'area["name"~"^'.$loc.'$", i]["admin_level"="'.$level.'"]->.searchArea;';
						
						array_push($geo, $queryText);
					}else{
						$location = $this->getGeoOfLocation($loc, $this->defaultUrl);
						$coordonate = $location;
						if ($location != null && isset($location[0])) {

							if (isset($location[0]["geo"]) && $location[0]["geo"] != null && isset($location[0]["geo"]["latitude"]) && isset($location[0]["geo"]["longitude"])) {

								array_push($geo, array("latitude" => $location[0]["geo"]["latitude"], "longitude" => $location[0]["geo"]["longitude"]));
							}
						}
					}
				}
				// var_dump($geo);die();
			}

			if (isset($params["longitude"])  && isset($params["latitude"])) {
				array_push($geo, array("latitude" => $params["latitude"], "longitude" => $params["longitude"]));
			}
			if (count($geo) > 0) {
				foreach ($geo as $key => $loc) {
					if (isset($params["type"]) && isset($params["tag"])) {
						foreach(explode(",", $params["tag"]) as $tmpTag) {
							foreach(explode(",", $params["type"]) as $tmpType) {
								// $query .= "nwr(around:" . $arround . "," . $loc["latitude"] . ", " . $loc["longitude"] . " )['" . $tmpTag . "' = '" . $tmpType . "'];";
								$query .= $level ? $loc."nwr['" . $tmpTag . "' = '" . $tmpType . "'](area.searchArea);" : "nwr(around:" . $arround . "," . $loc["latitude"] . ", " . $loc["longitude"] . " )['" . $tmpTag . "' = '" . $tmpType . "'];";
							}	
						}
					} else if (isset($params["types"]) && isset($params["osmAllTYpe"])) {
						$types = array();
						$this->getTypeAndTag($params["types"], $params["osmAllTYpe"], $types);
						foreach ($types as $key => $type) {
							for ($i = 0; $i < count($type); $i++) {
								foreach(explode(",", $type[$i]["tag"]) as $tmpTag) {
									foreach(explode(",", $type[$i]["type"]) as $tmpType) {
										// $query .= "nwr(around:" . $arround . "," . $loc["latitude"] . ", " . $loc["longitude"] . " )['" . $tmpTag . "' = '" . $tmpType . "'];";
										$query .= $level ? `nwr['` . $tmpTag . `' = '` . $tmpType . `'](area.searchArea);` : "nwr(around:" . $arround . "," . $loc["latitude"] . ", " . $loc["longitude"] . " )['" . $tmpTag . "' = '" . $tmpType . "'];";
									}	
								}
							}
						}
					}
				}
			}
		}

		$query .= ");out center;"; 
		return [
			"query"=> $query,
			"coordonate"=> $coordonate
		];
	}

	private function getTypeAndTag($types, $osmAllTYpe, &$res)
	{
		if ($types != null  && $osmAllTYpe != null) {
			if (isset($types["allType"])  && $types["allType"] != "false" && $types["allType"] != false) {
				foreach ($osmAllTYpe as $kk => $vv) {
					if ($vv != null) {
						if (isset($vv["tag"]) && isset($vv["type"])) {
							if (!isset($res[$vv["tag"]]))
								$res[$vv["tag"]] = array();

							$array = array(
								"type" => $vv["type"], "name" => $vv["name"]
							);

							if (isset($vv["image"]))
								$array["image"] = $vv["image"];

							if (isset($vv["icon"]))
								$array["icon"] = $vv["icon"];

							if (isset($vv["marker"]))
								$array["marker"] = $vv["marker"];

							
							array_push($res[$vv["tag"]], $array);
						}
					}
				}
			} else {
				foreach ($types as $index => $type) {
					if (isset($osmAllTYpe[$index]) && $osmAllTYpe[$index] != null && $type != "false" && $type != false) {
						if (isset($osmAllTYpe[$index]["element"]) && $osmAllTYpe[$index]["element"] != null) {
							$this->getTypeAndTag($type, $osmAllTYpe[$index]["element"], $res);
						} else if (isset($osmAllTYpe[$index]["tag"]) && isset($osmAllTYpe[$index]["type"])) {
							if (!isset($res[$osmAllTYpe[$index]["tag"]]))
								$res[$osmAllTYpe[$index]["tag"]] = array();

							$array = array(
								"type" => $osmAllTYpe[$index]["type"], "name" => $osmAllTYpe[$index]["name"]
							);

							if (isset($osmAllTYpe[$index]["image"]))
								$array["image"] = $osmAllTYpe[$index]["image"];

							if (isset($osmAllTYpe[$index]["icon"]))
								$array["icon"] = $osmAllTYpe[$index]["icon"];

							if (isset($osmAllTYpe[$index]["marker"]))
								$array["marker"] = $osmAllTYpe[$index]["marker"];

							array_push($res[$osmAllTYpe[$index]["tag"]], $array);
						}
					}
				}
			}
		}
	}

	private function getGeoOfLocation($location, $url)
	{
		$query = '[out:json][timeout:25];(nwr["place"="city"]["name"="' . $location . '"];nwr["place"="country"]["name"="' . $location . '"];nwr["type"="boundary"]["name"="' . $location . '"];);/*added by auto repair*/(._;>;);/*end of auto repair*/out body;';
		return Osm::getData($query, null, true, true);
	}

	public function contribution($params, $comment = "", $contextType = "", $contextId = "") {
		if($params != null && isset($params["id"]) && isset($params["type"]) && isset($params["update"]))
			return Rest::json(Osm::updateElement($params["type"], $params["id"], $params["update"], $comment, $contextType, $contextId));
		else if($params != null && isset($params["lat"]) && isset($params["lon"]) && isset($params["create"]))
			return Rest::json(Osm::createElement($params["lat"], $params["lon"], $params["create"], $comment, $contextType, $contextId));

		return  Rest::json(["res" => false]);
	}
}
