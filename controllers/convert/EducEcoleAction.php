<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class EducEcoleAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($url = null) {

    	$res = Convert::convertEducEcoleToPh($url);

  		if (isset($res)) {
			return Rest::json($res);
		}

		Yii::app()->end();
	}
}

?>