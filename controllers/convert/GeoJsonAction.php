<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Import, Rest, Yii;
class GeoJsonAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($file=null, $type=null, $url=null) {
		
		$controller=$this->getController();

		if (isset($_FILES['file'])) {
			$file = file_get_contents($_FILES['file']['tmp_name']);
		}

		$res = Import::GetParams($file, $type, $url);

		if (isset($res)) {
			return Rest::json($res);
		}
			Yii::app()->end();
	}
}

?>