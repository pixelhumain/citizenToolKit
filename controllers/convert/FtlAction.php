<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class FtlAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($url=null, $text_filter=null) {

		$res = Convert::convertFtl($url, $text_filter);

  		if (isset($res)) {
			return Rest::json($res);
		}

		Yii::app()->end();
	}
}

?>