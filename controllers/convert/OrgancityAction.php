<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class OrgancityAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($url=null) {

		$res = Convert::ConvertOrgancityToPh($url);

  		if (isset($res)) {
			return Rest::json($res);
		}

		Yii::app()->end();
	}
}

?>