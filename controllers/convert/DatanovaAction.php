<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert;

use CAction, Convert, Rest, Yii;
class DatanovaAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($url) {

    	$res = Convert::convertDatanovaToPh($url);

  		if (isset($res)) {
			return Rest::json($res);
		}

		Yii::app()->end();
	}
}

?>