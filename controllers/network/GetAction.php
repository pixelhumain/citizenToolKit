<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\network;
use CAction;
use Network;
use Rest;
use Yii;

class GetAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null) {
		$controller=$this->getController();
		// Get format
		header("Access-Control-Allow-Origin: *");
		$result = Network::getById($id);
		return Rest::json($result);
    }
}



?>