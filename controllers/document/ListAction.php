<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document;
use Authorisation;
use CAction;
use Document;
use Element;
use Organization;
use Rest;
use Yii;

class ListAction extends \PixelHumain\PixelHumain\components\Action {

	public function run($id, $type,$getSub=false,$tpl=null) 
	{
		$controller=$this->getController();
		
		$controller->title = "RESSOURCES";
		$controller->subTitle = "Toutes les ressources de nos associations";
		$controller->pageTitle = ucfirst($controller->module->id)." - ".$controller->title;

		if( $type == Organization::COLLECTION )
		{
			$organizations = array();
			$organization = Organization::getById($id);
			$organizations[$id] = $organization['name'];
		}
		$documents = Document::getWhere( array( "type" => $type, 
												"id" => $id ) );
		
		if($getSub && $type == Organization::COLLECTION && Authorisation::canEditMembersData($id)) {
		}

		$categories = Document::getAvailableCategories($id, $type);
		$params = array("documents"=>$documents, 
						"id" => $id, 
						"categories" => $categories,
						"getSub" => $getSub
						);
		if(@$organizations)
			$params["organizations"] = $organizations;

		if( @$tpl == "json" ){
			 $documentsLight = array(
			 	"list"=>array(),
			 	"element" => Element::getByTypeAndId($type, $id, array("name"))
			 	);
			foreach (  $documents as $k => $v) {
				$documentsLight["list"][$k] = array(
					"path" => Yii::app()->getRequest()->getBaseUrl(true)."/upload/communecter/".$v["folder"],
					"name" => $v["name"],
					"id" => (string)$v["_id"],
					"author" => $v["author"],
				);
			}
			return Rest::json( $documentsLight );
		}
		else if( Yii::app()->request->isAjaxRequest )
			return $controller->renderPartial("documents",$params,true);
		else
		return	$controller->render("documents",$params);
	}
	
}