<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document;

use CAction, Yii, Document, Rest;
class UploadAction extends \PixelHumain\PixelHumain\components\Action {
	

	public function run($dir,$folder=null,$ownerId=null,$input,$rename=false) {
		
        if(strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
            return json_encode(array('result'=>false,'error'=>Yii::t("document","Error! Wrong HTTP method!")));
        }
        if(array_key_exists($input,$_FILES) && $_FILES[$input]['error'] == 0 ) {
            $file = $_FILES[$input];
        } else {
            error_log("WATCH OUT ! - ERROR WHEN UPLOADING A FILE ! CHECK IF IT'S NOT AN ATTACK");
            return json_encode(array('result'=>false,'msg'=>Yii::t("document","Something went wrong with your upload!")));
        }
        
        $res = Document::checkFileRequirements($file, $input, array("dir"=>$dir, "typeEltFolder"=>$folder, "idEltFolder"=>$ownerId) );
        if ($res["result"]) {
            $res = Document::uploadDocument($file, $res["uploadDir"],$input,$rename);
            if ($res["result"]) {
                return Rest::json(array('result'=>true,
                                        "success"=>true,
                                        'name'=>$res["name"],
                                        'dir'=> $res["uploadDir"],
                                        'size'=> (int)filesize ($res["uploadDir"].$res["name"]) ));
                exit;
            }
        }
        
        return Rest::json(array('result'=>false,'msg'=>Yii::t("document","Something went wrong with your upload!")));
    	exit;
	}

}