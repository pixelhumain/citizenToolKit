<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document;
use Authorisation;
use CAction;
use Document;
use Yii;
use function json_encode;

class UpdateAction extends \PixelHumain\PixelHumain\components\Action {
	

	public function run() {
		
        if(strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
            return json_encode(array('result'=>false,'error'=>Yii::t("document","Error! Wrong HTTP method!")));
        }
        $doc=Document::getById($_POST["id"]);
        if(!empty($doc)){
            if (Authorisation::canParticipate(Yii::app()->session["userId"], $doc["type"], $doc["id"])){
                $res = Document::update($_POST["id"], $_POST);
                if ($res) {
                    return json_encode(array('result'=>true,
                                                "message"=>Yii::t("common","Document is well updated"),
                                                'data'=>$_POST
                                                ));
                }else 
                    return json_encode(array('result'=>false,'msg'=>Yii::t("document","Something went wrong with the update!")));
            } else
                return json_encode(array('result'=>false, "msg" => Yii::t("document","You are not allowed to delete this document !"), "id" => $_POST["id"]));
        } else
          return json_encode(array('result'=>false, "msg" => Yii::t("document","Document doesn't exist !"), "id" => $_POST["id"]));
	}

}