<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document;

use CAction, Document, Rest;
use MongoId;

class GetListDocumentsWhereAction extends \PixelHumain\PixelHumain\components\Action {
	
	public function run() {
		if(!empty($_POST["params"]["_id"])){
			if(is_array($_POST["params"]["_id"])){
				$temp = [];
				foreach ($_POST["params"]["_id"] as $k => $v) {
					$temp[] = new MongoId($v);
				}
				$_POST["params"]["_id"] = [];
				$_POST["params"]["_id"]['$in']  = $temp; unset($temp);
			}elseif(is_string($_POST["params"]["_id"])){
				$_POST["params"]["_id"] = new MongoId($_POST["params"]["_id"]);
			}
		}
		$result = [];
		if(!empty($_POST["params"])){
			$result = Document::getListDocumentsWhere($_POST["params"],@$_POST["docType"],@$_POST["limit"], $_POST["indexMin"]);
			foreach ($result as $key => $value) {
				$result[$key]["docPath"] = Document::getDocumentPath($value, true);
			}
		}

		return Rest::json($result);
	}

}