<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document;

use CAction, Document, Rest;
class GetListByIdAction extends \PixelHumain\PixelHumain\components\Action {
	

	public function run($id, $type) {
		$result = Document::getWhere(array("id" => $id,
											"type" => $type));
		return Rest::json($result);
	}

}