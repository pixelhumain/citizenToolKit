<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\document;

use CAction, Rest, Document;
class DeleteDocumentByIdAction extends \PixelHumain\PixelHumain\components\Action {
	

	public function run($id, $forcedUnloggued=null) {
		return Rest::json( Document::removeDocumentById($id, $forcedUnloggued));
	}

}