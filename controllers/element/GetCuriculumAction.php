<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Yii;
class GetCuriculumAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($type, $id) { 
    	$cv = Element::getCuriculum($id, $type);
		//return Rest::json($urls);
		$params = array("curiculum"=>$cv);
		
		$controller = $this->getController();
        return $controller->renderPartial("curiculum", $params, true);
	}
}

?>