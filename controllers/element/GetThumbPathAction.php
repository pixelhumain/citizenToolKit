<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use CAction;
use Element;
use Rest;
use Yii;
use Preference;
use Event;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
class GetThumbPathAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($id=null,$type=null) {
        $controller=$this->getController();
        $el = Element::getByTypeAndId($type,$id);
        $user =  Yii::app()->session["user"];
        if($type!=null && $type == "events"){
        	if(Preference::isActivitypubActivate($user["preferences"])){
           		Utils::federateElement(array('id' => $id, 'attachment' => $el["profilImageUrl"]));
        	}
        }
		return Rest::json(array(
			"profilImageUrl" => $el['profilImageUrl'],
			"profilThumbImageUrl" => $el["profilThumbImageUrl"],
			"profilMarkerImageUrl" => $el["profilMarkerImageUrl"],)); 
    	Yii::app()->end();
    }
}
?>