<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Yii, Element, Exception;
class SaveContactAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run() { 
    	try {
    		if(Yii::app()->request->isAjaxRequest && isset(Yii::app()->session["userId"])){
	            $res = Element::saveContact($_POST);
	            return json_encode( $res );  
	        }
    	} catch (Exception $e) {
    		$res["result"] = false;
    		$res["msg"] = $e->getMessage();
    	}
        
    }
}

?>