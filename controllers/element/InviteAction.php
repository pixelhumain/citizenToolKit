<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use CAction;
use Element;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use Yii;

class InviteAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id=null, $type=null, $session=null) {
    	
        $controller=$this->getController();
        if(!empty(Yii::app()->session["userId"])){
            $views = $controller->costum["htmlConstruct"]["invite"]["views"] ?? "invite";
            $params = array(
                "parentType" => ( empty($type) ? Person::COLLECTION : $type ) ,
                "parentId" => ( empty($id) ? Yii::app()->session["userId"] : $id ) ,
                "search" => true,
                "invitationLinks" => InvitationLink::getLinks($type, $id)
            );

            if($id && $type){
                $params["parent"] = Element::getByTypeAndId($type, $id);
            }

            if(!empty($params["parentType"]) && $params["parentType"] != Person::COLLECTION){
                $parent = Element::getElementById($id, $type, null, array("links", "id"));
                $params["parentLinks"] = ( !empty($parent["links"]) ? $parent["links"] : array() );
                $params["id"] = (empty($parent["id"]) ? $id : $parent["id"]);
            }

            //Rest::json($params); exit ;

            if( !empty( $controller->costum ) && !empty( $controller->costum["roles"])){
                $params["roles"] = $controller->costum["roles"] ;
            } else
                $params["roles"] = array();
            
            if(Yii::app()->request->isAjaxRequest)
                return $controller->renderPartial($views,$params,true);
            else 
               return $controller->render( $views ,$params);
        }
         
    }
}