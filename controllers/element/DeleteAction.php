<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use Action;
use Authorisation;
use Badge;
use CacheHelper;
use CAction;
use Costum;
use Crowdfunding;
use Element;
use Event;
use Form;
use Organization;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use Preference;
use Project;
use Proposal;
use Rest;
use Room;
use Template;
use Cms;
use Yii;;

use Ressource, Classified, Poi;

class DeleteAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run($type, $id)
    {
        $controller = $this->getController();
        $reason = @$_POST["reason"];
        $user = Yii::app()->session["user"];

        if (!Authorisation::canDeleteElement($id, $type, Yii::app()->session["userId"])) {
            return Rest::json(array("result" => false, "msg" => "You are not allowed to delete this element !"));
            return;
        }
        $paramsCostum = array(
            "id" => $id,
            "collection" => $type
        );
        $params = Costum::sameFunction("elementBeforeDelete", $paramsCostum);
        $elemTypes = array(
            Organization::COLLECTION, Organization::CONTROLLER,
            Project::COLLECTION, Project::CONTROLLER,
            Event::COLLECTION, Event::CONTROLLER,
            Proposal::COLLECTION, Proposal::CONTROLLER,
            Action::COLLECTION, Action::CONTROLLER,
            Template::COLLECTION, Template::CONTROLLER,
            Cms::COLLECTION, Cms::CONTROLLER,
            Room::COLLECTION, Room::CONTROLLER
        );

        if ($type == Person::COLLECTION || $type == Person::CONTROLLER) {
            $res = Person::deletePerson($id, Yii::app()->session["userId"]);
        } else if (in_array($type, $elemTypes)) {
            $res = Element::askToDelete($type, $id, $reason, Yii::app()->session["userId"], $elemTypes);
            if (Utils::isActivitypubEnabled()){
                $deleteActivity = ActivitypubActivityCreator::createDeleteActivity($type, $id);
                if($deleteActivity!=null){
                    Handler::handle($deleteActivity);
                }
            }
        } else if (in_array($type, array(Ressource::COLLECTION, Classified::COLLECTION, Poi::COLLECTION, Badge::COLLECTION, Crowdfunding::COLLECTION, Template::COLLECTION, Cms::COLLECTION))) {
            $res = Element::deleteSimple($id, $type, Yii::app()->session["userId"]);
            $this->removeCacheIfBadge($type);
        } else {
            return Rest::json(array("result" => false, "msg" => "Impossible to delete that kind of element " . $type));
            return;
        }

        return Rest::json($res);
    }
    private function removeCacheIfBadge($type)
    {
        $controller = $this->controller;
        if ($controller->costum && @$controller->costum["contextSlug"]) {
            if ($type == Badge::COLLECTION) {
                CacheHelper::delete($controller->costum["contextSlug"]);
                if (CacheHelper::get($_SERVER['SERVER_NAME']))
                    CacheHelper::delete($_SERVER['SERVER_NAME']);
            }
        }
    }
}
