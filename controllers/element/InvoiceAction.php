<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Document, Yii;
class InvoiceAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type, $id) { 
		$controller=$this->getController();
		$params=array("type"=>$type,"id"=>$id, "invoices"=>[]);
		$params["invoices"] = Document::getWhere(array("customerId" => $id, "type" => "pdf"));
		$page = "invoice";
		if(Yii::app()->request->isAjaxRequest)
			return $controller->renderPartial($page,$params,true);
		else 
		return	$controller->render( $page , $params );
	}
}