<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use CAction;
use Element;
use Rest;
use Yii;

class NetworkAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id, $type) {
		$controller=$this->getController();
		// Get format
		$result = Element::myNetwork($id, $type);

		return Rest::json($result);
    }
}

?>