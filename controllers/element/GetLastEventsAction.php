<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Rest, Yii;
class GetLastEventsAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run() { 
    	//$controller=$this->getController();
		$element = Element::getLastEvents($_POST["col"], $_POST["id"], (Float)$_POST["nbEvent"], $_POST["startDateUTC"], @$_POST["tags"]);
		return Rest::json($element);
	}
}

?>