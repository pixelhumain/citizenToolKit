<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Yii, Rest;
class DeleteDataAction extends \PixelHumain\PixelHumain\components\Action {

	public function run() {

		//$controller = $this->getController();
		$params = $_POST ;
		$params["date"] = time();
		$res = Yii::app()->mongodb->selectCollection("remove")->insert( $params);
		

		return Rest::json($res);
	}
}