<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Yii, Person, ActStr, ActivityStream, Rest;
class GetNotificationsAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Notifications By Element and User
*/
    public function run($type,$id) { 
        $res = array();
        if( Yii::app()->session["userId"] )
        {
          if($type != Person::COLLECTION){
            $params = array(
              '$and'=> 
                array(
                  array("notify.id.".Yii::app()->session["userId"] => array('$exists' => true),
                  "verb" => array('$ne' => ActStr::VERB_ASK)),
                  array('$or'=> array(
                    array("target.type"=>$type, "target.id" => $id),
                    array("target.parent.type"=>$type, "target.parent.id" => $id)
                    )
                  ) 
                ) 
              );
          }else
            $params = array("notify.id.".Yii::app()->session["userId"] => array('$exists' => true));
            $res = ActivityStream::getNotificationsByTypeAndId($params);
        } else
            $res = array('result' => false , 'msg'=>'something somewhere went terribly wrong');
            
        return Rest::json($res,false);
    }
}

?>