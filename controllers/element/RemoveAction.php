<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use Action;
use ActivityStream;
use CAction;
use Classified;
use Cron;
use Crowdfunding;
use Event;
use Form;
use MongoId;
use Organization;
use Person;
use PHDB;
use Poi;
use Project;
use Proposal;
use Survey;
use Yii;

class RemoveAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($id) {
		$controller = $this->getController();
		$params = PHDB::findOneById(Cron::ASK_COLLECTION, $id );
		$res = array("result" => false);
		if(!empty($params)){
			$res = array("result" => true);
			if( !empty($params["removeMail"]) && 
				( $params["removeMail"] == "true" || $params["removeMail"] === true ) ){
				$col = array(
					Organization::COLLECTION,
					Project::COLLECTION,
					Event::COLLECTION,
					Poi::COLLECTION,
					Form::COLLECTION,
					Form::ANSWER_COLLECTION,
					Survey::COLLECTION,
					Proposal::COLLECTION,
					Classified::COLLECTION,
					Action::COLLECTION,
					Crowdfunding::COLLECTION
				);

				foreach ($col as $k => $v) {
					$where = array( "email" => $params["email"]);
					$action = array('$set' => array("email" => ""));

					$elt = PHDB::find($v, $where, array("name"));
					foreach ($elt as $keyElt => $valueElt) {
					    PHDB::update($v, array("_id"=>new MongoId($keyElt)), $action);
					}

					$whereC = array( "contacts.email" => $params["email"]);

					$eltC = PHDB::find($v, $whereC, array("name", "contacts"));

					foreach ($eltC as $keyElt => $valueElt) {
						$c = array();


						foreach ($valueElt["contacts"] as $keyC => $valC) {
							if( empty($valC["email"]) || $valC["email"] != $params["email"]){
								$c[] = $valC;
							}
						}
						$actionC = array('$set' => array("contacts" => $c));
					    PHDB::update($v, array("_id"=>new MongoId($keyElt)), $actionC);
					}
				}
				$person = PHDB::findOne(Person::COLLECTION, $where, array("name", "roles"));
				if(!empty($person) && !empty($person["roles"]) && !empty($person["roles"]["tobeactivated"])){
					Person::deletePerson((String) $person["_id"], Yii::app()->session["userId"], true);
				}

				$res["res"][]= array("result" => true, "type" => "removeMail", "msg" => Yii::t("common","Your email has been deleted from all items") );
			}

			if( !empty($params["notMail"]) && ($params["notMail"] == "true" || $params["notMail"] === true ) ){
				//$mails = array("email" => $params["email"], "date" => time());
				ActivityStream::saveNotSendMail($params["email"]);
				//Yii::app()->mongodb->selectCollection("mails")->insert( $mails);
				$res["res"][] = array(
									"result" => true,
									"type" => "notMail",
									"msg" => Yii::t("common","You will no longer receive any emails from the platform") );
			}

			//PHDB::remove( Cron::ASK_COLLECTION, array( "_id"=>new MongoId($id) ) );
		}else{
			$res["msg"] = Yii::t("common","You have not made any request or the request has expired");
		}
		//Rest::json($res); exit;
		return $controller->renderPartial("remove", $res);
	}
}