<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Person, Element, CacheHelper, CTKException, Rest, Form, Yii, Crowdfunding, Exception, MongoDate, DateTime, Costum, Log;
use DataValidator;
use DateTimeZone;
use MongoId;
use PHDB;

/**
 * Update an information field for a person
 *
 * //SAVE an attribute
 * var formData={"id":userId,"collection":"citoyens",path : "test","value":["OOO","frrfrf"] };
 *
 * //DELETE an element FROM an array
 * var formData={ "id":userId, "collection":"citoyens", path : "test.0", pull:"test","value":null};
 *
 * //an element to an array
 * var formData={"id":userId,"collection":"citoyens","path":"test","arrayForm":true,"value":"eeeyyy"};
 *
 *
 * dataHelper.path2Value( formData, function(params) {
 * toastr.success("Favoris bien supprimé");
 * });
 */
class UpdatePathValuedAction extends \PixelHumain\PixelHumain\components\Action {

	public function run() {
		\Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
		\Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
		\Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
		//check user input to prevent xss injection
		$_POST = DataValidator::clearUserInput($_POST);

		$controller = $this->getController();
		if (Person::logguedAndAuthorized()) {
			// setType change a value in a defined path to a new value with a defined type
			// if setType is a string, apply directly the type
			// else map every value in the array to apply type
			if (isset($_POST["updatePartial"]) && $_POST["updatePartial"] == true && isset($_POST["setType"])) {
				$temp = [];
				foreach ($_POST["value"] as $k => $v) {
					if (is_array($_POST["setType"]))
						$temp[$k] = $this->parseValueBySetType($this->arrayGetValueBySpecificPath($_POST["setType"], 'type', 'path', $k), $v);
					else
						$temp[$k] = $this->parseValueBySetType($_POST["setType"], $v);
				}
				$_POST["value"] = $temp;
			} elseif (!empty($_POST["setType"]) && !empty($_POST["value"]))
				$_POST["value"] = $this->parseValueBySetType($_POST["setType"], $_POST["value"]);
			//if(isset($_POST["format"]) && $_POST["format"] == true && gettype($_POST["value"]) == "array")
			array_walk_recursive(
				$_POST,
				function (&$value) {
					if ($value == "true")
						$value = true;
					else if ($value == "false")
						$value = false;
				}
			);

			// Save Logs

			if (!empty($_POST["saveLog"])) {
				//var_dump($_POST);

				$dataSend =  array(
					"userId"         =>  Yii::app()->session["userId"],
					"userName"       =>  Yii::app()->session["user"]["name"],
					"costumId"       =>  $_POST["saveLog"]["costumId"],
					"costumSlug"     =>  $_POST["costumSlug"],
					"costumEditMode" =>  $_POST["costumEditMode"],
					"action"         => @$_POST["saveLog"]["action"],
					"blockPath"      => @$_POST["saveLog"]["blockPath"],
					"blockName"      => @$_POST["saveLog"]["blockName"],
					"collection"     => @$_POST["collection"],
					"page"           => @$_POST["saveLog"]["page"],
					"created"        =>  time()
				);
				if ($_POST["saveLog"]["action"] == "costum/editBlock") {
					$dataSend["idBlock"] = $_POST["id"];
					$dataSend["path"]    = $_POST["saveLog"]["path"];
					$dataSend["keyPage"] = $_POST["saveLog"]["keyPage"];
					$dataSend["blockType"] = $_POST["saveLog"]["blockType"];
					$dataSend["kunik"] = $_POST["saveLog"]["kunik"];
					$dataSend["value"] = DataValidator::restructureArray($_POST["saveLog"]["value"] ?? []);
					$dataSend["olderData"]  = DataValidator::restructureArray($_POST["saveLog"]["olderData"] ?? []);
					if (isset($_POST["saveLog"]["subAction"]))
						$dataSend["subAction"] = $_POST["saveLog"]["subAction"];
				} else if (($_POST["saveLog"]["action"] == "costum/options")) {
					$dataSend["path"]    = $_POST["saveLog"]["path"];
					$dataSend["value"] = $_POST["saveLog"]["value"] ?? [];
					unset($dataSend["blockPath"]);
					unset($dataSend["blockName"]);
					unset($dataSend["page"]);
				} elseif ($_POST["saveLog"]["action"] == "costum/addBlock") {
					$dataSend["idBlock"] = $_POST["id"];
				}

				Log::saveAndClean(
					$dataSend
				);
			} elseif ($_POST["collection"] == "cms" && !empty($_POST["costumSlug"]) && !empty($_POST["id"]) && !empty($_POST["path"]) && $_POST["costumEditMode"]) {
				Log::saveAndClean(
					array(
						"userId"         =>  Yii::app()->session["userId"],
						"userName"       =>  Yii::app()->session["user"]["name"],
						"costumSlug"     =>  $_POST["costumSlug"],
						"costumEditMode" =>  $_POST["costumEditMode"],
						"action"         =>  "costum/editBlock",
						"collection"     =>  $_POST["collection"],
						"created"        =>  time(),
						"value"          =>  $_POST["value"],
						"idBlock"        =>  $_POST["id"],
						"path"           =>  $_POST["path"]
					)
				);
			}

			if (!empty($_POST["collection"])) {
				if (!empty($_POST["id"]) && !empty($_POST["path"])) {
					try {
						//updatePathValue($collection, $id, $path, $value, $arrayForm=null, $aedit=null, $pull=null) {
						if (isset($_POST["value"]) && ($_POST["value"] === "true" || $_POST["value"] === "false"))
							$_POST["value"] = ($_POST["value"] === 'true') ? true : false;
						$res = Element::updatePathValue($_POST["collection"], $_POST["id"], $_POST["path"], @$_POST["value"], @$_POST["arrayForm"], @$_POST["edit"], @$_POST["pull"], @$_POST["formParentId"], @$_POST["updatePartial"], @$_POST["tplGenerator"], @$_POST["renameKey"]);

						/*if($controller->costum && @$controller->costum["contextSlug"]){
                                if(isset($_POST["removeCache"]) && $_POST["removeCache"] == true){
                                    CacheHelper::delete($controller->costum["contextSlug"]);
                                    if(CacheHelper::get($_SERVER['SERVER_NAME']))
                                        CacheHelper::delete($_SERVER['SERVER_NAME']);
                                }
                            }*/
						if (isset($_POST["updateCache"]) && $_POST["updateCache"] == true) {
							if ($controller->costum && @$controller->costum["contextSlug"]) {
								Costum::forceCacheReset();
							}
						}
					} catch (CTKException $e) {
						return Rest::json(array("result" => false, "msg" => $e->getMessage(), $_POST["path"] => $_POST["value"]));
					}
				} else {
					if (!empty($_POST["value"])) {
						if (in_array($_POST["collection"], [Form::COLLECTION, Form::INPUTS_COLLECTION])) {
							$_POST["value"]["created"] = time();
							$_POST["value"]["creator"] = Yii::app()->session["userId"];
							Yii::app()->mongodb->selectCollection($_POST["collection"])->insert($_POST["value"]);
							return Rest::json(array(
								"result" => true,
								"msg" => Yii::t("common", "Information saved"),
								"saved" => $_POST["value"]
							));
						} else if ($_POST["collection"] == Crowdfunding::COLLECTION) {
							$_POST["value"]["created"] = time();
							$_POST["value"]["creator"] = Yii::app()->session["userId"];
							$saved = Yii::app()->mongodb->selectCollection($_POST["collection"])->insert($_POST["value"]);
							return Rest::json(array("result" => true, "msg" => Yii::t("common", "Information saved"), "saved" => $saved));
						} else {
							// $_POST["value"]["created"] = time();
							// $_POST["value"]["creator"] = Yii::app()->session["userId"];
							$params = $_POST["value"];
							$params["collection"] = $_POST["collection"];
							if (isset($_POST["id"]))
								$params["id"] = $_POST["id"];
							try {
								if (!isset($params["name"]) && !empty($params['path']))
									$params["name"] = $params["collection"] . $params["path"];
								$res = Element::save($params);
								if (!empty($params['type']) && $params['type'] == "template" && isset($params["cmsList"])) {
									foreach ($params["cmsList"] as $key => $value) {
										PHDB::update("cms", ["_id" => new MongoId($value)], [
											'$set' => [
												"tplParent" => (string)$params["id"],
												"haveTpl"   => "true"
											]
										]);
									}
								}

								return Rest::json(array("result" => true, "msg" => Yii::t("common", "Information saved"), "saved" => $res));
							} catch (Exception $e) {
								$res = array("result" => false, "error" => "401", "msg" => $e->getMessage());
								return Rest::json($res);
							}
						}

						//$saved = PHDB::insert( $_POST["collection"], $_POST["value"]);
					} else
						return Rest::json(["result" => false, "msg" => "Value cannot be empty"]);
				}
			} else {
				return Rest::json(array("result" => false, "msg" => Yii::t("common", "Invalid request")));
			}
			return Rest::json(array(
				"result" => true,
				"msg" => Yii::t("common", "Information updated"),
				"text" => $_POST["value"],
				"id"     => $_POST["id"],
				"elt" => $res["elt"]
			));
		} else
			return Rest::json(array("result" => false, "msg" => Yii::t("common", "Please Login First")));
	}

	function string_set_type($tosetvalue, $type) {
		if (!empty($type) && $tosetvalue != "") {
			if ($type === "int")
				return intval($tosetvalue);
			if ($type === "float")
				return floatval($tosetvalue);
			if (in_array($type, ["boolean", "bool"]))
				return filter_var($tosetvalue, FILTER_VALIDATE_BOOLEAN);
			if ($type === "isoDate") {
				if ($tosetvalue !== "now")
					$tosetvalue = str_replace('/', '-', $tosetvalue);
				$transform = [];
				$datetime = new DateTime($tosetvalue);
				$transform[] = $datetime->getTimestamp();
				$datetime->setTimezone(new DateTimeZone("UTC"));
				$transform[] = $datetime->getTimestamp();
				return new MongoDate($datetime->getTimestamp());
			}
			if ($type === "timestamp") {
				if (strpos($tosetvalue, "T") !== false)
					$date = DateTime::createFromFormat("Y-m-d\TH:i", $tosetvalue);
				else
					$date = new DateTime($tosetvalue);
				return $date->getTimestamp();
			}
		}
		return null;
	}

	function array_get_value(array &$array, $path, $glue = '.') {
		if (!is_array($path)) {
			$path = explode($glue, $path);
		}

		$ref = &$array;

		foreach ((array)$path as $parent) {
			if (is_array($ref) && array_key_exists($parent, $ref)) {
				$ref = &$ref[$parent];
			} else {
				return null;
			}
		}
		return $ref;
	}

	function array_set_value(array &$array, $path, $value, $glue = '.') {
		if (!is_array($path)) {
			$path = explode($glue, (string)$path);
		}

		$ref = &$array;

		foreach ($path as $parent) {
			if (isset($ref) && !is_array($ref)) {
				$ref = array();
			}

			$ref = &$ref[$parent];
		}

		$ref = $value;
	}

	function array_unset_value(&$array, $path, $glue = '.') {
		if (!is_array($path)) {
			$path = explode($glue, $path);
		}

		$key = array_shift($path);

		if (empty($path))
			unset($array[$key]);
		else
			$this->array_unset_value($array[$key], $path);
	}

	function arrayGetValueBySpecificPath(array &$array, $pathToGet, $pathFoSearch, $valueOfPathForSearch) {
		$keys = array_column($array, $pathFoSearch);
		$index = array_search($valueOfPathForSearch, $keys);
		if (!isset($array[$index][$pathToGet])) {
			die();
		}
		return $index !== false && isset($array[$index][$pathToGet]) ? $array[$index][$pathToGet] : null;
	}

	function parseValueBySetType($setType, $value) {
		if (gettype($setType) === 'string') {
			$value = $this->string_set_type($value, $setType);
		} else {
			foreach ($setType as $idsetType => $valuesetType) {
				if (isset($valuesetType["path"]) && isset($valuesetType["path"])) {
					$setTypeValue = $this->string_set_type($this->array_get_value($value, $valuesetType["path"], "."), $valuesetType["type"]);
					$this->array_set_value($value, $valuesetType["path"], $setTypeValue, ".");
				}
			}
		}
		return $value;
	}
}
