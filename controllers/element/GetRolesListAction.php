<?php

    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

    use  Element, Rest;
    class GetRolesListAction extends \PixelHumain\PixelHumain\components\Action { 
        public function run() {         
            $element = Element::getByTypeAndId($_POST["type"], $_POST["id"]);
            $links = @$element["links"];
            $roleslists = Element::getRolesList($links, $_POST["type"], $_POST["id"]);
            return Rest::json($roleslists);
        }
    }

?>