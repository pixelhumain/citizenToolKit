<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Network, Rest, Yii;
class GetNetworksAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($type, $id) { 
    	$networks = Network::getListNetworkByUserId($id);
    	//var_dump($networks);
		return Rest::json($networks);
	}
}

?>