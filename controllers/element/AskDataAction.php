<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use CAction;
use Yii;

class AskDataAction extends \PixelHumain\PixelHumain\components\Action {

    public function run() {
        $controller = $this->getController();
        //$controller->layout = "//layouts/mainSearch";

        if(Yii::app()->request->isAjaxRequest)
			return  $controller->renderPartial("askData");
        else 
		return	$controller->render( "askData");
    }
}