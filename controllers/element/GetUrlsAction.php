<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Rest, Yii;
class GetUrlsAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($type, $id) { 
    	$urls = Element::getUrls($id, $type);
		return Rest::json($urls);
	}
}

?>