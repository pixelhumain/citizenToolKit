<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Rest, Authorisation, Yii, MongoId,Person;
use PHDB;

class MergeDataAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($element = "", $id="", $into="", $merge = false, $deleteAfterMerge = false) { 
		if ( ! Person::logguedAndValid() ) {
            $this->getController()->layout = "//layouts/empty";
			return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in","text"=> Yii::t('survey', 'You must sign in to access in this features')));
        }

		if ( !Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) {
			$this->getController()->layout = "//layouts/empty";
			return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>"Please Login as Superadmin First","icon"=>"fa-sign-in","text"=> "You must be logged as an Superadmin user to do this action !"));
        }

		
		$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
		if($element == " " && $id == " " && $into){
			echo "paramètre invalide ou incomplète!";
		}
		else if($merge){
			$elementToMerge = Element::getByTypeAndId( $element, new MongoId($into));
			$elementToDelete = Element::getByTypeAndId( $element, new MongoId($id));
			if( (isset($elementToDelete["slug"]) && $elementToDelete["slug"] == "unknown") || (isset($elementToMerge["slug"]) && $elementToMerge["slug"] == "unknown")){
				if($elementToDelete["slug"] == "unknown")
					echo "element to delete non trouvé ";
				if($elementToMerge["slug"] == "unknown")
					echo "element to merge non trouvé ";
			}
			else{
				$arrayToMerge = Self::mergeArray($elementToMerge, $elementToDelete);

				$merge = Element::merge($element, $arrayToMerge, $elementToDelete);
				if($deleteAfterMerge == "true"){
					Element::deleteElement($element,  $elementToDelete["_id"], "doublons", Yii::app()->session["userId"]);
				}
				return Rest::json($merge);
			}
		}
		else{
			$elementToMerge = Element::getByTypeAndId( $element, new MongoId($into));
			$elementToDelete = Element::getByTypeAndId( $element, new MongoId($id));
			
			if((isset($elementToDelete["slug"]) && $elementToDelete["slug"] == "unknown") || (isset($elementToMerge["slug"]) && $elementToMerge["slug"] == "unknown")){
				if($elementToDelete["slug"] == "unknown")
					echo "element to delete non trouvé ";
				if($elementToMerge["slug"] == "unknown")
					echo "element to merge non trouvé ";
			}
			else{
				$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
				$controller=$this->getController(); 

				if(Yii::app()->request->isAjaxRequest){
					return Rest::json(["elementToMerge" => $elementToMerge, "elementToDelete" => $elementToDelete]);
				}

				$params = array(
					"id" => $id,
					"assetsPath"  =>  $controller->module->getParentAssetsUrl(),
					"baseUrl"   => $baseUrl ,
					"into"   => $into ,
					"type"   => $element ,
					"dateLeft" => " ",
					"dateRight" => " ",
				);

				if(isset($elementToDelete["updated"])){
					$params["dateLeft"] = Date("Y-m-d", $elementToDelete["updated"]);
				}

				if(isset($elementToMerge["updated"])){
					$params["dateRight"] = Date("Y-m-d", $elementToMerge["updated"]);
				}
				
				return $controller->renderPartial("merge", $params, true);
			}
			
		}
	}

	private function mergeArray($arrayPrio, $arrayToDeleteAfterMerge){
		foreach($arrayToDeleteAfterMerge as $key  => $data){
			if(!isset($_POST["resultDelete"]) || (isset($_POST["resultDelete"]) && !in_array($key, $_POST["resultDelete"])  ) || $key == "geo" || $key == "geoPosition"){
				if($key != "_id"){
					if(isset($_POST["left"]) && in_array($key, $_POST["left"])){
						if(isset($_POST["right"]) && is_array($data) && in_array($key, $_POST["right"])){
							$tmp = [];

							if(isset( $arrayPrio[$key]))
								$tmp = Self::mergeTags($tmp, $arrayPrio[$key]);
								
							$tmp = Self::mergeTags($tmp, $data);
							$arrayPrio[$key] = $tmp;
							unset($tmp);
						}
						else{
							$arrayPrio[$key] = $data;
						}
							
					}
					else if((is_object($arrayPrio) && property_exists($key , $arrayPrio)) || (is_array($arrayPrio) && array_key_exists($key , $arrayPrio))){		
						if(!is_array($data) && !is_object($data) && !is_array($arrayPrio[$key]) && !is_object($arrayPrio[$key])){
							
							if(isset($_POST["left"]) && in_array($data, $_POST["left"])){
								$arrayPrio[$key] = $data ;
							}
							if($data != $arrayPrio[$key]){	
								if($arrayPrio[$key] == "undefined")	
									$arrayPrio[$key] = $data ;
							}
						}
						else{
							if($key == "tags"){
								$tmp = [];

								if(isset( $arrayPrio[$key]))
									$tmp = Self::mergeTags($tmp, $arrayPrio[$key]);
								
								$tmp = Self::mergeTags($tmp, $data);
								$arrayPrio[$key] = $tmp;
								unset($tmp);
							}
							else
								$arrayPrio[$key] = Self::mergeArray($arrayPrio[$key], $data );
						}
					}	
					else{
						if(!is_object($arrayPrio))
							$arrayPrio[$key] = $data;	
					}	
				}
			}
			else{
				if(!is_object($arrayPrio))
					$arrayPrio[$key] = '$unset';
			}
		} 
		return $arrayPrio;
	}

	private function mergeTags($result = [], $tags){
		for($i = 0; $i < count($tags); $i++){
			if(isset($tags[$i])){
				if(!in_array($tags[$i], $result))
					array_push($result, $tags[$i]);
			}
		}
		return $result;
	}

}

?>