<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use Event;
use Badge;
use CacheHelper;
use CAction;
use Costum;
use Element;
use Exception;
use Person;
use Rest;
use Thing;
use Yii;
use PHDB;
use Preference;
use Organization;
use function getallheaders;

class SaveAction extends \PixelHumain\PixelHumain\components\Action
{
    /**
     * Save an Element
     * Element can be : Organization, Event, Projets
     * See databinding of different types of element for the format of the data
     */

    public function run()
    {

        $res = array("result" => false, "error" => "401", "msg" => Yii::t("common", "Login First"));
        if (Costum::isSameFunction("elementActionBeforeSave"))
            Costum::sameFunction("elementActionBeforeSave");
        if (Person::logguedAndAuthorized()) {
            // Old condition :: collection url qui passe sans validation de user //
            //if(Person::logguedAndValid() || (isset($_POST["collection"]) && $_POST["collection"] == "url")) {
            $toSave = array();
            $contextData = array();

            if ($_SERVER['REQUEST_METHOD'] == 'PUT' || $_SERVER['REQUEST_METHOD'] == 'POST') {
                $headers = getallheaders();

                if (
                    array_key_exists('X-SmartCitizenData', $headers)
                    && array_key_exists('X-SmartCitizenMacADDR', $headers)
                ) {

                    $toSave = Thing::fillSmartCitizenData($headers);
                } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $toSave = $_POST;
                }

                try {
                    if(isset($toSave['federateElement'])){
                        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
                        if (Preference::isActivitypubActivate($user["preferences"])) {
                             if ((isset($toSave['collection']) && in_array($toSave['collection'], ['events', 'projects','badges'])) || (isset($toSave['typeElement']) && in_array($toSave['typeElement'], ['events', 'projects']))) {
                                if (isset($toSave["public"]) &&  ($toSave["public"] == "true" || $toSave["public"])) {
                                    $toSave = $this->getToSave($toSave);
                                }
                            }
                        }
                    }
                    $res = Element::save($toSave);

                    $this->removeCacheIfBadge();
                } catch (Exception $e) {
                    $res = array("result" => false, "error" => "401", "msg" => $e->getMessage());
                }

                //var_dump($res);exit();
                $res['resquest'] = $_SERVER['REQUEST_METHOD'];
            }
        }
        return Rest::json($res);
    }
    private function removeCacheIfBadge()
    {
        $controller = $this->controller;
        if ($controller->costum && @$controller->costum["contextSlug"]) {
            if ($_POST["collection"] == Badge::COLLECTION) {
                CacheHelper::delete($controller->costum["contextSlug"]);
                if (CacheHelper::get($_SERVER['SERVER_NAME']))
                    CacheHelper::delete($_SERVER['SERVER_NAME']);
            }
        }
    }

    /**
     * @param array $toSave
     * @return array
     */
    public function getToSave(array $toSave): array
    {
        $uuid = Utils::federateElement($toSave);
        if (isset($uuid["object"]) && $uuid["object"] != null) {
            $toSave["objectId"] = $uuid["object"];
        }
        if (isset($uuid["actor"]) && $uuid["actor"] != null) {
            $toSave["attributedTo"] = $uuid["actor"];
        }
        return $toSave;
    }
}
