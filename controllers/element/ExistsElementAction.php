<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use Element;
use MongoId;
use Osm;
use Rest, Organization, Project;

class ExistsElementAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/

    public function run() { 
    	$controller=$this->getController();
        $res = array("result" => false, "elt" => []);
        $params = $_POST;
        if(isset($params["collection"])) {
            if($params["collection"] != "all") {
                $result = $this->getElement($params["collection"], $params);
                
                if($result != null && count($result) > 0) {
                    $res["result"] = true;
                    foreach($result as $elt) {
                        $res["elt"] = $elt;
                    }
                    
                }
            }
            else {
                $allCollection = [
                    Organization::COLLECTION,
                    Project::COLLECTION,
                ];
                
                foreach($allCollection as $collection) {
                    $result = $this->getElement($collection, $params);
                    if($result != null && count($result) > 0) {
                        $res["result"] = true;
                        foreach($result as $elt) {
                            $res["elt"] = $elt;
                        }
                        
                    }
                }
                if(!(count($res["elt"]) > 0)){
                    $geoData = Osm::getAdressOfElementByLatAndLon($params["lat"], $params["long"]);
                    $res["geo"] = $geoData;
                }
            }
        }
    	return Rest::json($res);
    }

    private function getElement($collection, $params) {
        $where = array();
        if(isset($params["id"]))
            $where["_id"] = new MongoId($params["id"]);
        else{
            if(isset($params["name"]))
                $where["name"] = $params["name"];
            if(isset($params["slug"]))
                $where["slug"] = $params["slug"];
            if(isset($params["email"]))
                $where["email"] = $params["email"];
        }

        if(count($where) > 1)
            $where = array('$and' => $where);

        $elements =  Element::getByWhere($collection, $where);
        foreach($elements as $key => $element) {
            if(isset($element["links"]) && $element["links"]) {
                $tmplinks = $element["links"];
                $elements[$key]["links"] = array_merge(Element::getAllLinksOld($tmplinks, $collection, $element["_id"]), Element::getAllLinks($tmplinks, $collection, $element["_id"]));
            }  
        }
            
        return $elements;
    }
}
