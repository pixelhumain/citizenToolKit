<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use Element;
use PHDB;
use Rest;

class GetContextAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($contextId = '', $contextType = '') {
        $element = PHDB::findOneById($contextType, $contextId);
        $res = array();
        if(!empty($element)) 
            $res = Element::getElementForJS($element, $contextType);
        return Rest::json($res);
    }
}
