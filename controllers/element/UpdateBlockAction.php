<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use CAction;
use CTKException;
use Element;
use Rest;
use Yii;
use Preference;
use Event;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
/**
* Update an information field for a element
*/
class UpdateBlockAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        if(!empty($_POST["block"])) {
			try {
            $user =  Yii::app()->session["user"];

            if(Utils::isActivitypubEnabled()){
                Utils::federateElement($_POST);
            }
				$res = Element::updateBlock($_POST);
				return Rest::json($res);
			} catch (CTKException $e) {
				return Rest::json(array("result"=>false, "msg"=>$e->getMessage(), "data"=>$_POST));
			}
		}
		return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Invalid request")));
        
    }
}