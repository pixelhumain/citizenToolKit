<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Person, Authorisation, Yii, CTKException, PHDB, MongoId, CacheHelper, Rest;
use DataValidator, Log , Costum;
class UnsetPath extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        if ( Person::logguedAndAuthorized() ) 
        {
            if (isset($_POST["id"]) && isset($_POST["collection"]) && isset($_POST["path"])) {
                $id = $_POST["id"];
                $collection = $_POST["collection"];
                $path = $_POST["path"];
                if ( !Authorisation::isInterfaceAdmin($id, $collection) && 
                    !Authorisation::isParentAdmin($id, $collection , Yii::app()->session["userId"]) &&
                    !Authorisation::canEditItemOrOpenEdition($id, $collection, Yii::app()->session['userId']) && 
                    !Authorisation::specificCondition() ) {
                    throw new CTKException(Yii::t("common","Can not update the element : you are not authorized to update that element !"));
                }

                if (!empty($_POST["saveLog"])) {
                    $dataSend =  array(
                        "userId"         =>  Yii::app()->session["userId"],
                        "userName"       =>  Yii::app()->session["user"]["name"],
                        "costumId"       =>  $_POST["saveLog"]["costumId"],
                        "costumSlug"     =>  $_POST["costumSlug"],
                        "costumEditMode" =>  $_POST["costumEditMode"],
                        "action"         => @$_POST["saveLog"]["action"],
                        "blockPath"      => @$_POST["saveLog"]["blockPath"],
                        "blockName"      => @$_POST["saveLog"]["blockName"],
                        "collection"     => @$_POST["collection"],
                        "page"           => @$_POST["saveLog"]["page"],
                        "created"        =>  time()
                    );
                    if ($_POST["saveLog"]["action"] == "costum/editBlock") {
                        $dataSend["idBlock"] = $_POST["id"];
                        $dataSend["path"]    = $_POST["saveLog"]["path"];
                        $dataSend["keyPage"] = $_POST["saveLog"]["keyPage"];
                        $dataSend["blockType"] = $_POST["saveLog"]["blockType"];
                        $dataSend["kunik"] = $_POST["saveLog"]["kunik"];
                        $dataSend["value"] = DataValidator::restructureArray($_POST["saveLog"]["value"] ?? []);
                        $dataSend["olderData"]  = DataValidator::restructureArray($_POST["saveLog"]["olderData"] ?? []);
                        if (isset($_POST["saveLog"]["subAction"]))
                            $dataSend["subAction"] = $_POST["saveLog"]["subAction"];
                    }elseif ($_POST["saveLog"]["action"] == "costum/addBlock") {
                        $dataSend["idBlock"] = $_POST["id"];
                    }
                    
                    Log::saveAndClean(
                    $dataSend
                    );
                } elseif ($_POST["collection"] == "cms" && !empty($_POST["costumSlug"]) && !empty($_POST["id"]) && !empty($_POST["path"]) && $_POST["costumEditMode"]) {
                    Log::saveAndClean(
                        array(
                            "userId"         =>  Yii::app()->session["userId"],
                            "userName"       =>  Yii::app()->session["user"]["name"],
                            "costumSlug"     =>  $_POST["costumSlug"],
                            "costumEditMode" =>  $_POST["costumEditMode"],
                            "action"         =>  "costum/editBlock",
                            "collection"     =>  $_POST["collection"],
                            "created"        =>  time(),
                            "value"          =>  @$_POST["value"],
                            "idBlock"        =>  $_POST["id"],
                            "path"           =>  $_POST["path"]
                        )
                    );
                }

                
                try{
                    $return =  PHDB::update( $collection,
                                    [ "_id" => new MongoId($id) ], 
                                    [ '$unset' => [$path =>true ] ]);

                    if($controller->costum && @$controller->costum["contextSlug"]){
                            CacheHelper::delete($controller->costum["contextSlug"]);
                    }
                    return Rest::json(array("result"=>true, "msg"=> Yii::t("cooperation", "processing delete ok"),"deleted"=>$return));
                }catch (CTKException $e) {
                    return Rest::json(array("result"=>false, "msg"=>$e->getMessage(), $path));
                }
            }
        } else 
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Please Login First")));

    }
}