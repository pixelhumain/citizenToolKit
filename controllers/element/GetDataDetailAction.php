<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Yii, Authorisation, CTKException, Person, MongoId, Event, PHDB, Preference, Project, Organization, Classified, Poi, ActionRoom, Proposal, Cooperation, Translate, Rest;
use Link;

class GetDataDetailAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/

    public function run($type, $id, $dataName, $sub=null, $isInviting=null, $limit=null, $tags=null, $sort=null) {
    	//$controller=$this->getController();

    	$contextMap = array();
		$element = @$type != "0" ? Element::getByTypeAndId(@$type, @$id) : null;
		$edit= (isset($type) && !empty($type) && isset(Yii::app()->session["userId"])) ? Authorisation::isElementAdmin($id, $type , Yii::app()->session["userId"]) : null; 
		if($dataName == "follows" || $dataName == "followers" || 
			$dataName == "members" || $dataName == "attendees" ||$dataName == "friends" ||
			$dataName == "contributors" || $dataName =="guests"){
			$connector=$dataName;
			if($dataName=="guests"){
				$connector=Element::$connectTypes[@$type];
			}
			if(isset($element["links"][$connector])){
				foreach ($element["links"][$connector] as $keyLink => $value){
					
					//var_dump($keyLink);
					try {
						$link = Element::getByTypeAndId($value["type"], $keyLink);
					} catch (CTKException $e) {
						error_log("The element ".$id."/".$type." has a broken link : ".$keyLink."/".$value["type"]);
						continue;
					}
					if(@$value["roles"] && !empty($value["roles"])){ 
			            $link["rolesLink"]=[]; 
			            $link["rolesLink"]=$value["roles"]; 
			        } 
			        if(!empty($edit)) $link["edit"]=$dataName;
			        if(isset($link["_id"])){
						if($dataName=="guests" && @$value["isInviting"]){
							//$link["type"] = $value["type"];
							$link["isInviting"] = $value["isInviting"];
							$contextMap[$keyLink] = $link;
						}else if($dataName!="guests" && !@$value["isInviting"]){
							//$link = Element::getByTypeAndId($value["type"], $keyLink);
							if($value["type"]==Person::COLLECTION){
								$link["statusLink"]=[];
								if(@$value[Link::TO_BE_VALIDATED])
									$link["statusLink"][Link::TO_BE_VALIDATED]=true;
								if(@$value[Link::IS_ADMIN])
									$link["statusLink"][Link::IS_ADMIN]=true;
								if(@$value[Link::IS_ADMIN_PENDING])
									$link["statusLink"][Link::IS_ADMIN_PENDING]=true;
								if(@$value[Link::IS_ADMIN_INVITING])
									$link["statusLink"]["isAdminInviting"]=true;
							}
							//$link["type"] = $value["type"];
							$contextMap[$keyLink] = $link;
						}else if($dataName!="guests" && !empty($isInviting)){
							//$link = Element::getByTypeAndId($value["type"], $keyLink);
							if($value["type"]==Person::COLLECTION){
								$link["statusLink"]=[];
								if(@$value[Link::TO_BE_VALIDATED])
									$link["statusLink"][Link::TO_BE_VALIDATED]=true;
								if(@$value[Link::IS_ADMIN])
									$link["statusLink"][Link::IS_ADMIN]=true;
								if(@$value[Link::IS_ADMIN_PENDING])
									$link["statusLink"][Link::IS_ADMIN_PENDING]=true;
								if(@$value[Link::IS_ADMIN_INVITING])
									$link["statusLink"]["isAdminInviting"]=true;
							}
							//$link["type"] = $value["type"];
							$contextMap[$keyLink] = $link;
						}
					}else
           				Link::disconnect($id, $type, $keyLink, $value["type"], null, $connector);
				}
			}
		}

		//Rest::json($contextMap); exit;
		if($dataName == "links"){
			$links=@$element["links"];
			$contextMap = Element::getAllLinks($links,$type, $id);
		}

		if($dataName == "events"){ //var_dump($element["links"]); exit;

			$arrayIdEvents = array();
			if(isset($element["links"]["events"])){
				foreach (array_reverse($element["links"]["events"]) as $keyEv => $valueEv) {
					$arrayIdEvents[] = new MongoId($keyEv) ;
					 //$event = Event::getSimpleEventById($keyEv);
					 // if(!empty($event) && 
					 // 	(Preference::isPublicElement(@$event["preferences"]) 
						// || Authorisation::canSeePrivateElement(@$event["links"], Event::COLLECTION, $keyEv, $event["creator"], @$type, @$id))){
					 // 	$contextMap[$keyEv] = $event;
					 // }
				}
			}
			if(isset($element["links"]["subEvents"])){
				foreach (array_reverse($element["links"]["subEvents"]) as $keyEv => $valueEv) {
					$arrayIdEvents[] = new MongoId($keyEv) ;
					 $event = Event::getSimpleEventById($keyEv);
					// if(!empty($event)&& 
					//  	(Preference::isPublicElement(@$event["preferences"]) 
					// 	|| Authorisation::canSeePrivateElement(@$event["links"], Event::COLLECTION, $keyEv, $event["creator"], @$type, @$id))){
					//  	$contextMap[$keyEv] = $event;
					//  }
				}

			}

			if(!empty($sub) && isset($element)){
				$arrayIdEvents = Element::getSubEvent($element, 0, intval($sub), $contextMap, $arrayIdEvents);
			}

			$sort  = ( (!empty($sort) && $sort == "1") ? 1 : -1);
			$events = PHDB::findAndSort( Event::COLLECTION,
							array("_id" => array('$in' => $arrayIdEvents)),
							array("startDate"=>$sort), null);



			foreach ($events as $keyEv => $event) {
				$event = Event::getSimpleEventById($keyEv, $event);
				if(!empty($event) && 
					(Preference::isPublicElement(@$event["preferences"]) 
					|| Authorisation::canSeePrivateElement(@$event["links"], Event::COLLECTION, $keyEv, $event["creator"], @$type, @$id))){
					if(!empty($edit)) $event["edit"]=Event::COLLECTION;
					$contextMap[$keyEv] = $event;
				}
			}
		}

		if($dataName == "projects"){
			if(isset($element["links"]["projects"])){
				foreach ($element["links"]["projects"] as $keyProj => $valueProj) {
					$project = Project::getPublicData($keyProj);
					if(isset($project["_id"])){	
						if(Preference::isPublicElement(@$project["preferences"]) 
							|| Authorisation::canSeePrivateElement($project["links"], Project::COLLECTION, $keyProj, $project["creator"], @$type, @$id)){
							//$project["type"] = "projects";
							//$project["typeSig"] = Project::COLLECTION;
							if(!empty($edit)) $project["edit"]=Project::COLLECTION;
			           		$contextMap[$keyProj] = $project;
		           		}
		           	}else
		           		Link::disconnect($id, $type, $keyProj, $valueProj["type"], null, "projects");
				}
			}

			if($type==Organization::COLLECTION){
				$orgas=PHDB::find(Organization::COLLECTION, array("parent.".$id=>array('$exists'=>true)) ,array("links"));
				if(!empty($orgas)){
					foreach($orgas as $k =>$link){
						if(@$link["links"]["projects"]){
							foreach ($link["links"]["projects"] as $keyProj => $valueProj) {
								if(!isset($contextMap[$keyProj])){
									$project = Project::getPublicData($keyProj);
									if(Preference::isPublicElement(@$project["preferences"]) 
										|| Authorisation::canSeePrivateElement($project["links"], Project::COLLECTION, $keyProj, $project["creator"], @$type, @$id)){
										if(!empty($edit)) $project["edit"]=Project::COLLECTION;
						           		$contextMap[$keyProj] = $project;
					           		}
					           	}
							}
						}
					}
				}
			}
			
		}
		if($dataName == "organizations"){
			if(isset($element["links"]["memberOf"]))
			foreach ($element["links"]["memberOf"] as $keyOrga => $valueOrga) {
				$orga = Organization::getPublicData($keyOrga);
				if(!empty($edit)) $orga["edit"]=Organization::COLLECTION;
				if(isset($orga["_id"]))
           			$contextMap[$keyOrga] = $orga;
           		else
           			Link::disconnect($id, $type, $keyOrga, Organization::COLLECTION, null, "memberOf");
			}
		}

		if($dataName == "classifieds" || $dataName == "ressources" || $dataName == "jobs"){
			$col = Classified::COLLECTION;
			$contextMap = Element::getByIdAndTypeOfParent( $col , $id, $type, array("updated"=>-1));
			if(!empty($edit)){
				foreach ($contextMap as $key => $value) {
					$contextMap[$key]["edit"] = $col;
				}
			}
		}


		if($dataName == "poi"){
			$contextMap = Element::getByIdAndTypeOfParent(Poi::COLLECTION, $id, $type, array("updated"=>-1));
			if(!empty($edit)){
				foreach ($contextMap as $key => $value) {
					$contextMap[$key]["edit"] = Poi::COLLECTION;
				}
			}
		}

		/*if($dataName == "collections"){
			if(@$element["collections"]){
				$collections = $element["collections"];
				foreach ($collections as $col => $value) {
					$collections[$col] = Collection::get($id, null, $col);
				}
				$contextMap = $collections;
			}
		}*/


		if( $dataName == "actionRooms" || $dataName == "vote" || $dataName == "actions" || $dataName == "discuss" ){
			$where = array("parentType"=>$type, "parentId"=>$id);
			if($dataName == "vote") $where["type"] = "vote";
			if($dataName == "actions")$where["type"] = "actions";
			if($dataName == "discuss")$where["type"] = "discuss";
			$contextMap = PHDB::findAndSortAndLimitAndIndex( ActionRoom::COLLECTION, $where);
		}
		
		if( $dataName == "surveys" ){
			$where = array("parentType"=>$type, "parentId"=>$id);
			$contextMap = PHDB::findAndSortAndLimitAndIndex( Proposal::COLLECTION, $where);
			foreach ($contextMap as $k => $value) {
				if(isset($contextMap[$k]["idParentRoom"])){
					unset($contextMap[$k]);
				}else{
					$contextMap[$k]["edit"] = Proposal::COLLECTION;
					$contextMap[$k]["voteRes"] = Proposal::getAllVoteRes($value);
					$contextMap[$k]["hasVote"] = @$value["votes"] ? Cooperation::userHasVoted( Yii::app()->session['userId'], $value["votes"]) : false;
				}
			}
		}
		
		if( $dataName == "proposals" ){
			$where = array("creator"=>$id);
			$contextMap = PHDB::findAndSortAndLimitAndIndex( Proposal::COLLECTION, $where);
			foreach ($contextMap as $k => $value) {
				$contextMap[$k]["edit"] = Proposal::COLLECTION;
				$contextMap[$k]["voteRes"] = Proposal::getAllVoteRes($value);
				$contextMap[$k]["hasVote"] = @$value["votes"] ? Cooperation::userHasVoted( Yii::app()->session['userId'], $value["votes"]) : false;
			}
		}


		/*if($dataName == "liveNow"){
			$post = $_POST; 
			// if( empty($_POST["searchLocalityCITYKEY"]) && 
			// 	(empty($_POST["searchLocalityDEPARTEMENT"]) || $_POST["searchLocalityDEPARTEMENT"][0] == "" || 
			// 	 $_POST["searchLocalityDEPARTEMENT"][0] == "undefined") && 
			// 	isset($element["address"])){
			// 	$levelS = City::getLevelForNowList($element["address"]);
			// 	$post["searchLocalityDEPARTEMENT"] = array($levelS);
			// }

			if( !empty($post["searchLocality"])){
				$scope = $post["searchLocality"];
				foreach ($post["searchLocality"] as $key => $value) {
					//$scopeName = @$value["name"];
					$scope = $value ;
				}
			}

			//EVENTS-------------------------------------------------------------------------------
			$query = array("startDate" => array( '$gte' => new MongoDate( time() ) ));

			if(@$type!="0" || !empty(@$post["searchLocality"]))
				$query = SearchNew::searchLocality(@$post["searchLocality"], $query);

			$events = PHDB::findAndSortAndLimitAndIndex( Event::COLLECTION,
							$query,
							array("startDate"=>1), 10);
			//var_dump($events);
			foreach ($events as $key => $value) {
				$events[$key]["typeEvent"] = @$value["type"];
				$events[$key]["type"] = "events";
				$events[$key]["typeSig"] = "events";
				if(@$value["startDate"]) {
					$events[$key]["updatedLbl"] = Translate::pastTime(@$value["startDate"]->sec,"timestamp");
					$events[$key]["startDate"] = date(DateTime::ISO8601, $value["startDate"]->sec);
				
		  		}

		  		if(@$value["endDate"]) {
					$events[$key]["endDate"] = date(DateTime::ISO8601, $value["endDate"]->sec);
		  		}
		  	}
		  	$contextMap = array_merge($contextMap, $events);
			

			//CLASSIFIED-------------------------------------------------------------------------------
			$query = array();
			if(@$type!="0" || !empty($post["searchLocality"]))
				$query = SearchNew::searchLocality(@$post["searchLocality"], $query);

			$classified = PHDB::findAndSortAndLimitAndIndex( Classified::COLLECTION, $query,
							array("updated"=>-1), 10);

			foreach ($classified as $key => $value) {
				$classified[$key]["type"] = "classifieds";
				$classified[$key]["typeSig"] = "classifieds";
				if(@$value["updated"]) {
					$classified[$key]["updatedLbl"] = Translate::pastTime(@$value["updated"],"timestamp");
		  		}
		  	}
		  	$contextMap = array_merge($contextMap, $classified);
			
			//RESSOURCE-------------------------------------------------------------------------------
			$query = array();
			if(@$type!="0" || !empty($post["searchLocality"]))
				$query = Searchnew::searchLocality(@$post["searchLocality"], $query);

			$ressource = PHDB::findAndSortAndLimitAndIndex( Ressource::COLLECTION, $query,
							array("updated"=>-1), 10);

			foreach ($ressource as $key => $value) {
				$ressource[$key]["type"] = "ressources";
				$ressource[$key]["typeSig"] = "ressources";
				if(@$value["updated"]) {
					$ressource[$key]["updatedLbl"] = Translate::pastTime(@$value["updated"],"timestamp");
		  		}
		  	}
		  	$contextMap = array_merge($contextMap, $ressource);
			
		  	//POI-------------------------------------------------------------------------------
			$query = array();
			if(@$type!="0" || !empty(@$post["searchLocality"]))
				$query = SearchNew::searchLocality(@$post["searchLocality"], $query);
			
			$pois = PHDB::findAndSortAndLimitAndIndex( Poi::COLLECTION, $query,
							array("updated"=>-1), 10);

			foreach ($pois as $key => $value) {
				$pois[$key]["type"] = "poi";
				$pois[$key]["typeSig"] = "poi";
				if(@$value["updated"]) {
					$pois[$key]["updatedLbl"] = Translate::pastTime(@$value["updated"],"timestamp");
		  		}
		  	}
		  	$contextMap = array_merge($contextMap, $pois);
		  	
			
			if(@$post["tpl"]=="json")
				return Rest::json($contextMap);
			else
				echo $this->getController()->renderPartial($post['tpl'], 
											array("result"=>$contextMap, 
												"element" => $element,
												"type"=>$type, 
												"id"=>$id, 
												//"scope"=>@$scopeName, 
												"scope"=>@$scope,
												"open"=> (@$type!="0"))); 
												//open : for home page (when no user connected)
			Yii::app()->end();
		}*/

		foreach ($contextMap as $key => $value) {
			if(@$contextMap[$key]["type"] && $contextMap[$key]["type"] == Event::COLLECTION)
  				$contextMap[$key]["updatedLbl"] = Translate::pastTime(@$value["startDate"],"date");
  			else
  				$contextMap[$key]["updatedLbl"] = Translate::pastTime(@$value["updated"],"timestamp");
		}

		if ($limit != null) {
			$contextMap = array_slice($contextMap, 0 ,$limit);
		}
		return Rest::json($contextMap);
	}
}



?>