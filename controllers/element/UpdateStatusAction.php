<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, DataValidator, Yii, Person, Rest, Role, Element, PHDB;
/**
   * Register a new user for the application
   * Data expected in the post : name, email, postalCode and pwd
   * @return Array as json with result => boolean and msg => String
   */
class UpdateStatusAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        if(DataValidator::missingParamsController($_POST, ["id", "type","action"]))
            return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));
     
      
        $result = array("result"=>false, "msg"=>Yii::t("common", "Incorrect request"));
    
        $controller=$this->getController();
        if (! (Person::logguedAndValid() && Yii::app()->session["userIsAdmin"])) {
          return Rest::json(array("result" => false, "msg" => "You are not super admin : you can not modify this role !"));
          return;
        }
      if($_POST["type"]==Person::COLLECTION){
        $res = Role::updatePersonRole($_POST["action"], $_POST["id"]);
        //$res=Preference::updateConfidentiality(Yii::app()->session["userId"],$_POST["typeEntity"],$_POST);
      }else{
        $res=Element::updateStatus($_POST["id"],$_POST["type"]);
      }

      $res["elt"] = PHDB::findOneById($_POST["type"], $_POST["id"], array("roles"));
		  return Rest::json($res);
    }
}