<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use CAction;
use CTKException;
use Document;
use Element;
use PHDB;
use Poi;
use Rest;
use Yii;

class GetAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Get element by Id and type
*/

  public function run($type,$id=null, $update=null, $edit=null) { 
    //if (isset(Yii::app()->session["userId"])) {
      try {
        if(!empty($_POST['id'])) 
          $id = $_POST['id'];
          
        if(!empty($id))
          $res = array("result" => true, "map" => Element::getByTypeAndId($type,$id, null, $update) );

        //pour :es type POI 
        //ex used in doc module
        if( $type == Poi::COLLECTION && $edit){
          $documents  = PHDB::find(Document::COLLECTION, array('id' => $id, "type" => $type ));
          if( count($documents) )
          {
            $res["map"]["documents"] = array();
            foreach ($documents as $key => $doc) {
              $res["map"]["documents"][] = array (
                "name" => $doc['name'], 
                "doctype" => $doc['doctype'],  
                "path" => Yii::app()->baseUrl."/".Yii::app()->params['uploadUrl'].$doc["moduleId"]."/".$doc["folder"]."/".$doc['name']
              );
            }
          }

          $res["map"]["editBtn"] = $this->getController()->renderPartial("costum.views.tpls.openFormBtn",
                                                                            [ 'edit' => "update",
                                                                              'tag' => null,
                                                                              'id' => $res["map"]["_id"] ],true);
        }

        // comment by Nicoss 26/04/2021
        
        // if( $type == Cms::COLLECTION && isset($res["map"]["page"])){
        //   //if CMS contains children blockcms
        //     $parentId = array_keys($res["map"]["parent"]);
        //     $blockCms  = PHDB::find(Cms::COLLECTION, ['parent.'.$parentId[0] => ['$exists'=>1], "page" => $res["map"]["page"] ] );
        //     $res["map"]["description"] = "";
        //     foreach ($blockCms as $k => $v) {
        //         $data = ["canEdit" => $edit,
        //                  "page" => $res["map"]["page"],
        //                  "blockCms"=>$v];
        //         if(isset($v["page"]) && isset($v["path"])){
        //           $res["map"]["description"] .= $v["page"].":".$v["path"]."<br/>";
        //           //$res["map"]["description"] .= $this->getController()->renderPartial("costum.views.".$v["path"], $data,true);
        //         }
        //     }
        // }
      } catch (CTKException $e) {
        $res = array("result"=>false, "msg"=>$e->getMessage());
      }
    // } else {
    //   $res = array("result"=>false, "msg"=>"Please login first");
    // }
    return Rest::json($res);
  }

}

?>