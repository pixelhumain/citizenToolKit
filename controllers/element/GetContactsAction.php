<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Rest, Yii;
class GetContactsAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($type, $id) { 
    	$contacts = Element::getContacts($id, $type);
		return Rest::json($contacts);
	}
}

?>