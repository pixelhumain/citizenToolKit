<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use Element;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;
use Rest;
use Yii;

class JoinElementAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();
        $data = $_POST;
        if(isset($_POST["contextId"]) && isset($_POST["contextType"])){
            $link = PHDB::findOne(InvitationLink::COLLECTION, ["targetId" => $_POST["contextType"], "targetId" => $_POST["contextId"], "isAdmin" => "false", "roles" => ""]);
            if(!$link){
                $link = [
                    "targetId" => $_POST["contextId"],
                    "targetType" => $_POST["contextType"],
                    "ref" => uniqid(),
                    "createOn" => time(),
                    "isAdmin" => "false",
                    "roles" => "",
                    "urlRedirection" => "",
                    "invitorId" => Yii::app()->session["userId"],
                ];
				Yii::app()->mongodb
					->selectCollection(InvitationLink::COLLECTION)
					->insert($link);
            }
            $data["links"] = $link;
            $infoData = Element::getInfoDetail([], Element::getByTypeAndId($_POST["contextType"], $_POST["contextId"]),$_POST["contextType"], $_POST["contextId"]);
            $data["linksBtn"] = @$infoData["linksBtn"];
            $response = BlockCmsWidget::widget([
                "path" => "tpls.blockCms.menu.joins",
                "notFoundViewPath" => "costum.views.tpls.blockCms.menu.joins",
                "config" => $data
            ]);
            return Rest::json(array("result"=>true, "msg"=> Yii::t("common", "Success"), "view" => $response));
        }
    }
}
