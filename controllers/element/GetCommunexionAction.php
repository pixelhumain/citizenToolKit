<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, CO2, Rest;
class GetCommunexionAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() { 
		$controller=$this->getController();
		$res = CO2::getCommunexionUser();
		return Rest::json($res);
	}
}
