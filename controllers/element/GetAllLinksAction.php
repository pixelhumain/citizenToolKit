<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Rest, Yii;
class GetAllLinksAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run($type, $id) { 
    	//$controller=$this->getController();
		$element = Element::getByTypeAndId($type, $id);
		$links=@$element["links"];
		$contextMap = Element::getAllLinks($links,$type, $id);
		return Rest::json($contextMap);
	}
}

?>