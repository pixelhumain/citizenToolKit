<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use MongoId;
use Person;
use PHDB;
use Rest;

class GetExternalNetwork extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        if(!isset($_SESSION["userId"]))
            return Rest::json(["error"=>"You must logged to perform this operartion."]);
        $user = PHDB::findOne(Person::COLLECTION, ["_id" => new MongoId($_SESSION["userId"])]);

        return $this->getController()->renderPartial("externalNetwork",["user"=>$user]); 
    }
}

?>