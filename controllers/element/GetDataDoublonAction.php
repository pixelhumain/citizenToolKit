<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Rest, Yii;
use PHDB, HtmlHelper, Authorisation, MongoId, Person;

class GetDataDoublonAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($info = "", $type = "", $name="", $source="", $last = "", $lastType = "") { 
		$info = (isset($_POST["info"])) ? $_POST["info"] : $info ;
		$type = (isset($_POST["type"])) ? $_POST["type"] : $type ;
		$name=(isset($_POST["name"])) ? $_POST["name"] : $name ;
		$source=(isset($_POST["source"])) ? $_POST["source"] : $source;
		$last = (isset($_POST["last"])) ? $_POST["last"] : $last ;
		$lastType = (isset($_POST["lastType"])) ? $_POST["lastType"] : $lastType ;

		$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
		$controller=$this->getController(); 

		if ( ! Person::logguedAndValid() ) {
            $this->getController()->layout = "//layouts/empty";
			return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in","text"=> Yii::t('survey', 'You must sign in to access in this features')));
        }

		if ( !Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) {
			$this->getController()->layout = "//layouts/empty";
			return $this->getController()->renderPartial("co2.views.default.unTpl",array("msg"=>"Please Login as Superadmin First","icon"=>"fa-sign-in","text"=> "You must be logged as an Superadmin user to do this action !"));
        }
		
		if($info != "" && $type != ""){
			$name = $info;
			$element = Element::getElementByName($name, $type, $source);

			foreach($element as $key => $val){
				if(isset($val["updated"])){
					$element[$key]["dateUpdate"] = Date("Y-m-d", $val["updated"]);
				}
			}

			return Rest::json($element);
		}
		else{
			$doublons = array();


			$allElementType = Element::getAllElementToDoublons();
			if($type != "" ){
				$doublons = Element::checkDoublonInCollection($type, $name, $source);
				return Rest::json(array("type" => $type, "doublons" => $doublons));
			}
			
			$doublons = Element::checkAllDoublon($name, $source);
			

			if(Yii::app()->request->isAjaxRequest){
				$doublons["name"] = $name;
				return Rest::json($doublons);
			}

			if($last != "" && $lastType != "" )
				$last = Element::getByTypeAndId( $lastType, new MongoId($last));
				

			$params = array(
				"doublons" => $doublons,
				"assetsPath"  =>  $controller->module->getParentAssetsUrl(),
				"baseUrl"   => $baseUrl ,
				"name"   => $info ,
				"allElementType"   => $allElementType,
				"type"   => $type ,
				"last"   => $last ,
			);

			//return var_dump($doublons);
			
			return $controller->renderPartial("getDoublon", $params, true);
		} 
    }
}
?>