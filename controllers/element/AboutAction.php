<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use CAction, Element, Event, Organization, Authorisation, Yii, Rest;
class AboutAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($type, $id, $view=null, $networkParams=null, $json=null) { 
		$controller=$this->getController();

		$element=Element::getByTypeAndId($type,$id);
		if(isset($element["parent"]) && !empty($element["parent"])){
        	foreach($element["parent"] as $k => $v){
                $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                $element['parent'][$k]=array_merge($element['parent'][$k], $elt);
            }
        }
		else if(isset($element["parentId"]) && isset($element["parentType"]) && 
			$element["parentId"] != "dontKnow" && $element["parentType"] != "dontKnow") {
            $elt = Element::getByTypeAndId( $element["parentType"], $element["parentId"],array("name", "slug","profilThumbImageUrl", "type"));
			if(isset($elt["_id"]) ){
				$element['parent'][(string)$elt["_id"]] =  $elt;	
			}
		}

        if(isset($element["organizer"]) && !empty($element["organizer"])){
            foreach($element["organizer"] as $k => $v){
                $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                $element['organizer'][$k]=array_merge($element['organizer'][$k], $elt);
            }
        }else if(isset($element["organizerId"]) && isset($element["organizerType"]) && 
            $element["organizerId"] != "dontKnow" && $element["organizerType"] != "dontKnow"){
        	$organizer=Element::getElementById( $element["organizerId"], $element["organizerType"], null, array("name", "slug","profilThumbImageUrl"));
        	if(!empty($organizer)){
            	$element['organizer']=array($element["organizerId"] => $organizer);
            	$element['organizer'][$element["organizerId"]]["type"]=$element["organizerType"];
            }
        }
        
		/*if(isset(Yii::app()->session["network"]){
			$params["openEdition"] = false;
			$params["edit"] = false;
		}*/
		if($type==Event::COLLECTION)
			$params["typesList"]=Event::$types;
		else if($type==Organization::COLLECTION)
			$params["typesList"]=Organization::$types;
		$params["element"] = $element;
		$params["type"] = $type;
		$params["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $type, $id);
		$params["openEdition"] = Authorisation::isOpenEdition($id, $type, @$element["preferences"]);
		$params = Element::getInfoDetail($params, $element, $type, $id);
		$params["params"] = $params;
		$page =(isset($view) && !empty($view)) ? $view : "about";
		if ($json == true) {
			return Rest::json($params["element"]);
		}
		if(Yii::app()->request->isAjaxRequest)
			return $controller->renderPartial($page,$params,true);
		else 
			return	$controller->render( $page , $params );
	}
}
