<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use Yii;

class AnswerInviteAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $params["controller"] = $controller;       
        $invitedMe = $_POST["invitedMe"];
        if (Yii::app()->session["userId"] && Yii::app()->session["userId"] != (is_array($invitedMe) && $invitedMe["invitorId"] ? $invitedMe["invitorId"] : null)) { 
            return $params["controller"]->renderPartial('co2.views.element.menus.answerInvite', 
            array(  "invitedMe"      => $_POST["invitedMe"],
                "element"   => $_POST["element"]
                ) ,true); 
        }else if (@Yii::app()->session["userId"] && Yii::app()->session["userId"] == (is_array($invitedMe) && $invitedMe["invitorId"] ? $invitedMe["invitorId"] : null)) {
            $str = "";
            $inviteCancel="Cancel";
            $option = null;
            $msgRefuse = Yii::t("common","Are you sure to cancel this invitation");
            $str .= `<div class="containInvitation">
                        <div class="statuInvitation"> `.  Yii::t("common", "Friend request sent");
            $str .= '<br>'.
                        '<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.disconnect(\''.$element["collection"].'\',\''.(string)$element["_id"].'\',\''.Yii::app()->session["userId"].'\',\''.Person::COLLECTION.'\',\''.Element::$connectTypes[$element["collection"]].'\',null,\''.$option.'\',\''.$msgRefuse.'\')" data-placement="bottom" data-original-title="'.Yii::t("common","Not interested by the invitation").'">'.
                           '<i class="fa fa-remove"></i> '.Yii::t("common",$inviteCancel).'</a>'.
                '</div>'.
            '</div>';
            return $str;
        }
    }
}