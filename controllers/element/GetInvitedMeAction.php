<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;
use Yii;
use Event;
use Organization;
use Authorisation;
use Element;

class GetInvitedMeAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $params["controller"] = $controller;  
        $elem = $_POST["element"];
        if($elem["collection"] == Event::COLLECTION)
            $paramsGetInfoDetail["typesList"]=Event::$types;
        else if($elem["collection"] == Organization::COLLECTION)
            $paramsGetInfoDetail["typesList"]=Organization::$types;

        $el = Element::getByTypeAndId($elem["collection"], (isset($elem["id"])) ? $elem["id"] : (string)$elem["_id"]);
        $paramsGetInfoDetail["element"] = $el;
        $paramsGetInfoDetail["type"] = $el["collection"];
        $id = (isset($el["id"])) ? $el["id"] : $el["_id"];
        $paramsGetInfoDetail["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $el["collection"], $id);
        $paramsGetInfoDetail["openEdition"] = Authorisation::isOpenEdition($id, $el["collection"], @$el["preferences"]);
        $getInfoDetail = Element::getInfoDetail($paramsGetInfoDetail, $el, $el["collection"], $id);
        $invitedMe = (isset($getInfoDetail["invitedMe"])) ? $getInfoDetail["invitedMe"] : [];
        if($invitedMe)
            if (Yii::app()->session["userId"] && Yii::app()->session["userId"] != (is_array($invitedMe) && $invitedMe["invitorId"] ? $invitedMe["invitorId"] : null)) { 
                return $params["controller"]->renderPartial('co2.views.element.menus.answerInvite', 
                    array(  "invitedMe"      =>  $invitedMe,
                        "element"   => $el
                        ) ,true); 
            }

    }
}