<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

class GetDataByUrlAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){

        $controller = $this->getController();
        $params = array();
        try{
        	$params = json_decode(file_get_contents($_POST['url']), true);
        }catch (CTKException $e){
            $params["error"][] = $e->getMessage();
        }
    	return Rest::json($params);   
    }
}

?>