<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element;

use Event;
use MongoId;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\components\Action as CAction;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Project;
use Rest;
use Yii;

class Select2ListAction extends CAction {
	public function run() {
		$output = [
			"results"	=>	[],
			"more"		=>	false
		];
		$fields = [
			"name",
			"slug",
			"profilImageUrl"
		];
		$default_images = [
			Organization::COLLECTION => Yii::app()->getModule("co2")->getAssetsUrl() . "/images/thumb/default_organizations.png",
			Project::COLLECTION => Yii::app()->getModule("co2")->getAssetsUrl() . "/images/thumb/default_projects.png",
			Event::COLLECTION => Yii::app()->getModule("co2")->getAssetsUrl() . "/images/thumb/default_events.png",
		];
		$where = ['$and' => []];
		if (!empty($_POST["user"])) {
			$user = $_POST["user"];
			$where['$and'][] = [
				'$or' => [
					["links.contributors.$user.isAdmin" => true],
					["links.members.$user.isAdmin" => true],
					["links.attendees.$user.isAdmin" => true],
				]
			];
		}
		if (!empty($_POST["type"]) && !empty($_POST["id"])) {
			$where['$and'][] = ["_id" => new MongoId($_POST["id"])];
			$db_elements = PHDB::find($_POST["type"], $where, $fields);
			foreach ($db_elements as $db_id => $db_element) {
				$output["results"][] = [
					"id"	=>	$db_id,
					"name"	=>	$db_element["name"],
					"slug"	=>	$db_element["slug"],
					"image"	=>	!empty($db_element["profilImageUrl"]) ? $db_element["profilImageUrl"] : $default_images[$_POST["type"]],
					"type"	=>	$_POST["type"]
				];
			}
		} else if (!empty($_POST["elements"])) {
			$where['$and'][] = ["name" => UtilsHelper::mongo_regex($_POST["search"])];
			$limit = intval($_POST["length"]);
			$offset = (intval($_POST["page"]) - 1) * $limit;
			foreach ($_POST["elements"] as $post_element) {
				$db_elements = PHDB::findAndFieldsAndSortAndLimitAndIndex($post_element, $where, $fields, ["name" => 1], $limit, $offset);
				if (!$output["more"]) {
					$count = PHDB::count($post_element, $where);
					$output["more"] = $offset + $limit < $count;
				}
				foreach ($db_elements as $db_id => $db_element) {
					$output["results"][] = [
						"id"	=>	$db_id,
						"name"	=>	$db_element["name"],
						"slug"	=>	$db_element["slug"] ?? null,
						"image"	=>	!empty($db_element["profilImageUrl"]) ? $db_element["profilImageUrl"] : $default_images[$post_element],
						"type"	=>	$post_element
					];
				}
			}
		}
		return Rest::json($output);
	}
}
