<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, Yii;
class CreateGraphAction extends \PixelHumain\PixelHumain\components\Action{
    
    public function run($insee){
        $controller=$this->getController();

        $params = array("insee" => $insee);

        /*$controller->title = ((!empty($name)) ? $name : "City : ".$insee)."'s Directory";
        $controller->subTitle = (isset($city["description"])) ? $city["description"] : "";
        $controller->pageTitle = ucfirst($controller->module->id)." - ".$controller->title;*/
        //$controller->render("createGraph",$params);

        $page = "createGraph";
		if(Yii::app()->request->isAjaxRequest)
          return $controller->renderPartial($page,$params,true);
        else 
		return	$controller->render( $page , $params );
    }
}

?>
