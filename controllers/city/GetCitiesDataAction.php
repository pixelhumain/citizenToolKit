<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, City, Rest;
/**
 */
class GetCitiesDataAction extends \PixelHumain\PixelHumain\components\Action
{
	 public function run($insee, $typeData, $type=null)
    {
        if(isset($_POST['cities']))
        {
            $listInsee[] = $insee;
            foreach ($_POST['cities'] as $key => $value) {
               $listInsee[] = $value;
            }

            $citiesData = City::getDataByListInsee($listInsee, $typeData);
        
            return Rest::json($citiesData);
        }
        else
        {
            return Rest::json(array('result' => false));
        }
    }
}