<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, City, Rest, Yii;
class UpdateCitiesGeoFormatAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$success = City::updateGeoPositions();
	    return Rest::json( $success );
    }
}