<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use CAction;
use City;
use News;
use Rest;
use Translate;
use TranslateCommunecter;
use TranslateGeoJson;
use TranslateKml;
use Yii;

class GetAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null, $format = null, $limit=50, $index=0, $tags = null, $multiTags=null , $key = null, $insee = null) {
		$controller=$this->getController();
		// Get format
		/*if( $format == Translate::FORMAT_SCHEMA)
	        $bindMap = TranslateSchema::$dataBinding_city;
		else*/

		if ($format == Translate::FORMAT_KML)
			$bindMap = TranslateKml::$dataBinding_city;
		elseif ($format == Translate::FORMAT_GEOJSON)
		 	$bindMap = TranslateGeoJson::$dataBinding_city;
		else 
	    	$bindMap = TranslateCommunecter::$dataBinding_city;

      	$result = Api::getData($bindMap, $format, City::COLLECTION, $id,$limit, $index, $tags, $multiTags, $key, $insee);

		if ($format == Translate::FORMAT_KML) {
			$strucKml = News::getStrucKml();   
			Rest::xml($result, $strucKml,$format);
		} else
			return Rest::json($result);
	}
}

?>