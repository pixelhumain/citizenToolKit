<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, City, Rest;
class GetLevelAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$params = City::getLevelById($_POST["cityId"]);
    	return Rest::json( $params );
	}
} ?>