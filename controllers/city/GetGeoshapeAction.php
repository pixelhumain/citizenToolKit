<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, City, Rest, Yii;
class GetGeoshapeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();
        $cities = City::getGeoShapeCity($_POST);
        return Rest::json($cities);
    }
}

?>