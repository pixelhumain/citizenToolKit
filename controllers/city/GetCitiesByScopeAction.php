<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, City, Rest, Yii;
/**
 */
class GetCitiesByScopeAction extends \PixelHumain\PixelHumain\components\Action
{
	 public function run()
    {
        $params = array('result' => false);
        if(isset($_POST['scopes']))
            $params = City::setCitiesByScope($_POST['scopes']);
        return Rest::json($params);
    }
}