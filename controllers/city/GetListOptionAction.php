<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, City, Rest;
use PixelHumain\PixelHumain\modules\communecter\models\CityOpenData;

/**
 */
class GetListOptionAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run()
    {
        if(isset($_POST['insee']) && isset($_POST['typeData']))
        {
            $where = array("insee"=>$_POST['insee'], $_POST['typeData'] => array( '$exists' => 1 ));
            $fields = array($_POST['typeData']);
            $option = City::getWhereData($where, $fields);
            $chaine = "" ;
            foreach ($option as $key => $value) 
            {
                foreach ($value as $k => $v) 
                {
                    if($k == $_POST['typeData'])
                    {
                        $chaine = CityOpenData::listOption2($v, $chaine, true, "");
                    }   
                }
            }
            return Rest::json($chaine);
        }
        else
        {
            return Rest::json(array('result' => false));
        }
    }
}