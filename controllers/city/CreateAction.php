<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;
use CAction;
use CTKException;
use Nominatim;
use Rest;
use Yii;

class CreateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();

        if (isset(Yii::app()->session["userId"])) {
            try {
                //Rest::json($_POST); exit;
                $res = Nominatim::createCity($_POST, Yii::app()->session["userId"]);
            } catch (CTKException $e) {
                $res = array("result"=>false, "msg"=>$e->getMessage());
            }
            return Rest::json($res);
        } else {
            $res = array("result"=>false, "msg"=>"You must be loggued to create a city");
            return Rest::json($res);
        }
    }
}