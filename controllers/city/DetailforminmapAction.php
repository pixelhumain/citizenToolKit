<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, City, Rest, Yii;
class DetailforminmapAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();
        $city = City::getDetailFormInMap($_POST["id"]);
        return Rest::json($city);
    }
}

?>