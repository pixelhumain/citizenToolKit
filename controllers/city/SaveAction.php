<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;
use CAction;
use City;
use CTKException;
use Rest;
use Yii;

class SaveAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();

        if (isset(Yii::app()->session["userId"])) {
            try {
                $res = City::insert($_POST["city"], Yii::app()->session["userId"]);
            } catch (CTKException $e) {
                $res = array("result"=>false, "msg"=>$e->getMessage());
            }
            return Rest::json($res);
        } else {
            $res = array("result"=>false, "msg"=>"You must be loggued to create a city");
            return Rest::json($res);
        }
    }
}