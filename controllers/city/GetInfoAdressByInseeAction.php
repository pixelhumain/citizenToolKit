<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction, City, Rest, Yii;
class GetInfoAdressByInseeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$where = array("insee"=>$_POST["insee"], "cp"=>$_POST["cp"]);
		$fields = array("alternateName");
        $adress = City::getWhere($where, $fields);
    	return Rest::json( $adress );
    }
}