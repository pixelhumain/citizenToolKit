<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city;

use CAction;
class StatisticCityAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($insee){
    	$controller=$this->getController();
    	$params = array();
    return	$controller->render("statistiqueCity",$params);
    }
}

?>