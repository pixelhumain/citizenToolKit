<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\stat;

use CAction, Yii;
use Log;

/**
* to create statistic
* Can be launch by cron
*/
class ChartLogsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {

		$controller = $this->getController();


		$params = array();		
		$page =  "chartLogs";

		$params['actionsLog'] = Log::getActionsToLog();
		$params['groups']['resultTypes'] = array('0'=> 'false', '1' => 'true');
		if(Yii::app()->request->isAjaxRequest){
			return $controller->renderPartial($page,$params,true);
		}
		else {
		return	$controller->render($page,$params);
		}

    }
}
