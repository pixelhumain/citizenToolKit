<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\stat;

use CAction;
use Stat;

/**
* to create statistic
* Can be launch by cron
*/
class CreateGlobalStatAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$retour = date('Y-m-d H:i:s')." - Démarrage du script pour calculer les statistiques<br/>";
    	Stat::createGlobalStat();
    	$retour .= date('Y-m-d H:i:s')." - Fin du script pour calculer les statistiques<br/>";
		return $retour;
    }
}
