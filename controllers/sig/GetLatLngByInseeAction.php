<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig;

use CAction, SIG, Rest, Yii;
class GetLatLngByInseeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
	    $postalCode = isset($_POST["postalCode"]) ? $_POST["postalCode"] : null;
    	$position = SIG::getLatLngByInsee($_POST["insee"], $postalCode);
	    return Rest::json( $position );
    }
}