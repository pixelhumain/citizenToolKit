<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig;

use CAction, PHDB, Organization, Rest, Yii;
class ShowLocalCompaniesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $whereGeo = $this->getGeoQuery($_POST, 'geo');
	    $where = array('type' => "company");
	    
	    $where = array_merge($where, $whereGeo);
	    				
    	$companies = PHDB::find(Organization::COLLECTION, $where);
    	$companies["origine"] = "ShowLocalCompanies";
    	  	
        return Rest::json( $companies );
    }
}