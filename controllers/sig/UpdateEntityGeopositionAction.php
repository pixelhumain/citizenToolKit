<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig;

use CAction, SIG;
class UpdateEntityGeopositionAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	SIG::updateEntityGeoposition($_POST["entityType"], $_POST["entityId"], $_POST["latitude"], $_POST["longitude"]);
    }
}