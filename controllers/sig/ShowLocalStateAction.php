<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig;

use CAction, PHDB, Organization, Rest, Yii;
class ShowLocalStateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $whereGeo = $this->getGeoQuery($_POST, 'geo');
	    $where = array('type' => "association");
	    
	    
	    $where = array_merge($where, $whereGeo);
	    				
    	$states = PHDB::find(Organization::COLLECTION, $where);
    	$states["origine"] = "ShowLocalState";
    	   	
        return Rest::json( $states );
    }
}