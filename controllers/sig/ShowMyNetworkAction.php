<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig;

use CAction, SIG, PHDB, Person, Rest, Yii;
class ShowMyNetworkAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $whereGeo = SIG::getGeoQuery($_POST, 'geo');
	    $where = array();//'cp' => array( '$exists' => true ));
	    
	    $where = array_merge($where, $whereGeo);
	    				
    	$citoyens = PHDB::find(Person::COLLECTION, $where);
    	$citoyens["origine"] = "ShowMyNetwork";
    	
    	
        return Rest::json( $citoyens );
    }
}