<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig;

use CAction, SIG, Rest, Yii;
class GetInseeByLatLngAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$city = SIG::getInseeByLatLngCp($_POST["latitude"], $_POST["longitude"],  (isset($_POST["cp"])) ? $_POST["cp"] : null);
	    return Rest::json( $city );
    }
}