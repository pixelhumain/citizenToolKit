<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig;

use CAction, SIG, Rest, Yii;
class GetCodeInseeByCityNameAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	//error_log(($_POST["cityName"]));
    	//error_log(utf8_encode($_POST["cityName"]));
    	$city = SIG::getCodeInseeByCityName($_POST["cityName"]);
	    return Rest::json( $city );
    }
}