<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\sig;

use CAction;
class CompaniesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        return $controller->renderPartial("companies");
    }
}