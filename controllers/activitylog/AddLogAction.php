<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\activitylog;
use CAction;
use Yii;

class AddLogAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller = $this->getController();
    	$params = array();
    	//$city = SIG::getInseeByLatLngCp("48.380317", "-4.5084217", "29200");
    	//$params["city"] = json_encode($city) ;
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("adddata",$params,true);
        else 
           return $controller->render("adddata",$params);
    }
}

?>