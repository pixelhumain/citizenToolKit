<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\bookmark;
use Authorisation;
use Bookmark;
use CAction;
use Person;
use Rest;
use Yii;
use function json_encode;

class DeleteAction extends \PixelHumain\PixelHumain\components\Action {
	

	public function run($type,$id) {
		if (! Person::logguedAndValid()) {
			return json_encode(array('result'=>false,'error'=>Yii::t("common","Please Log in order to update document !")));
		}

		if (Authorisation::canParticipate(Yii::app()->session["userId"], $type, $id)){
				foreach($_POST["ids"] as $data){
					$res = Bookmark::removeById($data);
				}
				// echo json_encode(array('result'=>true, "msg" => Yii::t("document","Image deleted")));
			} else {
		    	$res = array('result'=>false, "msg" => Yii::t("document","You are not allowed to delete this document !"), "id" => $id);
		}
		return Rest::json($res);
	}
}