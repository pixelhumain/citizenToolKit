<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\bookmark;
use Authorisation;
use Bookmark;
use CAction;
use DataValidator;
use Person;
use Rest;
use Yii;
use function json_encode;

class SaveAction extends \PixelHumain\PixelHumain\components\Action {
	

	public function run() {
		if(DataValidator::missingParamsController($_POST, ["type","parentId","parentType"]) )
		    return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));
     
		if (! Person::logguedAndValid()) {
			return json_encode(array('result'=>false,'error'=>Yii::t("common","Please Log in order to save a search and be alert !")));
		}
		if (Authorisation::canParticipate(Yii::app()->session["userId"], $_POST["parentType"], $_POST["parentId"])){
			$res = Bookmark::save($_POST);
			//echo json_encode(array('result'=>true, "msg" => Yii::t("document","Bookmark has been succesfully added")));
		} else 
		    $res =	array('result'=>false, "msg" => Yii::t("document","You are not allowed to save a bookmark in this context !"), "id" => $_POST["parentId"]);
		return Rest::json($res);
	}
}