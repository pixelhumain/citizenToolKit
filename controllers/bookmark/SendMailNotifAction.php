<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\bookmark;

use CAction, Bookmark, Rest;
class SendMailNotifAction extends \PixelHumain\PixelHumain\components\Action {
	

	public function run() {
		$book = Bookmark::sendMailNotif();
		return Rest::json($book);
	}
}