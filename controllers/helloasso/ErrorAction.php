<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\helloasso;

use PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CampFundValidateAction;

class ErrorAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		CampFundValidateAction::card_payment_socket("credit-card-pay-error", null);
		return $this->getController()->render("co2.views.pay.close_popup");
	}
}
