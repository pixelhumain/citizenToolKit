<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\helloasso;

use Bill;
use PaymentMethod;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\HelloassoPay;
use PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CampFundValidateAction;
use PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\FundingAction;
use Rest;

class ReturnAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		$controller = $this->getController();
		if (!empty($_GET["checkoutIntentId"])) {
			$bills = Bill::getByHelloAssoCheckoutId(intval($_GET["checkoutIntentId"]));
			$paymentMethod = PaymentMethod::getByElementAndType(["id" => $bills["context"]["id"], "type" => $bills["context"]["type"]], "helloasso");
			$payment = HelloassoPay::create(
				$paymentMethod["config"]["clientId"],
				$paymentMethod["config"]["clientSecret"],
			);
			$payment->slug($paymentMethod["config"]["slug"]);
			$out = $payment->get_checkout($_GET["checkoutIntentId"]);
			if ($out["success"] && !empty($out["content"]["order"])) {
				$metadata = $out["content"]["metadata"] ?? [];
				$request = false;
				switch ($metadata["context"]) {
					case "campagne-ftl":
						$request = FundingAction::req_validate_fund(
								json_decode($metadata["communs"], 1),
								$metadata["form"],
								json_decode($metadata["bill"], 1),
								"helloasso"
							);
						break;
				}
				if ($request["success"])
					CampFundValidateAction::card_payment_socket("credit-card-pay-success", $metadata);
				else
					CampFundValidateAction::card_payment_socket("credit-card-pay-error", $request["content"]);
			} else
				CampFundValidateAction::card_payment_socket("credit-card-pay-error", $out["content"]["metadata"] ?? ["error" => "No order found"]);
		}
		return $controller->render("co2.views.pay.close_popup");
	}
}
