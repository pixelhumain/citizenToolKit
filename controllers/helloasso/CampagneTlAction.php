<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\helloasso;

use Bill;
use CacheHelper;
use Organization;
use PaymentMethod;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\HelloassoPay;
use Rest;
use Yii;

class CampagneTlAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		$costum = CacheHelper::getCostum(null, null, $_POST["costumSlug"]);
		$paymentMethod = PaymentMethod::getByElementAndType(["id" => $costum["contextId"], "type" => $costum["contextType"]], "helloasso");
		$payment = HelloassoPay::create(
			$paymentMethod["config"]["clientId"],
			$paymentMethod["config"]["clientSecret"]
		);
		if (!$payment)
			return (Rest::json([
				"success"	=> 0,
				"content"	=> "Cannot initialize payment process"
			]));
		$required = ["tl", "communs", "infos", "form"];
		foreach ($required as $require) {
			if (!isset($_POST[$require]))
				return (Rest::json([
					'success'	=> 0,
					'content'	=> "Missing required field ($require)"
				]));
		}
		$communs = $_POST["communs"];
		$commun_names = array_map(fn($c) => $c["name"], $communs);
		// créer la facture
		$db_bill = $_POST["infos"];
		$db_bill["parent"] = [
			"type"	=> Organization::COLLECTION,
			"id"	=> $_POST["tl"]
		];
		$db_bill["created"] = time();
		$db_bill["context"] = [
			"id" => $costum["contextId"],
			"type" => $costum["contextType"]
		];
		Bill::create($db_bill);
		$db_bill["id"] = (string)$db_bill["_id"];
		// Récupérer le lien de paiement
		// $url = "https://qa.communecter.org";
		$url = Yii::app()->getBaseUrl(1);
		$payment->slug($paymentMethod["config"]["slug"])
			->amount(array_reduce($communs, fn($a, $b) => $a + $b["amount"] * 100, 0))
			->metadata([
				"bill"		=> json_encode($db_bill),
				"form"		=> $_POST["form"],
				"communs"	=> json_encode($communs),
				"context"	=> "campagne-ftl"
			])
			->item_name(implode(", ", $commun_names) . ". Financé(s) par " . $db_bill["name"])
			->on_success("$url/co2/helloAsso/return")
			->on_error("$url/co2/helloAsso/error")
			->on_cancel("$url/co2/helloAsso/cancel");
		$checkout = $payment->init_checkout_intent();
		if (!$checkout["success"])
			return (Rest::json([
				"success"	=> 0,
				"content"	=> $checkout["content"]
			]));
		Bill::add_helloasso_checkout_id($db_bill, $checkout["content"]["id"]);
		$args = [
			"link" => [
				"tl"	=> $_POST["tl"],
				"bill"	=> (string)$db_bill["_id"],
			]
		];
		$out = [
			"success"	=> 1,
			"content"	=> [
				"url" => $checkout["content"]["redirectUrl"],
				"bill_id"	=> (string)$db_bill["_id"],
				"render"	=> $this->getController()->renderPartial("costum.views.custom.aap.cart.cart_processing", $args)
			]
		];
		return (Rest::json($out));
	}
}
