<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\organization;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use CAction;
use News;
use Organization;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\RssGenerator;
use Rest;
use Translate;
use TranslateCommunecter;
use TranslateGeoJson;
use TranslateGogoCarto;
use TranslateJsonFeed;
use TranslateKml;
use TranslateSchema;
use Yii;
use TranslateFtl;

class GetAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null, $format = null, $limit=50, $index=0, $tags = null, $multiTags=null , $key = null, $insee = null, $fullRepresentation = null, $namecity=null, $level3=null, $level4=null, $level1=null, $extraFormat=null, $fileName=null, $rssParametre=null) {
		$controller=$this->getController();
		// Get format
		header("Access-Control-Allow-Origin: *");

		// Get format
		if( !empty($controller->costum )){
			$test = ucfirst($controller->costum["slug"]);
			if(!empty($test::$dataBinding_allOrganization))
				$bindMap = $test::$dataBinding_allOrganization;


		}

		if( empty($bindMap) && $format == Translate::FORMAT_SCHEMA)
	        $bindMap = (empty($id) ? TranslateSchema::$dataBinding_allOrganization : TranslateSchema::$dataBinding_organization);
		else if ( empty($bindMap) && $format == Translate::FORMAT_KML)
			$bindMap = (empty($id) ? TranslateKml::$dataBinding_allOrganization : TranslateKml::$dataBinding_organization);
		else if ( empty($bindMap) && $format == Translate::FORMAT_GEOJSON) 
			$bindMap = (empty($id) ? TranslateGeoJson::$dataBinding_allOrganization : TranslateGeoJson::$dataBinding_organization);
		else if ( empty($bindMap) && $format == Translate::FORMAT_JSONFEED)
			$bindMap = TranslateJsonFeed::$dataBinding_allOrganization;
			// $bindMap = (empty($id) ? TranslateJsonFeed::$dataBinding_allOrganization : TranslateGeoJson::$dataBinding_organization);
		else if ( empty($bindMap) && $format == Translate::FORMAT_GOGO){
			 $bindMap = ( (!empty($fullRepresentation) && $fullRepresentation == "true" ) ? TranslateGogoCarto::$dataBinding_organization : TranslateGogoCarto::$dataBinding_organization_symply);
			//$bindMap = TranslateGogoCarto::$dataBinding_organization_symply;
		}
		else if(  empty($bindMap) && ($format == Translate::FORMAT_MD || $format == Translate::FORMAT_TREE))
			$bindMap = Organization::CONTROLLER;
		else if( empty($bindMap) && $format == Translate::FORMAT_FTL){
	       $bindMap = TranslateFtl::$dataBinding_allOrganization;
		}
		else if( empty($bindMap) ){
	       $bindMap = (empty($id) ? TranslateCommunecter::$dataBinding_allOrganization : TranslateCommunecter::$dataBinding_organization);
		}

		$p = array();
		if(!empty($level1))
			$p["level1"] = $level1;

		if(!empty($level3))
			$p["level3"] = $level3;

		if(!empty($level4))
			$p["level4"] = $level4;
		//var_dump($bindMap); exit;
		$sort=array("name"=>1);
      	$result = Api::getData($bindMap, $format, Organization::COLLECTION, $id,$limit, $index, $tags, $multiTags, $key, $insee, null, null, null, $namecity, $p, null, $extraFormat, $sort);
		
      	if ($format == Translate::FORMAT_KML) {
			$strucKml = News::getStrucKml();		
			Rest::xml($result, $strucKml,$format);	
		} else if ($format == "csv" || $extraFormat=="csv") {
			$fileName=$fileName ?? null;
			// $res = $result["entities"];
			// $head = Export::toCSV($res, ";", "'");
			//Rest::json($result["entities"]); exit;
			Rest::csv($result["entities"], false, false, null, null, $fileName);
		}  else if ($format === "rss") {
			$urlRss = Yii::$app->request->hostInfo; 
            $rssContent = RssGenerator::generateRss($id, $rssParametre, $urlRss, $result);
    		Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
    		Yii::$app->response->content = $rssContent;
    		return Yii::$app->response;
        }
		else
			return Rest::json($result);
    }
}



?>