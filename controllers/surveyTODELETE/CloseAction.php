<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\surveyTODELETE;

use CAction, Survey, Rest, Yii;
class CloseAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = Survey::closeEntry( $_POST );
        return Rest::json( $res );
    }
    
}