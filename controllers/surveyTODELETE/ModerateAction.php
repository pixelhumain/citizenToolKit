<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\surveyTODELETE;

use CAction, Survey, Rest, Yii;
class ModerateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = Survey::moderateEntry( $_POST );
        return Rest::json( $res );
    }
    
}