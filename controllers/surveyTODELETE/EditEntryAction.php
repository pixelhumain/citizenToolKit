<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\surveyTODELETE;

use CAction, PHDB, Survey, MongoId, Yii, Document;
class EditEntryAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run( $survey,$id=null )
    {
        $controller=$this->getController();
        $parentSurvey = PHDB::findOne (Survey::PARENT_COLLECTION, array("_id"=>new MongoId ( $survey ) ) );
        $params = array( "parentSurvey" => $parentSurvey );
        if($id)
        {
            $entry = PHDB::findOne (Survey::COLLECTION, array("_id"=>new MongoId ( $id ) ) );
            
            //TKA BUG : organizerId can be an organisation 
            //we need a person
            //to test with organization as organizer  
            if($entry['organizerId'] != Yii::app()->session["userId"] )
              return array('result' => false , 'msg'=>'Access Denied');

            $params ["title"] = $entry["name"];
            $params ["content"] = $controller->renderPartial( "entry", array( "survey" => $entry ), true);
            $params ["contentBrut"] = $entry["message"];
            $params ["survey"] = $entry;
                 

          if( isset($entry["organizerType"]) )
          {
              
          }

          //Images
          $contentKeyBase = Yii::app()->controller->id.".".Yii::app()->controller->action->id;
          $limit = array(Document::IMG_PROFIL => 1);
          $images = Document::getListDocumentsURLByContentKey($id, $contentKeyBase, Document::DOC_TYPE_IMAGE, $limit);
          $params["images"] = $images;
          $params["contentKeyBase"] = $contentKeyBase;
        }
        return $controller->renderPartial("editEntrySV" , $params,true);
    }
}