<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\surveyTODELETE;

use CAction, Person, Survey, Yii, Rest;
//Delete a Survey
class DeleteAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id) {
        //Check if connected
        if( ! Person::logguedAndValid()) {
            $res = array("result"=>false, "msg"=>"You must be loggued to delete a room");
        } else {
            $res = Survey::deleteEntry( $id,  Yii::app()->session["userId"]);
        }

        return Rest::json( $res );
    }
    
}