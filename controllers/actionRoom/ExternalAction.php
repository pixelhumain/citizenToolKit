<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom;

use CAction, PHDB, ActionRoom, MongoId, CTKException;
class ExternalAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id)
    {
      $controller=$this->getController();

      $room = PHDB::findOne (ActionRoom::COLLECTION, array("_id"=>new MongoId ( $id ) ),array('name','url','parentId','parentType','status') );
      if(!isset($room)) 
          throw new CTKException("Impossible to find this room");

      return $controller->renderPartial( "iframe", array( "room"=>$room )  );
    }
}
