<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom;

use CAction, ActionRoom, Rest, Yii;
class AssignMeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = ActionRoom::assignMe( $_POST );
        return Rest::json( $res );
    }
    
}