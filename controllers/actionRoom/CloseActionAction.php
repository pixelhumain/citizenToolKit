<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom;

use CAction, ActionRoom, Rest, Yii;
class CloseActionAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = ActionRoom::closeAction( $_POST );
        return Rest::json( $res );
    }
    
}