<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom;

use CAction, Lists, Yii;
class EditRoomAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run( )
    {
        $controller=$this->getController();

        $listRoomTypes = Lists::getListByName("listRoomTypes");
        foreach ($listRoomTypes as $key => $value) {
            //error_log("translate ".$value);
            $listRoomTypes[$key] = Yii::t("rooms",$value, null, Yii::app()->controller->module->id);
        }
        $tagsList =  Lists::getListByName("tags");
        $params = array(
            "listRoomTypes" => $listRoomTypes,
            "tagsList" => $tagsList
        );
		if(Yii::app()->request->isAjaxRequest)
	        return $controller->renderPartial("editRoomSV" , $params,true);
	    else
  		return	$controller->render( "editRoomSV" , $params );
    }
}