<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom;

use CAction, Person, ActionRoom, Yii, Rest;
/**
 * @return [json] 
 */
class DeleteRoomAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id) {
        $res = array();
        
        //Check if connected
        if( ! Person::logguedAndValid()) {
            $res = array("result"=>false, "msg"=>"You must be loggued to delete a room");
        } else {
            $res = ActionRoom::deleteActionRoom($id, Yii::app()->session["userId"]);
        }

        return Rest::json($res);
    }
}