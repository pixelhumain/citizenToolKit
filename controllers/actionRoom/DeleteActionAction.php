<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom;

use CAction, Person, Actions, Yii, Rest;
//Delete an action
class DeleteActionAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id) {
        //Check if connected
        if( ! Person::logguedAndValid()) {
            $res = array("result"=>false, "msg"=>"You must be loggued to delete an action");
        } else {
            $res = Actions::deleteAction( $id,  Yii::app()->session["userId"]);
        }

        return Rest::json( $res );
    }
    
}