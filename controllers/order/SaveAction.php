<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\order;
use an;
use CAction;
use CTKException;
use Order;
use Person;
use Rest;
use Yii;

class SaveAction extends \PixelHumain\PixelHumain\components\Action
{
    /**
	* Save a new organization with the minimal information
	* @return an array with result and message json encoded
	*/
    public function run() {
		$controller=$this->getController();
		// Retrieve data from form
		//$newOrganization = Order::newOrganizationFromPost($_POST);
		try{
			if ( Person::logguedAndValid() ) {
				//Save the organization
				return Rest::json(Order::insert($_POST, Yii::app()->session["userId"]));
			} else {
				return Rest::json(array("result"=>false, "msg"=>"You are not loggued with a valid user !"));
			}
		} catch (CTKException $e) {
			return Rest::json(array("result"=>false, "msg"=>$e->getMessage()));
		}
    }
}