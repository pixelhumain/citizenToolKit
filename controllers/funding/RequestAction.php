<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\funding;

use CAction;
class RequestAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $controller->layout = "//layouts/mainSimple";
        /*if(Yii::app()->session["userId"]) 
          $controller->redirect(Yii::app()->homeUrl);
        else
          $detect = new Mobile_Detect;*/
       return $controller->render( "request" );
    }
}