<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\folder;
use CAction;
use CTKException;
use Folder;
use Person;
use Rest;
use Yii;

class AddAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!") );
        if( !Person::logguedAndValid() )
            return array("result"=>false, "msg"=>Yii::t("common","Please Login First") );
        else{	
			try {
				$res = Folder::create( @$_POST['id'], @$_POST['type'], @$_POST['name']);
			} catch (CTKException $e) {
				$res = array( "result" => false , "msg" => $e->getMessage() );
			}
		}

		return Rest::json($res);
    }
}