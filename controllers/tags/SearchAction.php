<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\tags;

use CAction;
use Rest;
use Tags;
use Yii;

class SearchAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($q  = null, $id = null, $format = null, $limit=50, $index=0, $tags = null, $multiTags=null , $key = null, $insee = null, $fullRepresentation = "true") {
        //$controller=$this->getController();
        
		$result = Tags::searchActiveTags($q);
		return Rest::json($result);
    }
}
