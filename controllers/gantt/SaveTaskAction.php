<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\gantt;

use CAction, Gantt, Rest;
class SaveTaskAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	$controller=$this->getController();
    	if($_POST)
    	$res=Gantt::saveTask($_POST);
    	return Rest::json( $res );
    }
}
