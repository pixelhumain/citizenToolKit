<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\gantt;

use CAction, Gantt, Rest;
class RemoveTaskAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($taskId,$parentType,$parentId) {
		$controller=$this->getController();
		$res = Gantt::removeTask($taskId,$parentType,$parentId);
		return Rest::json($res);
	}
}