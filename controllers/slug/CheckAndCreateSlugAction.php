<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\slug;

use CAction, Slug, CO2, Rest;
class CheckAndCreateSlugAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$res=Slug::checkAndCreateSlug($_POST["text"]);
    	return Rest::json(array("result"=>$res));
    }
}