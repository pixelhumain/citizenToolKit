<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\slug;

use CAction, Slug, CO2, Rest;
class CheckAction extends \PixelHumain\PixelHumain\components\Action
{
	 /**
	 * TODO bouboule : La PHPDOC
	 */
    public function run() {
    	$res=Slug::check($_POST["slug"],@$_POST["type"],@$_POST["id"]);

    	$paramsApp = CO2::getThemeParams();
    	return Rest::json(array("result"=>$res, "domaineName"=>@$paramsApp["domaineName"]));
    }
}