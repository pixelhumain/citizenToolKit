<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\slug;

use CAction, Slug, Rest;
class GetInfoAction extends \PixelHumain\PixelHumain\components\Action
{
	 /**
	 * TODO bouboule : La PHPDOC
	 */
    public function run($key=null) {
        $clearSlug = explode(".", $key); //remove les param GET (#myslug?tpl=kkchose)
        $clearSlug = $clearSlug[0];

    	$res=Slug::getBySlug($clearSlug);
    	if(!empty($res))
    		return Rest::json(array("result"=>true,"contextId"=>$res["id"],"contextType"=>$res["type"]));
    	else
    		return Rest::json(array("result"=>false));
    }
}