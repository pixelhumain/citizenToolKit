<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\collections;
use CAction;
use Collection;
use CTKException;
use Person;
use Rest;
use Yii;

class AddAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!") );
        if( !Person::logguedAndValid() )
            return array("result"=>false, "msg"=>Yii::t("common","Please Login First") );
        else{	
			try {
				$res = Collection::add( @$_POST['id'], @$_POST['type'], @$_POST['collection']);
			} catch (CTKException $e) {
				$res = array( "result" => false , "msg" => $e->getMessage() );
			}
		}

		return Rest::json($res);
    }
}