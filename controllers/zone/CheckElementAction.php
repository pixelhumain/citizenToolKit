<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use Event;
use MongoDate;
use MongoId;
use Organization;
use Person;
use PHDB;
use Project;
use Rest;
use SIG;
use Yii;
use Zone;

class CheckElementAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $ids = [
            "597b1c336ff992f0038b45cd" => "Brussel-Hoofdstad",
            "5ab25b8740bb4eab54b9afc3" => "Brussel-Hoofdstad - Bruxelles-Capitale",
            "597b1c0c6ff992f0038b45bf" => "Nivelles 1",
            "597b1b666ff992f0038b4595" => "Nivelles 2",
            "597b1bd06ff992f0038b45b0" => "Mouscron"
        ];
        $data = [];
        foreach ($ids as $key => $value) {
            $organizations = PHDB::count( Organization::COLLECTION, array("address.level4" => $key));
            $citoyens = PHDB::count( Person::COLLECTION, array("address.level4" => $key));
            $projects = PHDB::count( Project::COLLECTION, array("address.level4" => $key));
            $events = PHDB::count( Event::COLLECTION, array("address.level4" => $key));
            $data[$value] = array("organizations" => $organizations, "citoyens" => $citoyens, "projects" => $projects, "events" => $events);
        }
        return Rest::json($data);
    }
}