<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\hackathon;

use DataValidator;
use PHDB;
use Rest;

class FindBatimentBoundsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $data = DataValidator::clearUserInput($_POST);
        if(isset($data["south"]) && isset($data["north"])){
            $batiments = PHDB::find("lapossesion", [
                "geometry" => [
                    '$geoIntersects' => [
                        '$geometry' => [
                            "type" => "Polygon",
                            "coordinates" => [
                                [
                                    [(float) $data["south"]["lng"], (float) $data["south"]["lat"]],
                                    [(float) $data["south"]["lng"], (float)$data["north"]["lat"]],
                                    [(float)$data["north"]["lng"], (float)$data["north"]["lat"]],
                                    [(float)$data["north"]["lng"], (float) $data["south"]["lat"]],
                                    [(float) $data["south"]["lng"], (float) $data["south"]["lat"]]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
            return Rest::json(array("result" => true, "batiments" => $batiments));
        }else{
            return Rest::json(array("result" => false, "msg" => "Impossible de faire la recherche sans les coordonnées"));
        }
    }
}
