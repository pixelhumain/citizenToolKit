<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\hackathon;

use DataValidator;
use PHDB;
use Rest;

class FindBatimentAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $data = DataValidator::clearUserInput($_POST);
        if(isset($data["latitude"]) && isset($data["longitude"])){
            $batiment = PHDB::find("lapossesion", [
                "geometry" => [
                    '$geoIntersects' => [
                        '$geometry' => [
                            "type" => "Point",
                            "coordinates" => [(float)$data["longitude"], (float)$data["latitude"]]
                        ]
                    ]
                ]
            ]);
            return Rest::json(array("result" => true, "batiment" => $batiment));
        }else{
            return Rest::json(array("result" => false, "msg" => "Impossible de faire la recherche sans les coordonnées"));
        }
    }   
}
