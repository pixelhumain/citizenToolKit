<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

    use Citoyen;
    use City;
    use Classified;
    use Event;
    use MongoId;
    use Organization;
    use PHDB;
    use Poi;
    use Project;
    use Ressource;
    use Rest;
    use SIG;

    class UpdateCitiesAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $_data = $_POST['value'];
            $_id = $_POST['id'];
            
            $postalCodes = array();
            if(!empty($_data["postalCodes"])){
                $codePostal = $_data["postalCodes"];
                foreach ($codePostal as $value) {
                    $newCP = array();
                    $newCP["postalCode"] = $value;
                    $newCP["name"] = $_data["alternateName"];
                    if(isset($_data["latitude"]) && isset($_data["longitude"])){
                        $newCP["geo"] = SIG::getFormatGeo($_data["latitude"], $_data["longitude"]);
                        $newCP["geoPosition"] = SIG::getFormatGeoPosition($_data["latitude"], $_data["longitude"]);
                    }
                    $postalCodes[] = $newCP;
                }
            }

            $_elementData = array(
                "address.addressLocality" => $_POST['value']['name']
            );

            $_elementWhere = array(
                "address.localityId" => $_id
            );

            if($_POST['updateType'] == "data") {
                PHDB::updateWithOptions(Organization::COLLECTION, $_elementWhere, array('$set' => $_elementData), array('multi' => true, 'multiple' => true));
                PHDB::update(Event::COLLECTION, $_elementWhere, array('$set' => $_elementData), array('multi' => true, 'multiple' => true)); 
                PHDB::update(Project::COLLECTION, $_elementWhere, array('$set' => $_elementData), array('multi' => true, 'multiple' => true));
                PHDB::update(Citoyen::COLLECTION, $_elementWhere, array('$set' => $_elementData), array('multi' => true, 'multiple' => true));
                PHDB::update(Poi::COLLECTION, $_elementWhere, array('$set' => $_elementData), array('multi' => true, 'multiple' => true));
                PHDB::update(Classified::COLLECTION, $_elementWhere, array('$set' => $_elementData), array('multi' => true, 'multiple' => true));
                PHDB::update(Ressource::COLLECTION, $_elementWhere, array('$set' => $_elementData), array('multi' => true, 'multiple' => true));
            }

            $_data["postalCodes"] = $postalCodes;

            if(isset($_data["latitude"]) && isset($_data["longitude"])){
                $_data["geo"] = SIG::getFormatGeo($_data["latitude"], $_data["longitude"]);
                $_data["geoPosition"] = SIG::getFormatGeoPosition($_data["latitude"], $_data["longitude"]);
                unset($_data["latitude"]);
                unset($_data["longitude"]);
            }
            
            $result["result"] = PHDB::update(City::COLLECTION, array("_id" => new MongoId($_id)), array('$set' => $_data));
            $result["message"] = "Cities Updated";
            $result["data"] = $_data;   
            return Rest::json($result);
        }
    }
?>