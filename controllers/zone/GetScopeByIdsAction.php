<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use CAction, Zone, Rest, Yii;
class GetScopeByIdsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
		$controller=$this->getController();
		$params = Zone::getScopeByIds($_POST);
		return Rest::json($params);
	}
}

?>