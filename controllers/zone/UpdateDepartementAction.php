<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use CAction, Zone, Rest, Yii;
use MongoRegex;
use PHDB,Role;

class UpdateDepartementAction extends \PixelHumain\PixelHumain\components\Action{
    public function run()
    {
		$controller=$this->getController();
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $geoJson = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Departement.geojson"), true);
            $i = 0;
            foreach ($geoJson["features"] as $key => $value) {
                PHDB::update(Zone::COLLECTION,  ['name' => new MongoRegex("/^".iconv("UTF-8", 'ASCII//TRANSLIT', $value["properties"]["nom"])."$/i"), "level" => "4", "countryCode" => "FR"],  array('$set' => array("geoShape" => $value["geometry"])));
                $i++;
            }
            return Rest::json("$i départements modifiés");
        }else{
            return "Vous n'êtes pas autorisé";
        }
    }
}
