<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use Citoyen;
use City;
use Event;
use Organization;
use PHDB;
use Project;
use Rest;
use Zone;

class GetZoneInfoAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null, $level = null){
        if($id && $level){
            $orga = PHDB::count(Organization::COLLECTION, ["address.level$level" => $id]);
            $citoyen = PHDB::count(Citoyen::COLLECTION, ["address.level$level" => $id]);
            $project = PHDB::count(Project::COLLECTION, ["address.level$level" => $id]);
            $event = PHDB::count(Event::COLLECTION, ["address.level$level" => $id]);
            $zone = PHDB::count(Zone::COLLECTION, ["level$level" => $id, "level" => ['$ne' => "6"]]);
            $shape = PHDB::findOneById(Zone::COLLECTION, $id, ["geoShape" => 1, "cities" => 1]);
            if(isset($shape["cities"])){
                $city = count($shape["cities"]);
            }else{
                $city = PHDB::count(City::COLLECTION, ["level$level" => $id]);
            }
            return Rest::json([
                "orga" => $orga,
                "citoyen" => $citoyen,
                "project" => $project,
                "event" => $event,
                "all" => $orga+$citoyen+$project+$event,
                "zone" => $zone,
                "city" => $city,
                "geoShape"  => $shape
            ]);
        }
    }
}
