<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use City;
use MongoId;
use PHDB;
use Rest;
use Zone;

class UpdateCommuneV7VAction extends \PixelHumain\PixelHumain\components\Action
{
    private $name = [
        "Manakara Atsimo" => "Manakara"
    ];
    public function run(){
        // Vatovavy Fitovinany
        $zones = PHDB::find(Zone::COLLECTION, array("level3Name" => array('$in' => ["Vatovavy", "Fitovinany"])), ["name", "level2", "level2Name", "level3", "level3Name", "level1", "level1Name"]);
        $districts = array();
        array_map(function($item) use (&$districts){
            $districts[$item["name"]] = array(
                "level1" => $item["level1"],
                "level1Name" => $item["level1Name"],
                "level2" => $item["level2"],
                "level2Name" => $item["level2Name"],
                "level3" => $item["level3"],
                "level3Name" => $item["level3Name"],
                "level4" => (string)$item["_id"],
                "level4Name" => $item["name"],
            );
        }, array_values($zones));
        $cities = PHDB::find(City::COLLECTION, array("level3Name" => "Vatovavy Fitovinany"), ["geoShape" => 0]);
        $returned = [];
        foreach ($cities as $key => $city) {
            $data = $city;
            unset($data['_id']);
            $name = isset($this->name[$city["level4Name"]]) ? $this->name[$city["level4Name"]] : $city["level4Name"];
            $data = array_merge($data, $districts[$name]);
            $returned[$city["name"]] = $data;
            PHDB::update(City::COLLECTION, ["_id" => new MongoId($key)], ['$set' => $data]);
        }
        return Rest::json($returned);
    }
}
