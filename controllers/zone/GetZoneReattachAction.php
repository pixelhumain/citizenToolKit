<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

    use PHDB;
    use Rest;
    use Zone;

    class GetZoneReattachAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            if(!empty($_POST['zoneId']) && !empty($_POST['level'])) {
                $where = array(
                    "level".$_POST['level'] => $_POST['zoneId'],
                    "level" => array('$ne' => '6')
                );
                $zone = PHDB::find(Zone::COLLECTION, $where);
    
                return Rest::json($zone);
            }
        }
    }
?>