<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoId;
use PHDB,PixelHumain\PixelHumain\components\Action,Rest,Role,Yii,Zone;

class UpdateSirenEPCIAction extends Action
{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $epcis = array(
                "5c962e896ff9924242d40c18" => array(
                    "SIREN" => "200090561",
                    "NAME" => "CC Vierzon-Sologne-Berry"
                ),
                "5c962e8f6ff9924242d40caa" => array(
                    "SIREN" => "242300135",
                    "NAME" => "CC du Pays Sostranien"
                ),
                "5c962e976ff9920256d40cc7" => array(
                    "SIREN" => "200089456",
                    "NAME" => "CA Seine-Eure"
                ),
                "5c962eae6ff992ff55d40eeb" => array(
                    "SIREN" => "200085751",
                    "NAME" => "CC Entre Bièvre et Rhône"
                ),
                "5c962eb16ff992ff55d40f10" => array(
                    "SIREN" => "200090579",
                    "NAME" => "CC Terre d’Émeraude Communauté"
                ),
                "5c962ebb6ff9924442d40d44" => array(
                    "SIREN" => "200085728",
                    "NAME" => "CC Brioude Sud Auvergne"
                ),
                "5c962ec56ff9924442d40dcd" => array(
                    "SIREN" => "200096956",
                    "NAME" => "CA Agglomération d’Agen"
                ),
                "5c962edb6ff9920e56d417dd" => array(
                    "SIREN" => "200083392",
                    "NAME" => "CA Laval Agglomération"
                ),
                "5c962ee56ff9920e56d41870" => array(
                    "SIREN" => "200096683",
                    "NAME" => "CC Centre Morbihan Communauté"
                ),
                "5c962ef16ff9920156d41600" => array(
                    "SIREN" => "200093201",
                    "NAME" => "Métropole Européenne de Lille"
                ),
                "5c962f056ff9920156d416b7" => array(
                    "SIREN" => "200090751",
                    "NAME" => "CA Grand Calais Terres et Mers"
                ),
                "5c962f4c6ff9924542d41374" => array(
                    "SIREN" => "200084952",
                    "NAME" => "CU Le Havre Seine Métropole"
                ),
                "5c962f546ff9924542d4138d" => array(
                    "SIREN" => "200090504",
                    "NAME" => "CA Coulommiers Pays de Brie"
                ),
                "5c962f7e6ff9924342d40f47" => array(
                    "SIREN" => "200096634",
                    "NAME" => "CC des Hautes Vosges"
                ),
            );
            $key = 0;
            foreach ($epcis as $old => $new){
                PHDB::update(Zone::COLLECTION, ["_id" => new MongoId($old)], ['$set' => array("epci" => $new["SIREN"], "name" => $new["NAME"])]);
                $key++;
            }
            return Rest::json("$key EPCIs changés");
        }else{
            return "Vous n'êtes pas autorisé";
        }
    }
}