<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoId;
use PHDB;
use PixelHumain\PixelHumain\components\Action;
use Rest;
use SIG;
use Zone;

class UpdateZoneAction extends Action{
    public function run(){
        $data = $_POST["value"];
        if(isset($data["geoPosition"])){
            $data["geoPosition"]["coordinates"] = array_map(function($coordinate){
                return doubleval($coordinate);
            }, $data["geoPosition"]["coordinates"]);
        }
        if(isset($data["latitude"]) && isset($data["longitude"])){
            $data["geo"] = SIG::getFormatGeo($data["latitude"], $data["longitude"]);
            $data["geoPosition"] = SIG::getFormatGeoPosition($data["latitude"], $data["longitude"]);
            unset($data["latitude"]);
            unset($data["longitude"]);
        }
        $result["result"] = PHDB::update(Zone::COLLECTION, array("_id" => new MongoId($_POST['id'])), array('$set' => $data));
        $result["message"] = "Element Updated";
        $result["data"] = $_POST["value"];   
        return Rest::json($result);
    }
}