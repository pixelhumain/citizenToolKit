<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use City;
use DataValidator;
use Event;
use MongoId;
use Organization;
use Person;
use PHDB;
use Project;
use Rest;
use Role;
use Yii;
use Zone;

class MergeZonetoZoneAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $data = DataValidator::clearUserInput($_POST);
            if(isset($data["zoneMerge"]) && isset($data["zoneDestination"]) && isset($data["level"])){
                $zone = PHDB::findOne(Zone::COLLECTION, array("_id" => new MongoId($data["zoneDestination"])));
                $toUpdate = array(
                    "address.level1" => $zone["level1"],
                    "address.level1Name" => $zone["level1Name"],
                );
                if($data["level"] == "2"){
                    $toUpdate["address.level2"] = $data["zoneDestination"];
                    $toUpdate["address.level2Name"] = $zone["name"];
                }else if($data["level"] == "3"){
                    $toUpdate["address.level2"] = $zone["level2"];
                    $toUpdate["address.level2Name"] = $zone["level2Name"];
                    $toUpdate["address.level3"] = $data["zoneDestination"];
                    $toUpdate["address.level3Name"] = $zone["name"];
                }else if($data["level"] == "4"){
                    $toUpdate["address.level2"] = $zone["level2"];
                    $toUpdate["address.level2Name"] = $zone["level2Name"];
                    $toUpdate["address.level3"] = $zone["level3"];
                    $toUpdate["address.level3Name"] = $zone["level3Name"];
                    $toUpdate["address.level4"] = $data["zoneDestination"];
                    $toUpdate["address.level4Name"] = $zone["name"];
                }
                PHDB::updateWithOptions(Organization::COLLECTION, array("address.level".$data["level"] => $data["zoneMerge"]), array('$set' => $toUpdate), array('multiple' => true, "multi" => true));
                PHDB::updateWithOptions(Event::COLLECTION, array("address.level".$data["level"] => $data["zoneMerge"]), array('$set' => $toUpdate), array('multiple' => true, "multi" => true));
                PHDB::updateWithOptions(Project::COLLECTION, array("address.level".$data["level"] => $data["zoneMerge"]), array('$set' => $toUpdate), array('multiple' => true, "multi" => true));
                PHDB::updateWithOptions(Person::COLLECTION, array("address.level".$data["level"] => $data["zoneMerge"]), array('$set' => $toUpdate), array('multiple' => true, "multi" => true));
                $toCities = $toUpdate = array_combine(
                    array_map(function($key) {
                        return preg_replace('/^address\./', '', $key);
                    }, array_keys($toUpdate)),
                    array_values($toUpdate)
                );
                PHDB::updateWithOptions(City::COLLECTION, array("level".$data["level"] => $data["zoneMerge"]), array('$set' => $toCities), array('multiple' => true, "multi" => true));
                PHDB::remove(Zone::COLLECTION, array("_id" => new MongoId($data["zoneMerge"])));
                return Rest::json(array("success" => true, "msg" => "Zone mergée avec ".$zone["name"]));
            }
            return Rest::json(array("success" => false, "msg" => "Impossible de faire la merge sans le bon paramètre"));
        }
        return Rest::json(array("success" => false, "msg" => "Vous n'avez pas les droits pour faire cette action"));
    }
}
