<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoId;
use PHDB;
use Rest;
use Yii;
use Zone;

class UpdateTranslateZoneMadaAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($level = null){
        if($level){
            $datadistrict = PHDB::find(Zone::COLLECTION, array("countryCode" => "MG", "level" => "$level"), ["name", "translateId"]);
            $translated = [];
            foreach ($datadistrict as $key => $value) {
                $translate = PHDB::findOneById(Zone::TRANSLATE, $value["translateId"]);
                if($key != $translate["parentId"]){
                    $data = $translate;
                    unset($data["_id"]);
                    $data["parentType"] = Zone::COLLECTION;
                    $data["parentId"] = $key;
                    $translated[$translate["origin"]] = $data;
                    Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($data);
                    PHDB::update(Zone::COLLECTION, 
                        array("_id"=>new MongoId($key)),
                        array('$set' => array("translateId" => (string)$data["_id"]))
                    );
                }
            }
            return Rest::json($translated);
        }
    }
}
