<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoId;
use PHDB;
use Rest;
use Role;
use Yii;
use Zone;

class UpdateTypeAction extends \PixelHumain\PixelHumain\components\Action
{
    private $type = [
        "1" => "Pays",
        "2" => "",
        "3" => "Région",
        "4" => "Département",
        "5" => "EPCI"
    ];
    public function run($level = null){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))) {
            $zones = PHDB::find(Zone::COLLECTION, ["level" => "$level"], ["level", "name", "type"]);
            $i = 0;
            foreach ($zones as $key => $zone){
                if(isset($zone["type"])){
                    if(!in_array($this->type[$level], $zone["type"])){
                        array_push($zone["type"], $this->type[$level]);
                    }
                }else{
                    $zone["type"][] = $this->type[$level];
                }
                PHDB::update(Zone::COLLECTION, array("_id" => new MongoId($key)), array('$set' => array("type" => $zone["type"])));
                $i++;
            }
            return Rest::json("$i zones modifiées");
        }else{
            return Rest::json("Who the hell are you ?");
        }
    }
}