<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use PHDB;
use Role;
use Yii;

class InsertionBatimentsLaPossesionAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){  

        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){  
            ini_set('memory_limit', '256M');
            set_time_limit(50000);  

            $data = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Hackaton/Batiments.geojson"), true);
            $allData = [];
            $elementsProcessed = 0;
            
            foreach ($data["features"] as $value) {

                $data = [
                    "usage" => $value["properties"]["USAGE1"],
                    "surface" => $value["properties"]["surface"],
                    "nature" => $value["properties"]["NATURE"],
                    "geometry" => $value["geometry"]
                ];   
                
                $allData[] = $data;
                $elementsProcessed++;
            }

            
            if (count($allData) > 0) {
                PHDB::batchInsert("lapossesion", $allData);
                echo "<br/>".$elementsProcessed." elements processed";
            }
        } else {
            echo "casse toi de la";
        }
    }

}