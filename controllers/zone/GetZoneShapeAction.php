<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use City;
use MongoId;
use PHDB;
use Rest;
use Zone;

class GetZoneShapeAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $data = $_POST;
        if(is_numeric($data["level"]) || $data["level"] == "QPV"){
            $where = array(
                "countryCode" => $data["countryCode"],
                "level" => $data["level"],
            );
            if($data["level"] == "QPV"){
                $where = array(
                    "type" => $data["level"]
                );
                if(isset($data["parentId"]) && !empty($data["parentId"]) && isset($data["parentLevel"]) && !empty($data["parentLevel"])){
                    $where["level5"] = $data["parentId"];
                }
            }
            if($data["level"] != "QPV" && isset($data["parentId"]) && !empty($data["parentId"]) && isset($data["parentLevel"]) && !empty($data["parentLevel"])){
                $where["level".$data["parentLevel"]] = $data["parentId"];
            }
            $zoneShape = PHDB::find(Zone::COLLECTION, $where);
        }else if($data["level"] == "cities"){
            if(isset($data["parentLevel"]) && $data["parentLevel"] == "5"){
                $epcis = PHDB::findOneById(Zone::COLLECTION, $data["parentId"], ["cities" => 1]);
                if(isset($epcis["cities"])){
                    $ids = array_map(function($id){
                        return new MongoId($id);
                    }, $epcis["cities"]);
                    $zoneShape = PHDB::find(City::COLLECTION, ["_id" => ['$in' => $ids]]);
                }else{
                    $where = array(
                        "country" => $data["countryCode"],
                    );
                    if($data["level"] != "QPV" && isset($data["parentId"]) && !empty($data["parentId"]) && isset($data["parentLevel"]) && !empty($data["parentLevel"])){
                        $where["level4"] = $data["parentId"];
                    }
                    $zoneShape = PHDB::find(City::COLLECTION, $where);
                }
            }else{
                $where = array(
                    "country" => $data["countryCode"],
                );
                if($data["level"] != "QPV" && isset($data["parentId"]) && !empty($data["parentId"]) && isset($data["parentLevel"]) && !empty($data["parentLevel"])){
                    $where["level4"] = $data["parentId"];
                }
                $zoneShape = PHDB::find(City::COLLECTION, $where);
            }
        }
        // return Rest::json($where);
        return Rest::json($zoneShape);
    }
}
