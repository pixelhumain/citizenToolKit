<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoDate;
use MongoId;
use PHDB;
use Rest;
use SIG;
use Yii;
use Zone;

class BelgiqueProvinceAction extends \PixelHumain\PixelHumain\components\Action
{
    private $additionnalData = ["5979d1f36ff9925a108b4578" => "Bruxelles-Capitale"];
    public function run(){
        $provinces = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Belgique/Provinces.geojson"), true);
        $dataZones = PHDB::find(Zone::COLLECTION, array("countryCode" => "BE", "level" => "2"), ["name", "level1", "level1Name"]);
        $zones = array();
        $dataToReturned = [];
        array_map(function($item) use (&$zones){
            $zones[$item["name"]] = array(
                "level1" => $item["level1"],
                "level1Name" => $item["level1Name"],
                "level2" => (string)$item["_id"],
                "level2Name" => $item["name"],
            );
        }, array_values($dataZones));
        foreach ($provinces["features"] as $value) {
            $data = [
                "name" => $value["properties"]["prov_name_f"],
                "countryCode" => "BE",
                "geoShape" => $value["geometry"],
                "modified" =>  new MongoDate(time()),
                "updated" => time()
            ];  
            $data = array_merge($data, $zones[$value["properties"]["reg_name_fr"]]);
            if(isset($value["properties"]["osm_id"]) && isset($value["properties"]["osm_type"])){
                $zoneNominatim = json_decode(SIG::getOSMDetails($value["properties"]["osm_id"], $value["properties"]["osm_type"]), true);
                if(!empty($zoneNominatim)){
                    $fieldName = (!empty($zoneNominatim["namedetails"]) ? "namedetails" : "names");
                    $origin = $value["properties"]["prov_name_f"];
                    $data["osmID"] = $value["properties"]["osm_id"];
                    $data["insee"] = $value["properties"]["osm_id"]."*BE";
                    if(isset($zoneNominatim["geometry"]["coordinates"])){
                        $data["geoPosition"] = SIG::getFormatGeoPosition($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                        $data["geo"] = SIG::getFormatGeo($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                    }
                    if(isset($zoneNominatim["extratags"]["wikidata"])){
                        $data["wikidataID"] = $zoneNominatim["extratags"]["wikidata"];
                    }
                    if(!empty($zoneNominatim[$fieldName])){
                        foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                            $arrayName = explode(":", $keyName);
                            if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName){
                                $translates[strtoupper($arrayName[1])] = $valueName;
                                if($arrayName[1] == "fr"){
                                    $data["name"] = $valueName;
                                }
                            }
                        }
                    }
                }
            }
            if(count($translates) == 0){
                if(isset($value["properties"]["reg_name_d"])){
                    $translate["DE"] = $value["properties"]["reg_name_d"];
                }
                if(isset($value["properties"]["reg_name_n"])){
                    $translate["NL"] = $value["properties"]["reg_name_n"];
                }
            }
            if(isset($value["properties"]["co_id"])){
                PHDB::update(Zone::COLLECTION, array("_id" => new MongoId($value["properties"]["co_id"])), array('$set' => $data));
                $info = PHDB::findOneById(Zone::COLLECTION, $value["properties"]["co_id"], ["translateId", "name"]);
                PHDB::update(Zone::TRANSLATE, array("_id" => new MongoId($info["translateId"])), array('$set' => ["translates" => $translates, "origin" => $data["name"]]));
                $data["_id"] = $value["properties"]["co_id"];
            }else{
                $data["level"] = ["3"];
                $data["created"] = time();
                $data["creator"] = Yii::app()->session["userId"];
                Yii::app()->mongodb->selectCollection(Zone::COLLECTION)->insert($data);
                $translate = [];
                $translate["countryCode"] = "BE";
                $translate["parentId"] = (string)$data["_id"];
                $translate["parentType"] = Zone::COLLECTION;
                $translate["translates"] = $translates;
                $translate["origin"] = $data["name"];
                Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($translate);
                $dataToUpdated = array("translateId" => (string)$translate["_id"]);
                PHDB::update(Zone::COLLECTION, 
                    array("_id"=>new MongoId((string)$data["_id"])),
                    array('$set' => $dataToUpdated)
                );
            }
            $dataToReturned[$data["name"]] = (string) $data['_id'];
        }
        $regions = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Belgique/Region.geojson"), true);
        foreach ($regions["features"] as $value) {
            if($value["properties"]["reg_name_f"] == "Bruxelles-Capitale"){
                $data = [
                    "name" => $value["properties"]["reg_name_f"],
                    "countryCode" => "BE",
                    "geoShape" => $value["geometry"],
                    "modified" =>  new MongoDate(time()),
                    "updated" => time()
                ];
                $data = array_merge($data, $zones["Bruxelles-Capitale"]);
                $translates = array();
                if(isset($value["properties"]["osm_id"]) && isset($value["properties"]["osm_type"])){
                    $zoneNominatim = json_decode(SIG::getOSMDetails($value["properties"]["osm_id"], $value["properties"]["osm_type"]), true);
                    if(!empty($zoneNominatim)){
                        $fieldName = (!empty($zoneNominatim["namedetails"]) ? "namedetails" : "names");
                        $origin = $value["properties"]["reg_name_f"];
                        $data["osmID"] = $value["properties"]["osm_id"];
                        $data["insee"] = $value["properties"]["osm_id"]."*BE";
                        if(isset($zoneNominatim["geometry"]["coordinates"])){
                            $data["geoPosition"] = SIG::getFormatGeoPosition($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                            $data["geo"] = SIG::getFormatGeo($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                        }
                        if(isset($zoneNominatim["extratags"]["wikidata"])){
                            $data["wikidataID"] = $zoneNominatim["extratags"]["wikidata"];
                        }
                        if(!empty($zoneNominatim[$fieldName])){
                            foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                                $arrayName = explode(":", $keyName);
                                if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName){
                                    $translates[strtoupper($arrayName[1])] = $valueName;
                                    if($arrayName[1] == "fr"){
                                        $data["name"] = $valueName;
                                    }
                                }
                            }
                        }
                    }
                }
                if(count($translates) == 0){
                    if(isset($value["properties"]["reg_name_d"])){
                        $translate["DE"] = $value["properties"]["reg_name_d"];
                    }
                    if(isset($value["properties"]["reg_name_n"])){
                        $translate["NL"] = $value["properties"]["reg_name_n"];
                    }
                }
                PHDB::update(Zone::COLLECTION, array("_id" => new MongoId("5979d1f36ff9925a108b4578")), array('$set' => $data));
                $info = PHDB::findOneById(Zone::COLLECTION, "5979d1f36ff9925a108b4578", ["translateId", "name"]);
                PHDB::update(Zone::TRANSLATE, array("_id" => new MongoId($info["translateId"])), array('$set' => ["translates" => $translates, "origin" => $data["name"]]));
                $data["_id"] = "5979d1f36ff9925a108b4578";
                $dataToReturned[$data["name"]] = (string) $data['_id'];
            }
        }
        return Rest::json($dataToReturned);
    }
}