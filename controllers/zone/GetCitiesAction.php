<?php 

    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

    use City;
    use MongoId;
    use PHDB;
    use Rest;
    use Zone;

    class GetCitiesAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            if(!empty($_POST['zoneId']) && !empty($_POST['level'])) {
                $where = array();
                $cities = array();
                if($_POST['level'] == "4") {
                    $where = array(
                        "level4" => $_POST['zoneId'],    
                    );

                    $cities = PHDB::find(City::COLLECTION, $where, array("geoShape" => 0));

                } else if($_POST['level'] == "5") {
                    $zone = PHDB::findOne(Zone::COLLECTION, array(
                        "_id" => new MongoId($_POST['zoneId']),
                    ), array("cities" => 1)); 
                    
                    if (isset($zone["cities"]) && !empty($zone["cities"])) {
                    
                        $where = array(
                            "_id" => array(
                                '$in' => array_map(function ($id){
                                    return new MongoId($id);
                                }, $zone["cities"])
                            )
                        );
                
                        $cities = PHDB::find(
                            City::COLLECTION,
                            $where,
                            array("geoShape" => 0)
                        );
                    } 
                }

                return Rest::json($cities);
            }
        }
    }
?>