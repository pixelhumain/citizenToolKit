<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoId;
use PHDB;
use Project;
use Rest;

class UpdateScopeEPCI extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $ids = [
            "5c962e6f6ff992e456d40ca6level5",
            "5c962e896ff9924242d40c1clevel5",
            "5c962e976ff9920256d40ccblevel5",
            "5c962eae6ff992ff55d40ed8level5",
            "5c962eb16ff992ff55d40f14level5",
            "5c962eb16ff992ff55d40f21level5",
            "5c962eb26ff992ff55d40f2dlevel5",
            "5c962ec66ff9924442d40de2level5",
            "5c962edc6ff9920e56d417f0level5",
            "5c962ef16ff9920156d415f8level5",
            "5c962f4b6ff9924542d41362level5",
            "5c962f4c6ff9924542d41376level5",
            "5c962f556ff992e456d40dealevel5"
        ];
        $whereDeleted = array_map(function($items){
            return array("scope.$items" => array('$exists' => true));
        }, $ids);

        $i = 0;
        $dataDeleted = PHDB::find(Project::COLLECTION, array('$or' => $whereDeleted), array("scope" => 1, "name" => 1));
        foreach ($dataDeleted as $ctrekey => $ctre){
            $intersect = array_intersect($ids, array_keys($ctre["scope"]));
            $ctre["scope"] = array_diff_key($ctre["scope"], array_flip($intersect));
            PHDB::update(Project::COLLECTION, array('_id' => new MongoId($ctrekey)), array('$set' => array("scope" => $ctre["scope"])));
            $i++;
        }
        $idModified = [
            "5c962e896ff9924242d40c18level5" => "CC Vierzon-Sologne-Berry",
            "5c962e8f6ff9924242d40caalevel5" => "CC du Pays Sostranien",
            "5c962e976ff9920256d40cc7level5" => "CA Seine-Eure",
            "5c962eae6ff992ff55d40eeblevel5" => "CC Entre Bièvre et Rhône",
            "5c962eb16ff992ff55d40f10level5" => "CC Terre d’Émeraude Communauté",
            "5c962ebb6ff9924442d40d44level5" => "CC Brioude Sud Auvergne",
            "5c962ec56ff9924442d40dcdlevel5" => "CA Agglomération d’Agen",
            "5c962edb6ff9920e56d417ddlevel5" => "CA Laval Agglomération",
            "5c962ee56ff9920e56d41870level5" => "CC Centre Morbihan Communauté",
            "5c962ef16ff9920156d41600level5" => "Métropole Européenne de Lille",
            "5c962f056ff9920156d416b7level5" => "CA Grand Calais Terres et Mers",
            "5c962f4c6ff9924542d41374level5" => "CU Le Havre Seine Métropole",
            "5c962f546ff9924542d4138dlevel5" => "CA Coulommiers Pays de Brie",
            "5c962f7e6ff9924342d40f47level5" => "CC des Hautes Vosges"
        ];
        $whereModifieds = array_map(function($items){
            return array("scope.$items" => array('$exists' => true));
        }, array_keys($idModified));
        $data = PHDB::find(Project::COLLECTION, array('$or' => $whereModifieds), array("scope" => 1, "name" => 1));
        foreach ($data as $ctrekey => $ctre){
            $intersect = array_intersect_key($idModified, array_flip(array_keys($ctre["scope"])));
            foreach ($intersect as $key => $value){
                $ctre['scope'][$key]['name'] = $value;
            }
            PHDB::update(Project::COLLECTION, array('_id' => new MongoId($ctrekey)), array('$set' => array("scope" => $ctre["scope"])));
            $i++;
        }
        return Rest::json("$i projet(s) modifiée(s)");
    }
}