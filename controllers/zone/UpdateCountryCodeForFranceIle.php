<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use PHDB;
use Zone;

class UpdateCountryCodeForFranceIle extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $where = array(
            '$and' => [
                ["level" => "1"],
                ["level" => "3"],
            ]
        );
        $countries = PHDB::find(Zone::COLLECTION, $where, ["geoShape" => 0]);
        foreach ($countries as $key => $country) {
            PHDB::updateWithOptions(Zone::COLLECTION, ["level3" => $key, "level" => "5"], [
                '$set' => [
                    "countryCode" => $country["countryCode"]
                ]
            ], ["multi" => true, "multiple" => true]);
        }
    }
}
