<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use PHDB;
use Zone;

class AddNewEPCIAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $arrayVar = [
            [
                "name" => "Plaine Commune",
                "epci" => "200057867",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1c4f6ff992f0038b45d6",
                "level4Name" => "SEINE-SAINT-DENIS",
            ],
            [
                "name" => "Boucle Nord de Seine",
                "epci" => "200057990",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1b8a6ff992f0038b45a1",
                "level4Name" => "HAUTS-DE-SEINE",
            ],
            [
                "name" => "Paris Ouest La Défense",
                "epci" => "200057982",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1b8a6ff992f0038b45a1",
                "level4Name" => "HAUTS-DE-SEINE",
            ],
            [
                "name" => "Grand Paris Seine Ouest",
                "epci" => "200057974",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1b8a6ff992f0038b45a1",
                "level4Name" => "HAUTS-DE-SEINE",
            ],
            [
                "name" => "Vallée Sud-Grand Paris",
                "epci" => "200057966",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1b8a6ff992f0038b45a1",
                "level4Name" => "HAUTS-DE-SEINE",
            ],
            [
                "name" => "Grand-Orly Seine Bièvre",
                "epci" => "200058014",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1d4b6ff992f0038b460e",
                "level4Name" => "VAL-DE-MARNE",
            ],
            [
                "name" => "Grand Paris Sud-Est Avenir",
                "epci" => "200058006",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1d4b6ff992f0038b460e",
                "level4Name" => "VAL-DE-MARNE",
            ],
            [
                "name" => "Paris Est Marne et Bois",
                "epci" => "200057941",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1d4b6ff992f0038b460e",
                "level4Name" => "VAL-DE-MARNE",
            ],
            [
                "name" => "Grand Paris Grand Est",
                "epci" => "200058790",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1c4f6ff992f0038b45d6",
                "level4Name" => "SEINE-SAINT-DENIS",
            ],
            [
                "name" => "Paris Terres d'Envol",
                "epci" => "200058097",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1c4f6ff992f0038b45d6",
                "level4Name" => "SEINE-SAINT-DENIS",
            ],
            [
                "name" => "Est Ensemble",
                "epci" => "200057875",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d18f6ff9925a108b456c",
                "level3Name" => "Île-de-France",
                "level4" => "597b1c4f6ff992f0038b45d6",
                "level4Name" => "SEINE-SAINT-DENIS",
            ],
            [
                "name" => "CC Gérardmer Hautes Vosges",
                "epci" => "200096642",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d1d46ff9925a108b4573",
                "level3Name" => "Grand Est",
                "level4" => "597b1ba26ff992f0038b45a6",
                "level4Name" => "VOSGES",
            ],
            [
                "name" => "CC Baud Communauté",
                "epci" => "200096675",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d1c36ff9925a108b4570",
                "level3Name" => "Bretagne",
                "level4" => "597b1c716ff992f0038b45de",
                "level4Name" => "MORBIHAN",
            ],
            [
                "name" => "CC de Bénévent Grand Bourg",
                "epci" => "242320000",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d1ac6ff9925a108b456f",
                "level3Name" => "Nouvelle-Aquitaine",
                "level4" => "597b1c826ff992f0038b45e5",
                "level4Name" => "CREUSE",
            ],
            [
                "name" => "CC du Pays Dunois",
                "epci" => "242320109",
                "countryCode" => "FR",
                "level" => [
                    "5"
                ],
                "level1" => "58bd5d6494ef471f218b4588",
                "level1Name" => "France",
                "level3" => "5979d1ac6ff9925a108b456f",
                "level3Name" => "Nouvelle-Aquitaine",
                "level4" => "597b1c826ff992f0038b45e5",
                "level4Name" => "CREUSE",
            ],
        ];
        foreach ($arrayVar as $key => $newEPCI) {
            PHDB::insert(Zone::COLLECTION, $newEPCI);
        }
    }
}