<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use City;
use MongoDate;
use MongoId;
use Rest, PHDB, Zone, Yii, Role;
use SIG;

class AddCommuneMadaAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $communeString = file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Commune.geojson");
        $communes = json_decode($communeString, true);
        $centresString = file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/CentreCommune.geojson");
        $centres = json_decode($centresString, true);
        if(isset($communes["features"]) && Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $datadistrict = PHDB::find(Zone::COLLECTION, array("countryCode" => "MG", "level" => "4"), ["name", "level2", "level2Name", "level3", "level3Name", "level1", "level1Name"]);
            $districts = array();
            $dataToReturned = [];
            array_map(function($item) use (&$districts){
                $districts[$item["name"]] = array(
                    "level1" => $item["level1"],
                    "level1Name" => $item["level1Name"],
                    "level2" => $item["level2"],
                    "level2Name" => $item["level2Name"],
                    "level3" => $item["level3"],
                    "level3Name" => $item["level3Name"],
                    "level4" => (string)$item["_id"],
                    "level4Name" => $item["name"],
                );
            }, array_values($datadistrict));
            foreach ($communes["features"] as $key => $commune) {
                $data = [
                    "name" => $commune["properties"]["ADM3_EN"],
                    "alternateName" => strtoupper($commune["properties"]["ADM3_EN"]),
                    "country" => "MG",
                    "geoShape" => $commune["geometry"]
                ];
                $data["modified"] = new MongoDate(time());
                $data["updated"] = time();
                $data = array_merge($data, $districts[$commune["properties"]["ADM2_EN"]]);
                if(isset($commune["properties"]["CO_ID"])){
                    $dataCo = PHDB::findOneById(City::COLLECTION, $commune["properties"]["CO_ID"]);
                    $data = array_merge($dataCo, $data);
                    $data["updator"] = Yii::app()->session["userId"];
                    PHDB::update(City::COLLECTION, array("_id" => new MongoId($commune["properties"]["CO_ID"])), array('$set' => $data));
                }else{
                    $centre = $centres["features"][$key];
                    $data["geo"] = [
                        "@type" => "GeoCoordinates",
                        "latitude" => (string) $centre["geometry"]["coordinates"][1],
                        "longitude" => (string) $centre["geometry"]["coordinates"][0],
                    ];
                    $data["geoPosition"] = array_merge($centre["geometry"], ["float" => true]);
                    if(!empty($commune["properties"]["OSM_ID"])){
                        $data["osmID"] = $commune["properties"]["OSM_ID"];
                        $data["insee"] = $commune["properties"]["OSM_ID"]."*MG";
                    }
                    $data["collection"] = City::COLLECTION;
                    $data["postalCodes"] = [];
                    $data["new"] = true;
                    $data["created"] = time();
                    $data["creator"] = Yii::app()->session["userId"];
                    Yii::app()->mongodb->selectCollection(City::COLLECTION)->insert($data);
                }
                $dataToReturned[$commune["properties"]["ADM3_EN"]] = (string) $data["_id"];
                if(!isset($commune["properties"]["CO_ID"])){
                    $translate = [];
                    $translate["countryCode"] = "MG";
                    $translate["parentId"] = (string)$data["_id"];
                    $translate["parentType"] = City::COLLECTION;
                    $translate["translates"] = [];
                    $translate["origin"] = $commune["properties"]["ADM3_EN"];
                    if(!empty($commune["properties"]["OSM_ID"]) && !empty($commune["properties"]["OSM_TYPE"])){
                        $zoneNominatim = json_decode(SIG::getOSMDetails($commune["properties"]["OSM_ID"], $commune["properties"]["OSM_TYPE"]), true);
                        $translates = array();
                        $fieldName = (!empty($zoneNominatim["namedetails"]) ? "namedetails" : "names");
                        $origin =$commune["properties"]["ADM3_EN"];
                        if(!empty($zoneNominatim[$fieldName])){
                            foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                                $arrayName = explode(":", $keyName);
                                if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName)
                                    $translates[strtoupper($arrayName[1])] = $valueName;
                            }
                        }
                        $translate["translates"] = $translates;
                    }
                    Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($translate);
                    $dataToUpdated = array("translateId" => (string)$translate["_id"]);
                    if(isset($zoneNominatim["extratags"]["wikidata"])){
                        $dataToUpdated["wikidata"] = $zoneNominatim["extratags"]["wikidata"];
                    }
                    PHDB::update(City::COLLECTION, 
                                array("_id"=>new MongoId((string)$data["_id"])),
                                array('$set' => $dataToUpdated)
                    );
                }
            }
            return Rest::json($dataToReturned);
        }
    }
}