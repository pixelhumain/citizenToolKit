<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use City;
use PHDB;
use Rest;
use Zone;

class GetZoneAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $query = array();
        if( !empty($_POST["countryCode"]) ){
            if(is_string($_POST["countryCode"]))
                $query["countryCode"] = $_POST["countryCode"];
            else
                $query["countryCode"] = array('$in' => $_POST["countryCode"]);
        }
        if( !empty($_POST["level"]) ){
            //var_dump("expression");exit;
            $query["level"] = array('$in' => $_POST["level"]);
        }

        if(!empty($_POST["upperLevelId"]) && !empty($_POST["level"])){
            //if(count($POST_level=1)
            $parentLevel=(count($_POST["level"])==1) ? $_POST["level"][0]-1 : 1;
            $query["level".$parentLevel] = $_POST["upperLevelId"] ;
        }
        if(!empty($_POST["level"]) && in_array(6,$_POST["level"])){
            $query["country"] = $query["countryCode"];
            unset($query["countryCode"]);
            unset($query["level"]);
            if(isset($parentLevel)){
                unset($query["level".$parentLevel]);
                $query["level4"] = $_POST["upperLevelId"];
            }
            $response = PHDB::findAndSort(City::COLLECTION, $query,["name" => 1], 0,["geoShape" => 0]);
        }else{
            $response = PHDB::findAndSort(Zone::COLLECTION, $query,["name" => 1], 0,["geoShape" => 0]);
        }
        return Rest::json($response);
    }
}
