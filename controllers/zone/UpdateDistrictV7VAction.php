<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoId;
use PHDB;
use Rest;
use Zone;

class UpdateDistrictV7VAction extends \PixelHumain\PixelHumain\components\Action
{
    private $datatoupdate = [
        "Ifanadiana" => "Vatovavy",
        "Mananjary" => "Vatovavy",
        "Nosy Varika" => "Vatovavy",
        "Vohipeno" => "Fitovinany",
        "Manakara Atsimo" => "Fitovinany",
        "Ikongo" => "Fitovinany"
    ];
    private $name = [
        "Manakara Atsimo" => "Manakara"
    ];
    public function run(){
        $zones = PHDB::find(Zone::COLLECTION, array("name" => array('$in' => ["Vatovavy", "Fitovinany"])), ["name", "level2", "level2Name", "level1", "level1Name"]);
        $regions = array();
        array_map(function($item) use (&$regions){
            $regions[$item["name"]] = array(
                "level1" => $item["level1"],
                "level1Name" => $item["level1Name"],
                "level2" => $item["level2"],
                "level2Name" => $item["level2Name"],
                "level3" => (string)$item["_id"],
                "level3Name" => $item["name"],
            );
        }, array_values($zones));
        $districts = PHDB::find(Zone::COLLECTION, array("level3Name" => "Vatovavy Fitovinany"), ["geoShape" => 0]);
        $returned = [];
        foreach ($districts as $key => $district) {
            $data = $district;
            unset($data['_id']);
            $data = array_merge($data, $regions[$this->datatoupdate[$data["name"]]]);
            if(isset($this->name[$data["name"]])){
                PHDB::update(Zone::TRANSLATE, ["_id" => new MongoId($data["translateId"])], array('$set' => ["origin" => $this->name[$data["name"]]]));
                $data["name"] = $this->name[$data["name"]];
            }
            PHDB::update(Zone::COLLECTION, ["_id" => new MongoId($key)], ['$set' => $data]);
            $returned[$key] = $data;
        }
        return Rest::json($returned);
    }
}
