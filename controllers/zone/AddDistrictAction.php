<?php 

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoId;
use Rest, PHDB, Zone, Yii, Role;

class AddDistrictAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $jsonString = file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/District.geojson");
        $districts = json_decode($jsonString, true);
        $jsonCentre = file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Centre.geojson");
        $centres = json_decode($jsonCentre, true);
        if(isset($districts["features"]) && Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $dataregions = PHDB::find(Zone::COLLECTION, array("countryCode" => "MG", "level" => "3", "level1Name" => ['$exists' => true]), ["name", "level2", "level2Name", "level1", "level1Name"]);
            $regions = array();
            $dataToReturned = [];
            array_map(function($item) use (&$regions){
                $regions[$item["name"]] = array(
                    "level1" => $item["level1"],
                    "level1Name" => $item["level1Name"],
                    "level2" => $item["level2"],
                    "level2Name" => $item["level2Name"],
                    "level3" => (string)$item["_id"],
                    "level3Name" => $item["name"],
                );
            }, array_values($dataregions));
            foreach ($districts["features"] as $key => $district) {
                $data = [
                    "name" => $district["properties"]["ADM2_EN"],
                    "countryCode" => "MG",
                    "level" => ['4'],
                    "geoShape" => $district["geometry"]
                ];
                $data = array_merge($data, $regions[$district["properties"]["ADM1_EN"]]);
                $centre = $centres["features"][$key];
                $data["geo"] = [
                    "@type" => "GeoCoordinates",
                    "latitude" => (string) $centre["geometry"]["coordinates"][1],
                    "longitude" => (string) $centre["geometry"]["coordinates"][0],
                ];
                $data["geoPosition"] = array_merge($centre["geometry"], ["float" => true]);
                Yii::app()->mongodb->selectCollection(Zone::COLLECTION)->insert($data);
                $translate = PHDB::findOne(Zone::TRANSLATE, array("countryCode" => "MG", "origin" => $district["properties"]["ADM2_EN"]));
                if(!$translate){
                    $translate = [];
                    $translate["countryCode"] = "MG";
                    $translate["parentId"] = (string)$data["_id"];
                    $translate["parentType"] = Zone::COLLECTION;
                    $translate["translates"] = [];
                    $translate["origin"] = $district["properties"]["ADM2_EN"];
                    Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($translate);
                }
                PHDB::update(Zone::COLLECTION, array("_id" => new MongoId((string) $data['_id'])), array('$set' => array(
                    "translateId" => (string)$translate['_id']
                )));
                $dataToReturned[$district["properties"]["ADM2_EN"]] = (string) $data['_id'];
            }
            return Rest::json($dataToReturned);
        }
    }
}