<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoDate;
use MongoId;
use PHDB;
use Rest;
use SIG;
use Yii;
use Zone;

class BelgiqueArrondissementAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $arrondissements = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Belgique/Arrondissement.geojson"), true);
        $dataZones = PHDB::find(Zone::COLLECTION, array("countryCode" => "BE", "level" => "3", "level1Name" => ['$exists' => true]), ["name", "level2", "level2Name", "level1", "level1Name"]);
        $zones = array();
        $dataToReturned = [];
        array_map(function($item) use (&$zones){
            $zones[$item["name"]] = array(
                "level1" => $item["level1"],
                "level1Name" => $item["level1Name"],
                "level2" => $item["level2"],
                "level2Name" => $item["level2Name"],
                "level3" => (string)$item["_id"],
                "level3Name" => $item["name"],
            );
        }, array_values($dataZones));
         foreach ($arrondissements["features"] as $value) {
            $data = [
                "name" => $value["properties"]["arr_name_fr"],
                "countryCode" => "BE",
                "geoShape" => $value["geometry"],
                "modified" =>  new MongoDate(time()),
                "updated" => time()
            ];
            $data = array_merge($data, $zones[$value["properties"]["prov_name_f"]]);
            if(isset($value["properties"]["osm_id"]) && isset($value["properties"]["osm_type"])){
                $zoneNominatim = json_decode(SIG::getOSMDetails($value["properties"]["osm_id"], $value["properties"]["osm_type"]), true);
                if(!empty($zoneNominatim)){
                    $fieldName = (!empty($zoneNominatim["namedetails"]) ? "namedetails" : "names");
                    $origin = $value["properties"]["arr_name_fr"];
                    $data["osmID"] = $value["properties"]["osm_id"];
                    $data["insee"] = $value["properties"]["osm_id"]."*BE";
                    if(isset($zoneNominatim["geometry"]["coordinates"])){
                        $data["geoPosition"] = SIG::getFormatGeoPosition($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                        $data["geo"] = SIG::getFormatGeo($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                    }
                    if(isset($zoneNominatim["extratags"]["wikidata"])){
                        $data["wikidataID"] = $zoneNominatim["extratags"]["wikidata"];
                    }
                    if(!empty($zoneNominatim[$fieldName])){
                        foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                            $arrayName = explode(":", $keyName);
                            if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName){
                                $translates[strtoupper($arrayName[1])] = $valueName;
                                if($arrayName[1] == "fr"){
                                    $data["name"] = $valueName;
                                }
                            }
                        }
                    }
                }
            }
            if(isset($value["properties"]["co_id"])){
                PHDB::update(Zone::COLLECTION, array("_id" => new MongoId($value["properties"]["co_id"])), array('$set' => $data));
                $info = PHDB::findOneById(Zone::COLLECTION, $value["properties"]["co_id"], ["translateId", "name"]);
                PHDB::update(Zone::TRANSLATE, array("_id" => new MongoId($info["translateId"])), array('$set' => ["translates" => $translates, "origin" => $data["name"]]));
                $data["_id"] = $value["properties"]["co_id"];
            }else{
                $data["level"] = ["4"];
                $data["created"] = time();
                $data["creator"] = Yii::app()->session["userId"];
                Yii::app()->mongodb->selectCollection(Zone::COLLECTION)->insert($data);
                $translate = [];
                $translate["countryCode"] = "BE";
                $translate["parentId"] = (string)$data["_id"];
                $translate["parentType"] = Zone::COLLECTION;
                $translate["translates"] = $translates;
                $translate["origin"] = $data["name"];
                Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($translate);
                $dataToUpdated = array("translateId" => (string)$translate["_id"]);
                PHDB::update(Zone::COLLECTION, 
                    array("_id"=>new MongoId((string)$data["_id"])),
                    array('$set' => $dataToUpdated)
                );
            }
            $dataToReturned[$data["name"]] = (string) $data['_id'];
        }
        return Rest::json($dataToReturned);
    }
}
