<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoDate;
use MongoId;
use PHDB;
use Rest;
use SIG;
use Yii;
use Zone;

class BelgiqueRegionAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $regions = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Belgique/Region.geojson"), true);
        $dataZones = PHDB::findOne(Zone::COLLECTION, array("countryCode" => "BE", "level" => "1"), ["name"]);
        $dataReturned = [];
        foreach ($regions["features"] as $value) {
            $data = [
                "name" => $value["properties"]["reg_name_f"],
                "countryCode" => "BE",
                "geoShape" => $value["geometry"],
                "modified" =>  new MongoDate(time()),
                "updated" => time(),
                "level1" => (string)$dataZones["_id"],
                "level1Name" => $dataZones["name"]
            ];           
            $translates = array(); 
            if(isset($value["properties"]["osm_id"]) && isset($value["properties"]["osm_type"])){
                $zoneNominatim = json_decode(SIG::getOSMDetails($value["properties"]["osm_id"], $value["properties"]["osm_type"]), true);
                if(!empty($zoneNominatim)){
                    $fieldName = (!empty($zoneNominatim["namedetails"]) ? "namedetails" : "names");
                    $origin = $value["properties"]["reg_name_f"];
                    $data["osmID"] = $value["properties"]["osm_id"];
                    $data["insee"] = $value["properties"]["osm_id"]."*BE";
                    if(isset($zoneNominatim["geometry"]["coordinates"])){
                        $data["geoPosition"] = SIG::getFormatGeoPosition($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                        $data["geo"] = SIG::getFormatGeo($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                    }
                    if(isset($zoneNominatim["extratags"]["wikidata"])){
                        $data["wikidataID"] = $zoneNominatim["extratags"]["wikidata"];
                    }
                    if(!empty($zoneNominatim[$fieldName])){
                        foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                            $arrayName = explode(":", $keyName);
                            if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName){
                                $translates[strtoupper($arrayName[1])] = $valueName;
                                if($arrayName[1] == "fr"){
                                    $data["name"] = $valueName;
                                }
                            }
                        }
                    }
                }
            }
            if(count($translates) == 0){
                if(isset($value["properties"]["reg_name_d"])){
                    $translate["DE"] = $value["properties"]["reg_name_d"];
                }
                if(isset($value["properties"]["reg_name_n"])){
                    $translate["NL"] = $value["properties"]["reg_name_n"];
                }
            }
            if(isset($value["properties"]["co_id"])){
                PHDB::update(Zone::COLLECTION, array("_id" => new MongoId($value["properties"]["co_id"])), array('$set' => $data));
                $info = PHDB::findOneById(Zone::COLLECTION, $value["properties"]["co_id"], ["translateId", "name"]);
                PHDB::update(Zone::TRANSLATE, array("_id" => new MongoId($info["translateId"])), array('$set' => ["translates" => $translates, "origin" => $data["name"]]));
                $data["_id"] = $value["properties"]["co_id"];
            }else{
                $data["level"] = ["2"];
                $data["created"] = time();
                $data["creator"] = Yii::app()->session["userId"];
                Yii::app()->mongodb->selectCollection(Zone::COLLECTION)->insert($data);
                $translate = [];
                $translate["countryCode"] = "BE";
                $translate["parentId"] = (string) $data["_id"];
                $translate["parentType"] = Zone::COLLECTION;
                $translate["translates"] = $translates;
                $translate["origin"] = $data["name"];
                Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($translate);
                $dataToUpdated = array("translateId" => (string)$translate["_id"]);
                PHDB::update(Zone::COLLECTION, 
                    array("_id"=>new MongoId((string)$data["_id"])),
                    array('$set' => $dataToUpdated)
                );
            }
            $dataToReturned[$data["name"]] = (string) $data['_id'];
        }
        return Rest::json(array("data" => $dataToReturned, "count" => count($dataToReturned)));
    }
}
