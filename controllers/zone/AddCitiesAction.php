<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

    use City;
    use PHDB;
    use Rest;
    use SIG;

    class AddCitiesAction extends \PixelHumain\PixelHumain\components\Action {
        public function run(){
            $_data = $_POST;

            $postalCodes = array();
            if(!empty($_data["postalCodes"])){
                $codePostal = $_data["postalCodes"];
                foreach ($codePostal as $value) {
                    $newCP = array();
                    $newCP["postalCode"] = $value;
                    $newCP["name"] = $_data["alternateName"];
                    if(isset($_data["latitude"]) && isset($_data["longitude"])){
                        $newCP["geo"] = SIG::getFormatGeo($_data["latitude"], $_data["longitude"]);
                        $newCP["geoPosition"] = SIG::getFormatGeoPosition($_data["latitude"], $_data["longitude"]);
                    }
                    $postalCodes[] = $newCP;
                }
            }

            $_data["postalCodes"] = $postalCodes;

            if(isset($_data["latitude"]) && isset($_data["longitude"])){
                $_data["geo"] = SIG::getFormatGeo($_data["latitude"], $_data["longitude"]);
                $_data["geoPosition"] = SIG::getFormatGeoPosition($_data["latitude"], $_data["longitude"]);
                unset($_data["latitude"]);
                unset($_data["longitude"]);
            }

            if(isset($_data["geoShape"]["coordinates"])){
                $_data["geoShape"]["coordinates"] = $this->array_map_recursive(function($coordinate){
                    return is_array($coordinate) ? $coordinate : doubleval($coordinate);
                }, $_data["geoShape"]["coordinates"]);
            }
        

            $result['result'] = PHDB::insert(City::COLLECTION, $_data);
            $result["message"] = "Cities add";
            $result["data"] = $_data;   
            return Rest::json($result);
        }

        function array_map_recursive($callback, $array){
            $func = function($item) use (&$func, &$callback) {
                return is_array($item) ? array_map($func, $item) : $callback($item);
            };
            return array_map($func, $array);
        }
    }
?>