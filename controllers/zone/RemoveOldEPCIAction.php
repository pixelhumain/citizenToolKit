<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use PHDB,PixelHumain\PixelHumain\components\Action,Rest,Role,Yii,Zone;

class RemoveOldEPCIAction extends Action
{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $epcis = ["240100578","ZZZZZZZZZ", "241800325", "242700623", "243800778", "200012060", "243900719", "243900412", "200036572", "245300306", "245901061", "247600497", "247600539", "247700438"];
            foreach ($epcis as $key => $epci) {
                PHDB::remove(Zone::COLLECTION, ["epci" => $epci]);
            }
            return Rest::json(count($epcis)." EPCIs supprimés");
        }else{
            return "Vous n'êtes pas autorisé";
        }
    }
}