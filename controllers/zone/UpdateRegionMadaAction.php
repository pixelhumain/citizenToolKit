<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use MongoId, MongoDate;
use PHDB, SIG;
use Rest;
use Zone;
use Role, Yii;

class UpdateRegionMadaAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $regions = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"].Yii::app()->getModule("co2")->getAssetsUrl()."/json/Region23.geojson"), true);
        $dataZones = PHDB::find(Zone::COLLECTION, array("countryCode" => "MG", "level" => "2"), ["name", "level1", "level1Name"]);
        $zones = array();
        $dataToReturned = [];
        array_map(function($item) use (&$zones){
            $zones[$item["name"]] = array(
                "level1" => $item["level1"],
                "level1Name" => $item["level1Name"],
                "level2" => (string)$item["_id"],
                "level2Name" => $item["name"],
            );
        }, array_values($dataZones));
        foreach ($regions["features"] as $value) {
            $data = [
                "name" => $value["properties"]["ADM1_EN"],
                "countryCode" => "MG",
                "geoShape" => $value["geometry"]
            ];
            $data["modified"] = new MongoDate(time());
            $data["updated"] = time();
            $data = array_merge($data, $zones[$value["properties"]["OLD_PROVIN"]]);
            $zoneNominatim = json_decode(SIG::getOSMDetails($value["properties"]["OSM_ID"], $value["properties"]["OSM_TYPE"]), true);
            $dataToReturned[$value["properties"]["ADM1_EN"]] = $zoneNominatim;
            if(!empty($value["properties"]["OSM_ID"])){
                $data["osmID"] = $value["properties"]["OSM_ID"];
                $data["insee"] = $value["properties"]["OSM_ID"]."*MG";
            }
            if(isset($zoneNominatim["geometry"]["coordinates"])){
                $data["geoPosition"] = SIG::getFormatGeoPosition($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
                $data["geo"] = SIG::getFormatGeo($zoneNominatim["geometry"]["coordinates"][1], $zoneNominatim["geometry"]["coordinates"][0]);
            }
            $translates = array();
            if(!empty($zoneNominatim)){
                $fieldName = (!empty($zoneNominatim["namedetails"]) ? "namedetails" : "names");
                $origin = $value["properties"]["ADM1_EN"];
                if(!empty($zoneNominatim[$fieldName])){
                    foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                        $arrayName = explode(":", $keyName);
                        if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName)
                            $translates[strtoupper($arrayName[1])] = $valueName;
                    }
                }
            }
            if(isset($zoneNominatim["extratags"]["wikidata"])){
                $data["wikidataID"] = $zoneNominatim["extratags"]["wikidata"];
            }
            if(isset($value["properties"]["CO_ID"])){
                PHDB::update(Zone::COLLECTION, array("_id" => new MongoId($value["properties"]["CO_ID"])), array('$set' => $data));
                $info = PHDB::findOneById(Zone::COLLECTION, $value["properties"]["CO_ID"], ["translateId", "name"]);
                PHDB::update(Zone::TRANSLATE, array("_id" => new MongoId($info["translateId"])), array('$set' => ["translates" => $translates]));
                $data["_id"] = $value["properties"]["CO_ID"];
            }else{
                $data["level"] = ["3"];
                $data["created"] = time();
                $data["creator"] = Yii::app()->session["userId"];
                Yii::app()->mongodb->selectCollection(Zone::COLLECTION)->insert($data);
                $translate = [];
                $translate["countryCode"] = "MG";
                $translate["parentId"] = (string)$data["_id"];
                $translate["parentType"] = Zone::COLLECTION;
                $translate["translates"] = $translates;
                $translate["origin"] = $value["properties"]["ADM1_EN"];
                Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($translate);
                $dataToUpdated = array("translateId" => (string)$translate["_id"]);
                PHDB::update(Zone::COLLECTION, 
                    array("_id"=>new MongoId((string)$data["_id"])),
                    array('$set' => $dataToUpdated)
                );
            }
            $dataToReturned[$value["properties"]["ADM1_EN"]] = (string) $data['_id'];
        }
        return Rest::json(array("data" => $dataToReturned, "count" => count($dataToReturned)));
    }
}