<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone;

use Event;
use MongoId;
use Organization;
use Person;
use PHDB;
use Project;
use Zone;

class MigrateDataBelgique extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $toMigrate = [
            "4" => [
                "5ab25b8740bb4eab54b9afc3" => "597b1c336ff992f0038b45cd",
                "597b1b666ff992f0038b4595" => "597b1c0c6ff992f0038b45bf",
                "597b1bd06ff992f0038b45b0" => "597b1b996ff992f0038b45a5"
            ]
        ];
        foreach ($toMigrate as $key => $value) {
            foreach ($value as $keyId => $valueId){
                $data = PHDB::findOne(Zone::COLLECTION, array("_id" => new MongoId($valueId)), ["geoShape" => 0, "geoPosition" => 0, "geo" => 0, "wikidataID" => 0, "osmID" => 0, "insee" => 0]);
                $toUpdate = array();
                if($key == "4"){
                    $toUpdate = array(
                        "address.level1" => $data["level1"],
                        "address.level1Name" => $data["level1Name"],
                        "address.level2" => $data["level2"],
                        "address.level2Name" => $data["level2Name"],
                        "address.level3" => $data["level3"],
                        "address.level3Name" => $data["level3Name"],
                        "address.level4" => $valueId,
                        "address.level4Name" => $data["name"],
                    );
                }
                PHDB::updateWithOptions(Organization::COLLECTION, array("address.level4" => $keyId), array('$set' => $toUpdate), array('multiple' => true, "multi" => true));
                PHDB::updateWithOptions(Event::COLLECTION, array("address.level4" => $keyId), array('$set' => $toUpdate), array('multiple' => true, "multi" => true));
                PHDB::updateWithOptions(Project::COLLECTION, array("address.level4" => $keyId), array('$set' => $toUpdate), array('multiple' => true, "multi" => true));
                PHDB::updateWithOptions(Person::COLLECTION, array("address.level4" => $keyId), array('$set' => $toUpdate), array('multiple' => true, "multi" => true));
            }
        }
    }
}
