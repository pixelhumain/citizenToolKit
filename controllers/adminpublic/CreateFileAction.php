<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic;
use CAction;
use Import;
use PHDB;
use Yii;

class CreateFileAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();

        $count = PHDB::count(Import::MAPPINGS, array("init"=> true) ) ;
        if($count == 0){
        	Import::initMappings();	
        }
        
        $userId = Yii::app()->session["userId"];
        $where = array("userId" => $userId);

        $params["allMappings"] = Import::getMappings($where);
        
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("createfile",$params,true);
        else 
           return $controller->render("createfile",$params);
    }
}

?>