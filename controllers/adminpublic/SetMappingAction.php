<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic;

use CAction, Yii, Import, Rest;
class SetMappingAction extends \PixelHumain\PixelHumain\components\Action
{    
    public function run(){

        $controller = $this->getController();
        $userid = Yii::app()->session["userId"];
        $params = Import::setMappings($userid,$_POST);

        return Rest::json($params);
        //Yii::add()->end();
    }
}
?>