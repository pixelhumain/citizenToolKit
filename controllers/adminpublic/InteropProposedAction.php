<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic;

use CAction;
class InteropProposedAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();
    
	    return $controller->renderPartial("interopProposed",array(),true);

    }
}

?>