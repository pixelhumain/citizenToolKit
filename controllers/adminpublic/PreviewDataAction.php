<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic;
use CAction;
use Import;
use Rest;

class PreviewDataAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $params = Import::previewData($_POST);
        // $params = Import::epci($_POST);
        //$params = Import::setWikiDataID($_POST);
        return Rest::json($params);
    }
}

?>