<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic;
use CAction;
use Import;
use Rest;

class AssignDataAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();
        $params = Import::parsing($_POST);
        return Rest::json($params);
    }
}

?>