<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic;

use CAction, Import, Rest;
class DeleteMappingAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){

        $controller = $this->getController();
        $params = Import::deleteMapping($_POST);
        return Rest::json($params);exit;
    }
}
?>