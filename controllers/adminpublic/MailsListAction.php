<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic;

use CAction, Cron, Yii;
class MailsListAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller = $this->getController();
    	$params = array();
        $where=array();
        if(isset($controller->costum) && isset($controller->costum["slug"]))
            $where=array("source.key"=>$controller->costum["slug"]);
    	$params["results"] = Cron::getCron($where);
    	//$params["city"] = json_encode($city) ;
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("mailslist",$params,true);
        else 
           return $controller->render("mailslist",$params);
    }
}

?>