<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cssFile;

use CTKException;
use Cms;
use Css;
use Person;
use Rest;
use Yii;

class GetbycostumidAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!") );	
			try {
				$res = Cms::getCssByCostumId( @$_POST['id']);
			} catch (CTKException $e) {
				$res = array( "result" => false , "msg" => $e->getMessage() );
			}
		return Rest::json($res);
    }
}