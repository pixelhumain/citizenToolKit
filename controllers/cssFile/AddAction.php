<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cssFile;

use CTKException;
use Cms;
use Person;
use Rest;
use Yii;

class AddAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!") );
        if( !Person::logguedAndValid() )
            return array("result"=>false, "msg"=>Yii::t("common","Please Login First") );
        else{	
			try {
				$res = Cms::createORupdateCss( @$_POST['id'], @$_POST['css']);
			} catch (CTKException $e) {
				$res = array( "result" => false , "msg" => $e->getMessage() );
			}
		}

		return Rest::json($res);
    }
}