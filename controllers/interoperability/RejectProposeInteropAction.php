<?php 

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability;

use CAction, PHDB, Yii, MongoId, Rest, Mail;
class RejectProposeInteropAction extends \PixelHumain\PixelHumain\components\Action
{
	function run() {

		$res = PHDB::findOneById('proposeOpenDataSource', $_GET["idpropose"]);

		Mail::rejectProposedInterop($res['url'], $res['userID'], Yii::app()->session["userId"], $res["description"]);

		PHDB::update('proposeOpenDataSource', array("_id" => new MongoId($_GET['idpropose'])) , 
            array('$set' => array("status" => "rejected")));

		return Rest::json(array("result"=>true, "La proposition à bien été refusé"));
	}
}