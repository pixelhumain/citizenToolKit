<?php 

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability;

use CAction, Yii, MongoDate, Rest, Mail;
class ProposeOpenDataSourceAction extends \PixelHumain\PixelHumain\components\Action
{
	function run() {

		$insertArrayRef = array(
			// "email" => "contact@communecter.org",
	        "url" => $_POST["url"],
	        "description" => $_POST["description"],
	        "status" => "proposed",
	        "userID" => Yii::app()->session["userId"],
	    	// "type" => ActionRoom::TYPE_VOTE ,
	   		"created" => new MongoDate(time()) );
		Yii::app()->mongodb->selectCollection( "proposeOpenDataSource")->insert($insertArrayRef);

		Mail::proposeInteropSource($_POST['url'], null, Yii::app()->session["userId"], $_POST['description']);

		return Rest::json(array("result"=>true, "Votre proposition à bien était pris en compte merci d'attendre la réponse d'unadministrateur de Communecter"));
	}
}