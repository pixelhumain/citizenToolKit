<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability;

use CAction;

class IndexAction extends \PixelHumain\PixelHumain\components\Action {

	public function run() {

		$controller=$this->getController();
		$controller->layout = "//layouts/mainSearch";
	return	$controller->render( "index" );
	}
}

?>
