<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability;

use CAction;
class COSMAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
		$controller=$this->getController();
		// $controller->layout = "//layouts/mainSearch";
		return $controller->renderPartial("co-osm", array(), true);
	}
}
?>

