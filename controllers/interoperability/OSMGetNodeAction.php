<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability;

use CAction, Services_OpenStreetMap, Yii;
class OSMGetNodeAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($nodeID = null) {

    	$controller=$this->getController();

		require __DIR__ . '/Services_Openstreetmap/vendor/autoload.php';
		require_once 'Services/OpenStreetMap.php';

		$osm = new Services_OpenStreetMap();
		$res = $osm->getNode($nodeID);

		$myText = print_r($res,true);

		return $myText;		

	}
}
