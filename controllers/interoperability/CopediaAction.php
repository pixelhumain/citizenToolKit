<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability;

use CAction;
class CopediaAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($url_wiki = null) {

		$controller=$this->getController();
		// $controller->layout = "//layouts/mainSearch";
		return $controller->renderPartial("copedia", array("url_wiki" => $url_wiki), true);
	}
}

?>

