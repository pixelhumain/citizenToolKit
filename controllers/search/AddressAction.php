<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;

use CAction, SIG, Rest, Yii;
class AddressAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($filter = null){
        $params = $_POST;
        $res = SIG::searchAddress($params);
        return Rest::json($res);
    }
}