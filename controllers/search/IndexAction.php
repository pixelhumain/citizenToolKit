<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;
use CAction;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run() {
		
		$controller=$this->getController();
		$controller->layout = "//layouts/mainSearch";
       return $controller->render( "index" );

		//return Rest::json(array("result" => true, "list" => $search));
	}
}