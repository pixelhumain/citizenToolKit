<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;

use CAction, Costum, PHDB, Zone, Rest, Slug;
use City;
use MongoId;

class GetZoneAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run() {
		
		if(!empty($_POST["upperLevelId"]) && !empty($_POST["level"]) && in_array(6,$_POST["level"]) ){
			$agglo = PHDB::findOne(Zone::COLLECTION,array("_id"=>new MongoId($_POST["upperLevelId"])), array("geoShape" => 0));
				if(isset($agglo["cities"])){
				// $count=0;
				$cities=array();
					foreach($agglo["cities"] as $i =>$idCity){
					 	$city=PHDB::findOne(City::COLLECTION,array("_id"=>new MongoId($idCity)), array("geoShape" => 0));
					 	//array_push($cities, $city);
					 	$cities[$idCity]=$city;	
					// 	$cities[(string)$city["_id"]]=$city;
					}
				}
				else{
					$cities = PHDB::find(City::COLLECTION,array("level4"=>$_POST["upperLevelId"]), array("geoShape" => 0));
				}	
			return Rest::json($cities);

				
		}
		else{
			$query = array();

			if( !empty($_POST["countryCode"]) ){
				if(is_string($_POST["countryCode"]))
					$query["countryCode"] = $_POST["countryCode"];
				else
					$query["countryCode"] = array('$in' => $_POST["countryCode"]);
			}
			if( !empty($_POST["level"]) ){
				//var_dump("expression");exit;
				$query["level"] = array('$in' => $_POST["level"]);
			}
			// else if(!empty($_POST["level"]) && in_array(6,$_POST["level"]) && isset($_POST["upperLevelId"])){

			if(!empty($_POST["upperLevelId"]) && !empty($_POST["level"]) && !in_array(6,$_POST["level"])){
				//if(count($POST_level=1)
					$parentLevel=(count($_POST["level"])==1) ? $_POST["level"][0]-1 : 1;
				$query["level".$parentLevel] = $_POST["upperLevelId"] ;

			}
						

			if( !empty($_POST["sortBy"]) ) {
                $zones =  PHDB::findAndSort(Zone::COLLECTION, $query, array("name" => 1), 0,array("geoShape" => 0));
                $functionSansAccent = function ($chaine) {
                    if (version_compare(PHP_VERSION, '5.2.3', '>='))
                        $str = htmlentities($chaine, ENT_NOQUOTES, "UTF-8", false);
                    else
                        $str = htmlentities($chaine, ENT_NOQUOTES, "UTF-8");

                    // NB : On ne peut pas utiliser strtr qui fonctionne mal avec utf8.
                    $str = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $str);

                    return $str;
                };
                $name = array_column($zones, 'name');
                $array_no_accet = array_map($functionSansAccent , $name);
                $array_lower= array_map('strtolower', $array_no_accet);
                array_multisort($array_lower, SORT_ASC, $zones );
			}else {
				$orderBy = [];
				if(Costum::isSameFunction("getSortedZone")){
					$orderBy=Costum::sameFunction("getSortedZone");
				}
				$zones =  PHDB::findAndSort(Zone::COLLECTION, $query, $orderBy,0, array("geoShape" => 0));
			}
			return Rest::json($zones);
		}	
	}
}