<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;

use CAction, SearchNew, MongoDate, PHDB, Event, Yii, DateTime, Rest;
class GetEventsForCalendarAction extends \PixelHumain\PixelHumain\components\Action{

    public function run($startDate , $endDate){
        $community = !empty($_POST['community']) ? $_POST['community'] : null;
        $sourceKey = !empty($_POST['sourceKey']) ? $_POST['sourceKey'] : "";
        $searchLocality = isset($_POST['locality']) ? $_POST['locality'] : null;
        $searchGeo = !empty($_POST["geoSearch"]) ? $_POST["geoSearch"] : null;

        
        $controller=$this->getController();
        $queries=SearchNew::getQueries($_POST);
        
      
        $rangeDate=array();
        if($startDate!=null){
            if($endDate!=null)
                $rangeDate=array(
                    '$or' =>array(
                       // array("recurrency" => true),
                        array(
                            "startDate" => array('$gte' => new MongoDate((float)$startDate), '$lte' => new MongoDate((float)$endDate )), 
                            //"startDate" => array('$lte' => new MongoDate((float)$endDate )) 
                        ),
                        array(
                            "startDate" => array( '$lte' => new MongoDate( (float)$startDate )), 
                            "endDate" => array( '$gte' => new MongoDate( (float)$startDate ))
                            //"endDate" => array( '$lte' => new MongoDate( (float)$endDate ) ) 
                        )   
                    )
                );
            else{
                    $rangeDate=array('$or'=> array(
                        array("startDate" => array( '$gte' => new MongoDate( (float)$startDate ) ) ),
                        array("endDate" => array( array('$exists'=>true), array( '$gte' => new MongoDate( (float)$startDate ) )) )));
            }
        }
        else if($endDate!=null)
            $rangeDate = array( "endDate" => array( '$lte' => new MongoDate( (float)$endDate ) ) );
        if(!empty($rangeDate))
            $query = SearchNew::addQuery($queries["global"], $rangeDate );    
        else
            $query=$queries["global"];  
        $sort=array("updated" => -1);
        $allEvents = PHDB::findAndSortAndLimitAndIndex( Event::COLLECTION, $query, 
                                        $sort , 0, 0);
        foreach ($allEvents as $key => $value) {
            //$allEvents[$key]["typeEvent"] = @$allEvents[$key]["type"];
            //$allEvents[$key]["type"] = "events";
            //$allEvents[$key]["typeSig"] = Event::COLLECTION;
            if(@$value["links"]["attendees"][Yii::app()->session["userId"]]){
                $allEvents[$key]["isFollowed"] = true;
            }
            if(@$allEvents[$key]["startDate"] && @$allEvents[$key]["startDate"]->sec){
                $allEvents[$key]["startDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
                $allEvents[$key]["startDate"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
            }
            if(@$allEvents[$key]["endDate"] && @$allEvents[$key]["endDate"]->sec){
                $allEvents[$key]["endDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
                $allEvents[$key]["endDate"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
            }
            // if(!empty($value["organizer"])){
            //     foreach($value["organizer"] as $k => $v){ 
            //         $elt=Element::getElementSimpleById($k, $v["type"],null, array("slug", "profilThumbImageUrl", "name"));
            //         if(!empty($elt)){
            //             $allEvents[$key]["organizer"][$k]["name"] = $elt["name"];
            //             $allEvents[$key]["organizer"][$k]["profilThumbImageUrl"] = ( !empty($elt["profilThumbImageUrl"]) ? $elt["profilThumbImageUrl"] : "" ) ;
            //             $allEvents[$key]["organizer"][$k]["slug"] = @$elt["slug"];
            //         }
            //     }
            // }
            $el = $value;
            if(@$el["links"]) 
            foreach(array("attendees") as $k) 
                if(@$value["links"][$k])
                $allEvents[$key]["counts"][$k] = count(@$value["links"][$k]);
        }
        $res = array("events" => $allEvents,
                        "count" => count($allEvents));
        
        return Rest::json($res);
    }
}