<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;

use CAction, City, SearchNew, Rest, Yii;
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("HTTP/1.1 200 OK");
    exit;
}
class GlobalAutoCompleteAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($filter = null){
        \Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
        \Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        \Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
        $searchP = $_POST;	
        if(@$_GET["name"])
        	$searchP = $_GET;
        
        if(isset($searchP["searchType"]) && count($searchP["searchType"])==1 && in_array("cities", $searchP["searchType"])){
            $search = (@$searchP['name']) ? trim(urldecode($searchP['name'])) : "";
            $country = (@$searchP['country']) ? trim(urldecode($searchP['country'])) : "";
            $subParams = (!empty($searchP["subParams"])) ? $searchP["subParams"] : null;
            $results=City::searchCity( $country, $search, false, false, (!empty($subParams[City::COLLECTION]) ? $subParams[City::COLLECTION] : null ) ) ;
            $res=array("results"=>$results);
        }else
            $res = SearchNew::globalAutoComplete($searchP);
        
        \Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
        \Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        \Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
        return Rest::json($res);
    }
}