<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;

use CAction, SearchNew, DateTime, MongoDate, PHDB, Event, Yii, Rest;
class SearchNextEventAction extends \PixelHumain\PixelHumain\components\Action{

    public function run(){
        
        $controller = $this->getController();
        $searchP = $_POST;    
        $queries=SearchNew::getQueries($searchP);
        //$query = array();
        //$query = Search::searchString("", $query);

        /*if(!empty($controller->costum["slug"])){
            $query = array('$and' => 
                            array(  $query , 
                                    array("source.toBeValidated.".$controller->costum["costum"]["slug"] => array('$exists'=>false) )
                            ) );
            $query = Search::searchSourceKey($controller->costum["slug"], $query);
        } else if(!empty($_POST["sourceKey"]) ) {
            $query = Search::searchSourceKey($_POST["sourceKey"], $query);
        }
        //Rest::json($controller->costum); exit;
        if( !empty($_POST["searchTags"]) ){
            $queryTags =  Search::searchTags($_POST["searchTags"], '$in') ;
            if(!empty($queryTags))
                $query = array('$and' => array( $query , $queryTags) );
        }*/
        
        if(!empty($_POST["startDateUTC"])){
            $date1 = new DateTime($_POST["startDateUTC"]);
            $startD = $date1->getTimestamp();
            /*$queryDate=array(
                '$or' =>array(
                    //array("startDate" => array('$exists' => 0)),
                    array(
                        "startDate" => array(   '$gte' => new MongoDate((float)$startD) 
                    ))
                    // ,
                    // array(
                    //     "startDate" => array( '$lte' => new MongoDate( (float)$startD )), 
                    //     "endDate" => array( '$gte' => new MongoDate( (float)$startD ))
                    // )
                )
            );*/
            $queryDate=array(
                '$and' =>array(
                    array("startDate" => array('$gte' => new MongoDate((float)$startD))), 
                    array("startDate" => array('$exists' => 1))
                )
            );
            $queryEvents = SearchNew::addQuery($queries["global"], $queryDate );
        }
        
       // $sort=array("startDate" => 1);
        //$sort= array();

       /* $query = array('$and' => 
                    array( $query , 
                        array("state" => array('$nin' => array("uncomplete", "deleted")),
                            "status" => array('$nin' => array("uncomplete", "deleted", "deletePending")),
                            '$or'=>array(
                                    array('preferences.private'=>array('$exists'=>false)), 
                                    array('preferences.private'=>false),
                                 )
                        )
                    )
                );*/
        $nextEvent = PHDB::findAndSortAndLimitAndIndex( Event::COLLECTION, $queryEvents, array("startDate" => 1) , 1, 0);
        if(empty($nextEvent)){
            $queryDate=array(
                '$and' =>array(
                    array("endDate" => array('$lte' => new MongoDate((float)$startD))), 
                    array("endDate" => array('$exists' => 1)),
                )
            );
            $queryOldEvents = SearchNew::addQuery($queries["global"], $queryDate );
            $countOld=PHDB::count(Event::COLLECTION, $queryOldEvents);
            $msgOld=($countOld > 1) ?  "<b>{count}</b> pasted events match this request" : "<b>{count}</b> pasted event matches this request";
            $res=array("countPasteEvents"=>$countOld, "msg" => Yii::t("common",$msgOld, array("{count}"=>$countOld)));
        }else
            $res=array("nextEvent"=>$nextEvent);
        return Rest::json($res);
    }
}