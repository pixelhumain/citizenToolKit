<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;

use CAction, Organization, Project, SearchNew, Event, Poi, Classified, Rest, Yii;
use Crowdfunding;

class SimplyAutoCompleteAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($filter = null)
    {

  		// $pathParams = Yii::app()->controller->module->viewPath.'/default/dir/';
		// echo file_get_contents($pathParams."simply.json");
		// die();
        $search = isset($_POST['name']) ? trim(urldecode($_POST['name'])) : null;
        $locality = isset($_POST['locality']) ? trim(urldecode($_POST['locality'])) : null;
        $scope = isset($_POST['scope']) ? $_POST['scope'] : null;
        $searchType = isset($_POST['searchType']) ? $_POST['searchType'] : null;
        $searchTags = isset($_POST['searchTag']) ? $_POST['searchTag'] : null;
        $searchPrefTag = isset($_POST['searchPrefTag']) ? $_POST['searchPrefTag'] : null;
        $searchBy = isset($_POST['searchBy']) ? $_POST['searchBy'] : "INSEE";
        $indexMin = isset($_POST['indexMin']) ? $_POST['indexMin'] : 0;
        $indexMax = isset($_POST['indexMax']) ? $_POST['indexMax'] : 100;
        $country = isset($_POST['country']) ? $_POST['country'] : "";
        $sourceKey = isset($_POST['sourceKey']) ? $_POST['sourceKey'] : null;
        $mainTag = isset($_POST['mainTag']) ? $_POST['mainTag'] : null;
        $paramsFiltre = isset($_POST['paramsFiltre']) ? $_POST['paramsFiltre'] : null;

        $parent = isset($_POST['parent']) ? $_POST['parent'] : null;
        $searchParams=array(
			"options" => null,
			"onlyCount" =>  false,
			"countType"=>  null,
			"indexMin" => $indexMin,
			"indexStep" => $indexMax,
			"initType" => null,
			"countResult" => false,
			"searchType"=>$searchType,
			"searchTypeOrga"=>[],
			"fields"=>["name", "profilThumbImageUrl","profilMediumImageUrl", "shortDescription", "address", "geo", "geoPosition", "collection", "tags"],
			"sortBy"=> array("updated"=>-1) 
		);

		if( !empty($parent) ) {
			//Rest::json($parent); exit ;
			$query = array();
			if(in_array($parent["type"], [Organization::COLLECTION, Project::COLLECTION] )){
				foreach($searchType as $v){
					if($v==Organization::COLLECTION)
						$query=SearchNew::addQuery($query, array('$or' => array("links.memberOf.".$parent["id"]=> array('$exists' => 1) )) );
					else if($v==Project::COLLECTION)
						$query=SearchNew::addQuery($query, array('$or' => array("links.contributors.".$parent["id"]=> array('$exists' => 1) )) );
					else if($v==Event::COLLECTION)
						$query=SearchNew::addQuery($query, array('$or' => array("links.organizer.".$parent["id"]=> array('$exists' => 1) )) );
					else if(in_array($v,[Poi::COLLECTION,Classified::COLLECTION,Crowdfunding::COLLECTION]))
						$query=SearchNew::addQuery($query, array('$or' => array("parent.".$parent["id"]=> array('$exists' => 1) )) );
				}
			}
		}  else {
			/***********************************  DEFINE GLOBAL QUERY   *****************************************/
			$queries=array();	
			$query = SearchNew::startQuery();

			// Condidition sur les éléments incomplet, en cours de suppression ou supprimer
      		$query = SearchNew::addQuery($query, array("state" => array('$nin' => array("uncomplete", "deleted") ) ) );
      		$query = SearchNew::addQuery($query, array("status" => array('$nin' => array("uncomplete", "deleted", "deletePending") ) ) );
      	  	$query = SearchNew::addQuery($query, array('$or'=>array(
						array('preferences.private'=>array('$exists'=>false)), 
						array('preferences.private'=>false),
						array('roles.isBanned'=>array('$exists'=>false))
				 	)
				) 
	      	);

	  		$query = SearchNew::searchText($search, $query);

			/***********************************  TAGS   *****************************************/

			if(!empty($searchTags)) {
				$verbTag = ( (!empty($paramsFiltre) && '$all' == $paramsFiltre) ? '$all' : '$in' ) ;
		  		$query =  SearchNew::searchTags($searchTags, $verbTag, $query);
		  	}

	      	/***********************************   MAINTAG    *****************************************/
	    	if(!empty($mainTag)){
	    		//var_dump($mainTag);exit;
				$verbMainTag = ( (!empty($searchPrefTag) && '$or' == $searchPrefTag) ? '$or' : '$and' );
				$query =  SearchNew::searchTags($mainTag, $verbMainTag, $query);
			}
			// SEARCH BY SOURCEKEY
			$query = SearchNew::searchSourceKey($query, $sourceKey);
		}

	  	$query =  SearchNew::searchLocalityByName($query, $_POST);
	  	if(!empty($scope))
	  		$query =  SearchNew::searchLocality($query, $scope);
	    $allRes = array();
	    $queries=array("global"=>$query);
	    $allRes=SearchNew::getResults($searchParams, $queries);
	  	//trie les éléments dans l'ordre alphabetique par name
	  	function mySort($a, $b){
	  		if(isset($a['name']) && isset($b['name'])){
		    	return ( strtolower($b['name']) < strtolower($a['name']) );
			}else{
				return false;
			}
		}

		usort($allRes, "mySort"); 

	  	$limitRes = $filters = array();
	  	$res['res'] = $allRes;
	  	$res['filters'] = [];
	  	return Rest::json($res);
    }
}
