<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;
use PHDB;
use CAction, SearchNew, Rest, Yii;
class GlobalAutoCompleteAdminAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=null, $id=null, $canSee=null){
        $res = SearchNew::searchAdmin($_POST, $type, $id, $canSee);
        return Rest::json($res);
    }
}