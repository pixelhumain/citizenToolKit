<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;

use CAction, SearchNew, DateTime, MongoDate, PHDB, PHType, Event, Yii, DateTimeZone, Element, Rest;
use Organization, Person, Preference;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;

class AgendaAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($filter = null)
	{
		$searchP = $_POST;
		if (@$_GET["name"])
			$searchP = $_GET;

		$recurrency = (isset($_POST["recurrency"])) ? $_POST["recurrency"] : true;
		$queries = SearchNew::getQueries($searchP);
		$fedeverseEventCount = 0;

		if (!empty($searchP["startDateUTC"]) || !empty($searchP["endDateUTC"])) {
			$parseDateUTC = array();

		    // Handle startDateUTC
            $date1 = new DateTime($searchP["startDateUTC"]);
            $startD = $date1->getTimestamp();
            
            // Handle endDateUTC, default to startDateUTC if not provided
            if (!empty($searchP["endDateUTC"])) {
                $date2 = new DateTime($searchP["endDateUTC"]);
            } else {
                $date2 = new DateTime($searchP["startDateUTC"]);
            }

            $date2->setTime(23, 59, 59);
            $endDay = $date2->getTimestamp();

			$parseDateUTC = array(
				//"dayStr" => $dayStr,
				"startD" => $startD,
				"endDay" => $endDay,
				"year" => $date1->format('Y'),
				"mon" => $date1->format('m'),
				"day" => $date1->format('d')
			);

			 // Query for events between startDate and endDate
			 $queryDate = array(
                '$or' => array(
                    array(
                        "startDate" => array(
                            '$gte' => new MongoDate((float)$startD),
                            '$lte' => new MongoDate($endDay)
                        )
                    ),
                    array(
                        "startDate" => array('$lte' => new MongoDate((float)$startD)),
                        "endDate" => array('$gte' => new MongoDate((float)$startD))
                    )
                )
            );
			
			if ($recurrency === true) {
				$dayStr = substr($date1->format('D'), 0, 2);
				array_push($queryDate['$or'], array(
					"openingHours.dayOfWeek" => $dayStr,
					"startDate" => array('$lte' => new MongoDate((float)$startD)),
					"endDate" => array('$gte' => new MongoDate((float)$startD))
				));
				array_push($queryDate['$or'], array(
					"openingHours.dayOfWeek" => $dayStr,
					"startDate" => array(
						'$gte' => new MongoDate((float)$startD),
						'$lte' => new MongoDate($endDay) 
					)
				));
				array_push($queryDate['$or'], array(
					"openingHours.dayOfWeek" => $dayStr,
					"startDate" => array('$exists' => 0 )
				));
				array_push($queryDate['$or'], array(
					"openingHours.dayOfWeek" => $dayStr,
					"endDate" => array('$exists' => 0 )
				));
				array_push($queryDate['$or'], array(
					"openingHours.dayOfWeek" => $dayStr,
					"startDate" => array('$exists' => 0 ),
					"endDate" => array('$exists' => 0 )
				));
			}
			$queryEvents = SearchNew::addQuery($queries["global"], $queryDate);
			$connectedUserId = Yii::app()->session["userId"];
			if ($connectedUserId && Utils::isActivitypubEnabled()) {

				$queryAp = array(
					'$and' => array(
						array("fromActivityPub" => true),
						array("attributedTo" => ['$in' => Utils::actorFollowsList()]),
						$queryDate,
						$queries["global"]
					)
				);
				$allEvents = array();
				$eventsCo = PHDB::findAndSortAndLimitAndIndex(PHType::TYPE_EVENTS, $queryEvents, array("startDate" => 1));
				$allFederationEvents = PHDB::findAndSortAndLimitAndIndex(PHType::TYPE_EVENTS, $queryAp, array("startDate" => 1));
				foreach ($allEvents as $key => $value) {
					if (@$allEvents[$key]["fromActivityPub"]) {
						unset($allEvents[$key]);
					}
				}

				if (isset($_POST) && ($_POST["fediverse"] != "true" || $_POST["fediverse"] != true)) {
					if (count($eventsCo) > 0) $allEvents = array_merge($allFederationEvents, $eventsCo);
					
				} else {
					if (count($allFederationEvents) > 0) $allEvents = $allFederationEvents;
				}
				$fedeverseEventCount = count($allFederationEvents);
			} else {
				$allEvents = PHDB::findAndSortAndLimitAndIndex(PHType::TYPE_EVENTS, $queryEvents, array("startDate" => 1));
				foreach ($allEvents as $key => $value) {
					if (@$allEvents[$key]["fromActivityPub"]) {
						unset($allEvents[$key]);
					}
				}
			}
		} else {
			$queryEvents = SearchNew::addQuery($queries["global"], array("openingHours" => array('$exists' => false)));
			$allEvents = PHDB::findAndSortAndLimitAndIndex(
				Event::COLLECTION,
				$queryEvents,
				array("startDate" => -1),
				$searchP["indexStep"],
				$searchP["indexMin"]
			);
		}

		if (!empty($allEvents)) {

			foreach ($allEvents as $key => $value) {
				if (@$value["links"]["attendees"][Yii::app()->session["userId"]]) {
					$allEvents[$key]["isFollowed"] = true;
				}
				if (@$allEvents[$key]["fromActivityPub"] && $allEvents[$key]["fromActivityPub"] == true) {
					$activity = Utils::activitypubObjectToEvent($allEvents[$key]["objectUUID"]);
					foreach ($activity as $activityKey => $activityValue) {
						$allEvents[$key][$activityKey] = $activityValue;
					}
				}
				if (@$allEvents[$key]["startDate"] && @$allEvents[$key]["startDate"]->sec) {

					$testS = $allEvents[$key]["startDate"];
					$allEvents[$key]["startDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
					$allEvents[$key]["startDate"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);


					if (!empty($searchP["startDateUTC"]) && !empty($allEvents[$key]["timeZone"])) {
						$timezoneEvent = new DateTimeZone($allEvents[$key]["timeZone"]);
						$date = new DateTime($allEvents[$key]["startDate"]);
						$date->setTimezone($timezoneEvent);
						$date->setDate(intval($parseDateUTC["year"]), intval($parseDateUTC["mon"]), intval($parseDateUTC["day"]));
						$allEvents[$key]["startDateSort"] = $date;
						//$allEvents[$key]["startDateSortFormat"] = $date->format(DateTime::ISO8601);
					}
				}

				if (@$allEvents[$key]["endDate"] && @$allEvents[$key]["endDate"]->sec) {
					$allEvents[$key]["endDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
					$allEvents[$key]["endDate"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
				}

				if (!empty($searchP["startDateUTC"])) {
					if (!empty($allEvents[$key]["openingHours"]) && !empty($allEvents[$key]["timeZone"])) {
						foreach ($allEvents[$key]["openingHours"] as $keyO => $valO) {
							if (!empty($valO) && !empty($valO["dayOfWeek"]) && $valO["dayOfWeek"] == $dayStr && isset($valO["hours"])) {
								$starHourstring = $valO["hours"][0]["opens"];
								$hourArray = explode(":", $starHourstring);
								$startHour = mktime($hourArray[0], $hourArray[1], 00, $parseDateUTC["mon"], $parseDateUTC["day"], $parseDateUTC["year"]);
								$elelele = date(DateTime::ISO8601, $startHour);
								$timezoneEvent = new DateTimeZone($allEvents[$key]["timeZone"]);
								$created_at = new DateTime($elelele);
								$created_at->setTimezone($timezoneEvent);
								$created_at->setTime($hourArray[0], $hourArray[1], 00);
								$allEvents[$key]["startDateSort"] = $created_at;
								$allEvents[$key]["startDateSortFormat"] = $created_at->format(DateTime::ISO8601);
							}
						}
					}
				}

				if (!empty($value["organizer"])) {
					foreach ($value["organizer"] as $k => $v) {
						$elt = Element::getElementSimpleById($k, $v["type"], null, array("slug", "profilThumbImageUrl", "name"));
						if (!empty($elt)) {
							$allEvents[$key]["organizer"][$k]["name"] = $elt["name"];
							$allEvents[$key]["organizer"][$k]["profilThumbImageUrl"] = (!empty($elt["profilThumbImageUrl"]) ? $elt["profilThumbImageUrl"] : "");
							$allEvents[$key]["organizer"][$k]["slug"] = @$elt["slug"];
						}
					}
				}
				$el = $value;
				if (@$el["links"])
					foreach (array("attendees") as $k)
						if (@$value["links"][$k]) $allEvents[$key]["counts"][$k] = count(@$value["links"][$k]);
			}
		}

		if (!empty($searchP["startDateUTC"])) {

			usort($allEvents, function ($a, $b) {
				if (isset($a['startDateSort']) && isset($b['startDateSort'])) {
					return $a["startDateSort"]->getTimestamp() - $b["startDateSort"]->getTimestamp();
				} else {
					return false;
				}
			});
		}
		$results["results"] = $allEvents;
		if (!empty($searchP['count'])) {

			if (!empty($startD)) {
					$queryCountDate = array(
						'$or' => array(
							array("openingHours.dayOfWeek" => array('$exists' => 1)),
							array(
								"startDate" => array(
									'$gte' => new MongoDate((float)$startD)
								)
							),
							array(
								"startDate" => array('$lte' => new MongoDate((float)$startD)),
								"endDate" => array('$gte' => new MongoDate((float)$startD))
							)
						)
					);
				$queries[Event::COLLECTION] = SearchNew::addQuery($queries["global"], $queryCountDate);
			} else {
				$queries[Event::COLLECTION] = SearchNew::addQuery($queries["global"], array("openingHours" => array('$exists' => false)));
			}
			$results["count"] = SearchNew::countResults($searchP, $queries);
			if ($connectedUserId && Utils::isActivitypubEnabled()) {
				$count = $results["count"][Event::COLLECTION] * 1;
				if (isset($_POST) && ($_POST["fediverse"] != "true" || $_POST["fediverse"] != true)) {
					$results["count"][Event::COLLECTION] = $count + $fedeverseEventCount;
				}else{
					$results["count"][Event::COLLECTION] = $fedeverseEventCount;
				}
				
			}
			
			
		}
		return Rest::json($results);
	}
}
