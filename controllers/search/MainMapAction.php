<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search;

use CAction;
class MainMapAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        //$controller->layout = "//layouts/mainSearch";
        return $controller->renderPartial("/default/mainMap");
    }
}