<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\survey;

use Form;
use Rest;

class GetAllFormsContextAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null)
    {
        if (isset($_POST["contextId"]) || !empty($id)) {
            $aapFormOnly = isset($_POST["aapFormOnly"]) ? (bool) $_POST["aapFormOnly"] : false;
            $contextId = isset($_POST["contextId"]) ? $_POST["contextId"] : $id;
            $forms = Form::getAllFormContext($contextId, $aapFormOnly);
            return Rest::json([
                "result" => true,
                "allForms" => $forms
            ]);
        } else {
            return Rest::json(array("result" => false, "msg" => "You must provide a contextId"));
        }
    }
}