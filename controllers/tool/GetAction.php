<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\tool;
use CAction;
use MongoId;
use Rest;

class GetAction extends \PixelHumain\PixelHumain\components\Action {

    public function run($what) {
		
		if($what == "mongoId")
			return Rest::json( array( "id" => (string)new MongoId() ) );
    }
}

?>