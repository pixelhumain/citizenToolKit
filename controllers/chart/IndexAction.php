<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\chart;
use Authorisation;
use CAction;
use Element;
use Yii;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=null, $id=null, $chart=null){

    	$controller=$this->getController();
		$element = Element::getByTypeAndId($type,$id);
		$params["element"] = $element;
		$params["properties"]=$element["properties"]["chart"][$chart];
		$params["parentType"] = $type;
		$params["parentId"] = $id;
		$params["chartKey"] = $chart;
		$params["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $type, $id);
		$params["openEdition"] = Authorisation::isOpenEdition($id, $type, @$element["preferences"]);
        if(Yii::app()->request->isAjaxRequest){
			return $controller->renderPartial("index", $params, true);

		}
    }
}