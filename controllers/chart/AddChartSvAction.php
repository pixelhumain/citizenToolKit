<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\chart;

use CAction, Element, Authorisation, Yii;
class AddChartSvAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=null, $id=null){

    	$controller=$this->getController();
		$element = Element::getByTypeAndId($type,$id);
		$params["element"] = $element;
		$params["properties"]=array();
		if (isset($element["properties"]["chart"])){
			$params["properties"]=$element["properties"]["chart"];
		}

		$params["parentType"] = $type;
		$params["parentId"] = $id;
		$params["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $type, $id);
		$params["openEdition"] = Authorisation::isOpenEdition($id, $type, @$element["preferences"]);

        if(Yii::app()->request->isAjaxRequest){
			return $controller->renderPartial("addChartSV", $params, true);
		}
    }
}