<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\chart;
use CAction;
use Rest;

/**
 * Retrieve all the Countries 
 * @return [json] {value : "codeinsee", text : "the Text"}
 */
class GetJsonAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
	    $name=$_POST["json"];
        //header('Access-Control-Allow-Origin: *');
        $docsJSON = file_get_contents("../../modules/co2/views/chart/json/".$name.".json", FILE_USE_INCLUDE_PATH);
        //echo $docsJSON;
        $docs = json_decode($docsJSON,true);       
        //print_r($docs);
      //  return $docs;
	  	return Rest::json($docs);
    }
}