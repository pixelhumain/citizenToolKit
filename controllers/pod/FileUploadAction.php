<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pod;

use CAction, Yii;
class FileUploadAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($itemId, $type, $resize = false, $edit=false, $contentId, $podId="",$image="")
    {
        $controller=$this->getController();
        $params = array();
		$params["type"] = $type;
		$params["itemId"] = $itemId;
		$params["resize"] = $resize;
		$params["contentId"] = $contentId;
		$params["podId"] = $podId;
		$params["editMode"] = $edit;
		$params["image"] = $image;
		if(Yii::app()->request->isAjaxRequest)
			return $controller->renderPartial('fileupload', $params, true);
		else
			return $controller->render("fileupload");
    }
}