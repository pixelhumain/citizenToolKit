<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pod;

use CAction, ActivityStream, Yii;
class ActivityListAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run( $type=null, $id= null)
    {
	    $controller=$this->getController();
	    $params["activities"]=ActivityStream::activityHistory($id,$type);
		$params["contextType"]=$type;
		$params["contextId"]=$id;
		//Rest::json($params); exit;
		if(Yii::app()->request->isAjaxRequest)
	        return $controller->renderPartial("activityList", $params,true);
	    else
  		return	$controller->render( "activityList" , $params );
    }
    
    
}