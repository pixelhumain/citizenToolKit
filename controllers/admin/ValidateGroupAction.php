<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, PHDB, MongoId, Person, Classified, Rest;
class ValidateGroupAction extends \PixelHumain\PixelHumain\components\Action{
	public function run(){

		$controller = $this->getController();
		
		if( !empty($controller->costum["slug"]) && 
			!empty($_POST["type"]) && 
			!empty($_POST["id"]) && 
			isset($_POST["valid"]) ){
			
			$elt = PHDB::findOneById( $_POST["type"], $_POST["id"], array("preferences", "links"));
			if($_POST["valid"] == "true"){
				if( !empty($elt["preferences"]["toBeValidated"]) ) {
					unset($elt["preferences"]["toBeValidated"][$controller->costum["slug"]]);
					if(count($elt["preferences"]["toBeValidated"]) == 0)
						unset($elt["preferences"]["toBeValidated"]);
				}
			}else{
				if( empty($elt["preferences"]["toBeValidated"]) )
					$elt["preferences"]["toBeValidated"] = array();
				$elt["preferences"]["toBeValidated"][$controller->costum["slug"]] = true ;
				
			}
			$query=array("preferences" => $elt["preferences"]);
			$res= PHDB::update( $_POST["type"], 
								array("_id" => new MongoId($_POST["id"])), 
								array('$set'=>$query));

			$sub = array();
			if(!empty($elt["links"])){
				foreach ($elt["links"] as $kL => $vL) {
					foreach ($vL as $key => $value) {
						if(!empty($value["type"]) && $value["type"] != Person::COLLECTION){
							$where = array(	"_id" => new MongoId($key),
											"source.keys" => array('$in' => array($controller->costum["slug"] ) ) );
							$subElt = PHDB::findOne($value["type"], $where, array("name","preferences", "links"));

							if(!empty($subElt)){
								if($_POST["valid"] == "true"){
									if( !empty($subElt["preferences"]["toBeValidated"]) ) {
										unset($subElt["preferences"]["toBeValidated"][$controller->costum["slug"]]);
										if(count($subElt["preferences"]["toBeValidated"]) == 0)
											unset($subElt["preferences"]["toBeValidated"]);
									}
								}else{
									if( empty($subElt["preferences"]["toBeValidated"]) )
										$subElt["preferences"]["toBeValidated"] = array();
									$subElt["preferences"]["toBeValidated"][$controller->costum["slug"]] = true ;
									
								}

								//$sub[$key] = $subElt;
								$querySub=array("preferences" => $subElt["preferences"]);
								$sub[$key] = PHDB::update( $value["type"], 
													array("_id" => new MongoId($key)), 
													array('$set'=>$querySub) );
							}
							
							

							
						}
					}
				}
			}
			
			$whereCl = array("parent.".$_POST["id"] => array('$exists' => true));
			$classifieds = PHDB::find( Classified::COLLECTION, $whereCl, array("name","preferences", "links"));
			//Rest::json($classifieds);
			if(!empty($elt["links"])){
				foreach ($classifieds as $key => $valC) {
					if($_POST["valid"] == "true"){
						if( !empty($valC["preferences"]["toBeValidated"]) ) {
							unset($valC["preferences"]["toBeValidated"][$controller->costum["slug"]]);
							if(count($valC["preferences"]["toBeValidated"]) == 0)
								unset($valC["preferences"]["toBeValidated"]);
						}
					}else{
						if( empty($valC["preferences"]["toBeValidated"]) )
							$valC["preferences"]["toBeValidated"] = array();
						$valC["preferences"]["toBeValidated"][$controller->costum["slug"]] = true ;
						
					}
					//$sub[$key] = $valC;
					$querySub=array("preferences" => $valC["preferences"]);
					$sub[$key] = PHDB::update( Classified::COLLECTION, 
										array("_id" => new MongoId($key)), 
										array('$set'=>$querySub) );
				}
			}


			return Rest::json(array("result"=>true, "elt" => $elt, "sub" => $sub));
		}
	}
}
?>