<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, MailError, Yii;
/**
 * Display the mail error dashboard
 */
class MailErrorDashboardAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();

      $controller->title = "Admin Mail Error Dashboard - Restricted Zone";
      $controller->subTitle = "Admin Mail Error Dashboard";
      $controller->pageTitle = ucfirst($controller->module->id)." - ".$controller->title;

      /* **************************************
      *  MAIL ERRORS
      ***************************************** */
      $mailErrors = MailError::getMailErrorSince(time() - 60*60*24*7);

      $params["mailErrors"] = $mailErrors;
      $params["path"] = "../admin/";
		  $page = $params["path"]."mailErrorTable";

      if(Yii::app()->request->isAjaxRequest){
        return $controller->renderPartial($page,$params,true);
      }
      else {
       return $controller->render($page,$params);
      }
    }
}
