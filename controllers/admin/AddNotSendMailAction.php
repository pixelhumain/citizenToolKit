<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Role, Yii, ActivityStream, Rest;
class AddNotSendMailAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $params = $_POST ;
            ActivityStream::saveNotSendMail($params["email"]);
            $res["res"] = array(
                                "result" => true, 
                                "type" => "notMail", 
                                "msg" => Yii::t("common","You will no longer receive any emails from the platform") );
    		return Rest::json($res); 
        } else {
            return Rest::json(array("result" => false,
                                "msg" => Yii::t("common","You are not admin") )); 
        }
    }
}

?>