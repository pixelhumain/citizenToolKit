<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
use Authorisation;
use CAction;
use Person;
use Rest;
use Yii;

class DeleteAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($type, $id) {
    	
        $controller=$this->getController();
        
        if ( ! Authorisation::isInterfaceAdmin()) {
            return Rest::json(array( "result" => false, "msg" => "You must be a super admin to delete something" ));
            return;
        }

        if ($type != Person::COLLECTION && $type != Person::CONTROLLER) {
            return Rest::json(array( "result" => false, "msg" => "For now you can only delete Person" ));
            return;   
        }

        $res = Person::deletePerson($id, Yii::app()->session["userId"]);
        return Rest::json($res);
    }
}