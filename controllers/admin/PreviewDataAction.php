<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
use CAction;
use Import;
use Rest;

class PreviewDataAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
        $params = Import::previewData($_POST);
        //$params = Import::setWikiDataID($_POST);
        return Rest::json($params);
    }
}

?>