<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
use CAction;
use Comment;
use News;
use Yii;

class ModerateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run( $id=null )
    {
      $controller = $this->getController();
      //var_dump($_GET);exit;
      $params = array();
      //if(isset($_REQUEST['all'])){

        $page =  "moderateAll";

        $params["news"] = News::getNewsToModerate();
        $params["comments"] =  Comment::getCommentsToModerate();

        //we moderate comments which is part of a news already moderate isAnabuse == true
        if(isset($params["comments"]) && is_array($params["comments"]))foreach($params["comments"] as $key => $val){
          $tmp = News::getById($val['contextId']);
          if(isset($tmp)){
            if(isset($tmp['moderate'])){
              if(isset($tmp['moderate']['isAnAbuse']) && $tmp['moderate']['isAnAbuse'] == true){
                unset($params["comments"][$key]);
              }
            }
          }
        }
      //}
      if(isset($_GET['rest']) && $_GET['rest'] =="/one" ){
        $page =  "moderateOne"; 
      }
      //else{
        //$page =  "moderate";
      //}

      if(Yii::app()->request->isAjaxRequest){
        return $controller->renderPartial($page,$params,true);
      }
      else {
       return $controller->render($page,$params);
      }
    }
}

?>