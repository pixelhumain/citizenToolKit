<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Authorisation, Person, Rest;
class SwitchtoAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($uid)
    {
    	
        $controller=$this->getController();
        $res = array( "result" => false );
        if(Authorisation::isInterfaceAdmin()){
            Person::clearUserSessionData();
            $person = Person::getById($uid,false);
            
            if( $person ){
                Person::saveUserSessionData($person);
                $res['result'] = true;
                $res['id']=(string)$person["_id"];
            }
        } 
        return Rest::json($res);
    }
}