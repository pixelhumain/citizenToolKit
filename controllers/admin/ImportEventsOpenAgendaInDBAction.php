<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Event, CTKException, MongoDate, PHDB, MongoId, Rest;
class ImportEventsOpenAgendaInDBAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();
        $params = array();
        if(!empty($_POST["jsonEventsAdd"])){
        	foreach (json_decode($_POST["jsonEventsAdd"], true) as $key => $eventOpenAgenda) {
        		try{
                    $event = Event::createEventsFromOpenAgenda($eventOpenAgenda);
                    $resEvent = Event::saveEventFromOpenAgenda($event, $controller->moduleId) ;
                    $eventGood["name"] = $resEvent["event"]["name"];
                    $eventGood["msg"] = $resEvent["msg"];
                    $params["result"][] = $eventGood;
                }
                catch (CTKException $e){
                    $eventError["name"] = $eventOpenAgenda["title"];
                    $eventError["msg"] = $e->getMessage();
                    $params["error"][] = $eventError;
                }

        	}
        }

        if(!empty($_POST["jsonEventsUpdate"])){
            foreach (json_decode($_POST["jsonEventsUpdate"], true) as $key => $eventOpenAgenda) {
                try{
                    $event = Event::getEventsOpenAgenda($eventOpenAgenda["uid"]);
                    $eventOpenAgenda = Event::createEventsFromOpenAgenda($eventOpenAgenda);
                    foreach ($event as $key => $value) {
                        $event["modified"] = new MongoDate(time());
                        $event['updated'] = time();
                        PHDB::update( Event::COLLECTION, array("_id" => new MongoId($value["_id"])), 
                                                  array('$set' => $event));
                    }
                    $eventGood["name"] = $eventOpenAgenda["name"];
                    $eventGood["msg"] = "L'événement a été mis a jours";
                    $params["result"][] = $eventGood;
                    
                }
                catch (CTKException $e){
                    $eventError["name"] = $eventOpenAgenda["name"];
                    $eventError["msg"] = $e->getMessage();
                    $params["error"][] = $eventError;
                }
                
            }
        }    
    	return Rest::json($params);   
    }
}

?>