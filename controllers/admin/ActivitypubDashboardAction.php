<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
class ActivitypubDashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {

        return $this->getController()->renderPartial("activitypubdashboard", array());
    }
}