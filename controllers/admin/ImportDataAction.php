<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
use CAction;
use Import;
use Yii;

class ImportDataAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();
    	$params = Import::importData($_FILES, $_POST);

        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("importData",$params,true);
        else 
           return $controller->render("importData",$params);
    }
}

?>