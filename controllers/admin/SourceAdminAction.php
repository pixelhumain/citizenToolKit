<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Person, Yii, Element;
class SourceAdminAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller = $this->getController();
        $params = array();

        $sourceAdmin = Person::getSourceAdmin(Yii::app()->session["userId"]);
        $result = array();

        if(!empty($sourceAdmin)){
            foreach ($sourceAdmin as $key => $value) {
                $result[$value] = Element::getAllEntitiesByKey($value);
            }
        }
        
        $params["entitiesSourceAdmin"] = $result;
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("sourceadmin",$params,true);
        else 
           return $controller->render("sourceadmin",$params);
    }
}

?>