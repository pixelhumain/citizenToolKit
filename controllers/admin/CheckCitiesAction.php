<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Yii;
class CheckCitiesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();

        
        //$params['cities'] = json_encode(City::getCitiesForcheck());
        $params['cities'] = array();
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("checkcities",$params,true);
        else 
           return $controller->render("checkcities",$params);
    }
}

?>