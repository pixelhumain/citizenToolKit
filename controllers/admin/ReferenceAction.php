<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, PHDB, Yii;
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class ReferenceAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($source=null)
    {

      $controller = $this->getController();
      $params=array(
        "typeDirectory"=>$_POST["initType"],
        "originCross"=>"network"
      );
      if(isset($controller->costum)) {
        $params["originCross"]="costum";
        $sourceKey=(isset($controller->costum["isTemplate"]) && isset($controller->costum["contextSlug"]) && $controller->costum["isTemplate"]) ? $controller->costum["contextSlug"] : $controller->costum["slug"];
      }
          $tmpSourceKey = array();
          if($sourceKey != null && $sourceKey != ""){
            //Several Sourcekey
            if(is_array($sourceKey)){
              foreach ($sourceKey as $value) {
                $tmpSourceKey[] = $value;
              }
            }//One Sourcekey
            else{
              $tmpSourceKey[] = $sourceKey;
            }
            $countRef=0;
            $countSource=0;
            $origin=(@$controller->costum) ? "costum" : "network";
            foreach($_POST["initType"] as $data){
               $countRef+=PHDB::count( $data , array("reference.".$origin => array('$in' => $tmpSourceKey)));
            }
            $params["countMenu"]["reference"] =$countRef;
            foreach($_POST["initType"] as $data){
              $countSource += PHDB::count( $data , array("source.keys" => array('$in' => $tmpSourceKey)));
            }
            $params["countMenu"]["source"] = $countSource;
            $params["sourceKey"]=$tmpSourceKey;
          }
      
		  $page = $_POST["page"] ?? "referenceTable";
        $params["filters"] =$_POST["filters"] ;
        if(Yii::app()->request->isAjaxRequest){
          return $controller->renderPartial($page,$params,true);
        }
        else {
         return $controller->render($page,$params);
        }
    }
}
