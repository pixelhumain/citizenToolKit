<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Yii;
class CircuitsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($dir=null) {
        $controller = $this->getController();
		$params=array("dir"=>$dir);
    	//$params = City::getAllCities();
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("terla/circuits",$params,true);
        else 
           return $controller->render("terla/circuits",$params);
    }
}

?>