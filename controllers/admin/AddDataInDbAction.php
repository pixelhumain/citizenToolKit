<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
use CAction;
use Import;
use Rest;
use Yii;

class AddDataInDbAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	
    	$controller=$this->getController();
    	$result = Import::addDataInDb($_POST);
    	
    	return Rest::json( $result );
    }
}