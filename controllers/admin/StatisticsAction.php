<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
use CAction;
use CO2Stat;
use Rest;
use Yii;

class StatisticsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($action="", $week="") {
        $controller = $this->getController();
    	//$params = array();
        $res=CO2Stat::getStatsByHash(@$week);

        //return Rest::json($res);
    
    	//$city = SIG::getInseeByLatLngCp("48.380317", "-4.5084217", "29200");
    	//$params["city"] = json_encode($city) ;

        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("../app/info/CO2/stats",array("week"=>$week),true);
        else 
           return $controller->render("../app/info/CO2/stats",array("week"=>$week));
    }
}

?>