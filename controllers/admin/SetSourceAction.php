<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, DataValidator, Yii, CTKException, Rest;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;

class SetSourceAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($action=null, $set=null){
        $controller = $this->getController();
        $params = array();
        $params=$_POST;
        if(DataValidator::missingParamsController($_POST, ["id", "type","sourceKey"]))
            return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));
        $res = array("result"=>false, "msg"=>Yii::t("common", "Something went wrong!"));
        try {
            if(!empty($action)){
                $sourceKey = $_POST['sourceKey'];
                if(isset(Yii::app()->session["costum"]) 
                  && isset(Yii::app()->session["costum"]["contextSlug"]))
                      $sourceKey=Yii::app()->session["costum"]["contextSlug"];

                if($action=="add")
                    $res = Admin::addSourceInElement($_POST["id"], $_POST["type"], $sourceKey, $set);
                if($action=="remove")
                    $res = Admin::removeSourceFromElement($_POST["id"], $_POST["type"], $sourceKey, $set);
            }
        } catch (CTKException $e) {
            $res = array("result"=>false, "msg"=>$e->getMessage());
        }
        return Rest::json($res);
    }
}

?>