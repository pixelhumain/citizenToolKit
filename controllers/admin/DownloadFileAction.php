<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction;
class DownloadFileAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=import.zip');
        header('Content-Length: ' . filesize(sys_get_temp_dir()."/import.zip"));
        readfile(sys_get_temp_dir()."/import.zip", true);
    }
}

?>