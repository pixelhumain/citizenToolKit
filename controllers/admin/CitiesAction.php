<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, City, Yii;
class CitiesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller = $this->getController();

    	$params = City::getAllCities();
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("cities",$params,true);
        else 
           return $controller->render("cities",$params);
    }
}

?>