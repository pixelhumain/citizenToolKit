<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Authorisation, Yii, Person, Rest;
/**
* Activate a user throw admin back office
*/
class ActivateUserAction extends \PixelHumain\PixelHumain\components\Action {

	public function run($user) {
		$controller=$this->getController();
		
		if (! Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) {
			$res = array("result" => false, "msg" => "You must be logged as an admin user to do this action !");
		} else {
			$res = Person::validateUser($user,true);
		}
		return Rest::json($res);
	}
}
