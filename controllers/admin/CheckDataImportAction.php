<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Import, Rest, Yii;
class CheckDataImportAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$array = Import::createArrayList($_POST["list"]);
    	return Rest::json( $array );
    }
}