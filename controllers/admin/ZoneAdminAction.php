<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use PHDB;
use PixelHumain\PixelHumain\components\Action;
use Yii;
use Zone;

class ZoneAdminAction extends Action{
    public function run(){
        $controller = $this->getController();
        $zones = PHDB::findAndSort(Zone::COLLECTION,["level" => "1"], ['name' => 1] ,0,["name", "translateId", "countryCode"]);
        // foreach ($zones as $key => $value) {
        //     $translate = PHDB::findOneById(Zone::TRANSLATE, $value["translateId"]);
        //     if(!empty($translate["translates"][strtoupper(Yii::app()->language)]))
        //         $zones[$key]["name"] = $translate["translates"][strtoupper(Yii::app()->language)] ;
        // }
        return $controller->renderPartial("co2.views.admin.zone",["pays" => $zones],true);
    }
}