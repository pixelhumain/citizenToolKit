<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, PHDB, ActivityStream, ActStr, DateTime, Yii;
class NotSendMailAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();
        $r = PHDB::find(ActivityStream::COLLECTION, array("verb" => ActStr::VERB_NOSENDING));
        foreach ($r as $key => $value) {
        	$r[$key]["date"] = date(DateTime::ISO8601, $value["created"]->sec);
        }

        $params = array("results" => $r);
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("notSendMail",$params,true);
        else 
           return $controller->render("notSendMail",$params);
    }
}

?>