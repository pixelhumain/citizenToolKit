<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, CacheHelper;
class MailingAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
        $costum = CacheHelper::getCostum();
        $isMailingActive = false;
	    if(isset($costum["htmlConstruct"]["adminPanel"]["menu"]["community"]["mailing"]) && $costum["htmlConstruct"]["adminPanel"]["menu"]["community"]["mailing"]==true){
            $isMailingActive = true;
        }
        if($isMailingActive){
            return $this->getController()->renderPartial("communityMailing", array("type"=>$type,"id"=>$id));
        }else {
            return "";
        }
    }
}