<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Yii;
class OpenAgendaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();
    	$params = array();

        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("openAgenda",$params,true);
        else 
           return $controller->render("openAgenda",$params);
    }
}

?>