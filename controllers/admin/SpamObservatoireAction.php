<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
use CAction;
use Comment;
use News;
use Yii, Authorisation, Element, PHDB,MongoId, IpSpam;

class SpamObservatoireAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller = $this->getController();
		$params = array();
		
		if(Authorisation::isInterfaceAdmin() && Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])){
            $this->setLocalisationOfIpSpamWithoutLoc();

            $allAggregate= array(
                array( '$group' => array(
                        '_id' => array(
                            'ip' => '$ip'
                            , 'city' => '$city'
                            , 'region' => '$region'
                            , 'country' => '$country'
                            , 'loc' => '$loc'
                        ),
                        'count' => array ('$sum' => 1),
                    )
                ),
                array( '$project' => array("ip" => '$_id.ip', "count" => '$count', "city" => '$_id.city', "region" => '$_id.region', "country" => '$_id.country', "loc" => '$_id.loc') ),
            );
            
            $villeAggregate= array(
                array( '$group' => array(
                        '_id' => array(
                             'city' => '$city'
                        ),
                        'count' => array ('$sum' => 1),
                    )
                ),
                array( '$project' => array("count" => '$count', "city" => '$_id.city') ),
            );
            
            $paysAggregate= array(
                array( '$group' => array(
                        '_id' => array(
                             'country' => '$country'
                        ),
                        'count' => array ('$sum' => 1),
                    )
                ),
                array( '$project' => array("count" => '$count', "country" => '$_id.country') ),
            );
            
            $regionAggregate= array(
                array( '$group' => array(
                        '_id' => array(
                             'region' => '$region'
                        ),
                        'count' => array ('$sum' => 1),
                    )
                ),
                array( '$project' => array("count" => '$count', "region" => '$_id.region') ),
            );
            
            $ipSpams = IpSpam::getAggregate($allAggregate);
            $countrys = IpSpam::getAggregate($paysAggregate);
            $citys = IpSpam::getAggregate($villeAggregate);
            $regions = IpSpam::getAggregate($regionAggregate);
            
            if(isset($ipSpams["result"]))
                $ipSpams = $ipSpams["result"];

            $params = array(
                "ipSpams" => $ipSpams,
                "countrys" => $countrys,
                "citys" => $citys,
                "regions" => $regions,
            );

			return $controller->renderPartial("co2.views.admin.spamObservatoire",$params,true);
		}
        else
        	return $controller->renderPartial("co2.views.error.error",array("error"=>array("code"=>"303", "message"=>Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system"))),true);
    }

    private function setLocalisationOfIpSpamWithoutLoc() {
        $ipSpams = IpSpam::getAll();

        if(isset($ipSpams["result"])){
            foreach($ipSpams["result"] as $ipSpam) {
                if(isset($ipSpam["ip"]) && (!isset($ipSpam["region"]) || !isset($ipSpam["city"]) || !isset($ipSpam["country"]) || !isset($ipSpam["loc"]))){
                    
                    $data = IpSpam::getInfoOfIp($ipSpam["ip"]);
        
                    if($data !== null && isset($data["ip"])){
                        $where = array("ip" => $ipSpam["ip"] );
                        $action = array(
                            '$set' => $data
                        );

                        IpSpam::updateByWhere($where, $action);
                    }
                }
            }
        }
    }
}

?>