<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;
use Authorisation;
use CAction;
use Costum;
use type;
use Yii;

/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 * @return type
 */
class DirectoryAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run( $tpl=null, $view=null, $id=null,$type=null ){
		$controller = $this->getController();
		$params = array();
        if(isset($_POST["page"])){
            $page = $_POST["page"];
            unset($_POST["page"]);
        }else{
            $page = "co2.views.admin.directory";
        }
        array_push($_POST["paramsFilter"]["defaults"]["fields"], "profilImageUrl");
		$params["panelAdmin"] = $_POST;
		$res=false;
		if(!empty($id))
			$params["context"]=array("id"=>$id, "type"=>$type);
		// check Interface Admin (costum, superAdmin)
		if(Authorisation::isInterfaceAdmin()){
			$res=true;
		}
		// Si context on regarde si l'utilisateur est administrateur (@isElementAdmin fait partie des fonctions customisables)
		else if(!empty($id) && !empty($type) && Authorisation::isElementAdmin( $id, $type,Yii::app()->session["userId"] )){
			$res=true;
		}
		// On peut mettre un process complétement costumisé
		else if(Costum::isSameFunction("authorizedPanelAdmin")) {
			$opt=(!empty($id) && !empty($type)) ? array("id"=>$id, "type"=>$type) : [];
			if(Costum::sameFunction("authorizedPanelAdmin", $opt))
            	$res=true;
        }
        if($res)
        	return $controller->renderPartial($page,$params,true);
        else
        	return $controller->renderPartial("co2.views.error.error",array("error"=>array("code"=>"303", "message"=>Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system"))),true);

			
		
		
	}
}
