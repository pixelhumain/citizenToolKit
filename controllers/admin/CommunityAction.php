<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Person, Yii, Element;
use Link;

class CommunityAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
        $ctrl = $this->getController();
    	$this->getController()->layout = "//layouts/empty";

       // $form = PHDB::findOne( Form::COLLECTION , array("id"=>$id));

    	if ( ! Person::logguedAndValid() ) {
            return $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"));
        }else{
            
            $community=Element::getCommunityByTypeAndId($type, $id, Link::ALL, Link::ALL_COMMUNITY);
            //var_dump($community);exit;
            foreach ($community as $key => $value) {
                $elt=Element::getElementSimpleById($key, $value["type"], null, array("name", "profilThumbUrl", "tags","address", "email", "links"));
                if(!empty($elt))
                    $community[$key]=array_merge($community[$key], $elt);
                else 
                    unset($community[$key]);
            }

			return $this->getController()->renderPartial("community", array(
                "type"=>$type, 
                "id"=>$id, 
                "connectTo"=> Link::$linksTypes[$type][Person::COLLECTION], 
                "results" => $community ));
		} 
    }
}