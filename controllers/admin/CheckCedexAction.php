<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\admin;

use CAction, Import, Rest;
class CheckCedexAction extends \PixelHumain\PixelHumain\components\Action{
    public function run()
    {
        $controller = $this->getController();
        $params = Import::checkCedex($_POST);
        return Rest::json($params);
    }
}

?>