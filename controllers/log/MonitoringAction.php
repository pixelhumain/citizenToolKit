<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\log;

use Log;
/**
*
*/
class MonitoringAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run( $id=null )
    {
		$controller = $this->getController();
		$summary = Log::getSummaryByAction();
		$actionsToLog = Log::getActionsToLog();
		return $controller->renderPartial('monitoring', array("summary" => $summary, "actionsToLog" => $actionsToLog));
    }
}