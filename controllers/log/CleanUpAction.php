<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\log;

use CAction;
/**
* upon Registration a email is send to the new user's email 
* he must click it to activate his account
* This is cleared by removing the tobeactivated field in the pixelactifs collection
*/
class CleanUpAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$retour = date('Y-m-d H:i:s')." - Démarrage du script pour nettoyer les logs<br/>";
    	Log::cleanUp();
    	$retour .= date('Y-m-d H:i:s')." - Fin du script pour nettoyer les logs<br/>";
		return $retour;
    }
}
