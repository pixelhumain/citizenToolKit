<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use CAction, CO2Stat, Person, Event, Project, Organization, Poi, Classified, Badge, Element, News, Form, Answer, Proposal, Action, Yii, Preference, Authorisation, PHDB;
use Crowdfunding;
use Endorsement;
use Link;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use yii\helpers\VarDumper;
use Rest;

class PageAction extends \PixelHumain\PixelHumain\components\Action
{
    /**
     * Dashboard Organization
     */
    public function run($type, $id, $view = null, $mode = null, $dir = null, $key = null, $folder = null, $json = null)
    {
        if (!(isset($_POST["costumSlug"]) && $_POST["costumSlug"] == "notragora")) {
            if ($type === 'badges') {
                return  $this->badges($id, $json);
            }
        }
        $controller = $this->getController();
        $this->getController()->layout = "//layouts/mainSearch";

        CO2Stat::incNbLoad("co2-page");

        if (in_array($type, [Person::COLLECTION, Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION, Poi::COLLECTION, Classified::COLLECTION, Badge::COLLECTION,templates])) {
            $element = Element::getByTypeAndId($type, $id);
        } else if ($type == News::COLLECTION)
            $element = News::getById($id);
        else if ($type == Form::COLLECTION) {
            $element = Form::getById($id);
            !empty($element) ? $element["collection"] = Form::COLLECTION : "";
        } else if ($type == Answer::COLLECTION)
            $element = Answer::getById($id);
        else if ($type == Crowdfunding::COLLECTION)
            $element = Crowdfunding::getCampaignById($id);
        if (@$element["fromActivityPub"] && $element["fromActivityPub"] == true) {
            $activities = Utils::activitypubToElement($element["objectId"]);
            foreach ($activities as $activityKey => $activityValue) {
                if ($activityKey != 'parent') {
                    $element[$activityKey] = $activityValue;
                }
            }
        }
        if(empty($element) && $type == Form::COLLECTION) {
            $element = Element::getByTypeAndId($type, $id);
            if(!empty($element) && isset($element["type"]) && $element["type"] == "aap") {
                if(isset($element["config"]))
                    $formConfig = Element::getElementById($element["config"], Form::COLLECTION, null, [ "name", "slug", "type", "parent", "endDate", "startDate", "email", "type"]);
                if(isset($formConfig) && isset($formConfig["parent"]) && is_array($formConfig["parent"])) {
                    $parentConfigId = array_key_first($formConfig["parent"]);
				    $parentConfigElem = $formConfig["parent"][$parentConfigId] ?? [];
                    if(isset($parentConfigElem["type"])) {
                        $parentConfigElem = Element::getElementById($parentConfigId, $parentConfigElem["type"], null, [ "name", "slug", "type", "endDate", "startDate", "email", "type"]);
                        isset($parentConfigElem["slug"]) ? $element["parentConfigSlug"] = $parentConfigElem["slug"] : "";
                    }
                }
                if(isset($element["parent"]) && is_array($element["parent"]) && !empty($element["parent"])) {
                    $parentId = array_key_first($element["parent"]);
				    $parentElem = $element["parent"][$parentId] ?? [];
                    if(isset($parentElem["type"])) {
                        $parentEl = Element::getElementById($parentId, $parentElem["type"], null, [ "name", "slug", "shortDescription", "type", "profilThumbImageUrl", "profilImageUrl", "profilMediumImageUrl", "endDate", "startDate", "email"]);
                        if(!empty($parentEl)) {
                            $element["parentSlug"] = $parentEl["slug"] ?? "notDefined";
                            $element["shortDescription"] = $parentEl["shortDescription"] ?? null; 
                            if(!isset($element["profilImageUrl"]) && isset($parentEl["profilImageUrl"]))
                                $element["profilImageUrl"] = $parentEl["profilImageUrl"];
                            if(!isset($element["profilMediumImageUrl"]) && isset($parentEl["profilMediumImageUrl"]))
                                $element["profilMediumImageUrl"] = $parentEl["profilMediumImageUrl"];
                            if(!isset($element["profilThumbImageUrl"]) && isset($parentEl["profilThumbImageUrl"]))
                                $element["profilThumbImageUrl"] = $parentEl["profilThumbImageUrl"];
                        }
                    }
                }
                $element["collection"] = Form::COLLECTION;
            }
        }
        //var_dump($element);exit;

        if (isset($controller)) {
            $shortDesc =  (isset($element["shortDescription"])) ? $element["shortDescription"] : "";
            if (empty($shortDesc))
                $shortDesc = (isset($element["description"])) ? substr($element["description"], 0, 140) . "..." : "";
            $controller->module->description = $shortDesc;
            $controller->module->pageTitle = (isset($element["name"])) ? $element["name"] : "";
            $controller->module->author = "";
            $controller->module->keywords = (@$element["tags"]) ? implode(",", @$element["tags"]) : "";
            if (@$element["profilImageUrl"]) {
                $controller->module->image = $element["profilImageUrl"];
            }
            $controller->module->relCanonical = Yii::app()->createUrl("/co2/app/page/type/" . $type . "/id/" . $id);
            if (strstr($_SERVER['REQUEST_URI'], "app/page")) {
                // referer not from the same domain
                $controller->module->share = Yii::app()->createUrl("#page.type." . $type . ".id." . $id);
                if (!Yii::app()->request->isAjaxRequest) {
                    $controller->layout = "//layouts/meta";
                    return $controller->render("welcome", array());
                }
            }
        }
        //element deleted 
        if ((!empty($element["status"]) && $element["status"] == "deleted") ||
            (!empty($element["tobeactivated"]) && $element["tobeactivated"] == true)
        ) {
            return $controller->redirect(Yii::app()->createUrl($controller->module->id));
        }

        //visibility authoraizations
        if (
            !Preference::isPublicElement(@$element["preferences"]) &&
            !Authorisation::canSeePrivateElement(@$element["links"], $type, $id, @$element["creator"], @$element["parentType"], @$element["parentId"])
        ) {
            $url = "/#";
            if (isset($_GET["network"]))
                $url = "/network/default/index?src=" . $_GET["network"];
            if (isset($controller->costum) && !empty($controller->costum))
                $url = $controller->costum["url"];
            $url = Yii::app()->createUrl($url);

            if (isset($controller->costum) && isset($controller->costum["host"]))
                return $controller->redirect(Yii::app()->createAbsoluteUrl(""));
            else
                return $controller->redirect(Yii::app()->createUrl($url));
        }

        /*if(@$element["parentId"] && @$element["parentType"] && 
            $element["parentId"] != "dontKnow" && $element["parentType"] != "dontKnow" && !@$element["parent"]){
            $element['parent'][$element["parentId"]] = Element::getElementById( $element["parentId"], $element["parentType"], null, array("name", "slug","profilThumbImageUrl"));
            $element['parent'][$element["parentId"]]["type"] = $element["parentType"];
        }*/

        if (@$element["parent"] && !empty($element["parent"]) && !@$element["parent"]["name"]) {
            foreach ($element["parent"] as $k => $v) {
                $elt = Element::getElementById($k, $v["type"], null, array("name", "slug", "profilThumbImageUrl"));
                if (!empty($elt))
                    $element['parent'][$k] = array_merge($element['parent'][$k], $elt);
            }
        }

        /* if(@$element["organizerId"] && @$element["organizerType"] && 
            $element["organizerId"] != "dontKnow" && $element["organizerType"] != "dontKnow"){

            $element['organizer'][$element["organizerId"]] = Element::getElementById( $element["organizerId"], $element["organizerType"], null, array("name", "slug","profilThumbImageUrl"));
            $element['organizer'][$element["organizerId"]]["type"] = $element["organizerType"];
        }*/

        if (@$element["organizer"] && !empty($element["organizer"])) {
            foreach ($element["organizer"] as $k => $v) {
                $elt = Element::getElementById($k, $v["type"], null, array("name", "slug", "profilThumbImageUrl"));
                if (!empty($elt))
                    $element['organizer'][$k] = array_merge($element['organizer'][$k], $elt);
            }
        }

        $params = array(
            "id" => @$id,
            "type" => @$type,
            "view" => @$view,
            "dir" => @$dir,
            "key" => @$key,
            "folder" => @$folder,
            "subdomain" => "page",
            "mainTitle" => "Page perso",
            "placeholderMainSearch" => "",
            "element" => @$element,
            "canEdit" => false,
            "canParticipate" => false,
            "canSee" => true
        );

        if (Authorisation::canEditItem(Yii::app()->session["userId"], $type, $id) || Authorisation::userOwner(Yii::app()->session["userId"], $type, $id)) {
            $params["canEdit"] = true;
            $params["canParticipate"] = true;
            $params["canSee"] = true;
        } else if (Authorisation::canParticipate(Yii::app()->session["userId"], $type, $id)) {
            $params["canParticipate"] = true;
            $params["canSee"] = true;
        } else if (Authorisation::canSee($type, $id)) {
            $params["canSee"] = true;
        }
        $params = Element::getInfoDetail($params, $element, $type, $id);

        $params["pageConfig"] = (isset($controller->appConfig["element"])) ? $controller->appConfig["element"] : null;
        $params["addConfig"] = (isset($controller->appConfig["add"])) ? $controller->appConfig["add"] : null;


        //bloque l'Ã©dition de la page (mÃªme si on est l'admin)
        //visualisation utilisateur
        if (@$mode == "noedit") {
            $params["edit"] = false;
        }

        if (@$_POST["preview"] == true) {

            $params["preview"] = $_POST["preview"];
            $typePreview = (in_array($type, [Organization::COLLECTION, Person::COLLECTION, Project::COLLECTION, Event::COLLECTION])) ? "element" : $type;
            if (
                isset($controller->costum["htmlConstruct"])
                && isset($controller->costum["htmlConstruct"]["preview"])
                && isset($controller->costum["htmlConstruct"]["preview"][$typePreview])
            ) {
                return $controller->renderPartial($controller->costum["htmlConstruct"]["preview"][$typePreview], $params);
            } else {
                if ($type == "classifieds") return $controller->renderPartial('eco.views.co.preview', $params);
                else if ($type == "poi") return $controller->renderPartial('../poi/preview', $params);
                else if ($type == "crowdfunding") return $controller->renderPartial('../crowdfunding/preview', $params);
                else if ($type == Answer::COLLECTION) {
                    return $controller->renderPartial('survey.views.tpls.answers.preview', $params);
                } else return $controller->renderPartial("../element/preview", $params);
            }
        } else {
            $params["element"]["rolesLists"] = [];
            if (!isset($params["element"]["counts"])) $params["element"]["counts"] = [];
            $params["element"]["counts"]["admin"] = 0;
            if (in_array($type, [Person::COLLECTION, Project::COLLECTION, Organization::COLLECTION, Event::COLLECTION, Proposal::COLLECTION, Action::COLLECTION])) {
                $params["element"]["counts"][Link::$linksTypes[$type][Person::COLLECTION] . "Active"] = 0;

                if (isset($params["element"]["links"][Link::$linksTypes[$type][Person::COLLECTION]])) {
                    foreach ($params["element"]["links"][Link::$linksTypes[$type][Person::COLLECTION]] as $e => $v) {
                        if (isset($v["roles"])) {
                            foreach ($v["roles"] as $role) {
                                if (!empty($role)) {
                                    if($params["canEdit"]){
                                        if (!isset($params["element"]["rolesLists"][$role])) {
                                            $params["element"]["rolesLists"][$role] = array("count" => 1, "label" => $role);
                                        } else
                                            $params["element"]["rolesLists"][$role]["count"]++;
                                    }else{
                                        if(!isset($v[Link::IS_INVITING]) && !isset($v[Link::TO_BE_VALIDATED]) && !isset($v[Link::IS_ADMIN_PENDING])){
                                            if (!isset($params["element"]["rolesLists"][$role])) {
                                                $params["element"]["rolesLists"][$role] = array("count" => 1, "label" => $role);
                                            } else
                                                $params["element"]["rolesLists"][$role]["count"]++;
                                        }
                                    }
                                        
                                }
                            }
                            
                        }
                        if (isset($v[Link::IS_INVITING])) {
                            if (isset($params["element"]["counts"][Link::IS_INVITING]))
                                $params["element"]["counts"][Link::IS_INVITING]++;
                            else
                                $params["element"]["counts"][Link::IS_INVITING] = 1;
                        } else if (isset($v[Link::TO_BE_VALIDATED]) || isset($v[Link::IS_ADMIN_PENDING])) {
                            if (isset($params["element"]["counts"]["toBeValidated"]))
                                $params["element"]["counts"]["toBeValidated"]++;
                            else
                                $params["element"]["counts"]["toBeValidated"] = 1;
                        } else {
                            $params["element"]["counts"][Link::$linksTypes[$type][Person::COLLECTION] . "Active"]++;
                            if (isset($v["isAdmin"])) {
                                $params["element"]["counts"]["admin"]++;
                            }
                        }
                    }
                }
                if(!$params["canParticipate"]){
                    $params["element"]["counts"][Link::$linksTypes[$type][Person::COLLECTION]] = $params["element"]["counts"][Link::$linksTypes[$type][Person::COLLECTION] . "Active"];
                }
            }
            if (in_array($type, [Person::COLLECTION, Project::COLLECTION, Organization::COLLECTION])) {
                $params["element"]["counts"]["poi"] = PHDB::count(Poi::COLLECTION, array("parent." . $id => array('$exists' => true)));
                // start - counter for crowdfunding campaign - specific condition if element is a direct parent of the campaign of if the parent of projects which own a campaign
                $whereCrowd = array("parent." . $id => array('$exists' => true), "type" => Crowdfunding::TYPE_CAMPAIGN);
                $collectionCrowd = Crowdfunding::COLLECTION;
                if (isset($params["element"]["links"][Link::$linksTypes[$type][Project::COLLECTION]])) {
                    $whereCrowd = array("parent." . $id => array('$exists' => true), '$or' => array(array("preferences.crowdfunding" => true), array("preferences.crowdfunding" => "true")));
                    $collectionCrowd = Project::COLLECTION;
                }
                $params["element"]["counts"]["crowdfunding"] = PHDB::count($collectionCrowd, $whereCrowd);
                // End - counter for crowdfunding campaign - 
                $params["element"]["counts"]["jobs"] = PHDB::count(Classified::COLLECTION, array("parent." . $id => array('$exists' => true), "type" => Classified::TYPE_JOBS));
                $params["element"]["counts"]["ressources"] = PHDB::count(Classified::COLLECTION, array("parent." . $id => array('$exists' => true), "type" => Classified::TYPE_RESSOURCES));
                $params["element"]["counts"]["classifieds"] = PHDB::count(Classified::COLLECTION, array("parent." . $id => array('$exists' => true), "type" => Classified::COLLECTION));
            }

            
            self::injectBadgesParams($params);
            if (Yii::app()->request->isAjaxRequest)
                return $controller->renderPartial("page", $params, true);
            else
                return $controller->render("page", $params);
        }
    }

    private static function injectBadgesParams(&$params)
    {
        if (!empty($params["element"]["badges"])) {
            $badges = $params["element"]["badges"];
            function sortByOrder($a, $b)
            {
                return (isset($a["order"]) ? $a["order"] : 99999) < (isset($b["order"]) ? $b["order"] : 99999) ? -1 : 1;
            }
            uasort($badges, "sortByOrder");
            $badgeToShow = [];
            foreach ($badges as $idBadge => $badgeValue) {
                if (!isset($badgeValue["attenteEmetteur"]) && !isset($badgeValue["attenteRecepteur"]) && isset($badgeValue["showBanner"]) && $badgeValue["showBanner"] == "true") {
                    $badgeToShow[$idBadge] = $badgeValue;
                }
            }
            $badgesImages = PHDB::findByIds(Badge::COLLECTION, array_keys($badgeToShow), ['profilMediumImageUrl']);
            foreach ($badgesImages as $idBadge => $badgeValue) {
                $badgeToShow[$idBadge]["image"] = $badgeValue["profilMediumImageUrl"] ?? '';
            }
            $params["badgesToShow"] = $badgeToShow;
        }
    }

    private function badges($id, $json = null)
    {
        $controller = $this->getController();
        $this->getController()->layout = "//layouts/mainSearch";
        $element = Element::getByTypeAndId(Badge::COLLECTION, $id);
        $element['creator'] = Element::getByTypeAndId(Person::COLLECTION, $element['creator']);
        $endorsements = Endorsement::getEndorsementByElement(Badge::COLLECTION, $id);
        $params = ['element' => $element, "endorsements" => $endorsements];

        if ($json == true) {
            return Rest::json($params);
        }
        if (Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("badge", $params, true);
        else
            return $controller->render("badge", $params);
    }
}
