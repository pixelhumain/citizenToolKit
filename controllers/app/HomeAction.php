<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;
use CAction;
use CO2Stat;
use Person;
use Preference;
use Yii;
use yii\helpers\Url;

class HomeAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run() { 
    	$controller=Yii::app()->getController();
        $controller->layout = "//layouts/mainSearch";
		CO2Stat::incNbLoad("co2-home");
    	if( !isset(Yii::app()->session["userId"])){
        	if(Yii::app()->request->isAjaxRequest)
               return $controller->renderPartial("welcomeAjax", array(), true);
            else
                return $controller->render( "welcome" , array());
        }
    	else {
            $channel=array("live"=>Preference::getAppConfig("live", Person::COLLECTION, Yii::app()->session["userId"], array(
                    "name"=>Yii::t("common", "My network"),
                    "url"=> "/news/co/index/type/citoyens/id/".Yii::app()->session["userId"]."/isLive/true",
                    "description"=>"All activities of my network. I see all posts form my community, friends, projects, organizations and enities I follow",
                    "edit"=>false,
                    "active" => true,
                    "order"=> 1
                )
                )
            );
            if(Yii::app()->request->isAjaxRequest)
        	   return $controller->renderPartial("home",$channel, true);
   		    else
			    return $controller->render( "home" , $channel);
        }
    }
}
