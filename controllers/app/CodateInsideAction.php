<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use Form;
use MongoId;
use Person;
use PHDB;
use Rest;

class CodateInsideAction extends \PixelHumain\PixelHumain\components\Action {

    public function run() {
        $request = $_POST;
        $forms = array();
        if(isset($request['contextId'])) {
            // $forms = PHDB::find(Form::COLLECTION, ['parent.'.$request['contextId'] => ['$exists' => true], 'active' => ['$ne' => false], 'subType' => ['$exists' => false], 'subForms' => ['$ne' => []], 'params' => ['$exists' => true], 'inArchive' => ['$ne' => true]], ['subForms', 'params', 'name', 'id', 'endDate']);
            $forms = PHDB::find(Form::COLLECTION, ['parent.'.$request['contextId'] => ['$exists' => true], 'active' => ['$ne' => false], 'subType' => ['$exists' => false], 'subForms' => ['$ne' => []], 'inArchive' => ['$ne' => true]], ['subForms', 'params', 'name', 'id', 'endDate']);
            foreach ($forms as $key => $value) {
                if(isset($value["endDate"]) && isset($value["params"]) && time() < strtotime(str_replace("/","-",$value["endDate"]))) {
                    $subForms = array();
                    $arrSub = array();
                    $answers = array();
                    $input = preg_quote('sondageDate', '~');
                    $finded = preg_grep('~'.$input.'~', array_keys($value['params']));
                    $answers = PHDB::find(Form::ANSWER_COLLECTION, ['form' => $key, 'updated' => ['$exists' => true]], ['user', 'links',  'form', 'answers', 'updated']);

                    $users = PHDB::findByIds(Person::COLLECTION,array_column($answers, 'user'),array("name", "profilThumbImageUrl"));
                    foreach($answers as $aK => $aV) {
                        if(isset($users[$aV['user']]))
                            $answers[$aK]['name'] = $users[$aV['user']]['name'];
                            $answers[$aK]['profil'] = isset($users[$aV['user']]['profilThumbImageUrl']) ? $users[$aV['user']]['profilThumbImageUrl'] : '';
                    }
                    if(count($finded) > 0) {
                        $params = array();
                        foreach($finded as $i => $val) {
                            $params[$val] = $forms[$key]['params'][$val];
                            if($i == count($finded) -1) {
                                $forms[$key]['params'] = $params;
                            }
                        }
                        if(isset($value["subForms"])) {
                            $subForms = PHDB::find(Form::COLLECTION, ["id" => ['$in' => $value["subForms"]], "inputs" => ['$exists' => true]], ["id", "name", "inputs"]);
                        }
                        $forms[$key]['answers'] = $answers;
                        $iteration = 0;
                        foreach ($subForms as $k => $v) {
                            $indexFinded = array_search($v['id'], $forms[$key]['subForms'], true);
                            if($indexFinded > -1 && isset($v["inputs"])) {
                                $indexFindedCOdate = array_search('tpls.forms.cplx.sondageDate', array_column(json_decode(json_encode($v['inputs']), TRUE), 'type'));
                                if($indexFindedCOdate > -1) {
                                    foreach ($v['inputs'] as $iK => $iV) {
                                        if($iV['type'] != 'tpls.forms.cplx.sondageDate') {
                                            unset($v['inputs'][$iK]);
                                        }
                                    }
                                    $arrSub[$v['id']] = array_merge($v["inputs"], ['_id' => $k]);
                                }
                            }
                            if($iteration == count($subForms) - 1) {
                                $forms[$key]['allForms'] = $arrSub;
                            }
                            $iteration++;
                        }
                    } else {
                        unset($forms[$key]);
                    }
                } else if(!isset($value["params"])) {
                    $subForms = array();
                    $arrSub = array();
                    $answers = array();
                    $input = preg_quote('sondageDate', '~');
                    // $finded = preg_grep('~'.$input.'~', array_keys($value['params']));
                    $answers = PHDB::find(Form::ANSWER_COLLECTION, ['form' => $key, 'updated' => ['$exists' => true]], ['name', 'user', 'links',  'form', 'answers', 'updated']);
                    $userIds = array_map(function($val){
                        return new MongoId($val['user']);
                    },$answers);
                    // var_dump($userIds);exit();
                    $forms[$key]['params'] = array();
                    if(isset($value["subForms"])) {
                        $subForms = PHDB::find(Form::COLLECTION, ["id" => ['$in' => $value["subForms"]], "inputs" => ['$exists' => true]], ["id", "name", "inputs"]);
                    }
                    $forms[$key]['answers'] = $answers;
                    $iteration = 0;
                    foreach ($subForms as $k => $v) {
                        // $indexFinded = array_search($v['id'], $forms[$key]['subForms'], true);
                        if(isset($v["inputs"])) {
                            $indexFindedCOdate = array_search('tpls.forms.cplx.sondageDate', array_column(json_decode(json_encode($v['inputs']), TRUE), 'type'));
                            if($indexFindedCOdate > -1) {
                                foreach ($v['inputs'] as $iK => $iV) {
                                    if($iV['type'] != 'tpls.forms.cplx.sondageDate') {
                                        unset($v['inputs'][$iK]);
                                    }
                                }
                                $arrSub[$v['id']] = array_merge($v["inputs"], ['_id' => $k]);
                            }
                        }
                        if($iteration == count($subForms) - 1) {
                            $forms[$key]['allForms'] = $arrSub;
                        }
                        $iteration++;
                    }
                } else {
                    unset($forms[$key]);
                }
            }
        }
        return Rest::json($forms);
    }
}