<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;
use CAction;
use CO2Stat;

/**
* retreive dynamically 
*/
class DashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($url=null, $type=null) {
    	$controller=$this->getController();        
        CO2Stat::incNbLoad("co2-dashboard");   
        $page=(!empty($url)) ? $url : "dashboard";
        
        $params = array(
    		"title" => "Tableau de bord national des CTEzzz",
    		"blocks" 	=> array(
				"lineCTE" => array(
					"title"   => "CTE lancés",
					"counter" => "115",
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.lineCTE/size/S"
				),
				"barPorteurbyDomaine" => array(
					"title"   => "Actions",
					"counter" => "392",
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.barPorteurbyDomaine/size/S"
				),
				"pieFinance" => array(
					"title"   => "Ventilation Financière",
					"counter" => "661,300,001",
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieFinance/size/S"
				)
			)
    	);
        return $controller->renderPartial($page, $params, true);
    }
}