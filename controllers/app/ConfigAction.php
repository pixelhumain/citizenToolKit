<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;
use CAction;
use Preference;
use Yii;
use Rest;

class ConfigAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run($type, $id, $json = null) { 
    	$controller=$this->getController();
        $this->getController()->layout = "//layouts/mainSearch";
        $appKey=(isset($_POST["appKey"])) ? $_POST["appKey"] : "search";
        $params=array(
        	"appKey"=>$appKey,
        	"searchP"=>(isset($_POST["searchP"])) ? $_POST["searchP"] : array() ,
        	"domFilters"=>(isset($_POST["domFilters"])) ? $_POST["domFilters"] : "",
			"activatedF"=>(isset($_POST["activatedF"])) ? $_POST["activatedF"] : "",
        	"settings"=>(isset($_POST["settings"])) ? $_POST["settings"] : Preference::getAppConfig($appKey, $type, $id)
        );
        if ($json == true) {
         return Rest::json($params["settings"]);
         }
         if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("co2.views.pod.appConfig", $params, true);
        else
           return $controller->render("co2.views.pod.appConfig", $params);
    }
 }