<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use Authorisation;
use Element;
use Exception;
use Person;
use PixelHumain\PixelHumain\components\Action;
use Slug;
use Yii;
use yii\caching\Cache;

class NewsbuilderAction extends Action{
    public function run(){
        try{
            if(!$_SESSION["userId"])
                throw new Exception("Vous devez connecter pour acceder à cet application");

            $user = Person::getById($_SESSION["userId"]);
            $context = $user;

            if(isset($_GET["slug"])){
                try{
                    $slug = Slug::getBySlug($_GET["slug"]);

                    if(!$slug)
                        throw new Exception("L'element que vous avez spécifier n'existe pas");

                    $context = Element::getElementById($slug["id"], $slug["type"]);
                    if(!$context)
                        throw new Exception("L'element que vous avez spécifier n'existe pas");

                    if(!(
                        ($slug["type"] === Person::COLLECTION && $slug["id"] === $_SESSION["userId"]) ||
                        Authorisation::isElementAdmin($slug["id"], $slug["type"], $_SESSION["userId"])
                    ))
                        throw new Exception("Vous n'êtes pas admin de cet element.");
                }catch(Exception $e){
                    throw $e;
                }   
            }

            $params = [
                "contextData" => Element::getElementForJS($context, $context["collection"])
            ];

            $controller=Yii::app()->getController();
            return $controller->renderPartial("news-builder", $params, true);
        }catch(Exception $e){
            return $e->getMessage();
        }
    }
}