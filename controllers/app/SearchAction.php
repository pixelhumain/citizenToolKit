<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;
use CAction;
use CO2Stat;
use Costum;

/**
* retreive dynamically 
*/
class SearchAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($page="search", $type=null) {
    	$controller=$this->getController();        
        CO2Stat::incNbLoad("co2-search"); 
        /* Vérification autorisation page dans un costum */
        if(($msgNotAuthorized = Costum::getCostumMsgNotAuthorized($page)) != "")
            return $msgNotAuthorized;
        $params = array("type" => "all", "page"=>$page);
        return $controller->renderPartial("search", $params, true);
    }
}