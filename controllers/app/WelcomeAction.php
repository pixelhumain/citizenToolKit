<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use CAction, CO2Stat, Yii, CacheHelper, Cms, Element, Menu, PHDB, Authorisation, Document;
class WelcomeAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run($page=null,$url=null){
        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        CO2Stat::incNbLoad("co2-welcome");  
       /* if(!empty(CacheHelper::getCostum())){
            $costum = CacheHelper::getCostum();
            if(!empty(Cms::getCmsByWhere(array(
              "page"=>"welcome",
              "parent.".$costum["contextId"] => array('$exists'=>1),
            )))){ 
	            $url="costum.views.cmsBuilder.tplEngine.index";
	            $params = [
	              //"tpl" => $el["slug"],
	              "slug"=>$costum["slug"],
	              "page" => "welcome",
	             // "el"=>$el,
	              "tplUsingId"     => "",
	              "tplInitImage"   => "",
	              "nbrTplUser"     => 0,
	              "nbrTplViewer"   => 0,
	              "paramsData"     => [],
	              "insideTplInUse" => array(),
	              "cmsList"        => array(),
	              "newCmsId"       => array(),
	              "costum"         => $costum,
	              "cmsInUseId"     => array()
	            ];
	          	$condition=array(
	              "page"=>"welcome",
	              "parent.".$costum["contextId"] => array('$exists'=>1),
	              "haveTpl"=>"false"
	            );
	            // Get tpl in use----------------------------------------------------------
	            $usingTpl = Cms::getCmsByWhere(array("tplsUser.".$costum["contextId"].".welcome" => "using"));
	           // var_dump($usingTpl);
	            if (!empty($usingTpl)) {
	              $tplKey = isset(array_keys($usingTpl)['0']) ? array_keys($usingTpl)['0'] : "";
	            //  $params["insideTplInUse"] = $params["using"][$params["tplUsingId"]]; 
	              $params["tplInitImage"] = Document::getListDocumentsWhere(array("id"=> $tplKey, "type"=>Cms::COLLECTION), "file");
	            // Get cms liste in use----------------------------------------------------
	                $condition=array(
	                  array('$or'=> array(
	                      array("tplParent" => $tplKey),
	                      array("haveTpl"=>"false"))

	                  ),
	                  "parent.".$costum["contextId"] => array('$exists'=>1),
	                  "page" => "welcome"
	                );
	            }		          
	            $params["cmsList"] = Cms::getCmsByWhere(
	                  $condition,
	                 array("position" => 1)
	            );
	        
		        $pageDetail = !empty($costum["app"]["#".$params["page"]]) ? $costum["app"]["#".$params["page"]] : [];
		        $msgNotAuthorized = '<div class="col-lg-offset-1 col-lg-10 col-xs-12 margin-top-50 text-center text-red">	
		                                <i class="fa fa-lock fa-4x "></i><br/>
		                                <h1 class="">'.Yii::t("organization", "Unauthorized Access.").'</h1>
		                            </div>';
		        if(Yii::app()->request->isAjaxRequest){
		            if(CacheHelper::getCostum() != false){
		                if(Menu::showButton($pageDetail))
		                    return $controller->renderPartial($url, $params, true);
		                else
		                    echo $msgNotAuthorized;

		            }else{
		                return $controller->renderPartial($url, $params, true);
		            }
		        }else{
		            if(CacheHelper::getCostum() != false){
		                if(Menu::showButton($pageDetail))
		                    return $controller->renderPartial($url, $params, true);
		                else 
		                    echo $msgNotAuthorized;

		            }else{
		                return $controller->renderPartial($url, $params, true);
		            }
		        }
		    }else{
		    	 if(Yii::app()->request->isAjaxRequest)
		            return $controller->renderPartial("welcomeAjax", array(), true);
		        else 
		           return $controller->render( "welcome" , array());
		    }

    	}else{*/
	        if(Yii::app()->request->isAjaxRequest)
	            return $controller->renderPartial("welcomeAjax", array(), true);
	        else 
	           return $controller->render( "welcome" , array());
   		//}
    }
}