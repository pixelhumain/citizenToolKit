<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use CAction, PHDB, Rest, Yii;
/**
* retreive dynamically 
*/
class CheckUrlExistsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        
        $result = array("status"=> "error");

		$query = array("url"=>@$_POST["url"]);
        $siteurl = PHDB::findOne("url", $query);

        if(!isset($siteurl["url"])){
			$query = array("url"=>@$_POST["url"]."/");
	        $siteurl = PHDB::findOne("url", $query);

	        if(!isset($siteurl["url"])){
				$result = array("status"=> "URL_NOT_EXISTS");
		    }else{
		    	$result = array("status"=> "URL_EXISTS");
		    }
	    }else{
	    	$result = array("status"=> "URL_EXISTS");
	    }
	    
    	return Rest::json($result);
    }
}