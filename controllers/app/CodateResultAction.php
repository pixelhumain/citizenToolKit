<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use Answer;

class CodateResultAction extends \PixelHumain\PixelHumain\components\Action {

    public function run() {
        return Answer::ResultCoDate(
                $_POST['parentForm'],
                $_POST['codatekunik'],
                $_POST['inputKey'],
                $_POST['inputValue'],
                $_POST['paramsData'],
                $_POST['showAllResult']
            );
    }
}