<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use Yii;

class CodocAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller=$this->getController();
        $this->getController()->layout = "//layouts/mainSearch";

        return $controller->renderPartial("codoc", [
            "assetsUrl" => Yii::app()->getModule("co2")->assetsUrl
        ]);
    }
}