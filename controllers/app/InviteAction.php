<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use ActStr;
use Citoyen;
use Form;
use Notification;
use Person;
use PHDB;
use Survey;
use Yii;

class InviteAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {

        if ( Person::logguedAndValid() ) {
            $communityMembers = PHDB::findByIds($_POST['contextType'], array($_POST['contextId']), ['links']);
            
           return Notification::constructNotification (
                ActStr::VERB_INVITE_ALL, 
                array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), 
                array("type"=> $_POST['contextType'],"id"=> $_POST['contextId'] ),
                NULL,// array("id" => $_POST["objectId"], "type" => $_POST["objectType"]),
                Form::COLLECTION,
                NULL,
                array("formid" => $_POST["objectId"], "inputid" => $_POST["inputId"]),
            );
        if(isset($_POST["objectId"])) {
            foreach ($communityMembers as $key => $community) {
                if(isset($community['links'])) {
                    if(isset($community['links']['members'])) {
                        /* foreach ($community['links']['members'] as $id => $member) {
                            Notification::constructNotification (
                                ActStr::VERB_INVITE, 
                                array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), 
                                array("type"=> $member['type'],"id"=> $id ),
                                array("id" => $_POST["objectId"], "type" => $_POST["objectType"]) 
                            );
                        } */
                    }
                }
            }
        }
        }
    }
}