<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use CAction, PHDB, CO2Stat,Costum;
/**
* retreive dynamically 
*/
class LiveAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        

        $indexMin = isset($_POST['indexMin']) ? $_POST['indexMin'] : 0;
        $indexMax = isset($_POST['indexMax']) ? $_POST['indexMax'] : 10;

        $indexStep = $indexMax - $indexMin;
       
        $query = array('srcMedia' => array('$in' => array("NCI", "NC1", "CALEDOSPHERE", "NCTV")));
    	$medias = PHDB::findAndSortAndLimitAndIndex("media", $query, array("date"=>-1) , $indexStep, $indexMin);
    	
        $params = array("medias" => $medias );

        CO2Stat::incNbLoad("co2-live");

        /* Vérification autorisation page dans un costum */
        if(($msgNotAuthorized = Costum::getCostumMsgNotAuthorized("live")) != "")
            return $msgNotAuthorized;
            
    	if(@$_POST['renderPartial'] == true)
    	return $controller->renderPartial("liveStream", $params, true);
    	else
    	return $controller->renderPartial("live", $params, true);
    }
}