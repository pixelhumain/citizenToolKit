<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use CO2Stat, Answer, Form, Yii;

class PanelAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($paneltype, $type, $id, $index = null)
    {
        $controller = $this->getController();
        $this->getController()->layout = "//layouts/mainSearch";

        CO2Stat::incNbLoad("co2-page");

        $params = [];
        switch ($paneltype) {
            case 'discussionpanel':
                if(in_array($type, [Answer::COLLECTION]) && !empty($id)) {
                    $params["params"] = Form::getCommentedQuestionParams($id, null, true, $index);
                }
                $params["url"] = "co2.views.comment.commentCoform";
                break;
            
            default:
                $params["url"] = "co2.views.comment.commentCoform";
                break;
        }
        if (Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("panel", $params, true);
        else
            return $controller->render("panel", $params);
    }
}
