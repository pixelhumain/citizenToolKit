<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use CAction, Yii;
class CalendarAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run(){
        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        $params=array("calendarConfig"=>array());
        // reccurency : true (par default : false)
        // viewCal : "month/week/day"
        // nextAndPrev : true 
        // capacity : true
        // dragAndDrop : true
        // clickAndCreate :true/false
        if(!empty($_POST))
            $params["calendarConfig"]=$_POST;
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("co2.views.pod.agenda", $params, true);
        else
           return $controller->render("co2.views.pod.agenda", $params);
    }
}