<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use CAction, CO2Stat, Yii, CacheHelper, Cms, Element, Menu, PHDB, Authorisation, Document, Form;
use Costum;
use PixelHumain\PixelHumain\modules\costum\components\CostumPage;

class ViewAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run($page=null,$url=null, $extra=null){
        $costumPage = new CostumPage($this->getController(), CacheHelper::getCostum());
                            
        if(Yii::app()->request->isAjaxRequest)
            return $costumPage->renderPartial(@$page, $url, $extra);
        
        return $costumPage->render(@$page, $url, $extra);
    }
}