<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;
use CAction;
use CO2Stat;
use Person;
use Preference;
use Yii;
use CacheHelper;
use yii\helpers\Url;
use PixelHumain\PixelHumain\modules\costum\components\CostumPage;
class IndexAction extends \PixelHumain\PixelHumain\components\Action {
/**
* Dashboard Organization
*/
    public function run($page=null,$url=null){
        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        CO2Stat::incNbLoad("co2-welcome");  
        
        $hash = (@Yii::app()->session["userId"]) ? $controller->appConfig["pages"]["#app.index"]["redirect"]["logged"] : $controller->appConfig["pages"]["#app.index"]["redirect"]["unlogged"];

        $params = array("type" => @$type );
        if(!@$hash || @$hash==""){ 
            $hash="search";
        }
        else if($hash=="agenda"){
            $hash="search";
            $params = array("type"=>"events");
        }  
        if(Yii::app()->request->isAjaxRequest){
            if(!empty($hash) && $hash!=="home"){
                if(isset($controller->appConfig["pages"]["#".$hash]["hash"]) && $controller->appConfig["pages"]["#".$hash]["hash"] == "#app.view"){
                    $costumPage = new CostumPage($this->getController(), CacheHelper::getCostum());
                    return $costumPage->renderPartial($hash);
                }else if(isset($controller->appConfig["pages"]["#".$hash]["hash"]) && $controller->appConfig["pages"]["#".$hash]["hash"] == "#app.search"){
                    $params = array("page"=>$hash);
                    $hash="search";
                }else{
                    $hash="welcomeAjax";
                }     
            }
            if($hash=="home"){
                $params=array("live"=>Preference::getAppConfig("live", Person::COLLECTION, Yii::app()->session["userId"], array(
                        "name"=>Yii::t("common", "My network"),
                        "url"=> Url::to(["news/co/index", "type"=>"citoyens","id"=>Yii::app()->session["userId"],"isLive"=>"true"]),
                        "description"=>"All activities of my network. I see all posts form my community, friends, projects, organizations and enities I follow",
                        "edit"=>false,
                        "active" => true,
                        "order"=> 1
                    )
                ));
            
            }
            return $controller->renderPartial($hash, $params, true);
        }
        else 
           return $controller->render( $hash , $params);
    }
}