<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app;

use Authorisation;
use CTKAction, PHDB, Form, Aap;
use Rest;
use Yii;

class OnPingRefreshViewAction extends CTKAction
{
    public function run($action)
    {
        $coWs = Yii::app()->params['cows'];
        $curl = curl_init($coWs["pingRefreshViewUrl"]);
        $params = ["action" => $action];
        if(isset($_POST) && count($_POST) > 0) {
            foreach($_POST as $key => $value) {
                $params[$key] = $value;
            }
        }
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_exec($curl);
    }
}