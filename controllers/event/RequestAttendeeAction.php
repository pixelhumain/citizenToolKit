<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event;

use Event;
use MongoId;
use Organization;
use Person;
use PHDB;
use Project;
use Rest;
use Yii;

class RequestAttendeeAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		if (empty($_POST["id"]))
			return (Rest::json([
				"result"	=> false,
				"content"	=> "Missing event id"
		]));
		$db_event = PHDB::findOneById(Event::COLLECTION, $_POST["id"], ["links.attendees"]);
		$db_att = $db_event["links"]["attendees"] ?? [];

        return Rest::json([
			'result' => true,
            'attendees' => $db_att
        ]);
	}
}
