<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use CAction;
use Event;
use News;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\RssGenerator;
use Rest;
use Translate;
use TranslateCommunecter;
use TranslateGeoJson;
use TranslateJsonFeed;
use TranslateKml;
use TranslateSchema;
use Yii;

class GetAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null, $format = null, $limit=50, $index=0, $tags = null, $multiTags=null , $key = null, $insee = null, $rssParametre=null) {
		$controller=$this->getController();
		// Get format
		if( $format == Translate::FORMAT_SCHEMA)
	        $bindMap = (empty($id) ? TranslateSchema::$dataBinding_allEvent : TranslateSchema::$dataBinding_event);
		elseif ($format == Translate::FORMAT_KML)
			$bindMap = (empty($id) ? TranslateKml::$dataBinding_allEvent : TranslateKml::$dataBinding_event);
		elseif ($format == Translate::FORMAT_GEOJSON)
			$bindMap = (empty($id) ? TranslateGeoJson::$dataBinding_allEvent : TranslateGeoJson::$dataBinding_event);
		elseif ($format == Translate::FORMAT_JSONFEED)
			$bindMap = TranslateJsonFeed::$dataBinding_allEvent;
		else if( $format == Translate::FORMAT_MD || $format == Translate::FORMAT_TREE)
			$bindMap = Event::CONTROLLER;
		else
	       $bindMap = (empty($id) ? TranslateCommunecter::$dataBinding_allEvent : TranslateCommunecter::$dataBinding_event);
		
		$result = Api::getData($bindMap, $format, Event::COLLECTION, $id,$limit, $index, $tags, $multiTags, $key, $insee);

      	if ($format == Translate::FORMAT_KML) {
			$strucKml = News::getStrucKml();		
			Rest::xml($result, $strucKml,$format);
		} else if ($format === "rss") {
			$urlRss = Yii::$app->request->hostInfo; 
            $rssContent = RssGenerator::generateRss($id, $rssParametre, $urlRss, $result);
    		Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
    		Yii::$app->response->content = $rssContent;
    		return Yii::$app->response;
        } else
			return Rest::json($result);
    }
}


?>