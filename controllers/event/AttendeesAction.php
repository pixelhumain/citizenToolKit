<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event;

use Event;
use MongoId;
use Organization;
use Person;
use PHDB;
use Project;
use Rest;
use Yii;

class AttendeesAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($method = "get") {
		switch ($method) {
			case "get":
				if (empty($_POST["id"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Missing event id"
					]));
				$db_event = PHDB::findOneById(Event::COLLECTION, $_POST["id"], ["links.attendees"]);
				$db_att = $db_event["links"]["attendees"] ?? [];
				$classified = [Organization::COLLECTION => [], Project::COLLECTION => [], Person::COLLECTION => []];
				$db_attendees = [];
				foreach ($db_att as $id => $atts)
					$classified[$atts["type"]][] = $id;
				foreach ($classified as $type => $ids)
					$db_attendees[$type] = PHDB::findByIds($type, $ids, ["name", "profilImageUrl"]);
				$attendees = [];
				foreach ($db_attendees as $type => $data) {
					$attendees[$type] = [];
					foreach ($data as $id => $element) {
						$image = Yii::app()->getModule("co2")->getAssetsUrl() . "/images/thumb/default_${type}.png";
						if (!empty($element["profilImageUrl"]))
							$image = $element["profilImageUrl"];
						$attendees[$type][] = [
							"id"	=> $id,
							"name"	=> $element["name"],
							"image"	=> $image
						];
					}
				}
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $attendees
				]));
				break;
			case "join":
				$user = Yii::app()->session["userId"] ?? "";
				if (empty($user))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "You must be connected"
					]));
				if (empty($_POST["id"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Event id is missing"
					]));
				$data = [
					"name"	=> Yii::app()->session["user"]["name"],
					"type"	=> Person::COLLECTION
				];
				PHDB::update(Event::COLLECTION, ["_id" => new MongoId($_POST["id"])], ["\$set" => ["links.attendees.$user" => $data]]);
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $data
				]));
				break;
			case "quit":
				$user = Yii::app()->session["userId"] ?? "";
				if (empty($user))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "You must be connected"
					]));
				if (empty($_POST["id"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Event id is missing"
					]));
				$data = [
					"name"	=> Yii::app()->session["user"]["name"],
					"type"	=> Person::COLLECTION
				];
				PHDB::update(Event::COLLECTION, ["_id" => new MongoId($_POST["id"])], ["\$unset" => ["links.attendees.$user" => 0]]);
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $data
				]));
				break;
		}
	}
}
