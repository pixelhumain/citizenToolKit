<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use MongoId, MongoRegex;
use PHDB,Cms,Element,Yii,Document,Project,Role,Organization;

class MigrateAncientImageAndBGImageAction extends \PixelHumain\PixelHumain\components\Action{
    public function run() {
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){  
            foreach([Organization::COLLECTION, Project::COLLECTION] as $v){
                $costum = PHDB::find($v, array("costum"=>array('$exists'=>true)), array("_id","slug"));     
                foreach($costum as $idCostum => $vCostum){
                    
                    $this->migrateCms(@$vCostum["slug"]);
                };             
            }
        } else {
            echo "Vous n'avez pas le droit"; 
        }    
    }

    private function migrateCms($slug=null)
    {    
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){      
            if ($slug != null){
                ini_set('memory_limit', '-1');
                ini_set('max_execution_time', 10000);
                $index=0;
                $limit=2000;
                $eltType = array();
                $fields = array("path" => 1 ,"css" => 1 );
                $allBlock = PHDB::findAndLimitAndIndex(Cms::COLLECTION, array(  
                    "path" => new MongoRegex("/tpls.blockCms.superCms.container|tpls.blockCms.superCms.elements.image/i"),
                    "source.key" => $slug           
                ),$limit, $index);

                $all = 0 ;
                $blockWithImage= 0 ;
                $i = 0 ;
                foreach($allBlock as $kBlock => $vBlock){
                    $arrayParams = [];
                    $image = Document::getListDocumentsWhere(
                        array(
                            "id" => $kBlock,
                            "type" => 'cms',
                            "subKey" => 'block',
                        ),
                        "image"
                    );

                    if ( count($image) > 0 ){
                        if ( $vBlock['path'] == "tpls.blockCms.superCms.container"){
                            if(isset($vBlock['css']) ){
                                $arrayParams['css'] = $vBlock['css'];
                                $arrayParams['css']['backgroundImage'] = "url('".$image["0"]["imagePath"]."')";
                            } else {
                                $arrayParams['css'] = [];
                                $arrayParams['css']['backgroundImage'] = "url('".$image["0"]["imagePath"]."')";
                            }

                            if(Yii::$app->fs->has(Yii::app()->params['uploadDir']."communecter/cms/".$kBlock )){
                                $arrayParams['css']['backgroundType'] = "backgroundUpload";
                            } else {
                                $arrayParams['css']['backgroundType'] = "backgroundColor";
                            }
                            
                        } 

                        if ($vBlock['path'] == "tpls.blockCms.superCms.elements.image"){
                            $arrayParams['image'] = $image["0"]["imagePath"];
                        }

                        if(!empty($arrayParams)){
                            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($kBlock) ],[ 
                                '$set'=> $arrayParams
                            ]);  
                            $i++;   
                        } 
                    }
                }

                echo $slug.": <span style='color: red;'>".$i."</span> blocs à jour <br>"; 

            } else {
                echo "slug invalide <br>"; 
            } 
        }
    }
}   
