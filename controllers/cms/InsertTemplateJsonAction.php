<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use Yii;
use Rest;
use PHDB;
use DataValidator;
class InsertTemplateJsonAction extends \PixelHumain\PixelHumain\components\Action {
    public function run(){
        $controllers	= $this->getController();
        $params      	= DataValidator::clearUserInput($_POST);
        $params["type"] = "tplJson";
        $params["costumCreator"] = $params["costumSlug"];
        $params["created"]		 = time();
        $params["userId"]	  	 = Yii::app()->session["userId"];
        unset($params["costumEditMode"]);
        PHDB::insert("versionning", $params);
    }
}