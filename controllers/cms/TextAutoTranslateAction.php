<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CAction,Cms,Rest,Yii,CacheHelper, Costum, ArrayHelper, CO2, PHDB;
use DataValidator;
use MongoId;

class TextAutoTranslateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $apiUrl = 'https://api-free.deepl.com/v2/translate';
        $authKey = Yii::app()->params['deeplKey'];
        if (isset($_POST) && isset($authKey)) {
            $_POST = DataValidator::clearUserInput($_POST);
            $collection = $_POST["costumType"];
            $costumId = $_POST["costumId"];
            $DataCostum = PHDB::findOne($collection, array("_id" => new MongoId($costumId)), ["slug","costum.numberOfCharacterTrad"] );

            $curl = curl_init();
            $params = http_build_query([ 
                'auth_key' => $authKey,
                'text' => $_POST['text'],
                'source_lang' => (isset($_POST['lang_source'])) ? $_POST['lang_source'] : NULL,
                'target_lang' => $_POST['lang_target'],
            ]);

            curl_setopt_array($curl, [
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $params,
                CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']
            ]);
            $result = curl_exec($curl);
            if (curl_errno($curl)) {
                echo 'Error:' . curl_error($curl);
            }
            curl_close($curl);
            $result = json_decode($result);
            if (isset($result->translations[0]->text)) {
                $textTraducted = $result->translations[0]->text;
                if( !empty($DataCostum["costum"]["numberOfCharacterTrad"]) ) {
                    $query = array("costum.numberOfCharacterTrad" => $DataCostum["costum"]["numberOfCharacterTrad"] + strlen($_POST['text']));
                }else
                    $query = array("costum.numberOfCharacterTrad" => strlen($_POST['text']));

                PHDB::update($collection, array("_id" => new MongoId($costumId)), array('$set' => $query));
                return Rest::json(array("result" => true, "msg"=> Yii::t("common", "Success"), "words" => $textTraducted, "update-count-character-success" => Yii::t("cms", "updated item")));
            } else
                return Rest::json(array("result" => false, "msg"=> "curl vide", "update-count-character-error" => Yii::t("cms", "You are not allowed to do this action")));
        } else {
            return Rest::json(array("result" => false, "msg"=> "Deepl Key or ParamsToSend NULL"));
        }
    }
}