<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use CAction;

/**
* retreive dynamically 
*/
class DirectoryAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($page="search", $type=null) {
    	$controller=$this->getController();        
        return $controller->renderPartial("costum.views.tpls.blockCms.searchAndCreate", array(), true);
    }
}