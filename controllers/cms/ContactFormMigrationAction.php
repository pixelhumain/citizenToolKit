<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

    use Cms;
    use MongoId;
    use PHDB;
    use Rest;
    use Role;
    use Yii;

    class ContactFormMigrationAction extends \PixelHumain\PixelHumain\components\Action {
        public function run(){
            if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
                $contactForm = PHDB::find(Cms::COLLECTION, array(
                    "path" => "tpls.blockCms.contact.contactForm",
                    "type" => array('$ne' => "blockCms"),
                    "alreadyupdated" => array('$exists' => 0)
                ));
                $i = 0;
                foreach ($contactForm as $key => $contact) {
                    $this->updateParams($contact);
                    $i++;
                }
                return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
            } else {
                echo "vous n'avez pas le droit"; 
            }
        }

        private function updateParams($contacts){
            $paramsData = [
                "labelColor",
                "labelSize",
                "inputSize",
                "inputHeight",
                "inputRadius",
                "inputBorder",
                "inputBgColor",
                "inputTextColor",
                "inputBorderType",
                "sendBtnBgColor",
                "sendBtnLabelColor",
                "sendBtnBorderColor",
                "sendBtnRadius",
                "sendBtnTextSize",
                "subTitleBlockTextSize"
            ];
           
            $id = new MongoId((string)$contacts['_id']);
            unset($contacts["_id"]);
            foreach ($paramsData as $index => $param) {
                if(isset($contacts[$param])){
                    if($index == 0){
                        $contacts["css"]["label"]["color"] = $contacts[$param];
                    }
                    if($index == 1){
                        $contacts["css"]["label"]["fontSize"] = $contacts[$param];
                    }
                    if($index == 2){
                        $contacts["css"]["champ"]["fontSize"] = $contacts[$param];
                    }
                    if($index == 3){
                        $contacts["css"]["champ"]["height"] = $contacts[$param];
                    }
                    if($index == 4){
                        $contacts["css"]["champ"]["borderBottomLeftRadius"] = $contacts[$param].px;
                        $contacts["css"]["champ"]["borderBottomRightRadius"] = $contacts[$param].px;
                        $contacts["css"]["champ"]["borderTopLeftRadius"] = $contacts[$param].px;
                        $contacts["css"]["champ"]["borderTopRightRadius"] = $contacts[$param].px;
                    }
                    if($index == 5){
                        $contacts["css"]["champ"]["border"] = $contacts[$param]."px solid ".$contacts["inputBorderColor"];
                    }
                    if($index == 6){
                        $contacts["css"]["champ"]["backgroundColor"] = $contacts[$param];
                    }
                    if($index == 7){
                        $contacts["css"]["champ"]["color"] = $contacts[$param];
                    }
                    if($index == 8){
                        $contacts["css"]["champ"]["borderType"] = $contacts[$param];
                    }
                    if($index == 9){
                        $contacts["css"]["sendBouton"]["backgroundColor"] = $contacts[$param];
                    }
                    if($index == 10){
                        $contacts["css"]["sendBouton"]["color"] = $contacts[$param];
                    }
                    if($index == 11){
                        $contacts["css"]["sendBouton"]["borderColor"] = $contacts[$param];
                    }
                    if($index == 12){
                        $contacts["css"]["sendBouton"]["borderBottomLeftRadius"] = $contacts[$param].px;
                        $contacts["css"]["sendBouton"]["borderBottomRightRadius"] = $contacts[$param].px;
                        $contacts["css"]["sendBouton"]["borderTopLeftRadius"] = $contacts[$param].px;
                        $contacts["css"]["sendBouton"]["borderTopRightRadius"] = $contacts[$param].px;
                    }
                    if($index == 13){
                        $contacts["css"]["sendBouton"]["fontSize"] = $contacts[$param];
                    }
                    if($index == 14){
                        $contacts["css"]["subTitleBlockTextSize"]["fontSize"] = $contacts[$param];
                    }
                    unset($contacts[$param]);
                }
            }
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$set' => $contacts
            ]);
            $this->removeOldAttribute($id);
        }

        public function removeOldAttribute($id)
        {
            $unset = [
                "labelColor" => true,
                "labelSize" => true,
                "inputSize" => true,
                "inputHeight" => true,
                "inputRadius" => true,
                "inputBorder" => true,
                "inputBgColor" => true,
                "inputTextColor" => true,
                "inputBorderType" => true,
                "inputBorderColor" => true,
                "inputOtheBorderColor" => true,
                "inputPlaceholderColor" => true,
                "sendBtnBgColor" => true,
                "sendBtnLabelColor" => true,
                "sendBtnBorderColor" => true,
                "sendBtnRadius" => true,
                "sendBtnTextSize" => true,
                "subTitleBlockTextSize" => true
            ];
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$unset' => $unset
            ]);
        }
    }
?>