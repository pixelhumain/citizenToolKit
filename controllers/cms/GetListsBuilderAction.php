<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CAction, Cms, Rest;
class GetListsBuilderAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
 		$blockList = Cms::getCmsByWhere(
		           		 array("type" => "blockCms"),array( 'name' => 1 )
		           	);
		 // Get sample template-----------------------------------------
		$tplList = Cms::getTplByWhere();
		return Rest::json(
			array(
				"blocks"=>$blockList,
				"tpls"=>$tplList
			)
		);
    }
}


?>