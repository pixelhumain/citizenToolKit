<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use CAction;
use PHDB;
use MongoId;
use Rest;
use Template;
use Authorisation;
use Cms;
use Yii;
use DataValidator;
class DeleteTemplate extends \PixelHumain\PixelHumain\components\Action {
    public function run($id) {
        $_POST = DataValidator::clearUserInput($_POST); 
        if(Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])){
            $res = Rest::json(array("result" => false, "msg" => Yii::t('cms', 'impossible to delete this template.')));
            $controller=$this->getController();
            if(!empty($this->findTemplate($id)[$id]["cmsList"])){
                foreach($this->findTemplate($id)[$id]["cmsList"] as  $key => $value) {
                    if(!empty($this->findTemplate($value))){
                        $this->deleteTemplateAction($value);
                        Cms::deleteCms($value,Template::COLLECTION);
                    }
                }
            }
            if($this->deleteTemplateAction($id)) {
                $res = Rest::json(array("result" => true, "msg" => Yii::t('cms', 'Template successfully deleted.')));
            }
        }else
        $res = Rest::json(array("result" => true, "msg" => Yii::t("common", "You are not allowed to do this action")));
        return $res;
    }

    public function findTemplate($id) {
        $templateData = PHDB::find($_POST["collection"], array( "_id" => new MongoId($id)));
        return $templateData;
    }

    public function deleteTemplateAction($id) {
        if(in_array($_POST["collection"], [Cms::COLLECTION, Template::COLLECTION])){
            $res = PHDB::remove($_POST["collection"], array("_id"=>new MongoId($id)));
            PHDB::remove($_POST["collection"], array("tplParent"=> $id));
            return $res;
        }
    }
}
