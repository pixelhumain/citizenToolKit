<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use Rest;
use DataValidator;
use PHDB;
use Yii;

class GenerateImageOpenAIAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        if (isset($_POST)) {
            $_POST = DataValidator::clearUserInput($_POST);
            if (isset($_POST["prompt"]) && isset($_POST["options"])) {
                $response = $this->generateImageIA($_POST["prompt"], $_POST["options"]);
                $imageResponse = json_decode($response, true);
                if (!empty($imageResponse) && !empty($imageResponse["data"]) && isset($imageResponse["data"][0]) && isset($imageResponse["data"][0]["b64_json"])) {
                    return Rest::json(array("result" => true, "msg"=> Yii::t("common", "Success"), "responses" => $imageResponse["data"][0]["b64_json"]));
                } else {
                    return Rest::json(array("result" => false, "msg"=> Yii::t("cms", "Generate image error, please try again")));
                }
            } else {
                return Rest::json(array("result" => false, "msg"=> Yii::t("cms", "Prompt error, please try again")));
            }
        } else {
            return Rest::json(array("result" => false, "msg"=> Yii::t("cms", "Open ai key not found or something went wrong")));
        }
    }

    private function decryptKey($costumId, $costumSlug, $encryptedApiKey) {
        $key = hash('sha256', $costumId . $costumSlug, true);
        $iv = substr($key, 0, 16); 
        $options = 0;
        $encryptedApiKey = base64_decode($encryptedApiKey);
        $decrypted = openssl_decrypt($encryptedApiKey, "aes-256-cbc", $key, $options, $iv);
        return $decrypted;
    }

    private function generateImageIA($prompt, $options) {
        $costum = CacheHelper::getCostum();
        $openai_endpoint    = "https://api.openai.com/v1/images/generations";
        $dataKey = PHDB::findOne("accesskey", array(
            "parent.".$costum["contextId"]."" => ['$exists' => true],
            "keys.openAIKey" => ['$exists' => true]
        ));
        if (isset($dataKey)) {
            $decryptedKey = $this->decryptKey($costum["contextId"], $costum["contextSlug"], $dataKey["keys"]["openAIKey"]);
            if (isset($decryptedKey)) {
                $openai_token = $decryptedKey;
            } else {
                return false;
            }
        } else {
            return false;
        }
        if (!isset($openai_endpoint)) {
            return false;
        }
        if (isset($options["n"]))
            $options["n"] = intval($options["n"]);
        $openai_model = "dall-e-3";
        $data = [
            "model" => $openai_model,
            "prompt" => $prompt,
            "response_format" => "b64_json"
        ];
        $data = array_merge($options, $data);
        $headers = [
            "Content-Type: application/json",
            "Authorization: Bearer ".$openai_token
        ];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $openai_endpoint,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers
        ]);
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            echo "Error:" . curl_error($curl);  
        }
        curl_close($curl);
        return $response;
    }
}