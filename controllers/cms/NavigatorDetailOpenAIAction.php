<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use DataValidator;
use NavigatorCriteria;
use PHDB;
use Rest;
use Yii;

class NavigatorDetailOpenAIAction extends  \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();
        $costum = CacheHelper::getCostum(null, null, "openAtlas");
        if (isset($_POST)) {
            $data = DataValidator::clearUserInput($_POST);
            if(isset($data["criteria"])){
                $criteriaData = PHDB::findOne(NavigatorCriteria::COLLECTION, array("name" => $data["criteria"]));
                if($criteriaData){
                    $view =  $controller->renderPartial("costum.views.custom.franceTierslieux.previewDataViz.answers", array("criteriaData" => $criteriaData, "criteria" => $data["criteria"], "formId" => $data["formId"], "path" => $data["path"]));
                    return Rest::json(array("result" => true, "msg"=> Yii::t("common", "Success"), "msg" => "Success", "html" => $view));
                }else{
                    if (isset($data["messages"]) && isset($data["options"])) {
                        $responseJSON = $this->callOpenAi($data["messages"], $data["options"]);
                        $response = json_decode($responseJSON, true);
                        if (!empty($response) && !empty($response["choices"]) && isset($response["choices"][0]) && isset($response["choices"][0]["message"]) && isset($response["choices"][0]["message"]["content"])) {
                            preg_match("/```html(.*?)```/s", $response["choices"][0]["message"]["content"], $html);
                            if (!empty($html[1])) {
                                $htmlCode = trim($html[1]);
                                $navigatorCriteria = [
                                    "name" => $data["criteria"],
                                    "collection" => NavigatorCriteria::COLLECTION,
                                    "description" => $htmlCode,
                                    "parent" => [
                                        "type" => $costum["contextType"],
                                        "id" => $costum["contextId"]
                                    ],
                                    "created" => time(),
                                    "updated" => time(),
                                    "creator" => Yii::app()->session["userId"]
                                ];
                                PHDB::insert(NavigatorCriteria::COLLECTION, $navigatorCriteria);
                            } else {
                                return Rest::json(array("result" => false,
                                 "msg"=> Yii::t("cms", "Something went wrong, please try again"), "content" => $response["choices"][0]["message"]["content"]));
                            }

                            $htmlRender = $controller->renderPartial("costum.views.custom.franceTierslieux.previewDataViz.answers", array("criteriaData" => $navigatorCriteria, "criteria" => $data["criteria"], "formId" => $data["formId"], "path" => $data["path"]));
                            return Rest::json(array("result" => true, "msg"=> Yii::t("common", "Success"), "msg" => "Success", "html" => $htmlRender));
                        } else {
                            return Rest::json(array("result" => false, "msg"=> Yii::t("cms", "Something went wrong, please try again")));
                        }
                    } else {
                        return Rest::json(array("result" => false, "msg"=> Yii::t("cms", "Something went wrong, please try again")));
                    }
                }
            }
        } else {
            return Rest::json(array("result" => false, "msg"=> Yii::t("cms", "Open ai key not found or something went wrong")));
        }
    }

    private function decryptKey($costumId, $costumSlug, $encryptedApiKey) {
        $key = hash('sha256', $costumId . $costumSlug, true);
        $iv = substr($key, 0, 16); 
        $options = 0;
        $encryptedApiKey = base64_decode($encryptedApiKey);
        $decrypted = openssl_decrypt($encryptedApiKey, "aes-256-cbc", $key, $options, $iv);
        return $decrypted;
    }
    private function callOpenAi($message, $options) {
        $costum = CacheHelper::getCostum(null, null, "openAtlas");
        $openai_endpoint    = "https://api.openai.com/v1/chat/completions";
        $dataKey = PHDB::findOne("accesskey", array(
            "parent.".$costum["contextId"]."" => ['$exists' => true],
            "keys.openAIKey" => ['$exists' => true]
        ));
        if (isset($dataKey)) {
            $decryptedKey = $this->decryptKey($costum["contextId"], $costum["contextSlug"], $dataKey["keys"]["openAIKey"]);
            if (isset($decryptedKey)) {
                $openai_token = $decryptedKey;
            } else {
                return false;
            }
        } else {
            return false;
        }
        if (!isset($openai_endpoint)) {
            return false;
        }
        $openai_model = "gpt-3.5-turbo";
        $openai_temparature = 0.7;
        $data = [
            "model" => $openai_model,
            "messages" => [
                [
                    "role" => "system",
                    "content" => "Vous parler avec ChatGPT"
                ],
                [
                    "role" => "user",
                    "content" => $message
                ],
            ],
            "temperature" => $openai_temparature
        ];
        if (!empty($options))
            $data = array_merge($options, $data);

        $headers = [
            "Content-Type: application/json",
            "Authorization: Bearer $openai_token"
        ];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $openai_endpoint,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers
        ]);
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            echo "Error:" . curl_error($curl);  
        }
        curl_close($curl);

        return $response;
    }
}
