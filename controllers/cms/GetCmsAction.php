<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CAction, Element, Cms, PHDB, MongoId, Yii, Rest, Template;
class GetCmsAction extends \PixelHumain\PixelHumain\components\Action{
    private function duplicateSuperCms($idSuperCms = null, $idToDuplicate = null){
        $cms_elt = [];
        foreach ($idSuperCms as $key => $value) {
            $params = Element::getElementById($value,Cms::COLLECTION);
                $idParent = [];
                $params["_id"] = new MongoId($key);
                $idParent[$_POST["parentId"]] =  ["type" => $_POST["parentType"], "name" => $_POST["parentSlug"]];
                $params["page"] = $_POST["page"];
                $params["parent"] = $idParent;
                $params["blockParent"] = $idToDuplicate;
                if (!empty($params["cmsList"])) {
                    $this->duplicateSuperCms($params["cmsList"],(String)$params["_id"]);
                    unset($params["cmsList"]);
                }
                // if (!empty($params["path"])) {                    
                    $cms_elt[] = $params;
                // }
        }
        $res_cms = PHDB::batchInsert(Cms::COLLECTION,$cms_elt);
            // $res=array("result"=>true, "msg"=>Yii::t("common","Information saved"));
        return Rest::json($res_cms);
    }
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        if ($_POST["action"] == "duplicate") {        	
        	$array_result = [];
        	foreach ($_POST["ide"] as $key => $value) {
        		$params = Element::getElementById($value,Cms::COLLECTION);
                      $idParent = [];
                      $params["_id"] = new MongoId($key);
                      $idParent[$_POST["parentId"]] =  ["type" => $_POST["parentType"], "name" => $_POST["parentSlug"]];
                      $params["page"] = $_POST["page"];
                      $params["parent"] = $idParent;
                      $params["haveTpl"] = "false";
                      if ($_POST["tplParent"] !== "") {                
                        // $params["tplParent"] = $_POST["tplParent"];                        
                        $params["haveTpl"] = "false";
                    }else if($params["type"] == "blockChild"){
                        unset($params["tplParent"]);
                        unset($params["haveTpl"]);
                    }else if($params["type"] == "blockCms"){
                        $params["type"] = "blockCopy";
                    }
                    if (!empty($params["cmsList"])) {
                        $this->duplicateSuperCms($params["cmsList"],(String)$params["_id"]);
                        unset($params["cmsList"]);
                    }
                    // if (!empty($params["path"])) {                   
                        $array_result[] = $params;
                    // }
            }
                // var_dump($array_result);
        	$res = PHDB::batchInsert(Cms::COLLECTION,$array_result);
            // $res=array("result"=>true, "msg"=>Yii::t("common","Information saved"));
            return Rest::json($array_result);
        }elseif($_POST["action"] == "removestat"){
          foreach ($_POST["ide"] as $key => $value) {
            $params = Element::getElementById($value,Cms::COLLECTION);
            $params["haveTpl"] = true;
            $params["tplParent"] = $_POST["tplId"];
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($value) ],['$set'=>$params]);
          }
        }elseif($_POST["action"] == "tplEmpty"){
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($_POST["tplId"]) ],[ '$unset' => ["cmsList" =>true ] ]);          
        //}elseif($_POST["action"] == "upCmsList"){
         //   PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($_POST["tplId"]) ],['$set'=> ["cmsList" => $_POST["ide"]]]);          
        }elseif($_POST["action"] == "delete"){
            foreach ($_POST["ide"] as $key => $value) {
            $res = Element::deleteSimple($value,"cms", Yii::app()->session["userId"]); 
         }   
         return Rest::json($res);    
        }elseif ($_POST["action"] == "getType") {
            $res = [];
            $templateDataCms = PHDB::find(Cms::COLLECTION, array( "type" => $_POST["type"]));
            $templateDataTemplate = PHDB::find(Template::COLLECTION, array( "type" => $_POST["type"]));
            if (isset($templateDataCms) && isset($templateDataTemplate)) {
                $res = (object) array_merge($templateDataCms, $templateDataTemplate);
            } else if (isset($templateDataTemplate)) {
                $res = $templateDataTemplate;
            } else if (isset($templateDataCms)) {
                $res = $templateDataCms;
            }
            return Rest::json($res);
        }
    }

}
