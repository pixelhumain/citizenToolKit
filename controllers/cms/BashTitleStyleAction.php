<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms;
use MongoId;
use PHDB;
use Rest;
use Role;
use Yii;

class BashTitleStyleAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $count = PHDB::find(Cms::COLLECTION, array(
                "path" => "tpls.blockCms.text.title_style",
                "type" => array('$ne' => "blockCms"),
                "alreadyupdated" => array('$exists' => 0)
            ));
            $i = 0;
            foreach ($count as $key => $map) {
                $this->updateParams($map);
                $i++;
            }
            return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
        } else {
            echo "vous n'avez pas le droit"; 
        }
    }

    private function updateParams($maps){
        $paramsData = [
            "colordecor",
            "bgcolor",
            "iconLeftColor",
            "textLeftColor",
            "colorLabelButton"
        ];
        $id = new MongoId((string)$maps['_id']);
        unset($maps["_id"]);
        foreach ($paramsData as $index => $param) {
            if(isset($maps[$param])){
                if($index == 0){
                    $maps["css"]["colordecor"]["backgroundColor"] = $maps[$param];
                } 
                if($index == 1){
                    $maps["css"]["bgcolor"]["backgroundColor"] = $maps[$param];
                }
                if($index == 2){
                    $maps["css"]["iconLeftColor"]["color"] = $maps[$param];
                }
                if($index == 3){
                    $maps["css"]["textLeftColor"]["color"] = $maps[$param];
                }
                unset($maps[$param]);
            }
        }
        PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
            '$set' => $maps
        ]);
        $this->removeOldAttribute($id);
    }

    public function removeOldAttribute($maps)
    {
        $unset = [
            "colordecor" => true,
            "bgcolor"=> true,
            "iconLeftColor" => true,
            "textLeftColor" => true,
            "colorLabelButton" => true
        ];
        PHDB::update(Cms::COLLECTION,[ "_id" => $maps ],[
            '$unset' => $unset
        ]);
    }
}