<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms, PHDB, MongoId, Yii, Authorisation, Rest, DataValidator;
class RepaireCmsAction extends \PixelHumain\PixelHumain\components\Action
{
    public $orphans = [];

    public $allCmsChild = [];

    public function run()
    {
        $_POST = DataValidator::clearUserInput($_POST);
        if (Authorisation::isInterfaceAdmin()) {
            if ($_POST['check'] == "true") {
                $this->orphans = $this->checkCmsOrphans();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $result = $this->orphans;
                if (!empty($result)) {
                    $res = array("result"=>$this->orphans, "msg"=>""); 
                }else{
                    $res = array("result"=>false, "msg"=>"Cant find CMS orphans!");
                }
                
            }else{
                $this->orphans = $this->checkCmsOrphans();
                foreach($this->orphans as $k => $idcms){
                    PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($idcms) ],[
                        '$set'=> [
                            "page" => $_POST['page']
                        ]
                    ]);         
                }
            }
        }else{

            $res = array("result"=>false, "msg"=>Yii::t("common", "You are not loggued or do not have acces to this feature "));
        }
        return Rest::json($res);
    }

    private function getAllChild($id, $page) {
        $results = Cms::findCmsOrphans($id,$page);
        $this->allCmsChild = array_merge($this->allCmsChild,$results);
        foreach ($results as $key => $value) {
            $childOrphans = $this->getAllChild($value["id"],$_POST['page']);
        }
    }
    private function checkCmsOrphans() {
        $checkCmsOrphans = [];
        $this->getAllChild($_POST['idCms'],$_POST['page']);
        foreach ($this->allCmsChild as $key => $value) {
            if ($value["page"] != $_POST['page']) {
                $checkCmsOrphans[] = $value["id"];
            }
        }
        return $checkCmsOrphans;
    }
}