<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use CAction, Cms, Rest;
class GetCategoryUsedbyTemplateAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $tplList = Cms::GetCategoryUsedbyTemplate($_POST);
        $groupedData = [];

        foreach ($tplList as $key => $value) {
            $type = $value['type'];
            if (!isset($groupedData[$type])) {
                $groupedData[$type] = [];
            }
            $groupedData[$type][$key] = $value;
        }

        return Rest::json($groupedData);
    }
}