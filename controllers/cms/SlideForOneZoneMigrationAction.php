<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms;
use MongoId;
use PHDB;
use Rest;
use Role;
use Yii;

class SlideForOneZoneMigrationAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $basicmaps = PHDB::find(Cms::COLLECTION, array(
                "path" => "tpls.blockCms.slide.slide_map_scenario_for_zone",
                "type" => array('$ne' => "blockCms"),
                "alreadyupdated" => array('$exists' => 0)
            ));
            $i = 0;
            foreach ($basicmaps as $key => $map) {
                $this->updateParams($map);
                $i++;
            }
            return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
        } else {
            echo "vous n'avez pas le droit"; 
        }
    }

    private function updateParams($maps){
        $paramsData = [
            "type",
            "typeGraph",
            "zones",
            "region",
            "country",
            "level"
        ];
        $id = new MongoId((string)$maps['_id']);
        unset($maps["_id"]);
        foreach ($paramsData as $index => $param) {
            if(isset($maps[$param])){
                if($index == 0){
                    $maps["element"]["map"] = $maps[$param];
                }
                if($index == 1){
                    $maps["element"]["graph"] = $maps[$param];
                }
                if($index == 2){
                    $maps["zone"]["zones"] = $maps[$param];
                }
                if($index == 3){
                    $maps["zone"]["region"] = $maps[$param];
                }
                if($index == 4){
                    $maps["zone"]["country"] = $maps[$param];
                }
                if($index == 5){
                    $maps["zone"]["level"] = $maps[$param];
                }
                unset($maps[$param]);
            }
        }
        $maps["path"] = "tpls.blockCms.slide.slidemapscenarioforzone";
        $maps["alreadyupdated"] = 1;
        PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
            '$set' => $maps
        ]);
        $this->removeOldAttribute($id);
    }

    public function removeOldAttribute($maps)
    {
        $unset = [
            "type" => true,
            "typeGraph" => true,
            "zones" => true,
            "region" => true,
            "country" => true,
            "level" => true
        ];
        PHDB::update(Cms::COLLECTION,[ "_id" => $maps ],[
            '$unset' => $unset
        ]);
    }
}
