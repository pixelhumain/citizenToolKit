<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use Rest;
use Yii;
use PHDB;

class CheckKeyOpenAIAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $costum = CacheHelper::getCostum();
        if((bool)$costum["editMode"]) {
            $ApiKey = PHDB::findOne("accesskey", [
                "parent.".$costum["contextId"]."" => ['$exists' => true],
                "keys.openAIKey" => ['$exists' => true]
            ]);
            if (!empty($ApiKey)) {
                return Rest::json(array("result" => true, "apiKey" => true));
            } else {
                return Rest::json(array("result" => true, "apiKey" => false));
            }
        } else {
            return Rest::json(array("result" => false, "msg" => Yii::t("common","Something went wrong!")));
        }
    }
}