<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use PHDB, Rest, Yii,DataValidator,Authorisation,MongoId;


class GetFeatureInfoAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run() {
		$_POST = DataValidator::clearUserInput($_POST);
        $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        $costumVersions = PHDB::findOne("versionning", ['type' => "documentation"],array("description"));

        if (@$_POST["insert"]) {
        	if(Authorisation::isInterfaceAdmin()){
        		if (!empty($costumVersions)) {        		
        			PHDB::update("versionning",[ "_id" => new MongoId((string) $costumVersions["_id"]) ],[
        				'$set'=> [
        					"description" => $_POST["description"]
        				]
        			]);

        		}else {
        			$costumToSave = ["description" => $_POST['description'], "type" =>  "documentation"];
        			PHDB::insert("versionning",$costumToSave);
        		}
        	}
        	$costumVersions = PHDB::findOne("versionning", ['type' => "documentation"],array("description"));
        	$res = array("result"=>true, "doc" => $costumVersions);
        }else{

        	$res = array("result"=>true, "doc" => $costumVersions);

        }
        return Rest::json($res);
    }

}
