<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms;
use MongoId;
use PHDB;
use Rest;
use Role;
use Yii;

class BashModalAgendaCalendarAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $count = PHDB::find(Cms::COLLECTION, array(
                "path" => "tpls.blockCms.events.modal_with_agenda_calendar",
                "type" => array('$ne' => "blockCms"),
                "alreadyupdated" => array('$exists' => 0)
            ));
            $i = 0;
            foreach ($count as $key => $map) {
                $this->updateParams($map);
                $i++;
            }
            return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
        } else {
            echo "vous n'avez pas le droit"; 
        }
    }

    private function updateParams($maps){
        $paramsData = [
            "colorlabelButton",
            "colorButton",
            "colorBorderButton",
            "eventBackgroundColor",
            "eventToday",
            "colorlabelButtonHover",
            "colorButtonHover"
        ];
        $id = new MongoId((string)$maps['_id']);
        unset($maps["_id"]);
        foreach ($paramsData as $index => $param) {
            if(isset($maps[$param])){
                if($index == 0){
                    $maps["css"]["buttonCss"]["color"] = $maps[$param];
                } 
                if($index == 1){
                    $maps["css"]["buttonCss"]["backgroundColor"] = $maps[$param];
                }
                if($index == 2){
                    $maps["css"]["buttonCss"]["borderColor"] = $maps[$param];
                }
                if($index == 3){
                    $maps["css"]["eventCss"]["backgroundColor"] = $maps[$param];
                }
                if($index == 4){
                    $maps["css"]["todayCss"]["backgroundColor"] = $maps[$param];
                }
                if($index == 5){
                    $maps["css"]["hover"]["buttonCss"]["Color"] = $maps[$param];
                }
                if($index == 6){
                    $maps["css"]["hover"]["buttonCss"]["backgroundColor"] = $maps[$param];
                }
                unset($maps[$param]);
            }
        }
        PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
            '$set' => $maps
        ]);
        $this->removeOldAttribute($id);
    }

    public function removeOldAttribute($maps)
    {
        $unset = [
            "colorlabelButton" => true,
            "colorButton" => true,
            "colorBorderButton" => true,
            "eventBackgroundColor" => true,
            "eventToday" => true,
            "colorlabelButtonHover" => true,
            "colorButtonHover" => true
        ];
        PHDB::update(Cms::COLLECTION,[ "_id" => $maps ],[
            '$unset' => $unset
        ]);
    }
}