<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use CAction, Element, Cms, PHDB, MongoId, Yii, Rest;
use Authorisation, DataValidator,costum;
use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;

class UpPersistCmsAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $_POST = DataValidator::clearUserInput($_POST);

        if(Authorisation::isInterfaceAdmin() ){ 

            $costum = CacheHelper::getCostum();
            Cms::updatePersistent($_POST["idCms"],$_POST['value'],$_POST['fromParent']);
            if ($_POST['value'] == "footer") {
                PHDB::update($costum["contextType"],
                    [ "_id" => new MongoId($costum["contextId"]) ],
                    [ '$set'=> ["costum.htmlConstruct.footer.blockID" => $_POST["idCms"]]]);
            }else{
                PHDB::update( $costum["contextType"],
                    [ "_id" => new MongoId($costum["contextId"]) ], 
                    [ '$unset' => ["costum.htmlConstruct.footer.blockID" =>true ]]);
            }
            if ($controller->costum && @$controller->costum["contextSlug"]) {

                Costum::forceCacheReset();
            }

            return Rest::json(array("result"=>true, "msg"=> Yii::t("common", "Success")));  
        }else{
            return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));  
        }
    }

}