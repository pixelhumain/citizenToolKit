<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use CAction, Element, Cms, PHDB, MongoId, Yii, Rest, MongoDate, Authorisation, DataValidator;
class UpdateOrderRevealAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $costum = CacheHelper::getCostum();
        $_POST = DataValidator::clearUserInput($_POST);
        if(Authorisation::isInterfaceAdmin()){
            $datas = $_POST["elements"];
            foreach ($datas as $key => $element) {
                $dataToUpdate = array(
                    'name' => "Slide ".($key+1),
                    "position" => $key
                );
                PHDB::update(Cms::COLLECTION, array('_id' => new MongoId($element)), array('$set' => $dataToUpdate));
            }
            return Rest::json(array("result"=>true, "msg"=> Yii::t("cms", "updated item")));
        }else
        return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));

    }
}