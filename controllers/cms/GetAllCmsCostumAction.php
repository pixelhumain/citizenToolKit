<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use CacheHelper;
use Element;
use Cms;
use Rest;
class GetAllCmsCostumAction extends \PixelHumain\PixelHumain\components\Action {
    public function run(){
        $controller=$this->getController();
        $costum = CacheHelper::getCostum();
        $allCms = [];
        $where=array(
          "blockParent"=>array('$exists'=>false),
          "parent.".$costum["contextId"] => array('$exists'=>1),
        );
        if(!empty($_POST["page"])){
          $where["page"]=$_POST["page"];
        }        
        if(!empty($_POST["withChildren"])){
          unset($where["blockParent"]);
        }
        $allCms = Cms::getCmsByWhere($where);

        return Rest::json(array("allCms"=>array_keys($allCms)));            

    }
}