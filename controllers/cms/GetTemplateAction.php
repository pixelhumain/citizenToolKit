<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CAction, Cms, Template,DataValidator, Rest,Yii,Authorisation,PHDB,Document;
class GetTemplateAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){
            $template = PHDB::findOneById($_POST["templateType"], $_POST["templateId"]);
            $screenShot = Document::getListDocumentsWhere(
                array(
                    "id"=> $_POST["templateId"],
                    "type"=>$_POST["templateType"]
                ), "image"
            );
            foreach ($screenShot as $key => $value) {
                $screenShot[$key]["imageThumbPath"] = $value["imagePath"];
            }
            $template["images"] = $screenShot;
            $res=array("result"=>true, "msg"=> "", "template" => $template);

        }
  
        return Rest::json($res);
    }
}