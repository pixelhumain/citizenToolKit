<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Costum;
use PixelHumain\PixelHumain\components\Action;
use Rest;

class ElementHasCostumAction extends Action {
	public function run($id, $type) {
		return (Rest::json(Costum::element_have_costum($id, $type)));
	}
}
