<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use PHDB;
use Role;
use Yii;

class CheckCmsElementAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($path)
    {
        if( Role::isUserSuperAdminCms(Role::getRolesUserId(Yii::app()->session["userId"]) ) ||  Role::isUserSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $parents = PHDB::find("cms", ["path" => $path, "type" => ['$ne' => "blockCms"]], array("parent", "page"));
            $slugs = array();
            $text = "";
            foreach ($parents as $element) {
                foreach ($element['parent'] as $key => $value) {
                    $parent = PHDB::findOneById(isset($value['type']) ? $value['type'] : (isset($value['collection']) ? $value['collection'] : ''), $key);
                    if($parent){
                        array_push($slugs, $parent['slug'].' ('.$element['page'].')');
                    }else{
                        $text .= $value['type'].' '.$value['name'].' '.$key."<br>";
                    }
                }
            }
            $text  .= "<br>Path : $path<br>Description : <br>Context : <br><dd><code>";
            foreach (array_unique($slugs) as $key => $value) {
                $text .= "- ".$value.'<br>';
            }
            $text .=  "</code>";
            return $text;
        }
    }
}