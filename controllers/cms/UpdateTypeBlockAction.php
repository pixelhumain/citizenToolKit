<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
 
use PHDB,Cms,MongoId;

class UpdateTypeBlockAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $allCms = PHDB::find(Cms::COLLECTION,array(
            "type" => array('$ne' => "blockCopy"),
            "haveTpl" => array('$exists' => true)
            )
        );
        $count = 0;
        $output = "";
        foreach($allCms as $kcms => $vcms){
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($kcms) ],[
                '$set'=> [
                    "type" => "blockCopy"
                ]
            ]);
            $count++;            
        } 
        $output .= $count ." bloc mis à jour";
        return $output;
    }
}