<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use CAction ,Cms, DataValidator, Log, Yii;

class DeleteAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($id=null) {
        $controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);

        if (!isset($_POST["blockName"]) && isset($_POST["blockPath"])){
            $strArray = explode('.',$_POST["blockPath"]);
            $blockName = end($strArray);
        } else {
            $blockName = $_POST["blockName"];
        }
        $dataLogSave = array(
            "userId"            =>  Yii::app()->session["userId"],
            "userName"          =>  Yii::app()->session["user"]["name"],
            "costumId"          =>  @$_POST["costumId"],
            "costumSlug"        =>  @$_POST["costumSlug"],
            "costumType"        =>  @$_POST["costumType"],
            "costumEditMode"    =>  @$_POST["costumEditMode"],
            "browser"           =>  @$_SERVER["HTTP_USER_AGENT"],
            "ipAddress"         =>  @$_SERVER["REMOTE_ADDR"],
            "created"           =>  time(),
            "action"            => "costum/deleteBlock",
            "blockPath"         =>  @$_POST["blockPath"],
            "blockName"         =>  $blockName,
            "collection"        =>  @$_POST["collection"],
            "page"              =>  @$_POST["page"],
            "idBlock"           =>  @$_POST["cmsId"],
            "allBlockToDelete"  =>  @$_POST["allBlockToDelete"]
        );
        if (isset($_POST["children"]))
            $dataLogSave["children"] = $_POST["children"];
        Log::saveAndClean(
            $dataLogSave
        );
        $res = Cms::deleteCms($_POST["cmsId"]);
        return $res;
    }
}