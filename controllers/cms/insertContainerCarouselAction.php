<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Authorisation;
use CacheHelper;
use Cms;
use DataValidator;
use MongoDate;
use MongoId;
use PHDB;
use Rest;
use Yii;

class insertContainerCarouselAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();
        $costum = CacheHelper::getCostum();
        $_POST = DataValidator::clearUserInput($_POST);
        if (Authorisation::isInterfaceAdmin()) {
            $listCms = PHDB::find(Cms::COLLECTION, array("blockParent" => $_POST["blockParent"]));
            foreach ($_POST["carouselData"] as $position => $value) {
                $cmsData = PHDB::findOne(Cms::COLLECTION, array("blockParent" => $_POST["blockParent"], "carouselTitle" => $value["title"]));
                if($cmsData){
                    PHDB::update(Cms::COLLECTION, array("_id" => new MongoId((string) $cmsData["_id"])), array('$set' => array(
                        "name" => "Carousel section ".($position+1),
                        "position" => $position,
                    )));
                    unset($listCms[(string)$cmsData["_id"]]);
                }else{
                    $newBlocContainer=array(
                        "path"=>"tpls.blockCms.superCms.container",
                        "name" => "Carousel section ".($position+1),
                        "position" => $position,
                        "collection" => Cms::COLLECTION,
                        "notEditable" => true,
                        "parent"=>array(
                            $costum["contextId"]=>array(
                                "type"=>$costum["contextType"],
                                "name"=>$costum["contextSlug"]
                            )),
                        "page"=>$_POST["page"],
                        "blockParent"=>$_POST["blockParent"],
                        "modified"=> new MongoDate(time()),
                        "updated" => time(),
                        "creator" => Yii::app()->session["userId"],
                        "created" => time(),
                        "carouselTitle" => $value["title"],
                        "css" => array(
                            "width" => "100%",
                            "height" => "100%",
                        ),
                        "source"=>array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]])
                    );
                    Yii::app()->mongodb->selectCollection(Cms::COLLECTION)->insert($newBlocContainer);
                }
            }
            if(count($listCms) > 0){
                foreach ($listCms as $id => $value) {
                    Cms::deleteCms($id);
                }
            }
            return Rest::json(array("result" => true, "msg" => Yii::t("cms", "Element well added")));
        } else
            return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));
    }
}