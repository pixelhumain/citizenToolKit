<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Authorisation;
use Cms;
use MongoId;
use PHDB;
use Rest;

class MigrateAnchorTargerAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {

        if(Authorisation::isInterfaceAdmin()){
            $cms = PHDB::find(Cms::COLLECTION, array("path" => "tpls.blockCms.superCms.container", "anchorTarget" => array('$exists' => 1)));
            $i = 0;
            foreach ($cms as $key => $value) {
                $value["advanced"]["anchorTarget"] = $value["anchorTarget"];
                unset($value["anchorTarget"]);

                PHDB::update(Cms::COLLECTION, array("_id" => new MongoId($key)), array('$set' => $value, '$unset' => array(
                    "anchorTarget" => 1
                )));
                $i++;
            }
            return Rest::sendResponse(200, "<p>$i blocks updated</p>");
        }else{
            return Rest::sendResponse(200, "Who the hell are You ?");
        }
    }
}
