<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use CAction, Element, Cms, PHDB, MongoId, Yii, Rest, MongoDate, Authorisation, DataValidator;
class InsertSlideRevealAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $costum = CacheHelper::getCostum();
        $_POST = DataValidator::clearUserInput($_POST);
        if(Authorisation::isInterfaceAdmin()){
            $lastPosition = PHDB::findAndSortAndLimitAndIndex(Cms::COLLECTION, array("blockParent" => $_POST["blockParent"]), array("position" => -1), 1);
            if(count($lastPosition) != 1){
                $position = 0;
            }else{
                $position = array_values($lastPosition)[0]["position"] + 1;
            }
            $newBlocContainer=array(
                "path"=>"tpls.blockCms.superCms.container",
                "name" => "Slide ".($position+1),
                "position" => $position,
                "collection" => Cms::COLLECTION,
                "parent"=>array(
                    $costum["contextId"]=>array(
                        "type"=>$costum["contextType"],
                        "name"=>$costum["contextSlug"]
                    )),
                "page"=>$_POST["page"],
                "blockParent"=>$_POST["blockParent"],
                "modified"=> new MongoDate(time()),
                "updated" => time(),
                "creator" => Yii::app()->session["userId"],
                "created" => time(),
                "advanced"=> array(
                    "balise" => "section"
                ),
                "css" => array(
                    "width" => "100%",
                    "paddingTop" => "0",
                    "paddingLeft" => "0",
                    "paddingRight" => "0",
                    "paddingBottom" => "0"
                ),
                "source"=>array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]])
            );
            Yii::app()->mongodb->selectCollection(Cms::COLLECTION)->insert($newBlocContainer);
            $viewBlocParams=Cms::getParamsView($newBlocContainer);
            return Rest::json(array("html" => $controller->renderPartial("costum.views.cmsBuilder.blockEngine.partials.chargeBlock", $viewBlocParams,true),"params" => $newBlocContainer));
        }else
        return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));

    }
}