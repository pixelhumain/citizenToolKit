<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CAction,Cms,Rest,Yii,CacheHelper, Costum, ArrayHelper, CO2, DataValidator;
class RefreshMenuAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
    	$costum = CacheHelper::getCostum();
    	$page='@themes/'.Yii::app()->theme->name.'/views/layouts/menus/constructMenu';
      if (isset($_POST["menuConfig"])) {
        $costumMenuConfig =DataValidator::convertStringToBoolean($_POST["menuConfig"]); 
      }
      //var_dump($costumMenuConfig);// exit;
      //$themeMenuConfig=ArrayHelper::getValueByPath($controller->appConfig, $_POST["path"]);
      //if(empty($themeMenuConfig)){
        $initMenuAppConfig=CO2::getThemeParams();
        $themeMenuConfig=ArrayHelper::getValueByPath($initMenuAppConfig, $_POST["path"]);
      //}
     	$listButtonMenus=array(
            "pages"=>$_POST["pagesConfig"],
            "mainMenuButtons"=>@$controller->appConfig["mainMenuButtons"],
            "elementMenuButtons"=>@$controller->appConfig["elementMenuButtons"]
        );
      
                //echo "icccciiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii";
           
              // print_r($costumMenuConfig); 
               // var_dump($costumMenuConfig);
               // echo $keyMenu;
                // Get value of menu themeParams config with costum config 
      //	var_dump($themeMenuConfig);
        $appConfigMenu=(!empty($costumMenuConfig)) ? Costum::constructMenu($costumMenuConfig,$themeMenuConfig , $listButtonMenus, null, @$costum["logo"]) : false;
        //var_dump($appConfigMenu);exit;
        $lastIndexKey=explode(".", $_POST["path"]);
        $connectedMode=null;
        if(@$_POST["connectedMode"] && $_POST["connectedMode"]==="false") $connectedMode=false;
      	$params=array(
                "params"=>    $appConfigMenu,
                "layoutPath"=>'@themes/'.Yii::app()->theme->name.'/views/layouts/',
                "path"=>$_POST["path"],
                "key"=> $lastIndexKey[(count($lastIndexKey)-1)],
                "connectedMode"=> $connectedMode
            );
       

    	/*if(isset($_POST["key"]))
    		$params["key"]=$_POST["key"];
      else if (@$_POST["id"]))
        $params["key"]=$_POST["id"];*/
 		//var_dump($params);
      	if(Yii::app()->request->isAjaxRequest)
			return $controller->renderPartial($page,$params,true);
		else 
			return	$controller->render( $page , $params );
    }   
}