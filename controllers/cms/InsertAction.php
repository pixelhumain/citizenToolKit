<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CAction;
use Cms;
use Rest;
use Authorisation, DataValidator;

class InsertAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
    	if(Authorisation::isInterfaceAdmin()){
	        if(isset($_POST["cms"]))
	            $res = Cms::insertMultiple($_POST["cms"]);       
	        if($res)
				return Rest::json(array("result" => true, "msg" => "Cms has been add succesfully","data"=>$_POST["cms"]));
			else
	            return Rest::json(array("result" => false, "msg" => "Cms not added"));
	    }else
	    	return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));
    }   
}