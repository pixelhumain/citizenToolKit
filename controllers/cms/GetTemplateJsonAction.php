<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use PHDB, Authorisation, Rest,CacheHelper, DataValidator, Yii;


class GetTemplateJsonAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        // $_POST = DataValidator::clearUserInput($_POST);
        $res = array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){            
        	$costum = CacheHelper::getCostum();
            $tplsJson = PHDB::find("versionning", ['type' => "tplJson"]);
            $res = array("result"=>true, "items" => $tplsJson);
        }
        
        return Rest::json($res);
    }

}
