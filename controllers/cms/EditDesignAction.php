<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Element;
use Slug;
use Rest;
use CacheHelper;
use Yii;
use Authorisation, DataValidator;

class EditDesignAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $_POST = DataValidator::clearUserInput($_POST);
        if (Authorisation::isInterfaceAdmin()) {
            $controller=$this->getController();
            $path = "costum.css";
            array_walk_recursive(
                $_POST["value"],
                function (&$value) {
                    if ($value=="true")
                        $value = true;
                    else if($value == "false")
                        $value = false;
                }
            );
            if($controller->costum && isset($controller->costum["contextSlug"])){
                CacheHelper::delete($controller->costum["contextSlug"]);
            }

            $res = Element::updatePathValue($_POST["costumType"],$_POST["costumId"], $path, $_POST["value"], null, null, null, null, true );
            return Rest::json(array("result"=>true, "msg"=>Yii::t("common","Information updated"),"value"=> $_POST["value"]));
        }else{
            return Rest::json(array("result"=>false, "msg"=>Yii::t("common", "Please Login First"),"value"=> $_POST["value"]));
        }
        
    }
}
