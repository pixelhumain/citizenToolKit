<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use Document;
use Rest;
class GetListFile extends \PixelHumain\PixelHumain\components\Action {
    public function run(){
        $controller=$this->getController();
        if (isset($_POST["costumId"]) && isset($_POST["costumType"]) && isset($_POST["doctype"]) && isset($_POST["subKey"])) {
            $initFiles = Document::getListDocumentsWhere(
                array(
                    "id"=> $_POST["costumId"],
                    "type"=> $_POST["costumType"],
                    "subKey"=>$_POST["subKey"]
                ), $_POST["doctype"]
            );
            return Rest::json(array("result" => $initFiles));
        }

    }
}