<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Authorisation;
use CacheHelper;
use Cms;
use DataValidator;
use MongoDate;
use MongoId;
use PHDB;
use Rest;
use Yii;

class InsertContainerSlideGalleryAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();
        $costum = CacheHelper::getCostum();
        $_POST = DataValidator::clearUserInput($_POST);
        if (Authorisation::isInterfaceAdmin()) {
            $listCms = PHDB::find(Cms::COLLECTION, array("blockParent" => $_POST["blockParent"]));
            foreach ($_POST["images"] as $position => $image) {
                $cmsData = PHDB::findOne(Cms::COLLECTION, array("blockParent" => $_POST["blockParent"], "css.backgroundImage" => "url('$image')", "css.backgroundType" => "backgroundUpload"));
                if($cmsData){
                    PHDB::update(Cms::COLLECTION, array("_id" => new MongoId((string) $cmsData["_id"])), array('$set' => array(
                        "name" => "Gallery slide ".($position+1),
                        "position" => $position,
                    )));
                    unset($listCms[(string)$cmsData["_id"]]);
                }else{
                    $newBlocContainer=array(
                        "path"=>"tpls.blockCms.superCms.container",
                        "name" => "Gallery slide ".($position+1),
                        "position" => $position,
                        "collection" => Cms::COLLECTION,
                        "notEditable" => true,
                        "parent"=>array(
                            $costum["contextId"]=>array(
                                "type"=>$costum["contextType"],
                                "name"=>$costum["contextSlug"]
                            )),
                        "page"=>$_POST["page"],
                        "blockParent"=>$_POST["blockParent"],
                        "modified"=> new MongoDate(time()),
                        "updated" => time(),
                        "creator" => Yii::app()->session["userId"],
                        "created" => time(),
                        "css" => array(
                            "backgroundType" => "backgroundUpload",
                            "backgroundImage" => "url('$image')",
                            "width" => "100%",
                            "height" => "100%",
                            "backgroundPosition" => "center center",
                            "backgroundRepeat" => "no-repeat",
                            "backgroundSize" => "cover"
                        ),
                        "source"=>array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]])
                    );
                    Yii::app()->mongodb->selectCollection(Cms::COLLECTION)->insert($newBlocContainer);
                }
            }
            if(count($listCms) > 0){
                foreach ($listCms as $id => $value) {
                    Cms::deleteCms($id);
                }
            }
            return Rest::json(array("result" => true, "msg" => Yii::t("cms", "Element well added")));
        } else
            return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));
    }
}