<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use MongoId;
use PHDB,Cms,Yii,Role;

class MigrateParamsButtonAction extends \PixelHumain\PixelHumain\components\Action{
    public function run() {

        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){ 
            $output = "";
            $i = 0;
            $allButton = PHDB::find(Cms::COLLECTION, array(  
                "path" =>  "tpls.blockCms.superCms.elements.button",
                "params" => array('$exists'=>1),
                "typeUrl" => array('$exists'=> 0),
                )
            );
            $output.= count($allButton) ." bouttons à mettre à jour <br>";
            foreach($allButton as $kButton => $vButton){
                $arrayParams = [];

                if(isset($vButton["params"]["external"])){
                    $arrayParams["link"] = $vButton["params"]["external"];
                } if(isset($vButton["params"]["url"])){
                    $arrayParams["link"] = $vButton["params"]["url"];
                } else {
                    $arrayParams["link"] = "";
                } 

                if (str_starts_with($arrayParams["link"], 'http')){
                    $arrayParams["typeUrl"] = "externalLink";
                } else if (str_starts_with($arrayParams["link"], '#')){
                    $arrayParams["typeUrl"] = "internalLink";
                }

                if(isset($vButton["params"]["type"])){
                    if ($vButton["params"]["type"] == "lbh"){
                        $arrayParams["targetBlank"] = true;
                    } else {
                        $arrayParams["targetBlank"] = false;
                    }
                }
                
                if(!empty($arrayParams)){
                    PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($kButton) ],[ 
                        '$set'=> $arrayParams
                    ]);  
                    $i++;   
                }
            }
            $output .= $i." blocs bouton  à jour"; 
        return $output;

        } else {
            echo "vous n'avez pas le droit"; 
        }
            
    }
}