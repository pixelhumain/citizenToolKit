<?php
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

    use Cms;
    use MongoId;
    use PHDB;
    use Rest;
    use Role;
    use Yii;

    class FooterContactMigrationAction extends \PixelHumain\PixelHumain\components\Action {
        public function run(){
            if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
                $footerContact = PHDB::find(Cms::COLLECTION, array(
                    "path" => "tpls.blockCms.footer.footer_contact",
                    "type" => array('$ne' => "blockCms"),
                    "alreadyupdated" => array('$exists' => 0)
                ));
                $i = 0;
                foreach ($footerContact as $key => $footer) {
                    $this->updateParams($footer);
                    $i++;
                }
                return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
            } else {
                echo "vous n'avez pas le droit"; 
            }
        }

        private function updateParams($footers){
            $paramsData = [
                "socialColor",
                "socialBgColor"
            ];
           
            $id = new MongoId((string)$footers['_id']);
            unset($footers["_id"]);
            foreach ($paramsData as $index => $param) {
                if(isset($footers[$param])){
                    if($index == 0){
                        $footers["css"]["social"]["color"] = $footers[$param];
                    }
                    if($index == 1){
                        $footers["css"]["social"]["backgroundColor"] = $footers[$param];
                    }
                    
                    unset($footers[$param]);
                }
            }
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$set' => $footers
            ]);
            $this->removeOldAttribute($id);
        }

        public function removeOldAttribute($id)
        {
            $unset = [
                "socialColor" => true,
                "socialBgColor" => true
            ];
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$unset' => $unset
            ]);
        }
    }
?>