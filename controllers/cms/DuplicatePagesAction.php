<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use Cms, PHDB, MongoId, Rest, Authorisation, DataValidator, Log, Yii;

class DuplicatePagesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {   
        $_POST = DataValidator::clearUserInput($_POST);  
        if (Authorisation::isInterfaceAdmin()) {
            $page = str_replace("#", "", $_POST['page']);
            $cms = PHDB::find(Cms::COLLECTION, ["page" => $page, "parent.".$_POST['costumId'] => ['$exists'=>1], "blockParent" => ['$exists'=>0], "path" => ['$exists'=>1]], ["path", "name"]); 
            $costum = PHDB::findOne($_POST['costumType'],['_id' => new MongoId($_POST['costumId'])]);
            $costumCache = CacheHelper::getCostum();
            $newPage = Cms::getNewPageName($page, $costum);
            $costum["costum"]["app"][$newPage['pagename']] = $costum["costum"]['app'][$_POST['page']];
            $costum["costum"]["app"][$newPage['pagename']]['name'] = $newPage['name'];
            $costum["costum"]["app"][$newPage['pagename']]['urlExtra'] = "/page/".trim($newPage['pagename'], "#");
            $set = array( 
                "costum.app"=> $costum["costum"]["app"]
            );
            if(!empty($cms)){
                    Cms::duplicateBlock(array_keys($cms),array(
                        "parentId"   => $_POST['costumId'], 
                        "parentType" => $_POST["costumType"],
                        "parentSlug" => $_POST['costumSlug'],
                        "page"       => trim($newPage['pagename'], "#")));       
            }

            // save action to Log
            Log::saveAndClean(
                array(
                    "userId"         =>  Yii::app()->session["userId"],
                    "userName"       =>  Yii::app()->session["user"]["name"],
                    "costumId"       =>  @$_POST["costumId"],
                    "costumSlug"     =>  @$_POST["costumSlug"],
                    "costumType"     =>  @$_POST["costumType"],
                    "costumEditMode" =>  @$_POST["costumEditMode"],
                    "browser"        =>  @$_SERVER["HTTP_USER_AGENT"],
                    "ipAddress"      =>  @$_SERVER["REMOTE_ADDR"],
                    "created"        =>  time(),
                    "action"         =>  @$_POST["action"],
                    "subAction"      =>  @$_POST["subAction"],
                    "page"           =>  str_replace('#', '', $_POST["page"]),
                )
            );
            PHDB::update($_POST['costumType'], array('_id' => new MongoId($_POST['costumId'])), array('$set' => $set));
            if(!isset($costumCache["resetCache"])){ 
                $costumCache["resetCache"]=true;
                CacheHelper::set($_POST['costumSlug'], $costumCache);
            }
            return Rest::json(array("costum" => $costum,"page" => array($newPage['pagename'] => $costum["costum"]["app"][$newPage['pagename']])));
        }
    }


    function findKeyInArray($array, $key){
        if(in_array($key,array_keys($array))){
            return Array($key);
        }
        foreach($array as $k=>$v){
            if(is_array($v)){
                $result = $this->findKeyInArray($v, $key);
                if ($result !== null){
                    array_unshift($result,$k);
                    return $result;
                }
            }
            if(in_array($key,array_keys($array))){
                return Array($key);
            }
        }
        return null;
    }
}
