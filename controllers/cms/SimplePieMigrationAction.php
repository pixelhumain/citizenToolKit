<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

    use Cms;
    use MongoId;
    use PHDB;
    use Rest;
    use Role;
    use Yii;
 
    class SimplePieMigrationAction extends \PixelHumain\PixelHumain\components\Action {
        public function run(){
            if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
                $simplepie = PHDB::find(Cms::COLLECTION, array(
                    "path" => "tpls.blockCms.graph.simplePie",
                    "type" => array('$ne' => "blockCms"),
                    "alreadyupdated" => array('$exists' => 0)
                ));
                $i = 0;
                foreach ($simplepie as $key => $pie) {
                    $this->updateParams($pie);
                    $i++;
                }
                return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
            } else {
                echo "vous n'avez pas le droit"; 
            }
        }

        private function updateParams($pies){
            $paramsData = [
                "percentColor"
            ];
           
            $id = new MongoId((string)$pies['_id']);
            unset($pies["_id"]);
            foreach ($paramsData as $index => $param) {
                if(isset($pies[$param])){
                    if($index == 0){
                        $pies["css"]["percentColor"]["color"] = $pies[$param];
                    }
                    
                    unset($pies[$param]);
                }
            }
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$set' => $pies
            ]);
            $this->removeOldAttribute($id);
        }

        public function removeOldAttribute($id)
        {
            $unset = [
                "percentColor" => true,
            ];
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$unset' => $unset
            ]);
        }
    }
?>