<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use CacheHelper,PHDB,Authorisation,MongoId,Rest,Yii,Cms,Costum,Document,Log;


class DeletePageAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller=$this->getController();
        $costum = CacheHelper::getCostum();
        if ( Authorisation::isInterfaceAdmin() ) {
            if (isset($_POST["costumId"]) && isset($_POST["costumType"]) && isset($_POST["costumSlug"]) && isset($_POST["pages"])) {
                $costumId = $_POST["costumId"];
                $costumCollection = $_POST["costumType"];
                $idCmsList = array();
                $pathToDelete = [];
                $unsetPath = [];
                $return = array();
                foreach ($_POST["pages"] as $key => $appValue) {
                    $pageName = trim($appValue, "#");
                    $cmsList = Cms::getCmsByWhere(array("parent." . $costumId => array('$exists' => 1), "page" => $pageName));

                    //Remove image and document related
                    $docToDelete=PHDB::remove(Document::COLLECTION, array("id"=>array('$in'=>array_keys($cmsList))));
                    foreach($cmsList as $id => $value){
                        if(Yii::$app->fs->has(Yii::app()->params['uploadDir']."communecter/cms/".$id)){
                            Yii::$app->fs->deleteDir(Yii::app()->params['uploadDir']."communecter/cms/".$id);
                        }
                    }

                    //Remove page from costum 
                    $path = "costum.app.".$appValue;
                    $pathBtn = "";
                    $unsetPath[$path] = true;

                    /* Select costum data à jour après ajout page dans firstStepper */
                    $costumData = Costum::init(null, null, null, $_POST["costumSlug"], null, null, null, null, "co2/components/CommunecterController.php 0.1");
                    if (isset($costumData["htmlConstruct"])) {
                        $pathBtnValue = $costumData["htmlConstruct"];
                        $allpaths = $this->getPaths($pathBtnValue, $appValue);
                        if (sizeof($allpaths) > 0) {
                            foreach ($allpaths as $keyPath => $valuePath) {
                                array_push($pathToDelete, "htmlConstruct.".$valuePath);
                            }
                        }
                    }
                    $return = PHDB::update($costumCollection,["_id" => new MongoId($costumId)],['$unset' => $unsetPath]);

                    //Remove all CMS from page
                    $cmsRemoved = PHDB::remove("cms",array("parent." . $costumId => array('$exists' => 1), "page" => $pageName));
                }

                if(!isset($costum["resetCache"])){ 
                    $costum["resetCache"]=true;
                    CacheHelper::set($costum["contextSlug"], $costum);
                }
                Costum::forceCacheReset();
                // Save log
                Log::saveAndClean(
                    array(
                        "userId"         =>  Yii::app()->session["userId"],
                        "userName"       =>  Yii::app()->session["user"]["name"],
                        "costumId"       =>  @$_POST["costumId"],
                        "costumSlug"     =>  @$_POST["costumSlug"],
                        "costumType"     =>  @$_POST["costumType"],
                        "costumEditMode" =>  @$_POST["costumEditMode"],
                        "browser"        =>  @$_SERVER["HTTP_USER_AGENT"],
                        "ipAddress"      =>  @$_SERVER["REMOTE_ADDR"],
                        "created"        =>  time(),
                        "action"         => "costum/page",
                        "subAction"      => "DeletePage",
                        "page"         =>  @$_POST["pages"],
                        // "unsetPath"      => $unsetPath
                    )
                );



                return Rest::json(array("result" => true, "msg" => Yii::t("cooperation", "processing delete ok"), "deleted" => $return, "deleted cms list"=> $cmsList, "unsetPath" => $unsetPath, "pathToDelete" => $pathToDelete));
            }
        } else
            return Rest::json(array("result" => false, "msg"=> Yii::t("common", "You are not allowed to do this action")));
    }

    private function getPaths($object, $element, $path = '') {
        $results = array();
        foreach ($object as $key => $value) {
            if ($key === $element) {
                $results[] = $path . $key;
            } elseif (is_array($value)) {
                $results = array_merge($results, $this->getPaths($value, $element, $path . $key . '.'));
            }
        }
        return $results;
    }
}
