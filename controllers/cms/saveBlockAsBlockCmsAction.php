<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use DataValidator,Role,Cms,Rest,Yii;
class saveBlockAsBlockCmsAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){

        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) {
			$controller = $this->getController();
			$_POST = DataValidator::clearUserInput($_POST);
			
			$blockIdsToDuplicate = [$_POST["idCmsToDuplic"]];      
            $params  = array(
				"newId" => $_POST["newId"],
                "parentId"   => $_POST['costumId'], 
                "parentType" => $_POST["costumType"],
                "parentSlug" => $_POST['costumSlug'],
				"type" => $_POST['type']
			);

			
			$dataBlocCreated = Cms::duplicateBlock($blockIdsToDuplicate, $params, null,"cms","templates");

			return Rest::json(array("result"=>true ,"msg"=> Yii::t("common", "Success") )); 
    	}else {
			return Rest::json(array("result"=>false,"msg"=> Yii::t("common", "Error") )); 
		}
	}	
}