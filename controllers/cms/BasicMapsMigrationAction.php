<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms;
use MongoId;
use PHDB;
use Rest;
use Role;
use Yii;

class BasicMapsMigrationAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $basicmaps = PHDB::find(Cms::COLLECTION, array(
                "path" => "tpls.blockCms.map.basicmaps",
                "type" => array('$ne' => "blockCms"),
                "alreadyupdated" => array('$exists' => 0)
            ));
            $i = 0;
            foreach ($basicmaps as $key => $map) {
                $this->updateParams($map);
                $i++;
            }
            return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
        } else {
            echo "vous n'avez pas le droit"; 
        }
    }

    private function updateParams($maps){
        $paramsData = [
            "mapHeight",
            "filterHeaderColor",
            "btnAnnuaireText",
            "btnBackground",
            "btnBorderColor",
            "btnBorderRadius",
            "btnAnnuaireTextColor",
            "btnLink",
            "isBtnLbh",
            "legendShow",
            "legendConfig",
            "legendCible",
        ];
        $id = new MongoId((string)$maps['_id']);
        unset($maps["_id"]);
        foreach ($paramsData as $index => $param) {
            if(isset($maps[$param])){
                if($index == 0){
                    $maps["css"]["map"]["height"] = $maps[$param].'px';
                }
                if($index == 1){
                    $maps["styleFilter"]["headerColor"] = $maps[$param];
                }
                if($index == 2){
                    $maps["btnAnnuaire"]["text"] = $maps[$param];
                }
                if($index == 3){
                    $maps["css"]["styleAnnuaire"]["backgroundColor"] = $maps[$param];
                }
                if($index == 4){
                    $maps["css"]["styleAnnuaire"]["borderColor"] = $maps[$param];
                }
                if($index == 5){
                    $maps["css"]["styleAnnuaire"]["borderBottomLeftRadius"] = $maps[$param].'px';
                    $maps["css"]["styleAnnuaire"]["borderBottomRightRadius"] = $maps[$param].'px';
                    $maps["css"]["styleAnnuaire"]["borderTopLeftRadius"] = $maps[$param].'px';
                    $maps["css"]["styleAnnuaire"]["borderTopRightRadius"] = $maps[$param].'px';
                }
                if($index == 6){
                    $maps["css"]["styleAnnuaire"]["color"] = $maps[$param];
                }
                if($index == 7){
                    $maps["btnAnnuaire"]["link"] = $maps[$param];
                }
                if($index == 8){
                    $maps["btnAnnuaire"]["isLbh"] = $maps[$param];
                }
                if($index == 9){
                    $maps["legende"]["show"] = $maps[$param];
                }
                if($index == 10){
                    $maps["legende"]["config"] = $maps[$param];
                }
                if($index == 11){
                    $maps["legende"]["cible"] = $maps[$param];
                }
                unset($maps[$param]);
            }
        }
        $maps["alreadyupdated"] = 1;
        PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
            '$set' => $maps
        ]);
        $this->removeOldAttribute($id);
    }

    public function removeOldAttribute($maps)
    {
        $unset = [
            "mapHeight" => true,
            "filterHeaderColor" => true,
            "btnAnnuaireText" => true,
            "btnBackground" => true,
            "btnBorderColor" => true,
            "btnBorderRadius" => true,
            "legendShow" => true,
            "legendConfig" => true,
            "legendCible" => true,
            "btnAnnuaireTextColor" => true,
            "btnLink" => true,
            "isBtnLbh" => true,
            "blockCmsColorTitle1" => true,
            "blockCmsColorTitle2" => true,
            "blockCmsColorTitle3" => true,
            "blockCmsColorTitle4" => true,
            "blockCmsColorTitle5" => true,
            "blockCmsColorTitle6" => true,
            "blockCmsPoliceTitle1" => true,
            "blockCmsPoliceTitle2" => true,
            "blockCmsPoliceTitle3" => true,
            "blockCmsPoliceTitle4" => true,
            "blockCmsPoliceTitle5" => true,
            "blockCmsPoliceTitle6" => true,
            "blockCmsTextAlignTitle1" => true,
            "blockCmsTextAlignTitle2" => true,
            "blockCmsTextAlignTitle3" => true,
            "blockCmsTextAlignTitle4" => true,
            "blockCmsTextAlignTitle5" => true,
            "blockCmsTextAlignTitle6" => true,
            "blockCmsTextLineHeightTitle1" => true,
            "blockCmsTextLineHeightTitle2" => true,
            "blockCmsTextLineHeightTitle3" => true,
            "blockCmsTextLineHeightTitle4" => true,
            "blockCmsTextLineHeightTitle5" => true,
            "blockCmsTextLineHeightTitle6" => true,
            "blockCmsTextSizeTitle1" => true,
            "blockCmsTextSizeTitle2" => true,
            "blockCmsTextSizeTitle3" => true,
            "blockCmsTextSizeTitle4" => true,
            "blockCmsTextSizeTitle5" => true,
            "blockCmsTextSizeTitle6" => true,
            "blockCmsUnderlineColorTitle1" => true,
            "blockCmsUnderlineColorTitle2" => true,
            "blockCmsUnderlineColorTitle3" => true,
            "blockCmsUnderlineColorTitle4" => true,
            "blockCmsUnderlineColorTitle5" => true,
            "blockCmsUnderlineColorTitle6" => true,
            "blockCmsUnderlineHeightTitle1" => true,
            "blockCmsUnderlineHeightTitle2" => true,
            "blockCmsUnderlineHeightTitle3" => true,
            "blockCmsUnderlineHeightTitle4" => true,
            "blockCmsUnderlineHeightTitle5" => true,
            "blockCmsUnderlineHeightTitle6" => true,
            "blockCmsUnderlineMargeBottomTitle1" => true,
            "blockCmsUnderlineMargeBottomTitle2" => true,
            "blockCmsUnderlineMargeBottomTitle3" => true,
            "blockCmsUnderlineMargeBottomTitle4" => true,
            "blockCmsUnderlineMargeBottomTitle5" => true,
            "blockCmsUnderlineMargeBottomTitle6" => true,
            "blockCmsUnderlineSpaceTitle1" => true,
            "blockCmsUnderlineSpaceTitle2" => true,
            "blockCmsUnderlineSpaceTitle3" => true,
            "blockCmsUnderlineSpaceTitle4" => true,
            "blockCmsUnderlineSpaceTitle5" => true,
            "blockCmsUnderlineSpaceTitle6" => true,
            "blockCmsUnderlineTitle1" => true,
            "blockCmsUnderlineTitle2" => true,
            "blockCmsUnderlineTitle3" => true,
            "blockCmsUnderlineTitle4" => true,
            "blockCmsUnderlineTitle5" => true,
            "blockCmsUnderlineTitle6" => true,
            "blockCmsUnderlineWidthTitle1" => true,
            "blockCmsUnderlineWidthTitle2" => true,
            "blockCmsUnderlineWidthTitle3" => true,
            "blockCmsUnderlineWidthTitle4" => true,
            "blockCmsUnderlineWidthTitle5" => true,
            "blockCmsUnderlineWidthTitle6" => true
        ];
        PHDB::update(Cms::COLLECTION,[ "_id" => $maps ],[
            '$unset' => $unset
        ]);
    }
}
