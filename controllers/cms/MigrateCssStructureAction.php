<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use MongoId, MongoRegex;
use PHDB,Cms,Element,Yii,Document,Project,Role;

class MigrateCssStructureAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($index=0, $limit=50) {
    

        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){  
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 1000);
            $allBlock = PHDB::findAndLimitAndIndex(Cms::COLLECTION, array(
                "css" => array('$exists'=>1),
                "path" => new MongoRegex("/tpls.blockCms.superCms/i"),
                "alreadyUpdated" => array('$exists'=> false),
            ), $limit, $index);
            $this->migrateCss($allBlock);
        } else {
            echo "vous n'avez pas le droit"; 
        }
            
    }

    private function migrateCss($allBlock){

        
            $output = ""; 
            
                    $i = 0;
                    $output.= count($allBlock) ." block à mettre à jour ===>";
                    if(count($allBlock) != 0){
                    foreach($allBlock as $kBlock => $vBlock){

                            $arrayParams = [];
                            
                            $arrayAdvanced = ["css","class"];

                            foreach ($arrayAdvanced as $vAdvanced){
                                if(isset($vBlock[$vAdvanced]["other"])){
                                    $newname = "other".ucfirst($vAdvanced);
                                    $arrayParams["advanced"][$newname] = $vBlock[$vAdvanced]["other"];                    
                                }
                            }

                            // position 

                            if (isset($vBlock["path"]) && $vBlock["path"] != "tpls.blockCms.superCms.elements.lineSeparator"){
                                if ( isset($vBlock["css"]["position"])){
                                    foreach ($vBlock["css"]["position"] as $keyPosition => $valPosition){
                                        $uc_name = ucfirst($keyPosition);
                                        $new_name = "position".$uc_name;
                                        $arrayParams["css"][$new_name] = $valPosition;
                                    }
                                }
                            }

                            // object fit
                            if(isset($vBlock["css"]["object-fit"])){
                                $arrayParams["css"]["objectFit"] = $vBlock["css"]["object-fit"];                    
                            }

                            // size 
                            $arraySize = ["width","height"];
                            foreach ($arraySize as $vSize){
                                if(isset($vBlock["css"]["size"][$vSize])){
                                    if (str_contains($vBlock["css"]["size"][$vSize], '%') || str_contains($vBlock["css"]["size"][$vSize], 'px') || $vBlock["css"]["size"][$vSize] == "auto"){
                                        $arrayParams["css"][$vSize] = $vBlock["css"]["size"][$vSize]; 
                                    } else {
                                        $arrayParams["css"][$vSize] = $vBlock["css"]["size"][$vSize]."px"; 
                                    }                 
                                }
                            }

                            //background
                            if(isset($vBlock["css"]["background"])){
                                foreach ($vBlock["css"]["background"] as $key => $val){
                                    $uc_name = ucfirst($key);
                                    $new_name = "background".$uc_name;
                                    $arrayParams["css"][$new_name] = $val;
                                }                  
                            } 

                            //Animation
                            if(isset($vBlock["css"]["aos"])){
                                foreach ($vBlock["css"]["aos"] as $key => $val){
                                    $uc_name = ucfirst($key);
                                    $new_name = "animation".$uc_name;
                                    $arrayParams["css"][$new_name] = $val;
                                }                  
                            } 

                            //Border
                            if(isset($vBlock["css"]["border"])){
                                foreach ($vBlock["css"]["border"] as $key => $val){
                                    if ($key != "radius") {
                                        $uc_name = ucfirst($key);
                                        if ($key == "color" || $key == "width" ){
                                            $new_name = "border".$uc_name;
                                        }else if ($key == "type"){
                                            $new_name = "borderStyle";
                                        } else {
                                            $new_name = "border".$uc_name."Width";
                                        }
                                        
                                        if ( $key == "top" || $key == "left" || $key == "bottom" || $key == "right" || $key == "width" ){
                                            if (str_contains($val, '%') || str_contains($val, 'px')){
                                                $arrayParams["css"][$new_name] = $val;
                                            } else {
                                                $arrayParams["css"][$new_name] = $val."px";
                                            }
                                        } else {
                                            $arrayParams["css"][$new_name] = $val;
                                        }
                                    } else if ($key == "radius") {
                                        foreach ($vBlock["css"]["border"][$key] as $keyRadius => $valRadius){
                                            $uc_name =  ucfirst($keyRadius);
                                            $uc_name =  ucwords($uc_name, '-');
                                            $new_name = str_replace( '-', "", "border".$uc_name."Radius" );
                                            if (str_contains($valRadius, '%') || str_contains($valRadius, 'px')){
                                                $arrayParams["css"][$new_name] = $valRadius;
                                            } else {
                                                if ($vBlock["path"] == "tpls.blockCms.superCms.container"){    
                                                    $arrayParams["css"][$new_name] = $valRadius."%";
                                                } else {  
                                                    $arrayParams["css"][$new_name] = $valRadius."px";
                                                }
                                            }
                                        } 
                                    }
                                }                   
                            } 

                            //Padding 
                            if(isset($vBlock["css"]["padding"])){
                                foreach ($vBlock["css"]["padding"] as $key => $val){
                                    if (!str_contains($key , '-')){
                                        $uc_name = ucfirst($key);
                                        $new_name = "padding".$uc_name;
                                        if (str_contains($val, '%') || str_contains($val, 'px')){
                                            $arrayParams["css"][$new_name] = $val;
                                        } else {
                                            if ($vBlock["path"] == "tpls.blockCms.superCms.container" ){    
                                                $arrayParams["css"][$new_name] = $val."%";
                                            } else {
                                                $arrayParams["css"][$new_name] = $val."px";
                                            }
                                        }
                                    }
                                }                      
                            } 

                            //Margin 
                            if(isset($vBlock["css"]["margin"])){
                                foreach ($vBlock["css"]["margin"] as $key => $val){
                                    if (!str_contains($key , '-')){
                                        $uc_name = ucfirst($key);
                                        $new_name = "margin".$uc_name;
                                        if (str_contains($val, '%') || str_contains($val, 'px')){
                                            $arrayParams["css"][$new_name] = $val;
                                        } else {
                                            if ($vBlock["path"] == "tpls.blockCms.superCms.container"){    
                                                $arrayParams["css"][$new_name] = $val."%";
                                            } else {  
                                                $arrayParams["css"][$new_name] = $val."px";
                                            }
                                        }
                                    }
                                }     
                            } 

                            // shadow
                            if(isset($vBlock["css"]["box-shadow"])){
                                $box  = [];
                                foreach ($vBlock["css"]["box-shadow"] as $key => $val){
                                    if ( $key == "x" || $key == "y" || $key == "blur" || $key == "spread"){
                                        if (str_contains($val, '%') || str_contains($val, 'px')){
                                            $box[$key] = $val;
                                        } else {
                                            $box[$key] = $val."px";
                                        }
                                    } else {
                                        $box[$key] = $val;
                                    }
                                }       
                                $arrayParams["css"]["boxShadow"] = @$box["x"]." ".@$box["y"]." ".@$box["blur"]." ".@$box["spread"]." ".@$box["color"]." ".@$box["inset"];            
                            }
                            
                            // Color and Fontsize (button)
                            if (isset($vBlock["path"]) && $vBlock["path"] == "tpls.blockCms.superCms.elements.button"){
                                if(isset($vBlock["css"]["color"])){
                                    $arrayParams["css"]["color"] = $vBlock["css"]["color"];                    
                                }
                                if(isset($vBlock["css"]["font-size"])){
                                    if (str_contains($vBlock["css"]["font-size"], '%') || str_contains($vBlock["css"]["font-size"], 'px') ||  $vBlock["css"]["font-size"] == "auto"){
                                        $arrayParams["css"]["fontSize"] = $vBlock["css"]["font-size"];  
                                    } else {
                                        $arrayParams["css"]["fontSize"] = $vBlock["css"]["font-size"]."px";  
                                    }                 
                                }
                               

                            }

                            //Hover 
                            
                            // Color and Fontsize 
                            if (isset($vBlock["path"]) && $vBlock["path"] == "tpls.blockCms.superCms.elements.button"){
                                if(isset($vBlock["css"]["hover"]["color"])){
                                    $arrayParams["css"]["hover"]["color"] = $vBlock["css"]["hover"]["color"];                    
                                }
                                if(isset($vBlock["css"]["hover"]["font-size"])){
                                    if (str_contains($vBlock["css"]["hover"]["font-size"], '%') || str_contains($vBlock["css"]["hover"]["font-size"], 'px') || $vBlock["css"]["hover"]["font-size"] == "auto"){
                                        $arrayParams["css"]["hover"]["fontSize"] = $vBlock["css"]["hover"]["font-size"];  
                                    } else {
                                        $arrayParams["css"]["hover"]["fontSize"] = $vBlock["css"]["hover"]["font-size"]."px";  
                                    }                 
                                }
                            }

                            //background hover
                            if(isset($vBlock["css"]["hover"]["background"])){
                                if(isset($vBlock["css"]["hover"]["background"])){
                                    foreach ($vBlock["css"]["hover"]["background"] as $key => $val){
                                        $uc_name = ucfirst($key);
                                        $new_name = "background".$uc_name;
                                        $arrayParams["css"]["hover"][$new_name] = $val;
                                    }                  
                                }                    
                            } 

                            //Padding hover
                            if(isset($vBlock["css"]["hover"]["padding"])){
                                if(isset($vBlock["css"]["hover"]["padding"])){
                                    foreach ($vBlock["css"]["hover"]["padding"] as $key => $val){
                                        if (!str_contains($key , '-')){ 
                                            $uc_name = ucfirst($key);
                                            $new_name = "padding".$uc_name;
                                            if (str_contains($val, '%') || str_contains($val, 'px')){
                                                $arrayParams["css"]["hover"][$new_name]  = $val;
                                            } else {
                                                if ($vBlock["path"] == "tpls.blockCms.superCms.container" ){    
                                                    $arrayParams["css"]["hover"][$new_name]  = $val."%";
                                                } else {  
                                                    $arrayParams["css"]["hover"][$new_name]  = $val."px";
                                                }
                                            }
                                        }
                                    }                  
                                }                                
                            } 

                            //Margin hover
                            if(isset($vBlock["css"]["hover"]["margin"])){
                                if(isset($vBlock["css"]["hover"]["margin"])){
                                    foreach ($vBlock["css"]["hover"]["margin"] as $key => $val){
                                        if (!str_contains($key , '-')){ 
                                            $uc_name = ucfirst($key);
                                            $new_name = "margin".$uc_name;
                                            if (str_contains($val, '%') || str_contains($val, 'px')){
                                                $arrayParams["css"]["hover"][$new_name]  = $val;
                                            } else {
                                                if ($vBlock["path"] == "tpls.blockCms.superCms.container" ){    
                                                    $arrayParams["css"]["hover"][$new_name]  = $val."%";
                                                } else {  
                                                    $arrayParams["css"]["hover"][$new_name]  = $val."px";
                                                }
                                            }
                                        }
                                    }                  
                                }                               
                            } 

                            // shadow hover
                            if(isset($vBlock["css"]["hover"]["box-shadow"])){
                                if(isset($vBlock["css"]["hover"]["box-shadow"])){
                                    foreach ($vBlock["css"]["hover"]["box-shadow"] as $key => $val){
                                        $uc_name = ucfirst($key);
                                        $new_name = "boxShadow".$uc_name;
                                        if ( $key == "x" || $key == "y" || $key == "blur" || $key == "spread"){
                                            if (str_contains($val, '%') || str_contains($val, 'px')){
                                                $arrayParams["css"]["hover"][$new_name] = $val;
                                            } else {
                                                $arrayParams["css"]["hover"][$new_name] = $val."px";
                                            }
                                        } else {
                                            $arrayParams["css"]["hover"][$new_name] = $val;
                                        }
                                    }                  
                                }                                 
                            }

                            //Border hover
                            if(isset($vBlock["css"]["hover"]["border"])){
                                foreach ($vBlock["css"]["hover"]["border"] as $key => $val){
                                    if ($key != "radius") {
                                        $uc_name = ucfirst($key);
                                        if ($key == "color" || $key == "width" ){
                                            $new_name = "border".$uc_name;
                                        }else if ($key == "type"){
                                            $new_name = "borderStyle";
                                        } else {
                                            $new_name = "border".$uc_name."Width";
                                        }
                                        
                                        if ( $key == "top" || $key == "left" || $key == "bottom" || $key == "right" || $key == "width" ){
                                            if (str_contains($val, '%') || str_contains($val, 'px')){
                                                $arrayParams["css"]["hover"][$new_name] = $val;
                                            } else {
                                                $arrayParams["css"]["hover"][$new_name] = $val."px";
                                            }
                                        } else {
                                            $arrayParams["css"]["hover"][$new_name] = $val;
                                        }
                                    } else if ($key == "radius") {
                                        foreach ($vBlock["css"]["hover"]["border"][$key] as $keyRadius => $valRadius){
                                            $uc_name =  ucfirst($keyRadius);
                                            $uc_name =  ucwords($uc_name, '-');
                                            $new_name = str_replace( '-', "", "border".$uc_name."Radius" );
                                            if (str_contains($valRadius, '%') || str_contains($valRadius, 'px')){
                                                $arrayParams["css"]["hover"][$new_name] = $valRadius;
                                            } else {
                                                if ($vBlock["path"] == "tpls.blockCms.superCms.container"){
                                                    $arrayParams["css"]["hover"][$new_name] = $valRadius."%";
                                                } else {
                                                    $arrayParams["css"]["hover"][$new_name] = $valRadius."px";
                                                }
                                            }
                                        } 
                                    }
                                }                   
                            } 

                            // End Hover 

                            //Alignement 
                            if (isset($vBlock["path"]) && $vBlock["path"] == "tpls.blockCms.superCms.container"){
                                
                                $keyToTransform = ["flex-direction","flex-wrap","align-content","justify-content","align-items","align-self","vertical-align"];

                                foreach ($keyToTransform as $value ){
                                    if(isset($vBlock["css"][$value])){
                                        $name =  ucwords($value, '-');
                                        $new_name = str_replace( '-', "", $name);
                                        $new_name = lcfirst($new_name);
                                        $arrayParams["css"][$new_name] = $vBlock["css"][$value];
                                    }
                                }
                            }

                            //line separator
                            if(isset($vBlock["css"]["lineSeparator"])){
                                $arrayParams["css"]["lineSeparator"] = $vBlock["css"]["lineSeparator"];                    
                            }

                            
                            //icon (line separator)
                            if (isset($vBlock["path"]) && ( $vBlock["path"] == "tpls.blockCms.superCms.elements.lineSeparator" || $vBlock["path"] == "tpls.blockCms.superCms.elements.icon" )){
                                
                                if(isset($vBlock["css"]["position"]["top"])){
                                    $arrayParams['positionTop'] = $vBlock["css"]["position"]["top"];                    
                                }

                                if( isset($vBlock["css"]["iconDisplay"]) && $vBlock["css"]["iconDisplay"] == true ){
                                        $arrayParams["css"]["icon"]['display'] = "block";         
                                } else {
                                    $arrayParams["css"]["icon"]['display'] = "none"; 
                                }

                                if(isset($vBlock["css"]["icon"]["style"])){
                                        $arrayParams['iconStyle'] = "fa fa-".$vBlock["css"]["icon"]["style"];  
                                }

                                $arrayParamsToSet = [];
                                if(isset($vBlock["css"]["icon"])){
                                    foreach($vBlock["css"]["icon"] as $key => $value){
                                        if ($key == "style" || $key == "color"){
                                            $arrayParamsToSet[$key] = $value;
                                        } else if ($key == "size") {
                                            $arrayParamsToSet['fontSize'] = $value."px"; 
                                        } else if ($key == "radius") {
                                            $arrayParamsToSet['borderRadius'] = $value."px"; 
                                        } else if ($key == "background") {
                                            foreach($vBlock["css"]["icon"][$key] as $keyBg => $valueBg){
                                                $uckeyBg = ucfirst($keyBg);
                                                $new_name = $key.$uckeyBg;
                                                $arrayParamsToSet[$new_name] = $valueBg;
                                            }
                                        } else if ($key == "position") {
                                            foreach ($vBlock["css"]["icon"][$key] as $keyPosition => $valPosition){
                                                $uc_name = ucfirst($keyPosition);
                                                $new_name = "position".$uc_name;
                                                $arrayParamsToSet[$new_name] = $valPosition;
                                            }   
                                        } else if ($key == "margin") {
                                            foreach ($vBlock["css"]["icon"][$key] as $keyMargin => $valMargin){
                                                $uc_name = ucfirst($keyMargin);
                                                $new_name = "margin".$uc_name;
                                                if (str_contains($valMargin, '%') || str_contains($valMargin, 'px')){
                                                    $arrayParamsToSet[$new_name] = $valMargin;
                                                } else {
                                                    $arrayParamsToSet[$new_name] = $valMargin."px";
                                                } 
                                            } 
                                        } else if ($key == "padding"){
                                            foreach ($vBlock["css"]["icon"][$key] as $keyPadding => $valPadding){
                                                $uc_name = ucfirst($keyPadding);
                                                $new_name = "padding".$uc_name;
                                                if (str_contains($valPadding, '%') || str_contains($valPadding, 'px')){
                                                    $arrayParamsToSet[$new_name] = $valPadding;
                                                } else {
                                                    $arrayParamsToSet[$new_name] = $valPadding."px";
                                                }
                                            }   
                                        } else {
                                            $arrayParamsToSet[$key] = $value;
                                        }
                                    }
                                }
 

                                if (isset($vBlock["path"]) && ( $vBlock["path"] == "tpls.blockCms.superCms.elements.lineSeparator") ){
                                    if (isset($arrayParams["css"]["icon"])){
                                        $arrayParams["css"]["icon"] = array_merge($arrayParams["css"]["icon"],$arrayParamsToSet);
                                    } else {
                                        $arrayParams["css"]["icon"] = $arrayParamsToSet ;
                                    }
                                } else if (isset($vBlock["path"]) && ( $vBlock["path"] == "tpls.blockCms.superCms.elements.icon") ) {
                                    if (isset($arrayParams["css"])){
                                        $arrayParams["css"] = array_merge($arrayParams["css"],$arrayParamsToSet);
                                    } else {
                                        $arrayParams["css"]["icon"] = $arrayParamsToSet ;
                                    }
                                }


                            }



                            if(!empty($arrayParams)){
                                $arrayParams['alreadyUpdated'] = true ;
                                PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($kBlock) ],[ 
                                    '$set'=> $arrayParams
                                ]);  
                                $i++;   
                            } 
                            
                        }
                        $output .= $i." blocs à jour <br>";  
            }

            echo $output;
    }
}   
