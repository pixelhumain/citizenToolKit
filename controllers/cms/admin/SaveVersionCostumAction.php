<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use CAction, Cms,PHDB, Authorisation, Rest,CacheHelper, DataValidator, Yii;


class SaveVersionCostumAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){
            // Save COSTUM's Data & config 
            $timestamp    = time();  
            $costumToSave = PHDB::findOneById($_POST["dataVersion"]["contextType"], $_POST["dataVersion"]["contextId"]);
            if ($_POST["dataVersion"]["versionDesc"] != "") {
                $costumToSave["data"]["versionDesciption"] = $_POST["dataVersion"]["versionDesc"];
            }
            $costumToSave["data"]["versionName"] = $_POST["dataVersion"]["versionName"];
            $costumToSave["data"]["costumId"]    = $_POST["dataVersion"]["contextId"];
            $costumToSave["data"]["collection"]  = $costumToSave["collection"];
            $costumToSave["data"]["costum"]      = $costumToSave["costum"];
            // if (isset($costumToSave["costum"]["version"])) {
            //       unset($costumToSave["data"]["costum"]["version"]);
            // }
            // use timestamp to reference version (RestoreCostumAction.php)
            $costumToSave["data"]["version"] = $timestamp;

            $cmsToSave = Cms::getCmsByWhere(["parent.".$_POST["dataVersion"]["contextId"] => array('$exists'=>1),"path" => array('$exists'=>1),"page" => array('$exists'=>1)]);
            foreach ($cmsToSave as $idCms => $vCms) {
                $cmsToSave[$idCms]["version"]  = $timestamp;
                $cmsToSave[$idCms]["originId"] = $idCms;
                unset($cmsToSave[$idCms]["_id"]);
            }

            PHDB::insert("versionning",$costumToSave["data"]);
            $res=array("result"=>true, "msg"=> Yii::t("common", "Success"));
            PHDB::batchInsert("versionning",$cmsToSave);  

        }
        return Rest::json($res);
    	
    }
	
}
