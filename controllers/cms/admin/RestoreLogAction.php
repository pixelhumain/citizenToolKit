<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use PHDB, Authorisation, DataValidator, Yii, Log, MongoId, Rest;


class RestoreLogAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res = array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){
            $savedLog  = PHDB::findOneById(Log::COLLECTION, $_POST["idLog"]);
            //var_dump($savedLog["olderData"]);
            if (isset($savedLog[costumType])) {
                $collection = $savedLog[costumType];
            }
            if (isset($savedLog["pathChanged"])) {
                foreach ($savedLog["pathChanged"] as $key => $value) {
                    $dataValue = $this->getValueByPath($savedLog["olderData"], $value);
                    var_dump($dataValue);
                    PHDB::update($collection,
                        array("_id" => new MongoId((string)$savedLog["costumId"])),
                        array('$set' => array("costum.".$value => $dataValue))
                    );
                    $res = array("result"=>false, "msg"=> "version Bien restauré");
                }
            }
        }
        return Rest::json($res);
    }

    function getValueByPath($data, $keys) {
        $keys = explode('.', $keys);
        $current = $data;
        foreach ($keys as $key) {
            if (!isset($current[$key])) {
                return null;
            }
            $current = $current[$key];
        }
        return $current;
    }

}
