<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use PHDB, Authorisation, Rest, DataValidator, Yii;


class DeleteSavedCostumAction extends \PixelHumain\PixelHumain\components\Action {

    public function run() {
        $controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res   = array("result"=>false, "msg"=> Yii::t("common", "Access denied")); 

        if(Authorisation::isInterfaceAdmin()){

            $res = PHDB::remove("versionning", ["version" => (int)$_POST["version"]]);

        }
        
        return Rest::json($res);

    }

}
