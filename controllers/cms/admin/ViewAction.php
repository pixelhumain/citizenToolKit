<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use CAction;
use Cms;
use Rest;
use Authorisation;

class ViewAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
    	if(Authorisation::isCostumAdmin())
    		return $controller->renderPartial("costum.views.cmsBuilder.costumizer.".$_POST["view"], array());
    	else
    		return $controller->renderPartial("co2.views.default.unauthorized.", array());
    		
    }
}
