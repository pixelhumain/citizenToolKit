<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use PHDB, Authorisation, Rest,CacheHelper, DataValidator, Yii;


class GetVersionCostumAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res = array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){
            
        	$costum = CacheHelper::getCostum();
            $costumVersions = PHDB::find("versionning", ['costumId' => $costum["contextId"],"costum"=>array('$exists'=>true)],array("versionName","versionDesciption","version","costumId"));

            $res = array("result"=>true, "items" => $costumVersions);
        }
        return Rest::json($res);
    }

}
