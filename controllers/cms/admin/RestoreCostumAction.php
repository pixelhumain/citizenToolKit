<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use CAction, Cms,PHDB, Authorisation, Rest,CacheHelper, DataValidator, Yii,MongoId;


class RestoreCostumAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res   = array("result"=>false, "msg"=> Yii::t("common", "Access denied"));

        if(Authorisation::isInterfaceAdmin()){

            // Restore costum
            $savedCostum  = PHDB::findOneById("versionning", $_POST["versionData"]["versionId"]);
            
            // var_dump($savedCostum);
            // Insert version timestemp into COSTUM to refer version (to check if running costum is already backed up). 
            // $savedCostum["costum"]["version"] = $savedCostum["version"];
            PHDB::update($savedCostum["collection"], array("_id" => new MongoId($savedCostum["costumId"])), array('$set' => array("costum" => $savedCostum["costum"])));

            // Restore CMS
            $savedCms = PHDB::find("versionning", ["version" => $savedCostum["version"],"collection" => "cms"]);
            
            // Keep CMS in case $savedCms is empty
            if (!empty($savedCms)) {
                $res = PHDB::remove(Cms::COLLECTION, ["parent.".$savedCostum["costumId"] => array('$exists'=>1),"path" => array('$exists'=>1),"page" => array('$exists'=>1)]);
                foreach ($savedCms as $key => $value) {
                    $savedCms[$key]["_id"] = new MongoId($value["originId"]);
                    unset($savedCms[$key]["originId"]);
                    unset($savedCms[$key]["version"]);
                }
                PHDB::batchInsert(Cms::COLLECTION,$savedCms);  
            }
            
            CacheHelper::delete($_POST["versionData"]["contextSlug"]);

            $res=array("result"=>true, "msg"=> Yii::t("common", "Success"));

        }
        return Rest::json($res);
    	
    }
	
}
