<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use CAction;
use Rest;
use Document;


class GetImageDocumentAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run() {
        $controller=$this->getController();

        $data = array(
            "id"=> $_POST['id'],
            "type"=>$_POST['type'],
            "subKey"=>$_POST['subkey'],
        );

        if($_POST['subkey'] == null){
            $data = array(
                "id"=> $_POST['id'],
                "type"=>$_POST['type'],
                "contentKey"=>$_POST['contentKey'],
            );
        }
        
        $initImage = Document::getListDocumentsWhere($data , "image"); 
        return Rest::json($initImage);
    }
}