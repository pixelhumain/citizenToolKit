<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use CAction, Cms, Authorisation, Element, Rest,CacheHelper,Costum, DataValidator, Log, Yii,MongoDate;

class UpdateCostumAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        // Fonction pour trouver la différence entre deux tableaux associatifs
        function array_diff_recursive($array1, $array2) {
            $diff = array();
            foreach($array1 as $key => $value) {
                if (is_array($value)) {
                    if (!isset($array2[$key]) || !is_array($array2[$key])) {
                        $diff[$key] = $value;
                    } else {
                        $recursive_diff = array_diff_recursive($value, $array2[$key]);
                        if (count($recursive_diff)) {
                            $diff[$key] = $recursive_diff;
                        }
                    }
                } else {
                    if (!array_key_exists($key, $array2) || $array2[$key] !== $value) {
                        $diff[$key] = $value;
                    }
                }
            }
            return $diff;
        }
        $_POST = DataValidator::clearUserInput($_POST);
        if(Authorisation::isInterfaceAdmin()){
            if(isset($_POST["params"])){
                $_POST["params"] = DataValidator::clearUserInput($_POST["params"]);
                $res = Costum::update(DataValidator::convertStringToBoolean($_POST["params"]));

                //Log::save(array("userId" => Yii::app()->session["userId"], "browser" => @$_SERVER["HTTP_USER_AGENT"], "ipAddress" => @$_SERVER["REMOTE_ADDR"], "created" => new MongoDate(time()), "action" => "costum/update", "params" => array("dataUpdate"=>json_encode($_POST["params"])), "contextSlug"=> @$_POST["costumSlug"], "contextId"=> @$_POST["costumId"], "costumType"=> @$_POST["contextType"]));

                $action = (isset($_POST["action"])) ? $_POST["action"] : "costum/config";
                $page  = (isset($_POST["page"])) ? $_POST["page"] : "";
                $keyPage  = (isset($_POST["keyPage"])) ? $_POST["keyPage"] : "";


                $restructuredObj = DataValidator::restructureArray($_POST["params"]);
                $restructuredOlderData = DataValidator::restructureArray($_POST["olderData"]);

                Log::saveAndClean(
                    array(
                        "userId"         =>  Yii::app()->session["userId"],
                        "userName"       =>  Yii::app()->session["user"]["name"],
                        "costumId"       =>  @$_POST["costumId"],
                        "costumSlug"     =>  @$_POST["costumSlug"],
                        "costumType"     =>  @$_POST["costumType"],
                        "costumEditMode" =>  @$_POST["costumEditMode"],
                        "browser"        =>  @$_SERVER["HTTP_USER_AGENT"],
                        "ipAddress"      =>  @$_SERVER["REMOTE_ADDR"],
                        "created"        =>  time(),
                        "action"         =>  $action,
                        "olderData"      =>  $restructuredOlderData,
                        "subAction"      =>  @$_POST["subAction"],
                        "params"         =>  $restructuredObj,
                        "pathChanged"    => @$_POST["pathChanged"],
                        "page"           => $page,
                        "keyPage"        => $keyPage
                    )
                );

                if($res===true)
                    $res=array("result"=>true, "msg"=>"all is every well", "params"=>$_POST["params"]);
            }
            else
                $res=array("result"=>false, "msg"=> "empty params to update");
        }else
            $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        return Rest::json($res);
    	
    }
	
}
