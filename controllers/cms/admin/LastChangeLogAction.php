<?php


namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use PHDB, Authorisation, DataValidator, Yii, Log, MongoId, Rest;

class LastChangeLogAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res = array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){
            $query = array("costumSlug" => $_POST["costumSlug"]);
            $lastModif = PHDB::findAndSort(Log::COLLECTION,
                $query , array("_id"=>-1) , 30);
            $lastAdmin = PHDB::distinct(Log::COLLECTION,"userName",$query);
            $items = [];
            $items["lastModif"] = $lastModif;
            $items["lastAdmin"] = $lastAdmin;
            $res = array("result"=>true, "items" => $items);
        }
        return Rest::json($res);
    }
}