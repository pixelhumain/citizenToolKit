<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use CAction, Log,PHDB, Authorisation, Rest,CacheHelper, DataValidator, Yii;


class SaveLogsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){
            Log::save(
                array(
                    "userId"         =>  Yii::app()->session["userId"],
                    "userName"       =>  Yii::app()->session["user"]["name"],
                    "costumId"       =>  $_POST["costumId"],
                    "costumSlug"     =>  $_POST["costumSlug"],
                    "costumEditMode" =>  $_POST["costumEditMode"],
                    "action"         => "costum/editBlock",
                    "value"          => $_POST["value"],
                    "path"           => $_POST["path"],
                    "idBlock"        => $_POST["cmsId"],
                    "blockPath"      => $_POST["blockPath"],
                    "blockName"      => $_POST["blockName"],
                    "collection"     => $_POST["collection"],
                    "page"           => $_POST["page"],
                    "created"        =>  time()
                )
            );

        }
    }
}