<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use Cms, Element, Rest,CacheHelper,PHDB, Authorisation, DataValidator;


class UpdateValuePageCmsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        $costum = CacheHelper::getCostum();
        $_POST = DataValidator::clearUserInput($_POST);
        if(isset($_POST["params"]) && Authorisation::isInterfaceAdmin()){
            $condition = array(
                "parent.".$costum["contextId"] => array('$exists'=>1),
                "page" => $_POST["params"]["keyApp"],
              );
            $res =PHDB::updateWithOptions(Cms::COLLECTION,
                            $condition,
			 				array( '$set' => array("page" => $_POST["params"]["newKey"])),
                            array('multiple' => true)); 
            //$allCms = Cms::getCmsByWhere($condition);

            /*foreach (array_keys($allCms) as $key => $value) {          
                Element:: updatepathvalue(Cms::COLLECTION,$value,"page", $_POST["params"]["newKey"]);
            } */
            return Rest::json(array("result"=>true));
        }else
            return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));  
    }
}
