<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use Authorisation, Rest,Costum;


class ResetCacheAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        if(Authorisation::isInterfaceAdmin()){
            Costum::forceCacheReset();
            $res=array("result"=>false, "msg"=> "Cache updated");
        }else
        $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));

        return Rest::json($res);

    }
	
}
