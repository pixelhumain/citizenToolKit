<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use Authorisation, Rest, Costum;
use DataValidator;
use Yii;

class UpdateFontsCacheAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        if(Authorisation::isInterfaceAdmin() && isset($_POST["fonts"])){
            Costum::updateFontsCache($_POST["fonts"]);
            $res=array("result"=>true, "msg"=> "Fonts cache updated");
        } else
            $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        return Rest::json($res);

    }
	
}