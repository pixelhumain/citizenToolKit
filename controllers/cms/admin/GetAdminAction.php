<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use PHDB,MongoId,Authorisation, Rest,CacheHelper,Person,Yii;


class GetAdminAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));

        if(Authorisation::isInterfaceAdmin()){
            $costum = PHDB::findOne($_POST['contextType'], array('_id' => new MongoId($_POST['contextId'])));
            $actions = PHDB::find("countUserActions",array("costumId" => $_POST['contextId']),["action","userId"] );
            $infoAdmin = [];
            $array_community = [];
            foreach ($costum["links"] as $keyC => $community){
                foreach($community as $key => $com){
                    $array_community[$key] = $com;
                }
            }




            foreach ($array_community as $memberId => $member) {
                if (isset($member['isAdmin']) && $member['isAdmin'] === true) {
                    $info = PHDB::findOne("citoyens", array('_id' => new MongoId($memberId)),["name","profilImageUrl","slug","roles","username"]);
                    if (isset($costum["creator"]) && $costum["creator"] == $memberId) {
                        $info["name"] = $info["name"]." (Créateur)";
                    }
                    $infoAdmin[$memberId] = $info;
                }
            }

            $superAdminInfo = PHDB::find("citoyens", array('$or' => array(
                ['roles.superAdmin' => true],
                ['roles.superAdminCms' => true]
            )),["name","profilImageUrl","slug","roles","username"]);
            // $mergedAdminInfo = array_merge($superAdminInfo, $infoAdmin);
            
            foreach ($superAdminInfo as $key => $value) {
                if (isset($array1[$key])) {
        // If the key already exists in $array1, merge the inner arrays
                    $infoAdmin[$key] = array_merge($array1[$key], $value);
                } else {
        // If the key doesn't exist in $array1, simply add it
                    $infoAdmin[$key] = $value;
                }
            }
            foreach ($actions as $key => $value) {
                if (isset($infoAdmin[$value["userId"]])){
                    $infoAdmin[$value["userId"]]["action"] = $value["action"];
                }
            }

            // echo "<pre>";
            // var_dump($superAdminInfo);
            // $infoAdmin = array_unique($mergedAdminInfo);
            // var_dump(array_unique(array_values($mergedAdminInfo)));

            $res = array("result"=>true, "admins" => $infoAdmin);
        }
        return Rest::json($res);
    	
    }
	
}
