<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use PHDB, Authorisation, Rest,CacheHelper, DataValidator, Yii, Log;


class GetLogsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res = array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){
            $costumLog = PHDB::findAndSort(Log::COLLECTION,
                array("costumId" => $_POST["costumId"],"idBlock" => $_POST["idBlock"]), array("created"=>-1) );
            $res = array("result"=>true, "items" => $costumLog);
        }
        return Rest::json($res);
    }

}
