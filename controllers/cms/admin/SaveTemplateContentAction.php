<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin;

use CAction, Cms,PHDB, Authorisation, Rest,CacheHelper, DataValidator, Yii,MongoId,Costum;


class SaveTemplateContentAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        if(Authorisation::isInterfaceAdmin()){
            // Get and save from CMS to Templates collection
            // echo "<pre>";
            // var_dump($_POST);exit();
            // var_dump($_POST);
        	$allCms = [];
        	$where=array(
        		"parent.".$_POST["paramsTpl"]["contextId"] => array('$exists'=>1),
        		"path" => array('$exists'=>1)
        	);

            $newApp = [];
            $costum = PHDB::findOneById($_POST["paramsTpl"]["contextType"],$_POST["paramsTpl"]["contextId"] );
        	if(!empty($_POST["paramsTpl"]["page"])){
        		$where["page"] = $_POST["paramsTpl"]["page"];
                //Add attr "templateOrigin" in the costum app (it indicates the template origin of this page)
                $costum['costum']["app"]["#".$where["page"]]["templateOrigin"] = $_POST["paramsTpl"]["tplId"];
                $newApp["costum.app."."#".$where["page"]] = $costum['costum']["app"]["#".$where["page"]];

        	}else{                
                //Add attr "templateOrigin" in the costum app (it indicates the template origin of this page)
                foreach ($costum['costum']['app'] as $keyApp => $appConfig) {
                    $appConfig["templateOrigin"] = $_POST["paramsTpl"]["tplId"];
                    $newApp["costum.app"][$keyApp] = $appConfig;
                }
            }   

        	$allCms = Cms::getCmsByWhere($where);

            foreach ($allCms as $key => $value) {
                $allCms[$key]["tplParent"] = $_POST["paramsTpl"]["tplId"];
                Cms::deleteCms($key,"templates");
            }
            
            PHDB::batchInsert("templates",$allCms); 
            PHDB::update($_POST["paramsTpl"]["contextType"],[ "_id" => new MongoId($_POST["paramsTpl"]["contextId"]) ],[
                '$set'=>  $newApp                
            ]);

            Costum::forceCacheReset();

            $res=array("result"=>true, "msg"=> Yii::t("common", "Success"), "Cms" => $allCms);

        }
        return Rest::json($res);
    	
    }
	
}
