<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

    use Cms;
    use MongoId;
    use PHDB;
    use Rest;
    use Role;
    use Yii;

    class QuestionLocalMigrationAction extends \PixelHumain\PixelHumain\components\Action {
        public function run(){
            if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
                $questionData = PHDB::find(Cms::COLLECTION, array(
                    "path" => "tpls.blockCms.faq.questionLocal",
                    "type" => array('$ne' => "blockCms"),
                    "alreadyupdated" => array('$exists' => 0)
                ));
                $i = 0;
                foreach ($questionData as $key => $question) {
                    $this->updateParams($question);
                    $i++;
                }
                return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
            } else {
                echo "vous n'avez pas le droit"; 
            }
        }

        private function updateParams($questions){
            $paramsData = [
                "questionSize",
                "questionColor",
                "questionBg",
                "btnBg",
                "btncolorLabel",
                "btnBorderColor",
                "textSize",
                "textColor",
                "btnBorderRadius"
            ];
           
            $id = new MongoId((string)$questions['_id']);
            unset($questions["_id"]);
            foreach ($paramsData as $index => $param) {
                if(isset($questions[$param])){
                    if($index == 0){
                        $questions["css"]["question"]["fontSize"] = $questions[$param];
                    }
                    if($index == 1){
                        $questions["css"]["question"]["color"] = $questions[$param];
                    }
                    if($index == 2){
                        $questions["css"]["question"]["backgroundColor"] = $questions[$param];
                    }
                    if($index == 3){
                        $questions["css"]["bouton"]["backgroundColor"] = $questions[$param];
                    }
                    if($index == 4){
                        $questions["css"]["bouton"]["color"] = $questions[$param];
                    }
                    if($index == 5){
                        $questions["css"]["bouton"]["borderColor"] = $questions[$param];
                    }
                    if($index == 6){
                        $questions["css"]["text"]["fontSize"] = $questions[$param];
                    }
                    if($index == 7){
                        $questions["css"]["text"]["color"] = $questions[$param];
                    }
                    if($index == 8){
                        $questions["css"]["bouton"]["borderBottomLeftRadius"] = $questions[$param].px;
                        $questions["css"]["bouton"]["borderBottomRightRadius"] = $questions[$param].px;
                        $questions["css"]["bouton"]["borderTopLeftRadius"] = $questions[$param].px;
                        $questions["css"]["bouton"]["borderTopRightRadius"] = $questions[$param].px;
                    }
                    unset($questions[$param]);
                }
            }
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$set' => $questions
            ]);
            $this->removeOldAttribute($id);
        }

        public function removeOldAttribute($id)
        {
            $unset = [
                "questionSize" => true,
                "questionColor" => true,
                "questionBg" => true,
                "btnBg" => true,
                "btncolorLabel" => true,
                "btnBorderColor" => true,
                "textSize" => true,
                "textColor" => true
            ];
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$unset' => $unset
            ]);
        }
    }
?>