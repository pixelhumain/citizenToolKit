<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use PHDB, Role, Yii, Cms, Document;

class CheckCmsOrphansAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($and)
    {
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){


            $allcms = PHDB::find("cms", array("type" => ['$ne' => "blockCms"]), array("page","parent","type"));
            $countCms = 0;
            $uselessCms = null;
            $text = "<h1 style='text-align: center'>CMS orphans</h1>";
            $eltParent = array();
            foreach ($allcms as $idCms => $cms) {
                $costumParent = array_keys($cms["parent"])[0];
                $cmsPage    = isset($cms["page"]) ? $cms["page"] : "Unknow";
                $costumId   = $costumParent;
                $costumName = @$cms["parent"][$costumParent]["name"];
                $costumType = "";
                $parent     = array();
                if (!in_array($costumParent, $eltParent))
                {
                    if (isset($cms["parent"][$costumParent]["type"])) {
                        $costumType = $cms["parent"][$costumParent]["type"];
                        if (isset($cms["parent"][$costumParent]["type"]) && !in_array($costumParent, $eltParent)) {

                            $parent = PHDB::findOneById($cms["parent"][$costumParent]["type"], $costumParent, array("costum.app","name"));
                            $eltParent[$costumParent] = $parent;
                        }

                    }
                }

                $appKey = array_keys(@$eltParent[$costumParent]["costum"]["app"]);

                if (!in_array("#welcome", $appKey))
                {
                    $appKey[] = "#welcome";
                }

                if (isset($cms["type"])) {
                    if (!in_array("#".@$cms["page"], $appKey) && @$cms["page"] != "allPages" || @$eltParent[$costumParent]['name'] == "") {
                        if ($cms["type"] == "blockCopy" || $cms["type"] == "blockChild") {
                            $uselessCms[] = $idCms;
                            if ($and == "show") {
                                $countCms++;
                                $text .= "<div style='padding: 20px; background-color: #".substr($costumId, -8)."'>";
                                $text .= "<strong style='font-size : 24px'> Costum name: ".@$eltParent[$costumParent]['name']."</strong>";
                                if (@$eltParent[$costumParent]['name'] == "") {
                                    $text .= "<br><strong style='color: red; background-color : white;padding: 2px'> Costum doest exist!</strong>";
                                }elseif (!in_array("#".@$cms["page"], $appKey)) {
                                    $text .= "<br><strong style='color: red; background-color : white;padding: 2px'> Cms page (".$cmsPage.") doest exist in (".@$eltParent[$costumParent]['name'].")</strong>";
                                }
                                $text .= "<div style='background-color : #ffffff70; padding:2px;'>";
                                $text .= "<p> Cms id: ".$idCms;
                                $text .= "<p> Cms page: ".$cmsPage;
                                $text .= "<p> cms type: ".$cms["type"];
                                $text .= "<p> costumId: ".$costumId;
                                $text .= "<p> costumType: ".$costumType;
                                $text .= "</div>";
                                $text .= "<hr></div>";
                            }
                    }
                }
            }
        }
        if ($and == "show") {
         $text .= "<h3>Total CMS: ".$countCms."</h3>";
     }else if($and == "showDocument"){            
       $text = $this->showDocCms($uselessCms);
   }elseif($and == "delete"){
    $text = $this->clearCms($uselessCms);
}
       return $text;
   }else{
        return "Vous n'avez pas le droit!!";
   }
}
        public function showDocCms($cmsToClear = null){
            $view = "";
                $view .= "<h3>Total CMS to delete: <span style='color : red'>".count($cmsToClear)."</span></h3>";
                $countFoldToDelete=0;
                $arrayAllBlock= $cmsToClear;
                $docCollectionToDelete=PHDB::find(Document::COLLECTION, array("id"=>$cmsToClear));
                echo "<pre>";
                var_dump($docCollectionToDelete);
                echo "</pre>";
                $view .= "<b>Data CMS in collection Document: <span style='color : red'>".count($docCollectionToDelete)."</span></b></br>";
                foreach($arrayAllBlock as $vCms){
                    if(Yii::$app->fs->has(Yii::app()->params['uploadDir']."communecter/cms/".$vCms)){
                        $countFoldToDelete++;
                        // $view .= $vCms["id"];
                    }

                }
                $view .= "<br/><b>Image in folder upload/communecter/cms : <span style='color : red'>".$countFoldToDelete."</span></b></br>";
                return $view;
        }
        public function clearCms($clear = null){
            $view = "";
                $view .= "<h3>Total CMS deleted: <span style='color : red'>".count($clear)."</span></h3>";
                $countFoldToDelete=0;
                $arrayAllBlock= $clear;
               // $docCollectionToDelete=PHDB::find(Document::COLLECTION, array("id"=>array('$in'=>$clear)));
                $docCollectionToDelete=PHDB::remove(Document::COLLECTION, array("id"=>array('$in'=>$arrayAllBlock)));
                $view .= "<b>Data CMS in collection Document deleted: <span style='color : red'>".count($docCollectionToDelete)."</span></b></br>";
                foreach($arrayAllBlock as $id=> $vCms){
                    if(Yii::$app->fs->has(Yii::app()->params['uploadDir']."communecter/cms/".$vCms )){
                        Yii::$app->fs->delete(Yii::app()->params['uploadDir']."communecter/cms/".$vCms );
                        $view .= $vCms."<br>";
                        $countFoldToDelete++;
                    }

                }
                $view .= "<br/><b>Image in folder upload/communecter/cms deleted: <span style='color : red'>".$countFoldToDelete."</span></b></br>";
                // PHDB::remove(Cms::COLLECTION, array("_id"=>array('$in'=>$arrayAllBlock)));
                return $view;
        }
}