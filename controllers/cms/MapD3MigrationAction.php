<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms;
use MongoId;
use PHDB;
use Rest;
use Role;
use Yii;

class MapD3MigrationAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $basicmaps = PHDB::find(Cms::COLLECTION, array(
                "path" => "tpls.blockCms.graph.graphforMap",
                "type" => array('$ne' => "blockCms"),
                "alreadyupdated" => array('$exists' => 0)
            ));
            $i = 0;
            foreach ($basicmaps as $key => $map) {
                $this->updateParams($map);
                $i++;
            }
            return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
        } else {
            echo "vous n'avez pas le droit"; 
        }
    }

    private function updateParams($maps){
        $paramsData = [
            "cluster",
            "marker",
            "showLegende",
            "dynamicLegende",
            "legendConfig",
            "legendeVar",
            "legendeCible",
            "legendeLabel",
            "useFilters",
            "filtersConfig",
            "zones",
            "country",
            "level"
        ];
        $id = new MongoId((string)$maps['_id']);
        unset($maps["_id"]);
        foreach ($paramsData as $index => $param) {
            if(isset($maps[$param])){
                if($index == 0){
                    $maps["marker"]["cluster"] = $maps[$param];
                }
                if($index == 1){
                    $maps["marker"]["marker"] = $maps[$param];
                }
                if($index == 2){
                    $maps["legende"]["show"] = $maps[$param];
                }
                if($index == 3){
                    $maps["legende"]["dynamic"] = $maps[$param];
                }
                if($index == 4){
                    $maps["legende"]["config"] = $maps[$param];
                }
                if($index == 5){
                    $maps["legende"]["var"] = $maps[$param];
                }
                if($index == 6){
                    $maps["legende"]["cible"] = $maps[$param];
                }
                if($index == 7){
                    $maps["legende"]["label"] = $maps[$param];
                }
                if($index == 8){
                    $maps["filters"]["activate"] = $maps[$param];
                }
                if($index == 9){
                    $maps["filters"]["config"] = $maps[$param];
                }
                if($index == 10){
                    $maps["zone"]["zones"] = $maps[$param];
                }
                if($index == 11){
                    $maps["zone"]["level"] = $maps[$param];
                }
                if($index == 12){
                    $maps["zone"]["country"] = $maps[$param];
                }
                unset($maps[$param]);
            }
        }
        $maps["alreadyupdated"] = 1;
        PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
            '$set' => $maps
        ]);
        $this->removeOldAttribute($id);
    }

    public function removeOldAttribute($maps)
    {
        $unset = [
            "cluster" => true,
            "marker" => true,
            "showLegende" => true,
            "dynamicLegende" => true,
            "legendConfig" => true,
            "legendeVar" => true,
            "legendeCible" => true,
            "legendeLabel" => true,
            "useFilters" => true,
            "filtersConfig" => true,
            "zones" => true,
            "country" => true,
            "level" => true
        ];
        PHDB::update(Cms::COLLECTION,[ "_id" => $maps ],[
            '$unset' => $unset
        ]);
    }
}
