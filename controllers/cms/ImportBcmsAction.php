<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use CacheHelper;
use Element;
use Cms;
use Rest;
use PHDB;
use MongoId;
use Template;
use DataValidator;
class ImportBcmsAction extends \PixelHumain\PixelHumain\components\Action {
    public function run(){
        $controllers   = $this->getController();
        $costum        = CacheHelper::getCostum();
        $currentCms    = [];
        $corruptedPage = [];
        $templateIds   = [];
        
        $params        = DataValidator::clearUserInput($_POST);
        // $params["pages"] = [];
        $params["parentId"]   = isset($params["contextId"]) ? $params["contextId"] : $costum["contextId"];
        $params["parentSlug"] = isset($params["contextSlug"]) ? $params["contextSlug"] : $costum["contextSlug"];
        $params["parentType"] = isset($params["contextType"]) ? $params["contextType"] : $costum["contextType"];
        $param_default = $params;
        if (!isset($params["pages"])) {
            $params["newCostum"] = true;
            $params["clearCMS"]  = true;
            $jsons  = array();
            if (strlen($params["thematic"]) === 24 && ctype_xdigit($params["thematic"])) {                
                $jsons = PHDB::find('versionning',array( "type" => "tplJson", "_id" => new MongoId($params["thematic"])));
            }else{
                $jsons = PHDB::find('versionning',array( "type" => "tplJson", "thematic" => $params["thematic"]));
            }
            if (!empty($jsons)) { 
                $json = $jsons[array_keys($jsons)[0]];
                $params["pages"] = $json["pages"];              
            }
        }

        if(!isset($params['pages']) && isset($params["thematic"]) && is_string($params["thematic"])) {
            $templateFiliere = PHDB::findOne(Template::COLLECTION,array(
                "name" => "Template Filiere",
                "source.key" => "templateFiliere"
            ));
            if (!empty($templateFiliere)) {
                Cms::useTemplate($templateFiliere,$params);
            }     
        } else {
            // Import with different page name
            if (count($params["pages"]) == 1 && isset($params["pages"][array_keys($params["pages"])[0]]["isTplPage"])) {
                $params["customPage"] = array_keys($params["pages"])[0];
            }
            
            $where = array(
                "parent.".$params["parentId"] => array('$exists' => 1),
                '$or' => array()
            );
            foreach ($params["pages"] as $keyPage => $pageImported) {  
                $where['$or'][] = array("page" => $keyPage);
                if(isset($pageImported["cms"])) {
                    foreach ($pageImported["cms"] as $index => $cmsImported) {       
                        $where['$or'][] = array("blockOrigin" => $cmsImported["blockOrigin"]);
                    }
                }
                if (!isset($costum["app"]["#".$keyPage]["templateOrigin"])) {   
                    // $corruptedPage[] = $keyPage;
                }     

                if (isset($pageImported["templateOrigin"]) && !in_array($pageImported["templateOrigin"],$templateIds)) {
                    $templateIds[] = $pageImported["templateOrigin"];    
                }
            }
            if (!empty($templateIds)) {
                foreach ($templateIds as $key => $tplId) {
                    $template = Element::getElementById($tplId,Template::COLLECTION);
                    if(isset($params["thematic"]) && is_string($params["thematic"]) && !isset($template['siteParams']) && !isset($params['page']))  {
                        $_templateFiliere = PHDB::findOne(Template::COLLECTION,array(
                            "name" => "Template Filiere",
                            "source.key" => "templateFiliere"
                        ));
                        if (!empty($_templateFiliere)) {
                            Cms::useTemplate($_templateFiliere,$param_default);
                        }      
                    } else {
                        if (!empty($template)) {
                            Cms::useTemplate($template,$params);
                        }
                    }              
                }
            }

            $currentCms = Cms::getCmsByWhere($where);
            $importationCount = [];

            foreach ($currentCms as $currentKey => $currentValue) {    

                if (array_key_exists($currentValue["page"], $params["pages"]) || $params["page"]) {                
                    $i           = 0;
                    $path        = "";
                    $importation = array(); 
                    foreach ($params["pages"] as $keyPage => $pageValue) {    
                        if(isset($pageImported["cms"])) {
                            foreach ($pageValue["cms"] as $key => $value) {  
                                if (isset($currentValue["blockOrigin"]) && isset($value["blockOrigin"])) {
                                    if ($value["blockOrigin"] == $currentValue["blockOrigin"]) {                    
                                        if (($value["path"] == "tpls.blockCms.superCms.elements.supertext" || "tpls.blockCms.superCms.elements.button") &&  isset($value["text"])) {
                                            $path = "text";
                                            $currentCms[$currentKey]["text"] = $value["text"];
                                        }
                                        if ($value["path"] == "tpls.blockCms.superCms.elements.title" && isset($value["title"])) {
                                            $path = "title";
                                            $currentCms[$currentKey]["title"] = $value["title"];
                                        }
                                        if ($value["path"] == "tpls.blockCms.superCms.elements.image" && isset($value["image"])) {
                                            $path = "image";
                                            $currentCms[$currentKey]["image"] = $value["image"];
                                        }

                                        if (isset($value["css"])) {
                                            $currentCms[$currentKey]["css"] = $value["css"];
                                        }
                                        $importationCount[$currentValue["page"]][$currentValue["blockOrigin"]] = $i++;
                                    }
                                }    
                                
                            } 
                        } 
                        if(isset($pageImported["cms"])) {
                            foreach ($pageValue["cms"] as $key => $value) {  
                                if (isset($currentValue["blockOrigin"]) && isset($value["blockOrigin"])) {
                                    if ($value["blockOrigin"] == $currentValue["blockOrigin"]) {                    
                                        if (($value["path"] == "tpls.blockCms.superCms.elements.supertext" || "tpls.blockCms.superCms.elements.button") &&  isset($value["text"])) {
                                            $path = "text";
                                            $currentCms[$currentKey]["text"] = $value["text"];
                                        }
                                        if ($value["path"] == "tpls.blockCms.superCms.elements.title" && isset($value["title"])) {
                                            $path = "title";
                                            $currentCms[$currentKey]["title"] = $value["title"];
                                        }
                                        if ($value["path"] == "tpls.blockCms.superCms.elements.image" && isset($value["image"])) {
                                            $path = "image";
                                            $currentCms[$currentKey]["image"] = $value["image"];
                                        }

                                        if (isset($value["css"])) {
                                            $currentCms[$currentKey]["css"] = $value["css"];
                                        }
                                        $importationCount[$currentValue["page"]][$currentValue["blockOrigin"]] = $i++;
                                    }
                                }    
                                
                            } 
                        }
                    }     
                    if (isset($currentCms[$currentKey]["css"]) && isset($currentCms[$currentKey]["css"]["backgroundColor"])) {
                        $importation["css.backgroundColor"] = $currentCms[$currentKey]["css"]["backgroundColor"];
                    }

                    if (!empty($currentValue) && $path != "") {
                        $importation[$path] = $currentCms[$currentKey][$path];
                    }



                    if (!empty($importation)) {     
                        PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($currentKey) ],[ '$set'=> $importation]);
                    }
                }

            }

            return Rest::json(array("allCms" => $currentCms,"importationCounter" => $importationCount, "corruptedPage" => $corruptedPage));       
        }
    }
}