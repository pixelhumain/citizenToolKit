<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CAction, Element, Cms, PHDB, MongoId, Yii, Rest, CacheHelper, Authorisation, Costum, DataValidator;
class UseTemplateAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){        
        $controller=$this->getController();
        $contextCostum = CacheHelper::getCostum();
        $allCmsId = isset($template["cmsList"]) ? $template["cmsList"] : [];
        // $tplSelectedType = $template["subtype"] ?? "page";
        /**Duplicate block**/
        if(Authorisation::isInterfaceAdmin()){
            /* WE HAVE 2 TYPE OF TEMPLATES. FOR WHOLE COSTUM (type site) AND DEDICATED FOR ONE "Page" */
            // DUPLICATE ALL PAGE FROM TEMPLATE AND CHANGE WHOLE COSTUM CONFIG ACCORDIND TO TEMPLATE IF IT IS A SITE 
            if (!empty($template["siteParams"])) {    
               $template["siteParams"]["app"] = array_unique(array_merge($template["siteParams"]["app"], @$contextCostum["app"]), SORT_REGULAR); 
               if ($_POST["action"] == "") { 
                // If action is empty string, it means THE TEMPLATE CHOOSED IS NEVER USED, then we duplicate all blocks
                    Cms::duplicateBlock($allCmsId, array(
                        "parentId"   => $_POST['parentId'], 
                        "parentType" => $_POST["parentType"],
                        "parentSlug" => $_POST['parentSlug'],
                        "useTemplate"=>true),
                    true,
                    true
                );
                // NOW, WE INCREMENT THE NUMBRE OF TEMPLATES USER
                Element:: updatepathvalue($_POST["collection"],$tplSelectedId,"userCounter", isset($template["userCounter"]) ? $template["userCounter"]+1 : 1);
                }elseif ($_POST["action"] == "using") {
                    // If action is using, THE TEMPLATE CHOOSED IS USING (Bouton reinitialiser est cliqué), Then we reset the template as default. 
                    // Remove the old block from each page then duplicate the template again 
                    // $cmsEachPage = array();
                    // $allCmsToReset = array();
                    $i = 0;
                    foreach (array_keys($template["siteParams"]["app"]) as $pageHash) {                 
                        PHDB::remove("cms",[
                            "parent.".$contextCostum["contextId"] => array('$exists'=>1),
                            "page" => trim($pageHash, "#")]
                        );
                        $i++;
                        if (count(array_keys($template["siteParams"]["app"])) == $i) {                                    
                            // Reduplicate the Template
                            Cms::duplicateBlock($allCmsId, array(
                                "parentId"   => $_POST['parentId'], 
                                "parentType" => $_POST["parentType"],
                                "parentSlug" => $_POST['parentSlug'],
                                "useTemplate"=>true),
                            true,
                            true
                        );
                        }
                    }
                }              
                foreach (array_keys($template["siteParams"]) as $key => $value) {
                    Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.".$value, $template["siteParams"][$value]);
                }
                Costum::forceCacheReset();
            // JUST DUPLICATE THE BLOCK FROM ONE PAGE IF TEMPLATE IS DEDICATED FOR ONE PAGE 
            }else{     
                Cms::duplicateBlock($allCmsId, array(
                    "parentId"   => $_POST['parentId'], 
                    "parentType" => $_POST["parentType"],
                    "parentSlug" => $_POST['parentSlug'],
                    "page"       => trim($pageHash, "#"),
                    "useTemplate"=>true),
                true,
                true
            );
            }

            // THEN SAVE THE TEMPLATES INTO COSTUM "used" AND "using" 
            /*if (!empty($contextCostum["tplUsed"])) {
                foreach ($contextCostum["tplUsed"] as $tplUsedId => $valueTplUsed) {  
                    if ($tplUsedId != $tplSelectedId) {
                        Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplUsedId, "used");
                    }else{
                        Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplUsedId, "using");
                    }                
                }
                if (!in_array($tplSelectedId,$contextCostum["tplUsed"])) {
                    Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplSelectedId, "using");
                }
            }else{             
                Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplSelectedId, "using");
            }*/
            Costum::forceCacheReset();
            $res = array("result" => true, "msg" => Yii::t("common", "Template has been well activated"));   
        }else
        $res = array("result" => false, "msg" => Yii::t("common", "You are not allowed to do this action"));
        //$res = Cms::useTemplate($_POST,$contextCostum);
        return Rest::json($res);
    }
}
