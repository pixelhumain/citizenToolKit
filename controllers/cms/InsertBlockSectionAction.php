<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use CAction, Element, Cms, PHDB, MongoId, Yii, Rest, MongoDate, Authorisation, DataValidator, Log;
class InsertBlockSectionAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        if(Authorisation::isInterfaceAdmin()){
            $newSection = Cms::insertSection($_POST['page']);
            //var_dump( $_POST);
            //var_dump((string)$newSection["newBlocContainer"]["_id"]);

            //Add logs history
            Log::saveAndClean(
                array(
                    "userId"         =>  Yii::app()->session["userId"],
                    "userName"       =>  Yii::app()->session["user"]["name"],
                    "costumId"       =>  @$_POST["costumId"],
                    "costumSlug"     =>  @$_POST["costumSlug"],
                    "costumType"     =>  @$_POST["costumType"],
                    "costumEditMode" =>  @$_POST["costumEditMode"],
                    "browser"        =>  @$_SERVER["HTTP_USER_AGENT"],
                    "ipAddress"      =>  @$_SERVER["REMOTE_ADDR"],
                    "created"        =>  time(),
                    "action"         => "costum/addBlock",
                    "blockPath"      =>  $newSection["newBlocContainer"]["path"],
                    "blockName"      =>  "container",
                    "collection"     =>  $newSection["newBlocContainer"]["collection"],
                    "page"           =>  @$_POST['page'],
                    "idBlock"        =>  (string)$newSection["newBlocContainer"]["_id"]
                )
            );

            return Rest::json(array("html" => $controller->renderPartial("costum.views.cmsBuilder.blockEngine.partials.chargeBlock", $newSection["viewBlocParams"],true),"params" => $newSection["newBlocContainer"]));
        }else
        return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));

    }
}