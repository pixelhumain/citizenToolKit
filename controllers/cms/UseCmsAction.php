<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use CAction, Element, Cms, PHDB, MongoId, Yii, Rest;
class UseCmsAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $id = $_POST["id"];
        $page = $_POST['page'];
        //if ($_POST["action"] == "duplicate") {
            $blockParent = @$_POST["tplParent"];
            $data = [];
            $dataPosted = array(
                "parentId"   => $_POST['costumId'], 
                "parentType" => $_POST["costumType"],
                "parentSlug" => $_POST['costumSlug'],
                "page"       => $page);
        
            if (!empty($_POST['type'])) {
                if ($_POST["type"] == "blockCopy") {                
                    $dataPosted["path"] = "tpls.blockCms.superCms.container";
                    $dataPosted["type"] = $_POST['type'];
                    $data = Cms::duplicateBlock($id, $dataPosted, $blockParent);
                }else{
                    $dataPosted["type"] = "blockChild";
                    $data = Cms::duplicateBlock($id, $dataPosted, $blockParent);
                }
            }

            $costum = CacheHelper::getCostum();
            $useFooter = false;
            $footerCMS = Cms::getCmsFooter($costum["contextId"]);
            if(!empty($footerCMS))
                $useFooter = true;        

            $resp = array("html" => $controller->renderPartial("../../../../pixelhumain/ph/themes/CO2/views/layouts/footer",array("useFooter" => $useFooter)), "footerCMS" => $useFooter);
            return Rest::json($resp);

            /*$costum = CacheHelper::getCostum();
            $res = array();
            $titleArray = [1, 2, 3, 4, 5, 6];
            $i=0;
            // if(empty($blockParent)){
                foreach ($data as $key => $value) {
                    $param = array(
                        "v" => $value,
                        "i" => $i,
                        "titleArray" => $titleArray,
                        "el" => null,
                        "page" => $page,
                        "cmsList"    => [],//PHDB::find(CMS::COLLECTION, array("_id" => array('$in' => $data))),
                        "costumData" => $costum,
                        "canEdit"    => true,
                    );
                    $res = $controller->renderPartial("costum.views.cmsBuilder.blockEngine.partials.chargeBlock", $param, true);
                    $i++;
                }*/

// echo $res;
                // var_dump($res);
            // }
        // return  $res;
    }

}
