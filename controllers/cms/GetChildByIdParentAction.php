<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms;
use MongoId;
use PHDB;
use Rest;
use Role;
use Yii;


class GetChildByIdParentAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id)
    {
        $cmsData = [];
        $cms = PHDB::findOne(Cms::COLLECTION, array('_id' => new MongoId($id)));
        array_push($cmsData, $cms);
        $cmsData = array_merge($cmsData, $this->getChild((string) $cms["_id"]));
        return Rest::json($cmsData);
    }
    public function getChild($id)
    {
        $childsData = array();
        $childs = PHDB::find(Cms::COLLECTION, array('blockParent' => $id));
        if(count($childs) > 0){
            foreach ($childs as $key => $value) {
                array_push($childsData, $value);
                $childsData = array_merge($childsData,$this->getChild($key));
            } 
        }
        return $childsData;
    }
}