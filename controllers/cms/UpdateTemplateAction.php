<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms;
use Element;
use MongoId;
use Person;
use PHDB;
use Rest;
use Template;
use Yii, DataValidator, CacheHelper;

class UpdateTemplateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $_POST = DataValidator::clearUserInput($_POST); 
        if(Person::logguedAndAuthorized()){
            $old = Template::getById($_POST['id']);
            $costum = CacheHelper::getCostum();

            if(!isset($old['_id']))
                $old = Cms::getById($_POST["id"]);
            if(empty($old) && $old['collection'] != $_POST["collection"])
                PHDB::remove($old['collection'], ['_id' => new MongoId($_POST['id'])]);
            $params = $_POST['value'];
            $params['id'] = $_POST['id'];

            if (!isset($params["cmsList"])) {
                $allCms = [];
                $where=array(
                    "blockParent"=>['$exists'=>false],
                    "type" => ['$ne' => "blockCms"] ,
                    "parent.".array_keys($params["parent"])[0] => ['$exists'=>1],
                );
                $allCms = Cms::getCmsByWhere($where);
                $params["cmsList"] = array_keys($allCms);
            }

            if(isset($params["costumEditMode"]))
			    unset( $params["costumEditMode"]);
            
            //Create template if doesn't exist/update if it's already exist
            $exists = PHDB::findOne($_POST["collection"], array('_id' => new MongoId($_POST['id'])));
            if(!@$exists){
                $params["creator"] = Yii::app()->session["userId"];
                $params["created"] = time();
                PHDB::updateWithOptions($_POST["collection"],array("_id"=>new MongoId($_POST['id'])), array('$set' => $params ),array('upsert' => true ));
            }else{
                $params["updated"] = time();
                PHDB::update($_POST["collection"],array("_id"=>new MongoId($_POST['id'])), array('$set' => $params));
            }

            if(isset($params['cmsList'])){
                foreach($params["cmsList"] as $key => $value){
                    PHDB::update("cms",["_id" => new MongoId($value)],[
                        '$set'=> [
                            "tplParent" => (String)$params["id"]
                        ]
                    ]);
                }

                if(isset($_POST["value"]["page"]) && isset($template["siteParams"])){
                    unset($template["siteParams"]);
                    PHDB::update($template['collection'],array("_id"=>new MongoId($_POST['id'])), array('$set' => $template, '$unset' => array("siteParams" => true)));
                }else{
                    PHDB::update($template['collection'],array("_id"=>new MongoId($_POST['id'])), array('$set' => $template));
                }
            }else{
                /*Case insertion template*/
                $template = $_POST;

                $template['collection'] = "templates";

                $template['cmsList'] = array_keys($allCms);
                
                if(isset($template["costumEditMode"]))
                    unset( $template["costumEditMode"]);

                if(isset($_POST["value"]["page"]) && isset($template["siteParams"]))
                    unset( $template["siteParams"]);

                $template["creator"] = Yii::app()->session["userId"];
                $template["created"] = time();              

                foreach ($_POST['value'] as $key => $value) {
                    $template[$key] = $value;
                }
                unset($template["value"]);

                $template["parent"] = array($costum["contextId"] => array("type" => $costum["contextType"]));
                PHDB::insert($template['collection'], $template);
               
            }
            
            return Rest::json(array("result"=>true, "msg"=>Yii::t("cms","Edition success")));
        }
        return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Please Login First")));
    }
}
