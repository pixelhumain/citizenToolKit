<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\costum;

use Cms;
use PHDB;

class CreateCostumAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller = $this->getController();
        $element = PHDB::findOne($_POST["type"],array( "slug" => $_POST["slug"]));
        $element["id"] = (String)$element["_id"];
        return $controller->renderPartial("co2.views.element.firstStepCostum", array("element" => $element));
    		
    }
}
