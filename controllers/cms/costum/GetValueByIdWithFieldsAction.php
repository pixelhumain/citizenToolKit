<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\costum;

use PHDB , Rest , MongoId;

class GetValueByIdWithFieldsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $where["_id"] = new MongoId($_POST["id"]);
        $values = PHDB::findOne($_POST["collection"], $where , $_POST["fields"]);

        return Rest::json(array("result"=>true, "data" => $values));
    }
}
