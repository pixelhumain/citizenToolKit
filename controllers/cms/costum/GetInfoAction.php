<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\costum;

use Cms;
use PHDB;
use MongoId;
use Document;
use Rest;
use CO2Stat;
use Yii;

class GetInfoAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
    	$controller = $this->getController();
        $sizeImg = 0;
        $sizeFile = 0;
        $sizeImgB = "";
        $sizeFileB = "";
        $members = 0;
        $element = PHDB::findOne($_POST["type"],array( "_id" => new MongoId($_POST["id"])));
        $nbProjects = PHDB::find("projects",array( 
                '$or' => array(
                    array("parent.".$_POST["id"].".type" => $_POST["type"]),
                    array("source.keys" => $_POST["costumSlug"]),
                    array("links.projects.".$_POST["id"].".type]" => $_POST["type"]),
                    array("links.memmberOf.".$_POST["id"].".type]" => $_POST["type"])
                )
                ),array("name")
        );
        $allImage = Document::getListDocumentsWhere(
            array(
              "id"=> $_POST["id"],
              "type"=>$_POST["type"]
            ),"image"
          );
        foreach($allImage as $kf => $vf){
            if(Document::urlExists($baseUrl.$vf["imagePath"])){
                $sz = 0;
                if(str_contains($vf["size"], "K"))
                    $sz = explode('K', $vf["size"])["0"];
                if(str_contains($vf["size"], "M"))
                    $sz = explode('M', $vf["size"])["0"]*1024;
                $sizeImg += $sz;      
            }  
        }  
        $sizeImgB = $sizeImg .'K';
        if($sizeImg >= 1024){
            $sizeImgB = self::convert($sizeImg);
        } 
        $allFile = Document::getListDocumentsWhere(
            array(
              "id"=> $_POST["id"],
              "type"=>$_POST["type"]
            ),"file"
        ); 
        foreach($allFile as $kf => $vf){
            if($vf["doctype"] == "file"){
                if(Document::urlExists($baseUrl.$vf["docPath"])){                    
                    $sizeFile += $vf["size"]/1024;        
                } 

            } 
        }
        $sizeFileB = number_format($sizeFile, 2). "K";
        if($sizeFile >= 1024){
            $sizeFileB = self::convert($sizeFile);
        }
        $res = array(
            "sizeImg" => $sizeImgB,
            "sizeFile" => $sizeFileB
        );
        $memberGeolo = 0;
        if($_POST["type"] == "organizations" && isset($element["links"]["members"])){
            $res["members"] = count($element["links"]["members"]);
            foreach($element["links"]["members"] as $km => $vm){
                if(isset($vm["type"])){
                    $member = PHDB::findOne($vm["type"],array(
                        '_id' => new MongoId($km),
                        "address" => array('$exists' => true)
                    ));
                    if($member){
                        $memberGeolo++;
                    }
                }
            } 
        }
        if($_POST["type"] == "projects" && isset($element["links"]["contributors"])){
            $res["contributors"] = count($element["links"]["contributors"]);
            foreach($element["links"]["contributors"] as $km => $vm){
                $member = PHDB::findOne($vm["type"],array(
                    '_id' => new MongoId($km),
                    "address" => array('$exists' => true)
                )); 
                if($member){
                    $memberGeolo++;
                }
            } 
        }
        $res["memberGeolo"] = $memberGeolo;
        if($_POST["type"] == "events" && isset($element["links"]["attendees"]))
            $res["attendees"] = count($element["links"]["attendees"]);
        $all = co2Stat::getStatsCustomAll(); 
        $tot = 0;
        foreach($all as $e=>$v){
            $tot = ($tot+$v);
        }
        $res["visit"] = $tot;
        
        $res["visitCarte"] = 0 ;
        if(isset($all["co2-map"]))
            $res["visitCarte"] = $all["co2-map"] ;
        
        $res["nbProject"] = 0;
        if($nbProjects){
            $res["nbProject"] = count($nbProjects);
        }
        
        return Rest::json($res);
    }
    private function convert($size){
        $sizeImg = $size / 1024;
        $sizeUnit = "MB";
        if($sizeImg >= 1024){
            $sizeImg = $sizeImg / 1024;
            $sizeUnit = "GB";
        }
        return number_format($sizeImg, 2)." ".$sizeUnit;
        
    }
}
