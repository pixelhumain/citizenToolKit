<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use MongoId;
use PHDB,Cms,Organization,Yii,Project,Role;

class CleanDataContainerspcmsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController(); 
        $output = "";
        $count = 0;
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
            foreach([Organization::COLLECTION, Project::COLLECTION] as $v){
                $parent=PHDB::find($v, array("costum"=>array('$exists'=>true)), array("_id","slug"));
                foreach($parent as $c){
                    if(@$c["_id"]){
                        $allCms =   PHDB::find(Cms::COLLECTION, 
                                        array(
                                            "parent.".(string)$c["_id"] => array('$exists'=>1), 
                                            "type" => "blockCopy",
                                            "path" => "tpls.blockCms.superCms.container"
                                        )
                                    );
                        if(count($allCms) != 0){
                            foreach($allCms as $kcms => $vcms){
                                $unset = [
                                    "blockCmsBgColor"  => true,
                                    "blockCmsAosDuration"  => true,
                                    "blockCmsAos"  => true,
                                    "blockCmsBgImg"  => true ,
                                    "blockCmsBgPaint" => true , 
                                    "blockCmsBgPosition"  => true ,
                                    "blockCmsBgRepeat" => true , 
                                    "blockCmsBgSize" => true , 
                                    "blockCmsBgTarget"  => true, 
                                    "blockCmsBgType"  => true ,                       
                                    "blockCmsBorderBottom" => true, 
                                    "blockCmsBorderColor" => true ,
                                    "blockCmsBorderLeft"  => true, 
                                    "blockCmsBorderRight" => true, 
                                    "blockCmsBorderTop"  => true,
                                    "blockCmsBorderType"  => true,
                                    "blockCmsBorderWidth"  => true,
                                    "blockCmsColorTitle1" => true,
                                    "blockCmsColorTitle2" => true,
                                    "blockCmsColorTitle3" => true,
                                    "blockCmsColorTitle4" => true,
                                    "blockCmsColorTitle5" => true,
                                    "blockCmsColorTitle6" => true,
                                    "blockCmsPoliceTitle1" => true,
                                    "blockCmsPoliceTitle2" => true,
                                    "blockCmsPoliceTitle3" => true,
                                    "blockCmsPoliceTitle4" => true,
                                    "blockCmsPoliceTitle5" => true,
                                    "blockCmsPoliceTitle6" => true,
                                    "blockCmsTextAlignTitle1" => true,
                                    "blockCmsTextAlignTitle2" => true,
                                    "blockCmsTextAlignTitle3" => true,
                                    "blockCmsTextAlignTitle4" => true,
                                    "blockCmsTextAlignTitle5" => true,
                                    "blockCmsTextAlignTitle6" => true,
                                    "blockCmsTextLineHeightTitle1" => true,
                                    "blockCmsTextLineHeightTitle2" => true,
                                    "blockCmsTextLineHeightTitle3" => true,
                                    "blockCmsTextLineHeightTitle4" => true,
                                    "blockCmsTextLineHeightTitle5" => true,
                                    "blockCmsTextLineHeightTitle6" => true,
                                    "blockCmsTextSizeTitle1" => true,
                                    "blockCmsTextSizeTitle2" => true,
                                    "blockCmsTextSizeTitle3" => true,
                                    "blockCmsTextSizeTitle4" => true,
                                    "blockCmsTextSizeTitle5" => true,
                                    "blockCmsTextSizeTitle6" => true,
                                    "blockCmsUnderlineColorTitle1" => true,
                                    "blockCmsUnderlineColorTitle2" => true,
                                    "blockCmsUnderlineColorTitle3" => true,
                                    "blockCmsUnderlineColorTitle4" => true,
                                    "blockCmsUnderlineColorTitle5" => true,
                                    "blockCmsUnderlineColorTitle6" => true,
                                    "blockCmsUnderlineHeightTitle1" => true,
                                    "blockCmsUnderlineHeightTitle2" => true,
                                    "blockCmsUnderlineHeightTitle3" => true,
                                    "blockCmsUnderlineHeightTitle4" => true,
                                    "blockCmsUnderlineHeightTitle5" => true,
                                    "blockCmsUnderlineHeightTitle6" => true,
                                    "blockCmsUnderlineMargeBottomTitle1" => true,
                                    "blockCmsUnderlineMargeBottomTitle2" => true,
                                    "blockCmsUnderlineMargeBottomTitle3" => true,
                                    "blockCmsUnderlineMargeBottomTitle4" => true,
                                    "blockCmsUnderlineMargeBottomTitle5" => true,
                                    "blockCmsUnderlineMargeBottomTitle6" => true,
                                    "blockCmsUnderlineSpaceTitle1" => true,
                                    "blockCmsUnderlineSpaceTitle2" => true,
                                    "blockCmsUnderlineSpaceTitle3" => true,
                                    "blockCmsUnderlineSpaceTitle4" => true,
                                    "blockCmsUnderlineSpaceTitle5" => true,
                                    "blockCmsUnderlineSpaceTitle6" => true,
                                    "blockCmsUnderlineTitle1" => true,
                                    "blockCmsUnderlineTitle2" => true,
                                    "blockCmsUnderlineTitle3" => true,
                                    "blockCmsUnderlineTitle4" => true,
                                    "blockCmsUnderlineTitle5" => true,
                                    "blockCmsUnderlineTitle6" => true,
                                    "blockCmsUnderlineWidthTitle1" => true,
                                    "blockCmsUnderlineWidthTitle2" => true,
                                    "blockCmsUnderlineWidthTitle3" => true,
                                    "blockCmsUnderlineWidthTitle4" => true,
                                    "blockCmsUnderlineWidthTitle5" => true,
                                    "blockCmsUnderlineWidthTitle6" => true,             
                                    "blockCmsLineSeparatorBg" => true ,
                                    "blockCmsLineSeparatorBottom"  => true,
                                    "blockCmsLineSeparatorHeight"  => true,
                                    "blockCmsLineSeparatorIcon"   => true,
                                    "blockCmsLineSeparatorPosition" => true , 
                                    "blockCmsLineSeparatorTop"  => true,
                                    "blockCmsLineSeparatorWidth"  => true,
                                    "marginBottom"  => true , 
                                    "marginTop"   => true ,
                                    "modeLg"   => true ,
                                    "modeMd"   => true ,
                                    "modeSm"   => true ,
                                    "modeXs"   => true ,
                                    "paddingBottom" => true  ,
                                    "paddingLeft"  => true  ,
                                    "paddingRight" => true  , 
                                    "paddingTop" => true  
                                ];
                                $count ++;
                                PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($kcms) ],[
                                    '$unset' => $unset
                                ]);
                            }
                        }
                    }
                }
            }
            $output .= $count."blocs supercms mis à jour ";
        }else{
            $output .= "Vous n'êts pas autorisé";
        }
        return $output;
    }
}