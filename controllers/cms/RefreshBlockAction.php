<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
    
    use PHDB, Rest, Cms, Yii , Costum;
    use Authorisation,DataValidator, Role;
    use CacheHelper;
    use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;

    class RefreshBlockAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $controller = $this->getController();
            $response = array();
            $dataBlock = [];
            $_POST = DataValidator::clearUserInput($_POST);
            if(!empty($_POST["idBlock"])){
                $dataBlock = Cms::getByIdWithChildreen($_POST["idBlock"], true, "cms");
                $pathExplode      = explode('.',@$dataBlock["path"]);
                $superKunik       = $pathExplode[count($pathExplode) - 1] . $dataBlock["_id"];
                $blockKey         = (string)$dataBlock["_id"];
                $costum = CacheHelper::getCostum();
                $costum["langCostumActive"] = Costum::setLanguageActiveInCostum($costum);
                $params     = [
                    "blockCms"  =>  $dataBlock,
                    "kunik"     =>  $superKunik,
                    "blockKey"  =>  $blockKey,
                    "content"   =>  array(),
                    "costum"    =>  $costum,
                    "page"      =>  @$dataBlock["page"],
                    "canEdit"   =>  $costum["editMode"],
                    "clienturi" =>  $_POST["clienturi"],
                    // "el"        => $costum
                ];
                if (!empty($dataBlock)) {
                    $response = BlockCmsWidget::widget([
                        "path" => @$dataBlock["path"],
                        //cette config (notFoundViewPath) peut être enlever quand tout les blocks cms sont transformés en Widget
                        "notFoundViewPath" => "costum.views." .  @$dataBlock["path"], 
                        "config" => $params
                    ]);
                }

                if (strpos(@$dataBlock["path"], "superCms.elements.image") == true){
                    $response = "<div class='cmsbuilder-block super-cms image-wrapper' data-blockType='element' data-kunik='$superKunik' data-id='$blockKey' data-name='image' style='height: max-content;'>$response</div>";
                }
                return Rest::json(array("result"=>true, "msg"=> Yii::t("common", "Success"), "view" => $response));
            } else {
                return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));  
            }
        }
    }


?>