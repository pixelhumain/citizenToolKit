<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Authorisation;
use CacheHelper;
use DataValidator;
use MongoId;
use PHDB;
use Rest;
use Yii;

class SaveKeyOpenAIAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $_POST = DataValidator::clearUserInput($_POST);
        $costum = CacheHelper::getCostum();
        if ((bool) $costum["editMode"] && Authorisation::isCostumAdmin() && isset($_POST["openAIKey"])) {
            $apiKey = PHDB::findOne("accesskey", array(
                "parent.".$costum["contextId"]."" => ['$exists' => true]
            ));
            if ($_POST["openAIKey"] == '$unset') {
                if (!is_null($apiKey) && isset($apiKey["keys"]) && isset($apiKey["keys"]["openAIKey"])) {
                    if (count($apiKey["keys"]) > 1) {
                        PHDB::update("accesskey", array("_id" => new MongoId((string) $apiKey["_id"])), array('$unset' => array(
                            "keys.openAIKey" => true
                        )));
                    } else {
                        PHDB::remove("accesskey", array("_id" => new MongoId((string) $apiKey["_id"])));
                    }
                    return Rest::json(array("result"=>true, "msg"=>Yii::t("cms", "API key removed successfully")));
                } else {
                    return Rest::json(array("result"=>false, "msg"=>Yii::t("common", "Something went wrong!")));
                };
            } else {
                $keyCrypted = $this->encryptKey($costum["contextId"], $costum["contextSlug"], $_POST["openAIKey"]);
                if (is_null($apiKey)) {
                    $newKey = array(
                        "creator" => Yii::app()->session["userId"],
                        "parent" => array(
                            $costum["contextId"] => array(
                                "type"=>$costum["contextType"],
                                "name"=>$costum["contextSlug"]
                            )
                        ),
                        "keys" => [
                            "openAIKey" => $keyCrypted
                        ],
                        "created" => time(),
                    );
                    PHDB::insert("accesskey", $newKey);
                    return Rest::json(array("result"=>true, "msg"=>Yii::t("cms", "API key successfully added")));
                } else {
                    PHDB::update("accesskey", array("_id" => new MongoId((string) $apiKey["_id"])), array('$set' => array(
                        "keys.openAIKey" => $keyCrypted,
                        "updated" => time()
                    )));
                    return Rest::json(array("result"=>true, "msg"=>Yii::t("cms", "API key modified successfully")));
                }
            }
        } else {
            return Rest::json(array("result"=>false, "msg"=>Yii::t("common", "You are not allowed to do this action")));
        }
    }

    private function encryptKey($costumId, $costumSlug, $apikey) {
        $key = hash('sha256', $costumId . $costumSlug, true);
        $iv = substr($key, 0, 16); 
        $options = 0;
        $encrypted = openssl_encrypt($apikey, "aes-256-cbc", $key, $options, $iv);
        return base64_encode($encrypted);
    }
}