<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
    
    use PHDB, Rest, Cms, Yii;
    use Authorisation,DataValidator, Role;
    use CacheHelper;
    use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;

    class RefreshMultiBlockAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $controller = $this->getController();
            $response = array();
            $dataBlock = [];
            $_POST = DataValidator::clearUserInput($_POST);
            if(count($_POST["idBlocks"]) > 0){
                foreach ($_POST["idBlocks"] as $key => $id) {
                    $dataBlock = Cms::getByIdWithChildreen($id, true);
                    $pathExplode      = explode('.',@$dataBlock["path"]);
                    $superKunik       = $pathExplode[count($pathExplode) - 1] . $dataBlock["_id"];
                    $blockKey         = (string)$dataBlock["_id"];
                    $costum = CacheHelper::getCostum();
                    $params     = [
                        "blockCms"  =>  $dataBlock,
                        "kunik"     =>  $superKunik,
                        "blockKey"  =>  $blockKey,
                        "content"   =>  array(),
                        "costum"    =>  $costum,
                        "page"      =>  @$dataBlock["page"],
                        "canEdit"   =>   $costum["editMode"],
                        "clienturi" =>  @$clienturi,
                        "extraParams" => @$_POST["extraParams"]
                        // "el"        => $costum
                    ];
                    if (!empty($dataBlock)) {
                        $response[$id] = BlockCmsWidget::widget([
                            "path" => @$dataBlock["path"],
                            //cette config (notFoundViewPath) peut être enlever quand tout les blocks cms sont transformés en Widget
                            "notFoundViewPath" => "costum.views." .  @$dataBlock["path"], 
                            "config" => $params
                        ]);

                        // $response = $controller->renderPartial("costum.views.".$dataBlock["path"], Cms::getParamsBlock($dataBlock),true);
                    }
                }
                return Rest::json(array("result"=>true, "msg"=> Yii::t("common", "Success"), "view" => $response));
            } else {
                return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));  
            }
        }
    }


?>