<?php 

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use PHDB, Rest, Yii,DataValidator,Authorisation,MongoId;


class CmsMarkdownAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run() {
		$_POST = DataValidator::clearUserInput($_POST);
        $res=array("result"=>false, "msg"=> Yii::t("common", "Access denied"));
        $notes = ["doc" => Yii::t("common", "Access denied")];
        
        if(Authorisation::isInterfaceAdmin()){
            if (isset($_POST["id"])) {
                $note = PHDB::findOneById("notes", $_POST["id"],array("name","doc","links"));
                if (!empty($notes)) {
                    if (array_key_exists(Yii::app()->session["userId"], $note["links"]["contributors"])) {
                        $notes = $note;
                    }
                }
            }else{    
                $notes = PHDB::find("notes",array( "links.contributors.".Yii::app()->session["userId"] => array('$exists'=>1)),array("name","doc","links"));
            } 
        }
        $res = array("result"=>true, "data" => $notes);
        return Rest::json($res);
    }

}

?>

