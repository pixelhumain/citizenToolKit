<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Element , Cms , Costum , Log;
use CAction , CacheHelper , PHDB, MongoId, Yii, Rest;
use Authorisation, DataValidator;
use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;

class DuplicateCmsAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $_POST = DataValidator::clearUserInput($_POST);
        if(Authorisation::isInterfaceAdmin() && !empty($_POST["idCmsToDuplic"])){  
            $blockIdsToDuplicate = $_POST["idCmsToDuplic"];     
            $from=(isset($_POST["from"])) ? $_POST["from"] : "cms";  
            $to=(isset($_POST["to"])) ? $_POST["to"] : "cms";  
            $page        = $_POST['page'];
            $params  = array(
                "parentId"   => $_POST['costumId'], 
                "parentType" => $_POST["costumType"],
                "parentSlug" => $_POST['costumSlug'],
                "page"       => $page
            );
            $costum = CacheHelper::getCostum();
            $costum["langCostumActive"] = Costum::setLanguageActiveInCostum($costum);
            $response = array(); 
            $blckParent=(isset($_POST["blockParent"])) ? $_POST["blockParent"] : null;
            $data = Cms::duplicateBlock($blockIdsToDuplicate, $params, $blckParent,$from,$to);
            $pathExplode  = explode('.',@$data["path"]);
            $superKunik   = $pathExplode[count($pathExplode) - 1] . $data["_id"];
            $blockKey     = (string)$data["_id"];
            $name = isset($data['name']) ? $data['name'] : (isset($data['blockName']) ? $data['blockName'] : 'Block cms');
           //var_dump($data);exit;
            if(empty($data["blockParent"])){
                $response = $controller->renderPartial("costum.views.cmsBuilder.blockEngine.partials.chargeBlock", Cms::getParamsView($data),true);
            } else {
                $newparams     = [
                    "blockCms"  =>  $data,
                    "kunik"     =>  $superKunik,
                    "blockKey"  =>  $blockKey,
                    "content"   =>  array(),
                    "costum"    =>  $costum,
                    "page"      =>  @$data["page"],
                    "canEdit"   =>   $costum["editMode"],
                    "clienturi" =>  $_POST["clienturi"],
                    "el"        => []
                ];

                $response = BlockCmsWidget::widget([
                    "path" => @$data["path"],
                    //cette config (notFoundViewPath) peut être enlever quand tout les blocks cms sont transformés en Widget
                    "notFoundViewPath" => "costum.views." .  @$data["path"], 
                    "config" => $newparams
                ]);
            }

            //Add logs history
            Log::saveAndClean(
                array(
                    "userId"         =>  Yii::app()->session["userId"],
                    "userName"       =>  Yii::app()->session["user"]["name"],
                    "costumId"       =>  @$_POST["costumId"],
                    "costumSlug"     =>  @$data["source"]["key"],
                    "costumType"     =>  @$data["costumType"],
                    "costumEditMode" =>  true,
                    "browser"        =>  @$_SERVER["HTTP_USER_AGENT"],
                    "ipAddress"      =>  @$_SERVER["REMOTE_ADDR"],
                    "created"        =>  time(),
                    "action"         => "costum/addBlock",
                    "blockPath"      =>  @$data["path"],
                    "blockName"      =>  @$data["blockName"] ?? substr($data["path"], strrpos($data["path"], '.') + 1),
                    "collection"     =>  @$data["collection"],
                    "page"           =>  @$data["page"],
                    "idBlock"        =>  $blockKey
                )
            );

            if (strpos(@$data["path"], "superCms") == false) {
                $response = "<div class='cmsbuilder-block stop-propagation col-md-12 col-xs-12 no-padding sp-elt-$blockKey super-cms' data-blockType='custom' data-kunik='$superKunik' data-id='$blockKey' data-name='$name'>$response</div>";
                $response .= '<a href="javascript:;" style="display:none;" class="edit'.$superKunik.'Params" , data-path="'.@$data["path"].'" data-id="'.$blockKey.'" data-collection="cms"></a>';
            }

            if (strpos(@$data["path"], "superCms.elements.image") == true) {
                $response = "<div class='cmsbuilder-block super-cms image-wrapper' data-blockType='element' data-kunik='$superKunik' data-id='$blockKey' data-name='image' style='height: max-content;'>$response</div>";
            }

            return Rest::json(array("result"=>true,"msg"=> Yii::t("common", "Success"), "view" => $response , "blockCms" => $data ));
        }else{
            return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));  
        }
    }

}