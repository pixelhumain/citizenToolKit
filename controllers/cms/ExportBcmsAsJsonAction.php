<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;
use CacheHelper;
use Element;
use Cms;
use PHDB;
use Rest;
class ExportBcmsAsJsonAction extends \PixelHumain\PixelHumain\components\Action {
    public function run(){
        $controller            = $this->getController();
        $costumCache           = CacheHelper::getCostum();
        $costum                = PHDB::findOneById($costumCache["contextType"],$costumCache["contextId"],["costum"]);
        $costum["contextId"]   = $costumCache["contextId"];
        $costum["contextType"] = $costumCache["contextType"];
        $allCms = [];
        $where = array(
          "page" => array('$exists' => 1),
          "parent.".$costum["contextId"] => array('$exists' => 1),
          '$or' => array(
            array("path" => "tpls.blockCms.superCms.container"),
            array("path" => "tpls.blockCms.superCms.elements.supertext"),
            array("path" => "tpls.blockCms.superCms.elements.button"),
            array("path" => "tpls.blockCms.superCms.elements.title"),
            array("path" => "tpls.blockCms.superCms.elements.image")
          )
        );
        
        $allCms  = Cms::getCmsByWhere($where);
        $allPage = array();
        foreach ($allCms as $cmsId => $cms) {
          $allCms[$cmsId] = [];
          $allCms[$cmsId]["path"] = $cms["path"];
          $allCms[$cmsId]["blockOrigin"] = isset($cms["blockOrigin"]) ? $cms["blockOrigin"] : $cmsId;
          $allCms[$cmsId]["page"] = $cms["page"];

          if (!isset($allCms[$cmsId]["page"]["templateOrigin"]) && isset($costum["costum"]["app"]["#".$cms["page"]]["templateOrigin"])) {
            $allPage[$cms["page"]]["templateOrigin"] = $costum["costum"]["app"]["#".$cms["page"]]["templateOrigin"];
          }

          if (isset($cms["css"]) && isset($cms["css"]["backgroundColor"])) {
            $allCms[$cmsId]["css"]["backgroundColor"] = $cms["css"]["backgroundColor"];
          }

          if (($cms["path"] == "tpls.blockCms.superCms.elements.supertext" || "tpls.blockCms.superCms.elements.button") &&  isset($cms["text"])) {
            $allCms[$cmsId]["text"] = $cms["text"];
          }

          if ($cms["path"] == "tpls.blockCms.superCms.elements.title" && isset($cms["title"])) {
            $allCms[$cmsId]["title"] = $cms["title"];
          }

          if ($cms["path"] == "tpls.blockCms.superCms.elements.image" && isset($cms["image"])) {
            $allCms[$cmsId]["image"] = $cms["image"];
          } 

          $allPage[$cms["page"]]["cms"][] = $allCms[$cmsId];
        }

        return Rest::json(array("pages"=>$allPage));            

    }
}