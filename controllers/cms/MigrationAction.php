<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use MongoId;
use PHDB,Cms,Organization,Element,Yii,Document,Project,Role;

class MigrationAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($slug = null, $page=null ,$id=null,$post=null, $indice = null) {
    	$controller=$this->getController(); 
        if(!$slug){
            $output = "";
            if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
                $baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
                foreach([Organization::COLLECTION, Project::COLLECTION] as $v){
                    $parent=PHDB::find($v, array("costum"=>array('$exists'=>true)), array("_id","slug"));
                    foreach($parent as $c){
                        if(@$c["_id"]){
                            $allCms = PHDB::find(Cms::COLLECTION, array(
                                                "parent.".(string)$c["_id"] => array('$exists'=>1), 
                                                '$and'=>array(
                                                    array(
                                                        '$or'=> array(
                                                            array("type" => array('$exists' => 0)),
                                                            array("type" => "blockCopy")
                                                        )
                                                    ),
                                                    array("page" => array('$exists' => 1)),
                                                    array("structags" => array('$exists' => 0)),
                                                    array(
                                                        '$and'=> array(
                                                            array("path" => array('$ne' => "tpls.blockCms.superCms.container")),
                                                            array("path" => array('$ne' => "tpls.blockCms.projects.callForProjects"))
                                                        )
                                                    )
                                                )
                                            ));
                            if(count($allCms) != 0){
                                foreach($allCms as $vId => $cms){
                                    if($vId != ""){
                                        /* **************************************
                                        *
                                        *    Preparing data 
                                        *
                                        ***************************************** */
                                        $childList = (isset($_POST['cmsList']) ? $_POST['cmsList'] : [$vId]);
                                        //** create new superCms 
                                        $arrayInsertSuperCms = array(
                                            "path" => "tpls.blockCms.superCms.container",
                                            "page" => $cms["page"],
                                            "type" => "blockCopy",
                                            "parent" => $cms["parent"],
                                            "creator" => @$cms["creator"],
                                            "collection" => "cms",
                                            "subtype" => "supercms",
                                            "class" => [
                                                "width" => "sp-cms-std"
                                            ],
                                            "cmsList" => $childList
                                        );
                                        if(isset($cms["name"]))
                                            $arrayInsertSuperCms["name"] = $cms["name"];
                
                                        if(isset($cms["haveTpl"]))
                                            $arrayInsertSuperCms["haveTpl"] = $cms["haveTpl"];
                
                                        if(isset($cms["tplParent"]))
                                            $arrayInsertSuperCms["tplParent"] = $cms["tplParent"];
                
                                        if(isset($cms["source"]))
                                            $arrayInsertSuperCms["source"] = $cms["source"];
                
                                        if(isset($cms["position"]))
                                            $arrayInsertSuperCms["position"] = $cms["position"];
                                        
                
                
                                        if(isset($cms["modeMd"]))
                                            $arrayInsertSuperCms["modeMd"] = $cms["modeMd"];
                                        if(isset($cms["modeSm"]))
                                            $arrayInsertSuperCms["modeSm"] = $cms["modeSm"];
                                        if(isset($cms["modeXs"]))
                                            $arrayInsertSuperCms["modeXs"] = $cms["modeXs"];
                
                                            
                                        if(isset($cms["blockCmsBgPaint"]))
                                            $arrayInsertSuperCms["blockCmsBgPaint"] = $cms["blockCmsBgPaint"];
                                        if(isset($cms["blockCmsBgType"])  && $cms["blockCmsBgType"] == "paint")
                                            $arrayInsertSuperCms["blockCmsBgType"] = $cms["blockCmsBgType"];
                
                                        if(isset($cms["marginBottom"]))
                                            $arrayInsertSuperCms["css"]["margin"]["bottom"] = $cms["marginBottom"]."px";
                                        if(isset($cms["marginTop"]))
                                            $arrayInsertSuperCms["css"]["margin"]["top"] = $cms["marginTop"]."px";
                                        if(isset($cms["paddingBottom"]))
                                            $arrayInsertSuperCms["css"]["padding"]["bottom"] = $cms["paddingBottom"]."px";
                                        if(isset($cms["paddingLeft"]))
                                            $arrayInsertSuperCms["css"]["padding"]["left"] = $cms["paddingLeft"]."px";
                                        if(isset($cms["paddingRight"]))
                                            $arrayInsertSuperCms["css"]["padding"]["right"] = $cms["paddingRight"]."px";
                                        if(isset($cms["paddingTop"]))
                                            $arrayInsertSuperCms["css"]["padding"]["top"] = $cms["paddingTop"]."px";
                
                
                                        /*** Background */
                                        if(isset($cms["blockCmsBgColor"]))
                                            $arrayInsertSuperCms["css"]["background"]["color"] = $cms["blockCmsBgColor"];
                                        if(isset($cms["blockCmsBgSize"]))
                                            $arrayInsertSuperCms["css"]["background"]["size"] = $cms["blockCmsBgSize"];
                                        if(isset($cms["blockCmsBgRepeat"]))
                                            $arrayInsertSuperCms["css"]["background"]["repeat"] = $cms["blockCmsBgRepeat"];
                                        if(isset($cms["blockCmsBgPosition"]))
                                            $arrayInsertSuperCms["css"]["background"]["position"] = $cms["blockCmsBgPosition"];
                                        if(isset($cms["blockCmsBgTarget"]))
                                            $arrayInsertSuperCms["css"]["background"]["target"] = $cms["blockCmsBgTarget"];
                                        if(isset($cms["path"]) && ($cms["path"] == "tpls.blockCms.text.backgroundPhotoParallax" || $cms["path"] == "tpls.blockCms.docs.modal_with_iframe_content" || $cms["path"] == "tpls.blockCms.docs.showing_pdf_in_popup_modal"|| $cms["path"] == "tpls.blockCms.events.modal_with_agenda_calendar" || $cms["path"] == "tpls.blockCms.photo.logo"|| $cms["path"] == "tpls.blockCms.text.textWithBtnCreateAcount"|| $cms["path"] == "tpls.blockCms.text.textWithButton"))
                                            $arrayInsertSuperCms["css"]["background"]["attachment"] = "fixed";
                                        
                                        
                                        if(isset($cms["modeLg"])){ 
                                            $widthBlock = ($cms["modeLg"]*100)/12;
                                            $arrayInsertSuperCms["css"]["size"]["width"] = $widthBlock."%";
                                        }else{
                                            $arrayInsertSuperCms["css"]["size"]["width"] = "100%";
                                        }
                
                
                                        // border 
                                        if(isset($cms["blockCmsBorderColor"]))
                                            $arrayInsertSuperCms["css"]["border"]["color"]= $cms["blockCmsBorderColor"];
                                        if(isset($cms["blockCmsBorderType"]))
                                            $arrayInsertSuperCms["css"]["border"]["type"]= $cms["blockCmsBorderType"];
                                        if(isset($cms["blockCmsBorderWidth"]))
                                            $arrayInsertSuperCms["css"]["border"]["width"]= $cms["blockCmsBorderWidth"];
                                        if(isset($cms["blockCmsBorderTop"]))
                                            $arrayInsertSuperCms["css"]["border"]["top"]= ($cms["blockCmsBorderTop"] == "true") ? "checked": "";
                                        if(isset($cms["blockCmsBorderBottom"]))
                                            $arrayInsertSuperCms["css"]["border"]["bottom"]= ($cms["blockCmsBorderBottom"] == "true") ? "checked": "";
                                        if(isset($cms["blockCmsBorderLeft"]))
                                            $arrayInsertSuperCms["css"]["border"]["left"]= ($cms["blockCmsBorderLeft"] == "true") ? "checked": "";
                                        if(isset($cms["blockCmsBorderRight"]))
                                        $arrayInsertSuperCms["css"]["border"]["right"]= ($cms["blockCmsBorderRight"] == "true") ? "checked": "";
                                            if(isset($cms["blockCmsBorderColor"]) && $cms["blockCmsBorderColor"] == "#ff007f" 
                                            && isset($cms["blockCmsBorderType"]) && $cms["blockCmsBorderType"] == "solid" 
                                            && isset($cms["blockCmsBorderWidth"]) && $cms["blockCmsBorderWidth"] == "5" 
                                            && isset($cms["blockCmsBorderTop"]) && $cms["blockCmsBorderTop"] == "" 
                                            && isset($cms["blockCmsBorderLeft"]) && $cms["blockCmsBorderLeft"] == "" 
                                            && isset($cms["blockCmsBorderBottom"]) && $cms["blockCmsBorderBottom"] == "" 
                                            && isset($cms["blockCmsBorderRight"]) && $cms["blockCmsBorderRight"] == ""){
                                                $arrayInsertSuperCms["css"]["border"]["width"]= "0";
                                        }
                                            /**** animation */
                                        if(isset($cms["blockCmsAos"]))
                                            $arrayInsertSuperCms["css"]["aos"]["type"]= $cms["blockCmsAos"];
                                        if(isset($cms["blockCmsAosDuration"]))
                                            $arrayInsertSuperCms["css"]["aos"]["duration"]= $cms["blockCmsAosDuration"];
                
                
                                        Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsertSuperCms);
                                        $newCmsID = $arrayInsertSuperCms['_id'];
                
                                        //** create new superCms  lineSeparator Top
                                        if(isset($cms["blockCmsLineSeparatorTop"]) && $cms["blockCmsLineSeparatorTop"] == "true") {
                
                                            $arrayInsertInlineSeparatorTop = array(
                                                "path" => "tpls.blockCms.superCms.elements.lineSeparator",
                
                                                "page" => $cms["page"],
                                                "type" => "blockChild",
                                                "parent" => $cms["parent"],
                                                "creator" => @$cms["creator"],
                                                "collection" => "cms",
                                                "source" => isset($cms["source"])??$cms["source"],
                                                "subtype" => "supercms",
                                                "blockParent" => (String)$newCmsID
                                            );
                                            if(isset($cms["blockCmsLineSeparatorBg"]))
                                                $arrayInsertInlineSeparatorTop["css"]["background"]["color"] = $cms["blockCmsLineSeparatorBg"];
                                            if(isset($cms["blockCmsLineSeparatorTop"]))
                                                $arrayInsertInlineSeparatorTop["css"]["position"]["top"] = $cms["blockCmsLineSeparatorTop"];
                                            if(isset($cms["blockCmsLineSeparatorHeight"]))
                                                $arrayInsertInlineSeparatorTop["css"]["size"]["height"] = $cms["blockCmsLineSeparatorHeight"];
                                            if(isset($cms["blockCmsLineSeparatorIcon"]))
                                                $arrayInsertInlineSeparatorTop["css"]["icon"]["style"] = $cms["blockCmsLineSeparatorIcon"];
                                            if(isset($cms["blockCmsLineSeparatorPosition"])){
                                                $alignPx = "";
                                                if($cms["blockCmsLineSeparatorPosition"] == "center"){
                                                    $alignPx = "50";
                                                }else if($cms["blockCmsLineSeparatorPosition"] == "right"){
                                                    $alignPx = "100";
                                                }
                                                $arrayInsertInlineSeparatorTop["css"]["position"]["left"]= $alignPx ;
                                            }
                                            if(isset($cms["blockCmsLineSeparatorWidth"]))
                                                $arrayInsertInlineSeparatorTop["css"]["size"]["width"] = $cms["blockCmsLineSeparatorWidth"];
                                            Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsertInlineSeparatorTop);
                                        }
                
                                        // ajout Documents
                                        // copy backgroundPhoto
                
                                        $backgroundPhoto = PHDB::find( Document::COLLECTION,array(
                                            "id" => $vId,
                                            "type" => 'cms',
                                            "subKey" => 'blockCmsBgImg',
                                        ));
                                        foreach($backgroundPhoto as $kBg => $vBg){
                                            $arrayDocs = [];
                                            $arrayDocs = $vBg;
                                            $arrayDocs["id"] =  (String)$newCmsID;
                                            $arrayDocs["folder"] = "cms/".$newCmsID."/album";
                                            $arrayDocs["subKey"] = "block";
                                            unset($arrayDocs["_id"]);
                                            if(isset($vBg["name"]) && file_exists( "../../pixelhumain/ph/upload/communecter/cms/".$vId."/album" )){
                                                if( !file_exists("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID)){
                                                    mkdir("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID); 
                                                }
                                                if( !file_exists("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album")){
                                                    mkdir("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album/"); 
                                                    if(file_exists("../../pixelhumain/ph/upload/communecter/cms/".$vId."/album/".$vBg["name"]))
                                                    Yii::$app->fs->copy("communecter/cms/".$vId."/album/".$vBg["name"], "communecter/cms/".$newCmsID."/album/".$vBg["name"]);
                                                }
                                                if(!file_exists("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album/thumb")){
                                                    mkdir("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album/thumb"); 

                                                    if(file_exists("../../pixelhumain/ph/upload/communecter/cms/".$vId."/album/thumb/".$vBg["name"]))
                                                    Yii::$app->fs->copy("communecter/cms/".$vId."/album/thumb/".$vBg["name"], "communecter/cms/".$newCmsID."/album/thumb/".$vBg["name"]);
                                                }
                                                    if(!file_exists("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/medium")){
                                                    mkdir("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album/medium"); 
                                                    if(file_exists("../../pixelhumain/ph/upload/communecter/cms/".$vId."/album/medium/".$vBg["name"]))
                                                
                                                    Yii::$app->fs->copy("communecter/cms/".$vId."/album/medium/".$vBg["name"], "communecter/cms/".$newCmsID."/album/medium/".$vBg["name"]);
                                                }                          
                                                }                               
                                            Yii::app()->mongodb->selectCollection("documents")->insert($arrayDocs);
                                            Document::removeDocumentById((String)$vBg["_id"], true);
                                            
                                            
                                        }
                
                                        //** create new superCms  lineSeparator Bottom
                                        if(isset($cms["blockCmsLineSeparatorBottom"]) && $cms["blockCmsLineSeparatorBottom"] == "true") {
                
                                            $arrayInsertInlineSeparatorBottom = array(
                                                "path" => "tpls.blockCms.superCms.elements.lineSeparator",
                
                                                "page" => $cms["page"],
                                                "type" => "blockChild",
                                                "parent" => $cms["parent"],
                                                "creator" => @$cms["creator"],
                                                "collection" => "cms",
                                                "source" => isset($cms["source"])??$cms["source"],
                                                "subtype" => "supercms",
                                                "blockParent" => (String)$newCmsID
                                            );
                                            if(isset($cms["blockCmsLineSeparatorBg"]))
                                                $arrayInsertInlineSeparatorBottom["css"]["background"]["color"] = $cms["blockCmsLineSeparatorBg"];
                                            if(isset($cms["blockCmsLineSeparatorBottom"]))
                                                $arrayInsertInlineSeparatorBottom["css"]["position"]["bottom"] = $cms["blockCmsLineSeparatorBottom"];
                                            if(isset($cms["blockCmsLineSeparatorHeight"]))
                                                $arrayInsertInlineSeparatorBottom["css"]["size"]["height"] = $cms["blockCmsLineSeparatorHeight"];
                                            if(isset($cms["blockCmsLineSeparatorIcon"]))
                                                $arrayInsertInlineSeparatorBottom["css"]["icon"]["style"] = $cms["blockCmsLineSeparatorIcon"];
                                            if(isset($cms["blockCmsLineSeparatorPosition"])){
                                                $alignPx = "";
                                                if($cms["blockCmsLineSeparatorPosition"] == "center"){
                                                    $alignPx = "50";
                                                }else if($cms["blockCmsLineSeparatorPosition"] == "right"){
                                                    $alignPx = "100";
                                                }
                                                $arrayInsertInlineSeparatorTop["css"]["position"]["left"]= $alignPx ;
                                            }
                                            if(isset($cms["blockCmsLineSeparatorWidth"]))
                                                $arrayInsertInlineSeparatorBottom["css"]["size"]["width"] = $cms["blockCmsLineSeparatorWidth"];
                                            Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsertInlineSeparatorBottom);
                
                                        } 
                                        $this->updateTplParents(new MongoId($cms['_id']), $newCmsID);		
                                    }
                                }
                                $output.= "".count($allCms)." cms du costum <a href ='".$baseUrl."/costum/co/index/slug/".$c["slug"]."' target='_blank'>".$c["slug"]."<a> migré <br>";
                            }else{
                                $output .= "Aucun cms à migrer pour la costum".@$c["slug"]."<br>";
                            }
                        }
                    }
                }
            }else{
                $output .= "Vous n'êtes pas autorisé";
            }
            return $output;
        }else{
            $where = array(
                "slug" => $slug,"costum.slug" => array('$exists'=>1)
            );
            $costumProject = PHDB::findOne("projects",$where);
            $costumOrga = PHDB::findOne("organizations",$where);
    
            if ($costumOrga != null) {
                $costums = $costumOrga;
            }elseif ($costumProject != null){
                $costums = $costumProject;
            }
    
            $output = "";
    
            $output .= "<center><font style='font-size: 25px'>Data migration des anciens blocs CMS vers superCMS </font></center>";
    
            if ($costums == null) {
                $output .= "<center><pre style='font-size: 25px'>COSTUM <span style='color:red; border: solid 1px;'> ".$slug." </span> is not found</pre></center>";
            }else{
                $output .=""; 
                $allPage = ["welcome"];
                
                foreach($costums["costum"]["app"] as $kAap => $vAap){
                    $allPage[]= str_replace("#","",$kAap);
                }
    
                $output .= "<center><pre style='font-size: 25px'>COSTUM <span style='color:green; border: solid 1px;'> ".$slug." </span></pre></center>";
                $output .= "<center><a href='/co2/CmsRefactorMigration/migrate/slug/".$slug."' style='font-size: 18px'>Liste des pages du costum </a></center>";
                if(!$page){
                    $output .= "<div style='padding-left:20%;padding-right:20%'>";
                    foreach($allPage as $kPage => $vPage){
                        $output .= "<a href='/co2/CmsRefactorMigration/migrate/slug/".$slug."/page/".$vPage." 'target='_blank' class='btn btn-danger'>Liste des cms de la page ".$vPage." </a> <br>";
                        
                    }
                    $output .= "</div>";
                }else{
                    $output .= "<center><pre style='font-size: 18px'>Page <span style='color:green; border: solid 1px;'> #".$page." </span></pre></center>";
                    $cmsList = Cms::getCmsByWhere(array(
                        "parent.".(string)$costums["_id"] => array('$exists'=>1),                        
                        '$and'=>array(
                            array(
                                '$or'=> array(
                                    array("type" => array('$exists' => 0)),
                                    array("type" => "blockCopy")
                                )
                            ),
                            array("page" => $page), 
                            array(
                                '$and'=> array(
                                    array("path" => array('$ne' => "tpls.blockCms.superCms.container")),
                                    array("path" => array('$ne' => "tpls.blockCms.projects.callForProjects"))
                                )
                            )
                        )
                    )); 


                    $output .= "<center>----------------------------------------------------------</center>";			
                    if(empty($cmsList)){
                        $output .= "<center><pre style='font-size: 25px;color:red;'>Aucun cms à migrer </pre></center>";
                        $output .= "<center style='font-size: 25px'>La page <span style='color:red;border: solid 1px;'> #".$page." </span> n'exist pas ou il n'a aucun block CMS.</center>";
                    }else{
                        $output .= "<form style='padding-left:20%;padding-right:20%'>";
                        $ids = "";
                        foreach ($cmsList as $key => $value) {
                            $ids .= (string)$value["_id"].",";
                            $output .= "<pre>";
                            $output .= isset($value["name"])??$value["name"]."<br>";
                            $output .= "<a href='/co2/CmsRefactorMigration/migrate/slug/".$slug."/page/".$page."/id/".(string)$value["_id"]."' class='btn btn-danger'>Migrer</a> ";
                            $output .= "<span style='color:red'>";
                            $output .= $value["path"];
                            $output .= "</span>";
                            $output .= "</pre><hr>";
                        }
                        $output .= "<center><a href='/co2/CmsRefactorMigration/migrate/slug/".$slug."/page/".$page."/id/".$ids."' style='height: 40px;width: 100px;font-weight: bold;'>Migrer tous</a></center>";
                        $output .= "</form>";
                    }
                    
                }
                if($id){
                    $allId = explode(",", $id);
                    foreach($allId as $kId => $vId){             
                         
                        if($vId != ""){
                            /* **************************************
                            *
                            *    Preparing data 
                            *
                            ***************************************** */
                            $cms = Element::getElementById($vId,Cms::COLLECTION);
                            $childList = (isset($_POST['cmsList']) ? $_POST['cmsList'] : [$vId]);
                            //** create new superCms
                            $arrayInsertSuperCms = array(
                                "path" => "tpls.blockCms.superCms.container",
                                "page" => $page,
                                "type" => "blockCopy",
                                "parent" => $cms["parent"],
                                "creator" => $cms["creator"],
                                "collection" => "cms",
                                "subtype" => "supercms",
                                "class" => [
                                    "width" => "sp-cms-std"
                                ],
                                "cmsList" => $childList
                            );
                            if(isset($cms["name"]))
                                $arrayInsertSuperCms["name"] = $cms["name"];
    
                             if(isset($cms["haveTpl"]))
                                 $arrayInsertSuperCms["haveTpl"] = $cms["haveTpl"];
    
                             if( isset($cms["tplParent"]))
                                 $arrayInsertSuperCms["tplParent"] = $cms["tplParent"];
    
                            if(isset($cms["source"]))
                                $arrayInsertSuperCms["source"] = $cms["source"];
    
                            if(isset($cms["position"]))
                                $arrayInsertSuperCms["position"] = $cms["position"];
                            
    
    
                            if(isset($cms["modeMd"]))
                                $arrayInsertSuperCms["modeMd"] = $cms["modeMd"];
                            if(isset($cms["modeSm"]))
                                $arrayInsertSuperCms["modeSm"] = $cms["modeSm"];
                            if(isset($cms["modeXs"]))
                                $arrayInsertSuperCms["modeXs"] = $cms["modeXs"];
    
                                
                            if(isset($cms["blockCmsBgPaint"]))
                                $arrayInsertSuperCms["blockCmsBgPaint"] = $cms["blockCmsBgPaint"];
                            if(isset($cms["blockCmsBgType"])  && $cms["blockCmsBgType"] == "paint")
                                $arrayInsertSuperCms["blockCmsBgType"] = $cms["blockCmsBgType"];
    
                            if(isset($cms["marginBottom"]))
                                $arrayInsertSuperCms["css"]["margin"]["bottom"] = $cms["marginBottom"]."px";
                            if(isset($cms["marginTop"]))
                                $arrayInsertSuperCms["css"]["margin"]["top"] = $cms["marginTop"]."px";
                            if(isset($cms["paddingBottom"]))
                                $arrayInsertSuperCms["css"]["padding"]["bottom"] = $cms["paddingBottom"]."px";
                            if(isset($cms["paddingLeft"]))
                                $arrayInsertSuperCms["css"]["padding"]["left"] = $cms["paddingLeft"]."px";
                            if(isset($cms["paddingRight"]))
                                $arrayInsertSuperCms["css"]["padding"]["right"] = $cms["paddingRight"]."px";
                            if(isset($cms["paddingTop"]))
                                $arrayInsertSuperCms["css"]["padding"]["top"] = $cms["paddingTop"]."px";
    
    
                            /*** Background */
                            if(isset($cms["blockCmsBgColor"]))
                                $arrayInsertSuperCms["css"]["background"]["color"] = $cms["blockCmsBgColor"];
                            if(isset($cms["blockCmsBgSize"]))
                                $arrayInsertSuperCms["css"]["background"]["size"] = $cms["blockCmsBgSize"];
                            if(isset($cms["blockCmsBgRepeat"]))
                                $arrayInsertSuperCms["css"]["background"]["repeat"] = $cms["blockCmsBgRepeat"];
                            if(isset($cms["blockCmsBgPosition"]))
                                $arrayInsertSuperCms["css"]["background"]["position"] = $cms["blockCmsBgPosition"];
                            if(isset($cms["blockCmsBgTarget"]))
                                $arrayInsertSuperCms["css"]["background"]["target"] = $cms["blockCmsBgTarget"]; 
                            if($cms["path"] == "tpls.blockCms.text.backgroundPhotoParallax")
                                $arrayInsertSuperCms["css"]["background"]["attachment"] = "fixed";
                            
                            
                            if(isset($cms["modeLg"])){ 
                                $widthBlock = ($cms["modeLg"]*100)/12;
                                $arrayInsertSuperCms["css"]["size"]["width"] = $widthBlock."%";
                            }else{
                                $arrayInsertSuperCms["css"]["size"]["width"] = "100%";
                            }
    
    
                            // border 
                            if(isset($cms["blockCmsBorderColor"]))
                                $arrayInsertSuperCms["css"]["border"]["color"]= $cms["blockCmsBorderColor"];
                            if(isset($cms["blockCmsBorderType"]))
                                $arrayInsertSuperCms["css"]["border"]["type"]= $cms["blockCmsBorderType"];
                            if(isset($cms["blockCmsBorderWidth"]))
                                $arrayInsertSuperCms["css"]["border"]["width"]= $cms["blockCmsBorderWidth"];
                            if(isset($cms["blockCmsBorderTop"]))
                                $arrayInsertSuperCms["css"]["border"]["top"]= ($cms["blockCmsBorderTop"] == "true") ? "checked": "";
                            if(isset($cms["blockCmsBorderBottom"]))
                                $arrayInsertSuperCms["css"]["border"]["bottom"]= ($cms["blockCmsBorderBottom"] == "true") ? "checked": "";
                            if(isset($cms["blockCmsBorderLeft"]))
                                $arrayInsertSuperCms["css"]["border"]["left"]= ($cms["blockCmsBorderLeft"] == "true") ? "checked": "";
                            if(isset($cms["blockCmsBorderRight"]))
                                $arrayInsertSuperCms["css"]["border"]["right"]= ($cms["blockCmsBorderRight"] == "true") ? "checked": "";

                            if(isset($cms["blockCmsBorderColor"]) && $cms["blockCmsBorderColor"] == "#ff007f" 
                            && isset($cms["blockCmsBorderType"]) && $cms["blockCmsBorderType"] == "solid" 
                            && isset($cms["blockCmsBorderWidth"]) && $cms["blockCmsBorderWidth"] == "5" 
                            && isset($cms["blockCmsBorderTop"]) && $cms["blockCmsBorderTop"] == "" 
                            && isset($cms["blockCmsBorderLeft"]) && $cms["blockCmsBorderLeft"] == "" 
                            && isset($cms["blockCmsBorderBottom"]) && $cms["blockCmsBorderBottom"] == "" 
                            && isset($cms["blockCmsBorderRight"]) && $cms["blockCmsBorderRight"] == ""){
                                $arrayInsertSuperCms["css"]["border"]["width"]= "0";
                            }
                                /**** animation */
                            if(isset($cms["blockCmsAos"]))
                                $arrayInsertSuperCms["css"]["aos"]["type"]= $cms["blockCmsAos"];
                            if(isset($cms["blockCmsAosDuration"]))
                                $arrayInsertSuperCms["css"]["aos"]["duration"]= $cms["blockCmsAosDuration"];
    
    
                            Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsertSuperCms);
                            $newCmsID = $arrayInsertSuperCms['_id'];
  
                            //** create new superCms  lineSeparator Top
                            if(isset($cms["blockCmsLineSeparatorTop"]) && $cms["blockCmsLineSeparatorTop"] == "true") {
    
                                $arrayInsertInlineSeparatorTop = array(
                                    "path" => "tpls.blockCms.superCms.elements.lineSeparator",
    
                                    "page" => $page,
                                    "type" => "blockChild",
                                    "parent" => $cms["parent"],
                                    "creator" => $cms["creator"],
                                    "collection" => "cms",
                                    "source" => isset($cms["source"])??$cms["source"],
                                    "subtype" => "supercms",
                                    "blockParent" => (String)$newCmsID
                                );
                                if(isset($cms["blockCmsLineSeparatorBg"]))
                                    $arrayInsertInlineSeparatorTop["css"]["background"]["color"] = $cms["blockCmsLineSeparatorBg"];
                                if(isset($cms["blockCmsLineSeparatorTop"]))
                                    $arrayInsertInlineSeparatorTop["css"]["position"]["top"] = $cms["blockCmsLineSeparatorTop"];
                                if(isset($cms["blockCmsLineSeparatorHeight"]))
                                    $arrayInsertInlineSeparatorTop["css"]["size"]["height"] = $cms["blockCmsLineSeparatorHeight"];
                                if(isset($cms["blockCmsLineSeparatorIcon"]))
                                    $arrayInsertInlineSeparatorTop["css"]["icon"]["style"] = $cms["blockCmsLineSeparatorIcon"];
                                if(isset($cms["blockCmsLineSeparatorPosition"])){
                                    $alignPx = "";
                                    if($cms["blockCmsLineSeparatorPosition"] == "center"){
                                        $alignPx = "50";
                                    }else if($cms["blockCmsLineSeparatorPosition"] == "right"){
                                        $alignPx = "100";
                                    }
                                    $arrayInsertInlineSeparatorTop["css"]["position"]["left"]= $alignPx ;
                                }
                                if(isset($cms["blockCmsLineSeparatorWidth"]))
                                    $arrayInsertInlineSeparatorTop["css"]["size"]["width"] = $cms["blockCmsLineSeparatorWidth"];
                                Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsertInlineSeparatorTop);
                            }
    
                            // ajout Documents
                            // copy backgroundPhoto
    
                            $backgroundPhoto = PHDB::find( Document::COLLECTION,array(
                                "id" => $vId,
                                "type" => 'cms',
                                "subKey" => 'blockCmsBgImg',
                            ));
                            foreach($backgroundPhoto as $kBg => $vBg){
                                $arrayDocs = [];
                                $arrayDocs = $vBg;
                                $arrayDocs["id"] =  (String)$newCmsID;
                                $arrayDocs["folder"] = "cms/".$newCmsID."/album";
                                $arrayDocs["subKey"] = "block";
                                unset($arrayDocs["_id"]);
                                if(isset($vBg["name"]) && file_exists( "../../pixelhumain/ph/upload/communecter/cms/".$vId."/album" )){
                                    if( !file_exists("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID)){
                                        mkdir("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID); 
                                    }
                                    if( !file_exists("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album")){
                                        mkdir("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album/"); 
                                        Yii::$app->fs->copy("communecter/cms/".$vId."/album/".$vBg["name"], "communecter/cms/".$newCmsID."/album/".$vBg["name"]);
                                    }
                                    if(!file_exists("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album/thumb")){
                                        mkdir("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album/thumb"); 
                                        Yii::$app->fs->copy("communecter/cms/".$vId."/album/thumb/".$vBg["name"], "communecter/cms/".$newCmsID."/album/thumb/".$vBg["name"]);
                                    }
                                     if(!file_exists("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/medium")){
                                        mkdir("../../pixelhumain/ph/upload/communecter/cms/".$newCmsID."/album/medium"); 
                                        Yii::$app->fs->copy("communecter/cms/".$vId."/album/medium/".$vBg["name"], "communecter/cms/".$newCmsID."/album/medium/".$vBg["name"]);
                                    }                          
                                 }                               
                                Yii::app()->mongodb->selectCollection("documents")->insert($arrayDocs);
                                Document::removeDocumentById((String)$vBg["_id"], true);
                                
                                
                            }
    
                            //** create new superCms  lineSeparator Bottom
                            if(isset($cms["blockCmsLineSeparatorBottom"]) && $cms["blockCmsLineSeparatorBottom"] == "true") {
    
                                $arrayInsertInlineSeparatorBottom = array(
                                    "path" => "tpls.blockCms.superCms.elements.lineSeparator",
    
                                    "page" => $page,
                                    "type" => "blockChild",
                                    "parent" => $cms["parent"],
                                    "creator" => $cms["creator"],
                                    "collection" => "cms",
                                    "source" => isset($cms["source"])??$cms["source"],
                                    "subtype" => "supercms",
                                    "blockParent" => (String)$newCmsID
                                );
                                if(isset($cms["blockCmsLineSeparatorBg"]))
                                    $arrayInsertInlineSeparatorBottom["css"]["background"]["color"] = $cms["blockCmsLineSeparatorBg"];
                                if(isset($cms["blockCmsLineSeparatorBottom"]))
                                    $arrayInsertInlineSeparatorBottom["css"]["position"]["bottom"] = $cms["blockCmsLineSeparatorBottom"];
                                if(isset($cms["blockCmsLineSeparatorHeight"]))
                                    $arrayInsertInlineSeparatorBottom["css"]["size"]["height"] = $cms["blockCmsLineSeparatorHeight"];
                                if(isset($cms["blockCmsLineSeparatorIcon"]))
                                    $arrayInsertInlineSeparatorBottom["css"]["icon"]["style"] = $cms["blockCmsLineSeparatorIcon"];
                                if(isset($cms["blockCmsLineSeparatorPosition"])){
                                    $alignPx = "";
                                    if($cms["blockCmsLineSeparatorPosition"] == "center"){
                                        $alignPx = "50";
                                    }else if($cms["blockCmsLineSeparatorPosition"] == "right"){
                                        $alignPx = "100";
                                    }
                                    $arrayInsertInlineSeparatorTop["css"]["position"]["left"]= $alignPx ;
                                }
                                if(isset($cms["blockCmsLineSeparatorWidth"]))
                                    $arrayInsertInlineSeparatorBottom["css"]["size"]["width"] = $cms["blockCmsLineSeparatorWidth"];
                                Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsertInlineSeparatorBottom);
    
                            }
    
                            if(isset($_POST['cmsList'])){
                                foreach ($_POST['cmsList'] as $key => $value) {
                                    $this->updateTplParents(new MongoId($value), $newCmsID);
                                }
                                PHDB::remove(Cms::COLLECTION, ["_id" => $cms['_id']]);
                                $costum = CacheHelper::getCostum();
                                if(isset($costum["contextType"]) && isset($costum["contextId"])){
                                    $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
                                }
                                $titleArray = [1, 2, 3, 4, 5, 6];
                                $param = array(
                                    "v" => PHDB::findOneById(CMS::COLLECTION, $newCmsID),
                                    "i" => $indice,
                                    "titleArray" => $titleArray,
                                    "el" => $el,
                                    "page" => $page,
                                    "cmsList"    => PHDB::find(CMS::COLLECTION, array("_id" => array('$in' => [$newCmsID]), "type" => "blockChild")),
                                    "costumData" => $costum,
                                    "canEdit"    => true,
                                );
                                $response = $controller->renderPartial("costum.views.cmsBuilder.blockEngine.partials.chargeBlock", $param);
                            }else{
                                $this->updateTplParents($cms['_id'], $newCmsID);
                            }
                            $output = "<center><font style='font-size: 25px'>bloc migré </font> <br>";
                            $output .= "<a href='/co2/CmsRefactorMigration/migrate/slug/".$slug."/page/".$page."' class='btn btn-danger'>Liste des cms </a> </center>";		 		
                        }
                    }
                    
                }
            }
            return isset($response) ? $response : $output;
        }
        
	}
    private function updateTplParents($id, $newID)
    {
        // update cms source                     
        $unset = [
            "paddingBottom" => true,
            "blockCmsBgColor"  => true,
            "blockCmsAosDuration"  => true,
            "blockCmsAos"  => true,
            "blockCmsBgImg"  => true ,
            "blockCmsBgPaint" => true , 
            "blockCmsBgPosition"  => true ,
            "blockCmsBgRepeat" => true , 
            "blockCmsBgSize" => true , 
            "blockCmsBgTarget"  => true, 
            "blockCmsBgType"  => true ,                       
            "blockCmsBorderBottom" => true, 
            "blockCmsBorderColor" => true ,
            "blockCmsBorderLeft"  => true, 
            "blockCmsBorderRight" => true, 
            "blockCmsBorderTop"  => true,
            "blockCmsBorderType"  => true,
            "blockCmsBorderWidth"  => true,
            "blockCmsLineSeparatorBg" => true ,
            "blockCmsLineSeparatorBottom"  => true,
            "blockCmsLineSeparatorHeight"  => true,
            "blockCmsLineSeparatorIcon"   => true,
            "blockCmsLineSeparatorPosition" => true , 
            "blockCmsLineSeparatorTop"  => true,
            "blockCmsLineSeparatorWidth"  => true,
            "marginBottom"  => true , 
            "marginTop"   => true ,
            "modeLg"   => true ,
            "modeMd"   => true ,
            "modeSm"   => true ,
            "modeXs"   => true ,
            "paddingBottom" => true  ,
            "paddingLeft"  => true  ,
            "paddingRight" => true  , 
            "paddingTop" => true  ,
            "haveTpl"   => true,
            "tplParent"   => true,
            "position"   => true,
        ];
        
        PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
            '$unset' => $unset,
            '$set'=> [
                "blockParent" => (String)$newID,
                "type" => "blockChild"
            ]
        ]);
    }	
}