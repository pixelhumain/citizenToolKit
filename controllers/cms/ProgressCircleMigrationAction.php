<?php 
    namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

    use Cms;
    use MongoId;
    use PHDB;
    use Rest;
    use Role;
    use Yii;

    class ProgressCircleMigrationAction extends \PixelHumain\PixelHumain\components\Action {
        public function run($path){
            if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
                $progressCircle = PHDB::find(Cms::COLLECTION, array(
                    "path" => $path,
                    "type" => array('$ne' => "blockCms"),
                    "alreadyupdated" => array('$exists' => 0)
                ));
                $i = 0;
                foreach ($progressCircle as $key => $circle) {
                    $this->updateParams($circle);
                    $i++;
                }
                return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
            } else {
                echo "vous n'avez pas le droit"; 
            }
        }

        private function updateParams($circles){
            $paramsData = [];
            if($circles["path"] == "tpls.blockCms.graph.progressCircle")
            {
                $paramsData = [
                    "percentColor",
                    "emptyColor",
                    "completeColor"
                ];
            } else if($circles["path"] == "tpls.blockCms.graph.progressCircleMultiple"){
                $paramsData = [
                    "percentColor",
                    "emptyColor",
                    "completeColor",
                    "textColor"
                ];
            }
            
            $id = new MongoId((string)$circles['_id']);
            unset($circles["_id"]);
            foreach ($paramsData as $index => $param) {
                if(isset($circles[$param])){
                    if($index == 0){
                        $circles["css"]["percentColor"]["fill"] = $circles[$param];
                    }
                    if($index == 1){
                        $circles["css"]["emptyColor"]["fill"] = $circles[$param];
                    }
                    if($index == 2){
                        $circles["css"]["completeColor"]["fill"] = $circles[$param];
                    }
                    if($index == 3){
                        $circles["css"]["textColor"]["color"] = $circles[$param];
                    }
                    unset($circles[$param]);
                }
            }
            // $circles["alreadyupdated"] = 1;
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$set' => $circles
            ]);
            $this->removeOldAttribute($id);
        }

        public function removeOldAttribute($id)
        {
            $unset = [
                "percentColor" => true,
                "emptyColor" => true,
                "completeColor" => true,
                "textColor" => true
            ];
            PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
                '$unset' => $unset
            ]);
        }
    }
?>