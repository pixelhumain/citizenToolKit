<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use MongoId;
use PHDB,Costum,Organization,Element,Yii,Document,Project,Role;

class MigrateAddClassAction extends \PixelHumain\PixelHumain\components\Action{
    public function run() {$controller=$this->getController(); 
        $output = "";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $count = 0;
            $output = "";
            $parent = [Organization::COLLECTION, Project::COLLECTION] ;
            foreach($parent as $v){
                $costumParent=PHDB::find($v, 
                            array(
                                '$or'=> array(
                                    array("costum.htmlConstruct.header.menuTop.left.addClass" => array('$exists' => 1)),
                                    array("costum.htmlConstruct.header.menuTop.right.addClass" => array('$exists' => 1))
                                )
                            )
                        );
                  
                foreach($costumParent as $kc => $vcostum){                                    
                    $set = array();
                    if(isset($vcostum["costum"]["htmlConstruct"]["header"]["menuTop"]["left"]["addClass"])){
                        $addClassLeft = "";
                        $colLeft = array();
                        $paddingLeft = "";   
                        $margingLeft = "";   
                        $lptop = "0";
                        $lpbottom = "0";
                        $lpleft = "0";
                        $lpright = "0"; 
                        $lmtop = "0";
                        $lmbottom = "0";
                        $lmleft = "0";
                        $lmright = "0"; 
                        $hiddenXsl = "";
                        $hiddenSml = "";
                        $hiddenMdl = ""; 
                        $addClassTopLeft = $vcostum["costum"]["htmlConstruct"]["header"]["menuTop"]["left"]["addClass"]; 
                        $arrayAddClassTopLeft = explode(" ", $addClassTopLeft);
                        foreach($arrayAddClassTopLeft as $kl => $vl){
                            if (str_contains($vl, 'col-') && !str_contains($vl, 'offset') ) { 
                                $class = explode("-",trim($vl));
                                if($class[1] == "lg"){
                                    $colLeft["colSm"] =  $class[2]; 
                                }
                                if($class[1] == "md"){
                                    $colLeft["colMd"] =  $class[2]; 
                                }
                                if($class[1] == "sm"){
                                    $colLeft["colSm"] =  $class[2]; 
                                }
                                if($class[1] == "xs"){
                                    $colLeft["colXs"] =  $class[2]; 
                                }                                
                            }elseif(str_contains($vl, 'padding-')){ 
                                $paddingl = explode("-",trim($vl));                             
                                if($paddingl[1] == "top"){
                                    $lptop =  $paddingl[2]; 
                                }
                                if($paddingl[1] == "bottom"){
                                    $lpbottom =  $paddingl[2]; 
                                }
                                if($paddingl[1] == "left"){
                                    $lpleft =  $paddingl[2]; 
                                }
                                if($paddingl[1] == "right"){
                                    $lpright =  $paddingl[2]; 
                                }
                                $paddingLeft = $lptop."px ".$lpright."px ".$lpbottom."px ".$lpleft."px";
                            }elseif(str_contains($vl, 'margin-')){ 
                                $margingl = explode("-",trim($vl));                             
                                if($margingl[1] == "top"){
                                    $lmtop =  $margingl[2]; 
                                }
                                if($margingl[1] == "bottom"){
                                    $lmbottom =  $margingl[2]; 
                                }
                                if($margingl[1] == "left"){
                                    $lmleft =  $margingl[2]; 
                                }
                                if($margingl[1] == "right"){
                                    $lmright =  $margingl[2]; 
                                }
                                $margingLeft = $lmtop."px ".$lmright."px ".$lmbottom."px ".$lmleft."px";
                            }elseif(str_contains($vl, 'hidden-')){
                                $classhiddenl = explode("-",trim($vl)); 
                                if($classhiddenl[1] == "xs"){
                                    $hiddenXsl = "on";
                                    $hiddenSml = "off";
                                    $hiddenMdl = "off";
                                }
                                if($classhiddenl[1] == "sm"){
                                    $hiddenXsl = "off";
                                    $hiddenSml = "on";
                                    $hiddenMdl = "off";
                                }
                                if($classhiddenl[1] == "md"){
                                    $hiddenMdl = "on";
                                    $hiddenXsl = "off";
                                    $hiddenSml = "off";
                                }
                                if($classhiddenl[1] == "lg"){
                                    $hiddenMdl = "on";
                                    $hiddenXsl = "off";
                                    $hiddenSml = "off";
                                }
                            }elseif(str_contains($vl, 'visible-')){ 
                                $classvisible = explode("-",trim($vl)); 
                                if($classvisible[1] == "xs"){
                                    $hiddenXsl = "off";
                                    $hiddenMdl = "on";
                                    $hiddenSml = "on";
                                }
                                if($classvisible[1] == "sm"){
                                    $hiddenSml = "off";  
                                    $hiddenXsl = "on";
                                    $hiddenMdl = "on";
                                    
                                }
                                if($classvisible[1] == "md"){
                                    $hiddenMdl = "off";
                                    $hiddenSml = "on";  
                                    $hiddenXsl = "on";
                                }
                                if($classvisible[1] == "lg"){
                                    $hiddenMdl = "off";
                                    $hiddenSml = "on";  
                                    $hiddenXsl = "on";
                                }
                            }elseif(str_contains($vl, 'text-left')){
                                $set["costum.css.menuTop.left.textAlign"] = "left";
                            }elseif(str_contains($vl, 'text-right')){
                                $set["costum.css.menuTop.left.textAlign"] = "right";                                
                            }elseif(str_contains($vl, 'text-center')){
                                $set["costum.css.menuTop.left.textAlign"] = "center";                                
                            }else{
                                $addClassLeft .= $vl." "; 
                            }
                        }
                    }
                    if(isset($vcostum["costum"]["htmlConstruct"]["header"]["menuTop"]["right"]["addClass"])){
                        $addClassRight = "";
                        $paddingRight = "";
                        $marginRight = "";
                        $colRight = array();
                        $rptop = "0";
                        $rpbottom = "0";
                        $rpleft = "0";
                        $rpright = "0"; 
                        $rmtop = "0";
                        $rmbottom = "0";
                        $rmleft = "0";
                        $rmright = "0"; 
                        $hiddenXsr = "";
                        $hiddenSmr = "";
                        $hiddenMdr = ""; 
                        $addClassTopRight = $vcostum["costum"]["htmlConstruct"]["header"]["menuTop"]["right"]["addClass"];
                        $arrayAddClassTopRight = explode(" ", $addClassTopRight);
                        foreach($arrayAddClassTopRight as $kr => $vr){
                            if (str_contains($vr, 'col-') && !str_contains($vr, 'offset')) { 
                                $classr = explode("-",trim($vr));
                                if($classr[1] == "lg"){
                                    $colRight["colMd"] =  $classr[2];
                                }
                                if($classr[1] == "md"){
                                    if($classr[2] == "3"){
                                        $colRight["colMd"] = "";
                                    }else{
                                        $colRight["colMd"] =  $classr[2]; 
                                    }
                                }
                                if($classr[1] == "sm"){
                                    $colRight["colSm"] =  $classr[2]; 
                                }
                                if($classr[1] == "xs"){
                                    $colRight["colXs"] =  $classr[2]; 
                                }                                
                            }elseif(str_contains($vr, 'padding-')){ 
                                    $paddingr = explode("-",trim($vr));
                                    
                                    if($paddingr[1] == "top"){
                                        $rptop =  $paddingr[2]; 
                                    }
                                    if($paddingr[1] == "bottom"){
                                        $rpbottom =  $paddingr[2]; 
                                    }
                                    if($paddingr[1] == "left"){
                                        $rpleft =  $paddingr[2]; 
                                    }
                                    if($paddingr[1] == "right"){
                                        $rpright =  $paddingr[2]; 
                                    }
                                    $paddingRight = $rptop."px ".$rpright."px ".$rpbottom."px ".$rpleft."px";
                            }elseif(str_contains($vr, 'margin-')){ 
                                    $marginr = explode("-",trim($vr));
                                    
                                    if($marginr[1] == "top"){
                                        $rmtop =  $marginr[2]; 
                                    }
                                    if($marginr[1] == "bottom"){
                                        $rmbottom =  $marginr[2]; 
                                    }
                                    if($marginr[1] == "left"){
                                        $rmleft =  $marginr[2]; 
                                    }
                                    if($marginr[1] == "right"){
                                        $rmright =  $marginr[2]; 
                                    }
                                    $marginRight = $rmtop."px ".$rmright."px ".$rmbottom."px ".$rmleft."px";
                            }elseif(str_contains($vr, 'hidden-')){
                                $classhiddenr = explode("-",trim($vr)); 
                                if($classhiddenr[1] == "xs"){
                                    $hiddenXsr = "on";
                                    $hiddenMdr = "off";
                                    $hiddenSmr = "off";
                                }
                                if($classhiddenr[1] == "sm"){
                                    $hiddenSmr = "on";
                                    $hiddenXsr = "off";
                                    $hiddenMdr = "off";
                                }
                                if($classhiddenr[1] == "md"){
                                    $hiddenMdr = "on";
                                    $hiddenSmr = "off";
                                    $hiddenXsr = "off";
                                }
                                if($classhiddenr[1] == "lg"){
                                    $hiddenMdr = "on";
                                    $hiddenSmr = "off";
                                    $hiddenXsr = "off";
                                }
                            }elseif(str_contains($vr, 'visible-')){
                                $classvisibler = explode("-",trim($vr)); 
                                if($classvisibler[1] == "xs"){
                                    $hiddenXsr = "off";
                                    $hiddenMdr = "on";
                                    $hiddenSmr = "on";
                                }
                                if($classvisibler[1] == "sm"){
                                    $hiddenSmr = "off";
                                    $hiddenXsr = "on";
                                    $hiddenMdr = "on";
                                }
                                if($classvisibler[1] == "md"){
                                    $hiddenMdr = "off"; 
                                    $hiddenSmr = "on";
                                    $hiddenXsr = "on";
                                }
                                if($classvisibler[1] == "lg"){
                                    $hiddenMdr = "off";
                                    $hiddenSmr = "on";
                                    $hiddenXsr = "on";
                                }
                            }elseif(str_contains($vr, 'text-left')){
                                $set["costum.css.menuTop.right.textAlign"] = "left";
                            }elseif(str_contains($vr, 'text-right')){
                                $set["costum.css.menuTop.right.textAlign"] = "right";                                
                            }elseif(str_contains($vr, 'text-center')){
                                $set["costum.css.menuTop.right.textAlign"] = "center";                                
                            }else{
                                $addClassRight .= $vr."";
                            }
                        }
                    }
                    if(isset($hiddenXsr) && $hiddenXsr != ""){ 
                        $set["costum.css.menuTop.right.hideOnMobil"] = $hiddenXsr;
                    }
                    if(isset($hiddenMdr) && $hiddenMdr != ""){ 
                        $set["costum.css.menuTop.right.hideOnDesktop"] = $hiddenMdr;
                    }
                    if(isset($hiddenSmr) && $hiddenSmr != ""){ 
                        $set["costum.css.menuTop.right.hideOnTablet"] = $hiddenSmr;
                    }


                    if(isset($hiddenXsl) && $hiddenXsl != ""){ 
                        $set["costum.css.menuTop.left.hideOnMobil"] = $hiddenXsl;
                    }
                    if(isset($hiddenMdl) && $hiddenMdl != ""){ 
                        $set["costum.css.menuTop.left.hideOnDesktop"] = $hiddenMdl;
                    }
                    if(isset($hiddenSml) && $hiddenSml != ""){ 
                        $set["costum.css.menuTop.left.hideOnTablet"] = $hiddenSml;
                    }


                    if(isset($paddingLeft) && $paddingLeft != ""){ 
                        $set["costum.css.menuTop.left.padding"] = $paddingLeft;
                    }
                    if(isset($margingLeft) && $margingLeft != ""){ 
                        $set["costum.css.menuTop.left.margin"] = $margingLeft;
                    }
                    if(isset($paddingRight) && $paddingRight != ""){
                        $set["costum.css.menuTop.right.padding"] = $paddingRight;
                    }
                    if(isset($marginRight) && $marginRight != ""){
                        $set["costum.css.menuTop.right.margin"] = $marginRight;
                    }
                    if(isset($colRight) && !empty($colRight))
                        $set["costum.css.menuTop.right.columnWidth"] = $colRight;
                    if(isset($colLeft)&& !empty($colLeft))
                        $set["costum.css.menuTop.left.columnWidth"] = $colLeft;                        
                    if(isset($addClassLeft))
                        $set["costum.htmlConstruct.header.menuTop.left.addClass"] = preg_replace('/\s\s+/', ' ', $addClassLeft);               
                    if(isset($addClassRight))
                        $set["costum.htmlConstruct.header.menuTop.right.addClass"] = preg_replace('/\s\s+/', ' ', $addClassRight);    
                        
                    if(!empty($set)){ 
                        $count++;
                        PHDB::update($v,[ "_id" => new MongoId($kc) ],[ 
                            '$set'=> $set
                        ]);
                    }
                }
            }
            return $count." costum mis à jour";
        }
    }
}