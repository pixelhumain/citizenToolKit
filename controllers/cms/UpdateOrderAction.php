<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CAction, Element, Cms, PHDB, MongoId, Yii, Rest, Authorisation, DataValidator;
class UpdateOrderAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $_POST = DataValidator::clearUserInput($_POST); 
        if (Authorisation::isInterfaceAdmin()) {
            $controller=$this->getController(); 
            foreach ($_POST["order"] as $key => $value) {    
              PHDB::update( "cms",  array("_id" => new MongoId($value)), array('$set' => array("position" => $key)));
          }
      }
  }
}