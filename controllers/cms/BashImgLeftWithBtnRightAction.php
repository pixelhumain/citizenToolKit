<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use Cms;
use MongoId;
use PHDB;
use Rest;
use Role;
use Yii;

class BashImgLeftWithBtnRightAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
            $count = PHDB::find(Cms::COLLECTION, array(
                "path" => "tpls.blockCms.textImg.imgLeftWithBtnRight",
                "type" => array('$ne' => "blockCms"),
                "alreadyupdated" => array('$exists' => 0)
            ));
            $i = 0;
            foreach ($count as $key => $map) {
                $this->updateParams($map);
                $i++;
            }
            return Rest::sendResponse(200, "<h3>$i blockCms updated</h3>", 'text/html');
        } else {
            echo "vous n'avez pas le droit"; 
        }
    }

    private function updateParams($maps){
        $paramsData = [
            "backgroundColor",
            "buttonRejoindreBg",
            "buttonRejoindreColor",
            "buttonRejoindreRadius",
            "buttonProjetBg",
            "buttonProjetColor",
            "buttonProjetRadius"
        ];
        $id = new MongoId((string)$maps['_id']);
        unset($maps["_id"]);
        foreach ($paramsData as $index => $param) {
            if(isset($maps[$param])){
                if($index == 0){
                    $maps["css"]["themeCss"]["backgroundColor"] = $maps[$param];
                } 
                if($index == 1){
                    $maps["css"]["btnJoinCss"]["backgroundColor"] = $maps[$param];
                }
                if($index == 2){
                    $maps["css"]["btnJoinCss"]["color"] = $maps[$param];
                }
                if($index == 3){
                    $maps["css"]["btnJoinCss"]["borderBottomLeftRadius"] = $maps[$param]."px";
                    $maps["css"]["btnJoinCss"]["borderBottomRightRadius"] = $maps[$param]."px";
                    $maps["css"]["btnJoinCss"]["borderTopLeftRadius"] = $maps[$param]."px";
                    $maps["css"]["btnJoinCss"]["borderTopRightRadius"] = $maps[$param]."px";
                }
                if($index == 4){
                    $maps["css"]["btnProjetCss"]["backgroundColor"] = $maps[$param];
                }
                if($index == 5){
                    $maps["css"]["btnProjetCss"]["color"] = $maps[$param];
                }
                if($index == 6){
                    $maps["css"]["btnProjetCss"]["borderBottomLeftRadius"] = $maps[$param]."px";
                    $maps["css"]["btnProjetCss"]["borderBottomRightRadius"] = $maps[$param]."px";
                    $maps["css"]["btnProjetCss"]["borderTopLeftRadius"] = $maps[$param]."px";
                    $maps["css"]["btnProjetCss"]["borderTopRightRadius"] = $maps[$param]."px";
                }
                unset($maps[$param]);
            }
        }
        PHDB::update(Cms::COLLECTION,[ "_id" => $id ],[
            '$set' => $maps
        ]);
        $this->removeOldAttribute($id);
    }

    public function removeOldAttribute($maps)
    {
        $unset = [
            "backgroundColor"=> true,
            "buttonRejoindreBg" => true,
            "buttonRejoindreColor" => true,
            "buttonRejoindreRadius" => true,
            "buttonProjetBg" => true,
            "buttonProjetColor" => true,
            "buttonProjetRadius" => true
        ];
        PHDB::update(Cms::COLLECTION,[ "_id" => $maps ],[
            '$unset' => $unset
        ]);
    }
}