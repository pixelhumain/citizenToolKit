<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms;

use CacheHelper;
use Cms, Yii, Rest, MongoDate, Authorisation, DataValidator;
class AddStructuredCmsAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $costum = CacheHelper::getCostum();
        $_POST = DataValidator::clearUserInput($_POST);
        if(Authorisation::isInterfaceAdmin()){
        	$blockKey = $_POST["dataStructure"]["sectionParent"];
            $page = $_POST["dataStructure"]["page"];
            $persistent = $_POST["dataStructure"]["persistent"] ?? null;

            foreach ($_POST["dataStructure"]["structure"] as $colKey => $columnWidth) {        		
              $newBlocContainer=array(
                 "blockParent"=> $blockKey,
                 "collection" => Cms::COLLECTION,
                 "modified"   => new MongoDate(time()),
                 "updated"    => time(),
                 "creator"    => Yii::app()->session["userId"],
                 "created"    => time(),
                 "source"     =>array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]]),
                 "path"       =>"tpls.blockCms.superCms.container",
                 "name"       => "Colonne",
                 "page"       => $page,
                 "css"        => ["width" => $columnWidth."%","justifyContent" => "space-evenly"],
                 "parent"=>array(
                    $costum["contextId"]=>array(
                       "type"=>$costum["contextType"],
                       "name"=>$costum["contextSlug"]
                   ))
             );

             if ($persistent !== null){
                $newBlocContainer['advanced'] = [ "persistent" => $persistent ];
             }

              Yii::app()->mongodb->selectCollection(Cms::COLLECTION)->insert($newBlocContainer);
          }
          // Get all inserted column and append into section
          $structuredSection = Cms::getByIdWithChildreen($blockKey, true);
          $viewParams = array(
           "v"       => $structuredSection,
           "i"       => "",
           "el"      => [],
           "page"    => $page,
           "type"    => "tpls.blockCms.superCms.container",
           "kunik"   => $blockKey,
           "cmsList" => [],
           "canEdit" => true,
           "blockKey"=>  $blockKey,
           "costumData" => $costum = CacheHelper::getCostum()

       );
          return Rest::json(array("html" => $controller->renderPartial("costum.views.cmsBuilder.blockEngine.partials.chargeBlock", $viewParams,true),"params" => $newBlocContainer));
      }else
        return Rest::json(array("result"=>false, "msg"=> Yii::t("common", "You are not allowed to do this action")));
    }
}