<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement;

use CAction, CTKException, Rest, Mail;
class RelaunchInvitationAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
        $controller=$this->getController();
        try {
            $res = Mail::relaunchInvitation($_POST["id"]);
        } catch (CTKException $e) {
            Rest::sendResponse(450, "Webhook : ".$e->getMessage());
            die;
        }
        Rest::sendResponse(200, "Ok : webhook handdled");
    }
}