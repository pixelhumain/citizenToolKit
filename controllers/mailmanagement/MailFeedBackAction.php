<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement;

use CAction, CTKException, Rest;
use Mail;
class MailFeedBackAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
        $controller=$this->getController();
        try {
            $res = Mail::mailFeedBack($_POST);
        } catch (CTKException $e) {
            Rest::sendResponse(450, "Webhook : ".$e->getMessage());
            die;
        }
         return Rest::json(array("result"=>true, "msg"=>"Ok : webhook handdled"));
    }
}