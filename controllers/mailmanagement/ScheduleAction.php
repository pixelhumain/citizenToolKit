<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement;

use Cron;
use Yii;
use Mail;
use CTKException;
use Rest;
class ScheduleAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
        $controller=$this->getController();
        try {
            
            $params = $_POST;

            $sendOnDate = date("Y-m-d", strtotime(str_replace('"', ' ', $_POST['sendOn'])));

            if(!empty($params["tplMail"])) {
                $fromMail=(isset($params["fromMail"]) && !empty($params["fromMail"])) ? $params["fromMail"] : null;
                $res = array (
                    "type" => Cron::TYPE_MAIL,
                    "status" => Cron::STATUS_FUTURE,
                    "sendOn" => $sendOnDate,
                    "tpl"=>$params["tpl"],
                    "subject" => $params["tplObject"],
                    "from"=>Yii::app()->params['adminEmail'],
                    "to" => $params["tplMail"],
                    "tplParams" => Mail::initTplParams($params)
                );
                $res=Mail::getCustomMail($res, $fromMail);
                Mail::schedule($res);
            } else {
                throw new CTKException(Yii::t("common","Missing email!"));
            }
        } catch (CTKException $e) {
            return Rest::sendResponse(450, "Webhook : ".$e->getMessage());
            die;
        }
        return Rest::json(array("result"=>true, "msg"=>"Ok : webhook handdled"));
 //       Rest::sendResponse(200, "Ok : webhook handdled");
    }
}