<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement;

use CAction, Cron, CTKException, Rest;
class UpdateToPendingAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
        $controller=$this->getController();
        try {
            $res = Cron::processUpdateToPending();
        } catch (CTKException $e) {
            Rest::sendResponse(450, "Webhook : ".$e->getMessage());
            die;
        }
        Rest::sendResponse(200, "Ok : webhook handdled");
    }
}