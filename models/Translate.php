<?php 

class Translate {
	const FORMAT_SCHEMA = "schema";
	const FORMAT_PLP = "plp";
	const FORMAT_AS = "activityStream";
	const FORMAT_COMMUNECTER = "communecter";
	const FORMAT_RSS = "rss";
	const FORMAT_KML = "kml";
	const FORMAT_GEOJSON = "geojson";
	const FORMAT_JSONFEED = "jsonfeed";
	const FORMAT_GOGO = "gogocarto";
	const FORMAT_MD = "md";
	const FORMAT_TREE = "tree";
	const FORMAT_FINDER = "finder";
	const FORMAT_FTL = "ftl";

	public static function convert($data,$bindMap)
	{
		$newData = array();
		foreach ($data as $keyID => $valueData) {
			if ( isset($valueData) ) {
				$newData[$keyID] = self::bindData($valueData,$bindMap);
			}
		}
		return $newData;
	}

	public static function convert_geojson($data,$bindMap)
	{
		$newData = array();
		foreach ($data as $keyID => $valueData) {
			if ( isset($valueData) ) {
				$newData[] = self::bindData($valueData,$bindMap);
			}
		}
		return $newData;
	}

	public static function convert_answer($data,$bindMap){
		$newData = array();
		$result=array();
		// var_dump($newData);exit;
		$userAuthExport=false;
		$authAnswerPathAndValue=[];
		if(isset($data["form"]["preferences"]["isOpenData"]) && $data["form"]["preferences"]["isOpenData"]){
			$userAuthExport=true;
		}
		if(!empty($data["token"]) && !empty($data["user"]) && !empty($data["tokenName"])){
		    $res=Authorisation::isMeteorConnected($data["token"],$data["user"],$data["tokenName"]);
			if($res){
			
				$userId=Yii::app()->session["userId"];
				$user=PHDB::findOne(Person::COLLECTION,array("_id"=>new MongoId($userId)));
				$holderId=(isset($data["form"]["parent"]) && sizeof(array_keys($data["form"]["parent"])>0)) ? array_keys($data["form"]["parent"])[0] : "" ;
				$holderType=(isset($data["form"]["parent"][$holderId]["type"])) ? $data["form"]["parent"][$holderId]["type"] : "";
				$holder=PHDB::findOne($holderType,array("_id"=>new MongoId($holderId)));
				$rolesInHolder = isset($user["links"]["memberOf"][$holderId]["roles"]) ? $user["links"]["memberOf"][$holderId]["roles"] : [];
				$isHolderAdmin=isset($user["links"]["memberOf"][$holderId]["isAdmin"]) ? $user["links"]["memberOf"][$holderId]["isAdmin"] : false;
			    if(!isset($user["links"]["memberOf"][$holderId]["isAdminPending"]) && isset($user["links"]["memberOf"][$holderId]["isAdmin"]) && $user["links"]["memberOf"][$holderId]["isAdmin"]){
					$userAuthExport=true;
				}
			}		
		
			$authAnswerPathAndValue=(isset($data["form"]["preferences"]["dataAccess"]["mappingPathValueRoles"])) ? $data["form"]["preferences"]["dataAccess"]["mappingPathValueRoles"] : [];
            
		}
		// var_dump($authAnswerPathAndValue);exit;
		if(!empty($data["answers"]))
		    foreach ($data["answers"] as $ansId => $answers) {
				$answererAuthRoles=[];
				foreach($authAnswerPathAndValue as $path=>$mapping){
					$value=ArrayHelper::getValueByDotPath($answers["answers"],$path);
					if(isset($authAnswerPathAndValue[$path][$value])){
						$answererAuthRoles[]=$authAnswerPathAndValue[$path][$value];
					}
					
				}
				// ---- To set authorisation on specific roles for the full answer, then step, ... ----
				// $formAuthRoles=(isset($data["form"]["preferences"]["dataAccess"]["roles"])) ? $data["form"]["preferences"]["dataAccess"]["roles"] : [];
				// $userCommonRoles=array_intersect($formAuthRoles,$answererAuthRoles);

				$commonRolesAnswererUser=(isset($rolesInHolder)) ? array_intersect($rolesInHolder,$answererAuthRoles) : [];
				$userAuthExport = (sizeof($commonRolesAnswererUser)>0) ? true : $userAuthExport;
				
				
				// var_dump($commonRolesAnswererUser,$userAuthExport);exit;
		    	$newData["@type"] = "Answer";
	  	        $newData["id"] = $ansId;
	  	        $newData["formId"]=(string)$data["form"]["_id"];
	  	        $newData["formName"]=$data["form"]["name"];
				if(isset($answers["followingAnswerId"])){
			        $newData["followingAnswerId"]=$answers["followingAnswerId"];
				}
				if(isset($answers["previousAnswerId"])){
			        $newData["previousAnswerId"]=$answers["previousAnswerId"];
				}		
				if(!empty($data["form"]["preferences"]["dataAccess"]["links"]) && isset($answers["links"]) && !empty($answers["links"][Organization::COLLECTION])){
					$linksData=PHDB::findByIds(Organization::COLLECTION,array_keys($answers["links"][Organization::COLLECTION]));
				    $dataBindingClass=(!empty($data["form"]["preferences"]["dataAccess"]["links"]["dataBindingClass"])) ? $data["form"]["preferences"]["dataAccess"]["links"]["dataBindingClass"] : "TranslateCommunecter";
					// var_dump($dataBindingClass);exit;
					$newData["linked_organization"]=Translate::convert($linksData , $dataBindingClass::$dataBinding_allOrganization);
				}
	  	        if(isset($answers["answers"]) ){
		    	    foreach($answers["answers"] as $step => $fields){
						// --- step authorisation ---

						$authStepPathAndValue=(isset($data["forms"][$step]["dataAccess"]["mappingPathValueRoles"])) ? $data["forms"][$step]["dataAccess"]["mappingPathValueRoles"] : [];
						$stepAuthRolesAns=[];
						foreach($authStepPathAndValue as $path=>$mapping){
							$value=ArrayHelper::getValueByDotPath($answers["answers"],$path);
							if(!empty($value) && is_array($value)){
								foreach($value as $relVal){
									if(isset($mapping[$relVal])){
								        $stepAuthRolesAns[]=$mapping[$relVal];
							        }
								}
							}else if(isset($authStepPathAndValue[$path][$value]) && is_string($value)){
								$stepAuthRolesAns[]=$authStepPathAndValue[$path][$value];
							}
						}
						// check authorised roles to access step
						$authStepRoles=isset($data["forms"][$step]["dataAccess"]["roles"]) ? $data["forms"][$step]["dataAccess"]["roles"] : [];
						// Comparison with user roles if exist
						$authStepUserRoles=array_intersect($authStepRoles,$stepAuthRolesAns);
						// authorization if common roles or not roles condition in dataAccess.roles
						$userAuthStep = (sizeof($authStepUserRoles)>0 || empty($authStepRoles) || in_array("all",$authStepRoles)) ? true : false; 
						$indexStep= strval(array_search($step, $data["form"]["subForms"]) + 1);
		    		    $stepName=(isset($data["forms"][$step]["name"])) ? $data["forms"][$step]["name"] : ((isset($data["forms"][$step]["step"])) ? $data["forms"][$step]["step"] : $step );
		    		    $newData[$stepName]=[];
		    		    $newData[$stepName]["stepNumber"]="step".$indexStep;
			            $newData[$stepName]["stepId"]=$step; 
						
						if($userAuthStep && $userAuthExport ){
							foreach ($fields as $field => $value){
								$formK=str_ireplace(Form::$cplx, "", $field);
								$label=isset($data["forms"][$step]["inputs"][$formK]["label"]) ? $data["forms"][$step]["inputs"][$formK]["label"] : "no label";
								$type=isset($data["forms"][$step]["inputs"][$formK]["type"]) ? $data["forms"][$step]["inputs"][$formK]["type"] : "no type";
	
								if(is_string($value)){
									$val=$value;
								}
								elseif(str_contains($type,"multiRadio")){
									$val=$value["value"]??"";
								}
								else {
									$val=$value;
								}
	
								$ans=[];
								$ans["label"]=$label;
								$ans["type"]=$type;
								$ans["value"]=$val;
								$ans["fullValue"]=$value;
								$ans["fieldId"]=$field;
								$newData[$stepName][$label] = Translate::bindData($ans,$bindMap); 	
							}
						}else{
							$newData[$stepName]["message"]="unauthorized step";
						}
			        }
			    }    
		        $newAnswer[$ansId] = $newData;[$ansId];
		    }
		$result["entities"]=$newAnswer;	

		return $result;
	}

	private static function bindData ( $data, $bindMap )
	{
		$newData = array();
		foreach ( $bindMap as $key => $bindPath ) 
		{

			if ( is_array( $bindPath ) && isset( $bindPath["valueOf"] ) ) 
			{
				/*if( $key == "@id")
					$newData["debug"] = strpos( $bindPath["valueOf"], ".");*/
				if( is_array( $bindPath["valueOf"] ))
				{
					//var_dump($bindPath["valueOf"]);
					//parse recursively for objects value types , ex links.projects
					if(isset($bindPath["object"]) )
					{
						//if dots are specified , we adapt the valueData map by focusing on a subpart of it
						//var_dump($bindPath["object"]);

						$currentValue = ( strpos( $bindPath["object"], "." ) > 0 ) ? self::getValueByPath( $bindPath["object"] ,$data ) : (!empty($data[$bindPath["object"]])?$data[$bindPath["object"]] : "" );
						
						//parse each entry of the list
						//var_dump(strpos( $bindPath["object"], "." ));
						if(!empty($currentValue)){
							$newData[$key] = array();
							foreach ( $currentValue as $dataKey => $dataValue) 
							{

								$refData = $dataValue;
								//if "collection" field  is set , we'll be fetching the data source of a reference object
								//we consider the key as the dataKey if no "refId" is set
								if( isset( $bindPath["collection"] ) ){
									if ( isset( $bindPath["refId"] ) ) 
										$dataKey = $bindPath["refId"];
									$refData = PHDB::findOne( $bindPath["collection"], array( "_id" => new MongoId( $dataKey ) ) );
								}
								$valByPath = self::bindData( $refData, $bindPath["valueOf"]);
								if(!empty($valByPath))
									array_push( $newData[$key] , $valByPath );
							}
						}
					} 
					//parse recursively for array value types, ex : address
					else if( isset($bindPath["parentKey"]) && isset( $data[ $bindPath["parentKey"] ] ) ){

						if($bindPath["parentKey"]=="scope"){
							foreach($data[ $bindPath["parentKey"]] as $k => $v){
								$firstScope=$v;
								break;
							}
							$valByPath=self::bindData( $firstScope, $bindPath["valueOf"] );
						}else{
							$valByPath = self::bindData( $data[ $bindPath["parentKey"] ], $bindPath["valueOf"] );
						}
						if(!empty($valByPath))
							$newData[$key] = $valByPath;
						//resulting array has more than one level 
					}
					else{
						$valByPath = self::checkAndGetArray(self::bindData( $data, $bindPath["valueOf"]));

						if(!empty($valByPath))
							$newData[$key] = $valByPath;
					}
						
				} 
				else if( strpos( $bindPath["valueOf"], ".") > 0 ) {
					//the value is fetched deeply in the source data map
					$valByPath = self::getValueByPath( $bindPath["valueOf"] ,$data );
					if(!empty($valByPath))
						$newData[$key] = $valByPath;
				}
				else if( isset( $data[ $bindPath[ "valueOf" ] ] )  ) {
					//otherwise simply get the value of the requested element
					$valByPath = $data[ $bindPath["valueOf"] ];
					if(!empty($valByPath))
						$newData[$key] = $valByPath;
				}

			}  else if( is_array( $bindPath )){
				// there can be a first level with a simple key value
				// but can have following more than a single level 
				$valByPath = self::bindData( $data, $bindPath ) ;

				if(!empty($valByPath))
						$newData[$key] = $valByPath;
			}	
			else{
				// var_dump($key);
				// echo "</br>";
				// var_dump($bindPath);
				// echo "</br>";
				// otherwise it's just a simple label element 
				//array_push($newData,$bindPath);
				$newData[$key] = $bindPath;
			}
			//post processing once the data has been fetched
			

			if( isset($newData[$key]) && ( isset( $bindPath["type"] ) || isset( $bindPath["prefix"] ) || isset( $bindPath["suffix"] ) ) ) 
				$newData[$key] = self::formatValueByType( $newData[$key] , $bindPath );			
		}

		$t = array_keys($newData);
		$sarray = true;
		foreach ($t as $key => $v) {
			if( !is_numeric($v))
				$sarray = false;
		}
		if($sarray == true)
			$newData = array_values($newData);
		
		return $newData;
	}


	private static function getValueByPath( $path , $currentValue ){
		//The value is somewhere in an array position is definied in a json syntax
		//explode dot seperators
		$path = explode(".", $path);
		//follow path until the leaf value
		foreach ($path as $pathKey) 
		{	
			if(!empty($currentValue[ $pathKey ])){
				if( is_object($currentValue[ $pathKey ]) && get_class( $currentValue[ $pathKey ] ) == "MongoId" ){
					$currentValue = (string)$currentValue[ $pathKey ];
					break;
				} 
				else
					$currentValue = $currentValue[ $pathKey ];
			}else{
				$currentValue = "" ;
			}
			
		}
		return $currentValue;
	}

	private static function formatValueByType($val, $bindPath ){	
		//prefix and suffix can be added to anything
		$prefix = ( isset( $bindPath["prefix"] ) ) ? $bindPath["prefix"] : "";
		$suffix = ( isset( $bindPath["suffix"] ) ) ? $bindPath["suffix"] : "";
		$outsite = ( isset( $bindPath["outsite"] ) ) ? $bindPath["outsite"] : null;
		//var_dump($val);
		if( isset( $bindPath["type"] ) && $bindPath["type"] == "url" )
		{	
			$val = $prefix.$val.$suffix ;
			if(empty($outsite)){
				$server = ((isset($_SERVER['HTTPS']) AND (!empty($_SERVER['HTTPS'])) AND strtolower($_SERVER['HTTPS'])!='off') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];

				if(!empty(Yii::app()->session["custom"]) && !empty(Yii::app()->session["custom"]["url"]))
            		$val = $server.Yii::app()->createUrl(Yii::app()->session["custom"]["url"].$val);
            	// else if(!empty(Yii::app()->session["custom"]))
            	// 	$val = $server.Yii::app()->createUrl("/costum/id/".Yii::app()->session["custom"]["slug"]."/".$val);
            	else
					$val = $server.Yii::app()->createUrl($val);
			}	
			if( isset( $bindPath["toArray"] ) && $bindPath["toArray"] == true )	
				$val = [ $val ];
			//$val = $server.Yii::app()->createUrl(Yii::app()->controller->module->id.$prefix.$val.$suffix);
		}
		else if( isset( $bindPath["type"] ) && $bindPath["type"] == "urlOsm" )
		{
			$val = $prefix.$val["latitude"]."/".$val["longitude"].$suffix;
		} 
		else if( isset( $bindPath["type"] ) && $bindPath["type"] == "array" )
		{
			$val = [ $val ];
		} 
		else if ( isset($bindPath["type"]) && $bindPath["type"] == "date")
		{
			$val = date('D, d M Y H:i:s O',$val->sec);			 
		}
		else if (isset($bindPath["type"]) && ($bindPath["type"] == "title")) 
		{
			$val = TranslateRss::specFormatByType($val, $bindPath);
		}
		else if (isset($bindPath["type"]) && ($bindPath["type"] == "description")) 
		{
			$val = TranslateRss::specFormatByType($val, $bindPath);
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "coor") {
			$val = TranslateKml::getKmlCoor($val, $bindPath);
		}
		else if (isset($bindPath["type"]) && $bindPath["type"] == "description_kml") {
			$val = TranslateKml::specFormatByType($val, $bindPath);
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "Point") {
			$val = TranslateGeojson::getGeojsonCoor($val, $bindPath);	
		}		
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "properties") {
			$val = TranslateGeojson::getGeoJsonProperties($val, $bindPath);	
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "image_rss") {
			$val = TranslateRss::getRssImage($val, $bindPath);		
		}			
		else if( isset( $bindPath["prefix"] ) || isset( $bindPath["suffix"] ) )
		{
			$val = $prefix.$val.$suffix;
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "openingHours") {
			$val = TranslateFtl::openingHours($val);		
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "socialNetwork") {
			$val = TranslateFtl::socialNetwork($val);		
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "typePlace") {
			$val = TranslateFtl::typePlace($val);		
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "manageModel") {
			$val = TranslateFtl::manageModel($val);		
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "compagnon") {
			$val = TranslateFtl::compagnon($val);		
		}
		elseif (isset($bindPath["type"]) && $bindPath["type"] == "telephone") {
			$val = TranslateFtl::telephone($val);		
		}				
	
		return $val;
	}


	private static function checkAndGetArray($array){	
		$val = $array ;
		if(count($array) == 0 || (count($array) == 1 && !empty($array["@type"])))
			$val = null ;

		return $val;
	}


	public static function pastTime($date,$type, $timezone=null) {

		//echo "Date : ".$date;
		if($type == "timestamp") {
	        $date2 = $date; // depuis cette date
	        if(@$date->sec) $date2 = $date->sec;

	    } elseif($type == "date") {
	        $date2 = strtotime($date); // depuis cette date
	        //error_log($date." - ".$date2);
	    } elseif($type == "datefr") { //echo $date;exit;
	    	$date2 = DateTime::createFromFormat('d/m/Y H:i', $date)->format('Y-m-d H:i');
	    	//var_dump($date2); exit;
	        $date2 = strtotime($date2); // depuis cette date
	        //error_log($date." - ".$date2);
	    } else {
	        return "Non reconnu";
	    }

	   
		//var_dump($date2); //exit;
	    $Ecart = time()-$date2;
	    $lblEcart = "";
	    $tradAgo=true;
	    if(time() < $date2){
	    	$tradAgo=false;
	    	$lblEcart = Yii::t("common","in")." ";
			$Ecart = $date2 - time();
	    }
	    //var_dump($Ecart); echo " ".$lblEcart; //exit;

		if(isset($timezone) && $timezone != ""){
			if(date_default_timezone_get()!=$timezone){
				//error_log("SET TIMEZONE ".$timezone);
				date_default_timezone_set($timezone); //'Pacific/Noumea'
			}
		}else{
			date_default_timezone_set("UTC");
			//error_log("SET TIMEZONE UTC");
		}

	    $Annees = date('Y',$Ecart)-1970;
	    $Mois = date('m',$Ecart)-1;
	    $Jours = date('d',$Ecart)-1;
	    $Heures = date('H',$Ecart);
	    $Minutes = date('i',$Ecart);
	    $Secondes = date('s',$Ecart);

	    if($Annees > 0) {
	        $res=$lblEcart.$Annees." ".Yii::t("translate","year".($Annees>1?"s":""))." ".Yii::t("common","and")." ".$Jours." ".Yii::t("translate","day".($Jours>1?"s":"")); // on indique les jours avec les année pour être un peu plus précis
	    }
	    else if($Mois > 0) {
	        $res=$lblEcart.$Mois." ".Yii::t("translate","month".($Mois>1?"s":""));
	        if($Jours > 0)
	        	$res.=" ".Yii::t("common","and")." ".$Jours." ".Yii::t("translate","day".($Jours>1?"s":"")); // on indique les jours aussi
	    }
	    else if($Jours > 0) {
	        $res=$lblEcart.$Jours." ".Yii::t("translate","day".($Jours>1?"s":""));
	    }
	    else if($Heures > 0) {
	        if($Heures < 10) $Heures = substr($Heures, 1, 1);
	        $res=$lblEcart.$Heures." ".Yii::t("translate","hour".($Heures>1?"s":""));
	        if($Minutes > 0) {
		    	if($Minutes < 10) $Minutes = substr($Minutes, 1, 1);
		        $res.=" ".Yii::t("common","and")." ".$Minutes." ".Yii::t("translate","minute".($Minutes>1?"s":""));
		    }
	    }
	    else if($Minutes > 0) {//var_dump($Minutes); //exit;
	    	if($Minutes < 10) $Minutes = substr($Minutes, 1, 1);
	        $res=$lblEcart.$Minutes." ".Yii::t("translate","minute".($Minutes>1?"s":""));

	    }
	    else if($Secondes > 0) {
	    	if($Secondes < 10) $Secondes = substr($Secondes, 1, 1);
	        $res=$lblEcart.$Secondes." ".Yii::t("translate","second".($Secondes>1?"s":""));
	    } else {
	    	//$tradAgo=false;
	    	if($tradAgo == true)
	    		$res=Yii::t("translate","Right now");
	    	else
	    		$res=Yii::t("translate","In a few second");
	    }
	    if($tradAgo)
	    	$res=Yii::t("translate","{time} ago",array("{time}"=>$res));
	    return $res;
	}

	public static function dayDifference($date,$type, $timezone=null) {

		//echo "Date : ".$date;
		if($type == "timestamp") {
	        $date2 = $date; // depuis cette date

	    } elseif($type == "date") {
	        $date2 = strtotime($date); // depuis cette date
	        //error_log($date." - ".$date2);
	    } elseif($type == "datefr") { //echo $date;exit;
	    	$date2 = DateTime::createFromFormat('d/m/Y H:i', $date)->format('Y-m-d H:i');
	    	//var_dump($date2); exit;
	        $date2 = strtotime($date2); // depuis cette date
	        //error_log($date." - ".$date2);
	    } else {
	        return "Non reconnu";
	    }

	   

	    $Ecart = time()-$date2;
	    $lblEcart = "";
	    $tradAgo=true;
	    if(time() < $date2){
	    	$tradAgo=false;
	    	$lblEcart = Yii::t("common","in")." ";
			$Ecart = $date2 - time();
	    }

		if(isset($timezone) && $timezone != ""){
			if(date_default_timezone_get()!=$timezone){
				date_default_timezone_set($timezone);
			}
		}else{
			date_default_timezone_set("UTC");
		}

	    $Annees = date('Y',$Ecart)-1970;
	    $Mois = date('m',$Ecart)-1;
	    $Jours = date('d',$Ecart)-1;
	    //$Heures = date('H',$Ecart);
	    //$Minutes = date('i',$Ecart);
	    //$Secondes = date('s',$Ecart);
	    
	    return $Jours + ($Mois*30) + ($Annees*356); //approximatif 30jours/mois
	}

	public static function strToClickable($str){
		$url = '@(http(s)?)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
		$string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $str);
		return $string;
	}

	
}
