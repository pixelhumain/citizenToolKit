<?php

class PaymentMethod {
    const COLLECTION = "paymentMethods";

    public static $dataBinding = array(
        "type" => array("name" => "type", "rules" => array("required")),
        "config" => array("name" => "config", "rules" => array("required")),
        "parent" => array("name" => "parent", "rules" => array("required")),
    );

    public function getByParentAndType($parentId, $parentType, $type){
        $paymentMethod = PHDB::findOne(self::COLLECTION, array("parent.id" => $parentId, "parent.type" => $parentType, "type" => $type));
        return $paymentMethod;
    }

    public static function save($data){
        $paymentMethod = array();
        if($data["type"] == "stripe" && !(isset($data["config"]["secretKey"]) && isset($data["config"]["webhookKey"]) && $data["config"]["secretKey"] != "" && $data["config"]["webhookKey"] != "")){
            return ["result" => false, "msg" => "La clé secrète et la clé webhook sont obligatoires pour le type de paiement stripe"];
        }
        if($data["type"] == "helloasso" && !(isset($data["config"]["clientId"]) && isset($data["config"]["clientSecret"]) && isset($data["config"]["slug"]) && $data["config"]["clientId"] != "" && $data["config"]["clientSecret"] != "" && $data["config"]["slug"] != "")){
            return ["result" => false, "msg" => "L'identifiant du client, la clé secrète et le slug de l'organisation sont obligatoires pour le type de paiement Hello Asso"];
        }
        if($data["type"] == "virement" && !(isset($data["config"]["iban"]) && $data["config"]["iban"] != "")){
            return ["result" => false, "msg" => "L'identification bancaire est obligatoire pour le type de paiement Virement bancaire"];
        }
        foreach (self::$dataBinding as $key => $binding) {
            if (isset($data[$binding["name"]]))
                $paymentMethod[$key] = $data[$binding["name"]];
        }
		Yii::app()->mongodb
			->selectCollection(self::COLLECTION)
			->insert($paymentMethod);
        return ["result" => true, "msg" => "Le moyen de paiement a bien été enregistré", "data" => $paymentMethod];
    }

    public static function removeById($id){
        PHDB::remove(self::COLLECTION, array("_id" => new MongoId($id)));
        return ["result" => true, "msg" => "Le moyen de paiement a bien été supprimé"];
    }

    public static function getByElement($parent){
        $paymentMethods = PHDB::find(self::COLLECTION, array("parent.id" => $parent["id"], "parent.type" => $parent["type"]));
        return $paymentMethods;
    }

    public static function getByElementAndType($parent, $type){
        $paymentMethods = PHDB::findOne(self::COLLECTION, array("type" => $type,"parent.id" => $parent["id"], "parent.type" => $parent["type"]));
        return $paymentMethods;
    }
}