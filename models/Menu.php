<?php
/*DEPRECATED DOCUMENT*/
class Menu {

	private static $v = array();

	public static function constructMenuBtn($btns, $btnClass, $connectedMode = null) {
		$menuButtonHtml = "";
		// var_dump($btns);exit; 
		foreach ($btns as $k => $v) {
			if (self::showButton($v, null, $connectedMode)) {
				if ($k === 'chat') {
					if (isset(Yii::app()->params["rocketchatEnabled"]) && Yii::app()->params["rocketchatEnabled"] == true && isset(Yii::app()->params["rocketchatURL"]) && !empty(Yii::app()->params["rocketchatURL"]) && isset(Yii::app()->session["rocketUserId"])) {
						if (isset($v["construct"])) {
							if (is_string($v["construct"])) {
								$menuButtonHtml .= self::{$v["construct"]}($v, $btnClass, $k);
							}
						} else {
							$menuButtonHtml .= self::commonButtonHtml($v, $btnClass, $k);
						}
					}
				} else {
					if (isset($v["construct"])) {
						if (is_string($v["construct"])) {
							$menuButtonHtml .= self::{$v["construct"]}($v, $btnClass, $k);
						}
					} else {
						$menuButtonHtml .= self::commonButtonHtml($v, $btnClass, $k);
					}
				}
			}
		}
		return $menuButtonHtml;
	}
	public static function showButton($value, $element = null, $connectedMode = null) {
		if (!empty($element)) {
			$show = true;
			if (isset($value["restricted"])) {
				if (isset($value["restricted"]["roles"])) $show = !(Authorisation::hasRolesCostum($value["restricted"]));
				if (isset($value["restricted"]["edit"]) && empty($edit)) $show = false;
				if (isset($value["restricted"]["members"])) $show = Authorisation::isElementMember((string)$element["_id"], $element["collection"], Yii::app()->session["userId"]);
				if (isset($value["restricted"]["admins"]) && $value["restricted"]["admins"] === true) $show = Authorisation::isElementAdmin((string)$element["_id"], $element["collection"], Yii::app()->session["userId"]);
				if (isset($value["restricted"]["costumAdmins"])) $show = Authorisation::isCostumAdmin();
				if (isset($value["restricted"]["types"]) && !in_array($element["collection"], $value["restricted"]["types"])) $show = false;
				if (isset($value["restricted"]["category"]) && (!isset($element["category"]) || !in_array($element["category"], $value["restricted"]["category"]))) $show = false;
				if (!empty($value["restricted"]["disconnected"]) && !empty(Yii::app()->session["userId"])) $show = false;
				if (!empty($value["restricted"]["connected"]) && filter_var($value["restricted"]["connected"]) && empty(Yii::app()->session["userId"])) $show = false;
			}
			return $show;
		}
		if (isset($value["connected"])) {
			if (Authorisation::isInterfaceAdmin() && $connectedMode === false) {
				if ($value["connected"] === false)
					return true;
				else if ($value["connected"] === true)
					return false;
			}
			if ($value["connected"] === true && empty(Yii::app()->session["userId"]))
				return false;
			else if ($value["connected"] === false && !empty(Yii::app()->session["userId"]))
				return false;
		}
		if (isset($value["restricted"]) && !Authorisation::accessMenuButton($value))
			return false;
		return true;
	}
	public static function image($value, $btnClass, $keyClass) {
		$costum = CacheHelper::getCostum();
		$assetsPath = (isset($value["assets"])) ? Yii::app()->getModule($value["assets"])->getAssetsUrl() : "";
		$imgUrl = @$value["url"];
		if (!str_contains($imgUrl, "/upload/"))
			$imgUrl = $assetsPath . @$value["url"];
		$imgUrlMin = (isset($value["urlMin"])) ? $assetsPath . $value["urlMin"] : $imgUrl;
		$heightImg = (isset($value["height"])) ? "height='" . $value["height"] . "'" : "";
		$widthImg = (isset($value["width"])) ? "width='" . $value["width"] . "'" : "";
		$heightImgMin = (isset($value["heightMin"])) ? "height='" . $value["heightMin"] . "'" : "height='40'";
		$widthImgMin = (isset($value["widthMin"])) ? "width='" . $value["widthMin"] . "'" : "";

		$classImg = (isset($value["class"])) ? $value["class"] : "";
		$classImg .= " cosDyn-" . $keyClass;
		$href = (isset($value["href"])) ? $value["href"] : /* : (!empty($costum["type"]) && $costum["type"] =="aap" ? "javasctipt:;" :*/ "#welcome"/*)*/;
		$aClass = (isset($value["aClass"])) ? $value["aClass"] : "btn btn-link no-padding lbh menu-btn-top";
		if (!empty($costum["type"]) && $costum["type"] == "aap") {
			$aClass = "aap-lists-btn lbh-menu-app";
		}
		$target = (isset($value["target"]) ? "target ='_blank'" : "");
		$str = '<a href="' . $href . '" class="' . $aClass . '" ' . $target . '>' .
			"<img src='" . $imgUrl . "' class='image-menu hidden-xs " . $classImg . "' " . $heightImg . "  " . $widthImg . "/>" .
			"<img src='" . $imgUrlMin . "' class='image-menu visible-xs " . $classImg . "' " . $heightImgMin . "  " . $widthImgMin . "/>" .
			'</a>';

		if (!empty($costum["type"]) && $costum["type"] == "aap") {
			$aapCostum = PHDB::findOneById($costum["contextType"], $costum["contextId"], ["name", "type", "oceco.subOrganization", "oceco.webConfig.hideLabelOrganism"]);
			$costumName = $aapCostum["name"];
			$activeSubOrganization = !empty($aapCostum["oceco"]["subOrganization"]) && !empty(Yii::app()->session['userId']);
			$subOrgChooserClass = $activeSubOrganization ? 'aap-organism-chooser' : '';
			$hideOrgaName = !empty($aapCostum["oceco"]["webConfig"]["hideLabelOrganism"]);
			$chevron = $activeSubOrganization ? '<span><i class="fa fa-chevron-down"></i></span>' : '';

			if (!$hideOrgaName) {
				$str .= '<a href="javascript:;" class="pull-left menu-btn-top padding-left-10 aap-organism-chooser">' .
					'<span class="ellipsis-2 hidden-xs">' . ucfirst($costumName) . '</span>' .
					$chevron .
					'</a>';
			}
		}

		return $str;
	}
	public static function ctrlK($value, $btnClass, $keyClass) {
		$str = '<div id="ctrlK">
            <button type="button" class="search-btn">
                <span class="search-btn-container">
                    <i class="fa fa-search"></i> <span class="search-btn-placeholder"> Search </span>
                </span>
                <span class="search-btn-keys"> CTRL </span>
                <span class="search-btn-keys"> K </span>
            </button>
        </div>';

		return $str;
	}
	public static function commonButtonHtml($value, $btnClass, $keyClass, $extraHtml = "") {
		$trad = (isset($value["trad"]) && !empty($value["trad"])) ? $value["trad"] : "common";
		$labelClass = (isset($value["labelClass"])) ? " " . $value["labelClass"] : "";
		$label = (isset($value["label"]) && !empty($value["label"])) ? "<span class='" . $labelClass . "'>" . Yii::t($trad, $value["label"]) . "</span>" : "";
		$icon = (isset($value["icon"])) ? "<i class='fa fa-" . $value["icon"] . "'></i>" : "";
		$img = (isset($value["img"]) && !empty($img)) ? true : false;
		$btnClass .= (isset($value["class"])) ? " " . $value["class"] : "";
		$btnClass .= " cosDyn-" . $keyClass;
		$idButton = (isset($value["id"])) ? $value["id"] : "";
		$blank = (isset($value["blank"])) ? "target='_blank'" : "";
		$href = (isset($value["href"])) ? $value["href"] : "javascript:;";
		$spanTooltips = "";
		$badgesHtml = "";
		if (isset($value["spanTooltip"]) && !empty($value["spanTooltip"]))
			$spanTooltips = '<span class="tooltips-menu-btn">' . Yii::t($trad, $value["spanTooltip"]) . '</span>';
		if (isset($value["badge"])) {
			$badgeClass = (isset($value["badge"]["class"])) ? $value["badge"]["class"] : "";
			$badgesHtml = '<span class="' . $badgeClass . ' badge animated bounceIn cosDyn-badge"></span>';
		}
		$dataModal = (isset($value["dataTarget"])) ? ' data-toggle="modal" data-target="' . $value["dataTarget"] . '" ' : "";

		if ($img) {
			$widthImgCommon = (isset($value["width"])) ? $value["width"] . "px" : "45px";
			$heightImgCommon = (isset($value["height"])) ? $value["height"] . "px" : "45px";

			return "<a href='" . $href . "' id='" . $idButton . "' " . $blank . " class='" . $btnClass . "' " . $dataModal . "> <img class='img-responsive' width='" . $widthImgCommon . "' height='" . $heightImgCommon . "' src='" . Yii::app()->getModule("costum")->assetsUrl . $value["img"] . "'> " . $badgesHtml . $label . $spanTooltips . $extraHtml . '</a>';
		} else {
			return "<a href='" . $href . "' id='" . $idButton . "' " . $blank . " class='commonButtonHtml " . $btnClass . "' " . $dataModal . ">" . $icon . " " . $badgesHtml . $label . $spanTooltips . $extraHtml . '</a>';
		}
	}
	public static function elementButtonHtml($v, $btnClass, $extraHtml = "") {
		$dataAttr = "";
		$dataAttr .= (@$v["action"]) ? "data-action='" . $v["action"] . "' " : "";
		$dataAttr .= (@$v["view"]) ? "data-view='" . $v["view"] . "' " : "";
		$linkBtn = (@$v["href"]) ? $v["href"] : "javascript:;";
		if (isset($v["dataAttr"])) {
			$dataAttr .= (isset($v["dataAttr"]["dir"])) ? "data-dir='" . $v["dataAttr"]["dir"] . "' " : "";
			$dataAttr .= (isset($v["dataAttr"]["toggle"])) ? "data-toggle='" . $v["dataAttr"]["toggle"] . "' " : "";
			$dataAttr .= (isset($v["dataAttr"]["target"])) ? "data-target='" . $v["dataAttr"]["target"] . "' " : "";
		}
		$iconHtml = "";
		if (isset($v["img"])) {
			$iconHtml = '<span class="' . ((isset($v["imgClass"])) ? $v["imgClass"] : "") . ' tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="' . Yii::t("common", @$v["label"]) . '">' .
				'<img src="' . Yii::app()->getModule("co2")->getAssetsUrl() . '/images/' . $v["img"] . '"></mg>' .
				'</span>';
		} else if (isset($v["icon"])) {
			$iconHtml = '<span class="' . ((isset($v["iconClass"])) ? $v["iconClass"] : "") . ' tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="' . Yii::t("common", @$v["label"]) . '">' .
				'<i class="fa fa-' . $v["icon"] . '"></i>' .
				'</span>';
		}
		$idbtn = "";
		$idbtn .= (@$v["id"]) ? "id='" . $v["id"] . "' " : "";
		$str = '<a href="' . $linkBtn . '" ' . @$idbtn . ' class="elementButtonHtml ssmla btn btn-default btn-menu-tooltips ' . @$v["class"] . '" ' . @$dataAttr . ' data-toggle="tooltip" data-placement="bottom" title="">' .
			$iconHtml .
			'<span class="tooltips-top-menu-btn tooltips-visivility" style="display: none;left: auto!important;visibility: hidden;">' .
			Yii::t("common", @$v["label"]) .
			'</span>' .
			'<span class="' . @$v["labelClass"] . '">' . Yii::t("common", @$v["label"]) . '</span>' .
			'</a>';
		return $str;
	}
	public static function removeSlash($url) {
		// supprimer le slash au debut de l'url
		//ex: '/https://....'
		return ltrim($url, '/');
	}
	public static function imageElt($v, $btnClass, $keyClass, $element) {
		$url = (isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element['profilMediumImageUrl']) : $element["defaultImg"];
		$btnClass .= (isset($v["class"])) ? " " . $v["class"] : "";
		$btnClass .= " cosDyn-" . $keyClass;
		return "<img src='" . self::removeSlash($url) . "' class='imageElt " . $btnClass . "'/>";
	}
	public static function nameElt($v, $btnClass, $keyClass, $element) {
		$name = (isset($element["name"])) ? $element["name"] : "";
		$btnClass .= (isset($v["class"])) ? " " . $v["class"] : "";
		$colorElt = (isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
		$btnClass .= " cosDyn-" . $keyClass;
		$str = '<div class="nameElt identity-min">' .
			'<div class="pastille-type-element bg-' . $colorElt . ' pull-left hidden"></div>' .
			' <div class="' . $btnClass . ' pull-left no-padding no-margin">' .
			'<div class="text-left padding-left-15" id="second-name-element">' .
			'<span id="nameHeader">' .
			'<h5 class="elipsis">' . @$element["name"] . '</h5>' .
			'</span>' .
			'</div>' .
			'</div>' .
			'</div>';
		return $str;
	}
	public static function inviteInElement($v, $btnClass, $keyClass, $element) {
		$urlLink = "#element.invite.type." . $element["collection"] . ".id." . (string)$element["_id"];
		$inviteLink = "people";
		$inviteText =  Yii::t("common", "Invite people");
		if ($element["collection"] == Organization::COLLECTION) {
			$inviteLink = "members";
			$inviteText =  Yii::t("common", 'Invite members');
		} else if ($element["collection"] == Project::COLLECTION) {
			$inviteLink = "contributors";
			$inviteText =  Yii::t("common", 'Invite contributors');
		}
		$iconBtn = (isset($v["icon"]) && !empty($v["icon"])) ? '<i class="fa fa-' . $v["icon"] . '"></i>' : "";
		if (isset($v["label"])) $inviteText = $v["label"];
		$whereConnect = ($element["collection"] != Person::COLLECTION) ? 'to the ' . Element::getControlerByCollection($element["collection"]) : "";
		$classHref = (isset($v["class"]) && !empty($v["class"])) ? $v["class"] : "text-red";
		$classHref .= (isset($v["tooltip"]) && !empty($v["tooltip"])) ? " tooltips" : "";
		$str = '<a  href="' . $urlLink . '" ' .
			'class="inviteInElement lbhp ' . $classHref . '" ' .
			'data-placement="bottom" ' .
			'data-original-title="' . Yii::t("common", "Invite {what} {where}", array("{what}" => Yii::t("common", $inviteLink), "{where}" => Yii::t("common", $whereConnect))) . '">' .
			$iconBtn . '<span class="label-menu">' . $inviteText . '</span>' .
			'</a>';
		return $str;
	}
	public static function dropdown($value, $btnClass, $keyClass) {
        if (!isset($value["adminOnly"]) || !$value["adminOnly"] || (isset($value["adminOnly"]) && $value["adminOnly"] && (Authorisation::isCostumAdmin() || Authorisation::isUserSuperAdminCms()))) {
    
            $iconHtml = !empty($value["icon"]) ? "<i class='fa fa-" . $value["icon"] . "'></i>" : "";
            $classDrop = isset($value["class"]) ? $value["class"] : "";
            $classDrop .= " cosDyn-" . $keyClass;
            $buttonConstruct = isset($value["buttonConstruct"]) ? $value["buttonConstruct"] : "li";
            $nbLi = count($value["buttonList"]);
            $statusBtn = [];
            $i = 0;
            $checkStatusBtn = true;
        
            foreach ($value["buttonList"] as $keyBtn => $vBtn) {
                array_push($statusBtn, self::showButton($vBtn));
            }
        
            if (!empty($statusBtn)) {
                $checkStatusBtn = array_reduce($statusBtn, function ($key, $item) {
                    return $key && is_bool($item) && $item === false;
                }, true);
            }
        
            if (!$checkStatusBtn) {
                $str = "<div class='dropdown " . $classDrop . "'>" .
                    "<a href='javascript:;' class='dropdown-toggle pull-left cosDyn-icon " . $btnClass . "' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" . $iconHtml;
        
                if (isset($value["spanTooltip"]) && !empty($value["spanTooltip"])) {
                    $str .= '<span class="tooltips-menu-btn">' . Yii::t("common", $value["spanTooltip"]) . '</span>';
                }
        
                $str .= "</a>";
                $str .= "<div class='dropdown-menu no-padding'>" .
                    '<ul class="dropdown-menu arrow_box">';
        
                foreach ($value["buttonList"] as $k => $v) {
                    $i++;
                    if (self::showButton($v)) {
                        if (isset($v["construct"])) {
                            $str .= self::{$v["construct"]}($v, $classDrop, $v["construct"]);
                        } else {
                            $str .= self::$buttonConstruct($v, $btnClass, "buttonList");
                        }
                    } else {
                        $nbLi--;
                    }
                }
        
                $str .= "</ul>" .
                    "</div>" .
                    "</div>";
        
                return $str;
            }
        
        } else {
            return "";
        }
        
	}
	public static function xsMenu($value, $btnClass, $keyClass, $element = null) {
		$iconHtml = (!empty($value["icon"])) ? "<i class='fa fa-" . $value["icon"] . " cosDyn-icon'></i>" : "";
		$dataIcon = (!empty($value["icon"])) ? "data-icon='fa-" . $value["icon"] . "'" : "";
		$classDrop = (isset($value["class"])) ? $value["class"] : "";
		$btnClass .= (isset($value["aClass"])) ? " " . $value["aClass"] : "";
		$btnClass .= " cosDyn-" . $keyClass;

		$label = (isset($value["label"])) ? "<span class='label-menu'>" . $value["label"] . "</span>" : "";
		$dataLabel = (!empty($value["label"])) ? "data-label='fa-" . $value["label"] . "'" : "";
		$targetMenu = (isset($value["target-menu"])) ? $value["target-menu"] : "main-xs-menu";
		$buttonConstruct = (isset($value["buttonConstruct"])) ? $value["buttonConstruct"] : "commonButtonHtml";
		$nbLi = count($value["buttonList"]);
		$i = 0;
		/* $str=  "<div class='xsMenu menu-xs-cplx pull-left'>".
                "<a href='javascript:;' class='visible-xs open-xs-menu ".$btnClass."' ".$dataLabel." ".$dataIcon." data-target=".$targetMenu.">".$iconHtml." ".$label;
                if(isset($value["spanTooltip"]) && !empty($value["spanTooltip"])){
                     $str.='<span class="tooltips-menu-btn">'.Yii::t("common",$value["spanTooltip"]).'</span>';
                }
                $str.="</a>";
                $str.="<div class='menu-xs-container shadow2 ".$targetMenu."'>";
                    foreach($value["buttonList"] as $k => $v){ 
                        $i++;
                        if(self::showButton($v, $element)){
                            if(isset($v["construct"]))
                                $str.= self::{$v["construct"]}($v, $btnClass);
                            else
                                $str.=self::$buttonConstruct($v, $btnClass);
                        }                                  
                    }  
                $str.="</div>";
            $str.="</div>"; */
		$burgerBtn = "<a href='javascript:;' class='visible-xs open-xs-menu " . $btnClass . "' " . $dataLabel . " " . $dataIcon . " data-target=" . $targetMenu . ">" . $iconHtml . " " . $label;
		if (isset($value["spanTooltip"]) && !empty($value["spanTooltip"])) {
			$burgerBtn .= '<span class="tooltips-menu-btn">' . Yii::t("common", $value["spanTooltip"]) . '</span>';
		}
		$burgerBtn .= "</a>";
		$xsItem = '';
		foreach ($value["buttonList"] as $k => $v) {
			$i++;
			if (self::showButton($v, $element)) {
				if (isset($v["construct"]))
					$xsItem .= self::{$v["construct"]}($v, $btnClass, "xsMenu");
				else
					$xsItem .= self::$buttonConstruct($v, $btnClass, "-buttonList");
			}
		}

		$str =  "<div class='xsMenu menu-xs-cplx pull-left'>";
		if ($xsItem !== "") {
			$str .= $burgerBtn;
		} else {
			$str .= "<a href='javascript:;' class='visible-xs open-xs-menu-collapse " . $btnClass . "' " . $dataIcon . " data-toggle='collapse' data-target='#menuTopCenter'>" . $iconHtml . " " . $label;
			if (isset($value["spanTooltip"]) && !empty($value["spanTooltip"])) {
				$str .= '<span class="tooltips-menu-btn">' . Yii::t("common", $value["spanTooltip"]) . '</span>';
			}
			$str .= "</a>";
		}
		$str .= "<div class='menu-xs-container shadow2 " . $targetMenu . "'>";
		$str .= $xsItem;
		$str .= "</div>";
		$str .= "</div>";
		return $str;
	}
	public static function li($v, $liClass = "", $keyClass) {
		$label = (isset($v["label"])) ? $v["label"] : "";
		$href = (isset($v["href"])) ? $v["href"] : "javascript:;";
		if (strpos($href, "{userSlug}") !== false && isset(Yii::app()->session["user"]) && isset(Yii::app()->session["user"]["slug"]))
			$href = str_replace("{userSlug}", "@" . Yii::app()->session["user"]["slug"], $href);
		$blank = (isset($v["blank"])) ? "target='_blank'" : "";
		$class = (isset($v["class"])) ? $v["class"] : "";
		$class .= " cosDyn-" . $keyClass;
		$iconHtml = (isset($v["icon"])) ? '<i class="fa fa-' . $v["icon"] . '"></i>' : "";
		$str = "";
		$dataModal = (isset($v["dataTarget"])) ? ' data-toggle="modal" data-target="' . $v["dataTarget"] . '" ' : "";
		$str .= "<li class='" . $liClass . "'>" .
			'<a href="' . $href . '" class="bg-white ' . $class . '" ' . $dataModal . ' ' . $blank . '>' .
			$iconHtml . ' ' . Yii::t("common", $label) .
			'</a>' .
			'</li>';

		return $str;
	}
	private static function sousMenuView($parent, $class, $key, $btnClass, $img, $icon, $iconClass, $dropDownFullwidth = false) {
		$targetBlankItem = "";
		$targetBlankItem = "";
		$labelPage = "";
		$hrefItem = $key;
		$costum = CacheHelper::getCostum();
		$costumLangActive = Costum::setLanguageActiveInCostum($costum);
		$classBtnMenuItem = $class . ' lbh-menu-app';
		if (isset($parent["construct"])) {
			$str = self::{$parent["construct"]}($parent, $class, $key);
		} else {
			if (!empty($parent["urlExtern"])) {
				$hrefItem = $parent["urlExtern"];
				$targetBlankItem = ' target="_blanc" ';
				$classBtnMenuItem = $class . ' lbh-urlExtern';
			}
			if (isset($parent["lbhAnchor"]) && $parent["lbhAnchor"] == true)
				$classBtnMenuItem = $class . ' lbh-anchor';
			if (isset($vd["class"])) $classBtnMenuItem .= " " . $parent["class"];
	
			if (isset($parent["name"])) {
				if (gettype($parent["name"]) == "array" && !empty($parent["name"]))
					$labelPage = (isset($parent["name"][$costumLangActive])) ? $parent["name"][$costumLangActive] : $parent["name"][array_key_first($parent["name"])];
				else
					$labelPage = $parent["name"];
			}
	
			if ($dropDownFullwidth === false) {
				$str = "<a href='" . $hrefItem . "' " . $targetBlankItem . " class='" . $classBtnMenuItem . " " . $btnClass . "'>";
				if (!empty($parent["img"]) && !empty($img)) {
					$costum = CacheHelper::getCostum();
					if ($costum)
						$str .= '<img src="' . Yii::app()->getModule("costum")->assetsUrl . $parent["img"] . '" class="imgMenu hidden-xs">';
					else
						$str .= '<img src="' . Yii::app()->getModule("co2")->assetsUrl . $parent["img"] . '" class="imgMenu hidden-xs">';
				} else if (!empty($icon) && !empty($parent["icon"]))
					$str .= '<i class="fa fa-' . $parent["icon"] . ' ' . $iconClass . '"></i>';
				$str .= $labelPage;
				$str .= "</a>";
				if (isset($parent["buttonList"])) {
					$str .= "<div class='sous-menu-btn-navigation'>";
					if (!empty($parent["buttonList"])) {
						foreach ($parent["buttonList"] as $key => $value) {
							$str .= self::sousMenuView($value, $class, $key, $btnClass, $img, $icon, $iconClass);
						}
					}
					$str .= "</div>";
				}
			} else {
				$str = "";
				$classForMenuHasSubMenu = "";
				if (isset($parent["buttonList"]) && !empty($parent["buttonList"])) {
					$str .= "<div class='sous-menu-btn-navigation-content'>";
					$classForMenuHasSubMenu = " menu-has-submenu";
				}
				$str .= "<a href='" . $hrefItem . "' " . $targetBlankItem . " class='" . $classBtnMenuItem . " " . $btnClass . "" . $classForMenuHasSubMenu . "'>";
				if (!empty($parent["img"]) && !empty($img)) {
					$costum = CacheHelper::getCostum();
					if ($costum)
						$str .= '<img src="' . Yii::app()->getModule("costum")->assetsUrl . $parent["img"] . '" class="imgMenu hidden-xs">';
					else
						$str .= '<img src="' . Yii::app()->getModule("co2")->assetsUrl . $parent["img"] . '" class="imgMenu hidden-xs">';
				} else if (!empty($icon) && !empty($parent["icon"]))
					$str .= '<i class="fa fa-' . $parent["icon"] . ' ' . $iconClass . '"></i>';
				$str .= $labelPage;
				$str .= "</a>";
				if (isset($parent["buttonList"])) {
					$str .= "<div class='sous-menu-btn-navigation-full-width' style='display: none;'>";
					if (!empty($parent["buttonList"])) {
						foreach ($parent["buttonList"] as $key => $value) {
							$str .= self::sousMenuView($value, $class, $key, $btnClass, $img, $icon, $iconClass, $dropDownFullwidth);
						}
					}
					$str .= "</div>";
				}
				if (isset($parent["buttonList"]) && !empty($parent["buttonList"]))
					$str .= "</div>";
			}
		}
		return $str;
	}
	public static function app($value, $btnClass, $keyClass) {
		$class = (!empty($value["class"])) ? $value["class"] : "menu-app hidden-xs";
		$showLabel = (!empty($value["label"])) ? true : false;
		$labelClass = (isset($value["labelClass"])) ? $value["labelClass"] : "";
		$iconClass = (isset($value["iconClass"])) ? $value["iconClass"] : "";
		$tooltips = (isset($value["spanTooltip"])) ? $value["spanTooltip"] : false;
		$img = (isset($value["img"])) ? $value["img"] : true;
		$icon = (isset($value["icon"])) ? $value["icon"] : true;
		$str = "<div class='cosDyn-" . $keyClass . "' style='position: relative;'>";
		$dropDownClassFullWidth = (isset($value["dropdownFullWidth"]) && $value["dropdownFullWidth"] === true) ? " dropdown-app-full-width" : "";
		$dropDownClassFullWidthValue = (isset($value["dropdownFullWidth"])) ? $value["dropdownFullWidth"] : false;
		foreach ($value["buttonList"] as $key => $v) {
			if (Authorisation::accessMenuButton($v) && !empty($v)) {
				$costum = CacheHelper::getCostum();
				$costumLangActive = Costum::setLanguageActiveInCostum($costum);
				$labelStr = "";
				if (isset($v["name"])) {
					if (gettype($v["name"]) == "array" && !empty($v["name"]))
						$labelStr = (isset($v["name"][$costumLangActive])) ? $v["name"][$costumLangActive] : $v["name"][array_key_first($v["name"])];
					else
						$labelStr = Yii::t("common", $v["name"]);
				}
				$href = $key;
				$targetBlank = "";
				$classBtnMenu = $class . ' lbh-menu-app ';

				if (isset($v["buttonList"])) {
					$classBtnMenu = $class . ' lbh-dropdown cosDyn-buttonList';
					$str .=
						'<div class="dropdown hidden-xs ' . $btnClass . ' tools-dropdown' . $dropDownClassFullWidth . ' ' . substr($key, 1) . '">
                        <a class="dropdown-toggle ' . $classBtnMenu . ' " type="button" data-toggle="dropdown">';
					if (!empty($v["img"]) && !empty($img)) {
						if ($costum)
							$str .= '<img src="' . Yii::app()->getModule("costum")->assetsUrl . $v["img"] . '" class="imgMenu hidden-xs">';
						else
							$str .= '<img src="' . Yii::app()->getModule("co2")->assetsUrl . $v["img"] . '" class="imgMenu hidden-xs">';
					} else if (!empty($icon) && !empty($v["icon"]))
						$str .= '<i class="fa fa-' . $v["icon"] . ' ' . $iconClass . '"></i>';
					if ($showLabel && !empty($labelStr)) $str .= '<span class="label-menu ' . $labelClass . '">' . $labelStr . '</span>';
					$str .= '<span class="caret"></span>
                        </a>
                        <div class="dropdown-menu drop-down-sub-menu" data-upperhash="' . $key . '">';
					if ($dropDownClassFullWidthValue) {
						$str .= '<div class="sous-menu-full-width">';
					}
					foreach ($v["buttonList"] as $kd => $vd) {
						if (self::showButton($vd)) {
							if ($dropDownClassFullWidthValue === false) {
								$str .= '<div class="drop-down-sub-menu-content">';
								$str .= self::sousMenuView($vd, $class, $kd, $btnClass, $img, $icon, $iconClass);
								$str .= '</div>';
							} else {
								$dropDownFullwidth = $dropDownClassFullWidthValue;
								$str .= self::sousMenuView($vd, $class, $kd, $btnClass, $img, $icon, $iconClass, $dropDownFullwidth);
							}
						}
					}
					if ($dropDownClassFullWidthValue)
						$str .= '</div>';
					$str .= '</div>
                    </div>';
				} else {
					if (isset($v["construct"])) {
						$str .= self::{$v["construct"]}($v, $class, $key);
					} else {
						if (!empty($v["urlExtern"])) {
							$href = $v["urlExtern"];
							$targetBlank = ' target="_blanc" ';
							$classBtnMenu = $class . ' lbh-urlExtern';
						}
	
						if (isset($v["lbhAnchor"])) {
							if ($v["lbhAnchor"] === true) {
								$classBtnMenu = $class . ' lbh-anchor';
							}
						}
	
						/* BEGIN: rajouter le hash dans un paramètre GET s'il s'agit de lbh-menu-app  */
						if (str_contains($classBtnMenu, "lbh-menu-app")) {
							$url = parse_url($href);
							if (isset($url["fragment"])) {
								$current_url = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
								$href = parse_url($current_url, PHP_URL_SCHEME) . "://" .
									parse_url($current_url, PHP_URL_HOST) .
									parse_url($current_url, PHP_URL_PATH);
	
								parse_str($_SERVER['QUERY_STRING'], $params);
								$params["h"] = $url["fragment"];
								$href .= "?" . http_build_query($params);
	
								$href .= "#" . $url["fragment"];
							}
						}
	
						if (isset($v["class"])) $classBtnMenu .= " " . $v["class"];
						$classBtnMenu .= ' cosDyn-buttonList';
						$str .= '<a href="' . $href . '" ' . $targetBlank . ' class="' . $classBtnMenu . ' ' . $btnClass . ' menu-not-have-sub-menu">';
						// Ajouter un icone par défaut si $v["icone"] est vide et  icone = true et showLabel = false
						if ($icon == true && $showLabel == false && empty($v["icon"])) {
							$v["icon"] = "home";
						}
						if (!empty($v["img"]) && !empty($img)) {
							$str .= '<img src="' . Yii::app()->getModule("costum")->assetsUrl . $v["img"] . '" class="imgMenu hidden-xs">';
						} else if (!empty($icon) && $icon == "true" && !empty($v["icon"])) {
							$str .= '<i class="fa fa-' . $v["icon"] . ' ' . $iconClass . '"></i>';
						}
						if ($showLabel && !empty($labelStr)) $str .= '<span class="label-menu ' . $labelClass . '">' . $labelStr . '</span>';
						if ($tooltips) $str .= '<span class="tooltips-menu-btn">' . $labelStr . '</span>';
						$str .= "</a>";
					}
				}
			}
		}
		$str .= "</div>";
		return $str;
	}
	public static function createElement($value, $btnClass, $keyClass) {
		return self::commonButtonHtml($value, $btnClass, $keyClass, '<div class="toolbar-bottom-adds toolbar-bottom-fullwidth font-montserrat"></div>');
	}

	public static function profilButton($value, $btnClass, $keyClass) {
		$tooltipsAccount = Yii::t("common", "My page");
		if (isset($value["spanTooltip"]))
			$tooltipsAccount = $value["spanTooltip"];
		$nameLabel = null;
		if (isset($value["name"]) && ($value["name"] === true || $value["name"] === "true"))
			$nameLabel = (isset(Yii::app()->session["user"]["name"])) ? Yii::app()->session["user"]["name"] : Yii::app()->session["user"]["username"];
		$strLabel = null;
		if (isset($value["label"]))
			$strLabel = $value["label"];

		$profilThumbImageUrl = (isset(Yii::app()->session["user"]["profilImageUrl"]) && !empty(Yii::app()->session["user"]["profilImageUrl"])) ? Yii::app()->getRequest()->getBaseUrl(true) . Yii::app()->session["user"]["profilImageUrl"] : Yii::app()->getModule("co2")->getAssetsUrl() . '/images/thumb/default_citoyens.png';
		//var_dump($profilThumbImageUrl) ; exit;
		$dataUrl = "";
		$href = "#page.type.citoyens.id." . Yii::app()->session['userId'];
		$classNav = "lbh";
		if (isset($value["dashboard"]) && !empty($value["dashboard"])) {
			$classNav = "open-modal-dashboard";
			$href = "javascript:;";
			$viewModal = (isset($value["view"])) ? $value["view"] : "";
			$dataUrl = " data-view='" . $viewModal . "' ";
			//$str.=$this->renderPartial("co2.views.person.dashboard", array("view"=>$viewModal));
		}
		$classBtn = (isset($value["class"])) ? $value["class"] : "pull-right";
		$classBtn .= " cosDyn-" . $keyClass;
		$str = '<a  href="' . $href . '" ' .
			'class="menu-name-profil ' . $classNav . ' ' . $classBtn . ' shadow2 btn-menu-tooltips" ' . $dataUrl . ' data-toggle="dropdown">';
		if (!empty($nameLabel)) {
			$str .= '<small class="hidden-xs hidden-sm margin-left-10" id="menu-name-profil">' .
				$nameLabel .
				'</small>';
		}
		if (!empty($value["img"]))
			$str .= '<img class="img-circle" id="menu-thumb-profil" width="40" height="40" src="' . $profilThumbImageUrl . '" alt="image">';
		if (!empty($strLabel)) {
			$str .= '<span class="margin-left-5" id="menu-label-profil">' .
				$strLabel .
				'</span>';
		}
		if ($tooltipsAccount !== false)
			$str .= '<span class="tooltips-menu-btn">' . $tooltipsAccount . '</span>';
		$str .= '</a>';

		return $str;
	}
	public static function languagesSelector($value, $btnClass, $keyClass) {
		$appConfig = CacheHelper::get("appConfig");
		$costum = CacheHelper::getCostum();
		$btnClass = "cosDyn-" . $keyClass;
		$str =
			'<ul class="nav navbar-nav ' . $btnClass . '">' .
			'<li class="dropdown">' .
			'<a href="#" class="dropdown-toggle btn btn-default btn-language padding-5" style="padding-top: 7px !important" data-toggle="dropdown" role="button">';
		if (is_array($costum)) {
			$otherLanguages = (isset($costum['otherLanguages'])) ? $costum["otherLanguages"] : [];
			$costumLangActive = Costum::setLanguageActiveInCostum($costum);
			$costumLangDefault = (isset($costum['language'])) ? $costum['language'] : "fr";
			$allLanguagesInDB = array_merge([$costumLangDefault], $otherLanguages);
			$languages = [];
			foreach ($allLanguagesInDB as $lang) {
				$keyLang = strtoupper($lang);
				if (array_key_exists($keyLang, $appConfig["DeeplLanguages"])) {
					$languages[$keyLang] = $appConfig["DeeplLanguages"][$keyLang];
				}
			}
			$str .=     '<span style="font-size: 20px;">' . $appConfig["DeeplLanguages"][strtoupper($costumLangActive)]["icon"] . '</span>' .
				'</a>' .
				'<div class="dropdown-menu dropdown-languages-content dropdown-languages-nouser" role="menu">' .
				'<ul class="dropdown-languages-select-form">';
			foreach ($languages as $lang => $value) {
				$classActive = ($costumLangActive === strtolower($lang)) ? ' active' : '';
				$str .= '<li class="btn-language-flag' . $classActive . '"><a href="javascript:;" onclick="coInterface.setLanguage(\'' . strtolower($lang) . '\', ' . true . ')">' .
					'<span style="font-size: 20px; margin-right: 10px">' . $value["icon"] . '</span>' . Yii::t("cms", ucfirst($value["label"])) .
					'</a></li>';
			}
			$str .= '</ul>' .
				'</div>';
		} else {
			$defaultLanguage = Yii::app()->language;
			$languages = $appConfig["languages"];
			$str .=     '<img src="' . Yii::app()->getRequest()->getBaseUrl(true) . '/images/flags/' . $defaultLanguage . '.png" width="22"/> <span class="caret"></span>' .
				'</a>' .
				'<div class="dropdown-menu dropdown-languages-content dropdown-languages-nouser" role="menu">' .
				'<ul class="dropdown-languages-select-form">';
			foreach ($languages as $lang => $label) {
				$classActive = ($defaultLanguage === $lang) ? ' active' : '';
				$str .= '<li class="btn-language-flag' . $classActive . '">' .
					'<a href="javascript:;" onclick="coInterface.setLanguage(\'' . $lang . '\')">' .
					'<img class="language-flag" src="' . Yii::app()->getRequest()->getBaseUrl(true) . '/images/flags/' . $lang . '.png" width="25"/>' . Yii::t("common", $label) .
					'</a>' .
					'</li>';
			}
			$str .= '</ul>' .
				'</div>';
		}
		$str .= '</li>' .
			'</ul>';
		return $str;
	}
	public static function searchBar($params, $btnClass, $keyClass) {
		$eventTargetInput = (!@$params["dropdownResult"]) ? "main-search-bar" : "second-search-bar";
		$eventTargetAddon = (!@$params["dropdownResult"]) ? "main-search-bar-addon" : "second-search-bar-addon";
		if (@$params["eventTargetInput"]) $eventTargetInput;
		if (@$params["eventTargetAddon"]) $eventTargetAddon;
		$classes = (isset($params["class"])) ? $params["class"] : "hidden-xs";
		$classes .= (isset($params["addClass"])) ? " " . $params["addClass"] : "";
		$classes .= " cosDyn-" . $keyClass;
		$placeholder = (@$params["placeholder"]) ? Yii::t("common", $params["placeholder"]) : Yii::t("common", "what are you looking for ?");
		$icon = (@$params["icon"]) ? $params["icon"] : "arrow-circle-right";
		$str = '<div class="searchBarInMenu ' . $classes . '">';

		if (!empty($params["pull-right"]) && ["pull-right"] == true) {
			$str .= '<span class="text-white input-group-addon pull-right ' . $eventTargetAddon . '" id="' . $eventTargetAddon . '">' .
				'<i class="fa fa-' . $icon . '"></i>' .
				'</span>' .
				'<input type="text" class="form-control pull-right text-center ' . $eventTargetInput . '" id="' . $eventTargetInput . '" placeholder="' . $placeholder . '">';
		} else {
			$str .= '<input type="text" class="form-control pull-left text-center ' . $eventTargetInput . '" id="' . $eventTargetInput . '" placeholder="' . $placeholder . '">' .
				'<span class="text-white input-group-addon pull-left ' . $eventTargetAddon . '" id="' . $eventTargetAddon . '">' .
				'<i class="fa fa-' . $icon . '"></i>' .
				'</span>';
		}


		//if(@$params["dropdownResult"]) 
		$str .= '<div class="dropdown-result-global-search hidden-xs col-sm-6 col-md-5 col-lg-5 no-padding" style="display:none;"></div>';
		$str .= "</div>";
		return $str;
	}

	public static function coTools($params, $btnClass, $keyClass) {
		$tools =
			'<div class="tools-dropdown dropdown cosDyn-' . $keyClass . '">
            <a href="javascript:;" class="dropdown-toggle menu-button  menu-btn-top" data-toggle="dropdown"><span class="fa fa-th"></span></a>
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li> <b>COTOOLS </b></li>
                <li class="divider"></li>
                <li class="col-xs-4"><a href="https://chat.communecter.org" target="_blank"><img src="' . Yii::app()->getModule("co2")->assetsUrl . '/images/chat.jpg" alt=""><p>Chat</p></a></li>
                <li class="col-xs-4"><a href="https://wekan.communecter.org" target="_blank"><img src="' . Yii::app()->getModule("co2")->assetsUrl . '/images/wekan.jpg" alt=""><p>Kaban</p></a></li>
                <li class="col-xs-4"><a href="https://conextcloud.communecter.org/" target="_blank"><img src="' . Yii::app()->getModule("co2")->assetsUrl . '/images/cloud.jpg" alt=""><p>Drive</p></a></li>
                <li  class="col-xs-4"><a href="https://peertube.communecter.org/" target="_blank"><img src="' . Yii::app()->getModule("co2")->assetsUrl . '/images/peertube.jpg" alt=""><p>Video</p></a></li>
                <li class="col-xs-4"><a href="https://codimd.communecter.org/" target="_blank"><img src="' . Yii::app()->getModule("co2")->assetsUrl . '/images/codimd.jpg" alt=""><p>Pad</p></a></li>
                <li  class="col-xs-4"><a href="https://meet.jit.si/co" target="_blank"><img src="' . Yii::app()->getModule("co2")->assetsUrl . '/images/jitsi.jpg" alt=""><p>Visio</p></a></li>';


		$tools .=    '
                    <li class="ocecotools-title"> <b>OCECO TOOLS </b></li>
                    <li class="divider"></li>';
		foreach (Form::ocecoTools() as $key => $value) {
			$tools .=        '<li class="col-xs-4">
                            <a href="javascript:;" class="btn-ocecotools" data-label="' . $value["label"] . '" data-path="' . $value["path"] . '" data-id="' . $value["id"] . '">
                                <img src="' . $value["logo"] . '" alt="">
                                <p style="white-space: normal;">' . $value["label"] . '</p>
                            </a>
                        </li>';
		}
		$tools .= '</ul>
        </div>';

		return $tools;
	}

	public static function cartConstruct($params, $btn_class, $key) {
		$html = '' .
			'<span class="cos-cart cos-cart-button commonButtonHtml menu-button btn-menu btn-menu-tooltips menu-btn-top">' .
			'	<i class="fa fa-shopping-cart"></i>' .
			'	<span class="cart-count topbar-badge badge animated bounceIn cosDyn-badge badge-tranparent"></span>' .
			'</span>' .
			'<div class="co-popup-campcart-container" style="display: none">
            <div class="co-popup-campcart">
                <input type="checkbox" checked class="checkbox-campcart-start hide">
                <div class="co-popup-campcart-question">
                    <div class="co-popup-campcart-question-container" >
						<div class="cart-stepper-header">
							<h5 class="">Votre panier</h5>
							<button class="cart-stepper-close btn btn-default close-campcart">
								<i class="fa fa-times"></i> <span class="hidden-xs">Fermer</span>
							</button>
						</div>
						<div class="camp-cart-btn margin-20" style="bottom: 0">
							<button class="btn btn-default cos-cart-next"> Suivant <i class="fa fa-long-arrow-right"></i> </button>
						</div>
						<div class="camp-cart-btn margin-20 preced">
							<button class="btn btn-default cos-cart-prev" style="display: none"> <i class="fa fa-long-arrow-left"></i> Précédent </button>
						</div>
                        <div class="col-lg-4 hidden-md hidden-xs co-popup-campcart-menu "">
                            <div class="center aap-campcartcontain-info-costum">
                                <img class="logo-info" src="">
                                <div class="title-info"></div>
                            </div>
                            <ul class="timeline aap-campcartListStepSwipping">
                                <li class="active" data-method="camp_proc_tl" data-actualstep="tl-list">
                                    <div class="name-step">Mes tiers-lieux et/ou Organisations</div>
                                    <p> </p>
                                </li>
                                <li data-method="camp_proc_commun" data-actualstep="commun-list">
                                    <div class="name-step">Communs financés</div>
                                    <p> </p>
                                </li>
                                <li data-method="camp_proc_payment_method" data-actualstep="payment-method" class="cart-hide-before-date">
                                    <div class="name-step">Méthode de paiement</div>
                                    <p> </p>
                                </li>
                                <li data-method="camp_proc_info" data-actualstep="bill-info" class="cart-hide-before-date">
                                    <div class="name-step">Information de facturation</div>
                                    <p> </p>
                                </li>
                                <li data-actualstep="payment" class="cart-hide-before-date">
                                    <div class="name-step">Paiement</div>
                                    <p> </p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12 col-lg-8 col-xs-12 co-popup-campcart-normal" >
                            <div class="" id="">
                                <div class="">
                                    <div class="">
                                        <div class="modal-body cos-cart">
                                            <i class="fa fa-spinner fa-spin"></i> Chargement
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<script>
			$(document.documentElement).on("keyup", function(e) {
				if (typeof e.key === "string" && e.key.toLowerCase() === "escape")
					$(".close-campcart").trigger("click");
			});
			$(".co-popup-campcart-container").off("click").on("click", function(e) {
				var self;

				self = $(this);
				if ($(e.target).is(self))
					$(".close-campcart").trigger("click");
			});
		</script>
		';

		/*'<div class="modal fade cos-cart">'.
		'	<div class="modal-dialog">'.
		'		<div class="modal-content">'.
		'			<div class="modal-header">'.
		'				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.
		'				<h4 class="modal-title">Votre panier</h4>'.
		'			</div>'.
		'			<div class="modal-body cos-cart">'.
		'				<i class="fa fa-spinner fa-spin"></i> Chargement'.
		'			</div>'.
		'			<div class="modal-footer">'.
		'				<button type="button" class="btn btn-primary cos-cart-validate">Valider</button>'.
		'			</div>'.
		'		</div>'.
		'	</div>'.
		'</div>';*/
		return ($html);
	}

	public static function openModalBtn($params, $class, $key) {
		$classBtnMenu = $class;
		if (isset($params["class"])) $classBtnMenu .= " " . $params["class"];
		$classBtnMenu .= ' cosDyn-buttonList';
		isset($params["config"]["typeUrl"]) && $params["config"]["typeUrl"] == "internalLink" ? $classBtnMenu .= " lbh " : "";
		isset($params["config"]["typeUrl"]) && $params["config"]["typeUrl"] == "insideModal" ? $classBtnMenu .= " btnOpenInsideModal" : "";
		$btnLink = isset($params["config"]["button"]) ? $params["config"]["button"] : "#";
		$modalBtnLabel = !empty($params["config"]["modalBtnLabel"]) ? Yii::t("common", $params["config"]["modalBtnLabel"]) : "Fermer";
		$btnRedirectUrl = isset($params["config"]["modalBtnRedirection"]) ? ('data-btnredirectionurl="' . $params["config"]["modalBtnRedirection"] . '"') : "";
		$str = '<a href="' .( isset($params["config"]["typeUrl"]) && $params["config"]["typeUrl"] == "insideModal" ? "javascript:;" : $btnLink ). '" ' .( !empty($params["config"]["button"]) ? ' data-link="' . $params["config"]["button"] . '"' : "" ). (!empty($params["config"]["modalLabel"]) ? ' data-title="'. Yii::t("common", $params["config"]["modalLabel"]) .'"' : "") . (isset($params["config"]["getFirstStepOnly"]) ? ' data-first-step-only="' . (filter_var($params["config"]["getFirstStepOnly"], FILTER_VALIDATE_BOOLEAN) ? "true" : "false") . '"' : "") . ' data-btn-label="'. $modalBtnLabel .'"' . (isset($params["config"]["typeUrl"]) && $params["config"]["typeUrl"] == "externalLink" ? ' target="_blank"' : "") . ' '. $btnRedirectUrl .' class="' . $classBtnMenu . '">';
		if (isset($params["config"]) && isset($params["config"]["showIcon"]) && ($params["config"]["showIcon"] === "true" || $params["config"]["showIcon"] === true)) {
			if ($params["label"] != "" && isset($params["config"]) && isset($params["config"]["showLabel"]) && ($params["config"]["showLabel"] === "true" || $params["config"]["showLabel"] === true)) {
				$style = " style = 'margin-right: 5px;'";
			} else {
				$style = "";
			}
			$str .= '<i class="fa fa-' . $params["icon"] .'"'.$style.'></i>';
		}
		if ($params["label"] != "" && isset($params["config"]) && isset($params["config"]["showLabel"]) && ($params["config"]["showLabel"] === "true" || $params["config"]["showLabel"] === true)) {
			$costum = CacheHelper::getCostum();
			$costumLangActive = Costum::setLanguageActiveInCostum($costum);
			$labelStr = "";
			if (gettype($params["label"]) == "array" && !empty($params["label"]))
				$labelStr = (isset($params["label"][$costumLangActive])) ? $params["label"][$costumLangActive] : $params["label"][array_key_first($params["label"])];
			else
				$labelStr = Yii::t("common", $params["label"]);
			$str .= '<span class="label-menu">' . $labelStr . '</span>';
		}
		if ($params["spanTooltip"] && $params["spanTooltip"] != "") $str .= '<span class="tooltips-menu-btn">' . $params["spanTooltip"] . '</span>';
		$str .= "</a>";
		return $str;
	} 
}
