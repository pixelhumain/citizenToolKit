<?php

use function MongoDB\is_string_array;

class Lists {
  const COLLECTION = "lists";

  public static $dataBinding = array(
    "name" => array("name" => "name"),
    "form" => array("form" => "form"),
    "list" => array("list" => "list"),
    "type" => array("name" => "type"),
    "subtype" => array("name" => "subtype"),
    "collection" => array("name" => "collection"),
    "description" => array("name" => "description"),
    "parent" => array("name" => "parent"),
    "parentId" => array("name" => "parentId"),
    "parentType" => array("name" => "parentType"),
    "media" => array("name" => "media"),
    "urls" => array("name" => "urls"),
    "medias" => array("name" => "medias"),
    "tags" => array("name" => "tags"),
    "structags" => array("name" => "structags"),
    "shortDescription" => array("name" => "shortDescription"),
    "modified" => array("name" => "modified"),
    "source" => array("name" => "source"),
    "updated" => array("name" => "updated"),
    "creator" => array("name" => "creator"),
    "created" => array("name" => "created"),
  );
  /**
   * checks the existence of an entry in a list collection 
   * if doesn't exist create it 
   * else do nothing 
   * @return [json Map] list
   */
  public static function newEntry($listName, $entryKey, $extra = array()) {
    //check if usage exist otherwise create it 
    $slug = InflectorHelper::slugify($entryKey);
    $exist = PHDB::findOne(self::COLLECTION, array("name" => $listName, "list." . $slug => array('$exists' => 1)));

    if (!$exist) {
      $params = array_merge(array("name" => $entryKey), $extra);
      PHDB::update(
        self::COLLECTION,
        array("name" => $listName),
        array('$set' => array("list." . $slug => $params))
      );
    }
  }

  /**
   * Retrieve a lists of list by name
   * @param array $listNames List of 
   * @return array List of list
   */
  public static function get($listNames) {
    $lists = PHDB::find(self::COLLECTION, array("name" => array('$in' => $listNames)), array("name", "list"));
    $res = array();
    foreach ($lists as $key => $value) {
      $res[$value["name"]] = $value["list"];
    }
    return $res;
  }

  /**
   * Retieve a list by name and return values 
   * @param String $name of the list
   * @return array of list value
   */
  public static function getListByName($name) {
    $res = array();
    //The tags are found in the list collection, key tags
    $list = PHDB::findOne(self::COLLECTION, array("name" => $name), array('list'));

    if (!empty($list['list']))
      $res = $list['list'];
    else
      throw new CTKException("Impossible to find the list name " . $name);

    return $res;
  }


  /**
   * Get Labels linked to ids on list collection
   * @param array $listName name of the list
   * @param array $ids array of ids to retrieve
   * @return array of labels
   */
  public static function getLabels($listName, $ids) {
    $listValue = self::get($listName);
    $res = array();
    foreach ($ids as $id) {
      $label = "";
      $label = @$listValue[$id];
      array_push($res, $label);
    }
    return $res;
  }

  public static function arrange_action_by_status(array $parent, array $actions, array $statuses): array {
    $db_list = PHDB::findOne(Lists::COLLECTION, ["parent." . $parent["id"] . ".type" => $parent["type"]], ["list"]);
    $attr_list = $db_list["list"] ?? [];
    $list = [];
    $search = null;
    if (is_string_array(array_keys($actions)))
      foreach ($actions as $key => $action) {
        $action['id'] = $key;
        $actions[$key] = $action;
      }
    while (($search = array_search($search, $attr_list)) !== false)
      array_unshift($list, $search);
    $output = [];
    foreach ($statuses as $status) {
      $action_statues = array_filter($actions, fn ($action) => Action::get_action_status(($action)) === $status);
      $ids = array_map(function ($filtered) {
        return ($filtered["id"]);
      }, $action_statues);
      $status_list = array_filter($list, fn ($item) => in_array($item, $ids));
      usort($action_statues, function ($a, $b) use ($status_list) {
        $pos_a = array_search($a["id"], $status_list);
        $pos_b = array_search($b["id"], $status_list);
        return ($pos_a - $pos_b);
      });
      $output = array_merge($output, $action_statues);
    }
    return ($output);
  }
}
