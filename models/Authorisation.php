<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\components\cocolight\utils\JWTMiddleware;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\Key;
use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\JWT;

class Authorisation {
	//**************************************************************
    // Super Admin Authorisation
    //**************************************************************
    public static function isUserSuperAdmin($userId) {
        $res = false;
        if (!empty($userId)) {
            $account = Element::getElementById($userId, Person::COLLECTION, null, array("roles"));
			if (empty($account["roles"]))
				return (false);
            $res = Role::isUserSuperAdmin($account["roles"]);
        }
        return $res;
    }

    //**************************************************************
    // Super Admin Authorisation
    //**************************************************************
    public static function isUser($userId,$roles) {
        $res = false;
        if (! empty($userId)) {
            $account = Element::getElementById($userId, Person::COLLECTION, null, array("roles"));
            $res = Role::isUser(@$account["roles"],$roles);
        }
        return $res;
    }

 	/**
 	 * Description : Check if user is connect
     * - to the web interface : communecter.org
     * - or to the mobile interface : meteor.communecter.org
     * @param String $token
     * @param String $accountId
 	 * @return Boolean
 	 */
    public static function isMeteorConnected( $token, $accountId, $tokenName, $test=null ) {

        $result = false;
        if($test)
            echo $token;

        $hashedToken = hash_hmac('sha256', $token, $accountId, true);
        $hashedToken = base64_encode($hashedToken);

        if( $account = PHDB::findOne( Person::COLLECTION , array("_id" => new MongoId($accountId), "loginTokens.hashedToken" => $hashedToken, 'loginTokens.name' => $tokenName ) ) )
        {
            if($test)
                var_dump($account);
            Person::saveUserSessionData($account);
            if($test)
                echo "<br/>".Yii::app()->session['userId'];
            $result = true;
        }
        return $result;
    }

    public static function isJwtconnected($jwt_token){
        $result = false;
        if(!empty($jwt_token) && !empty(Yii::app()->params['jwt']) && !empty(Yii::app()->params['jwt']['secretKey']) && !empty(Yii::app()->params['jwt']['alg'])){
                try {
                    $decoded = JWT::decode($jwt_token,new Key(Yii::app()->params['jwt']['secretKey'], Yii::app()->params['jwt']['alg']));
                    if( $account = PHDB::findOne(Person::COLLECTION, ["_id" => new MongoId($decoded->id)]) )
                    {
                        Person::saveUserSessionData($account);
                        $result = true;
                    }
                    return $result;
                } catch (Exception $e) {
                    // Person::clearUserSessionData();
                    return $result;
                }
        }
        return $result;
    }
        

    public static function isValidUser( $user, $pwd ) {

        $result = false;
        Person::clearUserSessionData();
        $account = PHDB::findOne(Person::COLLECTION, array( '$or' => array(
                                                        array("email" => new MongoRegex('/^'.preg_quote(trim($user)).'$/i')),
                                                        array("username" => $user) ) ));
        if( @$account )
        {
            if (Person::checkPassword($pwd, $account)) {
                Person::saveUserSessionData($account);
                Person::updateLoginHistory((String) $account["_id"]);
                $result = true;
            }
        }
        return $result;
    }


    /**
     * Return true if the organization can modify his members datas
     * Depends if the params isParentOrganizationAdmin is set to true and if the organization
     * got a flag canEditMember set to true
     * @param String $organizationId An id of an organization
     * @return boolean True if the organization can edit his members data. False, else.
     */
    public static function canEditMembersData($organizationId) {
        $res = false;
        if (Yii::app()->params['isParentOrganizationAdmin']) {
            $organization = Organization::getById($organizationId);
            if (isset($organization["canEditMember"]) && $organization["canEditMember"])
                $res = true;
        }
        return $res;
    }

    /**
    * Get the authorization for edit an item
    * @param type is the type of item, (organization or event or person or project)
    * @param itemId id of the item we want to edits
    * @return a boolean
    */
    public static function canEditItem($userId, $type, $itemId,$parentType=null,$parentId=null,$deleteProcess=false){
        $res=false;
        $check = false;


        if(!empty($userId)){
        	//DDA
        	if( $type == Room::COLLECTION || $type == Room::CONTROLLER ||

	            $type == Action::COLLECTION || $type == Action::CONTROLLER ||
	            $type == Proposal::COLLECTION || $type == Proposal::CONTROLLER ) {

	            if( $parentType == null || $parentId == null ){
	                $elem = Element::getByTypeAndId($type, $itemId);

	                if( empty($elem["preferences"]) &&
	                    empty($elem["preferences"]["private"]) ) {
	                    $parentId = $elem["parentId"];
	                    $parentType = $elem["parentType"];
	                }
	            }
	            $isDDA = true;
	            $type = ( !empty($parentType) ? $parentType : $type );
	            $itemId = ( !empty($parentId) ? $parentId : $itemId );
	            $check = true;
	        }
	        //Super Admin can do anything
	        if(self::isInterfaceAdmin($itemId,$type,$userId)  )
	            return true;

	        if ( in_array($type, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION]) ) {
	            //Check if delete pending => can not edit

	            $isStatusDeletePending = Element::isElementStatusDeletePending($type, $itemId);
	            if ($isStatusDeletePending && $deleteProcess === false ) {
	                return false;
	            }
	            //Element admin ?
	            else if (self::isElementAdmin($itemId, $type, $userId)) {
	                return true;
	            //Source admin ?
	            } else if (self::isSourceAdmin($itemId, $type, $userId)) {
	                return true;
	            } else if(@$isDDA == true) {
	                return self::canParticipate($userId, $type, $itemId);
	            }
	        } else if($type == Person::COLLECTION) {
	            if($userId==$itemId)
	                $res = true;
	        } else if($type == City::COLLECTION ) {
	            if($check)
	                $res = self::isLocalCitizen( $userId, ($parentType == City::CONTROLLER) ? $parentId : $itemId );
	            else
	                $res = true;
	        } else if(in_array($type, [Cms::COLLECTION,Poi::COLLECTION, Classified::COLLECTION, Network::COLLECTION,Crowdfunding::COLLECTION])){
                 $res = self::canEdit($userId, $itemId, $type);
            }
	        else if($type == Proposal::COLLECTION)
	           $res = self::canParticipate($userId, $type, $itemId);
	        else if($type == Action::COLLECTION)
	           $res = self::canParticipate($userId, $type, $itemId);
	        else if($type == Answer::COLLECTION){
                $res=Answer::canEdit($itemId);
            }else if($type == Form::COLLECTION){
                $res=Form::canAdmin($itemId);
            }else if($type == Badge::COLLECTION){
                $res=Badge::canEdit($itemId,$userId);
            }
        }
        return $res;
    }

    /**
    * check for any element if a user is either member, contributor, attendee
    * @param type is the type of item, (organization or event or person or project)
    * @param itemId id of the item we want to edits
    * @return a boolean
    */
    public static function canParticipate($userId, $itemType, $itemId, $openEdition=true){
        $res=false;

        if( $userId )
        {
            if($openEdition)
                $res = Preference::isOpenEdition(Preference::getPreferencesByTypeId($itemId, $itemType));
            //var_dump($res);
            if($res != true){
                if( $itemType == Person::COLLECTION && $itemId == $userId)
                    $res = true;
                if(in_array($itemType, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION]))
                    $res=self::isElementMember($itemId, $itemType, $userId);
                if($itemType == City::COLLECTION)
                    $res = self::isLocalCitizen($userId, $itemId);
                if($itemType == News::COLLECTION)
                    $res = true;
                if($itemType == Classified::COLLECTION)
                    $res = true;
                if($itemType == Proposal::COLLECTION){
                    $el = Element::getElementById($itemId, $itemType, null, array("preferences", "parent", "parentId", "parentType") );
                    if( !empty($el["preferences"]) &&
                                isset($el["preferences"]["private"]) &&
                                $el["preferences"]["private"] == false ){
                        $res = true;
                    }else if( !empty($el["parentType"]) && !empty($el["parentId"]) ){
                        $res = self::canParticipate(Yii::app()->session['userId'], $el["parentType"], $el["parentId"]);
                    }else if(!empty($parent)){
                        foreach($parent as $k => $v){
                            $res=self::canParticipate(Yii::app()->session['userId'], $v["type"], $k);
                        }
                    }
                }
            }
        }
        return $res;
    }
    /**
    * check for any element if a user is either member, contributor, attendee
    * @param type is the type of item, (organization or event or person or project)
    * @param itemId id of the item we want to edits
    * @return a boolean
    */
    public static function canSee($contextType=null, $contextId=null, $openEdition=true){
        $res=false;

        if( isset(Yii::app()->session["userId"]) )
        {
            $res=self::canEditItem(Yii::app()->session["userId"], $contextType, $contextId);
            if(empty($res))
                $res=self::canParticipate(Yii::app()->session["userId"], $contextType, $contextId);
            if(empty($res))
                $res = Costum::sameFunction("canSee");
        }
        return $res;
    }
    /*** Simple way to know is interface admin
    * - Working with session on user
    * --Check user super admin of communecter
    * --Check costum admin
    * --[TODO] Check network admin
    * -- Check source admin
    **/
    public static function isInterfaceAdmin($id=null, $type=null, $user = ""){
		if (empty($user))
			$user = Yii::app()->session["userId"];
        if( self::isUserSuperAdmin($user) )
            return true;
        if((empty($type) && self::isCostumAdmin() ) || (!empty($type) && $type != "citoyens" && self::isCostumAdmin()))
            return true;
        if(!empty($id) && !empty($type) && self::isSourceAdmin ( $id, $type ,$user))
            return true;
        //TODO add check if parent exist and has parent authorization
        return false;
    }
    public static function isUserSuperAdminCms(){
      $res = false;
      $userId = Yii::app()->session["userId"];
        if (!empty($userId)) {
            $account = Element::getElementById($userId, Person::COLLECTION, null, array("roles"));
            $res = Role::isUserSuperAdminCms(@$account["roles"]);
        }
        return $res;
    }

    /**
    * Check if an user is admin of the costum if costum is used
    */
    public static function isCostumAdmin(){
        $costum = CacheHelper::getCostum();
        if(isset(Yii::app()->session['costum']) && !empty(Yii::app()->session['costum']) && !is_bool($costum) && !empty(Yii::app()->session["costum"][$costum["slug"]])){
            if(isset(Yii::app()->session["costum"][$costum["slug"]]['isCostumAdmin']) && !empty(Yii::app()->session["costum"][$costum["slug"]]))
                return Yii::app()->session["costum"][$costum["slug"]]['isCostumAdmin'];
        }
        return false;
    }
    /**
     * Return true if the user is source admin of the entity(organization, event, project)
     * @param String the id of the entity
     * @param String the type of the entity
     * @param String the id of the user
     * @return bool
     */
    public static function isSourceAdmin($idEntity, $typeEntity ,$idUser){
        $res = false ;
        $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)));
        if(!empty($project["source"]["sourceKey"])){
            $user = PHDB::findOne(Person::COLLECTION,array("_id"=>new MongoId($idUser),
                                                        "sourceAdmin" => $entity["source"]["sourceKey"]));
        }
        if(!empty($user))
            $res = true ;
        return $res;
    }
     /**
     * Return true if the entity is in openEdition
     * @param String the id of the entity
     * @param String the type of the entity
     * @return bool
     */
    public static function isOpenEdition($idEntity, $typeEntity, $preferences=null){
        $res = false ;
        if(empty($preferences)){
            $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)),array('preferences'));
            $preferences = @$entity["preferences"];
        }
        if(!empty($preferences)){
           $res = Preference::isOpenEdition($preferences);
        }


        return $res;
    }


    /**
     * Return true if the entity is in openEdition
     * @param String the id of the entity
     * @param String the type of the entity
     * @return bool
     */
    public static function isOpenData($idEntity, $typeEntity, $preferences=null){
        $res = false ;
        if(empty($preferences)){
            $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)),array('preferences'));
            $preferences = @$entity["preferences"];
        }
        if(!empty($preferences)){
           $res = Preference::isOpenData($preferences);

        }


        return $res;
    }


    public static function canEditItemOrOpenEdition($idEntity, $typeEntity, $userId, $parentType=null,$parentId=null){
        $res = false ;
        if($typeEntity != "answers") {
            $res = self::isOpenEdition($idEntity, $typeEntity);
        }else{
            $res = self::isOpenAnswer($idEntity, $typeEntity);
        }
        if($res != true)
            $res = self::canEditItem($userId, $typeEntity, $idEntity, $parentType, $parentId);

        return $res;
    }

    /**
     * Return true if the user can delete the element
     * @param type $elementType
     * @param type $elementId
     * @param type $userId
     * @return boolean
     */
    public static function canDeleteElement($elementId, $elementType, $userId, $elt = null) {
        //If open Edition : the element can be deleted
        $res = false;
        if ($elementType == Person::COLLECTION && $elementId == $userId){
            $res = true;  
        }else{
        //    $res = self::isOpenEdition($elementId, $elementType);
            if($res != true) {
                //check if the user is super admin
                $res = self::isUser($userId, array(Role::SUPERADMIN, Role::COEDITOR ));
                if ($res != true) {
                    // check if the user can edit the element (admin of the element)
                    $res = self::canEditItem($userId, $elementType, $elementId,null,null,true);

                     if ($res != true && !empty($elt) &&
                            !empty($elt["source"]) &&
                            !empty($elt["source"]["key"]) ) {
                        //$user = PHDB::findOneById(Person::COLLECTION, $userId);
                        $el = Slug::getElementBySlug($elt["source"]["key"], array() );
                        if(!empty($el) )
                            $res = self::isElementAdmin($el["id"], $el["type"] ,$userId);
                    }
                }
            }
        }

        return $res;
    }

    public static function canSeePrivateElement($links, $type, $id, $creator, $parentType=null, $parentId=null){
        if(!@Yii::app()->session["userId"])
            return false;
        else{
            // SuperAdmin Or costum Admin Or source admin
            if(self::isInterfaceAdmin($id, $type))
                return true;
            //creator access to his creativity
            if(!empty($creator) && Yii::app()->session["userId"]==$creator)
                return true;
            // attendees and contributors directly access and see the element
            if( !empty($links) &&
                @$links[Link::$linksTypes[$type][Person::COLLECTION]] &&
                @$links[Link::$linksTypes[$type][Person::COLLECTION]][Yii::app()->session["userId"]] &&
                !@$links[Link::$linksTypes[$type][Person::COLLECTION]][Yii::app()->session["userId"]]["toBeValidated"] )
                return true;
            if(!empty($parentType) && !empty($parentId))
                return self::canParticipate(Yii::app()->session["userId"], $parentType, $parentId, false);
            if(Authorisation::isElementMember($id, $type, Yii::app()->session["userId"]))
                return true;

            return false;
        }

    }
     /**
     * Return true if the user is  admin of the entity (organization, event, project)
     * @param String the id of the entity
     * @param String the type of the entity
     * @param String the id of the user
     * @return bool
     */
    public static function isElementAdmin($elementId, $elementType ,$userId, $checkParent=false, $considerSuperAdmin =true){
        $res = false ;
        //var_dump("isElementAdmin:".$elementId.":".$elementType);exit;
        if( self::isInterfaceAdmin($elementId,$elementType,$userId) && $considerSuperAdmin) {
            //var_dump("isInterfaceAdmin");exit
            return true;
        } else if(empty($res) && in_array($elementType, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION])){

            $eltAdmin = Element::getElementById($elementId, $elementType, null, array("name","links", "parent","organizer"));

            if(!empty($eltAdmin)){
                // CHECK FIRST IN ELEMENT DIRECTLY IF USER IS ADMIN
                if(isset($eltAdmin["links"])
                    && !empty(Link::$linksTypes[$elementType])
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]])
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId])
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["isAdmin"])
                    && !isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["isAdminPending"])
                    && !isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["toBeValidated"])
                    && !isset($eltAdmin["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["isInviting"])
                    ) {
                    return true;
                // CHECK THEN IF USER IS ADMIN OF PARENT OF THIS ELEMENT
                } else if(isset($eltAdmin["parent"]) && !empty($eltAdmin["parent"]) && empty($checkParent)){
                    foreach($eltAdmin["parent"] as $k => $v){
                        if($v["type"]==Person::COLLECTION){
                            if($k==Yii::app()->session["userId"])
                                $res=true;
                        }else{
                            $res=self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
                        }
                        if(!empty($res))
                            return $res;
                    }
                }else if(isset($eltAdmin["organizer"]) && !empty($eltAdmin["organizer"]) && empty($checkParent)){
                    foreach($eltAdmin["organizer"] as $k => $v){
                        if($v["type"]==Person::COLLECTION){
                            if($k==Yii::app()->session["userId"])
                                $res=true;
                        }else{
                            $res=self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
                        }
                        if(!empty($res))
                            return $res;
                    }
                }
            }
        }else if( $elementType == Form::COLLECTION ) {
            //$res = self::isFormAdmin($userId, $elementId);
            $res = true;
        }
        //var_dump(Costum::isSameFunction("isElementAdmin"));exit;
        if(empty($checkParent)){
            if(Costum::isSameFunction("isElementAdmin")) {
                $res=Costum::sameFunction("isElementAdmin", array("elementId" => $elementId, "elementType"=>$elementType, "userId"=>$userId) );
            }
        }
        return $res;
    }
    public static function isElementAdminBySlug($slug ,$userId, $checkParent=false){
        $el = Slug::getElementBySlug($slug);
        return self::isElementAdmin($el["id"], $el["type"] ,$userId, $checkParent=false);
    }
    /**
     * Return true if the user is member of the element
     * @param String the id of the user
     * @param String the elementId of the element
     * @param String the elementType of the element
     * @return boolean true or false
     */

    public static function isElementMember($elementId, $elementType, $userId) {
        $res = false;

        //Get the members of the organization : if there is no member then it's a new organization
        //We are in a creation process
        if(in_array($elementType, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION])){
            if(self::isElementAdmin($elementId, $elementType ,$userId))
                return true;
            $elt = Element::getElementById($elementId, $elementType, null, array("links", "parent"));
            if(!empty($elt)){
                // CHECK FIRST IN ELEMENT DIRECTLY IF USER IS ADMIN
                if(isset($elt["links"])
                    && isset($elt["links"][Link::$linksTypes[$elementType][Person::COLLECTION]])
                    && isset($elt["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId])
                    && !isset($elt["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["toBeValidated"])
                    && !isset($elt["links"][Link::$linksTypes[$elementType][Person::COLLECTION]][$userId]["isInviting"])
                    )
                    $res = true;
                // CHECK THEN IF USER IS ADMIN OF PARENT OF THIS ELEMENT
                else if(empty($res) && isset($elt["parent"]) && !empty($elt["parent"])){
                    foreach($elt["parent"] as $k => $v){
                        if($v["type"]==Person::COLLECTION){
                            if($k==Yii::app()->session["userId"])
                                $res=true;
                        }else{
                            $res=self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"]);
                        }
                        if(!empty($res))
                            return true;
                    }
                }
            }
        }
        if(Costum::isSameFunction("isElementMember")) {
            $res=Costum::sameFunction("isElementMember", array("elementId" => $elementId, "elementType"=>$elementType, "userId"=>$userId));
        }
        return $res;
    }

    //check si le user est creator ou canEditItem du parent
    public static function canEdit($userId, $id,$type, $extraParams = []){
        //$res = false;
        $elem = Element::getElementById($id, $type, null, array("parent", "creator","links"));
        if(!empty($elem) && !empty($userId)) {
            if(self::isInterfaceAdmin($id, $type, $userId)){
                return true;
            }else if($userId == @$elem["creator"])
                return true;
            else if(!empty($userId) && $type == "answers" && !empty($elem["links"]) && !empty($elem["links"]["canEdit"]) && !empty($elem["links"]["canEdit"][$userId]) )
                return true;
            else if (!empty($userId) && $type == "answers" && !empty($extraParams["checkContributors"]) && !empty($elem["links"]["contributors"][$userId]))
                return true;
            else if(!empty($elem["parent"])){
                foreach($elem["parent"] as $k => $v){
                    if($v["type"]==Person::COLLECTION && $k== $userId)
                        return true;
                    else if(self::canEditItem($userId, $v["type"], $k))
                        return true;
                }
            }
        }
        return false;
    }

    public static function userOwner($userId, $type, $id){
        $res = false;
        $el = Element::getElementById($id, $type, null, array("creator", 'user')) ;
        if( @$el && $userId && ($userId === @$el["creator"] || $userId === @$el["user"]) )
            return true;

        return $res;
    }
    /**
     * List the user that are admin of the organization
     * @param string $organizationId The organization Id to look for
     * @param boolean $pending : true include the pending admins. By default no.
     * @return type array of person Id
     */
    public static function listAdmins($parentId, $parentType, $pending=false) {
        $res = array();

        if (in_array($parentType, [Organization::COLLECTION, Event::COLLECTION, Project::COLLECTION])){
            $parent = Element::getElementById($parentId, $parentType, null, array("links"));
            $link=Link::$linksTypes[$parentType][Person::COLLECTION];
        }else if ($parentType == Form::COLLECTION){
            $parent = Form::getLinksById($parentId);
            $link="survey";
        }

        if ($users = @$parent["links"][$link]) {
            foreach ($users as $personId => $linkDetail) {
                if (@$linkDetail["isAdmin"] == true) {
                    $userActivated = Role::isUserActivated($personId);
                    if($userActivated){
                        if ($pending) {
                            array_push($res, $personId);
                        } else if (@$linkDetail["isAdminPending"] == null || @$linkDetail["isAdminPending"] == false) {
                            array_push($res, $personId);
                        }
                    }
                }
            }
        }

        return $res;
    }
     public static function verifCaptcha($captcha, $reponse) {
        function rpHash($value) {
                    $hash = 5381;
                    $value = strtoupper($value);
                    for($i = 0; $i < strlen($value); $i++) {
                        $hash = (leftShift32($hash, 5) + $hash) + ord(substr($value, $i));
                    }
                    return $hash;
                }

        // Perform a 32bit left shift
        function leftShift32($number, $steps) {
            // convert to binary (string)
            $binary = decbin($number);
            // left-pad with 0's if necessary
            $binary = str_pad($binary, 32, "0", STR_PAD_LEFT);
            // left shift manually
            $binary = $binary.str_repeat("0", $steps);
            // get the last 32 bits
            $binary = substr($binary, strlen($binary) - 32);
            // if it's a positive number return it
            // otherwise return the 2's complement
            return ($binary[0] == "0" ? bindec($binary) :
                -(pow(2, 31) - bindec(substr($binary, 1))));
        }


        if (rpHash($reponse) == $captcha){
            return true ;
        } else {
            return false ;
        }
    }
    // CHANGE SESSION COSTUM ROLES USER
    public static function accessMenuButton($value){
        $res=true;

        if(isset($value["restricted"])){
            $restricted=$value["restricted"];
            $costum = CacheHelper::getCostum();
            //WARNING peut être test $costum["slug"] exist en dessous
            if(Authorisation::isInterfaceAdmin())
                $res = true;
            else if(isset($restricted["admins"]) && $restricted["admins"]==true){
                $res = false;
            }else if(isset($restricted["connected"]) && $restricted["connected"] && !isset(Yii::app()->session["userId"]))
                $res = false;
            else if(isset($restricted["members"]) && $restricted["members"]==true && (empty(Yii::app()->session["userId"]) || !Authorisation::isElementMember($costum["contextId"], $costum["contextType"], Yii::app()->session["userId"])) ){
                $res = false;
            }else if(isset($restricted["roles"])){
                if(isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) && !empty(array_intersect(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"], $restricted["roles"]))){
                    $res = true;
                }else if(Authorisation::hasRolesCostum($restricted)){
                    $res = true;
                }else{
                    $res = false;
                }
            }
            if(!empty($restricted["disconnected"]) && !empty(Yii::app()->session["userId"])){
                $res = false;
            }
        }
        return $res;
    }


    public static function specificCondition($collection, $id, $tplGenerator,$path){
        $res= false;
        if(isset(Yii::app()->session["userId"]) && $tplGenerator){
            if(self::isElementAdmin($id,$collection,Yii::app()->session["userId"]) || self::isInterfaceAdmin()){
                $costum=CacheHelper::getCostum();
                $tplPath =(isset($costum) && isset($costum["contextId"])) ? "tplsUser.".$costum["contextId"].".welcome" : "allToRoot";

                $validPath=[
                    "tplsUser.".Yii::app()->session["userId"].".welcome",
                    "allToRoot",
                    "costum.colors",
                    "costum.slug",
                    "costum.loaded",
                    "costum.dashboard",
                    "costum.reference.themes",
                    $tplPath
                ];

                if(in_array($path,$validPath)==true){
                    $res = true;
                }
            }
        }

        if( !empty(Yii::app()->session["costum"]) &&
            !empty(Yii::app()->session["costum"]["form"]) &&
            !empty(Yii::app()->session["costum"]["form"]["anyOnewithLinkCanAnswer"]) ){
            $res = true;
        } elseif ($collection == Form::ANSWER_COLLECTION){
            $ell = PHDB::findOneById($collection , $id );

            $ellform = PHDB::findOneById(Form::COLLECTION , $ell["form"] );

            if(!empty($ellform["temporarymembercanreply"]) && $ellform["temporarymembercanreply"] == "true"){
                $res = true;
            }
        }
        return $res;
    }



    /**
    * check if a user is a local citizen
    * @param cityId is a unique  city Id
    * @return a boolean
    */
    public static function isLocalCitizen($userId, $cityId) {
        $cityMap = City::getUnikeyMap($cityId);
        //echo Yii::app()->session["user"]["codeInsee"] ."==". $cityMap["insee"];
        return (@Yii::app()->session["user"]["codeInsee"] && Yii::app()->session["user"]["codeInsee"] == $cityMap["insee"] ) ? true : false;
    }


     /**
     * Return true if the user is  admin of the parents entity
     * @param String the id of the entity
     * @param String the type of the entity
     * @param String the id of the user
     * @param Object the entity
     * @return bool
     */
    public static function isParentAdmin($id, $type ,$userId, $elt = null){
        $res = false ;
        if(empty($elt))
            $elt = Element::getElementById($id, $type, null, array("name", "parent"));
        if(isset($elt["parent"]) && !empty($elt["parent"]) && empty($checkParent)){
            foreach($elt["parent"] as $k => $v){
                if($v["type"]==Person::COLLECTION){
                    if($k==Yii::app()->session["userId"])
                        $res=true;
                }else{
                    $res=self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
                }
                if(!empty($res))
                    return $res;
            }
        }
        return $res;
    }

    public static function isElementChildAdmin($id, $type ,$userId, $elt = null, $formParentId = null){
            $res = false ;
            if(empty($elt))
                $elt = Element::getElementById($id, $type, null, array("name", "type", "id", "step"));
                $formParent = Element::getElementById($formParentId, Form::COLLECTION, null, array("name", "parent", "subForms"));
            if(isset($elt["type"]) && (isset($elt["id"]) || isset($elt["step"])) && ($elt["type"] == "openForm" || $elt["type"] == "inputs" ) && isset($formParent["subForms"]) && ((isset($elt["id"]) && in_array($elt["id"], $formParent["subForms"])) || (isset($elt['step']) && in_array($elt["step"], $formParent["subForms"]))) ){

                if(isset($formParent["parent"]) && !empty($formParent["parent"]) && empty($checkParent)){
                    foreach($formParent["parent"] as $k => $v){
                        if($v["type"]==Person::COLLECTION){
                            if($k==Yii::app()->session["userId"])
                                $res=true;
                        }else{
                            $res=self::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
                        }
                        if(!empty($res))
                            return $res;
                    }
                }
            }
            return $res;
    }

    /**
     * check if emetteur / createur / public badge
     * @param $badge Badge element from Mongo
     */
    public static function canAssignBadge($badgeId, $badgeElement = null)
    {
        return self::isPublicBadge($badgeId, $badgeElement) || self::badgeIsAdminEmetteur($badgeId, $badgeElement);
    }

    public static function badgeIsAdminEmetteur($badgeId, $badgeElement = null)
	{
        if(!isset($badgeElement)){
            $badgeElement = PHDB::findOneById(Badge::COLLECTION, $badgeId);
        }
        if(!isset($badgeElement['issuer'])){
            return $badgeElement['creator'] == Yii::app()->session["userId"];
        }
        if(!is_array($badgeElement['issuer'])){
            return false;
        }
		foreach($badgeElement['issuer'] as $idEmetteur => $value){
            if($value['type'] == Person::COLLECTION){
                $id = ((string) $idEmetteur);
                if(Yii::app()->session['userId'] == $id){
                    return true;
                }
            }else if(Authorisation::isElementAdmin($idEmetteur, $value['type'], Yii::app()->session['userId'], false, false)){
				return true;
			}
		}
		return false;
	}
    public static function isPublicBadge($badgeId, $badgeElement=null)
	{
        if(!isset($badgeElement)){
            $badgeElement = PHDB::findOneById(Badge::COLLECTION, $badgeId);
        }
		return isset($badgeElement) && isset($badgeElement['preferences']) && isset($badgeElement['preferences']['private']) && !$badgeElement['preferences']['private'];
 	}

    public static function hasRolesCostum($authRoles){
        $costum = CacheHelper::getCostum();
        $element = Slug::getElementBySlug($costum["slug"], ["links"]);
        $linkType = Link::$linksTypes[$costum["contextType"]][Person::COLLECTION];
        $hasIt = false;
        if(isset($element["el"]) && isset($element["el"]["links"]) && isset($element["el"]["links"][$linkType]) && isset($element["el"]["links"][$linkType][Yii::app()->session["userId"]]) && isset(Yii::app()->session["userId"])){
            $userIfMember = $element["el"]["links"][$linkType][Yii::app()->session["userId"]];

            if(isset($userIfMember["isAdmin"]) && $userIfMember["isAdmin"]){
                $hasIt = true;
            }else if(!isset($userIfMember["toBeValidated"]) && !isset($userIfMember["isInviting"])){
                $userRoles = [];
                if(isset($userIfMember) && isset($userIfMember["roles"])){
                    $userRoles = $userIfMember["roles"];
                }
                if(isset($authRoles) && isset($authRoles["roles"]) && count($userRoles)!=0){
                    $userRoles = array_map('strtolower', $userRoles);
                    foreach($authRoles["roles"] as $k => $v){
                        if(in_array(strtolower($v), $userRoles)){
                            $hasIt = true;
                        }
                    }
                }
            }
        }

        return $hasIt;
    }

    public static function hasRoles($type,$id,$authRoles){
        $element = Element::getElementSimpleById($id,$type,null, array("links"));
        $hasIt = false;
        if(isset($element) && isset($element["links"]) && isset($element["links"][Link::$linksTypes[$type][Person::COLLECTION]]) && isset($element["links"][Link::$linksTypes[$type][Person::COLLECTION]][Yii::app()->session["userId"]]) && isset(Yii::app()->session["userId"])){
            $userIfMember = $element["links"][Link::$linksTypes[$type][Person::COLLECTION]][Yii::app()->session["userId"]];

            if(isset($userIfMember["isAdmin"]) && $userIfMember["isAdmin"]){
                $hasIt = true;
            }else if(!isset($userIfMember["toBeValidated"]) && !isset($userIfMember["isInviting"])){
                $userRoles = [];
                if(isset($userIfMember) && isset($userIfMember["roles"])){
                    $userRoles = $userIfMember["roles"];
                }
                if(isset($authRoles) && isset($authRoles["roles"]) && count($userRoles)!=0){
                    $userRoles = array_map('strtolower', $userRoles);
                    foreach($authRoles["roles"] as $k => $v){
                        if(in_array(strtolower($v), $userRoles)){
                            $hasIt = true;
                        }
                    }
                }
            }
        }

        return $hasIt;
    }

    public static function isOpenAnswer($idEntity, $typeEntity){
        $res = false;
        if(!empty($idEntity) && !empty($typeEntity)){
            $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)),array('form'));
            $form = @$entity["form"];
            if(!empty($entity["user"]) && $entity["user"] == Yii::app()->session['userId'] && !empty($form["onlymemberaccess"]) && !filter_var(      $form["onlymemberaccess"], FILTER_VALIDATE_BOOLEAN)){
                $res = true;
            }
        }
        return $res;
    }
    public static function canAddDocuments($elementId, $elementType){
        $res = false;
        if(Costum::isSameFunction("canAddDocuments")) {
            $res=Costum::sameFunction("canAddDocuments", array("elementId" => $elementId, "elementType"=>$elementType) );
        }
        return $res;
    }
}

