<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;
class RevocationList extends AbstractOpenBadge{
    public $type = 'RevocationList'; 
    public $id; 
    public $issuer; 
    public $revokedAssertions;
}