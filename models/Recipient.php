<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;

class Recipient extends AbstractOpenBadge{
    public $hashed;
    public $identity;
    public $type;
    public $salt;
}