<?php

use ElephantIO\Client;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use PHDB;
class Link
{

    const person2person = "friends";
    const person2organization = "memberOf";
    const person2projects = "projects";
    const person2events = "events";
    const person2personfollows = "follows";
    const person2personfollowers = "followers";


    const event2subevent = "subEvents";
    const event2person = "attendees";

    const organization2element = "members";
    const organization2person = "members people";
    const organization2organization = "members org";
    const organization2personOrga = "organizer";

    const project2person = "contributors people";
    const project2organization = "contributors org";
    const project2element = "contributors";

    const need2Item = "needs";
    const element2forms = "answers";
    const element2projects = "projects";

    //Link options
    const TO_BE_VALIDATED = "toBeValidated";
    const IS_ADMIN = "isAdmin";
    const IS_ADMIN_PENDING = "isAdminPending";
    const INVITED_BY_ID = "invitorId";
    const INVITED_BY_NAME = "invitorName";
    const IS_INVITING = "isInviting";
    const IS_ADMIN_INVITING = "isAdminInviting";
    const ALL_COMMUNITY = "allCommunity";
    const ALL = "all";



    public static $linksTypes = array(
        Person::COLLECTION =>
        [
            Person::COLLECTION =>  "friends",
            Project::COLLECTION =>  "projects",
            Event::COLLECTION =>  "events",
            Organization::COLLECTION =>  "memberOf",
            Proposal::COLLECTION => "proposals",
            Actions::COLLECTION => "contributors",
            Form::ANSWER_COLLECTION => "contributors"
        ],
        Organization::COLLECTION =>
        array(
            Project::COLLECTION =>  "projects",
            Event::COLLECTION =>  "events",
            Person::COLLECTION => "members",
            Organization::COLLECTION => "members",
            Proposal::COLLECTION => "proposals"
        ),
    	Event::COLLECTION => 
    		array(	
                Event::COLLECTION => "subEvent",
                Person::COLLECTION => "attendees",
                Organization::COLLECTION => "attendees",
                Proposal::COLLECTION=>"proposals"
        ),
        Project::COLLECTION =>
        array(
            Organization::COLLECTION => "contributors",
            Person::COLLECTION => "contributors",
            Project::COLLECTION =>  "projects",
            Action::COLLECTION => "actions",
            Proposal::COLLECTION => "proposals"
        ),
        Action::COLLECTION => array(
            Project::COLLECTION => "projects",
            Proposal::COLLECTION => "proposals",
            Organization::COLLECTION => "contributors",
            Person::COLLECTION => "contributors",
        ),
        Actions::COLLECTION => array(
            Project::COLLECTION => "projects",
            Proposal::COLLECTION => "proposals",
            Organization::COLLECTION => "contributors",
            Person::COLLECTION => "contributors",
        ),
        Proposal::COLLECTION => array(
            Project::COLLECTION => "projects",
            Organization::COLLECTION => "contributors",
            Person::COLLECTION => "contributors",
            Proposal::COLLECTION => "proposals"
        ),
        Crowdfunding::COLLECTION => array(
            Project::COLLECTION =>  "projects",
            Event::COLLECTION =>  "events",
            Person::COLLECTION => "members",
            Organization::COLLECTION => "members"
        ),
        Form::ANSWER_COLLECTION => array(
            Person::COLLECTION => "contributors",
            Organization::COLLECTION => "contributors"
        )   

    );
    public static $connectTypes = array(
        Organization::COLLECTION => "member",
        Project::COLLECTION =>  "contributor",
        Event::COLLECTION =>  "attendee",
        Person::COLLECTION => "friend",
        Action::COLLECTION => "contributor",
        Form::ANSWER_COLLECTION => "contributor"
    );

    public static $connectLink = array(
        "parent", "organizer", "producer"
    );
    public static function getConnectLink($object)
    {
        foreach (self::$connectLink as $value) {
            if (@$object[$value]) {
                foreach ($object[$value] as $key => $type) {
                    $elt = Element::getElementById($key, $type["type"], null, array("name", "slug", "profilThumbImageUrl"));
                    $object[$value][$key] = array_merge($object[$value][$key], $elt);
                }
            }
        }
        return $object;
    }


    /** TODO BOUBOULE  ----- TO DELETE ConnectParentToChild do it
     * Add a member to an organization
     * Create a link between the 2 actors. The link will be typed members and memberOf
     * The memberOf should be an organization
     * The member can be an organization or a person
     * 2 entry will be added :
     * - $memberOf.links.members["$memberId"]
     * - $member.links.memberOf["$memberOfId"]
     * @param String $memberOfId The Id memberOf (organization) where a member will be linked. 
     * @param String $memberOfType The Type (should be organization) memberOf where a member will be linked. 
     * @param String $memberId The member Id to add. It will be the member added to the memberOf
     * @param String $memberType MemberType to add : could be an organization or a person
     * @param String $userId The userId doing the action
     * @param Boolean $userAdmin if true the member will be added as an admin 
     * @param Boolean $pendingAdmin if true the member will be added as a pending admin 
     * @return result array with the result of the operation
     */
    /* public static function addMember($memberOfId, $memberOfType, $memberId, $memberType, 
                        $userId, $userAdmin = false, $userRole = "", $pendingAdmin=false) {
        
        $organization=Organization::getById($memberOfId);
        $listAdmin=Authorisation::listOrganizationAdmins($memberOfId);
       // print_r($listAdmin);
        //TODO SBAR => Change the boolean userAdmin to a role (admin, contributor, moderator...)        
        //1. Check if the $userId can manage the $memberOf
        $toBeValidated = false;
        $notification="";
        if (!Authorisation::isOrganizationAdmin($userId, $memberOfId)) {
            // Specific case when the user is added as an admin
            if (!$userAdmin) {
                // Add a toBeValidated tag on the link
                if(@$organization["links"]["members"] && count($organization["links"]["members"])!=0 && !empty($listAdmin)){
                	$toBeValidated = true;
					$notification="toBeValidated";
                }
            }
        }

        // Create link between both entity
		$res=self::connect($memberOfId, $memberOfType, $memberId, $memberType, $userId,"members",$userAdmin,$pendingAdmin, $toBeValidated, $userRole);
        $res=self::connect($memberId, $memberType, $memberOfId, $memberOfType, $userId,"memberOf",$userAdmin,$pendingAdmin, $toBeValidated, $userRole);
        //3. Send Notifications
	    //TODO - Send email to the member
        return array("result"=>true, "msg"=>"The member has been added with success", "memberOfId"=>$memberOfId, "memberId"=>$memberId,"notification" => $notification);
    }*/
    /**
     * Add a contributor when a project is created
     * Create a link between the creator person and project. The link will be typed contributors and projects
     * The parent could be an organization then creation of link between organization and project
     * Only creator is declare as admin of the project 
     * @param String $creatorId - The creator Id to add. It will be the contributor added to the contributors as admin
     * @param String $creatorType - The creator is a person (session)
     * @param String $parentId - The id of the context. Could be the session user or an organization
     * @param String $parentType - The type of the context. Could be the session user or an organization
     * @param String $projectId - Id of project linked to creator and parent if organization type
     * @return result array with the result of the operation
     */
    public static function addContributor($creatorId, $creatorType, $parentId, $parentType, $projectId)
    {
        if ($parentType == Organization::COLLECTION) {
            $res = self::connect($parentId, $parentType, $projectId, Project::COLLECTION, $creatorId, "projects");
            $res = self::connect($projectId, Project::COLLECTION, $parentId, $parentType, $creatorId, "contributors");
        }
        $res = self::connect($creatorId, $creatorType, $projectId, Project::COLLECTION, $creatorId, "projects", true);
        $res = self::connect($projectId, Project::COLLECTION, $creatorId, $creatorType, $creatorId, "contributors", true);

        return array("result" => true);
    }
    /** TODO BOUBOULE  - PLUS UTILISER ?? A SUPPRIMER
     * Remove a member of an organization
     * Delete a link between the 2 actors.
     * The memberOf should be an organization
     * The member can be an organization or a person
     * 2 entry will be deleted :
     * - $memberOf.links.members["$memberId"]
     * - $member.links.memberOf["$memberOfId"]
     * @param type $memberOfId The Id memberOf (organization) where a member will be deleted. 
     * @param type $memberOfType The Type (should be organization) memberOf where a member will be deleted. 
     * @param type $memberId The member Id to remove. It will be the member removed from the memberOf
     * @param type $memberType MemberType to remove : could be an organization or a person
     * @param type $userId $userId The userId doing the action
     * @return result array with the result of the operation
     */
    /*public static function removeMember($memberOfId, $memberOfType, $memberId, $memberType, $userId) {
        
        //0. Check if the $memberOfId and the $memberId exists
        $memberOf = Element::checkIdAndType($memberOfId, $memberOfType);
        $member = Element::checkIdAndType($memberId, $memberType);
        
        //1.1 the $userId can manage the $memberOf (admin)
        // Or the user can remove himself from a member list of an organization
        if (!Authorisation::isOrganizationAdmin($userId, $memberOfId)) {
            if ($memberId != $userId) {
                throw new CTKException("You are not admin of the Organization : ".$memberOfId);
            }
        }

        //2. Remove the links
        PHDB::update( $memberOfType, 
                   array("_id" => $memberOf["_id"]) , 
                   array('$unset' => array( "links.members.".$memberId => "") ));
 
        PHDB::update( $memberType, 
                       array("_id" => $member["_id"]) , 
                       array('$unset' => array( "links.memberOf.".$memberOfId => "") ));

        //3. Send Notifications
        //TODO - Send email to the member

        return array("result"=>true, "msg"=>"The member has been removed with success", "memberOfid"=>$memberOfId, "memberid"=>$memberId);
    }*/

    private static function checkIdAndType($id, $type, $actionType = null)
    {
        if ($type == Organization::COLLECTION) {
            $res = Organization::getById($id);
            if (@$res["disabled"] && $actionType != "disconnect") {
                throw new CTKException("Impossible to link something on a disabled organization");
            }
        } else if ($type == Person::COLLECTION) {
        	$res = Person::getById($id);
        } else if ($type== Event::COLLECTION){
        	$res = Event::getById($id);
        } else if ($type== Project::COLLECTION){
        	$res = Project::getById($id);
        }else if ( $type == Poi::COLLECTION){
            $res = Poi::getById($id);
        } else if ($type == ActionRoom::COLLECTION_ACTIONS) {
            $res = ActionRoom::getActionById($id);
        } else if ($type == Survey::COLLECTION) {
            $res = Survey::getById($id);
        } else {
            throw new CTKException("Cannot manage this type : " . $type . Survey::COLLECTION);
        }
        if (empty($res)) throw new CTKException("The actor (" . $id . " / " . $type . ") is unknown");

        return $res;
    }

    public static function afterSave($collection, $params, $import = false, $warnings = null)
    {
        //Manage link with the creator depending of the role selected
        $isToLink = false;
        if (@$params["role"] == "admin") {
            $isToLink = true;
            $isAdmin = true;
        } else if (@$params["role"] == "member") {
            $isToLink = true;
            $isAdmin = false;
        } else if (@$params["role"] == "creator") {
            $isToLink = false;
        }
        unset($params["role"]);
        if ($isToLink) {
            //Create link in both entity person and organization 
            Link::connect((string)$params["_id"], Organization::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "members", $isAdmin);
            Link::connect(Yii::app()->session["userId"], Person::COLLECTION, (string)$params["_id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf", $isAdmin);
        }


        if ((!@$params['parent'] || count($params["parent"]) == 0) && !in_array($collection, [Organization::COLLECTION, Event::COLLECTION]))
            $params['parent'][Yii::app()->session["userId"]] = array("type" => Person::COLLECTION);

        if (@$params["parent"]) {
            foreach (@$params["parent"] as $key => $v) {
                //Link contributors to project
                if ($collection == Project::COLLECTION)
                    Link::addContributor(Yii::app()->session["userId"], Person::COLLECTION, $key, $v["type"], $params["_id"]);
                //Link parent event to this child
                else if ($collection == Event::COLLECTION)
                    Link::connect($key, Event::COLLECTION, $params["_id"], Event::COLLECTION, Yii::app()->session["userId"], "subEvents");
                //Link subProject to another project if parentType is a project
                if ($v["type"] == Project::COLLECTION){
                    Link::connect($key, Project::COLLECTION, (string)$params["_id"], Project::COLLECTION, Yii::app()->session["userId"], "projects");
                    Link::connect( (string)$params["_id"], Project::COLLECTION,$key, Project::COLLECTION, Yii::app()->session["userId"], "projects");
                }
            }
        }
        /*
        * except if organiser type is dontKnow
        *   Add the creator as the first attendee
        *   He is admin because he is admin of organizer
        */
        if (@$params['organizer'] && !empty($params["organizer"])) {
            foreach ($params['organizer'] as $key => $v) {
                $isAdmin = false;
                if ($v["type"] == Person::COLLECTION) {
                    $isAdmin = true;
                    Link::attendee((string)$params["_id"], Yii::app()->session['userId'], $isAdmin);
                }
                Link::addOrganizer($key, $v["type"], (string)$params["_id"], Yii::app()->session['userId']);
            }
        }
        return true;
    }

    /**
     * Connection between 2 class : organization, person, event, projects, places
     * Create a link between the 2 actors. The link will be typed as knows
     * 1 entry will be added:
     * - $origin.links.knows["$target"]
     * @param type $originId The Id of actor who wants to create a link with the $target
     * @param type $originType The Type (Organization or Person or Project or Places) of actor who wants to create a link with the $target
     * @param type $targetId The itemId that will be linked (Organization or Person or Project or Places)
     * @param type $targetType The Type (Organization or Person or Project or Places) that will be linked
     * @param type $userId The userId doing the action
     * @param type $connectType the name of connection in the target about user 
     * @param type $isAdmin boolean if new connection has admin role in the parent
     * @param type $pendingAdmin boolean if new connection has to be validated by an admin
     * @param type $isPending boolean if new connection add itself and need admin validation
     * @param type $role array if user added has a role in parent item
     * @return result array with the result of the operation
     */
    public static function connect($originId, $originType, $targetId, $targetType, $userId, $connectType, $isAdmin = false, $pendingAdmin = false, $isPending = false, $isInviting = false, $role = "", $settings = false)
    {

        //0. Check if the $originId and the $targetId exists
        $origin = Element::checkIdAndType($originId, $originType,"disconnect");
        $target = Element::checkIdAndType($targetId, $targetType,"disconnect");
        $links = array("links." . $connectType . "." . $targetId . ".type" => $targetType, "updated" => time(), "modified" => new MongoDate(time()));
        if ($isPending) {
            //If event, refers has been invited by and user as to confirm its attendee to the event
            /*  if($targetType==Event::COLLECTION || $originType==Event::COLLECTION){
		    	$links["links.".$connectType.".".$targetId.".".Link::INVITED_BY_ID] = $userId;
                $links["links.".$connectType.".".$targetId.".".Link::INVITED_BY_NAME] = Yii::app()->session["user"]["name"];
		    }*/ //else
            $links["links." . $connectType . "." . $targetId . "." . Link::TO_BE_VALIDATED] = $isPending;
        }
        if ($isInviting) {
            $links["links." . $connectType . "." . $targetId . "." . Link::INVITED_BY_ID] = $userId;
            $links["links." . $connectType . "." . $targetId . "." . Link::INVITED_BY_NAME] = Yii::app()->session["user"]["name"];
            if ($isInviting === "admin") {
                $links["links." . $connectType . "." . $targetId . "." . Link::IS_ADMIN_INVITING] = true;
            } else {
                $links["links." . $connectType . "." . $targetId . "." . Link::IS_INVITING] = $isInviting;
            }
        }/*else if($targetType==Event::COLLECTION || $originType==Event::COLLECTION){
		    PHDB::update($originType, 
                       array("_id" => $origin["_id"]) , 
                       array(
                        '$unset' => array("links.".$connectType.".".$targetId => ""),
                        '$set' => array( "updated"=>time(),"modified" => new MongoDate(time()) )
                        ));
	    }*/
        if ($isAdmin) {
            $links["links." . $connectType . "." . $targetId . "." . Link::IS_ADMIN] = $isAdmin;
            if ($pendingAdmin) {
                $links["links." . $connectType . "." . $targetId . "." . Link::IS_ADMIN_PENDING] = true;
            }
        }
        if ($role != "")
            $links["links." . $connectType . "." . $targetId . ".roles"] = $role;
        if ($settings)
            $links["links." . $connectType . "." . $targetId . "." . $settings["name"]] = $settings["value"];
        /* Add date */
        $links["links." . $connectType . "." . $targetId . ".date" ] = new MongoDate(time());

        //2. Create the links 
        if(isset($origin["links"][$connectType]) && $origin["links"][$connectType] === []){ 
            PHDB::update(
                    $originType,
                    array("_id" => $origin["_id"]),
                    array('$unset' => array("links.".$connectType => ""))
                );

        }
        PHDB::update(
            $originType,
            array("_id" => @$origin["_id"]),
            array('$set' => $links)
        );

        return array("result" => true, "msg" => "The link knows has been added with success", "originId" => $originId, "targetId" => $targetId);
    }

    /**
     * Disconnect 2 actors : organization or Person
     * Delete a link knows between the 2 actors.
     * 1 entry will be deleted :
     * - $origin.links.knows["$target"]
     * @param type $originId The Id of actor where a link with the $target will be deleted
     * @param type $originType The Collection (Organization or Person) of actor where a link with the $target will be deleted
     * @param type $targetId The actor that will be unlinked
     * @param type $targetType The Type (Organization or Person) that will be unlinked
     * @param type $userId The userId doing the action
     * @return result array with the result of the operation
     */
    public static function disconnect($originId, $originType, $targetId, $targetType, $userId, $connectType, $linkOption = null, $settings = null)
    {

        //0. Check if the $originId and the $targetId exists
        $origin = Element::checkIdAndType($originId, $originType, "disconnect");
        $target = Element::checkIdAndType($targetId, $targetType, "disconnect");
        if (!isset($origin["_id"]))
            return false;
        $unset = array("links." . $connectType . "." . $targetId => "");
        if ($linkOption != null && ($linkOption == self::IS_ADMIN_PENDING || $linkOption == self::IS_ADMIN_INVITING)) {
            if (!@$origin["links"][$connectType][$targetId][self::TO_BE_VALIDATED]) {
                $unset = array(
                    "links." . $connectType . "." . $targetId . "." . self::IS_ADMIN_PENDING => "",
                    "links." . $connectType . "." . $targetId . "." . self::IS_ADMIN_INVITING => "",
                    "links." . $connectType . "." . $targetId . "." . self::IS_ADMIN => ""
                );
            }
        }
        if ($settings != null && $settings["value"] == "default") {
            $unset = array("links." . $connectType . "." . $targetId . "." . $settings["name"] => "");
        }
        //2. Remove the links
        PHDB::update(
            $originType,
            array("_id" => $origin["_id"]),
            array('$unset' => $unset)
        );

        //3. Send Notifications
        //TODO - Send email to the member

        return array("result" => true, "msg" => "The link follows has been removed with success", "originId" => $originId, "targetId" => $targetId, "parentEntity" => $target);
    }

    /**
     * Check if two actors are connected with a links knows
     * @param type $originId The Id of actor to check the link with the $target
     * @param type $originType The Type (Organization or Person) of actor to check the link with the $target
     * @param type $targetId The actor to check that is linked
     * @param type $targetType The Type (Organization or Person) to check that is linked
     * @return boolean : true if the actors are connected, false else
     */
    public static function isConnected($originId, $originType, $targetId, $targetType)
    {
        $res = false;
        $where = array(
            "_id" => new MongoId($originId),
            "links.follows." . $targetId =>  array('$exists' => 1)
        );

        $originLinksKnows = PHDB::findOne($originType, $where);

        $res = isset($originLinksKnows);

        return $res;
    }

   
    /**
	 * Add a organizer to an event
	 * Create a link between the 2 actors. The link will be typed event and organizer
	 * @param type $organizerId The Id (organization) where an event will be linked. 
	 * @param type $eventId The Id (event) where an organization will be linked. 
	 * @param type $userId The user Id who try to link the organization to the event
	 * @return result array with the result of the operation
	 */
    public static function addOrganizer($organizerId, $organizerType, $eventId, $userId) {
		error_log("Try to add organizer ".$organizerId."/".$organizerType." from event ".$eventId);
        
        if (in_array($organizerType, [Organization::COLLECTION, Project::COLLECTION])) {
            $isUserAdmin = Authorisation::isElementAdmin($organizerId, $organizerType, $userId);
        } else if ($organizerType == Person::COLLECTION) {
            $isUserAdmin = ($userId == $organizerId) || Authorisation::isUserSuperAdmin($userId);
        } else if ($organizerType == Event::NO_ORGANISER || $organizerType == Event::COLLECTION) {
            $isUserAdmin = true;
        } else {
            throw new CTKException("Unknown organizer type = " . $organizerType);
        }

        if ($isUserAdmin != true)
            $isUserAdmin = Authorisation::isOpenEdition($organizerId, $organizerType);

        if ($isUserAdmin != true)
            return array("result" => false, "msg" => "You can't remove the organizer of this event !");

        if ($organizerType != Event::NO_ORGANISER) {
            PHDB::update(
                $organizerType,
                array("_id" => new MongoId($organizerId)),
                array('$set' => array("links.events." . $eventId . ".type" => Event::COLLECTION))
            );
            PHDB::update(
                Event::COLLECTION,
                array("_id" => new MongoId($eventId)),
                array('$set' => array("links.organizer." . $organizerId . ".type" => $organizerType))
            );
        }
        //TODO SBAR : add notification for new organizer
        $res = array("result" => true, "msg" => "The event organizer has been added with success");

        return $res;
    }

    public static function removeOrganizer($organizerId, $organizerType, $eventId, $userId)
    {
        error_log("Try to remove organizer " . $organizerId . "/" . $organizerType . " from event " . $eventId);

        if (in_array($organizerType, [Organization::COLLECTION, Project::COLLECTION])) {
            $isUserAdmin = Authorisation::isElementAdmin($organizerId, $organizerType, $userId);
        } else if ($organizerType == Person::COLLECTION) {
            $isUserAdmin = ($userId == $organizerId) || Authorisation::isUserSuperAdmin($userId);
        } else if ($organizerType == Event::NO_ORGANISER || $parentType == Event::COLLECTION) {
            return array("result" => true, "msg" => "Nothing to remove for an unknown organizer");
        } else {
            throw new CTKException("Unknown organizer type = " . $organizerType);
        }

        if ($isUserAdmin != true)
            $isUserAdmin = Authorisation::isOpenEdition($organizerId, $organizerType);

        if ($isUserAdmin != true)
            return array("result" => false, "msg" => "You can't remove the organizer of this event !");


        if ($organizerType != Event::NO_ORGANISER) {
            PHDB::update(
                Event::COLLECTION,
                array("_id" => new MongoId($eventId)),
                array('$unset' => array("links.organizer." . $organizerId => ""))
            );
            //TODO SBAR : add notification for old organizer
            $res = array("result" => true, "msg" => "The organizer has been removed with success");
        }

        return $res;
    }


    public static function addParent($parentId, $parentType, $childId, $childType, $userId)
    {
        //error_log("Try to add parent ".$parentId."/".$parentType." from ".$childType." / ".$childId);
        //var_dump($parentType); var_dump($childType); exit;
        
        if (in_array($parentType, [Organization::COLLECTION, Project::COLLECTION])) {
            if(Authorisation::isElementAdmin($childId, $childType, $userId)){
                $isUserAdmin = Authorisation::isElementAdmin($childId, $childType, $userId);
            }else{
                $isUserAdmin = Authorisation::isElementAdmin($parentId, $parentType, $userId);
            }
            //$isUserAdmin = Authorisation::isElementAdmin($parentId, $parentType, $userId);
        } else if ($parentType == Person::COLLECTION) {
            $isUserAdmin = ($userId == $parentId) || Authorisation::isUserSuperAdmin($userId);
        } else if ($parentType == Event::NO_ORGANISER || $parentType == Event::COLLECTION) {
            $isUserAdmin = true;
        } else {
            throw new CTKException("Unknown parent type = " . $parentType);
        }

        if ($isUserAdmin != true)
            $isUserAdmin = Authorisation::isOpenEdition($parentId, $parentType);

        if ($isUserAdmin != true)
            return array("result" => false, "msg" =>Yii::t("common","You can't add a parent of this")." ".Yii::t("common",$childType)."!");      
        if ($parentType != Event::NO_ORGANISER) {

            if($childType != Ressource::COLLECTION)
                PHDB::update($parentType,
        					array("_id" => new MongoId($parentId)),
        					array('$set' => array(	
                                "links.".self::$linksTypes[$parentType][$childType].".".$childId.".type"=>$childType,
                                "links.".self::$linksTypes[$parentType][$childType].".".$childId.".date"=>new MongoDate(time())
                            ))
        		);

            //$set = array(   "parentId" => $childId,
            //              "parentType" => $childType ) ;

            if($childType != Ressource::COLLECTION){
                $set["links.".self::$linksTypes[$childType][$parentType].".".$parentId.".type"] = $parentType;
                $set["links.".self::$linksTypes[$childType][$parentType].".".$parentId.".date"] = new MongoDate(time());
            }
            PHDB::update(
                $childType,
                array("_id" => new MongoId($childId)),
                array('$set' => $set)
            );
        }
        //TODO SBAR : add notification for new parent
        $res = array("result" => true, "msg" => "The element parent has been added with success");

        return $res;
    }
    public static function addBadge($parentId, $parentType, $badgeId, $badgeName, $issuedOn, $userId)
    {
        if (in_array($parentType, [Organization::COLLECTION, Project::COLLECTION])) {
            $isUserAdmin = Authorisation::isElementAdmin($parentId, $parentType, $userId);
        } else if ($parentType == Person::COLLECTION) {
            $isUserAdmin = ($userId == $parentId) || Authorisation::isUserSuperAdmin($userId);
        } else if ($parentType == Event::NO_ORGANISER || $parentType == Event::COLLECTION) {
            $isUserAdmin = true;
        } else {
            throw new CTKException("Unknown parent type = " . $parentType);
        }

        if ($isUserAdmin != true)
            $isUserAdmin = Authorisation::isOpenEdition($parentId, $parentType);

        if ($isUserAdmin != true)
            return array("result" => false, "msg" => "You can't remove the parent of this event !");

        if ($parentType != Event::NO_ORGANISER) {
                if(empty($issuedOn)){
                    $issuedOn = new MongoDate(time());
                }
                $set = array(
                    "badges.".$badgeId =>
                    [
                        "type" => "badges" , 
                        "name" => $badgeName, 
                        "issuedOn" => $issuedOn
                    ]
                );
                PHDB::update($parentType,
                    array("_id" => new MongoId($parentId)),
                    array('$set' => $set)
        		);
        }
        //TODO SBAR : add notification for new parent
        $res = array("result" => true, "msg" => "The element parent has been added with success");

        return $res;
    }

    public static function removeBadges($parentId, $parentType, $badgeId, $userId)
    {
        $childType = Badge::COLLECTION;
        //error_log("Try to remove parent ".$parentId."/".$parentType." from ".$childType." / ".$childId);
        
        if (in_array($parentType, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Event::NO_ORGANISER])) {
            $eltType = ($parentType == Event::NO_ORGANISER) ? Event::COLLECTION : $parentType;
            $isUserAdmin = Authorisation::isElementAdmin($parentId, $eltType, $userId);
        } else if ($parentType == Person::COLLECTION) {
            $isUserAdmin = ($userId == $parentId) || Authorisation::isUserSuperAdmin($userId);
        } else {
            throw new CTKException("Unknown parent type = " . $parentType);
        }

        if ($isUserAdmin != true)
            $isUserAdmin = Authorisation::isOpenEdition($parentId, $parentType);

        if ($isUserAdmin != true)
            return array("result" => false, "msg" => "You can't remove these badge");
		PHDB::update($parentType,
					array("_id"=>new MongoId($parentId)),
					array('$unset'=> array("badges." . $badgeId => ""))
	    );
        $res = array("result" => true, "msg" => "The parent has been removed with success");
        return $res;
    }
    
    public static function removeParent($parentId, $parentType, $childId, $childType, $userId)
    {
        //error_log("Try to remove parent ".$parentId."/".$parentType." from ".$childType." / ".$childId);
         if (in_array($parentType, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Event::NO_ORGANISER])) {
            $eltType = ($parentType == Event::NO_ORGANISER) ? Event::COLLECTION : $childType;
            if(Authorisation::isElementAdmin($childId, $eltType, $userId)){
                $isUserAdmin = Authorisation::isElementAdmin($childId, $eltType, $userId);
            }else{
                $eltType = ($parentType == Event::NO_ORGANISER) ? Event::COLLECTION : $parentType;
                $isUserAdmin = Authorisation::isElementAdmin($parentId, $eltType, $userId);
            }
        } else if ($parentType == Person::COLLECTION) {
            $isUserAdmin = ($userId == $parentId) || Authorisation::isUserSuperAdmin($userId);
        } else {
            throw new CTKException("Unknown parent type = " . $parentType);
        }

        if ($isUserAdmin != true)
            $isUserAdmin = Authorisation::isOpenEdition($parentId, $parentType);
        if ($isUserAdmin != true)
            return array("result" => false, "msg" =>Yii::t("common","You can't remove the parent of this")." ".Yii::t("common",$childType)."!");

        if(	
            (  
                $childType != Ressource::COLLECTION ) &&
            (   $parentType != Person::COLLECTION ||
    			(	$parentType == Person::COLLECTION && 
    				!self::isLinked($childId, $childType, $userId, null) ) ) 
		) {
			PHDB::update($parentType,
						array("_id"=>new MongoId($parentId)),
						array('$unset'=> array("links.".self::$linksTypes[$parentType][$childType].".".$childId => ""))
	        );
			PHDB::update($childType,
					array("_id"=>new MongoId($childId)),
					array('$unset'=> array("links.".self::$linksTypes[$childType][$parentType].".".$parentId => ""))
        	);
		}
        


        //TODO SBAR : add notification for old parent
        $res = array("result" => true, "msg" => Yii::t("common","The parent has been removed with success"));
        //}

        return $res;
    }

    /**
     * Link a person to an event when an event is create
     * Create a link between the 2 actors. The link will be typed event and organizer
     * @param type $eventId The Id (event) where a person will be linked. 
     * @param type $userId The user (person) Id who want to be link to the event
     * @param type $userAdmin (Boolean) to set if the member is admin or not
     * @param type $creator (Boolean) to set if attendee is creator of the event
     * @return result array with the result of the operation
     * When organization or project create an event, this parent is admin
     * Creator is automatically attendee but stays admin if he is parent admin 
     */
    public static function attendee($eventId, $userId, $isAdmin = false, $creator = true)
    {
        //Link::addLink($userId, Person::COLLECTION, $eventId, PHType::TYPE_EVENTS, $userId, "events");
        //Link::addLink($eventId, PHType::TYPE_EVENTS, $userId, Person::COLLECTION, $userId, "attendees");
        $userType = Person::COLLECTION;
        $link2person = "links.events." . $eventId;
        $link2event = "links.attendees." . $userId;
        $where2person = array($link2person . ".type" => Event::COLLECTION, $link2person . ".isAdmin" => $isAdmin);
        $where2event = array($link2event . ".type" => $userType, $link2event . ".isAdmin" => $isAdmin);
        // TODO BOUBOULE - REMOVE THIS LINKS 
        /*if($creator) {
	   		$where2person[$link2person.".isCreator"] = true;
	   		$where2event[$link2event.".isCreator"] = true;
   		}*/
        PHDB::update(
            Person::COLLECTION,
            array("_id" => new MongoId($userId)),
            array('$set' => $where2person)
        );

        PHDB::update(
            PHType::TYPE_EVENTS,
            array("_id" => new MongoId($eventId)),
            array('$set' => $where2event)
        );
    }

    /** TODO BOUBOULE - TO DELETE (NOT USED ANYMORE)
     * Connect 2 actors : Event, Person, Organization or Project
     * Create a link between the 2 actors. The link will be typed as knows, attendee, event, project or contributor
     * 1 entry will be added for example :
     * - $origin.links.knows["$target"]
     * @param type $originId The Id of actor who wants to create a link with the $target
     * @param type $originType The Type (Organization, Person, Project or Event) of actor who wants to create a link with the $target
     * @param type $targetId The actor that will be linked
     * @param type $targetType The Type (Organization, Person, Project or Event) that will be linked
     * @param type $userId The userId doing the action (Optional)
     * @param type $connectType The link between the two actors
     * @return result array with the result of the operation
     */
    /* private static function addLink($originId, $originType, $targetId, $targetType, $userId= null, $connectType){

    	//0. Check if the $originId and the $targetId exists
        $origin = Element::checkIdAndType($originId, $originType);
        $target = Element::checkIdAndType($targetId, $targetType);

        //2. Create the links
        PHDB::update( $originType, 
                       array("_id" => $originId) , 
                       array('$unset' => array("links.".$connectType.".".$targetId => "") ));

        //3. Send Notifications
        //TODO - Send email to the member

        return array("result"=>true, "msg"=>"The link ".$connectType." has been added with success", "originId"=>$originId, "targetId"=>$targetId);
    }*/

    /*
	* function isLinked is generally called on Communecter and two times in models/Person.php
	* it permits to return true if a link between an entity(Person/Orga/Project/Event) and a person exists
	* @param type string $itemId is the id of the entity checked
	* @param type string $itemType is the type of the entity checked
	* @param type string $userId is the id of the user logged
	*/
    public static function isLinked($itemId, $itemType, $userId, $links = null)
    {
        $res = false;
        if ($itemType == Person::COLLECTION) $linkType = self::person2person;
        elseif ($itemType == Organization::COLLECTION) $linkType = self::organization2person;
        elseif ($itemType == Event::COLLECTION) $linkType = self::event2person;
        elseif ($itemType == Project::COLLECTION) $linkType = self::project2person;
        else $linkType = "unknown";

        //var_dump($linkType);
        if (empty($links)) {
            $item = Element::getElementById($itemId, $itemType, null, array("links"));
            $links = @$item["links"];
        }
        //var_dump($links);
        if (isset($links) && isset($links[$linkType])) {
            foreach ($links[$linkType] as $key => $value) {
                if ($key == $userId) {
                    //exception for event when attendee is invited
                    if (!isset($value["isInviting"]))
                        $res = true;
                }
            }
        }
        return $res;
    }

    ///// TODO BOUBOULE - AN ORGANIZER COULD BE A PROJECT OR AN ORGA OR A PERSON
    //// DOCUMENT THIS FUNCTION 
    public static function removeEventLinks($eventId)
    {
        $events = Event::getById($eventId);
        foreach ($events["links"] as $type => $item) {
            foreach ($item as $id => $itemInfo) {
                if ($type == "organizer") {
                    $res = PHDB::update(
                        Organization::COLLECTION,
                        array("_id" => new MongoId($id)),
                        array('$unset' => array("links.events." . $eventId => ""))
                    );
                } else {
                    $res = PHDB::update(
                        Person::COLLECTION,
                        array("_id" => new MongoId($id)),
                        array('$unset' => array("links.events." . $eventId => ""))
                    );
                }
            }
        }
        return $res;
    }

    // TODO BOUBOULE - COULD BE DELETED FOR A BETTER INTERPRETATION OF ROLE
    public static function removeRole($contextId, $contextType, $childId, $childType, $roles, $userId, $connectType)
    {

        //0. Check if the $memberOfId and the $memberId exists
        $memberOf = Element::checkIdAndType($contextId, $contextType);
        $member = Element::checkIdAndType($childId, $childType);

        //1.1 the $userId can manage the $memberOf (admin)
        // Or the user can remove himself from a member list of an organization
        if (!Authorisation::isElementAdmin($contextId, $contextType, $userId)) {
            //if ($memberId != $userId) {
            throw new CTKException("You are not admin of the Organization : " . $contextId);
            //}
        }
        if ($connectType == "members")
            $connectTypeOf = "memberOf";
        else if ($connectType == "contributors" || $connectType == "projectExtern")
            $connectTypeOf = "projects";
        if ($connectType == "attendees")
            $connectTypeOf = "events";
        if (empty($roles)) {
            $action = '$unset'; 
            $roles = 1;
            $msg = "Roles have been removed with success";
        } else {
            $action = '$set';
            if(is_string($roles)){
                $roles = explode(",", $roles);
                foreach($roles as $kr => $vr){
                    $roles[$kr] =  trim($vr);
                } 
            }
            $msg = "Roles have been added with success";
        }
        //2. Remove the role
        PHDB::update(
            $contextType,
            ["_id" => new MongoId($contextId)],
            [$action => ["links." . $connectType . "." . $childId . ".roles" => $roles]]
        );

        //3. Remove the role
        PHDB::update(
            $childType,
            ["_id" => new MongoId($childId)],
            [$action => ["links." . $connectTypeOf . "." . $contextId . ".roles" => $roles]]
        );

        return [
            "result" => true,
            "msg" => Yii::t("common", $msg),
            "memberOfid" => $contextId, "memberid" => $childId, "roles" => $roles
        ];
    }

    /** TODO BOUBOULE - TO DELETE WITH CTK/CONTROLLERS/PERSON/DISCONNECTACTION.PHP
     * Delete a link between the 2 actors.
     * @param $ownerId is the person who want to remowe a link
     * @param $targetId is the id of item we want to be unlink with
     * @param $ownerLink is the type of link between the owner and the target
     * @param $targetLink is the type of link between the target and the owner
     * @return result array with the result of the operation
     */
    public static function disconnectPerson($ownerId, $ownerType, $targetId, $targetType, $ownerLink, $targetLink = null)
    {

        //0. Check if the $owner and the $target exists
        $owner = Element::checkIdAndType($ownerId, $ownerType);
        $target = Element::checkIdAndType($targetId, $targetType);

        //1. Remove the links
        PHDB::update(
            $ownerType,
            array("_id" => new MongoId($ownerId)),
            array('$unset' => array("links." . $ownerLink . "." . $targetId => ""))
        );

        if (isset($targetLink) && $targetLink != null) {
            PHDB::update(
                $targetType,
                array("_id" => new MongoId($targetId)),
                array('$unset' => array("links." . $targetLink . "." . $ownerId => ""))
            );
        }

        //3. Send Notifications

        return array("result" => true, "msg" => "The link has been removed with success");
    }


    /** TODO BOUBOULE - NOT USE ANYMORE === TO DELETE 
     * Add a link between the 2 actors.
     * @param $ownerId is the person who want to add a link
     * @param $targetId is the id of item we want to be link with
     * @param $ownerLink is the type of link between the owner and the target
     * @param $targetLink is the type of link between the target and the owner
     * @return result array with the result of the operation
     */
    /*public static function connectPerson($ownerId, $ownerType, $targetId, $targetType, $ownerLink, $targetLink = null){
    	 //0. Check if the $owner and the $target exists
        $owner = Element::checkIdAndType($ownerId, $ownerType);
        $target = Element::checkIdAndType($targetId, $targetType);

        PHDB::update( $ownerType, 
           array("_id" => new MongoId($ownerId)) , 
           array('$set' => array( "links.".$ownerLink.".".$targetId.".type" => $targetType) ));

        //Mail::newConnection();

        if(isset($targetLink) && $targetLink != null){
         	$newObject = array('type' => $ownerType );
	        PHDB::update( $targetType, 
			               array("_id" => new MongoId($targetId)) , 
			               array('$set' => array( "links.".$targetLink.".".$ownerId.".type" => $ownerType) ));
	    }

        return array("result"=>true, "msg"=>"The link has been added with success");
    }*/

    /**
     * Add a link between the 2 entity (person to an entity).
     * @param $parentId is the id of parent where we add a link followers
     * @param $parentType is the type of parent wher user wants to follows
     * @param $child is an array having id, type of user who wants to follows
     * @return result array with the result of the operation
     */
    public static function follow($parentId, $parentType, $child)
    {

        $childId = @$child["childId"];
        $childType = $child["childType"];
        $levelNotif=null;
		if ( in_array($parentType, array( Organization::COLLECTION, Project::COLLECTION, Classified::COLLECTION, Form::COLLECTION ) ) ){
            $parentData = Element::getByTypeAndId($parentType, $parentId);           
            $parentController = Element::getControlerByCollection($parentType);
        } else if ($parentType == Person::COLLECTION) {
            $parentData = Person::getById($parentId);
            $parentController = Person::CONTROLLER;
            $levelNotif = "user";
        } else
            throw new CTKException(Yii::t("common", "Can not manage the type ") . $parentType);

        //Retrieve the child info
        //var_dump($childId);
        $pendingChild = Person::getById($childId);
        //var_dump($pendingChild);exit;
        if (!$pendingChild) {
            return array("result" => false, "msg" => "Something went wrong ! Impossible to find the children " . $childId);
        }
        $parentConnectAs = "followers";
        $childConnectAs = "follows";
        $verb = ActStr::VERB_FOLLOW;
        $msg = Yii::t("common", "You are following") . " " . $parentData["name"];
        Link::connect($parentId, $parentType, $childId, $childType, Yii::app()->session["userId"], $parentConnectAs);
        Link::connect($childId, $childType, $parentId, $parentType, Yii::app()->session["userId"], $childConnectAs);
        // if($parentType==Person::COLLECTION)
        //     Mail::follow($parentData, $parentType);
        //else
          //  Mail::follow($element, $elementType, $listOfMail);
        Notification::constructNotification($verb, $pendingChild , array("type"=>$parentType,"id"=> $parentId,"name"=>$parentData["name"]), null, $levelNotif);
        
		return array( "result" => true , "msg" => $msg, "parent" => $parentData );
	}
	 /**
     * Check and remove the link "follows" if a user already follow an entity and will become a member or contributor or knows
     * @param $parentId is the id of parent where we add a link followers
     * @param $parentType is the type of parent wher user wants to follows
     * @param $child is an array having id, type of user who wants to follows
     * @return result array with the result of the operation
     */
    public static function checkAndRemoveFollowLink($parentId, $parentType, $childId, $childType)
    {
        if ($childType == Person::COLLECTION) {
            $person = Person::getById($childId);
            if (isset($person["links"]["follows"][$parentId]) && $person["links"]["follows"][$parentId]["type"] == $parentType) {
                Link::disconnect($childId, $childType, $parentId, $parentType, Yii::app()->session['userId'], "follows");
                Link::disconnect($parentId, $parentType, $childId, $childType, Yii::app()->session['userId'], "followers");
            }
        }
    }
    /**
     * Author @clement.damiens@gmail.com && @sylvain.barbot@gmail.com
     * Connect A Child to parent with a link. 
     * Child can be a person or an organization.
     * Parent can be an Organization or a Project
     * The child can be set as admin of the parent
     * 
     * @param String $parentId Id of the parent to link to
     * @param String $parentType type of the parent to link to
     * @param Array $child array discribing a child to link to the parent. 
     *                         childId => the id of the child. Could be empty if it's an invitation
     *                         childType => the type of the child. Mandatory
     *                         childName => Used if it's an invitation
     *                         childEmail => Used if it's an invitation
     * @param boolean $isConnectingAdmin Is the child admin ? 
     * @param String $userId The userId doing the action
     * @param string $userRole The role the child inside the parent. Not use yet.
     * @return array of result ()
     */
    public static function connectParentToChild($parentId, $parentType, $child, $isConnectingAdmin, $userId, $userRole = "")
    {
        $typeOfDemand = "admin";
        $childId = @$child["childId"];
        $childType = $child["childType"];
        $isInviting = false;
        $levelNotif = null;

        $parentData = Element::getElementSimpleById($parentId, $parentType, null, array("_id", "name", "links", "title"));
        $usersAdmin = ($parentType != Person::COLLECTION) ? Authorisation::listAdmins($parentId,  $parentType, false) : [];
        $actionFromAdmin = ($parentType != Person::COLLECTION) ? in_array($userId, $usersAdmin) : false;


        $actionFromMember = false;
        if ($parentType == Form::COLLECTION) {
            $parentData["name"] = $parentData["title"];
            $parentUsersList = Form::getLinksFormsByFormId($parentId, "all", null);

            if ($childType == Project::COLLECTION &&  !empty($child["link"]) && $child["link"] == "projectExtern") {
                $parentConnectAs = "projectExtern";
                $childConnectAs = "forms";
                if (!$isConnectingAdmin)
                    $typeOfDemand = "projectExtern";
            } else {
                $parentConnectAs = "members";
                $childConnectAs = "forms";
                if (!$isConnectingAdmin)
                    $typeOfDemand = "members";
            }
        } else {
            $parentUsersList = Element::getCommunityByTypeAndId($parentType, $parentId, "all");
            // controller de l'event ici
            $parentController = Element::getControlerByCollection($parentType);
            $parentConnectAs = self::$linksTypes[$parentType][$childType];
            if ($childType == Organization::COLLECTION && $parentType == Organization::COLLECTION) 
                $childConnectAs = "memberOf";
            else
                $childConnectAs=self::$linksTypes[$childType][$parentType];
                
            if(!$isConnectingAdmin)
                $typeOfDemand=Link::$connectTypes[$parentType];

           
        }
        if ($parentType == Proposal::COLLECTION)
            $parentData["name"] = $parentData["title"];

        if($parentType == Form::ANSWER_COLLECTION){
            $ans = PHDB::findOneById(Form::ANSWER_COLLECTION,$parentId);
            if(isset($ans["answers"]["aapStep1"]["titre"]))
                $parentData["name"] = $ans["answers"]["aapStep1"]["titre"];
        }
        //else {
        //throw new CTKException(Yii::t("common","Can not manage the type ").$parentType);
        //}


        if (!$parentData) {
            return array("result" => false, "msg" => "Unknown " . $parentController . ". Please check your parameters !");
        }
        //Check if the user is admin

        if (isset($parentUsersList[$userId]))
            $actionFromMember = true;
        if ($childType == Organization::COLLECTION) {
            $class = "Organization";
            //ou Child type Person
        } else if ($childType == Person::COLLECTION) {
            $class = "Person";
        } else if ($childType == Project::COLLECTION) {
            $class = "Project";
        } else {
            //Rest::json($parentData); exit ;
            return array("result" => false, "msg" => "Unknown " . $childType . ". Please check your parameters !");
        }


        //if the childId is empty => it's an invitation
        //Let's create the child
        //TODO RAPHA CLEM : Est-ce que on peut arriver la sans que childID sois renseigner ?

        if (empty($childId)) {
            $invitation = ActStr::VERB_INVITE;
            $child = array(
                'name' => @$child["childName"],
                'email' => @$child["childEmail"],
                'invitedBy' => Yii::app()->session["userId"]
            );

            //Child Type d'organization
            if ($childType == Organization::COLLECTION) {
                $child["type"] = (isset($_POST["organizationType"])) ? $_POST["organizationType"] : "";
            }

            //create an entry in the right collection type
            $result = $class::createAndInvite($child);
            if ($result["result"]) {
                $childId = $result["id"];
            } else {
                return array("result" => false, "name" => @$child["childName"], "type" => $childType, "email" => @$childEmail, "msgError" => $result);
            }
        }

        //Retrieve the child info
        //$pendingChild = $class::getById($childId);
        $pendingChild = Element::getElementSimpleById($childId, $childType, null, ["_id", "name", "profilThumbImageUrl"]);
        $pendingChild["id"] = $childId;
        if (!$pendingChild) {
            return array("result" => false, "msg" => "Something went wrong ! Impossible to find the children " . $childId);
        }

        //Check if the child is already link to the parent with the connectType
        $alreadyLink = false;
        if ($typeOfDemand != "admin") {
            if (@$parentUsersList[$childId] && $userId != $childId)
                $alreadyLink = true;
        } else {
            if (@$parentUsersList[$childId] && @$parentUsersList[$childId]["isAdmin"])
                $alreadyLink = true;
        }
        if ($alreadyLink)
            return array("result" => false, "type" => "info", "msg" => $pendingChild["name"] . " " . Yii::t("common", "is already " . $typeOfDemand . " of") . " " . Yii::t("common", "this " . $parentController) . " !");

        //Check : You are already member or admin
        if ($actionFromAdmin && $userId == $childId)
            return array("result" => false, "type" => "info", "msg" => Yii::t("common", "You are already admin of") . " " . Yii::t("common", "this " . $parentController) . " !");
        // else if(!$alreadyLink && $userId == $childId)
        //   $askingToBecome=true;
        else if (!$alreadyLink && $userId != $childId && Authorisation::isInterfaceAdmin())
            $actionFromAdmin = true;

        if ($parentType == Proposal::COLLECTION)
            $levelNotif = Proposal::COLLECTION;
        else if ($parentType == Person::COLLECTION)
            $levelNotif = "user";
        else if ($isConnectingAdmin == true)
            $levelNotif = "asAdmin";
        else
            $levelNotif = "asMember";
        $userIsChild = ($childId == Yii::app()->session["userId"]) ? true : false;
        //First case : The parent doesn't have an admin yet or it is an action from an admin or it is an event or a friend actions: 
        if (
            count($usersAdmin) == 0 ||
            $actionFromAdmin ||
            ($actionFromMember && !$userIsChild) ||
            in_array($parentType, [Event::COLLECTION, Person::COLLECTION])
        ) {

            //the person is automatically added as member (admin or not) of the parent
            $inviteProcess = false;
            if ($actionFromAdmin)
                $inviteProcess = true;
            else if ($actionFromMember && !$userIsChild)
                $inviteProcess = true;
            else if ($parentType == Event::COLLECTION && !$userIsChild)
                $inviteProcess = true;
            else if ($parentType == Person::COLLECTION)
                $inviteProcess = true;
            else if (count($usersAdmin) == 0 && !$userIsChild)
                $inviteProcess = true;
            if($parentType == Form::ANSWER_COLLECTION)
                $inviteProcess = false;

            if ($inviteProcess) {
                //If admin add as admin or member
                $verb = ActStr::VERB_INVITE;
                if ($isConnectingAdmin == true) {
                    if (
                        @$parentData["links"]
                        && @$parentData["links"][$parentConnectAs]
                        && @$parentData["links"][$parentConnectAs][$childId]
                        && @$parentData["links"][$parentConnectAs][$childId][self::TO_BE_VALIDATED]
                    ) {
                        $isInviting = "admin";
                        self::validateLink($parentId, $parentType, $childId, $childType, self::TO_BE_VALIDATED, Yii::app()->session["userId"]);
                    }
                    $msg=Yii::t("common","{who} is well invited to administrate {what}",array("{who}"=>$pendingChild["name"],"{what}"=>$parentData["name"]));
					$pendingChild["isAdmin"]=true;
				} else if($parentType==Person::COLLECTION){
                    $verb=ActStr::VERB_INVITE;
                    $msg=Yii::t("common","Invitation has been successfully send to {who}", array("{who}"=>$pendingChild["name"])); 
                    $levelNotif=Person::COLLECTION;
                  //  $targetIsAuthor=true;
                }
                else
					$msg=Yii::t("common","{who} is well invited to join {what}",array("{who}"=>@$pendingChild["name"],"{what}"=>@$parentData["name"]));
                //SPECIFIC USECASE FOR ORGANIZATION => NO INVITATION PROCESS
                if ($childType == Organization::COLLECTION || $childType == Project::COLLECTION) {
                    $verb = ActStr::VERB_ADD;
                    $msg = Yii::t("common", "{who} is well added to {what}", array("{who}" => $pendingChild["name"], "{what}" => @$parentData["name"]));
                } else {
                    if (isset($parentUsersList[$childId]) || $isInviting === "admin") {
                        $isInviting = "admin";
                    } else {
                        $isInviting = true;
                    }
                    $pendingChild["isInviting"] = true;
                }
                $toBeValidated = false;
                //Mail::someoneInviteYouToBecome($parentData, $parentType, $pendingChild, $typeOfDemand);
            } else {
                // Verb Confirm in ValidateLink
                $verb = ActStr::VERB_JOIN;
                $toBeValidated = false;
                $msg = Yii::t("common", "You are now {what} of {where}", array("{what}" => Yii::t("common", $typeOfDemand), "{where}" => Yii::t("common", "this " . $parentController)));
            }

            // Check if links follows exists than if true, remove of follows and followers links
            self::checkAndRemoveFollowLink($parentId, $parentType, $childId, $childType);
            $toBeValidatedAdmin = false;


            //Second case : Not an admin doing the action.
        } else {

            //Someone ask to become an admin
            $verb = ActStr::VERB_ASK;
            if ($isConnectingAdmin) {
                //Admin validation process
                $toBeValidatedAdmin = true;
                $toBeValidated = false;
                $pendingChild["isAdminPending"] = true;
                if (!@$parentUsersList[$childId]) {
                    $toBeValidated = true;
                    $pendingChild["toBeValidated"] = true;
                }
            } else {
                $toBeValidatedAdmin = false;
                $toBeValidated = true;
                $pendingChild["toBeValidated"] = true;
            }
            //Notification and email are sent to the admin(s)
            //CREATE VARIABLE OF EMAIL AND GENERALIZE EMAIL someoneDemandToBecome || someoneInvitingYouTo
            $listofAdminsEmail = array();
            foreach ($usersAdmin as $adminId) {
                $currentAdmin = Person::getEmailById($adminId);
                array_push($listofAdminsEmail, $currentAdmin["email"]);
            }
            //CREATE VARIABLE OF EMAIL AND GENERALIZE EMAIL someoneDemandToBecome || someoneInvitingYouTo
            if (count($listofAdminsEmail)) {
                // Mail::someoneDemandToBecome($parentData, $parentType, $pendingChild, $listofAdminsEmail, $typeOfDemand);
            }
            //TODO - Notification
            $msg = Yii::t("common", "Your request has been sent to other admins.");
            // After : the 1rst existing Admin to take the decision will remove the "pending" to make a real admin
        }

        $valNotif = array(
            "typeOfDemand" => $typeOfDemand,
            "verb" => $verb
        );
        //Rest::json($valNotif);
        Link::connect($parentId, $parentType, $childId, $childType, Yii::app()->session["userId"], $parentConnectAs, $isConnectingAdmin, $toBeValidatedAdmin, $toBeValidated, $isInviting, $userRole);
        Link::connect($childId, $childType, $parentId, $parentType, Yii::app()->session["userId"], $childConnectAs, $isConnectingAdmin, $toBeValidatedAdmin, $toBeValidated, $isInviting, $userRole);
        Notification::constructNotification($verb, $pendingChild , array("type"=>$parentType,"id"=> $parentId,"name"=>@$parentData["name"]), null, $levelNotif, null, $valNotif);

        $res = array("result" => true, "msg" => $msg, "parent" => $parentData, "parentType" => $parentType, "newElement" => $pendingChild, "newElementType" => $childType);

        return $res;
    }

    /**
     * Description
     * @param type $parentId 
     * @param type $parentType 
     * @param type $childId 
     * @param type $childType 
     * @param type $linkOption 
     * @param type $userId 
     * @return type
     */
    public static function validateLink($parentId, $parentType, $childId, $childType, $linkOption, $userId)
    {

        $res = array("result" => false, "msg" => Yii::t("common", "Something went wrong!"));

        if ($childType == Organization::COLLECTION) {
            $class = "Organization";
            if ($linkOption == Link::IS_ADMIN_PENDING) {
                return array("result" => false, "msg" => "Impossible to validate an organization as admin !");
            }
            //ou Child type Person
        } else if ($childType == Person::COLLECTION) {
            $class = "Person";
        } else {
            return array("result" => false, "msg" => "Unknown " . $childType . ". Please check your parameters !");
        }
        //Retrieve the child info
        $pendingChild = $class::getById($childId, false);
        if (!$pendingChild) {
            return array("result" => false, "msg" => "Something went wrong ! Impossible to find the children " . $childId);
        }

        //Retrieve parent and connection
        if (in_array($parentType, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Person::COLLECTION])) {
            $parent = Element::getElementById($parentId, $parentType, null, array("name", "links"));
            $connectTypeOf = Link::$linksTypes[Person::COLLECTION][$parentType];
            $connectType = Link::$linksTypes[$parentType][Person::COLLECTION];
            $usersAdmin = Authorisation::listAdmins($parentId,  $parentType, false);
            $typeOfDemand = Link::$connectTypes[$parentType];
        } else if ($parentType == Form::COLLECTION) {
            $parent = Form::getByIdMongo($parentId);
            $parent["name"] = $parent["title"];
            $connectTypeOf = "forms";
            $connectType = "members";
            $typeOfDemand = "members";
            $usersAdmin = Authorisation::listAdmins($parentId,  $parentType, false);
        } else {
            throw new CTKException(Yii::t("common", "Can not manage the type ") . $parentType);
        }
        if ($linkOption == Link::IS_ADMIN_PENDING)
            $typeOfDemand = "admin";
        //Check if the user is admin
        $actionFromAdmin = in_array($userId, $usersAdmin);
        //Check the link exists in order to update it
        if (isset($parent["links"][$connectType][$childId][$linkOption])
            && $parent["links"][$connectType][$childId][$linkOption] &&
            isset($pendingChild["links"][$connectTypeOf][$parentId][$linkOption]) &&
            $pendingChild["links"][$connectTypeOf][$parentId][$linkOption]
        ) {

            self::checkAndRemoveFollowLink($parentId, $parentType, $childId, $childType);
            self::updateLink($parentType, $parentId, $childId, $childType, $connectType, $connectTypeOf, $linkOption);
            if (
                $linkOption == Link::IS_ADMIN_PENDING &&
                (@$parent["links"][$connectType][$childId][Link::TO_BE_VALIDATED] &&
                    @$pendingChild["links"][$connectTypeOf][$parentId][Link::TO_BE_VALIDATED])
            )
                self::updateLink($parentType, $parentId, $childId, $childType, $connectType, $connectTypeOf, Link::TO_BE_VALIDATED);
        } else {
            return array(
                "result" => false,
                "msg" => "The link " . $linkOption . " does not exist between " . @$parent["name"] . " and " . @$pendingChild["name"]
            );
        }
        $user = array(
            "id" => $childId,
            "type" => $childType,
            "name" => $pendingChild["name"]
        );
        $target = array(
            "type" => $parentType,
            "id" => $parentId,
            "name" => $parent["name"]
        );
        //Notifications
        if ($linkOption == Link::IS_ADMIN_PENDING) {
            $verb=ActStr::VERB_ACCEPT;
            $levelNotif = "asAdmin";
            $msg = Yii::t("common", "{who} has been validated as admin of {what}", array("{who}" => $pendingChild["name"], "{what}" => $parent["name"]));
            //MAIL TO CHILDREN 
            //REMOVE ASK NOTIF FOR COMMUNITY
        } else if ($linkOption == Link::TO_BE_VALIDATED) {
            $verb=ActStr::VERB_ACCEPT;
            $levelNotif="asMember";
            $msg = Yii::t("common","{who} has been validated as member of {what}",array("{who}"=>$pendingChild["name"],"{what}"=>$parent["name"]));
            //MAIL TO CHILDREN
            //REMOVE ASK NOTIF FOR COMMUNITY
        } else if ($linkOption == Link::IS_INVITING || $linkOption == Link::IS_ADMIN_INVITING) {
            $verb = ActStr::VERB_CONFIRM;
            $msg = Yii::t("common", "Your answer has been succesfully registered");
            if (@$pendingChild["links"][$connectType][$parentId]["isAdmin"] && @$parent["links"][$connectType][$childId]["isAdmin"])
                $levelNotif = "asAdmin";
            else if ($parentType == Person::COLLECTION)
                $levelNotif = "asFriend";
            else
                $levelNotif = "asMember";
            $target["invitorId"] = $parent["links"][$connectType][$childId]["invitorId"];
            //MAIL TO INVITOR
        }
        if ($verb == ActStr::VERB_ACCEPT)
            Mail::someoneConfirmYouTo($parent, $parentType, $pendingChild, $typeOfDemand);
        Notification::constructNotification($verb, $user, $target, null, $levelNotif);
        return array("result" => true, "msg" => $msg);
    }

    /*
	* This function is similar to disconnect but he just remove a value as pending, isAdminPending, isPending
	* @valueUpdate is string to know which line disconnect
	* Using for acceptAsAdmin, AcceptAsMember, etc...
	*/
    private static function updateLink($parentType, $parentId, $userId, $userType, $connectType, $connectTypeOf, $valueUpdate)
    {
        PHDB::update(
            $parentType,
            array("_id" => new MongoId($parentId)),
            array('$unset' => array("links." . $connectType . "." . $userId . "." . $valueUpdate => ""))
        );
        PHDB::update(
            $userType,
            array("_id" => new MongoId($userId)),
            array('$unset' => array("links." . $connectTypeOf . "." . $parentId . "." . $valueUpdate => ""))
        );
        return array("result" => true, "msg" => "The link has been added with success");
    }


    public static function multiconnect($child, $parentId, $parentType)
    {
        $result = array("result" => false, "msg" => Yii::t("common", "Incorrect request"));
        $client = null;
        if(Yii::app()->params['cows']['enable']){
            $client = Client::create(str_replace("wss:", "https:", Yii::app()->params['cows']['serverUrl']), ["client" => Client::CLIENT_4X]);
            $client->connect();
        }
        
        if (!Person::logguedAndValid()) {

            if($client != null){
                $client->disconnect();
            }
            return array("result" => false, "msg" => Yii::t("common", "You are not loggued or do not have acces to this feature "));
        }
        $newMembers = array();
        $msg = false;
        $finalResult = false;
        $onlyOrganization = true;

        foreach ($child as $key => $contact) {

            if (@$contact["childId"] != $parentId) {
                $roles = "";
                $child2 = array(
                    "childId" => @$contact["childId"],
                    "childType" => @$contact["childType"] == "people" ? "citoyens" : @$contact["childType"],
                    "childName" => @$contact["childName"],
                    "childEmail" => @$contact["childEmail"],
                );
                if (!empty($contact["roles"])){
                    foreach($contact["roles"] as $kr => $vr){
                        $contact["roles"][$kr] =  trim($vr);
                    }
                    $roles = $contact["roles"];
                }
                // if (!empty($contact["roles"]))
                //     $roles = $contact["roles"];
                if ($child2["childType"] == Person::COLLECTION)
                    $onlyOrganization = false;

                if (!empty($contact["link"]))
                    $child2["link"] = $contact["link"];
                $isConnectingAdmin = (@$contact["connectType"] == "admin") ? true : false;
                $res = Link::connectParentToChild($parentId, $parentType, $child2, $isConnectingAdmin, Yii::app()->session["userId"], $roles);

                if ($res["result"] == true) {
                    if($child2["childType"] == Organization::COLLECTION){
                        if($client != null){
                            $client->emit('directory_event', [
                                'name' => 'join_user',
                                "id" => $parentId,
                                'data' => Element::getByTypeAndId(Organization::COLLECTION, $child2["childId"])
                            ]);
                        }
                    }else{
                        if($client != null){
                            $client->emit('directory_event', [
                                'name' => 'invite_user',
                                "id" => $parentId,
                                'data' => [
                                    "invitorId" => Yii::app()->session["userId"],
                                    "invitorName" => Yii::app()->session["user"]["name"],
                                    "invitedId" => $child2["childId"],
                                    "donne" => Element::getByTypeAndId(Citoyen::COLLECTION, $child2["childId"])
                                ]
                            ]);
                        }
                    }
                    if ($msg != 2)
                        $msg = 1;
                    $newMember = $res["newElement"];
                    $newMember["childType"] = $res["newElementType"];
                    array_push($newMembers, $newMember);
                    $finalResult = true;
                } else {
                    if ($msg == 1) {
                        $msg = 2;
                    } else if ($msg != 2) {
                        $msg = false;
                    }
                }
            }
        }


        if ($finalResult == true) {
            if ($msg == 1)
                $msg = Yii::t("common", "New member(s) have been succesfully added");
            else
                $msg = Yii::t("common", "New member(s) have been succesfully added except those already in the community");
            $result = array("result" => true, "msg" => $msg, "newMembers" => $newMembers, "onlyOrganization" => $onlyOrganization);
        } else $res = $result;


        if($client != null){
            $client->disconnect();
        }
        //var_dump($res); exit;
        return $res;
    }


    public static function invite($list, $parent)
    {
        $res = array(); 
        if (!empty($list["citoyens"]) && count($list["citoyens"]) > 0) {
            foreach ($list["citoyens"] as $key => $value) {
                if(!empty($value["roles"]))
                    foreach($value["roles"] as $kr => $vr){
                        $value["roles"][$kr] =  trim($vr);
                    }  
                $child = array();
                /*if($parent["parentType"] == Person::COLLECTION){
                    $child[] = array( "childId" => $parent["parentId"],
                                    "childType" => $parent["parentType"]);
                    $res["citoyens"][] = Link::multiconnect($child, $parent["parentId"], $parent["parentType"]);
                }
                else*/
                if ($parent["parentType"] == Action::COLLECTION) {

                    $params = array(
                        "id" => $parent["parentId"],
                        "child" => array(
                            "id" => $key,
                            "type" => Person::COLLECTION,
                            "name" => @$value["name"]
                        )
                    );
                    $res["citoyens"][] = ActionRoom::assignPeople($params);
                } else {
                    $child = array();
                    $child[] = array(
                        "childId" => $key,
                        "childType" => Person::COLLECTION,
                        "childName" => @$value["name"],
                        "roles" => (empty($value["roles"]) ? array() : $value["roles"]),
                        "connectType" => (empty($value["isAdmin"]) ? "" : $value["isAdmin"])
                    );
                    $res["citoyens"][] = Link::multiconnect($child, $parent["parentId"], $parent["parentType"]);
                }
            }
        }

        if (!empty($list["invites"]) && count($list["invites"]) > 0) {
            $res["invites"] = array();
            foreach ($list["invites"] as $key => $value) { 
                if(!empty($value["roles"]))
                    foreach($value["roles"] as $kr => $vr){
                        $value["roles"][$kr] =  trim($vr);
                    }
                $resMail = Mail::authorizationMail($value["mail"]);
                if ($resMail == false) {

                    $res["invites"][] = array("result" => false, "msg" => Yii::t("common", "You cannot enter this email address : ") . $value["mail"]);
                } else {
                    $child = array();
                    $newPerson = array(
                        "name" => $value["name"],
                        "email" => $value["mail"],
                        "invitedBy" => Yii::app()->session["userId"]
                    );

                    $creatUser = Person::createAndInvite($newPerson, @$value["msg"]);
                    if (!empty($creatUser["result"]) && $creatUser["result"] == true) {
                        if ($parent["parentType"] == Person::COLLECTION) {
                            $child = array();
                            $child[] = array(
                                "childId" => $creatUser["id"],
                                "childType" => Person::COLLECTION,
                                "childName" => $newPerson["name"],
                                "roles" => (empty($value["roles"]) ? array() : $value["roles"]),
                                "connectType" => (empty($value["isAdmin"]) ? "" : $value["isAdmin"])
                            );

                            $res["invites"][] = Link::multiconnect($child, $parent["parentId"], $parent["parentType"]);
                        } else if ($parent["parentType"] == Action::COLLECTION) {
                            $params = array(
                                "id" => $parent["parentId"],
                                "child" => $creatUser["id"]
                            );
                            $res["citoyens"][] = ActionRoom::assignPeople($params);
                        } else {
                            $child = array();
                            $child[] = array(
                                "childId" => $creatUser["id"],
                                "childType" => Person::COLLECTION,
                                "childName" => $value["name"],
                                "roles" => (empty($value["roles"]) ? array() : $value["roles"]),
                                "connectType" => (empty($value["isAdmin"]) ? "" : $value["isAdmin"])
                            );
                            $res["invites"][] = Link::multiconnect($child, $parent["parentId"], $parent["parentType"]);
                        }
                    }
                }
            }
        }

        if (!empty($list["organizations"]) && count($list["organizations"]) > 0) {

            foreach ($list["organizations"] as $key => $value) {
                if(!empty($value["roles"]))
                    foreach($value["roles"] as $kr => $vr){
                        $value["roles"][$kr] =  trim($vr);
                    }
                $child = array();
                if ($parent["parentType"] == Person::COLLECTION) {
                    $child = array(
                        "id" => $key,
                        "type" => Person::COLLECTION
                    );
                    $res["citoyens"][] = Link::follow($parent["parentId"], $parent["parentType"], $child);
                } else {
                    $child = array();
                    $child[] = array(
                        "childId" => $key,
                        "childType" => Organization::COLLECTION,
                        "childName" => $value["name"],
                        "roles" => (empty($value["roles"]) ? array() : $value["roles"])
                    );

                    $res["organizations"][] = Link::multiconnect($child, $parent["parentId"], $parent["parentType"]);
                }
            }
        }

        return $res;
    }
    public static function hasRoles($id, $type, $roles)
    {
        if (isset(Yii::app()->session["userId"])) {
            $linksElt = Element::getElementById($id, $type, null, array("links"));
            if (!empty($linksElt["links"]) && isset($linksElt["links"][self::$linksTypes[$type][Person::COLLECTION]])) {
                foreach ($linksElt["links"][self::$linksTypes[$type][Person::COLLECTION]] as $k => $v) {
                    if (isset($v["roles"]) && !empty(array_intersect($v["roles"], $roles))) {
                        if ($k == Yii::app()->session["userId"])
                            return true;
                        //true in isElementAdmin just to not check parent of element send
                        else if ($v["type"] != Person::COLLECTION && Authorisation::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true))
                            return true;
                    }
                }
            }
        }
        return false;
    }
    // $links is an extract of links ex : $c["links"]["contributors"]
    // $fields : list of wanted fields from DB, no need to get everything when null
    public static function groupFind($links, $fields = null)
    {

        $idsLCitoyen = array();
        $idsLOrga = array();
        $types = array();
        $ids = array();

        //group all ids together by type
        foreach ($links as $i => $t) {
            if (isset($t["type"])) {
                if (!isset($types[$t["type"]])) {
                    $res[$t["type"]] = array();
                    $types[$t["type"]] = array();
                }

                $types[$t["type"]][] = new MongoId($i);
            }
        }

        $res = array();

        //db.getCollection('documents').find({"_id" : { '$in':[ObjectId("5d273f4e539f22c821e9c255"),ObjectId("5d273f4e539f226948e9c259")]})
        foreach ($types as $type => $list) {
            //var_dump( array("_id" => array( '$in'=> $list )));
            $res[$type] = PHDB::find($type, array("_id" => array('$in' => $list)), $fields);
        }
        //var_dump($res);exit;
        return $res;
    }

    public static function groupFindByType($type, $links, $fields = null, $extraWhere = null)
    {

        $ids = array();

        //group all ids together by type
        foreach ($links as $i => $id) {
            if (is_string($id) && strlen($id) == 24 && ctype_xdigit($id))
                $ids[] = new MongoId($id);
            else
                $ids[] = new MongoId($i);
        }
        $where = array("_id" => array('$in' => $ids));
        if ($extraWhere)
            $where = array_merge($where, $extraWhere);
        $list = PHDB::find($type, $where, $fields);
        //reorderr in same order 
        $res = array();
        foreach ($links as $i => $id) {
            if (is_string($id) && strlen($id) == 24 && ctype_xdigit($id) && !empty($list[$id]))
                $res[$id] = $list[$id];
            else if (!empty($list[$i]))
                $res[$i] = $list[$i];
        }
        return  $res;
    }
    public static function groupFindAppendAttribute($links, $fields = array("name", "profilThumbImageUrl", "profilMediumImageUrl", "slug"))
    {
        $ids = array(
            Person::COLLECTION => [],
            Organization::COLLECTION => [],
            Project::COLLECTION => [],
            Event::COLLECTION => []
        );
        //$types = array();
        //        $ids = array();

        //group all ids together by type
        foreach ($links as $i => $t) {
            if (isset($t["type"]))
                array_push($ids[$t["type"]], new MongoId($i));
        }

        // $res = array();

        //db.getCollection('documents').find({"_id" : { '$in':[ObjectId("5d273f4e539f22c821e9c255"),ObjectId("5d273f4e539f226948e9c259")]})
        foreach ($ids as $type => $list) {
            //var_dump( array("_id" => array( '$in'=> $list )));
            $links = array_merge_recursive(
                $links,
                PHDB::find(
                    $type,
                    array(
                        "_id" => array('$in' => $list),
                        '$or' => array(
                            array('preferences.private' => array('$exists' => false)),
                            array('preferences.private' => false),
                        )
                    ),
                    $fields
                )
            );
        }
        foreach ($links as $e => $v) {
            if (!isset($v["name"])) {
                unset($links[$e]);
            }
        }
        return $links;
    }

    public static function getParentsCommunity($el)
    {
        $communityLinks = array();
        if (!empty($el["parent"])) {
            foreach ($el["parent"] as $key => $value) {
                $cl = Element::getCommunityByTypeAndId($key, $value["type"]);
                if (!empty($cl))
                    $communityLinks = array_merge($communityLinks, $cl);
            }
        }
        return $community;
    }

    public static function connectByUrl($invitationRef){
        $invitationLink = PHDB::findOne(InvitationLink::COLLECTION, ["ref" => $invitationRef]);
        if(!$invitationLink)
            return ["result" => false, "code" => "INVALID_INVITATION_REF"];
        if(!Yii::app()->session["userId"])
            return ["result" => false, "code" => "USER_NOT_LOGGED"];

        $element = Element::getByTypeAndId($invitationLink["targetType"], $invitationLink["targetId"]);
        if(!$element)
            return ["result" => false, "code" => "ELEMENT_NOT_EXIST"];

        $invitor = Person::getById($invitationLink["invitorId"]);
        if(!$invitor)
            return ["result" => false, "code" => "INVITOR_NOT_EXIST"];

        $elementPersonLink = self::$linksTypes[$invitationLink["targetType"]][Person::COLLECTION];
        $alreadyLinked = isset($element["links"][$elementPersonLink][Yii::app()->session["userId"]]);
        if($alreadyLinked)
            return ["result" => true];
    
        $isAdmin = isset($invitationLink["isAdmin"]) && filter_var($invitationLink["isAdmin"], FILTER_VALIDATE_BOOLEAN);

        
        $elementToPersonKey = "links.".$elementPersonLink.".".Yii::app()->session["userId"];
        $elementToPerson = [
            $elementToPersonKey => [
                "inviteByLink" => true,
                "inviteLinkId" => $invitationLink["_id"], 
                "invitorId" => $invitationLink["invitorId"],
                "invitorName" => $invitor["name"],
                "type" => Person::COLLECTION
            ]
        ];

        if($isAdmin)
            $elementToPerson[$elementToPersonKey]["isAdmin"] = true;

        if(isset($invitationLink["roles"]) && is_array($invitationLink["roles"])){
            foreach($invitationLink["roles"] as $kr => $vr){
                $invitationLink["roles"][$kr] =  trim($vr);
            }
            $elementToPerson[$elementToPersonKey]["roles"] = $invitationLink["roles"];
        }

        PHDB::update(
            $invitationLink["targetType"],
            ["_id" => new MongoId($invitationLink["targetId"])],
            ['$set' => $elementToPerson]
        );

        $personElementLink = self::$linksTypes[Person::COLLECTION][$invitationLink["targetType"]];
        $personToElementKey = "links.".$personElementLink.".".$invitationLink["targetId"];
        $personToElement = [
            $personToElementKey => [
                "inviteByLink" => true,
                "inviteLinkId" => $invitationLink["_id"], 
                "invitorId" => $invitationLink["invitorId"],
                "invitorName" => $invitor["name"],
                "type" => $invitationLink["targetType"]
            ]
        ];
        if($isAdmin)
            $personToElement[$personToElementKey]["isAdmin"] = true;
        if(isset($invitationLink["roles"]) && is_array($invitationLink["roles"])){
            foreach($invitationLink["roles"] as $kr => $vr){
                $invitationLink["roles"][$kr] =  trim($vr);
            }
            $personToElement[$personToElementKey]["roles"] = $invitationLink["roles"];
        }

        PHDB::update(
            Person::COLLECTION,
            ["_id" => new MongoId(Yii::app()->session["userId"])],
            ['$set' => $personToElement]
        );

        return ["result"=>true, "target" => $invitationLink["targetId"]];
    }
}
