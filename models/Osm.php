<?php

use SIG, Import, Yii, Element, MongoId, Organization, Project;

class Osm
{
	const OSM_VERSION = "0.6";
	const OVERPASS_API = "https://overpass-api.de/api/interpreter";
	const OSM_API = "https://api.openstreetmap.org/api/" . self::OSM_VERSION . "/";
	const WIKIDATA_API = "https://query.wikidata.org/sparql";
	const USER_AGENT = "https://www.communecter.org/";
	const USERNAME_CONTRIBITEUR = "fanilontsoadinah@gmail.com";
	const PASSWORD_CONTRIBUTEUR = "d!d!nah05";
	const MADATLAS_COMMENT_SEND = "CARTOPARTIE #MADATLAS";
	const MADATLAS_CONTRIBUTION = "madatlasContribution";
	const TAG_SOURCE = "source";
	const MADATLAS_NAME = "MadAtlas";


	const MADATLAS_COMMENT = [
		"MADATLAS", "Madatlas", "madatlas", "madAtlas", "MadAtlas", "MADAATLAS", "Madaatlas", "madaatlas", "madaAtlas", "MadaAtlas",
		"L3Pro", "L3 Pro", "L3pro", "L3 pro", "l3Pro", "l3pro", "l3 Pro", "l3 pro", "remove hashtags", "remove hashtags", "hashtags removed", "updates at Andrainjato university campus"
	];

	const QUERRY = [
		"address", "geo", "geoPosition", "isOsmData", "name", "osmId", "osmType", "shortDescription", "tags",
		"website", '_id', "isCoData", "slug", "madatlasContribution", "comment", "wikidataId", "collection", "type", "email", "profilImageUrl"
	];

	const OSM_KEY_TAG = [
		"aeroway", "amenity", "building", "tourism", "sport", "highway", "leisure", "government"
	];

	public static function getUserMail()
	{
		if (session_status() == PHP_SESSION_NONE)
			session_start();

		$username = self::USERNAME_CONTRIBITEUR;
		if ($_SESSION != null && isset($_SESSION["osm-account"]) && $_SESSION["osm-account"] != null && isset($_SESSION["osm-account"]["user"]) &&  $_SESSION["osm-account"]["user"] != "")
			$username = $_SESSION["osm-account"]["user"];

		return $username;
	}

	public static function getUserPassword()
	{
		if (session_status() == PHP_SESSION_NONE)
			session_start();

		$password = self::PASSWORD_CONTRIBUTEUR;
		if ($_SESSION != null && isset($_SESSION["osm-account"]) && $_SESSION["osm-account"] != null && isset($_SESSION["osm-account"]["password"]) &&  $_SESSION["osm-account"]["password"] != "")
			$password = $_SESSION["osm-account"]["password"];

		return $password;
	}

	public static function closeChangeset($changesetId)
	{
		$username = self::getUserMail();
		$password = self::getUserPassword();

		$xml_data = '<?xml version="1.0" encoding="UTF-8"?>';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::OSM_API . "changeset/" . $changesetId . '/close');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

		$response = curl_exec($ch);
		curl_close($ch);

		if ($response != null)
			return ["res" => true, "result" => $response];

		return ["res" => false, "result" => ""];
	}


	public static function completeDataViaWikidata($wikidataId)
	{
		$apiUrl = self::WIKIDATA_API . "?format=json&query=";
		$query = "
		SELECT ?itemLabel ?description
		WHERE {
		wd:$wikidataId rdfs:label ?itemLabel.
		OPTIONAL { wd:$wikidataId schema:description ?description. }
		FILTER(LANG(?itemLabel) = 'fr')
		}
		";

		$encodedQuery = urlencode($query);
		$fullUrl = $apiUrl . $encodedQuery;

		$options = [
			CURLOPT_URL => $fullUrl,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => [
				'Accept: application/json',
				'User-Agent: ' . self::USER_AGENT,
			],
		];

		$ch = curl_init();
		curl_setopt_array($ch, $options);

		$response = curl_exec($ch);

		if (curl_errno($ch)) {
		}

		curl_close($ch);

		$data = json_decode($response, true);

		if (isset($data['results']['bindings'][0])) {
			$itemLabel = $data['results']['bindings'][0]['itemLabel']['value'];
			$description = isset($data['results']['bindings'][0]['description']['value']) ? $data['results']['bindings'][0]['description']['value'] : '';

			return [
				"name" => $itemLabel,
				"description" => $description
			];
		}

		return [];
	}

	public static function updateElement($type, $id, $update, $comment = "", $contextType = "", $contextId = "")
	{
		$changesetId = self::createChangeset($comment);
		$response = self::uploadChangeset($changesetId, $type, $id, $update, $contextType, $contextId);
		self::closeChangeset($changesetId);
		return $response;
	}

	public static function createElement($lat, $lon, $update, $comment = "", $contextType = "", $contextId = "")
	{
		$changesetId = self::createChangeset($comment);
		$response = self::uploadNewElementChangeset($changesetId, $lat, $lon, $update, $contextType, $contextId);
		self::closeChangeset($changesetId);
		return $response;
	}

	public static function convertData($element, $type = null,  $getComment = false, $isCity = false)
	{
		$res = array();
		$index = 0;

		if ($isCity && isset($element[0]) && (isset($element[0]["lat"]) && isset($element[0]["lon"])) || (isset($element[0]["center"]) && $element[0]["center"] != null && isset($element[0]["center"]["lat"]) && isset($element[0]["center"]["lon"]))) {
			$element = [
				"0" => $element[0]
			];
		}

		foreach ($element as $data) {
			
			if(isset($data["type"]) && $data["type"] == "area"){
				continue;
			}

			if (!$isCity || ($isCity && $index == 0 && ((isset($data["lat"]) && isset($data["lon"])) || (isset($data["center"]) && $data["center"] != null && isset($data["center"]["lat"]) && isset($data["center"]["lon"]))))) {
				$tmp = array();
				$isExisteInCo = false;
				$tags = $data["tags"];
				$name = (isset($tags["name"])) ? $tags["name"]  : "";
				$name = ($name == "" && isset($tags["name:fr"])) ? $tags["name:fr"] : $name;
				$name = ($name == "" && isset($tags["name:en"])) ? $tags["name:en"] : $name;

			
				$lat = 0;
				$lon = 0;

				if (isset($data["lat"]) && isset($data["lon"])) {
					$lat = (is_numeric($data["lat"]) ? strval($data["lat"]) : $data["lat"]);
					$lon = (is_numeric($data["lon"]) ? strval($data["lon"]) : $data["lon"]);
				}

				if (isset($data["center"]) && $data["center"] != null && isset($data["center"]["lat"]) && isset($data["center"]["lon"])) {
					$lat = (is_numeric($data["center"]["lat"]) ? strval($data["center"]["lat"]) : $data["center"]["lat"]);
					$lon = (is_numeric($data["center"]["lon"]) ? strval($data["center"]["lon"]) : $data["center"]["lon"]);
				}
				$tmpTag = [];
				foreach (self::OSM_KEY_TAG as $key) {
					if (isset($tags[$key]))
						array_push($tmpTag, $tags[$key]);
				}

				if (!$isCity && isset($data["tags"]) && $data["tags"] != null) {
					
					$allCollection = [
						Organization::COLLECTION,
						Project::COLLECTION,
					];

					$where = array(
						"name" => $name,

					);

					foreach ($allCollection as $collection) {
						$result = Element::getByWhere($collection, $where);
						if ($result != null && count($result) > 0) {
							$isExisteInCo =  true;

							foreach ($result as $elt) {
								$itIs = true;
								if (isset($elt["geo"]["latitude"]) && isset($elt["geo"]["longitude"]) && ($elt["geo"]["latitude"] != $lat || $elt["geo"]["longitude"] != $lon))
									$itIs = false;

								if ($itIs) {
									if ((!isset($elt["shortDescription"]) || (isset($elt["shortDescription"]) &&  $elt["shortDescription"] == "")) && isset($tags["description"]) && $tags["description"] != "")
										$elt["shortDescription"] = $tags["description"];

									$elt["isCoData"] = true;
									$elt["isOsmData"] = false;

									if (!isset($elt["address"]) || !isset($elt["geo"]) || !isset($elt["geoPosition"])) {

										$geoData = self::getAdressOfElementByLatAndLon($lat, $lon);

										if (isset($geoData["address"]))
											$elt["address"] = $geoData["address"];

										if (isset($geoData["geo"]))
											$elt["geo"] = $geoData["geo"];

										if (isset($geoData["geoPosition"]))
											$elt["geoPosition"] = $geoData["geoPosition"];
									}

									$elt["osmId"]	= (isset($data["id"])) ? $data["id"]  : "";
									$elt["osmType"] = (isset($data["type"])) ? $data["type"]  : "";


									if (isset($elt["tags"])) {
										foreach ($elt["tags"] as $t) {
											if (!in_array($t, $tmpTag))
												array_push($tmpTag, $t);
										}
									}
									$elt["tags"] = $tmpTag;

									$elt["website"] = (isset($tags["contact:website"])) ? $tags["contact:website"]  : "";
									$elt["website"] = (isset($tags["website"]) && $elt["website"] == "") ? $tags["website"]  : $elt["website"];


									if ((isset($tags[self::MADATLAS_CONTRIBUTION]) && $tags[self::MADATLAS_CONTRIBUTION] == "1") || (isset($tags[self::TAG_SOURCE]) && $tags[self::TAG_SOURCE] == self::MADATLAS_NAME))
										$elt[self::MADATLAS_CONTRIBUTION] = true;

									else if (!$isCity && $getComment && isset($_POST["costumSlug"]) && strpos($_POST["costumSlug"], "madaatlas") !== false && !isset($tags[self::MADATLAS_CONTRIBUTION]) && (!isset($tags[self::TAG_SOURCE]) || (isset($tags[self::TAG_SOURCE]) && $tags[self::TAG_SOURCE] != self::MADATLAS_NAME))) {
										$elt["comment"] =  self::getComment($elt["osmType"], $elt["osmId"]);
										$isMadatlas = false;
										foreach (self::MADATLAS_COMMENT as $comment) {
											if (strpos($tmp["shortDescription"], $comment) !== false) {
												$isMadatlas = true;
												break;
											}
										}
										if ($isMadatlas) {
											$elt[self::MADATLAS_CONTRIBUTION] =  true;
											self::updateElement($elt["osmType"], $elt["osmId"], [self::TAG_SOURCE => self::MADATLAS_NAME]);
										}
									}

									if (isset($tags["wikidata"]))
										$elt["wikidataId"] = $tags["wikidata"];

									$tmp = array();
									foreach (self::QUERRY as $query) {
										if (isset($elt[$query]))
											$tmp[$query] = $elt[$query];
									}

									$tmp["tagOsm"] = array();
									$tmp["osmId"]	= (isset($data["id"])) ? $data["id"]  : " ";
									foreach ($tags as $kk => $value) {
										$key = explode(":", $kk);
										$key = implode("_", $key);
										
										$tmp["tagOsm"][$key] = $value;
									}
								}
							}
						}
					}
				}

				if (!$isExisteInCo) {
					$tmp = array("isOsmData" => true);
					$tmp["osmId"] = (isset($data["id"])) ? $data["id"]  : " ";
					if (isset($data["type"]))
						$tmp["osmType"] = $data["type"];


					// $result = self::getAdressOfElementByLatAndLon($lat, $lon);

					// if (isset($result["address"]))
					// 	$tmp["address"] = $result["address"];

					// if (isset($result["geo"]))
					// 	$tmp["geo"] = $result["geo"];
					// else {
						$tmp["geo"] = [
							"@type" => "GeoCoordinates",
							"latitude" => $lat,
							"longitude" => $lon
						];
					// }

					// if (isset($result["geoPosition"]))
					// 	$tmp["geoPosition"] = $result["geoPosition"];
					// else {
						$tmp["geoPosition"] = [
							"type" => "Point",
							"float" => true,
							"coordinates" => [
								floatval($lat),
								floatval($lon),
							],
						];
					// }


					if (isset($data["tags"]) && $data["tags"] != null) {
						$tags = $data["tags"];
						$tmp["name"] = (isset($tags["name"])) ? $tags["name"]  : "";
						$tmp["name"] = ($tmp["name"] == "" && isset($tags["name:fr"])) ? $tags["name:fr"] : $tmp["name"];
						$tmp["name"] = ($tmp["name"] == "" && isset($tags["name:en"])) ? $tags["name:en"] : $tmp["name"];

						// $tmpTag = [];
						// foreach (self::OSM_KEY_TAG as $key) {
						// 	if (isset($tags[$key]))
						// 		array_push($tmpTag, $tags[$key]);
						// }

						$tmp["tags"] = $tmpTag;
						$tmp["shortDescription"] = (isset($tags["description"])) ? $tags["description"]  : "";

						$tmp["website"] = (isset($tags["contact:website"])) ? $tags["contact:website"]  : "";

						if (!$isCity && isset($tags[self::MADATLAS_CONTRIBUTION]) && $tags[self::MADATLAS_CONTRIBUTION] == "1" || (isset($tags[self::TAG_SOURCE]) && $tags[self::TAG_SOURCE] == self::MADATLAS_NAME))
							$tmp[self::MADATLAS_CONTRIBUTION] = true;

						else if (!$isCity && $getComment && isset($_POST["costumSlug"]) && strpos($_POST["costumSlug"], "madaatlas") !== false && !isset($tags[self::MADATLAS_CONTRIBUTION]) && (!isset($tags[self::TAG_SOURCE]) || (isset($tags[self::TAG_SOURCE]) && $tags[self::TAG_SOURCE] != self::MADATLAS_NAME))) {
							$tmp["comment"] =  self::getComment($tmp["osmType"], $tmp["osmId"]);
							$isMadatlas = false;
							foreach (self::MADATLAS_COMMENT as $comment) {
								if (strpos($tmp["shortDescription"], $comment) !== false) {
									$isMadatlas = true;
									break;
								}
							}
							if ($isMadatlas) {
								$tmp[self::MADATLAS_CONTRIBUTION] =  true;
								self::updateElement($tmp["osmType"], $tmp["osmId"], [self::TAG_SOURCE => self::MADATLAS_NAME]);
							}
						}

						if (!$isCity && isset($tags["wikidata"])) {
							$tmp["wikidataId"] = $tags["wikidata"];
							if ($tmp["name"] == "" || $tmp["shortDescription"] == "") {
								$wikidata = self::completeDataViaWikidata($tags["wikidata"]);
								if ($wikidata != null) {

									if ($tmp["name"] == ""  && isset($wikidata["name"])) {
										$tmp["name"] = $wikidata["name"];
										self::updateElement($tmp["osmType"], $tmp["osmId"], ["name" => $wikidata["name"]]);
									}

									if ($tmp["shortDescription"] == ""  && isset($wikidata["description"])) {
										$tmp["shortDescription"] = $wikidata["description"];
										self::updateElement($tmp["osmType"], $tmp["osmId"], ["description" => $wikidata["description"]]);
									}
								}
							}
						}

						if (!$isCity) {
							$tmp["tagOsm"] = array();
							foreach ($tags as $kk => $value) {
								// $key = $kk;
								// $explode = explode(":", $kk);
								// if (count($explode) > 1) {
								// 	$key = implode("_", $explode);
								// }
								$key = explode(":", $kk);
								$key = implode("_", $key);

								$tmp["tagOsm"][$key] = $value;
							}
						}
					}

					if (!$isCity) {
						if ($type != null) {
							if (isset($type["marker"]))
								$tmp["profilMarkerImageUrl"] = Yii::app()->getModule('map')->assetsUrl . "/" . $type["marker"];
							else
								$tmp["profilMarkerImageUrl"] = Yii::app()->getModule('map')->assetsUrl . "/" . "/images/markers/osm/default.png";
						} else
							$tmp["profilMarkerImageUrl"] = Yii::app()->getModule('map')->assetsUrl . "/" . "/images/markers/osm/default.png";//else if (isset($data["profilMarkerImageUrl"]))
							//$tmp["profilMarkerImageUrl"] = $data["profilMarkerImageUrl"];
					}

					if (!isset($data["profilMarkerImageUrl"]))
						$tmp["profilMarkerImageUrl"] = Yii::app()->getModule('map')->assetsUrl . "/" . "/images/markers/osm/default.png";
				}
				$index++;

				if (count($tmp) > 0) {
					if (isset($tmp["shortDescription"])) {
						$isMadatlas = false;
						foreach (self::MADATLAS_COMMENT as $comment) {
							if (strpos($tmp["shortDescription"], $comment) !== false) {
								$isMadatlas = true;
								break;
							}
						}
						if ($isMadatlas)
							$tmp[self::MADATLAS_CONTRIBUTION] =  true;
					}

					if (!$isCity && isset($tmp["osmId"]))
						$res[$tmp["osmId"]] = $tmp;
					else
						array_push($res, $tmp);
				}
				if ($isCity)
					break;
			}
		}
		return $res;
	}

	public static function createChangeset($comment = "")
	{
		$username = self::getUserMail();
		$password = self::getUserPassword();

		$comment = ($comment != "") ? $comment : self::MADATLAS_COMMENT_SEND;

		$xml_data = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_data .= '<osm>';
		$xml_data .= '<changeset>';
		$xml_data .= '<tag k="created_by" v="https://www.communecter.org"/>';
		$xml_data .= '<tag k="comment" v="' . $comment . '"/>';
		$xml_data .= '</changeset>';
		$xml_data .= '</osm>';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::OSM_API . 'changeset/create');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	public static function getAdressOfElementByLatAndLon($lat, $lon)
	{
		$res = [];
		$city = SIG::getCityByLatLngGeoShape($lat, $lon, null,  null);
		if (!empty($city)) {
			$newA = Import::getAddressConform($city, array());
			$newGeo = SIG::getFormatGeo($lat, $lon);
			$newGeoPosition = SIG::getFormatGeoPosition($lat, $lon);

			$res["address"] =  (empty($newA) ? null : $newA);
			$res["geo"] = (empty($newGeo) ? null : $newGeo);
			$res["geoPosition"] = (empty($newGeoPosition) ? null : $newGeoPosition);
		}
		return $res;
	}

	public static function getData($query, $type = null, $convertData = true, $isCity = false)
	{
		$element = [];
		$httpOptions = [
			"http" => [
				"method" => "GET",
				"header" => "User-Agent: " . self::USER_AGENT
			]
		];

		$urlToSend = self::OVERPASS_API .  "?data=" . urlencode($query);

		$streamContext = stream_context_create($httpOptions);
		$res = json_decode(file_get_contents($urlToSend, false, $streamContext), true);
		if ($res != null && !empty($res) && isset($res["elements"])) {
			if ($convertData)
				$element = self::convertData($res["elements"], $type, true, $isCity);
			else
				$element = $res["elements"];
		}
		return $element;
	}

	public static function getChangeset($type, $id)
	{
		$url = self::OSM_API . $type . "/" . $id . ".json";
		$httpOptions = [
			"http" => [
				"method" => "GET",
				"header" => "User-Agent: " . self::USER_AGENT
			]
		];

		$streamContext = stream_context_create($httpOptions);
		$res = json_decode(file_get_contents($url, false, $streamContext), true);
		if ($res != null && !empty($res) && isset($res["elements"]) && $res["elements"] != null && isset($res["elements"][0]) && $res["elements"][0] != null && isset($res["elements"][0]["changeset"])) {
			return $res["elements"][0]["changeset"];
		}
		return null;
	}

	public static function getComment($type, $id)
	{
		$changesetId = self::getChangeset($type, $id);
		if ($changesetId != null) {
			$url = self::OSM_API . "changeset/" . $changesetId . ".json";
			$httpOptions = [
				"http" => [
					"method" => "GET",
					"header" => "User-Agent: " . self::USER_AGENT
				]
			];

			$streamContext = stream_context_create($httpOptions);
			$res = json_decode(file_get_contents($url, false, $streamContext), true);
			if ($res != null && !empty($res) && isset($res["elements"]) && $res["elements"] != null && isset($res["elements"][0]) && $res["elements"][0] != null && isset($res["elements"][0]["tags"]) && $res["elements"][0]["tags"] != null && isset($res["elements"][0]["tags"]["comment"])) {
				return $res["elements"][0]["tags"]["comment"];
			}
		}
		return "";
	}

	public static function getInfoElementOsm($type, $id)
	{
		$username = self::getUserMail();
		$password = self::getUserPassword();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::OSM_API . $type . '/' . $id . '.json');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

		$response = curl_exec($ch);
		curl_close($ch);
		if ($response != null) {
			$response = json_decode($response, true);
		}
		if ($response != null && isset($response["elements"]) && $response["elements"] != null && isset($response["elements"][0]))
			return $response["elements"][0];
		return $response;
	}

	public static function getValueTagChange($tags, $update)
	{
		$str = "";
		$indexSet = [];
		$tmpUpdate = [];
		$asTagMadatlas = false;
		foreach ($update as $up) {
			if ($up != null && isset($up["value"]) && isset($up["key"]))
				$tmpUpdate[$up["key"]] = $up["value"];
		}

		foreach ($tags as $key => $value) {
			if ($value != "") {
				if ($key != self::MADATLAS_CONTRIBUTION  && $key != "amenity,building" && ($key != self::TAG_SOURCE || ($key == self::TAG_SOURCE && $value != self::MADATLAS_NAME))) {
					if (isset($tmpUpdate[$key]) && $tmpUpdate[$key] != "" && $tmpUpdate[$key] != $value)
						$str .= '<tag k="' . $key . '" v="' . htmlspecialchars($tmpUpdate[$key]) . '"/>';
					else
						$str .= '<tag k="' . $key . '" v="' . htmlspecialchars($value) . '"/>';
				}
			}

			$indexSet[$key] = $key;
		}

		foreach ($tmpUpdate as $key => $value) {
			if ($value != "" && !isset($indexSet[$key]) && $value != "")
				$str .= '<tag k="' . $key . '" v="' . htmlspecialchars($value) . '"/>';
		}

		//$str .= '<tag k="'.self::TAG_SOURCE.'" v="' . htmlspecialchars(self::MADATLAS_NAME) . '"/>';

		return $str;
	}

	public static function setAccountUser($email = "", $password = "")
	{
		if (session_status() == PHP_SESSION_NONE)
			session_start();
		if ($email != "" && $password != "") {
			$_SESSION["osm-account"] = [
				"user" => $email,
				"password" => $password,
			];
		} else {
			$email = self::USERNAME_CONTRIBITEUR;
			$password = self::PASSWORD_CONTRIBUTEUR;

			$_SESSION["osm-account"] = [
				"user" => $email,
				"password" => $password,
			];
		}

		return true;
	}

	public static function uploadChangeset($changesetId, $type, $id, $update, $contextType = "", $contextId = "")
	{
		$username = self::getUserMail();
		$password = self::getUserPassword();

		$element = self::getInfoElementOsm($type, $id);
		$xml_data = "";
		if ($element != null && isset($element["type"]) && $element["type"] == "node" && isset($element["id"]) && isset($element["lat"]) && isset($element["lon"]) && isset($element["version"]) && isset($element["tags"]) && $element["tags"] != null) {
			$xml_data = '<?xml version="1.0" encoding="UTF-8"?>';
			$xml_data .= '<osmChange version="' . self::OSM_VERSION . '">';
			$xml_data .= '<modify>';
			$xml_data .= '<' . $element["type"] . ' id="' . $element["id"] . '" visible="true" version="' . $element["version"] . '" changeset="' . $changesetId . '"  lat="' . $element["lat"] . '" lon="' . $element["lon"] . '">';

			$xml_data .= self::getValueTagChange($element["tags"], $update);

			$xml_data .= '</' . $element["type"] . '>';
			$xml_data .= '</modify>';
			$xml_data .= '</osmChange>';
		} else if ($element != null && isset($element["type"]) && ($element["type"] == "way"  || $element["type"] == "relation") && isset($element["id"]) && isset($element["nodes"]) && $element["nodes"] != null && isset($element["version"]) && isset($element["tags"]) && $element["tags"] != null) {
			$xml_data = '<?xml version="1.0" encoding="UTF-8"?>';
			$xml_data .= '<osmChange version="' . self::OSM_VERSION . '">';
			$xml_data .= '<modify>';
			$xml_data .= '<' . $element["type"] . ' id="' . $element["id"] . '" visible="true" version="' . $element["version"] . '" changeset="' . $changesetId . '">';

			foreach ($element["nodes"] as  $nodeId) {
				$xml_data .= '<nd ref="' . $nodeId . '"/>';
			}

			$xml_data .= self::getValueTagChange($element["tags"], $update);

			$xml_data .= '</' . $element["type"] . '>';
			$xml_data .= '</modify>';
			$xml_data .= '</osmChange>';
		}

		if ($xml_data != "") {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, self::OSM_API . "changeset/" . $changesetId . '/upload');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

			$response = curl_exec($ch);
			curl_close($ch);

			if ($response != null) {
				// $response = simplexml_load_string($response);
				// if($contextType != "" && $contextId != "") {
				// 	$data_json = [];

				// 	foreach($element as $key => $val) {
				// 		$data_json[$key] = $val;
				// 	}

				// 	if(!isset($data_json["tags"]))
				// 		$data_json["tags"] = [];


				// 	foreach($update as $up) {
				// 		if($up != null && isset($up["value"]) && isset($up["key"])) 
				// 			$data_json["tags"][$up["key"]] = $up["value"];
				// 	}

				// 	$element = self::convertData([$data_json]);

				// 	$update = self::addNewElementInParent($contextId, $contextType, $element);

				// 	return ["res" => true, "result" => $response, "update" => $update, "data_json" => $data_json, "changesetId" => $changesetId, "element" => $element];
				// }



				return ["res" => true, "result" => $response, "changesetId" => $changesetId];
			}
		}
		return ["res" => false, "result" => $element];
	}

	public static function uploadNewElementChangeset($changesetId, $lat, $lon, $update, $contextType = "", $contextId = "")
	{
		$username = self::getUserMail();
		$password = self::getUserPassword();

		$data_json = [
			"lat" => $lat,
			"lon" => $lon,
			"type" => "node",
			"tags" => [],
		];

		$xml_data = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_data .= '<osmChange version="' . self::OSM_VERSION . '">';
		$xml_data .= '<create>';
		$xml_data .= '<node version="1" id="-1" changeset="' . $changesetId . '" timestamp="' . date("Y-m-d\TH:i:s\Z") . '"  user="1" uid="1" lat="' . $lat . '" lon="' . $lon . '">';

		foreach ($update as $value) {
			if ($value != null && isset($value["key"]) && isset($value["value"]) && $value["value"] != "") {
				$data_json["tags"][$value["key"]] = $value["value"];
				$xml_data .= '<tag k="' . $value["key"] . '" v="' . htmlspecialchars($value["value"]) . '"/>';
			}
		}

		//$xml_data .= '<tag k="'.self::TAG_SOURCE.'" v="' . htmlspecialchars(self::MADATLAS_NAME) . '"/>';
		$xml_data .= '</node>';
		$xml_data .= '</create>';
		$xml_data .= '</osmChange>';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::OSM_API . "changeset/" . $changesetId . '/upload');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

		$response = curl_exec($ch);
		curl_close($ch);

		if ($response != null) {
			$response = simplexml_load_string($response);
			if ($response != null && $contextType != "" && $contextId != "" && isset($response->node) &&  $response->node != null && isset($response->node["new_id"]) &&  $response->node["new_id"] != null) {
				$data_json["id"] =  (string) $response->node["new_id"];
				$element = self::convertData([$data_json]);

				$update = self::addNewElementInParent($contextId, $contextType, $element);

				return ["res" => true, "result" => $response, "update" => $update, "data_json" => $data_json, "changesetId" => $changesetId, "elementId" => (string) $response->node["new_id"], "element" => $element];
			}
			return ["res" => true, "result" => $response, "changesetId" => $changesetId];
		}


		return ["res" => false, "result" => $response];
	}

	private static function addNewElementInParent($contextId, $contextType, $element)
	{
		$where = array(
			"_id" => new MongoId($contextId)
		);

		$update = "tsy nety mintsy";

		foreach ($element as $osmId => $value) {
			$action = array(
				'$set' => array(
					"costum.osm.elts.element." . $osmId => $value
				),
			);

			$update = PHDB::update($contextType, $where, $action);
		}

		return $update;
	}
}
