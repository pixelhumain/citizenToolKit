<?php

use Authorisation,Person, Yii, Element, Log;

class IpSpam {
	const COLLECTION = "ipSpam";
	const CONTROLLER = "ipSpam";
	public static $dataBinding = array(
		"ip" => array("name" => "ip", "rules" => array("required")),
		"city" => array("name" => "city"),
		"region" => array("name" => "region"),
		"country" => array("name" => "country"),
		"loc" => array("name" => "loc"),
		"org" => array("name" => "org"),
		"postal" => array("name" => "postal"),
		"timezone" => array("name" => "timezone"),
	);
	
	public static function addElementToSpam($idElement, $typeElement) {
		if ( !self::isSuperAdmin()) 
            return self::setAccessDined();

		$where = array("_id" => new MongoId($idElement));
			
		$action = [
			'$set' =>  array(
				'isSpam' => true,
				'userSpamDetector' => array(
					"id" => Yii::app()->session["userId"],
					"name" => self::getUserFindSpam()
				),
				"dateSpamDetector" => new MongoDate(time())
			)
		];
						
		PHDB::update($typeElement, $where, $action);
		Log::save(array("userId" => Yii::app()->session["userId"], "browser" => @$_SERVER["HTTP_USER_AGENT"], "ipAddress" => @$_SERVER["REMOTE_ADDR"], "created" => new MongoDate(time()), "action" => "addToSpamElement", "params" => array("id" => $_POST["id"], "type" => $_POST["type"])));
		
		$res=array("result"=>true, "msg"=> Yii::t("common","Information updated"), "user" => Element::getByTypeAndId(Citoyen::COLLECTION, Yii::app()->session["userId"]));

		return $res;
	}

	public static function getAll() {
		$allAggregate= array(
            array( '$group' => array(
                    '_id' => array(
                        'id' => '$_id',
                        'ip' => '$ip'   
                        , 'city' => '$city'
                        , 'region' => '$region'
                        , 'country' => '$country'
                        , 'loc' => '$loc'
                    ),
                    'count' => array ('$sum' => 1),
                )
            ),
            array( '$project' => array("id" => '$_id.id',"ip" => '$_id.ip', "count" => '$count', "city" => '$_id.city', "region" => '$_id.region', "country" => '$_id.country', "loc" => '$_id.loc') ),
        );
        return self::getAggregate($allAggregate);
	}

	public static function getAggregate($aggregate) {
		return PHDB::aggregate(self::COLLECTION, $aggregate);
	}
	
	public static function getById($id) { 
	  	$ipSpam = PHDB::findOneById( self::COLLECTION ,$id );
	  	return $ipSpam;
	}


	public static function getListBy($where){
		$ipSpams = PHDB::findAndSort( self::COLLECTION , $where, array("updated"=>-1));
	  	return $ipSpams;
	}

	public static function getInfoOfIp($ip) {
		$result = array();
		$url = "http://ipinfo.io/{$ip}";

		$data = file_get_contents($url);
		$data = json_decode($data, true);

		if($data != null && !empty($data) && isset($data["ip"])){
			if(isset($data["readme"]))
				unset($data["readme"]);
				
			$result = $data;
		}

		return $result;
	}

	private static function getUserFindSpam(){
        $name = "";
        $declaredBy = Element::getByTypeAndId(Citoyen::COLLECTION, Yii::app()->session["userId"]);
        if(!empty($declaredBy) && isset($declaredBy["name"]))
            $name = $declaredBy["name"];
        return $name;
    }

	public static function getWhere($where = array()){
		return PHDB::find(self::COLLECTION, $where);
	}

	public static function insert($ipSpam){
		
        try {
        	$valid = DataValidator::validate( self::CONTROLLER, json_decode (json_encode ($ipSpam), true), null );
        } catch (CTKException $e) {
        	$valid = array("result"=>false, "msg" => $e->getMessage());
        }
        if( $valid["result"]) 
        {

			Yii::app()->mongodb->selectCollection(self::COLLECTION)->insert($ipSpam);

			return array("result"=>true, "msg"=>"Ajout effectuer", "backup"=>$ipSpam);
		}else 
            return array( "result" => false, "error"=>"400",
                          "msg" => Yii::t("common","Something went really bad : ".$valid['msg']) );

	}

	public static function insertByIp($ip) {
		if ( !self::isSuperAdmin()) 
            return self::setAccessDined();

		$data = self::getInfoOfIp($ip);

		if($data !== null && !empty($data) && isset($data["ip"])){
			self::insert($data);	
			return array("result"=>true, "msg"=>Yii::t("common","Information updated"), "data"=>$data);
		}
		return array("result"=>false, "msg"=>"Erreur lors de l'ajout de l'élément", "data" => $data);
	} 
	
	public static function isBanned($ip = "") {
		$isBanned = false;
		$result = PHDB::findOne(self::COLLECTION, [
			"ip" => $ip
		]);

		if(!empty($result) && isset($result["ip"]) && $result["ip"] == $ip)
			$isBanned = true;


		return $isBanned;
	}

	private static function isSuperAdmin() {
		if ( ! Person::logguedAndValid() ||  (!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) 
			return false;

		return true;
	}

	public static function noSpamElement($idElement, $typeElement) {
		if ( !self::isSuperAdmin()) 
            return self::setAccessDined();

		$creatorId = $idElement;

        if($typeElement != Citoyen::COLLECTION){
            $creator = Element::getCreatorElement($typeElement, $idElement);

            foreach($creator as $idCreator => $creatorElement){
                $creatorId = $idCreator; 
            }
        }

		$aggregate= array(
            array( '$group' => array(
                    '_id' => array(
                        'ipAddress' => '$ipAddress'
                    ),
                    'count' => array ('$sum' => 1),
                    'minDate' => array ( '$min' => '$created' ),
                    'maxDate' => array ( '$max' => '$created' )
                )
            ),
            array( '$match' => array("_id.userId" => $creatorId) ),
            array( '$project' => array( "count" => '$count', "ipAddress" => '$_id.ipAddress') ),
        );

        $resultLog = PHDB::aggregate(Log::COLLECTION, $aggregate);

        if(isset($resultLog["result"]) && !empty($resultLog["result"]))
            $resultLog = $resultLog["result"];

        foreach($resultLog as $index => $log){
            if(isset($log["ipAddress"]) && $log["ipAddress"] != null && $log["ipAddress"] != "" && $log["ipAddress"] != "127.0.0.1" && $log["ipAddress"] != "localhost" ){
                PHDB::remove(self::COLLECTION, array("ip" => $log["ipAddress"]));
            }
        }

		$where = array("_id" => new MongoId($idElement));
			
        $action = [
            '$unset' =>  array(
                'isSpam' => "",
                'userSpamDetector' => "",
                "dateSpamDetector" => ""
            )
        ];
						
		$result = PHDB::update($typeElement, $where, $action);
        return array("result"=>true, "msg"=> Yii::t("common","Information updated"));
	}

	private static function  setAccessDined() {
		return array("result" => false, "msg" => Yii::t("common", "Access denied"));
	}

	public static function updateByWhere($where, $action) {
		if ( !self::isSuperAdmin()) 
            return self::setAccessDined();
		
		PHDB::update(self::COLLECTION, $where, $action);

		return array("result"=>true, "msg"=>Yii::t("common","Information updated"));
	}
}
?>