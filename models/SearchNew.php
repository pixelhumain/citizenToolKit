<?php
    
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
    
    class SearchNew {
        
        public static function addQuery($query, $request) {
            
            // if(!empty($request)){
            // 	if(empty($query) || count($query) === 0)
            // 		$query = $request;
            // 	else if(count($query) === 1 ){
            // 		// Rest::json($query);
            // 		// echo "<br/>";Rest::json($request);echo "<br/><br/>";
            // 		$query = array('$and' => array($query,$request));
            // 	}
            // 	else
            // 		$query['$and'][] = $request;
            // }
            
            // if(!empty($query['$and'])){
            $query['$and'][] = $request;
            // } else {
            // 	$query['$and'] = $request;
            // }
            return $query;
        }
        
        public static function startQuery() {
            $query['$and'] = array();
            return $query;
        }
        
        public static function checkFields($fields) {
            if (empty($fields))
                $fields = array("name", "collection");
            $forbidenFields = array("preferences");
            foreach ($forbidenFields as $v) {
                if (($key = array_search($v, $fields)) !== false)
                    unset($fields[$key]);
            }
            // foreach ($searchType as $key => $value) {
            // 	$fields = $value ;
            // }
            
            return $fields;
        }
        // Equivalent globalAutocomplete ci-dessous sauf qu'il sort tous les elts privés
        // BIEN sur après plusieurs vérifications des droits
        public static function searchAdmin($post, $type = null, $id = null, $canSee = null) {
            $results = array("results" => []);
            $fields = (!empty($_POST["fields"]) ? $_POST["fields"] : array());
            if (Authorisation::isInterfaceAdmin() ||
                (!empty($id) && !empty($type)
                    && (Authorisation::isElementAdmin($id, $type, Yii::app()->session["userId"])
                        || $canSee == true && Authorisation::canSee($type, $id)))) {
                $results = self::querySearchAll($post, $fields, true);
                $results = Utils::filterSearchAdminDataForActivityPubFlux($type, $id, $post, $results);
            } else if (Costum::isSameFunction("authorizedPanelAdmin")) {
                $opt = (!empty($id) && !empty($type)) ? array("id" => $id, "type" => $type) : [];
                if (Costum::sameFunction("authorizedPanelAdmin", $opt) || (!empty($id) && !empty($type) && ($canSee == true && Authorisation::canSee($type, $id))))
                    $results = self::querySearchAll($post, $fields, true);
            }
            return $results;
        }


        public static function globalAutoComplete($post) {
            // rajout necessaire pour activitypub :  fromActivitPub,attributedTo,objectId
            $fields = array(
                "name", "collection", "slug", "fromActivityPub", "attributedTo", "objectId", "created", "updated", "profilThumbImageUrl",
                "profilImageUrl","profilMediumImageUrl", "shortDescription", "parent", "tags", "type", "section", "category", "startDate", "endDate", "openingHours",
                "address","addresses", "scope", "geo", "geoPosition", "links", "description", "isSpam", "userSpamDetector", "dateSpamDetector"
            );
            if (Costum::isSameFunction("addSearchFields")) {
                $newFields = Costum::sameFunction("addSearchFields");
                $fields = array_merge($fields, $newFields);
            }
            //TODO : Faire une fonction qui vérifie les fields demandés
            if (isset($post["fieldShow"])) {
                $fields = array_merge(
                    [   "slug","fromActivityPub", "attributedTo", "objectId", "updated", "category",
                        "profilThumbImageUrl", "profilImageUrl","profilMediumImageUrl", "parent", "section", "links",
                        "openingHours", "scope", "geo", "geoPosition", "isSpam", "userSpamDetector", "dateSpamDetector"
                    ], $post["fieldShow"]);
            } else if (isset($post["fields"]))
                $fields = array_merge($fields, $post["fields"]);
            $fields = self::checkFields($fields);
            $results = self::querySearchAll($post, $fields);
            $results = Utils::filterResultsForActivityPubFlux($results);
            return $results;
        }
        
        public static function querySearchAll($post, $fields, $admin = null) {
            if (isset($post['searchType']) && !empty($post['searchType'])) {
                //$costum = CacheHelper::getCostum();
                $searchParams = array(
                    "options"        => (!empty($post['options'])) ? $post['options'] : null,
                    "ranges"         => isset($post['ranges']) ? $post['ranges'] : null,
                    "startDateUTC"   => isset($post['startDateUTC']) ? $post['startDateUTC'] : null,
                    "onlyCount"      => isset($post['onlyCount']) ? true : false,
                    "countType"      => isset($post['countType']) ? $post['countType'] : null,
                    "indexMin"       => isset($post['indexMin']) ? $post['indexMin'] : 0,
                    "indexStep"      => isset($post['indexStep']) ? $post['indexStep'] : 30,
                    "initType"       => isset($post['initType']) ? $post['initType'] : null,
                    "countResult"    => isset($post["count"]) ? true : false,
                    "searchType"     => (is_string($post['searchType'])) ? [$post['searchType']] : $post['searchType'],
                    "searchTypeOrga" => [],
                    "fields"         => $fields,
                    "sortBy"         => array()
                );
                
                if (isset($post["sortBy"]) && !empty($post["sortBy"]) && Api::isAssociativeArray($post["sortBy"])) {
                    $key = array_keys($post["sortBy"])[0];
                    $value = array_values($post["sortBy"])[0];
                    $searchParams["sortBy"][$key] = (int)$value;
                } else {
                    $searchParams["sortBy"] = (isset($post["sortBy"]) && !empty($post["sortBy"])) ? array($post["sortBy"][0] => 1) : array("updated" => -1);
                }
                
                if (isset($post["sort"]) && !empty($post["sort"])) {
                    $searchParams["sortBy"] = $post["sort"];
                    foreach ($searchParams["sortBy"] as $key => $value) {
                        $searchParams["sortBy"][$key] = (int)$value;
                    }
                }
                if(isset($post["doublon"]) && $post["doublon"]){
                    $searchParams["doublon"] = true;
                }
                if(isset($post["openingHours"]) && $post["openingHours"]){
                    $searchParams["openingHours"] = true;
                }
                foreach ($searchParams['searchType'] as $v) {
                    if (in_array($v, [
                        Organization::TYPE_NGO, Organization::TYPE_BUSINESS, Organization::TYPE_GROUP, Organization::TYPE_GOV, Organization::TYPE_COOP
                    ])) {
                        array_push($searchParams["searchTypeOrga"], $v);
                        
                        if (!in_array(Organization::COLLECTION, $searchParams["searchType"]))
                            array_push($searchParams["searchType"], Organization::COLLECTION);
                        
                    }
                }
                
                //*********************************  DEFINE GLOBAL QUERY   ******************************************
                $queries = self::getQueries($post, $admin);
                $results["results"] = array();
                if (!$searchParams["onlyCount"])
                    $results["results"] = self::getResults($searchParams, $queries);
                if ($searchParams["countResult"] && !empty($searchParams['countType']))
                    $results["count"] = self::countResults($searchParams, $queries);
                if(isset($searchParams["doublon"]) && $searchParams["doublon"]){                    
                    $results["count"] = array();
                    $results["count"][$searchParams['searchType'][0]] = count($results["results"]);
                }
                /**/
                if ((isset($post["activeContour"]) && $post["activeContour"] == true)) {
                    if (isset($post['locality']) && gettype($post['locality']) == "array") {
                        $zones = array();
                        foreach ($post["locality"] as $locality) {
                            array_push($zones, new MongoId($locality['id']));
                        }
                        $results["zones"] = PHDB::find(Zone::COLLECTION, array("_id" => array('$in' => $zones)), ["name", "geoShape"]);
                    } else {
                        $ids = array_column(array_values($results["results"]), "_id");
                        $levels = PHDB::distinct("organizations", "address.level3", array("_id" => array('$in' => $ids)));
                        $ids = array_map(function ($level) {
                            if ($level != "") {
                                return new MongoId($level);
                            }
                        }, $levels);
                        $results["zones"] = PHDB::find(Zone::COLLECTION, array("_id" => array('$in' => $ids), "level" => "3"), ["name", "geoShape"]);
                    }
                }
                
            } else
                $results = array("results" => [], "count" => [], "msg" => "Error because of attr searchType sent for search");
            return $results;
        }
        
        /**
         * searchText
         *
         * @param string $text
         * @param array $query
         *
         * @return array $query
         */
        public static function searchText($text, $query, $post = null) {
            if (!empty($text)) {
                $textRegExp = self::accentToRegex($text);
                $orArray = array(
                    array("name" => new MongoRegex("/.*{$textRegExp}.*/i")),
                    array("title" => new MongoRegex("/.*{$textRegExp}.*/i")),
                    array("slug" => new MongoRegex("/.*{$textRegExp}.*/i")),
                    array("sigle" => new MongoRegex("/.*{$textRegExp}.*/i")),
                    array("shortDescription" => new MongoRegex("/.*{$textRegExp}.*/i"))//,
                    //array( "description" => new MongoRegex("/.*{$textRegExp}.*/i"))
                );
                
                if (isset($post["textPath"]) && !empty($post["textPath"])) {
                    $orArray[] = array($post["textPath"] => new MongoRegex("/.*{$textRegExp}.*/i"));
                }
                
                if (isset($post["searchBy"]) && !empty($post["searchBy"])) {
                    $arraySearch = [];
                    if(is_array($post["searchBy"])){ 
                        foreach($post["searchBy"] as $searchBy){
                            array_push($arraySearch,[$searchBy => (new MongoRegex("/.*{$textRegExp}.*/i"))]);
                        }
                    }else{
                        $searchBy = $post["searchBy"]??"";
                        if($searchBy == "ALL"){
                            $arraySearch = array(
                                array("name" => new MongoRegex("/.*{$textRegExp}.*/i")),
                                array("title" => new MongoRegex("/.*{$textRegExp}.*/i")),
                                array("slug" => new MongoRegex("/.*{$textRegExp}.*/i")),
                                array("tags" => new MongoRegex("/.*{$textRegExp}.*/i")),
                                array("source.keys" => new MongoRegex("/.*{$textRegExp}.*/i")),
                                array("sigle" => new MongoRegex("/.*{$textRegExp}.*/i")),
                                //array("shortDescription" => new MongoRegex("/.*{$textRegExp}.*/i"))//,
                                array( "description" => new MongoRegex("/.*{$textRegExp}.*/i"))
                            );
                        }else{
                            $searchBy = explode(",", $post["searchBy"]);
                            foreach($searchBy as $sBy){
                                array_push($arraySearch,[$sBy => (new MongoRegex("/.*{$textRegExp}.*/i"))]);
                            }
                        }
                    } 
                    $orArray = $arraySearch;
                    //var_dump($orArray);exit();
                }
                
                if (Costum::isSameFunction("addGlobalSearchFields")) {
                    $newFields = Costum::sameFunction("addGlobalSearchFields", $textRegExp);
                    $newFields = array($newFields);
                    $orArray = array_merge($orArray, $newFields);
                }
                
                $explodeSearchRegExp = explode("[ --']", $textRegExp);
                if (count($explodeSearchRegExp) > 1) {
                    foreach ($explodeSearchRegExp as $data) {
                        $orArray[] = array("name" => new MongoRegex("/.*{$data}.*/i"));
                    }
                }
                if (isset($post["mapping"]) && !empty($post["mapping"])) {
                    foreach ($post["mapping"] as $key => $value) {
                        if (!empty($value) &&
                            !empty($value["path"]) &&
                            empty($value['query']) ||
                            @$value['query'] == "searchString") {
                            $orArray[] = array($value["path"] => new MongoRegex("/.*{$textRegExp}.*/i"));
                            $explodeSearchRegExp = explode("[ --']", $textRegExp);
                            if (count($explodeSearchRegExp) > 1) {
                                foreach ($explodeSearchRegExp as $data) {
                                    $orArray[] = array($value["path"] => new MongoRegex("/.*{$data}.*/i"));
                                }
                            }
                        }
                        if ($key == "custom") {
                            $orArray = [];
                            $orArray[] = array($value["path"] => new MongoRegex("/.*{$textRegExp}.*/i"));
                        }
                    }
                }
                
                $query = self::addQuery($query, array('$or' => $orArray));
            }
            
            return $query;
        }
        
        public static function getResults($searchParams, $queries) {
            $allResults = array();
            foreach ($searchParams["searchType"] as $type) {
                if (!in_array($type, $searchParams["searchTypeOrga"])) {
                    $methodName = "searchIn" . ucfirst($type);

                    if(isset($searchParams["doublon"]) && $searchParams["doublon"]){
                        // if (isset($searchParams["ranges"])) {
                        //     $searchParams["indexMin"] = $searchParams["ranges"][$type]["indexMin"];
                        //     $searchParams["indexStep"] = $searchParams["ranges"][$type]["indexMax"] - $searchParams["ranges"][$type]["indexMin"];
                        // }
                        $finalQuery = (isset($queries[$type])) ? $queries[$type] : $queries["global"];
                        $fields = array(
                            "_id" => 1,
                            "name" => 1
                        );
                        $groups = array(
                            "_id" => '$name',
                            'documentIds' => array('$addToSet' => '$_id'),
                            'count'  => array('$sum' => 1),
                            "name" =>  array('$first' => 'name'),
                            "slug" =>  array('$first' => '$slug'),
                            "links" =>  array('$first' => '$links'),
                            "collection" =>  array('$first' => '$collection'),
                        );
                        foreach($searchParams["fields"] as $field){
                            $fields[$field] = '$allDocuments.'.$field;
                          //  $groups[$field] = array('$first' => "$".$field);
                        }
                        $aggregate= array(
                            array( '$match' => $finalQuery),   
                            array( '$group' => $groups ),                             
                            array( '$match' => array("count" => array('$gt' => 1))),
                            array(
                                '$lookup' => array(
                                    'from' => $type, 
                                    'localField' => 'documentIds', 
                                    'foreignField' => '_id', 
                                    'as' => 'allDocuments' 
                                )
                            ),
                            array(
                                '$unwind' => '$allDocuments'
                            ),   
                            array(
                                '$addFields' => array(
                                    '_id' => '$allDocuments._id'
                                )
                            ),
                            array(
                                '$project' => $fields
                            ),
                            array(
                                '$sort' => array(
                                    'name' => 1 
                                )
                            )
                        );
                        $resultsAggregate = PHDB::aggregate($type,$aggregate);
                        $results = [];
                        foreach($resultsAggregate["result"] as $va){
                            $results[(String) $va["_id"]] = $va;
                        }
                    }else if (method_exists("SearchNew", $methodName)) {
                        $results = self::$methodName($searchParams, $queries);
                    } else {
                        if (isset($searchParams["ranges"])) {
                            $searchParams["indexMin"] = $searchParams["ranges"][$type]["indexMin"];
                            $searchParams["indexStep"] = $searchParams["ranges"][$type]["indexMax"] - $searchParams["ranges"][$type]["indexMin"];
                        }
                        $finalQuery = (isset($queries[$type])) ? $queries[$type] : $queries["global"];
                        //echo json_encode($finalQuery);exit;
                        $results = PHDB::findAndFieldsAndSortAndLimitAndIndex($type, $finalQuery, $searchParams["fields"], $searchParams["sortBy"], $searchParams["indexStep"], $searchParams["indexMin"]);
                        //var_dump($results);exit;
                        if ($type == Event::COLLECTION && !empty($results) && empty($searchParams["fields"])) {
                            foreach ($results as $keyEv => $event) {
                                $results[$keyEv] = Event::getSimpleEventById($keyEv, $event);
                            }
                        }
                        if ($type == News::COLLECTION && !empty($results)) {
                            foreach ($results as $keyEv => $event) {
                                $results[$keyEv] = News::getById($keyEv, $event);
                            }
                        }
                    }
                    $convertMethodName = "convertDataFor" . ucfirst($type);
                    if (Costum::isSameFunction($convertMethodName)) {
                        $results = Costum::sameFunction($convertMethodName, $results);
                    } else if (method_exists("SearchNew", $convertMethodName)) {
                        $results = self::$convertMethodName($results);
                    }
                    if (!empty($results))
                        $allResults = array_merge($allResults, $results);
                }
            }
            return $allResults;
        }
        
        public static function searchInOrganizations($searchParams, $queries) {
            $allOrga = array();
            if (!empty($searchParams["searchTypeOrga"])) {
                // Cas où l'on recherche avec des multi collection (ranges), dans ce cas on boucle sur les type orga (NGO, Group, etc)
                // Qui sont considéré comme des collections dan le moteur de filtres
                if (isset($searchParams["ranges"])) {
                    foreach ($searchParams["searchTypeOrga"] as $v) {
                        if (isset($searchParams["ranges"])) {
                            $searchParams["indexMin"] = $searchParams["ranges"][$v]["indexMin"];
                            $searchParams["indexStep"] = $searchParams["ranges"][$v]["indexMax"] - $searchParams["ranges"][$v]["indexMin"];
                        }
                        $queryOrga = self::addQuery($queries["global"], array("type" => $v));
                        $resMultiOrga = PHDB::findAndFieldsAndSortAndLimitAndIndex(Organization::COLLECTION, $queryOrga, $searchParams["fields"], $searchParams["sortBy"], $searchParams["indexStep"], $searchParams["indexMin"]);
                        if (!empty($resMultiOrga))
                            $allOrga = array_merge($allOrga, $resMultiOrga);
                    }
                } else {
                    // Cas où l'on recherche par type d'organisations
                    $queryTypeOr = [];
                    foreach ($searchParams["searchTypeOrga"] as $v) {
                        $queryTypeOr[] = array("type" => $v);
                    }
                    $queryOrga = self::addQuery($queries["global"], array('$or' => $queryTypeOr));
                    $allOrga = PHDB::findAndFieldsAndSortAndLimitAndIndex(Organization::COLLECTION, $queryOrga, $searchParams["fields"], $searchParams["sortBy"], $searchParams["indexStep"], $searchParams["indexMin"]);
                }
            } else {
                $allOrga = PHDB::findAndFieldsAndSortAndLimitAndIndex(Organization::COLLECTION, $queries["global"], $searchParams["fields"], $searchParams["sortBy"], $searchParams["indexStep"], $searchParams["indexMin"]);
            }
            return $allOrga;
        }

        public static function searchInSpam($searchParams, $queries){
            $types = array(
                "organisation" => Organization::COLLECTION,
                "citoyens" => Citoyen::COLLECTION,
                "poi" => Poi::COLLECTION,
                "project" => Project::COLLECTION
            );
            
            $allSpam=array();
            foreach($types as $collection){
                $resMultiSpam = PHDB::findAndFieldsAndSortAndLimitAndIndex($collection, $queries["global"], $searchParams["fields"], $searchParams["sortBy"], $searchParams["indexStep"], $searchParams["indexMin"]);
                if(!empty($resMultiSpam))
                    $allSpam = array_merge($allSpam, $resMultiSpam);	
            }
                    
            return $allSpam;
        }
        
        /**
         * searchAnswers
         *
         * @param array $filters
         * @param array $query
         *
         * @return array $query
         */
        public static function searchInAnswers($searchParams, $queries) {
            //Rest::json($query); exit;
            if (Costum::isSameFunction("searchAnswers")) {
                $params = array(
                    "query"     => $queries["global"],
                    "fields"    => $searchParams["fields"],
                    "indexStep" => $searchParams["indexStep"],
                    "indexMin"  => $searchParams["indexMin"],
                    "sortBy"    => $searchParams["sortBy"]
                );
                $allAnswers = Costum::sameFunction("searchAnswers", $params);
            } else {
                $allAnswers = PHDB::findAndFieldsAndSortAndLimitAndIndex(Answer::COLLECTION, $queries["global"], $searchParams["fields"], $searchParams["sortBy"], $searchParams["indexStep"], $searchParams["indexMin"]);
            }
            return $allAnswers;
        }
        
        /**
         * searchTags
         *
         * @param array $searchTags
         * @param string $verb
         * @param array $query
         * @param string $tagsPath
         *
         * @return array $query
         */
        public static function searchTags($searchTags, $verb = '$in', $query = null, $tagsPath = 'tags') {
            if (!empty($searchTags)) {
                $tmpTags = array();
                if (is_array(@$searchTags)) {
                    foreach ($searchTags as $value) {
                        if (trim($value) != "") {
                            $value = rtrim(rtrim($value), " ,");
                            $tmpTags[] = new MongoRegex("/^" . self::accentToRegex($value) . "$/i");
                        }
                    }
                } else
                    $tmpTags[] = new MongoRegex("/^" . self::accentToRegex(@$searchTags) . "$/i");
                
                if (count($tmpTags)) {
                    $allverb = array('$in', '$all');
                    if (!in_array($verb, $allverb))
                        $verb = '$in';
                    //$query = array("tags" => array($verb => $tmpTags)) ;
                    $query = (!empty($query)) ? self::addQuery($query, array($tagsPath => array($verb => $tmpTags))) : array($tagsPath => array($verb => $tmpTags));
                }
            }
            
            
            return $query;
        }
        
        //*********************** Count search results********************************************************//
        public static function countResults($searchParams, $queries) {
            $count = array();
            $count["spam"] = 0;
            foreach ($searchParams["countType"] as $value) {
                $finalQuery = (isset($queries[$value])) ? $queries[$value] : $queries["global"];
                $col = $value;
                if (in_array($value, ["Group", "NGO", "LocalBusiness", "GovernmentOrganization", "Cooperative"])) {
                    $finalQuery = self::addQuery($finalQuery, array("type" => $value));
                    $col = Organization::COLLECTION;
                }
                $count[$value] = PHDB::count($col, $finalQuery);

				$spamQuerys = array();
				foreach($finalQuery as $typequery => $spamquery){
					$spamQuerys[$typequery] = array();
					foreach($spamquery as $index => $query){
						if(!isset($query["isSpam"]))
							$spamQuerys[$typequery][$index] = $query;
						else
							$spamQuerys[$typequery][$index] = array( "isSpam"=>array('$exists' => true ) );
					}
				}
				$count["spam"] += PHDB::count( $col , $spamQuerys);
            }
            return $count;
        }
        
        
        /**
         * searchFilters
         *
         * @param array $filters
         * @param array $query
         *
         * @return array $query
         */
        public static function searchFilters($filters, $query, $moreParams = []) {
            //Rest::json($filters); exit;
            $fieldPrefix = isset($moreParams["fieldPrefix"]) ? $moreParams["fieldPrefix"]."." : "";
            if (!empty($filters)) {
                foreach ($filters as $key => $value) {
                    if ($key == '$or') {
                        $orArray = [];
                        foreach ($value as $k => $v) {
                            if (!empty($v['$gt'])) {
                                if (($k == "updated" || $k == "created") && is_numeric($v['$gt'])) {
                                    $v['$gt'] = intval($v['$gt']);
                                } else if ($k == "modified") {
                                    if (is_numeric($v['$gt'])) {
                                        $v['$gt'] = new MongoDate($v['$gt']);
                                    }
                                }
                            }
                            if ($k === '_id' && !empty($v['$in'])) {
                                $v['$in'] = array_map(function ($in) {
                                    return new MongoId($in);
                                }, $v['$in']);
                            }
                            if ($k === 'tags' && !empty($v['$in'])) {
                                $v['$in'] = array_map(function ($in) {
                                    return new MongoRegex("/$in/i");
                                }, $v['$in']);
                            }
                            
                            array_push($orArray, array($fieldPrefix . $k => $v));
                        }
                        $query = self::addQuery($query, array('$or' => $orArray));
                    } else if (isset($value['$exists'])) {
                        //echo $value['$exists']; echo gettype($value['$exists']); echo "<br>";
                        if ($value['$exists'] == "true" || $value['$exists'] == "false" || $value['$exists'] == 1)
                            $query = self::addQuery($query, array($fieldPrefix . $key => array('$exists' => filter_var($value['$exists'], FILTER_VALIDATE_BOOLEAN))));
                        else
                            $query = self::addQuery($query, array($fieldPrefix . $key . "." . $value['$exists'] => array('$exists' => 1)));
                    } else if (isset($value['exists'])) {
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$exists' => $value['exists'])));
                    } else if (!empty($value['$not'])) {
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$not' => new MongoRegex("/" . $value['$not'] . "/i"))));
                    } else if (!empty($value['not'])) {
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$not' => new MongoRegex("/" . $value['not'] . "/i"))));
                    } else if (!empty($value['$ne'])) {
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$ne' => $value['$ne'])));
                    } else if (!empty($value['$nin'])) {
                        if ($key == "_id") {
                            $value['$nin'] = array_map(function ($val) {
                                return new MongoId($val);
                            }, $value['$nin']);
                        }
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$nin' => $value['$nin'])));
                    } else if (!empty($value['nin'])) {
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$nin' => $value['nin'])));
                    } else if (!empty($value['$in'])) {
                        if ($key == "_id") {
                            $value['$in'] = array_map(function ($val) {
                                return new MongoId($val);
                            }, $value['$in']);
                        }
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$in' => $value['$in'])));
                    } else if (!empty($value['in'])) {
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$in' => $value['in'])));
                    } else if (!empty($value['$gt'])) {
                        if (($key == "updated" || $key == "created") && is_numeric($value['$gt'])) {
                            $value['$gt'] = intval($value['$gt']);
                        } else if ($key == "modified") {
                            if (is_numeric($value['$gt'])) {
                                $value['$gt'] = new MongoDate($value['$gt']);
                            }
                        }else if(($key=="startDate" || $key=="endDate") && strtotime($value['$gt'])!==false){
                            $value['$gt']=new MongoDate(strtotime($value['$gt']));
                        }
                        
                        
                        // var_dump(array($key => array('$gt' => new MongoDate(strtotime($value['$gt'])))));exit;
                        /**
                         * Convert the second parameter of $gt to numeric.
                         */
                        if (is_array($value['$gt'])) {
                            if (isset($value['$gt'][0]['$size']) && isset($value['$gt'][1]) && is_scalar($value['$gt'][1]) && is_numeric($value['$gt'][1])) {
                                $value['$gt'][1] = (strpos($value, '.') === false ? intval($value['$gt'][1]) : floatval($value['$gt'][1]));
                            }
                        }
                        $query = self::addQuery($query, array($fieldPrefix . $key => array('$gt' => $value['$gt'])));
                    } else if (is_array($value)) {
                            foreach ($value as $k => $v) {
                                if($v === 'true' || $v === 'false') {
                                    if($v === 'true')
                                        $value[$k] = true;
                                    if($v === 'false')
                                        $value[$k] = false;
                                    $query = self::addQuery($query, array($fieldPrefix . $key => $value[$k]));
                                }else{
                                    $value[$k] = new MongoRegex("/.*{$v}.*/i");
                                    $query = self::addQuery($query, array($fieldPrefix . $key => array('$in' => $value)));
                                }
                            }

                    } else if (is_string($value)) {
                        $bool = (in_array($value, ["true", "false"])) ? filter_var($value, FILTER_VALIDATE_BOOLEAN) : $value;
                        $orQuery = array(
                            '$or' => array(
                                array($fieldPrefix . $key => $value),
                                array($fieldPrefix . $key => $bool)
                            )
                        );
                        $query = self::addQuery($query, $orQuery);
                    } else if (is_bool($value)) {
                        $string = $value ? "true" : "false";
                        //var_dump($string);exit;
                        $orQuery = array(
                            '$or' => array(
                                array($fieldPrefix . $key => $value),
                                array($fieldPrefix . $key => $string)
                            )
                        );
                        $query = self::addQuery($query, $orQuery);
                    }
                }
                //    exit;
            }
            //Rest::json($query); exit;
            return $query;
        }
        
        
        public static function searchLocality($query, $localities) {
            $allQueryLocality = array();
            if (!empty($localities)) {
                foreach ($localities as $key => $locality) {
                    if (!empty($locality)) {
                        if (@$locality["type"] == City::COLLECTION) {
                            $queryLocality = array("address.localityId" => @$locality["id"]);
                            if (!empty($locality["postalCode"]))
                                $queryLocality = array_merge($queryLocality, array("address.postalCode" => new MongoRegex("/^" . $locality["postalCode"] . "/i")));
                        } else if (@$locality["type"] == "cp") {
                            $queryLocality = array("address.postalCode" => new MongoRegex("/^" . $locality["name"] . "/i"));
                            if (!empty($locality["countryCode"]))
                                $queryLocality = array_merge($queryLocality, array("address.addressCountry" => $locality["countryCode"]));
                        } else if (@$locality["type"] == "country")
                            $queryLocality = array("address.addressCountry" => $locality["countryCode"]);
                        else {
                            $queryLocality = array("address." . $locality["type"] => @$locality["id"]);
                            if (isset($locality["level"])) {
                                //$queryScopes = array("scope"=> array(
                                //	'$elemMatch'=>array($locality["type"] => $locality["id"])
                                //));
                                $queryScopes = array("scope" => array($locality["type"] => $locality["id"]));
                                $allQueryLocality[] = $queryScopes;
                            }
                        }
                        
                        
                        $queryScope = array("scope." . $key => array('$exists' => 1));
                        $allQueryLocality[] = $queryLocality;
                        $allQueryLocality[] = $queryScope;
                        
                    }
                }
            }
            
            
            if (count($allQueryLocality) === 1)
                $query = self::addQuery($query, $allQueryLocality[0]);
            else if (count($allQueryLocality) > 1)
                $query = self::addQuery($query, array('$or' => $allQueryLocality));
            
            return $query;
        }
        
        /* SearcLocalityByName est un cas isolé des networks
            La structure du params est par exemple {searchLocalityDepartement => "MEURTE ET MOSELLE"}
            L'idée est donc de récupérer l'id de la zone en question et de la formater en query pour la db
        */
        public static function searchLocalityByName($query, $post) {
            $localityReferences['NAME'] = "address.addressLocality";
            $localityReferences['CODE_POSTAL_INSEE'] = "address.postalCode";
            $localityReferences['DEPARTEMENT'] = "address.postalCode";
            $localityReferences['REGION'] = ""; //Spécifique
            $localityReferences['INSEE'] = "address.codeInsee";
            
            foreach ($localityReferences as $key => $value) {
                if (isset($post["searchLocality" . $key]) && is_array($post["searchLocality" . $key])) {
                    foreach ($post["searchLocality" . $key] as $locality) {
                        
                        //OneRegion
                        $queryLocality = array();
                        if ($key == "REGION") {
                            if ($locality == "La Réunion")
                                $locality = "Réunion";
                            $dep = PHDB::findOne(Zone::COLLECTION, array("name" => $locality), array("name"));
                            if (!empty($dep))
                                $queryLocality = array("address.level3" => (string)$dep["_id"]);
                        } else if ($key == "DEPARTEMENT") {
                            $dep = PHDB::findOne(Zone::COLLECTION, array("name" => $locality), array("name"));
                            if (!empty($dep))
                                $queryLocality = array("address.level4" => (string)$dep["_id"]);
                        }//OneLocality
                        else {
                            $queryLocality = array($value => new MongoRegex("/" . $locality . "/i"));
                        }
                        
                        //Consolidate Queries
                        if (!empty($queryLocality)) {
                            if (isset($allQueryLocality)) {
                                $allQueryLocality = array('$or' => array($allQueryLocality, $queryLocality));
                            } else {
                                $allQueryLocality = $queryLocality;
                            }
                        }
                    }
                }
            }
            if (isset($allQueryLocality) && is_array($allQueryLocality))
                $query = self::addQuery($query, $allQueryLocality);
            return $query;
        }
        
        public static function getQueries($post, $admin = null) {
            $costum = CacheHelper::getCostum();
            $queries = array();
            $query = self::startQuery();
            
            // Condidition sur les éléments incomplet, en cours de suppression ou supprimer
            $query = self::addQuery($query, array("state" => array('$nin' => array("uncomplete", "deleted"))));
            $query = self::addQuery($query, array("status" => array('$nin' => array("uncomplete", "deleted", "deletePending"))));
            // Recevoir les résultats privés ou à valider dans les moteurs d'administration
            if (empty($admin)) {
                $query = self::addQuery($query, array(
                                                  '$or' => array(
                                                      array('preferences.private' => array('$exists' => false)),
                                                      array('preferences.private' => false)
                                                  )
                                              )
                );
                $query = self::addQuery($query, array('roles.isBanned' => array('$exists' => false)));
                if (!empty($costum["slug"])) {
                    $query = self::addQuery($query, array(
                                                      '$and' => array(
                                                          array("preferences.toBeValidated." . $costum["slug"] => array('$exists' => false)),
                                                          array("source.toBeValidated." . $costum["slug"] => array('$exists' => false))
                                                      )
                                                  )
                    );
                }
                
            }
            
            if (!empty($post["links"])) {
                $orLinks = array();
                foreach ($post["links"] as $v) {
                    $orLinks[] = array("links." . $v["type"] . "." . $v["id"] => array('$exists' => true));
                }
                
                if (!empty($orLinks))
                    $query = self::addQuery($query, array('$or' => $orLinks));
                
            }
            
            if (!empty($post["latest"]))
                $query = self::addQuery($query, array("updated" => array('$exists' => 1)));
            
            if (!empty($post["lastTimes"]))
                $query = self::addQuery($query, array("updated" => array('$gt' => $post["lastTimes"])));
            
            if (!empty($post["name"]))
                $query = self::searchText($post["name"], $query, $post);
            
            if (!empty($post["searchTags"]) &&
                (count($post["searchTags"]) > 1 || count($post["searchTags"]) == 1 && $post["searchTags"][0] != "")) {
                $operator = (!empty($post["options"]) && isset($post["options"]["tags"]) && isset($post["options"]["tags"]["verb"])) ? $post["options"]["tags"]["verb"] : '$in';
                $query = self::searchTags($post["searchTags"], $operator, $query);
            }
            
            if (!empty($post["type"])) {
                if (is_array($post["type"]))
                    $query = self::addQuery($query, array("type" => ['$in' => $post["type"]]));
                else
                    $query = self::addQuery($query, array("type" => $post["type"]));
            }
            if(!empty($post["openingHours"])){
                $timezone = !empty(Yii::app()->session["timezone"]) ? Yii::app()->session["timezone"] : "UTC";
               date_default_timezone_set($timezone);
                $currentDay = date('D'); 
                $dayMap = [
                    'Mon' => 'Mo',
                    'Tue' => 'Tu',
                    'Wed' => 'We',
                    'Thu' => 'Th',
                    'Fri' => 'Fr',
                    'Sat' => 'Sa',
                    'Sun' => 'Su',
                ];
                $currentDay = $dayMap[$currentDay];
                $currentTime = date('H:i'); 
                $queryHours = [
                    'openingHours' => [
                        '$elemMatch' => [
                            'dayOfWeek' => $currentDay,
                            'hours' => [
                                '$elemMatch' => [
                                    'opens' => ['$lte' => $currentTime], 
                                    'closes' => ['$gte' => $currentTime]
                                ]
                            ]
                        ]
                    ]
                ];
                $query = self::addQuery($query, $queryHours);
            }
            
            
            if (!empty($post["filters"]))
                $query = self::searchFilters($post["filters"], $query);

            if (isset($post["email"]))
                $query = self::addQuery($query, array('email' =>$post["email"]));
            
            if (!empty($post["community"]) && $post["community"] != false)
                $queryCommunity = self::searchCommunity($query, $post["community"]);
            
            if (!empty($post["sourceKey"]) && empty($post["notSourceKey"]))
                $query = self::searchSourceKey($query, $post["sourceKey"]);
            else if (!empty($post["costumSlug"]) && empty($post["notSourceKey"]))
                $query = self::searchSourceKey($query, $post["costumSlug"]);
            
            
            if (!empty($post['locality'])) {
                $query = self::searchLocality($query, $post['locality']);
                $queries[Person::COLLECTION] = self::addQuery($query, array("preferences.publicFields" => array('$in' => array("locality"))));
            }
            
            
            if (!empty($post["priceMax"]))
                $query = self::addQuery($query, array('price' => array('$lte' => (int)$post["priceMax"])));
            
            if (!empty($post["priceMin"]))
                $query = self::addQuery($query, array('price' => array('$gte' => (int)$post["priceMin"])));
            
            if (!empty($post["devise"]))
                $query = self::addQuery($query, array('devise' => $post["devise"]));
            
            if (!empty($post["subType"]))
                $query = self::searchFilters(array('subtype' => $post["subType"]), $query);
            
            if (!empty($post["category"]))
                $query = self::searchFilters(array('category' => $post["category"]), $query);
            
            if (!empty($post["section"])) {
                if (is_array($post["section"]))
                    $query = self::addQuery($query, array("section" => ['$in' => $post["section"]]));
                else
                    $query = self::addQuery($query, array('section' => $post["section"]));
            }
            if (isset($post["mapUsed"]) && $post["mapUsed"] == true) {
                CO2Stat::incNbLoad("co2-map");             
                $query = self::addQuery($query, array("geo" => array('$exists' => true)));                
            }
            //Rest::json($query); exit;
            if(isset($post["searchType"]) && !empty($post["searchType"]) && in_array("spam", $post["searchType"]))
                $query = self::addQuery($query, array( "isSpam"=>array('$exists' => true ) ));
            else
			    $query = self::addQuery($query, array( "isSpam"=>array('$exists' => false ) ));
            
            if (!empty($queryCommunity)) {
                $query = array(
                    '$or' => array(
                        $query,
                        $queryCommunity
                    )
                );
            }
            $queries["global"] = $query;
            return $queries;
        }
        
        
        public static function searchCommunity($query, $community) {
            $arrayIds = array();
            if (!empty($community) && !empty($community["sourceKey"])) {
                if (is_array($community["sourceKey"])) {
                    foreach ($community["sourceKey"] as $keyC => $valueC) {
                        $sourceCostum = Slug::getElementBySlug($valueC, array("name"));
                        $arrayIds[] = array(
                            "type" => $sourceCostum["type"],
                            "id"   => $sourceCostum["id"]
                        );
                    }
                } else {
                    $sourceCostum = Slug::getElementBySlug($community["sourceKey"], array("name"));
                    $arrayIds[] = array(
                        "type" => $sourceCostum["type"],
                        "id"   => $sourceCostum["id"]
                    );
                }
            } else {
                $costum = CacheHelper::getCostum();
                if (!empty($costum) && !empty($costum["contextType"]) && !empty($costum["contextId"]))
                    $arrayIds[] = array(
                        "type" => $costum["contextType"],
                        "id"   => $costum["contextId"]
                    );
            }
            
            $queryCommunity = array();
            
            if (!empty($arrayIds)) {
                // TODO  : Vérifier avec Pierre cette boucle.
                foreach ($arrayIds as $keyelt => $valelt) {
                    $allCommunity = Element::getByTypeAndId($valelt["type"], $valelt["id"]);
                    $rolesSearch = (isset($community["roles"])) ? $community["roles"] : "";
                    
                    if ($valelt["type"] == "projects") {
                        if ($rolesSearch != "")
                            $es = array("links.projects." . $valelt["id"] . ".roles" => $rolesSearch);
                        else
                            $es = array("links.projects." . $valelt["id"] => array('$exists' => 1));
                        
                        array_push($queryCommunity, $es);
                    }
                    
                    array_push($queryCommunity, array("links.organizer." . $valelt["id"] => array('$exists' => 1)));
                    array_push($queryCommunity, array("links.memberOf." . $valelt["id"] => array('$exists' => 1)));
                    
                    
                    if (isset($community["sourceKey"]) && $community["children"] = true) {
                        foreach ($allCommunity as $key => $value) {
                            if (!empty($value)) {
                                array_push($queryCommunity, array("links.organizer." . $key => array('$exists' => 1)));
                                array_push($queryCommunity, array("links.memberOf." . $key => array('$exists' => 1)));
                            }
                        }
                    }
                }
            }
            
            
            if (!empty($queryCommunity)) {
                $queryFinal = array(
                    '$and' => array(
                        $query,
                        array('$or' => $queryCommunity)
                    )
                );
                $query = self::addQuery($query, array('$or' => $queryCommunity));
            }
            
            return $query;
        }
        
        public static function searchSourceKey($query, $sourceKey) {
            $tmpSourceKey = array();
            if ($sourceKey != null && $sourceKey != "") {
                //Several Sourcekey
                if (is_array($sourceKey)) {
                    foreach ($sourceKey as $value) {
                        $tmpSourceKey[] = $value;
                    }
                }//One Sourcekey
                else {
                    $tmpSourceKey[] = $sourceKey;
                }
                
                if (count($tmpSourceKey) > 0) {
                    $costum = CacheHelper::getCostum();
                    $origin = (@$costum) ? "costum" : "costum";
                    
                    $query = self::addQuery($query, array(
                        '$or' => array(
                            array("source.keys" => array('$in' => $tmpSourceKey)),
                            array("reference." . $origin => array('$in' => $tmpSourceKey))
                        )
                    ));
                }
                unset($tmpSourceKey);
            }
            return $query;
        }
        
        public static function convertDataForProposals($allFound) {
            foreach ($allFound as $keyS => $survey) {
                if (@$allFound[$keyS]["dateEnd"]) $allFound[$keyS]["dateEnd"] = date("Y-m-d H:i:s", $allFound[$keyS]["dateEnd"]);
                if (@$allFound[$keyS]["endDate"]) $allFound[$keyS]["endDate"] = date("Y-m-d H:i:s", $allFound[$keyS]["endDate"]);
                if (@$allFound[$keyS]["startDate"]) $allFound[$keyS]["startDate"] = date("Y-m-d H:i:s", $allFound[$keyS]["startDate"]);
                if (@$allFound[$keyS]["created"]) $allFound[$keyS]["created"] = date("Y-m-d H:i:s", $allFound[$keyS]["created"]);
                $allFound[$keyS]["voteRes"] = Proposal::getAllVoteRes($allFound[$keyS]);
                $allFound[$keyS]["hasVote"] = @$allFound[$keyS]["votes"] ? Cooperation::userHasVoted(
                    @Yii::app()->session['userId'], $allFound[$keyS]["votes"]) : false;
                if (isset($allFound[$keyS]["parentType"]) && !empty($allFound[$keyS]["parentType"]) &&
                    isset($allFound[$keyS]["parentId"]) && !empty($allFound[$keyS]["parentId"])) {
                    $allFound[$keyS]["auth"] = Authorisation::canParticipate(@Yii::app()->session['userId'],
                                                                             @$allFound[$keyS]["parentType"], @$allFound[$keyS]["parentId"]);
                }
                
                if (!empty($allFound[$keyS]["producer"])) {
                    $arrayKey = array();
                    foreach ($allFound[$keyS]["producer"] as $k => $v) {
                        $elt = Element::getElementById($k, $v["type"], null, array("name", "slug", "profilThumbImageUrl"));
                        if (!empty($elt))
                            $allFound[$keyS]["producer"][$k] = array_merge($allFound[$keyS]["producer"][$k], $elt);
                    }
                }
            }
            return $allFound;
        }
        
        /**
         * accentToRegex
         *
         * @param string $text
         *
         * @return string $text
         */
        public static function accentToRegex($text) {
            $text = preg_quote($text, '/');
            $from = str_split(utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ'));
            $to = str_split(strtolower('SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy'));
            //‘ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿaeiouçAEIOUÇ';
            //‘SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyyaeioucAEIOUÇ';
            $text = utf8_decode($text);
            $regex = array();
            
            foreach ($to as $key => $value) {
                if (isset($regex[$value]))
                    $regex[$value] .= $from[$key];
                else
                    $regex[$value] = $value;
            }
            
            foreach ($regex as $rg_key => $rg) {
                $text = preg_replace("/[$rg]/", "_{$rg_key}_", $text);
            }
            
            foreach ($regex as $rg_key => $rg) {
                $text = preg_replace("/_{$rg_key}_/", "[$rg]", $text);
            }
            
            //transform white space to regex
            $text = preg_replace("/\s/", "[\s'-]?", $text);
            return utf8_encode($text);
        }
        
        public static function accentToRegexSimply($text) {
            $text = preg_quote($text, '/');
            $from = str_split(utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ'));
            $to = str_split(strtolower('SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy'));
            //‘ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿaeiouçAEIOUÇ';
            //‘SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyyaeioucAEIOUÇ';
            $text = utf8_decode($text);
            
            foreach ($from as $key => $value) {
                $text = str_replace($value, $to[$key], $text);
            }
            return utf8_encode($text);
        }
        
        /**
         * Parcours un array de manière récursive pour chercher [tags] ou [notags] dans des requetes complexes avec '$and' ou '$or'
         * @return le meme array avec les entrées [tags] et [notags] modifiées
         */
        public static function prepareTagsRequete($where) {
            foreach ($where as $key => $subwhere) {
                if ($key === "tags") {
                    $where["tags"] = self::prepareTagsRegex($subwhere, '$in');
                } else if ($key === "notags") {
                    $where["notags"] = self::prepareTagsRegex($subwhere, '$nin');
                } else if (is_array($subwhere)) {
                    $where[$key] = self::prepareTagsRequete($subwhere);
                }
            }
            if (isset($where["tags"]) && !empty($where["tags"])) {
                if (isset($where["notags"]) && !empty($where["notags"])) {
                    $notag = array("tags" => $where["notags"]);
                    $oldwhere = $where;
                    unset($oldwhere["notags"]);
                    $where = array('$and' => array($notag, $oldwhere));
                }
            } else {
                if (isset($where["notags"]) && !empty($where["notags"])) {
                    $where["tags"] = $where["notags"];
                    unset($where["notags"]);
                }
            }
            return $where;
        }
        
        //TODO check si on peut pas mixer avec 'searchTags'
        public static function prepareTagsRegex($wheretags, $MongoDBoperator) {
            if (isset($wheretags) && !empty($wheretags)) {
                $queryTag = array();
                foreach ($wheretags as $key => $tag) {
                    if (is_array($tag)) {
                        //Error
                    } else if ($tag != "")
                        $queryTag[] = new MongoRegex("/" . $tag . "/i");
                }
                if (!empty($queryTag))
                    $wheretags = array($MongoDBoperator => $queryTag);
                else {
                    //Error
                }
            } else {
                //Error
            }
            return $wheretags;
        }
        
        
    }

?>