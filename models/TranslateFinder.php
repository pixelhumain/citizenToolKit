<?php 
class TranslateFinder {
/*

	----------------- COMMUNECTER ----------------- 
*/
	

	public static function build($data,$type)
	{
		
		$ct = 0;
		foreach ($data as $keyID => $valueData) {

			if ( isset($valueData) ) {
				
				
				
				$information = array(
					array( "label" => "my ID :".$keyID ),
					array( "label" => "email : ".@$valueData["email"]),
					array( "label" => "username : ".@$valueData["username"]),
					array( "label" => "country : ".@$valueData["address"]["addressCountry"]),
					array( "label" => "city : ".@$valueData["address"]["addressLocality"]),
					array( "label" => "postalCode : ".@$valueData["address"]["postalCode"]),
					array( "label" => "see in map : http://www.openstreetmap.org/?mlat=".@$valueData["geo"]["latitude"]."&mlon=".@$valueData["geo"]["longitude"]."&zoom=12")
				);
				
				if(@$valueData["shortDescription"])
					$information[] = array( 
						"label" => "shortDescription".@$valueData["shortDescription"], "size" => 3000
						);
				if(@$valueData["description"])
					$information[] = array( "label" => "description".@$valueData["description"],"size"  => 3000);
				
				$person = array(
						"label" => $valueData["name"],
						"icon"  => "user ",
						"children" => $information
				);

				$str = "";
				if( @$valueData["links"]["memberOf"] )
				{
					$elements = array();	
					foreach ($valueData["links"]["memberOf"] as $ix => $o) {
						$el = array( "label" => @$o["name"],
									 "url"	 => @$o["url"]["communecter"] );
						$elements[] = $el;
					}
					$orgas = array(
							"label" 	=> "Organizations",
							"icon" 		=> "users ",
							"children"  => $elements
					);
				}

				if( @$valueData["links"]["projects"] )
				{
					$elements = array();	
					foreach ($valueData["links"]["projects"] as $ix => $o) {
						$el = array( "label" => @$o["name"],
									 "url"	 => @$o["url"]["communecter"] );
						$elements[] = $el;
					}
					$projects = array(
							"label" 	=> "Projects",
							"icon" 		=> "lightbulb-o ",
							"children"  => $elements
					);
				}

				if( @$valueData["links"]["events"] )
				{
					$elements = array();	
					foreach ($valueData["links"]["events"] as $ix => $o) {
						$el = array( "label" => @$o["name"],
									 "url" 	 => @$o["url"]["communecter"] );
						$elements[] = $el;
					}
					$events = array(
							"label" 	=> "Events",
							"icon" 		=> "calendar",
							"children"  => $elements
					);
				}
				
					/*	"# Points of interests"."\n".
						"# Ressources : "."\n".
						"# Places : "."\n".
						"# Classifieds : "; */
			}
		}
		return array( $person, $orgas, $projects, $events );
	}

}