<?php
class Bill {
	const COLLECTION = "bills";

	public static function create(&$bill) {
		Yii::app()->mongodb
			->selectCollection(self::COLLECTION)
			->insert($bill);
	}

	public static function add_helloasso_checkout_id($bill, int $id) {
		$db_where = [];
		if (is_array($bill) && !empty($bill["_id"]))
			$db_where["_id"] = $bill["_id"];
		elseif (is_string($bill))
			$db_where["_id"] = new MongoId($bill);
		$action = [
			'$set'	=> [
				"helloassoCheckout"	=> $id
			]
		];
		PHDB::update(self::COLLECTION, $db_where, $action);
	}

	public static function getByHelloAssoCheckoutId($id) {
		$bill = PHDB::findOne(self::COLLECTION, ["helloassoCheckout" => $id]);
		return $bill;
	}
}
