<?php

class ActivityStreamReference{
    const COLLECTION = "activityStreamReference";
    const NOTIFICATION_STATES = ["isUnseen", "isUnread"];

    public static function add($activityStream){
        if(isset($activityStream["type"]) && isset($activityStream["updated"]) && isset($activityStream["notify"]["id"])){
            $refs = [];
            foreach($activityStream["notify"]["id"] as $userId=>$value){
                $refs[] = [
                    "notificationId"=>$activityStream["_id"],
					"type"=>$activityStream["type"],
					"userId"=>$userId,
					"isUnread"=>isset($value["isUnread"])?$value["isUnread"]:false,
					"isUnseen"=>isset($value["isUnseen"])?$value["isUnseen"]:false,
					"updated"=>$activityStream["updated"],
                    "verb"=>$activityStream["verb"],
                    "targetId"=>isset($activityStream["target"]["id"])?$activityStream["target"]["id"]:null,
                    "targetType"=>isset($activityStream["target"]["type"])?$activityStream["target"]["type"]:null,
                    "targetParentId"=>isset($activityStream["target"]["parent"]["id"])?$activityStream["target"]["parent"]["id"]:null,
                    "targetParentType"=>isset($activityStream["target"]["parent"]["type"])?$activityStream["target"]["parent"]["type"]:null,
                    "notifyObjectType"=>isset($activityStream["notify"]["objectType"])?$activityStream["notify"]["objectType"]:null,
                    "objectId"=>isset($activityStream["object"]["id"])?$activityStream["object"]["id"]:null,
                    "objectType"=>isset($activityStream["object"]["type"])?$activityStream["object"]["type"]:null,
                    "author"=> (isset($activityStream["author"]) && is_array($activityStream["author"])) ? key($activityStream["author"]):null
                ];
            }
            Yii::app()->mongodb->selectCollection(self::COLLECTION)->batchInsert($refs);
        }
    }

    public static function updateStateByUser($stateName, $stateValue){
        if(array_search($stateName, self::NOTIFICATION_STATES) > -1){
            $userId = Yii::app()->session["userId"];
            Yii::app()->mongodb->selectCollection(self::COLLECTION)->update(
                ["userId"=>$userId],
                [
                    '$set'=>[$stateName=>$stateValue]
                ], 
                ["multiple"=>true]
            );
        }
    }

    public static function updateStateByNotificationId($notificationId, $stateName, $stateValue){
        if(array_search($stateName, self::NOTIFICATION_STATES) > -1){
            $userId = Yii::app()->session["userId"];
            Yii::app()->mongodb->selectCollection(self::COLLECTION)->update(
                [
                    "notificationId"=> new MongoId($notificationId),
                    "userId"=>$userId
                ],
                [
                    '$set'=>[$stateName=>$stateValue]
                ], 
                ["multiple"=>true]
            );
        }
    }

    public static function removeByUserId($userId){
        PHDB::remove(self::COLLECTION, [
            "userId"=>$userId
        ]);
    }

    public static function removeByNotificationId($notificationId){
        PHDB::remove(self::COLLECTION, [
            "userId"=>Yii::app()->session["userId"],
            "notificationId"=> new MongoId($notificationId)
        ]);
    }

    public static function initializeState($notificationId){
        PHDB::update(
            self::COLLECTION, 
            ["notificationId"=> new MongoId($notificationId)], 
            ['$set'=>[
                "isUnseen"=>true,
                "isUnread"=>true
            ]]
        );
    }

    public static function getNotificationByConstruct($construct){
        $where = [
            "verb"=>$construct["verb"],
            "targetId"=>$construct["target"]["id"],
            "targetType"=>$construct["target"]["type"],
            "updated"=>[
                '$gte'=> new MongoDate(strtotime('-7 days', time()))
            ]
        ];
        if($construct["labelUpNotifyTarget"]=="object")
            $where["author"] = Yii::app()->session["userId"];
        if($construct["labelUpNotifyTarget"]=="object" && $construct["verb"]==ActStr::VERB_ACCEPT)
            $where["objectId"] = [ '$ne'=>null ];

        if($construct["levelType"])
            $where["notifyObjectType"] = $construct["levelType"];
        else if($construct["verb"]==Actstr::VERB_POST && !@$construct["target"]["targetIsAuthor"] && !@$construct["target"]["userWall"])
            $where["notifyObjectType"] = News::COLLECTION;
        
        if($construct["object"] && !empty($construct["object"]) && in_array($construct["verb"], [Actstr::VERB_COMMENT, Actstr::VERB_REACT])){
            $where["objectId"] = $construct["object"]["id"];
            $where["objectType"] = $construct["object"]["type"];
        }

        $ref = PHDB::findOne(self::COLLECTION, $where, ["notificationId"]);
        
        return $ref ? PHDB::findOneById(ActivityStream::COLLECTION, $ref["notificationId"]) : null;
    }

    public static function getNotificationsByTimeLimit($param, $sort=array("updated"=>-1)){
        $refs = PHDB::findAndSort(self::COLLECTION, $param, $sort);
        $notificationIds = [];
        foreach($refs as $ref)
            $notificationIds[] = new MongoId($ref["notificationId"]);

        return PHDB::findAndSort(
            ActivityStream::COLLECTION, 
            array("_id" => array( '$in'=>$notificationIds ) ),
            $sort
        );
    }

    public static function getNotificationsByStep($param, $indexMin=0, $sort=array("updated"=>-1), $indexStep=15)
	{
        $refs = PHDB::findAndSortAndLimitAndIndex(self::COLLECTION, $param, $sort, $indexStep, $indexMin);
        $notificationIds = [];
        foreach($refs as $ref)
            $notificationIds[] = new MongoId($ref["notificationId"]);
        return PHDB::findAndSort(
            ActivityStream::COLLECTION, 
            array("_id" => array( '$in'=>$notificationIds ) ),
            $sort
        );
	}

    public static function countUnseenNotifications($userId, $elementType, $elementId){
		if($elementType != Person::COLLECTION){
            $params = [
                '$and' => [
                    [
                        'userId'=>$userId,
                        'isUnseen'=>true,
                        'verb'=>['$ne'=>ActStr::VERB_ASK],
                        'type'=>['$ne'=>"oceco"]
                    ],
                    [
                        '$or'=>[
                            [
                                "targetType"=>$elementType,
                                "targetId"=>$elementId
                            ],
                            [
                                "targetParentType"=>$elementType,
                                "targetParentId"=>$elementId
                            ]
                        ]
                    ]
                ]
            ];
        }else{
            $params = [
                'userId'=>$userId,
                'isUnseen'=>true,
            ];
        }

        return PHDB::count(self::COLLECTION, $params);
	}

    public static function importFromActivityStream(){
        $activityStreams = Yii::app()->mongodb->selectCollection(ActivityStream::COLLECTION)->find();

        foreach($activityStreams as $activityStream)
            self::add($activityStream);
    }
}