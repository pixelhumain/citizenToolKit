<?php
 
class Cms {
	const COLLECTION = "cms";
	const CONTROLLER = "cms";
	const MODULE = "cms";
	const ICON = "fa-file";
	//TODO Translate
	public static $types = array (
		"cms"			=> "CMS",
		"doc"			=> "Documentation",
	);

	public static $option = array(
		"tpls.blockCms.events.blockevent"                    		=> "Le bloc d'évènements",
		"tpls.blockCms.events.blockeventcommunity"     				=> "Les évènements de la communauté",
        "tpls.blockCms.events.eventcarousel"                		=> "Les évènements en carousel",
        "tpls.blockCms.events.blockeventsmarterritoire"				=> "Les évènements smarterritoire",
        "tpls.blockCms.events.blockeventslide"         				=> "Les évènements en slide",
        "tpls.blockCms.article.blockarticles"          	 			=> "Les articles en bloc",
        "tpls.blockCms.article.blockarticlescommunity" 				=> "Les articles de la communauté",
        "tpls.blockCms.article.blockarticlescarousel"  				=> "Les articles sous forme de carousel",
        "tpls.blockCms.map.basicmaps"                      			=> "La carte basic",
        "tpls.blockCms.map.communitymaps"                  			=> "La carte de la communauté",
        "tpls.blockCms.community.communitycarousel"            		=> "La Communauté en carousel",
        "tpls.blockCms.community.communityblock"            		=> "Un block de la communautée",
        "tpls.blockCms.textImg.blockwithimg"                 	 	=> "Un bloc avec un texte et une image",
        "tpls.blockCms.projects.blockcarousel"              		=> "Les projets sous forme de carousel",
        "tpls.blockCms.news.basicnews"                          	=> "L'actualité"
        // "tpls.blockCms.textImg.textLeft"							=> "Texte a gauche image a droite",
        // "tpls.blockCms.textImg.textLeft"							=> "Texte a droite image a gauche"
        // "tpls.app.appDda"                        					=> "Affiche les annonces",
        // "tpls.app.appRessource"                  					=> "Affiche les sondages"
    );

    public static $icones = array(
		"fa-newspaper-o"    => 	"News (atualité)",
        "fa-calendar "      => 	"Calendrier (évènement)",
        "fa-lightbulb-o "   => 	"Idée (projets)",
        "fa-map-marker"     => 	"Carte",
        "fa-bullhorn"		=> 	"Annonces",
        "fa-user"			=>	"Personne",
        "fa-gavel"			=> 	"Sondages",
        "fa-rocket"			=> 	"rocket",

    );

	//From Post/Form name to database field name
	public static $dataBinding = array (
	    "id" => array("name" => "id"),
	    "section" => array("name" => "section"),
	    "type" => array("name" => "type"),
	    "subtype" => array("name" => "subtype"),
	    "category" => array("name" => "category"),
	    "collection" => array("name" => "collection"),
	    "name" => array("name" => "name"),
	    "address" => array("name" => "address", "rules" => array("addressValid")),
	    "addresses" => array("name" => "addresses"),
	    "streetAddress" => array("name" => "address.streetAddress"),
	    "postalCode" => array("name" => "address.postalCode"),
	    "city" => array("name" => "address.codeInsee"),
	    "addressLocality" => array("name" => "address.addressLocality"),
	    "addressCountry" => array("name" => "address.addressCountry"),
	    "preferences" => array("name" => "preferences"),
	    "geo" => array("name" => "geo"),
	    "geoPosition" => array("name" => "geoPosition"),
	    "description" => array("name" => "description"),
	    "parent" => array("name" => "parent"),
	    "parentId" => array("name" => "parentId"),
	    "parentType" => array("name" => "parentType"),
	    "position" => array("name" => "position"),
	    "media" => array("name" => "media"),
	    "urls" => array("name" => "urls"),
	    "medias" => array("name" => "medias"),
	    "tags" => array("name" => "tags"),
	    "structags" => array("name" => "structags"),
	    "level" => array("name" => "level"),
	    "shortDescription" => array("name" => "shortDescription"),
	    "rank" => array("name" => "rank"),
	 	"path" => array("name" => "path"),
	 	"haveTpl" => array("name" => "haveTpl"),
	    "page" => array("name" => "page"),	
	    "allPage" => array("name" => "choosePage"),	
	    "gitlab" => array("name" => "gitlab"),	
	    "share" => array("name" => "share"),	

	    "tplParent" => array("name" => "tplParent"),
	    "userCounter" => array("name" => "userCounter"),
	    "blockParent" => array("name" => "blockParent"),
	    "order" => array("name" => "order"),

	    "tplsUser" => array("name" => "tplsUser"),	
	    "cmsList" => array("name" => "cmsList"),	    
	    "modified" => array("name" => "modified"),
	    "source" => array("name" => "source"),
	    "updated" => array("name" => "updated"),
	    "creator" => array("name" => "creator"),
	    "created" => array("name" => "created"),
	    "color"	=> array("name" => "color"),
	    "siteParams"	=> array("name" => "siteParams"),
	    "dontRender"	=> array("name" => "dontRender"),
	    "css"	=> array("name" => "css")
	);
	public static function insertMultiple($cms){
		$res = Yii::app()->mongodb->selectCollection("cms")->batchInsert($cms);
		return $res;
	}

	public static function getCms($post){
		$cms = Element::getElementById($post["ider"],Cms::COLLECTION);
		return $cms;
	}
	public static function getCmsExist($post){
		$cmsList = PHDB::find(Cms::COLLECTION, 
			array( "parent.".$post["contextId"] => array('$exists'=>1))
		);
		$res["element"] = array();
		foreach ($cmsList as $key => $value) {
			array_push($res["element"],array(
				"id"  =>  (String) $value["_id"]
			));
		}
		return array_merge($res);
	}
	public static function getProperties($post){
		$cmsListe = PHDB::find(Cms::COLLECTION, 
			array( "parent.".$post["contextId"] => array('$exists'=>1))
		);
		return $cmsListe;
	}

 	public static function GetCategoryUsedbyTemplate($post){
		$categoryUsed = PHDB::find("templates", ["type" => ['$exists'=>1],"category" => ['$exists'=>1]],["category","type","shared"]);
		return $categoryUsed;
	}

	public static function getConfig(){
		return array(
			"collection"    => self::COLLECTION,
            "controller"   => self::CONTROLLER,
            //"module"   => self::MODULE,
			//"init"   => Yii::app()->getModule( self::MODULE )->assetsUrl."/js/init.js" ,
			//"form"   => Yii::app()->getModule( self::MODULE )->assetsUrl."/js/dynForm.js" ,
            "categories" => CO2::getContextList(self::MODULE),
            "lbhp"=>true
		);
	}
	/**
	 * get all poi details of an element
	 * @param type $id : is the mongoId (String) of the parent
	 * @param type $type : is the type of the parent
	 * @return list of cmses
	 */
	public static function getPoiByIdAndTypeOfParent($id, $type, $orderBy=null){
		$cmses = PHDB::findAndSort(self::COLLECTION,array("parentId"=>$id,"parentType"=>$type), $orderBy);
	   	return $cmses;
	}
	/**
	 * get poi with limit $limMin and $limMax
	 * @return list of cmses
	 */
	//TODO corriger ou supprimer -> DEPRECIATED use getPoiByTagWhereSortAndLimit
	public static function getCmsByTagsAndLimit($limitMin=0, $indexStep=15, $searchByTags=array(), $where=array(), $fields=array()){
		
		if(empty($where))
			$where = array("name"=>array('$exists'=>1));
		else
			$where["name"]=array('$exists'=>1);
		if(@$searchByTags && !empty($searchByTags)){
			$queryTag = array();
			foreach ($searchByTags as $key => $tag) {
				if($tag != "")
					$queryTag[] = new MongoRegex("/".$tag."/i");
			}
			if(!empty($queryTag))
				$where["tags"] = array('$in' => $queryTag); 			
		}

		
		
		$cmses = PHDB::findAndSort( self::COLLECTION, $where, array("updated" => -1), $limitMin, $fields);
	   	return $cmses;
	}

	/**
	 * get cms with generic where param and limit $limMin and $limMax
	 * @return list of cmses
	 */
	public static function getCmsByWhereSortAndLimit($where=array(),$orderBy=array("updated" => -1), $indexStep=10, $indexMin=0){
		$cmses = PHDB::findAndSortAndLimitAndIndex(self::COLLECTION, SearchNew::prepareTagsRequete($where), $orderBy, $indexStep, $indexMin);
		return $cmses;
	}	

	//dans une liste de cmses retourne tout ceux qui ont une $val donnée
	//$l is an array of cmses 
	//$val is a string 
	// work for tags and structags
	public static function getCmsByStruct( $l, $val, $field="tags" ){
		$res = array();

	    foreach ( $l as $k => $v ) {
	        if( isset( $v[$field] ) && is_string($v[$field]) && $val == $v[$field] )
	        	$res[] = $v;
	        else if( isset( $v[$field] ) && is_array($v[$field]) && in_array( $val, $v[$field] ) ) 
	            $res[] = $v;
	    }
	    return $res;
	}

	/**
	 * get a CMS By Id
	 * @param String $id : is the mongoId of the cms
	 * @return cms
	 */
	public static function getById($id) { 
		
	  	$cms = PHDB::findOneById( self::COLLECTION ,$id );
	  	
	  	// Use case notragora
	  	$where=array("id"=>@$id, "type"=>self::COLLECTION, "doctype"=>"image");
	  	$cms["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
	  	$where["doctype"]="file";
	  	$cms["files"] = Document::getListDocumentsWhere($where,"file");
	  	return $cms;
	}

	public static function deleteCms($id,$collection=null){
		$cms = Element::getElementById($id,self::COLLECTION);
		$collection = ($collection == null) ? CMS::COLLECTION : $collection;
		$cmsChild = PHDB::find($collection, array("blockParent" => $id));
		$parentId = null;
		$parentType = null;
		if( isset($cms["type"]) && $cms["type"] != "blockCms" ){
			$parentId =  array_keys($cms["parent"])[0];
			$parentType = $cms["parent"][$parentId]["type"];
		}

		if(isset($cms["type"]) && $cms["type"]=="blockCms" && !Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) &&  !in_array(Yii::app()->session["user"]["slug"], ["mirana", "gova", "ifals", "devchriston", "nicoss","Jaona","anatolerakotoson","yorre"]) && $imgValue["yorre"] !== "" ){
			return Rest::json(array("result"=> false, "error"=>"403", "msg" => "Forbidden : Only allowed for the super administrator"));
		}else if(isset($cms["type"]) && $cms["type"]!="blockCms" && !Authorisation::canDeleteElement($parentId,$parentType,Yii::app()->session["userId"])){
			return Rest::json(array("result"=> false, "error"=>"403", "msg" => "Forbidden : Only allowed for the administrator"));
		}elseif (!empty($cmsChild)) {           
			CMS::deleteChild($cmsChild,$collection);
			PHDB::remove($collection, array("_id"=>new MongoId($id)));
			//REMOVE TABS CMS ENTRY
			PHDB::update(CMS::COLLECTION,["tabs.".$id => ["\$exists" => true]], ["\$unset" => ["tabs.".$id => ""]]);
			return Rest::json(array("result" => true, "msg" => "The element has been deleted succesfully")); 	
			// return Rest::json(array("result"=> false, "msg" => "<b>Can't be deleted :</b><br> This container is not empty. Try to delete their content first!"));
		}else{

			PHDB::remove($collection, array("_id"=>new MongoId($id)));
			//REMOVE TABS CMS ENTRY
			PHDB::update(CMS::COLLECTION,["tabs.".$id => ["\$exists" => true]], ["\$unset" => ["tabs.".$id => ""]]);
			return Rest::json(array("result" => true, "msg" => "The element has been deleted succesfully"));
		}
	}

	public static function deleteChild($cmsList,$collection=null)
	{
		$collection = ($collection == null) ? CMS::COLLECTION : $collection;
		foreach ($cmsList as $cms) {
			$cmsChild = PHDB::find($collection, array("blockParent" => (String)$cms['_id']));
			if(!empty($cmsChild)){
				CMS::deleteChild($cmsChild,$collection);
			}
			PHDB::remove($collection, array("_id"=>$cms['_id']));
			//REMOVE TABS CMS ENTRY
			PHDB::update(CMS::COLLECTION,["tabs.".(String)$cms['_id'] => ["\$exists" => true]], ["\$unset" => ["tabs.".(String)$cms['_id'] => ""]]);
		}
	}

	public static function delete($id, $userId) {
		if ( !@$userId) {
            return array( "result" => false, "msg" => "You must be loggued to delete something" );
        }
        
        $cms = self::getById($id);
        if (!self::canDeleteCms($userId, $id, $cms)) 
        	return array( "result" => false, "msg" => "You are not authorized to delete this cms.");
        
        //Delete the comments
        $resComments = Comment::deleteAllContextComments($id,self::COLLECTION, $userId);
		if (@$resComments["result"]) {
			PHDB::remove(self::COLLECTION, array("_id"=>new MongoId($id)));
			$resDocs = Document::removeDocumentByFolder(self::COLLECTION."/".$id);
		} else {
			return $resComments;
		}
		
		return array("result" => true, "msg" => "The element has been deleted succesfully", "resDocs" => $resDocs);
	}

	public static function canDeleteCms($userId, $id, $cms = null) {
		if ($cms == null) 
			$cms = self::getById($id);
		//To Delete cms, the user should be creator or can delete the parent of the cms
        if ( $userId == @$cms['creator'] || Authorisation::canDeleteElement(@$cms["parentId"], @$cms["parentType"], $userId)) {
            return true;
        } else {
        	return false;
        }
    }



    public static function getDataBinding() {
	  	return self::$dataBinding;
	}

	//based on a list opis 
	//hierarchy can be defined with tags 
	//the parent carries tag #test, and all his children cayy #test.chichi1, #test.chichi2 , 
	//$field is the tag based field hierarchy works on...
	public static function hierarchy($pList, $field = "tags"){
		$cmsList = array();
		$blockParents = array();
		foreach ($pList as $key => $value) 
		{	
			//
			if(isset( $value[ $field ]) && is_string( $value[ $field ]) ){
				$cmsTree = explode(".", $value[ $field ]);
				//this is a child
				if ( count($cmsTree) > 1 ) 
				{
					$parentTag = $cmsTree[0];
					if(!isset($blockParents[ $parentTag ]) )
						$blockParents[ $parentTag ] = array();

					$blockParents[ $parentTag ][] = $value;
				} 
				//this is a parent
				else 
					$cmsList[$value[ $field ]] = $value;
			}
			//use case if strutags is an array
			else if(isset( $value[ $field ]) && is_array( $value[ $field ]) && count($value[ $field ]))
			{
				foreach ( $value[ $field ] as $tk => $tv ) 
				{
					$cmsTree = explode(".", $tv);
					if ( count($cmsTree) > 1 ) 
					{
						$parentTag = $cmsTree[0];
						if(!isset($blockParents[ $parentTag ]) ){
							$blockParents[ $parentTag ] = array();
						}

						$blockParents[ $parentTag ][] = $value;
					} 
					else 
						$cmsList[$tv] = $value;
				}
			} else
				$cmsList[] = $value; 
		}
		return array( "orphans"=>$cmsList,
						
					  "parentTree"=>$blockParents );	
	}

	public static function getCmsByType($type) {

		return PHDB::find(Cms::COLLECTION,array("type" => $type));
		
	}

	public static function getCmsByWhere($where=array(),$sortCriteria=null) {
		if($sortCriteria!=null)
			return PHDB::findAndSort(Cms::COLLECTION,$where,$sortCriteria,1000);
		else
			return PHDB::find(Cms::COLLECTION,$where);
	}

	public static function getCmsWithChildren($costumId, $page, $sortCriteria=null, $footer = null){
		$parents = Cms::getCmsByWhere(
			[
				'$and' => array(
					[
						'$or'=> array(
							["advanced.persistent"=>array('$exists'=>true)],
							[ "page" => $page ]
						)
					],
					["blockParent"=>array('$exists'=>false)],
					[ "parent.".$costumId => array('$exists'=>1) ]
				)
			],
			$sortCriteria
		);

		$blockChildren = Cms::getCmsByWhere(

			[
				//TRY REMOVE and Have TPL TYPE////
				//"type" => "blockChild",
				'$and' => array(
					//TRY REMOVE and Have TPL TYPE////
					[
						'$or'=> array(
							["advanced.persistent"=>array('$exists'=>true)],
							[ "page" => $page ]
						)
					],
					["blockParent"=>array('$exists'=>1)],
					[ "parent.".$costumId => array('$exists'=>1) ]
				)
				// //END TRYYYY/////
				// "parent.".$costumId => array('$exists'=>1)
			], 
			["position" => 1]
		);	

		$countChildren = PHDB::count(Cms::COLLECTION,array(
			"type" => "blockChild",
			"parent.".$costumId => array('$exists'=>1),
			"page" => $page
		));
		if ($countChildren > 400) {
			$str = "<div class='container' style='width:100%; text-align: center;background-color: lightgray;height: 190px;display: flex;
			justify-content: center;'><div style='background-color: white;width: 500px;margin: auto;padding: 25px;'><h1 style='color:#e6344d'>Oups</h1><p>Chargement incomplet!!</p>";
			if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) {
				$str .= "<font>Impossible de charger ".$countChildren." des block CMS</font>";
			};
			$str .="</div></div>";
			echo $str;
		}
		
		$addChildren = function($parent) use(&$addChildren, &$blockChildren){
			$parent["blockChildren"] = [];
			foreach($blockChildren as $childKey => $child){
				if(isset($child["blockParent"]) && ($child["blockParent"] == $parent["_id"]->{'$id'})){
					$parent["blockChildren"][$childKey] = $addChildren($child);
					if ($child["path"] == "tpls.blockCms.superCms.container" && isset($child["alias"])) {	
						foreach ($child["alias"] as $keyParent => $valueParent) {	
							foreach ($valueParent["container"] as $key => $value) {
								$parent["blockChildren"][$childKey]["blockChildren"][$value] = self::getByIdWithChildreen($value);	
								$parent["blockChildren"][$childKey]["blockChildren"][$value]["_id"] = new MongoId($value);
							}
						}		
					}
				}
			}	
			return $parent;
		};

		foreach($parents as $id => $parent){
			$parents[$id] = $addChildren($parent);
			if (isset($parents[$id]["alias"])) {	
				foreach ($parents[$id]["alias"] as $keyParent => $valueParent) {	
					foreach ($valueParent["container"] as $key => $value) {
						$parents[$id]["blockChildren"][$value] = self::getByIdWithChildreen($value);	
						$parents[$id]["blockChildren"][$value]["_id"] = new MongoId($value);
					}
				}
			}
		} 
		return $parents;
	}

	public static function getCmsFooter($costumId){

		$footer = self::getCmsWithChildren($costumId, "allPages", array("position" => 1));

		return $footer;
	}
	public static function getByIdWithChildreen($id, $nofile=false , $collection=null){
		if ($collection === null) {
			$collection = "templates";
		}
		$cms = PHDB::findOneById( $collection ,$id );
		if ( empty($cms) ){
			$cms = PHDB::findOneById( self::COLLECTION ,$id );
			$collection = self::COLLECTION;
		}

		$addChildren = function($parent) use(&$addChildren, $collection){
			$parent["blockChildren"] = [];
			$parentId = "";
			if (isset($parent["parent"])) {
				$parentId = array_keys($parent["parent"])[0];
				$blockChildren = PHDB::find(
					$collection, 
					["blockParent" => @$parent["_id"]->{'$id'},
					"parent.".$parentId => array('$exists'=>1)
				]
			);
				foreach($blockChildren as $childKey => $child){
					if(isset($child["blockParent"]) && ($child["blockParent"] == $parent["_id"]->{'$id'})){
						$parent["blockChildren"][$childKey] = $addChildren($child);
					}
				}
			}
			return $parent;
		};

		$cms = $addChildren($cms);
		
		if (isset($cms["alias"])) {	
			foreach ($cms["alias"] as $keyParent => $valueParent) {	
				foreach ($valueParent["container"] as $key => $value) {
					$cms["blockChildren"][$value] = self::getByIdWithChildreen($value);	
				}
			}
		}
		
		// Use case notragora
		if(empty($nofile)){
			$where=array("id"=>@$id, "type"=>$collection, "doctype"=>"image");
			$cms["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
			$where["doctype"]="file";
			$cms["files"] = Document::getListDocumentsWhere($where,"file");
		}
		return $cms;
	}


	public static function getTplByWhere($where=array(),$sortCriteria=null) {
		return	PHDB::find("cms", array("type" => "template","category"  =>  "Sample"));
	}
	public static function listPage($pList, $field = "page"){
		$listPage = array();
		$pages = array();
		foreach ($pList as $key => $value) {
			if(isset( $value[ $field ]) && is_string( $value[ $field ]) ){
				$pages[] = $value[ $field ];
			}
		}
		foreach(array_unique($pages) as $filter) {
			$listPage[] = $filter;
		}
		return ($listPage);
	}
	public static function duplicateDocumentFromTplBlock(){
		/* Just check if image OR BackgroundUrl to duplicate in good costum*/ //"be carefull to duplicate document";
	}

	public static function recursiveDuplicateBlock($blockTODuplicate, $context, $blockParent=null,$to=self::COLLECTION){



		$duplicateBlock=$blockTODuplicate;
		if(!in_array($context["parentSlug"], array_keys(@$blockTODuplicate["parent"]))){
			self::duplicateDocumentFromTplBlock();
		}
		//print_r($blockTODuplicate);
		if(!empty($duplicateBlock["blockChildren"]))
			$duplicateChildren=$duplicateBlock["blockChildren"];
		
		if(!isset($duplicateBlock["blockName"]) && isset($duplicateBlock["name"])) {					
			$duplicateBlock["blockName"] = $duplicateBlock["name"];
		}

		$duplicateBlock["blockOrigin"] = (string)$duplicateBlock["_id"];
		
		$removeDataUnuseful=["_id","profilImageUrl","profilMediumImageUrl", "profilThumbImageUrl", "profilMarkerImageUrl", "blockChildren", "haveTpl", "type", "name"];
		foreach($removeDataUnuseful as $v){
			if(isset($duplicateBlock[$v]) && ($v!="name" || @$blockTODuplicate["type"]!= "blockCms"))
			 	unset($duplicateBlock[$v]);
		}
		/** The first condition is for two useCase (and potentially the third case to develop in front)
			1 -- Duplicate the children inside this recursive loop after the first parent 
			2 -- Case of using BLOCKCMS (template) where destination is defined (cms/useCmsAction.php)
			3 -- [TO DEVELOP] : Case when we want to copy, cut or duplicate a block to another destination
		**/

		if(!isset($duplicateBlock["path"])){
			$duplicateBlock["name"] = "Block CMS Not Found";
			$duplicateBlock["path"] = "tpls.blockNotFound";
		}

		//up persistence if parent is persistent
		if(!empty($blockParent)){
			$duplicateBlock["blockParent"]=$blockParent;
			$parentData = PHDB::findOneById( self::COLLECTION , $blockParent );
			if ( isset($parentData["advanced"]["persistent"])){
				$duplicateBlock["advanced"]["persistent"]= $parentData["advanced"]["persistent"];
			}
		}
		//Or case of duplicate other then section like title or colunm inside directly 
		else if(!empty($blockTODuplicate["blockParent"])){
			$duplicateBlock["blockParent"]=$blockTODuplicate["blockParent"];
		} 
		$duplicateBlock["parent"] = array(
			$context["parentId"]=>array(
				"type" => $context["parentType"], "name" => $context["parentSlug"]
			)
		);
		$duplicateBlock["modified"]=new MongoDate(time());
        $duplicateBlock["updated"] = time();
        $duplicateBlock["creator"] = Yii::app()->session["userId"];
        $duplicateBlock["created"] = time();
        $duplicateBlock["source"]  = array("insertOrign"=>"costum", "key"=>$context["parentSlug"], "keys"=>[$context["parentSlug"]]);
		if (!empty($context["page"])) {
			$duplicateBlock["page"] = trim($context["page"], "#");
		} 

		// add type  if context type setted and blockParent not set ( can save section only as blockCmsType) 
		// remove page and order 
		// add name for new blockCms
		$parentDuplicatedId = "";
		if(isset($context["type"]) && isset($context["type"]) == "templateBlock" && !isset($duplicateBlock["blockParent"])){
			$duplicateBlock["type"] = "template";
			$duplicateBlock["css"]["width"] = "100%";
			unset($duplicateBlock["page"]);
			unset($duplicateBlock["source"]);
			unset($duplicateBlock["order"]);
			unset($duplicateBlock["position"]);
			Yii::app()->mongodb->selectCollection($to)->update(array("_id" => new MongoId($context["newId"])), $duplicateBlock, array("upsert" => true));
			$parentDuplicatedId = $context["newId"];
		} else {
			Yii::app()->mongodb->selectCollection($to)->insert($duplicateBlock);
			$parentDuplicatedId = (string)$duplicateBlock["_id"];
		}

		if(!empty($duplicateChildren)){
			$duplicateBlock["blockChildren"]=[];
			foreach($duplicateChildren as $key => $v){
				$child=self::recursiveDuplicateBlock($v, $context, $parentDuplicatedId,$to);
				$duplicateBlock["blockChildren"][(string)$child["_id"]]=$child;
			}
		}
		return $duplicateBlock;

	}
	public static function duplicateBlock($idCms, $context, $blockParent=null, $from=self::COLLECTION,$to=self::COLLECTION){
		/*if ($count >= $limit) {
			echo 'Unable to duplicate too many CMS';
			return;
		}*/
		//$cms_elt = [];
		
		$cmsInsertWithChildren = [];
		//$allBlocks=array();	
		foreach ($idCms as $key) {
			// GET THE BLOCK WHITH ALL CHILDREN TO DUPLICATE
			$blockTODuplicate=self::getByIdWithChildreen($key, true);
			if(!empty($blockTODuplicate))
				$cmsInsertWithChildren=self::recursiveDuplicateBlock($blockTODuplicate, $context, $blockParent,$to);
			//var_dump($blockTODuplicate);
			//var_dump($cmsInsertWithChildren);
			//exit;
		//	$duplicateBlock = Element::getElementById($value,Cms::COLLECTION);
			/*$duplicateBlock["parent"] = array(
				$post["parentId"]=>array(
					"type" => $post["parentType"], "name" => $post["parentSlug"]
				)
			);
			//$defaultType = "blockCms";
			$duplicateBlock["modified"]=new MongoDate(time());
            $duplicateBlock["updated"] = time();
            $duplicateBlock["creator"] = Yii::app()->session["userId"];
            $duplicateBlock["created"] = time();
            $duplicateBlock["source"]  = array("insertOrign"=>"costum", "key"=>$post["parentSlug"], "keys"=>[$post["parentSlug"]]);
			if (!empty($post["page"])) {
				$duplicateBlock["page"] = trim($post["page"], "#");
			}	
			if (!empty($duplicateBlock["type"]) || $blockParentId == ""){
				$defaultType = $duplicateBlock["type"];
				if ($duplicateBlock["type"] == "blockCopy" || $blockParentId == "") {
					$duplicateBlock["haveTpl"] = "false";
					$duplicateBlock["type"] = "blockCopy";
					$duplicateBlock["tplParent"] = null;
				}else{					
					$duplicateBlock["type"] = "blockChild";
				}
				$duplicateBlock["blockParent"] = $blockParentId;
			}
			if (!empty($post["type"])){
				if(isset($post['type']) && $post['type'] == "blockCopy"){
					if($blockParentId){
						$duplicateBlock["haveTpl"] = "true";
						$duplicateBlock["type"] = "blockCopy";
						$duplicateBlock["tplParent"] = $blockParentId;
					}else{
						$duplicateBlock["haveTpl"] = "false";
						$duplicateBlock["type"] = "blockCopy";
						$duplicateBlock["tplParent"] = null;
					}
				}else{
					$duplicateBlock['type'] = "blockChild";
					unset($duplicateBlock["haveTpl"]);
				}
			}
			$removeDataUnuseful=["_id","profilImageUrl","profilMediumImageUrl", "profilThumbImageUrl", "profilMarkerImageUrl"];
			foreach($removeDataUnuseful as $v){
				if(@$duplicateBlock[$v]) unset($duplicateBlock[$v]);
			}
			Yii::app()->mongodb->selectCollection(Cms::COLLECTION)->insert( $duplicateBlock );
			//PHDB::insert(Cms::COLLECTION, $duplicateBlock);
		/*	$childrenBlock = PHDB::find(CMS::COLLECTION, array("blockParent" => $value), array("_id"));
			$childrenBlock = array_keys($childrenBlock);
			if (!empty($childrenBlock)) {
				self::duplicateBlock($childrenBlock,(String)$duplicateBlock["_id"], array(
					"parentId"   => $post['parentId'], 
					"parentType" => $post["parentType"],
					"parentSlug" => $post['parentSlug'],
					"page"       => $duplicateBlock['page']),
				$count + 1,
				$limit);
			}                  
			//$cms_elt[] = $params;
			$cmsInserted[] = $duplicateBlock;
			*/
			/*****Image feature*****/
			/*if ($defaultType != "blockCms") {
				$initImages = PHDB::find( Document::COLLECTION,array(
					"id"=> $value,
					"type"=>CMS::COLLECTION,
					"doctype" => "image"
				));
				$listImages = Document::getListOfImage($initImages);
				foreach($listImages as $id => $imgValue){
					if(!empty($imgValue["imagePath"]) && file_exists(getcwd().$imgValue["imagePath"])){
						$upload_dir = str_replace($value, (string)$duplicateBlock["_id"], $imgValue["imagePath"]);
						$upload_dir = str_replace("upload/", "", $upload_dir);
						$upload_dir = str_replace($imgValue["name"], "", $upload_dir);
						if(strpos($upload_dir, Document::GENERATED_ALBUM_FOLDER) === false)
							$upload_dir.="album";
						if( !Yii::$app->fs->has ( $upload_dir ) ){
							Yii::$app->fs->createDir ( $upload_dir );
						}
						$stream = fopen(getcwd() . $imgValue["imagePath"], 'r+');
						Yii::$app->fs->writeStream($upload_dir."copy_".$imgValue["name"], $stream, ['visibility' => 'public']);
						$params = array(
							"id" => (string)$duplicateBlock["_id"],
							"type" => CMS::COLLECTION,
							"folder" => CMS::COLLECTION."/".(string)$duplicateBlock["_id"]."/".Document::GENERATED_ALBUM_FOLDER,
							"moduleId" => "communecter",
							"name" => "copy_".$imgValue["name"],
							"size" => $imgValue['size'],
							"subKey"=>"block",
							"contentKey" => Document::IMG_SLIDER,
							"doctype"=> Document::DOC_TYPE_IMAGE,
							"author" => (isset(Yii::app()->session["userId"])) ? Yii::app()->session["userId"] : "",
						);	
						Document::save($params);
					}
				}*/
			//}*/
			
		}

		//$res_cms = PHDB::batchInsert(Cms::COLLECTION,$cms_elt);
            // $res=array("result"=>true, "msg"=>Yii::t("common","Information saved"));
		return $cmsInsertWithChildren;
	}

	public static function upPersistentChild ($blockTOPersist,$value) {
		$persistBlock=$blockTOPersist;
		$persistChildren = array();
		if(!empty($persistBlock["blockChildren"]))
			$persistChildren=$persistBlock["blockChildren"];

		if(!empty($persistChildren)){
			$persistBlock["blockChildren"]=[];
			foreach($persistChildren as $key => $v){

				PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($key) ],[
					'$set'=> [
						"advanced.persistent" => $value
					]
				]);

				$child=self::upPersistentChild($v,$value);

			}
		}

		return $persistBlock;
	}

	public static function updatePersistent($idCms,$value,$fromParent=false)
	{	
		PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($idCms) ],[
			'$set'=> [
				"advanced.persistent" => $value
			]
		]);

		if($fromParent){
			$cms = self::getByIdWithChildreen($idCms, true, Cms::COLLECTION);
			self::upPersistentChild($cms,$value);
		}


	}

	public static function insertSection($page){
		$costum=Costum::getCostumCache();
		$newBlocContainer=array(
			"path"=>"tpls.blockCms.superCms.container",
			"collection" => Cms::COLLECTION,
			"parent"=>array(
				$costum["contextId"]=>array(
					"type"=>$costum["contextType"],
					"name"=>$costum["contextSlug"]
				)),
			"css"     => ["justifyContent" => "space-evenly"],
			"page"	  =>$page,
			"source"  =>array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]]),
			"updated" => time(),
			"creator" => Yii::app()->session["userId"],
			"created" => time(),
			"modified"=> new MongoDate(time())
    
		);

		Yii::app()->mongodb->selectCollection(self::COLLECTION)->insert($newBlocContainer);
		$viewBlocParams=self::getParamsView($newBlocContainer);
		return ["viewBlocParams" => $viewBlocParams, "newBlocContainer" => $newBlocContainer];

	}

	public static function findCmsOrphans($idCms,$page){	

		$cmsOrphans = [];	
		$childBlock = PHDB::find(CMS::COLLECTION, array("blockParent" => $idCms), array("page"));
		$childBlockID = array_keys($childBlock);
		if (!empty($childBlock)) {			
			foreach ($childBlock as $key => $value) {
				// if ($value["page"] !== $page) {
					$cmsOrphans[] = array("id" => $value["_id"]->{'$id'}, "page" => $value["page"]);
				// }
			}
		}
		return $cmsOrphans;
	}

	public static function getParamsView($params, $clienturi="", $el=[]){  
		$costum=Costum::getCostumCache();
		$convertToview = [
            "cmsList"   =>  [],
            "blockKey"  => (string)$params["_id"],
            "v"  => $params,
            "page"      => $params['page'] ?? "",
            "canEdit"   => $costum["editMode"],
            "type"      => @$params["path"],
            "kunik"     => (string)$params["_id"],
            "content"   => "",
            "el"        => $el,
            "i"=>0,
            'costumData'    => $costum,
            'blockName' => @$params["name"],
            "clienturi" => $clienturi,
            // 'range'     => $i,
            "defaultImg" => Yii::app()->controller->module->assetsUrl . "/images/thumbnail-default.jpg"
        ];
       return $convertToview;
    }
    public static function getParamsBlock($params, $clienturi="", $el=[]){  
		$path = $params["path"];
	    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
	    $pathExplode = explode('.', $params["path"]);
	    $count = count($pathExplode);
	    $content = isset($params['content']) ? $params['content'] : [];
	    $kunik = $pathExplode[$count - 1] . $params["_id"];
	    $blockKey = (string)$params["_id"];
		$blockName = (string)@$params["name"];
		//var_dump($v); exit;
		//var_dump($this->context->costum);die;

		$convertToview = [
			"cmsList"   => $params,
			"blockKey"  => $blockKey,
			"blockCms"  => $params,
			"page"      => $params["page"],
			"canEdit"   => @$costum["editMode"],
			"type"      => $path,
			"kunik"     => $kunik,
			"content"   => $content,
			"blockName" => $blockName,
			"costum"=>Costum::getCostumCache(),
			"el"=> [],
			'range'     => 0,
			"clienturi" => $clienturi
		];
	    return $convertToview;
    }

	// create or update cssFile 
	public static function createORupdateCss($contextId, $css)
    {
        
        $params=array(
            "contextId"=>$contextId, 
            "css"=>$css, 
            "collection"=>"cssFile",
            "created"=>time(),
            "updated"=>time());

		$cssFile = PHDB::find("cssFile", array("contextId"=>$contextId));

		if(empty($cssFile)){
			Yii::app()->mongodb->selectCollection("cssFile")->insert($params);
		}
		else{
			Yii::app()->mongodb->cssFile->findAndModify( array("contextId"=>$contextId), array('$set' => array("css"=> $css , "updated" => time() )));
		}
    }

	// get cssFile
	public static function getCssByCostumId($contextId)
    {
		return $cssFile = PHDB::find("cssFile", array("contextId"=>$contextId));
    }

	public static function getNewPageName($page, $costum)
    {
        $pagename = str_replace(preg_replace('/[^0-9]/', '', $page), "", $page);
		$costumLang = (isset($costum["costum"])) ? Costum::setLanguageActiveInCostum($costum["costum"]) : "fr";
        $menu = array_keys($costum['costum']['app']);
        $pageNumber = (intval(preg_replace('/[^0-9]/', '',max(array_filter($menu, function($v, $k) use ($pagename) {
            if(strpos($v, $pagename)> -1){
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH))))+1);
        $newPageName = "#$pagename$pageNumber";
        $newName = [];
		$newName[$costumLang] = ucfirst($pagename)." $pageNumber";
        return array("pagename" => $newPageName, "name" => $newName);
    }

	// public static function useTemplate($params,$contextCostum,$resetCache = true){
	// 	$params = DataValidator::clearUserInput($params);
    //     $template = Element::getElementById($params["id"],$params["collection"]);
    //     $tplSelectedId = (string)$template['_id'];
        	
    //     $allCmsId = isset($template["cmsList"]) ? $template["cmsList"] : [];
    //     $tplSelectedType = $template["subtype"] ?? "page";
    //     /**Duplicate block**/
    //     if(Authorisation::isInterfaceAdmin()){
    //         /* WE HAVE 2 TYPE OF TEMPLATES. FOR WHOLE COSTUM (type site) AND DEDICATED FOR ONE "Page" */
    //         // DUPLICATE ALL PAGE FROM TEMPLATE AND CHANGE WHOLE COSTUM CONFIG ACCORDIND TO TEMPLATE IF IT IS A SITE 
    //         if (!empty($template["siteParams"])) {    
    //            $template["siteParams"]["app"] = array_unique(array_merge($template["siteParams"]["app"], @$contextCostum["app"]), SORT_REGULAR); 
    //            if ($params["action"] == "") { 
    //             // If action is empty string, it means THE TEMPLATE CHOOSED IS NEVER USED, then we duplicate all blocks
    //             Cms::duplicateBlock($allCmsId, array(
    //                 "parentId"   => $params['parentId'], 
    //                 "parentType" => $params["parentType"],
    //                 "parentSlug" => $params['parentSlug']
    //             ));
    //             // NOW, WE INCREMENT THE NUMBRE OF TEMPLATES USER
    //             Element:: updatepathvalue($params["collection"],$tplSelectedId,"userCounter", isset($template["userCounter"]) ? $template["userCounter"]+1 : 1);
    //             }elseif ($params["action"] == "using") {
    //                 // If action is using, THE TEMPLATE CHOOSED IS USING (Bouton reinitialiser est cliqué), Then we reset the template as default. 
    //                 // Remove the old block from each page then duplicate the template again 
    //                 $cmsEachPage = array();
    //                 $allCmsToReset = array();
    //                 $i = 0;
    //                 foreach (array_keys($template["siteParams"]["app"]) as $pageHash) {                 
    //                     PHDB::remove("cms",[
    //                         "parent.".$contextCostum["contextId"] => array('$exists'=>1),
    //                         "page" => trim($pageHash, "#")]
    //                     );
    //                     $i++;
    //                     if (count(array_keys($template["siteParams"]["app"])) == $i) {                                    
    //                         // Reduplicate the Template
    //                         Cms::duplicateBlock($allCmsId, array(
    //                             "parentId"   => $params['parentId'], 
    //                             "parentType" => $params["parentType"],
    //                             "parentSlug" => $params['parentSlug']
    //                         ));
    //                     }
    //                 }
    //             }              
    //             foreach (array_keys($template["siteParams"]) as $key => $value) {
    //                 Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.".$value, $template["siteParams"][$value]);
    //             }
    //         // JUST DUPLICATE THE BLOCK FROM ONE PAGE IF TEMPLATE IS DEDICATED FOR ONE PAGE 
    //         }else{     
    //             Cms::duplicateBlock($allCmsId, array(
    //                 "parentId"   => $params['parentId'], 
    //                 "parentType" => $params["parentType"],
    //                 "parentSlug" => $params['parentSlug'],
    //                 "page"       => $params['page']));
    //         }

    //         // THEN SAVE INTO COSTUM THE TEMPLATES used AND using 
    //         if (!empty($contextCostum["tplUsed"])) {
    //             foreach ($contextCostum["tplUsed"] as $tplUsedId => $valueTplUsed) {  
    //                 if ($tplUsedId != $tplSelectedId) {
    //                     Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplUsedId, "used");
    //                 }else{
    //                     Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplUsedId, "using");
    //                 }                
    //             }
    //             if (!in_array($tplSelectedId,$contextCostum["tplUsed"])) {
    //                 Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplSelectedId, "using");
    //             }
    //         }else{     
    //             Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.tplUsed.".$tplSelectedId, "using");
    //         }
	// 		if($resetCache)
    //         	Costum::forceCacheReset();
    //         $res = array("result" => true, "msg" => Yii::t("common", "Template has been well activated"));   
    //     }else
    //     $res = array("result" => false, "msg" => Yii::t("common", "You are not allowed to do this action"));
    //     return $res;
	// }

	public static function updateBlockById($objectId, $value)
    {
		PHDB::update(self::COLLECTION, ["object.id" => $objectId], [
			'$set' => [
				"persitence" => $value
			]
		]);
    }
    
	public static function useTemplate($template, $params)
    {
        $allCmsId 	   = isset($template["cmsList"]) ? $template["cmsList"] : [];
    	$tplSelectedId = (string)$template['_id'];    	
        $contextCostum = [];
        if(isset($params['newCostum'])){
            $contextCostum = [
                "contextType" => $params["parentType"],
                "contextId" => $params["parentId"],
                "app" => []
            ];
        }else{
            $contextCostum = CacheHelper::getCostum();
        }

        if (isset($params["clearCMS"])) {
        	// PHDB::remove(
        	// 	self::COLLECTION,
        	// 	[
        	// 		"parent.".$contextCostum["contextId"] => array('$exists'=>1),
        	// 		"page"  => array('$exists'=>1),
        	// 		"type"  => array('$exists'=>0)
        	// 	]);
        } 

		// save action log
        $dataSend =  array(
            "userId"         =>  Yii::app()->session["userId"],
            "userName"       =>  Yii::app()->session["user"]["name"],
            "costumId"       =>  @$params["parentId"],
            "costumSlug"     =>  @$params["parentSlug"],
            "costumType"     =>  @$params["parentType"],
            "costumEditMode" =>  @$params["costumEditMode"],
            "browser"        =>  @$_SERVER["HTTP_USER_AGENT"],
            "ipAddress"      =>  @$_SERVER["REMOTE_ADDR"],
            "created"        =>  time(),
            "action"         => "costum/addBlock",
            "blockPath"      =>  @$template["path"],
            "blockName"      =>  @$template["name"] ?? substr($template["path"], strrpos($template["path"], '.') + 1),
            "collection"     =>  @$template["collection"],
            "type"           =>  @$template["type"],
            "idBlock"        =>  $tplSelectedId,
            "subtype"        =>  @$template["subtype"]
        );
        if (isset($params["page"])) {
        	$dataSend["page"] = str_replace('#', '', $params["page"]);
        }

        /* WE HAVE 2 TYPE OF TEMPLATES. FOR WHOLE COSTUM (type site) AND DEDICATED FOR ONE "Page" */
        // DUPLICATE ALL PAGE FROM TEMPLATE AND CHANGE WHOLE COSTUM CONFIG ACCORDIND TO TEMPLATE IF IT IS A SITE
        if (!empty($template["siteParams"]) && !isset($params["customPage"])) { 
            $template["siteParams"]["app"] = array_unique(array_merge($template["siteParams"]["app"], @$contextCostum["app"]), SORT_REGULAR); 
            if (isset($params["page"])) {          	
        	// Utilisation de template Costum partiellement (use certain page)
            	if (isset($params["customPage"])) {        		
            		$where = array(
            			"page" => $params["customPage"],
            			'$or' => array()
            		);
            		foreach ($allCmsId as $index => $id) {  
            			$where['$or'][] = array("_id" => new MongoId($id));
            		}

            		$cmsToDuplicate = PHDB::find("templates", $where);
            		$allCmsId = array_keys($cmsToDuplicate);
            	}
            }
            if (!isset($params["action"]) || empty($params["action"])) { 
            // If action is empty string, it means THE TEMPLATE CHOOSED IS NEVER USED, then we duplicate all blocks
                self::duplicateBlock($allCmsId, array(
                    "parentId"   => $params['parentId'], 
                    "parentType" => $params["parentType"],
                    "parentSlug" => $params['parentSlug'],
                    "useTemplate"=>true
                ),null,false);
            // NOW, WE INCREMENT THE NUMBRE OF TEMPLATES USER
                PHDB::update(Template::COLLECTION,[ "_id" => new MongoId($tplSelectedId) ],[
                	'$set'=> [
                		"userCounter" => isset($template["userCounter"]) ? $template["userCounter"]+1 : 1
                	]
                ]);
            }elseif ($params["action"] == "using") {
                // If action is using, THE TEMPLATE CHOOSED IS USING (Bouton reinitialiser est cliqué), Then we reset the template as default. 
                // Remove the old block from each page then duplicate the template again 
                // $cmsEachPage = array();
                // $allCmsToReset = array();
                $i = 0;
                foreach (array_keys($template["siteParams"]["app"]) as $pageHash) {                 
                    PHDB::remove("cms",[
                        "parent.".$contextCostum["contextId"] => array('$exists'=>1),
                        "page" => trim($pageHash, "#")]
                    );
                    $i++;
                    if (count(array_keys($template["siteParams"]["app"])) == $i) {                                    
                        // Reduplicate the Template
                        self::duplicateBlock($allCmsId, array(
                            "parentId"   => $params['parentId'], 
                            "parentType" => $params["parentType"],
                            "parentSlug" => $params['parentSlug'],
                            "useTemplate"=>true
                        ),null,false);
                    }

                }
            }
            foreach ($template["siteParams"]["app"] as $keyPage => $Page) {
                $template["siteParams"]["app"][$keyPage]["templateOrigin"] = $tplSelectedId;
            }              
            foreach (array_keys($template["siteParams"]) as $key => $value) {
            	PHDB::update($contextCostum["contextType"],[ "_id" => new MongoId($contextCostum["contextId"]) ],[
            		'$set'=> [
            			"costum.".$value => $template["siteParams"][$value]
            		]
            	]);
                // Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.".$value, $template["siteParams"][$value]);
            }
            Costum::forceCacheReset();
        // JUST DUPLICATE THE BLOCK FROM ONE PAGE IF TEMPLATE IS DEDICATED FOR ONE PAGE 
        }else{   
            self::duplicateBlock($allCmsId, array(
                "parentId"   => $params['parentId'], 
                "parentType" => $params["parentType"],
                "parentSlug" => $params['parentSlug'],
                "page"       => trim($params["page"], "#"),
                "useTemplate"=>true),null,false);

            $dataSend["page"] =  str_replace('#', '', $params["page"]);
        }

        if ($template["type"] == "page") {
        	PHDB::update($contextCostum["contextType"],[ "_id" => new MongoId($contextCostum["contextId"]) ],[
        		'$set'=> [
        			"costum.app.".$_POST['page'].".templateOrigin" => $tplSelectedId
        		]
        	]);
            // Element:: updatepathvalue($contextCostum["contextType"],$contextCostum["contextId"],"costum.app.".$_POST['page'].".templateOrigin", $tplSelectedId);
        }

        Log::saveAndClean(
            $dataSend
        );
    }
}
?>