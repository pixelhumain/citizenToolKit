<?php
class Endorsement {
	
	const COLLECTION = "endorsements";
	const CONTROLLER = "endorsement";
	const CONTEXT = "https://w3id.org/openbadges/v2";
	public static $dataBinding = array (
        "collection" => array("name" => "collection"),
        "endorsementComment" => array("name" => "endorsementComment"),
		"issuer" => array("name" => "issuer"),
		"claimElementId" => array("name" => "claimElementId"),
		"claimElementType" => array("name" => "claimElementType"),
		"claimBadge" => array("name" => "claimBadge"),
		"modified" => array("name" => "modified"),
        "updated" => array("name" => "updated"),
        "creator" => array("name" => "creator"),
        "created" => array("name" => "created"),
    );
	public static function getEndorsementByElement($type, $id){
		return PHDB::find(Endorsement::COLLECTION, ["claimElementType" => $type, "claimElementId" => $id, "claimBadge" => ['$exists' => false]]);
	}
	public static function getEndorsementByElementByBadge($type, $id, $badge){
		return PHDB::find(Endorsement::COLLECTION, ["claimElementType" => $type, "claimElementId" => $id, "claimBadge" => $badge]);
	}
}
?>