<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;

use Badge;
use Exception;

abstract class AbstractOpenBadge {
    protected $SKIP_KEY = [
        '@context', 'context', 'mapVarClass', 'SKIP_KEY'
    ];
    public function __construct( ) {
        $parent_vars = get_class_vars(__CLASS__);
        $this->mapVarClass = array_merge($parent_vars['mapVarClass'], $this->mapVarClass);
        $this->SKIP_KEY = array_merge($parent_vars['SKIP_KEY'], $this->SKIP_KEY);
    }
    protected $mapVarClass = [
        'image' => Image::class
    ];
    public function serialize() : string
    {
        return json_encode($this->getAsAssociativeArray());
    }
    public function deserialize(string $json) : void
    {
        $json = preg_replace('/\s+/', '', $json);
        $values = json_decode($json, true);
        $this->setFromAssociativeArray($values);
    }
    public function setFromAssociativeArray($array)
    {
        $var_names = array_keys(get_class_vars(get_class($this)));
        foreach ($var_names as $name) {
            if(in_array($name, $this->SKIP_KEY)){
                continue;
            }
            if(isset($array[$name])){
                if(is_array($array[$name]) && key_exists($name, $this->mapVarClass)){
                    if(is_array($this->mapVarClass[$name])){
                        $arrayRes = [];
                        foreach ($array[$name] as $key => $value) {
                            $tmp = new $this->mapVarClass[$name][0]();
                            $tmp->setFromAssociativeArray($value);
                            $arrayRes[$key] = $tmp;
                        }
                        $this->$name = $arrayRes;
                    }else{
                        $this->$name = new $this->mapVarClass[$name]();
                        $this->$name->setFromAssociativeArray($array[$name]);
                    }
                }else{
                    $this->$name = $array[$name];
                }
            }
        }
    }
    public function getAsAssociativeArray()
    {
        $array = [];
        $var_names = array_keys(get_class_vars(get_class($this)));
        foreach ($var_names as $name) {
            if(in_array($name, $this->SKIP_KEY)){
                continue;
            }
            $value = $this->getItemArray($this->$name, $name);
            if(isset($value)){
                $array[$name] = $value;
            }
        }
        if(in_array("context", $var_names)){
            $array['@context'] = $this->context;
            unset($array['context']);
        }
        return $array;
    }
    private function getItemArray($item){
        if(isset($item)){
            if($item instanceof AbstractOpenBadge){
                return $item->getAsAssociativeArray();
            }else if(is_array($item)){
                $array = [];
                foreach($item as $key => $value){
                    $array[$key] = $this->getItemArray($value);
                } 
                return $array;
            }else{
                return $item;
            }
        }
        return NULL;
    }
    public function checkContext()
    {
        if($this->context != Badge::CONTEXT){
            throw new Exception("Cannot support this version of OPEN BADGES");
        }
    }
}