<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;
class EndorsementClass extends AbstractOpenBadge{
    public $id;
    public $type;
    public $claim;
    public $issuer;
    public $issuedOn;
    public $verification;
    public $revoked;
    public $revocationReason;
    public $endorsementComment;
}