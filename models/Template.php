<?php

class Template {
	const COLLECTION = "templates";
	const CONTROLLER = "template";
	const MODULE = "template";
	const ICON = "fa-file";

	//From Post/Form name to database field name
	public static $dataBinding = array (
	    "id" => array("name" => "id"),
	    "section" => array("name" => "section"),
	    "type" => array("name" => "type"),
	    "subtype" => array("name" => "subtype"),
	    "category" => array("name" => "category"),
	    "collection" => array("name" => "collection"),
	    "templates" => array("name" => "templates"),
	    "name" => array("name" => "name"),
	    "address" => array("name" => "address", "rules" => array("addressValid")),
	    "addresses" => array("name" => "addresses"),
	    "streetAddress" => array("name" => "address.streetAddress"),
	    "postalCode" => array("name" => "address.postalCode"),
	    "city" => array("name" => "address.codeInsee"),
	    "addressLocality" => array("name" => "address.addressLocality"),
	    "addressCountry" => array("name" => "address.addressCountry"),
	    "preferences" => array("name" => "preferences"),
	    "geo" => array("name" => "geo"),
	    "geoPosition" => array("name" => "geoPosition"),
	    "description" => array("name" => "description"),
	    "parent" => array("name" => "parent"),
	    "parentId" => array("name" => "parentId"),
	    "parentType" => array("name" => "parentType"),
	    "position" => array("name" => "position"),
	    "media" => array("name" => "media"),
	    "urls" => array("name" => "urls"),
	    "medias" => array("name" => "medias"),
	    "tags" => array("name" => "tags"),
	    "structags" => array("name" => "structags"),
	    "level" => array("name" => "level"),
	    "shortDescription" => array("name" => "shortDescription"),
	    "rank" => array("name" => "rank"),
	    "blocTplId" => array("name" => "blocTplId"),
	 	"path" => array("name" => "path"),
	 	"haveTpl" => array("name" => "haveTpl"),
	    "page" => array("name" => "page"),	
	    "allPage" => array("name" => "choosePage"),	
	    "gitlab" => array("name" => "gitlab"),	

	    "tplParent" => array("name" => "tplParent"),
	    "templateParent" => array("name" => "templateParent"),
	    "screenshot" => array("name" => "screenshot"),
	    "order" => array("name" => "order"),

	    "tplsUser" => array("name" => "tplsUser"),	
		"cmsList" => array("name" => "cmsList"),
	    "modified" => array("name" => "modified"),
	    "source" => array("name" => "source"),
	    "updated" => array("name" => "updated"),
	    "creator" => array("name" => "creator"),
	    "created" => array("name" => "created"),
	    "color"	=> array("name" => "color"),
	    "siteParams"	=> array("name" => "siteParams"),
	    "dontRender"	=> array("name" => "dontRender")
	);

	/**
	 * get a TEMPLATE By Id
	 * @param String $id : is the mongoId of the template
	 * @return template
	 */
	public static function getById($id) { 
		
	  	$template = PHDB::findOneById( self::COLLECTION ,$id );
	  	
	  	// Use case notragora
	  	$where=array("id"=>@$id, "type"=>self::COLLECTION, "doctype"=>"image");
	  	$template["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
	  	$where["doctype"]="file";
	  	$template["files"] = Document::getListDocumentsWhere($where,"file");
	  	return $template;
	}
}
?>