<?php
class ActivityStr
{

    // activity
    const ACCEPT_ACTIVITY = "Accept";
    // FOLLOW
    const ACCEPT_FOLLOW_ACTIVITY= "AcceptFollow";
    const ADD_ACTIVITY = "Add";
    const ANNOUNCE_ACTIVITY = "Announce";
    const ARRIVE_ACTIVITY = "Arrive";
    const BLOCK_ACTIVITY = "Block";
    const CREATE_ACTIVITY = "Create";
    const DELETE_ACTIVITY = "Delete";
    const DISLIKE_ACTIVITY = "Dislike";
    const FLAG_ACTIVITY = "Flag";
    const FOLLOW_ACTIVITY = "Follow";
    const IGNORE_ACTIVITY = "Ignore";
    const JOIN_ACTIVITY = "Join";
    const LEAVE_ACTIVITY = "Leave";
    const LIKE_ACTIVITY = "Like";
    const LISTEN_ACTIVITY = "Listen";
    const MOVE_ACTIVITY = "Move";
    const OFFER_ACTIVITY = "Offer";
    const PROJECT_ACTIVITY = "Project";
    const QUESTION_ACTIVITY = "Question";
    const READ_ACTIVITY = "Read";
    // Reject
    const REJECT_ACTIVITY = "Reject";

    const REJECT_FOLLOW_ACTIVITY = 'RejectFollow';

    const REMOVE_ACTIVITY = "Remove";

    const REMOVE_FOLLOW_ACTIVITY = "RemoveFollow";
    const TENTATIVE_ACCEPT_ACTIVITY = "TentativeAccept";
    const TENTATIVE_REJECT_ACTIVITY = "TentativeReject";
    const TRAVEL_ACTIVITY = "Travel";
    const UNDO_ACTIVITY = "Undo";
    // Follow 
    const UNDO_FOLLOW_ACTIVITY = "UndoFollowActivity";
    const UPDATE_ACTIVITY = "Update";
    const VIEW_ACTIVITY = "View";
    // actor 
    const APPLICATION_ACTOR = "Application";
    const GROUP_ACTOR = "Group";
    const ORGANIZATION_ACTOR = "Organization";
    const PERSON_ACTOR = "Person";
    const PROJECT_ACTOR = "Project";
    const SERVICE_ACTOR = "Service";
    // object
    const ARTICLE_OBJECT = "Article";
    const AUDIO_OBJECT = "Audio";
    const DOCUMENT_OBJECT = "Document";
    const EVENT_OBJECT = "Event";
    const IMAGE_OBJECT = "Image";
    const MENTION_OBJECT = "Mention";
    const NOTE_OBJECT = "Note";
    const PAGE_OBJECT = "Page";
    const PLACE_OBJECT = "Place";
    const PROFILE_OBJECT = "Profile";
    const PROJECT_OBJECT = "Project";
    const RELATIONSHIP_OBJECT = "Relationship";
    const TOMBSTONE_OBJECT = "Tombstone";
    const VIDEO_OBJECT = "Video";

    const INVITE_BECOME_ADMIN_PROJECT = 'InviteToBecomeAdminProject';
    const INVITE_FOLLOW_ACTIVITY = "InviteFollow";
    const INVITE_REJECT_ACTIVITY = "InviteReject";
    const UNDO_FOLLOW_PROJECT_ACTIVITY = "UndoFollowProject";

}
