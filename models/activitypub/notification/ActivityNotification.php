<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification;
use Project;
use ActivityStream;
use ActStr;
use Event;
use Badge;
use Preference;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PHDB;
use Person;
use News;
use Yii;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
class ActivityNotification
{
    private function getLocalUrlFromObjectId($object): string
    {
        $type =  Person::COLLECTION;
        if($object->get('type') == 'Event'){
            $type = Event::COLLECTION;
        }else if($object->get('type') == 'Project'){
            $type = Project::COLLECTION;
        }else if($object->get('type') == 'Note'){
            $type = News::COLLECTION;
        } else if($object->get('type') == 'Badge'){
            $type = Badge::COLLECTION;
        }
        else {
            if(!empty($object->get('target'))){
                $target = Type::createFromAnyValue($object->get('target'));
               if($target->get('type') == "Project"){
                   $type =  Project::COLLECTION;
               }
            }
        }
        if(!empty($object->get('target'))){
            $target = Type::createFromAnyValue($object->get('target'));
            $element = PHDB::findOne($type, ["objectId" => $target->get("id")]);
        }else{
            $element = PHDB::findOne($type, ["objectId" => $object->get("id")]);
        }

        if(!$element){
            $url = "";
        }else{
            if($type == Event::COLLECTION){
                $url = "agenda";
            }else{
                $url = "page.type.".$type.".id.".$element['_id'];
            }

        }
        return $url;
    }
    public static function send($activity, $actor,$targets = null)
    {
        $activityArray = $activity->toArray();
        $object = Type::createFromAnyValue($activityArray['object']);
        if(!$object || !is_object($object)) return false;
        $notificationTranformer = NotificationTransformer::getActivityType($activityArray);
        if(!$notificationTranformer || is_object($notificationTranformer)) return false;
        $asParam = array(
            "type" => "activitypub",
            "author" =>   array(
                'id' => $actor->get('id'),
                'type' => $actor->get('type'),
                'name' => $actor->get('name'),
                'profilThumbImageUrl' => $actor->get('profilThumbImageUrl')
            ),
            "verb" =>$notificationTranformer,
            "object" => array(
                "type" => $notificationTranformer,
                "id"   => $object->get('id')
            ),
        );
        $actionMsg = $activityArray["summary"] ?? "Activité inconnue ...";
        $usersToNotify  = [];
        if(isset($targets) && count($targets) > 0 ){
            foreach($targets as $target){
                $user = PHDB::findOne(Person::COLLECTION, ["username" => $target->get("preferredUsername")]);
                if($user && isset($user["preferences"]) && Preference::isActivitypubActivate($user["preferences"]))
                    $usersToNotify[] = $user;
            }
        }
        if(isset($activityArray["cc"]) && count($activityArray['cc']) > 0){
            foreach ($activityArray['cc'] as $cc){
                if (Utils::getWebsiteDomain($cc) == Config::HOST()){
                    $usersToNotify[]= PHDB::findOne(Person::COLLECTION, array('username' => Utils::getLinkToUsername($cc)));
                }
            }
        }
        if($object->get('type') == "Follow" || $object->get('type') == "Accept"){
            $usersToNotify[]= PHDB::findOne(Person::COLLECTION, array('username' => Utils::getLinkToUsername($object->get('actor') )));
        }
        $processedIds = [];
        foreach($usersToNotify as $user){
            $userId = (string) $user['_id'];
            if (!in_array($userId, $processedIds)) {
                $asParam['id'] = (string) $user['_id'];
                $stream = ActStr::buildEntry($asParam);
                $stream["notify"] = ActivityStream::addNotification(array(
                    "persons" => array( (string)$user['_id'] => array(
                        "isUnread" => true,
                        "isUnseen" => true
                    )),
                    "label"   => $actionMsg,
                    "icon"    => Utils::getWebsiteFavicon($object->get('id')),
                    "url"     => (new ActivityNotification)->getLocalUrlFromObjectId($object)
                ));
                ActivityStream::addEntry($stream);
                $processedIds[] = $userId;
            }
        }
    }
}
