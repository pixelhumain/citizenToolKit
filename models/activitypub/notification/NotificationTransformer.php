<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification;

use ActivityStr;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use Yii;

class NotificationTransformer
{
    public static function getActorName($activity)
    {
        if (isset($activity['actor'])) {
            $actor = $activity['actor'];
            if (is_string($actor)) {
                return $actor;
            } elseif (is_array($actor) && isset($actor['name'])) {
                return $actor['name'];
            }
        }

        return '';
    }

    public static function getActivityType($activity)
    {
        if (isset($activity['type'])) {
            if (filter_var($activity['type'], FILTER_VALIDATE_URL)) {
                $atv = Type::createFromAnyValue($activity['type']);
                return $atv->get('type');
            } else {
                return $activity['type'];
            }
        }

        return '';
    }
    public static function getObjectType($activity)
    {
        if (isset($activity['object'])) {
            $object = $activity['object'];

            if (is_string($object)) {
                if (filter_var($object, FILTER_VALIDATE_URL)) {
                    $obj = Type::createFromAnyValue($object);
                    if($obj!= null && is_object($obj)) {
                        return $obj->get('type');
                    }else{
                        return null;
                    }
                } else {
                    return $object;
                }
            } elseif (is_array($object) && isset($object['type'])) {
                return $object['type'];
            }else{
                return null;
            }
        }

        return '';
    }
    public static function getObjectName($activity)
    {
        if (isset($activity['object'])) {
            $object = $activity['object'];
            if (is_string($object)) {
                return $object;
            } elseif (is_array($object) && isset($object['name'])) {
                return $object['name'];
            }
        }

        return '';
    }
    public static function getObjectId($activity)
    {
        if (isset($activity['object'])) {
            $object = $activity['object'];
            if (is_string($object)) {
                return $object;
            } elseif (is_array($object) && isset($object['id'])) {
                return $object['id'];
            }
        }

        return '';
    }
}
