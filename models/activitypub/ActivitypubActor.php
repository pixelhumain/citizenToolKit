<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub;

use Exception;
use MongoId;
use Person as GlobalPerson;
use Organization;
use PHDB;
use PhpParser\Node\Expr\Cast\Object_;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Webfinger;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use Project;

class ActivitypubActor
{
    public static function getLocalFollowersOfExternalUser($actorId)
    {
        $followers = [];
        try {
            $users = PHDB::find(GlobalPerson::COLLECTION, [
                "links.activitypub.follows" => [
                    '$elemMatch' => [
                        "invitorId" => $actorId
                    ]
                ]
            ]);
            foreach ($users as $user) {
                $followers[] = ActivitypubTranslator::coPersonToActor($user);
            }
        } catch (Exception $e) {
        }

        return $followers;
    }

    public static function getCoPersonAsActor($username): AbstractActor
    {
        $user = PHDB::findOne(GlobalPerson::COLLECTION, ["username" => $username]);
        if (!$user)
            throw new Exception("User not found");
        return ActivitypubTranslator::coPersonToActor($user);
    }

    public static function searchActorByRessourceAddress($address): AbstractActor
    {
        $parts = Webfinger::parseResource($address);

        $url = "https://" . $parts["host"] . "/.well-known/webfinger?resource=acct:" . $parts["user"] . "@" . $parts["host"];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Suivre les redirections
        $res = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($res, true);
        if (!$res || !isset($res["links"]))
            throw new Exception("Remote data could not be fetch");
        $data = null;
        foreach ($res["links"] as $link) {
            if ($link["rel"] == "self") {
                $data = Type::createFromAnyValue($link["href"]);
                break;
            }
        }
        if (!$data)
            throw new Exception("Remote data could not be fetch");
        return $data;
    }
    public static function getCoGroupAsActor($slug): AbstractActor
    {
        //  $group = PHDB::findOne(Organization::COLLECTION, ["slug"=>$slug,"type" => "Group"]);
        $group = PHDB::findOne(Organization::COLLECTION, ["slug" => $slug]);

        if (!$group)
            throw new Exception("Group not found");
        return ActivitypubTranslator::coGroupToActor($group);
    }
    public static function getCoProjectAsObject($slug): AbstractActor
    {

        $project = PHDB::findOne(Project::COLLECTION, ["slug" => $slug]);

        if (!$project) {
            throw new Exception("Project not found");
        }
        return ActivitypubTranslator::coProjectAsActor($project);
    }
    public static function getCoPersonAsActorByUserId($param): AbstractActor
    {
        $user = PHDB::findOne(
            GlobalPerson::COLLECTION,
            [
                "_id" => new MongoId($param)
            ]
        );
        if (!$user)
            throw new Exception("User not found");
        return ActivitypubTranslator::coPersonToActor($user);
    }
}
