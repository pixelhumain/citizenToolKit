<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\schemas;
use DateTime;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Accept;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Delete;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Reject;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Undo;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Yii;
use Person;
use MongoId;
use Badge as LocalBadge;
use PH;

class Badge extends AbstractElement{
    private string $collection = LocalBadge::COLLECTION;

    public function __construct(){
        parent::__construct();
    }
    public final function create($params) {
        //  notification
        $summary = Yii::t(
            "activitypub",
            "{who} has create new badge :  '{what}'",
            array(
                "{who}" => $this->actor->get("name"),
                "{what}" => $params['name']
            )
        );
        $badgeParams = $this->getDefaultObjectAttributes();
        $instrument = null;
        if(isset($params['name'])){
            $badgeParams["name"] = $params['name'];
        }
        if(isset($params['description'])){
            $badgeParams["content"] = $params['description'];
        }
        /** if(isset($params['isParcours'])){
        $badgeParams["isParcours"] = $params['isParcours'];
        }**/
        if(isset($params['icon'])){
            $badgeParams['icon'] = $params['icon'];
        }
        if (isset($params['tags'])) {
            $badgeParams['tag']= Utils::coTagWithTypeToActivitypubTag($params['tags']);
        }
        if(isset($params['criteria'])){
            $badgeParams['criteria'] = $params['criteria'];
        }
        $badge = Type::create("Badge",$badgeParams);
        return Type::create("Create", [
            "actor" => $this->actor->get("id"),
            "object" => $badge,
            "attributedTo" => $this->actor->get("id"),
            "published" => (new \DateTime())->format(DATE_ISO8601),
            "summary" => $summary,
            "to" => $badge->get("to"),
            "cc" => $badge->get("cc")
        ]);

    }

    public final function update($params){
        $badge = PHDB::findOneById($this->collection, $params['id']);
        $object = Type::createFromAnyValue($badge["objectId"]);
        if (!$object)
            return null;
        $badgeParams = $this->getDefaultObjectAttributes();
        $updatedObject = $this->multipleUpdateKey($object, $params);

        $activity = Type::create("Update", $badgeParams);
        $activity->set("object", $updatedObject['object']);
        $activity->set("actor", $this->actor->get("id"));
        $activity->set("cc", $object->get("cc"));
        $summary = Yii::t(
            "activitypub",
            "{who} has updated info {what} of badge {where}",
            array(
                "{who}" => $this->actor->get("name"),
                "{what}" => implode(',', $updatedObject['attributeUpdated']),
                "{where}" => $badge['name']
            )
        );
        $activity->set("summary", $summary);
        return $activity;
    }
    public final function delete($id){
        $res = PHDB::findOneById($this->collection, $id);
        if(!isset($res["objectId"])) return null;
        $object = Type::createFromAnyValue($res["objectId"]);
        if (!$object)
            return null;
        $activity = Type::create("Delete", [
            "actor" => $this->actor->get("id"),
            "to" => $object->get("to"),
            "cc" => $object->get("cc"),
            "object" => [
                "id" => $object->get("id"),
                "type" => "Tombstone"
            ],
            "published" => (new DateTime())->format(DATE_ISO8601)
        ]);
        $summary = Yii::t(
            "activitypub",
            "{who} has delete note named {what}",
            array(
                "{who}" => $this->actor->get("id"),
                "{what}" => $object->get('name')
            )
        );
        $activity->set("summary", $summary);
        return $activity;
    }
    public  function assign($params){
        $object = Type::createFromAnyValue($params['objectId']);
        if (!$object) return null;
        $award = Type::createFromAnyValue($params['awardId']);
        $instrument = null;
        if(isset($params['narrative'])) $instrument['narrative'] = $params['narrative'];
        if(isset($params['evidences'])) $instrument['evidences'] = $params['evidences'];
        if(isset($params['expiredOn'])) $instrument['expiredOn'] = $params['expiredOn'];
        if(isset($params['attenteEmetteur'])) $instrument['attenteEmetteur'] = $params['attenteEmetteur'];
        if(isset($params['attenteRecepteur'])) $instrument['attenteRecepteur'] = $params['attenteRecepteur'];
        $cc = $object->get("cc");
        if(!Utils::isInSameDomain($params[ 'objectId' ], $this->actor->get('id'))){
            $cc[] = $object->get("attributedTo");
            for ( $i = 0; $i < count($cc); $i++ ) {
                if($cc[$i] == $this->actor->get('id')){
                    unset($cc[$i]);
                }
            }
        }
        $activity = Type::create("Offer", [
            "actor" => $this->actor->get("id"),
            "to" => $object->get("to"),
            "cc" => $cc,
            "target" =>  $award->get('id'),
            "object" => $object->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601)
        ]);
        if($instrument!==null) $activity->set('instrument',$instrument);
        $summary = Yii::t("activitypub", "{who} assign badge {what} to", array(
            "{who}" => $award->get('preferredUsername'),
            "{what}" =>  $object->get('name')
        ));
        $activity->set("summary", $summary);

        return $activity;
    }
    public  function confirm($params)
    {
        $activity = ActivitypubActivity::getByUUID($params['activityId']);
        $object = Type::createFromAnyValue($activity->get("object"));
        $acceptActivity = new Accept();
        $acceptActivity->setObject($activity->get("id"));
        $acceptActivity->setActor($this->actor->get('id'));
        $cc = $this->getFollowersConditional($activity);
        $acceptActivity->setCc($cc);
        $acceptActivity->setInstrument(array('confirmType' => $params['confirmType']));
        $summary = Yii::t(
            "activitypub",
            "{who} has confirmed your badge",
            array(
                "{who}" => $activity->get("actor")
            )
        );
        $acceptActivity->setSummary($summary);
        return $acceptActivity;
    }
    public  function revoke($params){
        $activity = ActivitypubActivity::getByUUID($params['activityId']);

        $rejectActivity = new Reject();
        $rejectActivity->setObject($activity->get("id"));
        $rejectActivity->setActor($this->actor->get("id"));
        $cc = $this->getFollowersConditional($activity);
        $rejectActivity->setCc($cc);
        $rejectActivity->setTo([Utils::PUBLIC_INBOX]);
        $instrument=null;
        if(isset($params['revokeAuthor'])) $instrument['revokeAuthor'] = $params['revokeAuthor'];
        if(isset($params['revokeReason'])) $instrument['revokeReason'] = $params['revokeReason'];

        $summary = Yii::t(
            "activitypub",
            "{who} has reject your badge",
            array(
                "{who}" => $this->actor->get("id")
            )
        );
        if($instrument!==null) $rejectActivity->set('instrument',$instrument);
        $rejectActivity->setSummary($summary);
        return $rejectActivity;
    }

    public function refused($params)
    {
        $activity = ActivitypubActivity::getByUUID($params['activityId']);
        $refusedActivity = new Delete();
        $refusedActivity->setObject($activity->get("id"));
        $refusedActivity->setActor($this->actor->get("id"));
        $cc= $this->getFollowersConditional($activity);
        $refusedActivity->setCc($cc);
        $refusedActivity->setTo([Utils::PUBLIC_INBOX]);
        $summary = Yii::t(
            "activitypub",
            "{who} has refused your badge",
            array(
                "{who}" => $this->actor->get("id")
            )
        );
        $refusedActivity->setSummary($summary);
        return $refusedActivity;
    }

    public function reinit($params)
    {
        $activity = ActivitypubActivity::getByUUID($params['activityId']);
        $reinitActivity = new Undo();
        $reinitActivity->setObject($activity->get("id"));
        $reinitActivity->setActor($this->actor->get("id"));
        $cc = $this->getFollowers();
        $reinitActivity->setCc($cc);
        $reinitActivity->setTo([Utils::PUBLIC_INBOX]);
        $summary = Yii::t(
            "activitypub",
            "{who} has reinit your badge",
            array(
                "{who}" => $this->actor->get("id")
            )
        );
        $reinitActivity->setSummary($summary);
        return $reinitActivity;
    }

    private function getFollowersConditional($activity)
    {
        $object = Type::createFromAnyValue($activity->get("object"));
        if($this->actor->get("id") == $object->get("attributedTo")){
            PHDB::insert('dump_ap',array('entity' =>  'eto angaha'));
            return $this->getFollowers();
        } else {

            $cc = $activity->get("cc");

            if(!Utils::isInSameDomain($object->get('id'), $this->actor->get('id'))){
                $cc[] = $activity->get("attributedTo");
                for ( $i = 0; $i < count($cc); $i++ ) {
                    if($cc[ $i ] == $this->actor->get('id')){
                        unset($cc[ $i ]);
                    }
                }
            }
            return $cc;
        }

    }
}
