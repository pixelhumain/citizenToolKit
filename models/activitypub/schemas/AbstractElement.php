<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\schemas;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Person;
abstract class AbstractElement
{
    private $user;
    protected $actor;
    public function __construct(){
        $this->user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $this->actor = ActivitypubTranslator::coPersonToActor($this->user);
    }
    public final function getFollowers():array{
        $followers = [];
        if (isset($this->user["links"]["activitypub"]["followers"]) && sizeof($this->user["links"]["activitypub"]["followers"]) > 0) {
            foreach ($this->user["links"]["activitypub"]["followers"] as $follower) {
                $followers[] = $follower["invitorId"];
            }
        }
        return $followers;
    }
    public final function getDefaultObjectAttributes(){
        $uuid = Utils::genUuid();
        return [
            "uuid" => $uuid,
            "actor" => $this->actor->get("id"),
            "attributedTo" => $this->actor->get('id'),
            "published" => (new \DateTime())->format(DATE_ISO8601),
            "cc" => $this->getFollowers(),
            "to" => [Utils::PUBLIC_INBOX]
        ];
    }
    public final function multipleSetKey($params):array{
        $data = [];
        foreach ($params as $key => $item) {
            $data[$key] = $item;
        }
        return $data;
    }
    public final function multipleUpdateKey($object,$params):array{
        unset($params['id']);
        $attributeUpdated = array();
        foreach ($params as $key => $item) {
            $object->set($key,$item);
            $attributeUpdated[] = $key;
        }
        return array('object' => $object, 'attributeUpdated' => $attributeUpdated);
    }
    abstract protected function create($params);
    abstract protected function update($params);
    abstract protected function delete($id);
}
