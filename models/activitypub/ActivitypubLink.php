<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub;

use Badge;
use Person;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use Project;


class ActivitypubLink
{
    // const for actor
    const GROUP =  'group';
    const PERSON = 'person';
    const PROJECT = 'project';
    const BADGE = 'badge';
    // constant for project
    const CONTRIBUTION = 'contributor';
    const ADMIN = 'admin';
    const ADD_AS_ADMIN = 'asadmin';
    const ADD_WITH_RULES = 'withRules';
    const BECOME_ADMIN = 'becomeadmin';
    const ADMIN_AND_ROLE = 'adminandrole';
    const INVITE_WITH_RULES = 'inviteWithRules';
    const CONTRIBUTOR = 'contributor';
    const FOLLOW = 'follow';

    //save link
    public static function saveLink($link,  $subject,  AbstractActor $actor, $activityId, $payload = null, $pending = true)
    {
        if ($payload["type"] == self::GROUP) {
            $collection = Organization::COLLECTION;
            $query = ["slug" => $payload["name"]];
        } else {
            $collection = Person::COLLECTION;
            $query = ["username" => $payload["name"]];
        }

        if (self::isLinkAlreadyExist($link, $subject, $actor, $payload))
            return false;
        $id = uniqid();
        $data = [
            "activityId" => $activityId,
            "invitorId" => $actor->get("id"),
            "invitorAdress" => ActivitypubTranslator::getActorAdress($actor),
            "invitorName" => $actor->get("name"),
            "invitorUsername" => $actor->get("preferredUsername")
        ];

        if ($pending)
            $data["pending"] = true;

        PHDB::update($collection, $query, [
            '$push' => [
                'links.activitypub.' . $link => $data
            ]
        ]);

        return $id;
    }

    //accept follow link
    public static function approveLink($link, $username, AbstractActor $invitor, $actorEntity)
    {
        $collection = ($actorEntity['type'] == "group") ? Organization::COLLECTION : Person::COLLECTION;
        $data = $actorEntity['type'] == "group" ? [
            "slug" => $actorEntity['name'],
            "links.activitypub." . $link . ".invitorId" => $invitor->get("id")
        ] : [
            "username" => $username,
            "links.activitypub." . $link . ".invitorId" => $invitor->get("id")
        ];
        if(self::isElementExist($collection,$data)){
            PHDB::update(
                $collection,
                $data,
                [
                    '$unset' => [
                        'links.activitypub.' . $link . ".$.pending" => ""
                    ]
                ]
            );
        }
    }
    public static function isElementExist($collection,$query){
        $entity = PHDB::findOne($collection, $query);
        return boolval($entity);
    }
    //check if link is already exist
    public static function isLinkAlreadyExist($link, AbstractActor $subject, AbstractActor  $actor, $actorEntity)
    {
        $collection = Person::COLLECTION;
        $query = [
            'username' => $subject->get("preferredUsername"),
            'links.activitypub.' . $link =>
            [
                '$elemMatch' => [
                    "invitorId" => $actor->get("id")
                ]
            ]
        ];
        switch ($actorEntity['type']) {
            case self::PROJECT:
                $collection = Project::COLLECTION;
                $query = [
                    'objectId' => $subject->get("id"),
                    'links.activitypub.' . $link => [
                        '$elemMatch' => [
                            "invitorId" => $actor->get("id")
                        ]
                    ]
                ];
                break;
            case self::GROUP:
                $collection = Person::COLLECTION;
                $query = [
                    'slug' => $subject->get("preferredUsername"),
                    'links.activitypub.' . $link => [
                        '$elemMatch' => [
                            "invitorId" => $actor->get("id")
                        ]
                    ]
                ];
                break;
            case self::BADGE:
                $collection = Badge::COLLECTION;
                $query = [
                    'slug' => $subject->get("preferredUsername"),
                    'links.activitypub.' . $link => [
                        '$elemMatch' => [
                            "invitorId" => $actor->get("id")
                        ]
                    ]
                ];
                break;
        }
        $entity = PHDB::findOne($collection, $query);
        return boolval($entity);
    }

    public static function deleteLink($link, $username, AbstractActor $invitor, $actorEntity)
    {
        $collection = $actorEntity['type'] == "group" ? Organization::COLLECTION : Person::COLLECTION;
        $data = $actorEntity['type'] == "group" ? [
            "slug" => $username
        ] : [
            "username" => $username
        ];
        if(self::isElementExist($collection,$data)){
            PHDB::update(
                $collection,
                $data,
                [
                    '$pull' => [
                        'links.activitypub.' . $link => [
                            "invitorId" => $invitor->get("id")
                        ]
                    ]
                ]
            );

        }
    }

    public static function pendingLink($link, $username, AbstractActor $invitor, $actorEntity)
    {

        $collection = $actorEntity['type'] == "group" ? Organization::COLLECTION : Person::COLLECTION;
        $data = $actorEntity['type'] == "group" ? [
            "slug" => $actorEntity['name'],
            "links.activitypub." . $link . ".invitorId" => $invitor->get("id")
        ] : [
            "username" => $username,
            "links.activitypub." . $link . ".invitorId" => $invitor->get("id")
        ];

        if(self::isElementExist($collection,$data)){
            PHDB::update(
                $collection,
                $data,
                [
                    '$set' => [
                        'links.activitypub.' . $link . '.$.pending' => true
                    ]
                ]
            );
        }
    }

      public static function saveLinkProject($link, $subject, AbstractActor $actor, $activityId, $payload = null, $pending = true)
    {
        $id = uniqid();
        $data = [
            "activityId" => $activityId,
            "invitorId" => $actor->get("id"),
            "invitorAdress" => ActivitypubTranslator::getActorAdress($actor),
            "invitorName" => $actor->get("name"),
            "invitorUsername" => $actor->get("preferredUsername")
        ];
         $data["isAdmin"] = true;
        if (self::isLinkAlreadyExist($link, $subject, $actor, array('type' => 'project'))) {
            PHDB::update(
                Project::COLLECTION,
                [
                    'objectId' => $subject->get('id'),
                    "links.activitypub." . $link . ".invitorId" => $actor->get("id")
                ],
                [
                    '$set' => [
                        'links.activitypub.' . $link . '.$' => $data
                    ]
                ]
            );
        } else {

            if(!empty($subject->get('id')) && self::isElementExist(Project::COLLECTION,['objectId' => $subject->get('id')])){
                PHDB::update(Project::COLLECTION, ['objectId' => $subject->get('id')], [
                    '$push' => [
                        'links.activitypub.' . $link => $data
                    ]
                ]);
            }
        }


        return $id;
    }
    public static function deleteFollowersProject($objectId, AbstractActor $invitor)
    {
        if(isset($objectId) && self::isElementExist(Project::COLLECTION,["objectId" => $objectId])){
            PHDB::update(
                Project::COLLECTION,
                ["objectId" => $objectId],
                [
                    '$pull' => [
                        'links.activitypub.followers' => [
                            "invitorId" => $invitor->get("id")
                        ]
                    ]
                ]
            );
        }
    }

    public static function deleteProject($link, $subject, AbstractActor $invitor)
    {
        if(!empty($subject->get('id')) && self::isElementExist(Project::COLLECTION,["objectId" => $subject->get('id')])){
            PHDB::update(
                Project::COLLECTION,
                ["objectId" => $subject->get('id')],
                [
                    '$pull' => [
                        'links.activitypub.' . $link => [
                            "invitorId" => $invitor->get("id")
                        ]
                    ]
                ]
            );
        }
    }

    public static function saveLinkForProject($link, $subject, AbstractActor $actor, $activityId, $instrument = null)
    {
        $id = uniqid();
        $data = [
            "activityId" => $activityId,
            "invitorId" => $actor->get("id"),
            "invitorAdress" => ActivitypubTranslator::getActorAdress($actor),
            "invitorName" => $actor->get("name"),
            "invitorUsername" => $actor->get("preferredUsername")
        ];
        if($instrument != 'follow' || $instrument != 'unfollow' ){
            self::deleteFollowersProject($subject->get('id'),$actor);
        }else{
            self::deleteProjectLink('followers',$subject->get('id'),$actor);
        }
        if (is_string($instrument)) {
            switch ($instrument) {
                case self::CONTRIBUTION:
                    $data["toBeValidated"] = true;
                    break;
                case self::ADMIN:
                    $data["isAdmin"] = true;
                    $data["isAdminPending"] = true;
                    $data["toBeValidated"] = true;
                    break;
                case self::BECOME_ADMIN:
                    $data["isAdmin"] = true;
                    $data["isInviting"] = true;
                    break;
                case self::ADD_AS_ADMIN:
                    $data["isAdmin"] = true;
                    break;

            }
        } else {
            switch ($instrument['as']) {
                case self::ADMIN_AND_ROLE:
                    $data["isAdmin"] = true;
                    $data["roles"] = explode(',', $instrument['rules']);
                    $data["isInviting"] = true;
                    break;
                case self::INVITE_WITH_RULES:
                    $data["roles"] = explode(',', $instrument['rules']);
                    $data["isInviting"] = true;
                    break;
                case self::CONTRIBUTOR:
                    $data["isInviting"] = true;
                    break;
                case self::ADD_WITH_RULES:
                    $data["roles"] = explode(',', $instrument['rules']);
                    break;
            }
        }
        if (self::isLinkAlreadyExist($link, $subject, $actor, array('type' => 'project'))) {
            PHDB::update(
                Project::COLLECTION,
                [
                    'objectId' => $subject->get('id'),
                    "links.activitypub." . $link . ".invitorId" => $actor->get("id")
                ],
                [
                    '$set' => [
                        'links.activitypub.' . $link . '.$' => $data
                    ]
                ]
            );
        } else {
            PHDB::update(Project::COLLECTION, ['objectId' => $subject->get('id')], [
                '$push' => [
                    'links.activitypub.' . $link => $data
                ]
            ]);
        }


        return $id;
    }
    public static function acceptProjectLink($link, $subject,  $actor, $activityId, $instrument = null)
    {
        $query = [
            'objectId' => $subject->get('id'),
            "links.activitypub." . $link . ".invitorId" => $actor->get("id")
        ];
        $project = PHDB::findOne(Project::COLLECTION, $query);
         
        $links = $project["links"]["activitypub"][$link];

        // recuperer les attributs de links
        $data = array_reduce($links, function ($carry, $v) use ($actor) {
            if ($carry === null && $v["invitorId"] == $actor->get("id")) {
                $carry = $v;
            }
            return $carry;
        }, null);
        $data["activityId"] = $activityId;
        if (is_string($instrument)) {
            switch ($instrument) {
                case self::CONTRIBUTION:
                    unset($data["toBeValidated"]);
                    break;
                case self::ADMIN:
                    $data["isAdmin"] = true;
                    unset($data["isAdminPending"]);
                    unset($data["toBeValidated"]);
                    break;
                case self::BECOME_ADMIN:
                    $data["isAdmin"] = true;
                    unset($data["toBeValidated"]);
                    break;
                case self::ADD_AS_ADMIN:
                    $data["isAdmin"] = true;
                    break;
            }
        } else {
            switch ($instrument['as']) {
                case self::ADMIN_AND_ROLE:
                    $data["isAdmin"] = true;
                    $data["roles"] = explode(',', $instrument['rules']);
                    unset($data["isInviting"]);
                    break;
                case self::INVITE_WITH_RULES:
                    $data["roles"] =explode(',', $instrument['rules']);
                    unset($data["isInviting"]);
                    break;
                case self::CONTRIBUTION:
                    unset($data["isInviting"]);
                    break;
                case self::ADD_WITH_RULES:
                    $data["roles"] = $instrument['rules'];
                    break;
            }
        }
        PHDB::update(
            Project::COLLECTION,
            $query,
            [
                '$set' => [
                    'links.activitypub.' . $link . '.$' => $data
                ]
            ]
        );
    }
    public static function deleteProjectLink($link, $subject, AbstractActor $actor)
    {
        if(!empty($actor) && !empty($subject) && self::isElementExist(Project::COLLECTION,[
            'objectId' => $subject->get('id'),
            "links.activitypub." . $link . ".invitorId" => $actor->get("id")
        ])){
            PHDB::update(
                Project::COLLECTION,
                [
                    'objectId' => $subject->get('id'),
                    "links.activitypub." . $link . ".invitorId" => $actor->get("id")
                ],
                [
                    '$pull' => [
                        'links.activitypub.' . $link => [
                            "invitorId" => $actor->get("id")
                        ]
                    ]
                ]
            );
        }
    }

    public static function saveTags($payload, $actorObject): void
    {
        $data= [
            'username' => $_SESSION["user"]['username'],
            "links.activitypub.follows.invitorId" => $actorObject['id']
        ];
        if(isset($_SESSION["user"]['username']) && self::isElementExist(Person::COLLECTION,$data))
        {
            PHDB::update(
                Person::COLLECTION,
                $data,
                [
                    '$set' => [
                        'links.activitypub.follows.$.tags' => $payload
                    ]
                ]
            );
        }
    }

    public static function getActorTags($actor)
    {
        $tags = [];
        $query = [
            'links.activitypub.follows' => [
                '$elemMatch' => [
                    "invitorId" => $actor
                ]
            ]
        ];
        $user = PHDB::findOne(Person::COLLECTION, $query);
        if(isset($user["links"]['activitypub']['follows'])){
            foreach ($user["links"]['activitypub']['follows'] as $key => $value) {
                if (isset($value['invitorId']) && $value['invitorId'] == $actor) {
                    if(isset($value['tags'])){
                        $tags = $value['tags'];
                    }

                }
            }
        }
        return $tags;
    }

    public static function getAllTags(): array
    {
        $tags = [];
        if(!isset($_SESSION["user"]["username"])) return $tags;
        $user = PHDB::findOne(Person::COLLECTION, [
            'username' => $_SESSION["user"]["username"]
        ]);
        if(isset($user["links"]['activitypub']['follows'])){
            foreach ($user["links"]['activitypub']['follows'] as $key => $value) {
                if(isset($value['tags'])){
                    $tags = array_merge($tags,$value['tags']);
                }
            }
        }
        return Utils::deduplicateArray($tags);
    }
}
