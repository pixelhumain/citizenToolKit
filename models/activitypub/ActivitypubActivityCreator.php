<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub;

use ActivityHub;
use Badge as BadgeClass;
use DateTime;
use Document;
use Error;
use Event;
use MongoId;
use Organization;
use Parsedown;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\AbstractObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Accept;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Invite;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Offer;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use Project;
use Slug;
use Yii;

class ActivitypubActivityCreator
{
    const CONTRIBUTORS = "contributors";

    public static function createNoteActivity($params,$userId = null)
    {
        $userIdent = $userId!=null ? $userId : Yii::app()->session["userId"];
        $user = PHDB::findOneById(Person::COLLECTION, $userIdent);
        $actor = ActivitypubTranslator::coPersonToActor($user);
        $noteParams = [
            "content" => self::transformContentBeforeSet($params),
            "attributedTo" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [Utils::PUBLIC_INBOX]
        ];

        if(isset($params["inReplyTo"]))
            $noteParams["inReplyTo"] = $params["inReplyTo"];

        if (isset($params["mediaImg"]["images"])) {
            $noteParams["attachment"] = [];
            foreach ($params["mediaImg"]["images"] as $docId) {
                $document = PHDB::findOne(Document::COLLECTION, ["_id" => new MongoId($docId)]);
                if ($document) {
                    $doxExt =  array_reverse(explode(".", $document["name"]))[0];
                    $doxExt = ($doxExt == "jpg") ? "jpeg" : $doxExt;
                    $noteParams["attachment"][] = [
                        "type" => "Document",
                        "mediaType" => "image/" . $doxExt,
                        "url" => Utils::createUrl("upload/communecter/" . $document["folder"] . "/" . $document["name"])
                    ];
                }
            }
        }
        if (@$params['parentType'] == "projects" && isset($params['parentId'])) {
            $project = PHDB::findOne(Project::COLLECTION, [
                "_id" => new MongoId($params['parentId'])
            ]);
            $params["target"] = array("type" => $params['parentType'], "id" => $project['objectId']);
            if (!$project) return;
            $noteParams['providerInfo'] = array("title" => $project["slug"] . '@' . Config::HOST(), "link" => array("icon" => "https://www.fediverse.to/static/images/icon.png"));
            if (isset($project["links"]["activitypub"]["followers"]) && sizeof($project["links"]["activitypub"]["followers"]) > 0) {
                foreach ($project["links"]["activitypub"]["followers"] as $follower) {
                    if ($follower["invitorId"] != $actor->get("id")) {
                        $noteParams["cc"][] = $follower["invitorId"];
                    }
                }
            }
            if (isset($project["links"]["activitypub"]["contributors"]) && sizeof($project["links"]["activitypub"]["contributors"]) > 0) {
                foreach ($project["links"]["activitypub"]["contributors"] as $follower) {
                    if ($follower["invitorId"] != $actor->get("id")) {
                        $noteParams["cc"][] = $follower["invitorId"];
                    }
                }
            }
            $summary = Yii::t(
                "activitypub",
                "{who} has create new note in {where} :  '{what}'",
                array(
                    "{who}" => $actor->get("name"),
                    "{what}" => Utils::displayFirst30Words($noteParams['content']),
                    "{where}" =>  $project["name"]
                )
            );
        } else {
            if (isset($user["links"]["activitypub"]["followers"]) && sizeof($user["links"]["activitypub"]["followers"]) > 0) {
                $noteParams["cc"] = [];
                foreach ($user["links"]["activitypub"]["followers"] as $follower) {
                    $noteParams["cc"][] = $follower["invitorId"];
                }
            }
            $summary = Yii::t(
                "activitypub",
                "{who} has create new note '{what}'",
                array(
                    "{who}" => $actor->get("name"),
                    "{what}" => $noteParams['content']
                )
            );
        }

        $note = Type::create("Note", $noteParams);
        $activity = Type::create("Create", [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601)
        ]);
        if ($params['markdownActive']) {
            $activity->set("source", array("content" => $params["text"], "mediaType" => "text/markdown"));
        }
        if (Utils::isTextHasUrl($params["text"])) {
            $activity->set("source", array("mediaType" => "text/html"));
        }
        if (isset($params['target']['id'])) {
            $activity->set("target", $params['target']['id']);
        }

        $activity->set("object", $note);
        $activity->set("to", $note->get("to"));
        $activity->set("cc", $note->get("cc"));
        $activity->set("summary", $summary);
        return $activity;
    }

    private static function transformContentBeforeSet($params)
    {
        if (isset($params["text"])) {
            $parsedown =  new Parsedown();
            if ($params['markdownActive'] == true) {
                return  $parsedown->text($params["text"]);
            } else {
                return Utils::isTextHasUrl($params["text"]) ? Utils::warpUrlIntoLinkTag($params["text"]) :  $params["text"];
            }
        } else {
            return "";
        }
    }

    public static function createDeleteNoteActivity($newsId)
    {
        $object = ActivitypubObject::getObjectByNewsId($newsId);
        if (!$object)
            return null;

        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $actor = ActivitypubTranslator::coPersonToActor($user);

        return Type::create("Delete", [
            "actor" => $actor->get("id"),
            "to" => $object->get("to"),
            "cc" => $object->get("cc"),
            "object" => [
                "id" => $object->get("id"),
                "type" => "Tombstone"
            ],
            "published" => (new DateTime())->format(DATE_ISO8601)
        ]);
    }

    public static function createUpdatePersonActivity($id)
    {
        $person = PHDB::findOneById(Person::COLLECTION, $id);
        if (!$person)
            throw new Error("Person not found.");

        $cc = [];
        if (isset($person["links"]["activitypub"]["followers"])) {
            foreach ($person["links"]["activitypub"]["followers"] as $follower)
                $cc[] = $follower["invitorId"];
        }

        $object = ActivitypubTranslator::coPersonToActor($person);
        return Type::create("Update", [
            "actor" => $object->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "object" => $object,
            "to" => [Utils::PUBLIC_INBOX],
            "cc" => $cc
        ]);
    }

    public static function cancelEventActivity($params)
    {

        $event = PHDB::findOneById(Event::COLLECTION, $params['id']);
        if (!$event)
            return null;
        $object =  PHDB::findOne(ActivitypubObject::COLLECTION, ["object.id" => $event["objectId"]]);
        $object["ical:status"] = "CANCELLED";
        $createEventActivity = [
            "id" => $event["objectId"] . '/activity',
            "actor" => $object['actor'],
            "published" => (new DateTime())->format(DATE_ISO8601),
            "updated" => (new DateTime())->format(DATE_ISO8601)
        ];
        $activity = Type::create("Update", $createEventActivity);
        $activity->set("object", $object);
        $activity->set("to", $object['to']);
        $activity->set("cc", $object['cc']);
        return $activity;
    }

    public static function createJoinEventActivity($activityId)
    {
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $actor = ActivitypubTranslator::coPersonToActor($user);
        $res = ActivitypubActivity::getByActivityByObjectId($activityId);
        if (!$res)
            return null;
        return Type::create("Join", [
            "actor" => $actor->get("id"),
            "object" => $res["object"],
            "attributedTo" => $res["attributedTo"],
            "id" => $res["id"]
        ]);
    }

    public static function createLeaveEventActivity($activityId)
    {
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $actor = ActivitypubTranslator::coPersonToActor($user);
        $res = ActivitypubActivity::getByActivityByObjectId($activityId);
        if (!$res)
            return null;
        return Type::create("Leave", [
            "actor" => $actor->get("id"),
            "object" => $res["object"],
            "attributedTo" => $res["attributedTo"],
            "id" => $res["id"]
        ]);
    }

    public static function createUpdateUploadActivity($ownerId, $res, $params)
    {
        // if (!isset($params['type'])) return;
        if ($params["type"] == Person::COLLECTION){
            return self::createUpdateNoteActivity(Yii::app()->session[ "userId" ]);
        }
        else  if ($params["type"] == BadgeClass::COLLECTION) {
                sleep(5);
                return self::updateUploadActivity($params["type"],$ownerId);

        } else {
            $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
            $actor = ActivitypubTranslator::coPersonToActor($user);
            $data = PHDB::findOne($params['type'], ['_id' => new MongoId($ownerId)]);
            if (!isset($data["objectId"]))
                return;
            $object = Type::createFromAnyValue($data["objectId"]);
            if (!$object || !is_object($object)) return;
            if (isset($res['isBanner']) && $res['isBanner'] == false) {
                $params = array('connectType' => 'updateAvatar', 'res' => $res);
            } else {

                $params = array('connectType' => 'updateBanner', 'res' => $res);
            }
            return self::updateProjectActivity($actor, $object, $params);
        }
    }

    public static function createUpdateNoteActivity($params)
    {
        $res = PHDB::findOne(ActivitypubObject::COLLECTION, ["payload.newsId" => $params["idNews"]]);
        if ($res) {
            $object = Type::createFromAnyValue($res["object"]);

            if ($object) {
                $object->set("updated", (new DateTime())->format(DATE_ISO8601));
                $object->set("attachment", []);
                $object->set("content", self::transformContentBeforeSet($params));
                if (isset($params["mediaImg"]["images"])) {
                    $attachments = [];
                    foreach ($params["mediaImg"]["images"] as $docId) {
                        $document = PHDB::findOne(Document::COLLECTION, ["_id" => new MongoId($docId)]);
                        if ($document) {
                            $doxExt =  array_reverse(explode(".", $document["name"]))[0];
                            $doxExt = ($doxExt == "jpg") ? "jpeg" : $doxExt;
                            $attachments[] = Type::create("Document", [
                                "mediaType" => "image/" . $doxExt,
                                "url" => Utils::createUrl("upload/communecter/" . $document["folder"] . "/" . $document["name"])
                            ]);
                        }
                    }
                    $object->set("attachment", $attachments);
                }


                $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
                $actor = ActivitypubTranslator::coPersonToActor($user);

                $activity = Type::create("Update", [
                    "actor" => $actor->get("id"),
                    "published" => (new DateTime())->format(DATE_ISO8601)
                ]);
                if ($params['markdownActive'] == true) {
                    $activity->set("source", array("content" => $params["text"], "mediaType" => "text/markdown"));
                }
                if (Utils::isTextHasUrl($params["text"])) {
                    $activity->set("source", array("mediaType" => "text/html"));
                }
                $activity->set("object", $object);
                $activity->set("to", $object->get("to"));
                $activity->set("cc", $object->get("cc"));
                return $activity;
            }
        }

        return null;
    }

    public static function updateUploadActivity($collection, $id)
    {
        $res = PHDB::findOne($collection, ['_id' => new MongoId($id)]);

        if (!$res)
            return null;
        PHDB::update(ActivitypubObject::COLLECTION, ["object.id" => $res["objectId"]], [
            '$push' => [
                'object.attachment' => Utils::coImageToActivityPubImage($res)
            ]
        ]);
    }

    public static function updateProjectActivity($actor, $object, $params)
    {
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        if (!$user) return;
        $projectParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [Utils::PUBLIC_INBOX]
        ];
        $project = PHDB::findOne(Project::COLLECTION, [
            "objectId" => $object->get('id')
        ]);
        $attributeUpdated = array();

        if (!$project)
            return null;
        if (isset($user["links"]["activitypub"]["followers"]) && sizeof($user["links"]["activitypub"]["followers"]) > 0) {
            $projectParams["cc"] = [];
            foreach ($user["links"]["activitypub"]["followers"] as $follower) {
                $projectParams["cc"][] = $follower["invitorId"];
            }
        }
        if ($params['connectType'] == 'updateBanner') {
            $objectArray = $object->toArray();
            $attachment = @$objectArray['attachment'] ? $objectArray['attachment'] : array();
            $arr = Utils::coImageBannerToActivityPubImage($attachment, $params['res']);
            $object->set("attachment",  $arr);
            $attributeUpdated[] = 'banner';
        } else if ($params['connectType'] == 'updateAvatar') {
            $objectArray = $object->toArray();
            $attachment = @$objectArray['attachment'] ? $objectArray['attachment'] : array();
            $arr =  Utils::coImageProfilToActivityPubImage($attachment, $params['res']);
            $object->set("attachment", $arr);
            $attributeUpdated[] = 'avatar';
        } else if ($params['connectType'] == 'update') {
            $objectArray = $object->toArray();
            if (@$params['name']) {
                $object->set('name', $params['name']);
                $attributeUpdated[] = 'nom';
            }

            if (@$params['description']) {
                $object->set('content', $params['description']);
                $attributeUpdated[] = 'description';
            }

            if (@$params['startDate']) {
                $object->set('startTime', $params['startDate']);
                $attributeUpdated[] = "date de debut";
            }
            if (@$params['endDate']) {
                $object->set('endTime', $params['endDate']);
                $attributeUpdated[] = "date fin";
            }
            if (@$params['email']) {
                $object->set('email', $params['email']);
                $attributeUpdated[] = "email";
            }

            if (@$params['avancement']) {
                $object->set('progress', $params['avancement']);
                $attributeUpdated[] = "progression";
            }

            if (@$params['tags']) {
                $object->set('tag', Utils::coTagToActivitypubTagEdit(@$objectArray['tag'], $params['tags']));
                $attributeUpdated[] = "tags";
            }
            if (@$params['attachment']) {
                $arrayAttach = array();
                $attachment = @$objectArray['attachment'] ? $objectArray['attachment'] : array();
                $arrayAttach[] =  Utils::coThumbToActivityPubImage($attachment, @$params['attachment']);
                $object->set('attachment', $arrayAttach);
                $attributeUpdated[] = "images";
            }
            if (@$params['address']) {
                $attributeUpdated[] = "localisation";
            }
            if (@$params['gitlab']) {
                $attributeUpdated[] = "le lien vers gitlab";
            }
            if (@$params['twitter']) {
                $attributeUpdated[] = "le lien vers twitter";
            }
            if (@$params['facebook']) {
                $attributeUpdated[] = "le lien vers facebook";
            }
            if (@$params['instagram']) {
                $attributeUpdated[] = "le lien vers instagram";
            }
            if (@$params['signal']) {
                $attributeUpdated[] = "le lien vers signal";
            }
            if (@$params['telegram']) {
                $attributeUpdated[] = "le lien vers telegram";
            }
            if (@$params['diaspora']) {
                $attributeUpdated[] = "le lien vers diaspora";
            }
            if (@$params['mastodon']) {
                $attributeUpdated[] = "le lien vers mastodon";
            }
            if (@$params['url']) {
                $attributeUpdated[] = "l'url";
            }
            //if(@$objectArray['attachment']){
            $attachmentArray = @$objectArray['attachment'] ? $objectArray['attachment'] : array();
            if (count(Utils::coAttachmentToActivitypubAttachment($params, $attachmentArray)) > 0) {
                $arrayAttach = Utils::coAttachmentToActivitypubAttachment($params, $attachmentArray);
                $object->set('attachment', $arrayAttach);
            }
            if (Utils::coLocationToActivitypubLocation($params, @$objectArray['uuid'])) {
                $object->set('location', Utils::coLocationToActivitypubLocation($params, @$objectArray['uuid']));
            }
        }
        //  $person->set("connectType", $params['connectType']);
        $activity = Type::create("Update", $projectParams);
        $activity->set("object",  $object);
        $activity->set("actor", $actor->get("id"));
        if (isset($projectParams["cc"])) {
            $activity->set("cc", $projectParams["cc"]);
        }
        $summary = Yii::t(
            "activitypub",
            "{who} has updated info {what} of project {where}",
            array(
                "{who}" => $actor->get("name"),
                "{what}" => implode(',', $attributeUpdated),
                "{where}" => $project['name']
            )
        );
        $activity->set("summary", $summary);


        return $activity;
    }

    public static function createOrUpdateEventActivity($toSave)
    {
        if (!$toSave['isEdit']) {
            $activity = self::createEventActivity($toSave);
        } else {
            $activity = self::updateEventActivity($toSave);
        }

        return $activity;
    }

    public static function createEventActivity($params)
    {

        $uuid = Utils::genUuid();

        if (@$params['organizer']) {

            $keys = array_keys($params['organizer']);
            $first_key = $keys[0];
            $actorId  = $first_key;
            if( $params['organizer'][$actorId]['type'] !== 'projects'){
                $collection = Organization::COLLECTION;
            }else{
                $collection = Person::COLLECTION;
                $actorId = $_SESSION["userId"];
            }
        } else {
            $collection = Person::COLLECTION;
            $actorId = $_SESSION["userId"];
        }
        $user = PHDB::findOneById($collection, $actorId);
        if (@$params['organizer']) {
            $keys = array_keys($params['organizer']);
            $first_key = $keys[0];
            $actorId  = $first_key;
            if( $params['organizer'][$actorId]['type'] !== 'projects'){
                $actor = ActivitypubTranslator::coGroupToActor($user);
            }else{
                $actor = ActivitypubTranslator::coPersonToActor($user);
            }
        } else {
            $actor = ActivitypubTranslator::coPersonToActor($user);
        }
        $event = [
            "name" =>  $params['name'],
            "actor" =>  $actor->get('id'),
            "startTime" => $params['startDate'],
            "endTime" => $params['endDate'],
            "content" => isset($params['shortDescription']) ? $params['shortDescription'] : "",
            "ical:status" => "CONFIRMED",
            "contacts" => [
                $actor->get('id')
            ],
            "to" => [Utils::PUBLIC_INBOX],
            "uuid" => $uuid,
            "updated" => (new DateTime())->format(DATE_ISO8601),
            "attributedTo" =>  $actor->get("id"),
            "participantCount" => 0,
            "maximumAttendeeCapacity" => 0,
            "repliesModerationOption" => "allow_all",
            "timezone" => $params['timeZone'],
            "commentsEnabled" => true,
            "attachment" => [],
            "category" => $params['type'],
            "published" => (new DateTime())->format(DATE_ISO8601),
            "tag" => [],
            "url" => "https://somemobilizon.instance/events/" . $uuid . '/activity',
            "mediaType" => "text/html",
            "anonymousParticipationEnabled" => false,
            "draft" => false,
            "joinMode" => "free"
        ];
        $invitor = [];

        if (@$params['tags'] || @$params['type']) {
            if (!@$params['tags']) {
                $event['tag'] = Utils::coTagWithTypeToActivitypubTag(null, $params['type']);
            } else {
                $event['tag'] = Utils::coTagWithTypeToActivitypubTag($params['tags'], $params['type']);
            }
        }
        if (isset($user["links"]["activitypub"]["followers"]) && sizeof($user["links"]["activitypub"]["followers"]) > 0) {
            foreach ($user["links"]["activitypub"]["followers"] as $follower) {
                $event["cc"][] = $follower["invitorId"];
                $invitor["cc"][] = $follower["invitorId"] . "/followers";
            }
        }
        $event['attachment'] = Utils::coAttachmentToActivitypubAttachment($params) ? Utils::coAttachmentToActivitypubAttachment($params) : null;
        if (Utils::coLocationToActivitypubLocation($params, $uuid) > 0) {
            $event['location'] = Utils::coLocationToActivitypubLocation($params, $uuid);
        }
        $createEventActivity = [
            "id" => "https://somemobilizon.instance/events/" . $uuid . '/activity',
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [Utils::PUBLIC_INBOX]
        ];
        $object = Type::create("Event", $event);
        if (@$event["cc"] && count($event["cc"]) > 0) {
            $object->set('cc', array_unique($event["cc"]));
        }
        $object->set("id", "https://somemobilizon.instance/events/" . $uuid);
        $activity = Type::create("Create", $createEventActivity);
        $activity->set("object", $object);
        $activity->set("to",  $object->get('to'));
        $activity->set("cc", $object->get('cc'));

        return $activity;
    }

    public static function updateEventActivity($params)
    {
        $event = PHDB::findOneById(Event::COLLECTION, $params['id']);

        if (!$event ||!isset($event["objectId"]))
            return null;
        $object = Type::createFromAnyValue($event["objectId"]);
        if (!$object || !is_object($object)) return null;
        $objectArray = $object->toArray();
        if (@$params['name']) {
            $object->set('name', $params['name']);
        }
        if (@$params['description']) {
            $object->set('content', $params['description']);
        }

        if (@$params['startDate']) {
            $object->set('startTime', $params['startDate']);
        }
        if (@$params['endDate']) {
            $object->set('endTime', $params['endDate']);
        }
        if (@$params['tags'] && @$params['type']) {
            $object->set('tag', Utils::coTagWithTypeToActivitypubTagEdit($objectArray['tag'], $params['tags'], $params['type']));
        } else {
            if (@$params['tags']) {
                $object->set('tag', Utils::coTagToActivitypubTagEdit($objectArray['tag'], $params['tags']));
            }
            if (@$params['type']) {
                $object->set('tag', Utils::coTypeCombineToActivitypubTagEdit($objectArray['tag'], $params['type']));
            }
        }
        if (@$params['attachment']) {
            $arrayAttach = array();
            $attachment = @$objectArray['attachment'] ? $objectArray['attachment'] : array();
            $arrayAttach[] =  Utils::coThumbToActivityPubImage($attachment, @$params['attachment']);
            $object->set('attachment', $arrayAttach);
        }
        //if(@$objectArray['attachment']){
        $attachmentArray = @$objectArray['attachment'] ? $objectArray['attachment'] : array();
        if (count(Utils::coAttachmentToActivitypubAttachment($params, $attachmentArray)) > 0) {
            $arrayAttach = Utils::coAttachmentToActivitypubAttachment($params, $attachmentArray);
            $object->set('attachment', $arrayAttach);
        }
        //}
        if (Utils::coLocationToActivitypubLocation($params, $objectArray['uuid'])) {
            $object->set('location', Utils::coLocationToActivitypubLocation($params, $objectArray['uuid']));
        }
        $createEventActivity = [
            "id" => $event["objectId"] . '/activity',
            "actor" => $objectArray['actor'],
            "published" => (new DateTime())->format(DATE_ISO8601),
            "updated" => (new DateTime())->format(DATE_ISO8601)
        ];
        $cc = [];
        /**$objcc = $object->get("cc");
        foreach ($objcc as $o) {
        $cc[] = Utils::removeLastSegmentFromUrl($o);
        }**/
        $activity = Type::create("Update", $createEventActivity);
        $activity->set("object", $object);
        $activity->set("to", $object->get("to"));
        $activity->set("cc", $object->get("cc"));
        return $activity;
    }

    public static function createOrUpdateProjectActivity($toSave)
    {
        $activity = null;
        if (!$toSave['isEdit']) {
            $activity = self::createProjectActivity($toSave);
        } else {
            $project  = PHDB::findOne(Project::COLLECTION, array('_id' => new MongoId($toSave['id'])));
            if (!$project) return null;
            if (!isset($project["objectId"])) return null;
            $subject = Type::createFromAnyValue($project["objectId"]);
            if (!$subject || !is_object($subject)) return null;
            $content = array_merge(array('connectType' => 'update'), $toSave);
            $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
            $actor = ActivitypubTranslator::coPersonToActor($user);
            $activity = self::updateProjectActivity($actor, $subject, $content);
        }

        return $activity;
    }

    public static function createProjectActivity($params)
    {
        $uuid = Utils::genUuid();
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $actor = ActivitypubTranslator::coPersonToActor($user);
        $slugActivity = Slug::checkAndCreateSlug($params['name']);

        $projectParams = [
            "uuid" => $uuid,
            "name" => $params['name'],
            "slug" => $slugActivity,
            "attributedTo" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [Utils::PUBLIC_INBOX]
        ];
        if (isset($params["email"])) {
            $projectParams["email"] = $params["email"];
        }
        if (isset($params["tags"])) {
            $projectParams["tags"] = $params["tags"];
        }
        if (isset($params["shortDescription"])) {
            $projectParams["shortDescription"] = $params["shortDescription"];
        }
        if (isset($params["url"])) {
            $projectParams["url"] = $params["url"];
        }

        if (isset($user["links"]["activitypub"]["followers"]) && sizeof($user["links"]["activitypub"]["followers"]) > 0) {
            $projectParams["cc"] = [];
            foreach ($user["links"]["activitypub"]["followers"] as $follower) {
                $projectParams["cc"][] = $follower["invitorId"];
            }
        }

        $project = Type::create("Project", $projectParams);
        $activity = Type::create("Create", [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601)
        ]);
        $activity->set("object", $project);
        $activity->set("to", $project->get("to"));
        $activity->set("cc", $project->get("cc"));
        $summary = Yii::t(
            "activitypub",
            "{who} has create project named {what}",
            array(
                "{who}" => $actor->get("name"),
                "{what}" => $project->get('name')
            )
        );
        $activity->set("summary", $summary);
        return $activity;
    }

    public static function createDeleteActivity($collection, $id)
    {

        $res = PHDB::findOne($collection, ['_id' => new MongoId($id)]);
        if(!isset($res["objectId"])) return null;
        $object = Type::createFromAnyValue($res["objectId"]);

        if (!$object)
            return null;
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $actor = ActivitypubTranslator::coPersonToActor($user);
        $activity = Type::create("Delete", [
            "actor" => $actor->get("id"),
            "to" => $object->get("to"),
            "cc" => $object->get("cc"),
            "object" => [
                "id" => $object->get("id"),
                "type" => "Tombstone"
            ],
            "published" => (new DateTime())->format(DATE_ISO8601)
        ]);
        $summary = Yii::t(
            "activitypub",
            "{who} has delete note named {what}",
            array(
                "{who}" => $actor->get("id"),
                "{what}" => $object->get('name')
            )
        );
        $activity->set("summary", $summary);
        return $activity;
    }

    /**
     * @throws \Exception
     *
     */
    public static function createContributionActivity($target, $instrument): AbstractObject
    {
        $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
        // send to admin the request
        $cc = ActivitypubActivity::getAllCCRecipients(Project::COLLECTION, "contributors", $target, $actor);
        $joinParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "object" => $actor,
            "to" => [Utils::PUBLIC_INBOX],
            "cc" => $cc,
            "target" => $target->get('id'),
            "instrument" => $instrument
        ];

        if ($instrument == 'contributor') {
            $summary = Yii::t(
                "activitypub",
                "{who} request to join the project {what}  as contributor",
                array(
                    "{who}" => $actor->get('name'),
                    "{what}" => $target->get('name')
                )
            );
            $joinParams['summary'] = $summary;
        }
        if ($instrument == 'admin') {
            $summary = Yii::t(
                "activitypub",
                "{who} request to join the project {what}  as admin",
                array(
                    "{who}" => $actor->get('name'),
                    "{what}" => $target->get('name')
                )
            );
            $joinParams['summary'] = $summary;
        }

        return Type::create("Join", $joinParams);
    }

    public static function createFollowActivity($object)
    {
        $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
        $cc = ActivitypubActivity::getAllCCRecipients(Project::COLLECTION, "contributors", $object, $actor);
        if(!isset($cc)){
            $cc = array($object->get('actor'));
        }
        $target = Type::createFromAnyValue($object->get('id'));
        $object = Type::createFromAnyValue($actor->get('id'));
        $joinParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "object" => $object,
            "to" => [Utils::PUBLIC_INBOX],
            "cc" => $cc,
            "target" => $target->get('id'),
            "instrument" => "follow"
        ];
        $summary = Yii::t(
            "activitypub",
            "{who} request to join the project {what}  as follower",
            array(
                "{who}" => $object->get('name'),
                "{what}" => $target->get('name')
            )
        );
        $joinParams['summary'] = $summary;
        $activity = Type::create("Join", $joinParams);
        return $activity;
    }

    public static function createInvitationProjectActivity($payload)
    {
        $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
        $object = Type::createFromAnyValue($payload['objectId']);
        if (!$object || !is_object($object)) return null;
        $invitor = Type::createFromAnyValue($payload['targetId']);
        $instrument = array('as' => 'contributor');
        if (isset($payload['inviteWithRoles'])) {
            $instrument = array('as' => 'inviteWithRules', 'rules' => $payload['inviteWithRoles']);
        }
        if (isset($payload['inviteAsAdmin'])) {
            $instrument = 'becomeadmin';
        }
        if (isset($payload['inviteWithRoles']) && isset($payload['inviteAsAdmin'])) {
            $instrument = array('as' => 'adminandrole', 'rules' => $payload['inviteWithRoles']);
        }
        $summary = "";
        $person = Type::createFromAnyValue($invitor);
        $target = Type::createFromAnyValue($object->get('id'));
        if (!$target || !is_object($target)) return null;
        $activity = new Invite();
        $activity->setActor($actor->get("id"));
        $activity->setObject($person);
        $activity->setPublished((new DateTime())->format(DATE_ISO8601));
        $activity->setTarget($target->get('id'));
        $activity->setCc([
            $person->get('id')
        ]);
        $activity->setTo([Utils::PUBLIC_INBOX]);
        $activity->setInstrument($instrument);
        if (is_string($instrument)) {
            if ($instrument == 'becomeadmin') {
                $summary = Yii::t(
                    "activitypub",
                    "{who} invite to become admin in projet {what}",
                    array(
                        "{who}" => $person->get('name'),
                        "{what}" => $target->get('name')
                    )
                );
            }
        } else {
            if ($instrument['as'] == 'adminandrole') {
                $summary = Yii::t(
                    "activitypub",
                    "{who} invite you to manage with rules {rules} in projet {what}",
                    array(
                        "{who}" => $person->get('name'),
                        "{rules}" => implode(',', $instrument['rules']),
                        "{what}" => $target->get('name')
                    )
                );
                $joinParams['summary'] = $summary;
            } else  if ($instrument['as'] == 'inviteWithRules') {
                $summary = Yii::t(
                    "activitypub",
                    "{who} invite you with rules {rules} in projet {where}",
                    array(
                        "{who}" => $person->get('name'),
                        "{what}" => $invitor->get('name'),
                        "{rules}" => implode(',', $instrument['rules']),
                        "{where}" => $target->get('name')
                    )
                );
                $joinParams['summary'] = $summary;
            } else if ($instrument['as'] == 'contributor') {
                $summary = Yii::t(
                    "activitypub",
                    "{who} invite you to contribute on projet {what}",
                    array(
                        "{who}" => $person->get('name'),
                        "{what}" => $target->get('name')
                    )
                );
                $joinParams['summary'] = $summary;
            }
        }
        $activity->setSummary($summary);
        return $activity;
    }

    public static function acceptContributionActivity($person, $objectId)
    {
        $object = Type::createFromAnyValue($objectId);
        if (!$object || !is_object($object)) return null;
        $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
        $project = PHDB::findOne(Project::COLLECTION, [
            "objectId" => $object->get('id')
        ]);

        $target = Type::createFromAnyValue($person);
        $activity = ActivitypubActivity::getLinkActivity("contributors", $project['_id'], $target, 'project');

        $summary = Yii::t("activitypub", "{who} accept as contributor  into {what}", array(
            "{who}" => $target->get('preferredUsername'),
            "{what}" =>  $object->get('name')
        ));
        $cc = ActivitypubActivity::getAllCCRecipients(Project::COLLECTION, "contributors", $object, $actor);
        $acceptactivity = new Accept();
        $acceptactivity->setObject($activity->get("id"));
        $acceptactivity->setActor($actor->get("id"));
        $acceptactivity->setTarget($activity->get("target"));
        $acceptactivity->setInstrument($activity->get("instrument"));
        $acceptactivity->setCc($cc);
        $acceptactivity->setTo($activity->get("to"));
        $acceptactivity->setSummary($summary);
        return $acceptactivity;
    }
    public static function createOfferProjectActivity($payload)
    {
        $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
        $object = Type::createFromAnyValue($payload['objectId']);
        if (!$object || !is_object($object)) return null;
        $invitor = Type::createFromAnyValue($payload['targetId']);
        if (isset($payload['connectType']) && $payload['connectType'] == 'addAsAdmin') {
            $instrument = 'asadmin';
        }
        if (isset($payload['connectType']) && $payload['connectType'] == 'withRules') {
            $instrument = array('as' => 'withRules', 'rules' => $payload['rules']);
        }
        $summary = "";
        $activity = new Offer();
        $activity->setActor($actor->get("id"));
        $activity->setObject($invitor);
        $activity->setPublished((new DateTime())->format(DATE_ISO8601));
        $activity->setTarget($object->get('id'));
        $activity->setCc([
            $invitor->get('id')
        ]);
        $activity->setTo([Utils::PUBLIC_INBOX]);
        $activity->setInstrument($instrument);
        if ($instrument['as'] == 'withRules') {
            $summary = Yii::t(
                "activitypub",
                "{who} add {invitor} as contributor with rules {rules} in projet {what}",
                array(
                    "{who}" => $actor->get('name'),
                    "{rules}" => implode(',', $instrument['rules']),
                    "{what}" => $object->get('name'),
                    "{invitor}" =>  $invitor->get('name')
                )
            );
            $activity->setSummary($summary);
        }
        if ($instrument == 'asadmin') {
            $summary = Yii::t(
                "activitypub",
                "{who} add you as admin in projet {where} in projet {what}",
                array(
                    "{who}" => $actor->get('name'),
                    "{what}" => $invitor->get('name'),
                    "{where}" => $object->get('name')
                )
            );
            $activity->setSummary($summary);
        }

        return $activity;
    }

    /**public static function rejectContributionActivity($person, $object)
    {
    $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
    $project = PHDB::findOne(Project::COLLECTION, [
    "objectId" => $object->get('id')
    ]);
    $target = Type::createFromAnyValue($person);
    $activity = ActivitypubActivity::getLinkActivity("contributors", $project['_id'], $target, 'project');
    $summary = Yii::t("activitypub", "{who} reject as contributor  into {what}", array(
    "{who}" => $target->get('preferredUsername'),
    "{what}" =>  $object->get('name')
    ));
    $cc = [];
    $cc = ActivitypubActivity::getAllCCRecipients(Project::COLLECTION, "contributors", $object, $actor, true);
    $rejectActivity = new Reject();
    $rejectActivity->setObject($activity->get("id"));
    $rejectActivity->setActor($actor->get("id"));
    $rejectActivity->setTarget($activity->get("target"));
    $rejectActivity->setCc($cc);
    $rejectActivity->setTo($activity->get("to"));
    $rejectActivity->setInstrument($activity->get("instrument"));
    $rejectActivity->setSummary($summary);
    return $rejectActivity;
    }**/


    public static function leaveContributionOnProjectActivity($actor, $object, $params)
    {
        $summary = Yii::t(
            "activitypub",
            "{who} leave contribution in project {where}",
            array(
                "{who}" => $actor->get('name'),
                "{where}" => $object->get('name')
            )
        );
        $projectParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [Utils::PUBLIC_INBOX]
        ];
        $projectParams["cc"] = ActivitypubActivity::getAllCCRecipients(Project::COLLECTION, "contributors", $object, $actor);

        $activity = Type::create("Leave", $projectParams);
        $activity->set("object",  $object->get('id'));
        $activity->setInstrument('contributor');
        $activity->set("summary",  $summary);
        return $activity;
    }


    public static function leaveFollowInProjectActivity($actor, $object, $params)
    {
        $summary = Yii::t(
            "activitypub",
            "{who} leave his follow in  {where}",
            array(
                "{who}" => $actor->get('name'),
                "{where}" => $object->get('name')
            )
        );
        $projectParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [Utils::PUBLIC_INBOX]
        ];
        $projectParams["cc"] = ActivitypubActivity::getAllCCRecipients(Project::COLLECTION, "contributors", $object, $actor);
        $activity = Type::create("Leave", $projectParams);
        $activity->set("object",  $object->get('id'));
        $activity->setInstrument('unfollow');

        $activity->set("summary",  $summary);
        return $activity;
    }
    public static function createDeleteContributionOnProjectActivity($params)
    {
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $actor = ActivitypubTranslator::coPersonToActor($user);
        $object = Type::createFromAnyValue($params['objectId']);
        if (!$object || !is_object($object)) return null;
        $summary = Yii::t(
            "activitypub",
            "{who} delete contribution in project {where}",
            array(
                "{who}" => $actor->get('name'),
                "{where}" => $object->get('name')
            )
        );
        $activity = Type::create("Delete", [
            "actor" => $actor->get("id"),
            "to" => $object->get("to"),
            "cc" => ActivitypubActivity::getAllCCRecipients(Project::COLLECTION, "contributors", $object, $actor),
            "object" => $params['targetId'],
            "target" => $object->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601)
        ]);
        $activity->set("summary",  $summary);
        return $activity;
    }

    public static function createOrUpdateBadge($toSave)
    {
        return !$toSave['isEdit'] ? (new Badge())->create($toSave) :(new Badge())->update($toSave);
    }
    public static function createAssignBadge($params)
    {
        return (new Badge())->assign($params);
    }

    public static function createConfirmAndRevokeBadgeActivity($subject, $payload)
    {
        if(isset($payload['isRevoke']) && isset($payload['activityId'])){
            return (new Badge())->revoke($payload);
        }
        if(isset($payload['isConfirm'])){
            if($payload[ 'isConfirm' ] == "true"){
                if(isset($payload["awardId"]) && isset($payload["activityId"])){
                    return (new Badge())->confirm($payload);
                }
            }else{

                if($payload[ 'confirmType' ] == "cancel_revoke"){
                    if(isset($payload["awardId"]) && isset($payload["activityId"])){
                        return (new Badge())->reinit($payload);
                    }
                }else{
                    if(isset($payload["awardId"]) && isset($payload["activityId"])){
                        return (new Badge())->refused($payload);
                    }
                }
            }
        }

        return null;
    }


}
