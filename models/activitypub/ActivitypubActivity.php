<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub;

use Exception;
use MongoDate;
use MongoId;
use Person;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\AbstractActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use Project;

class ActivitypubActivity
{
    const COLLECTION = "activitypub_activity";

    public static function save(Activity $activity, array $receivers, $payload = null)
    {
        $activityUUID = uniqid();

        //set activity id if it doesn't have one
        if (!$activity->get("id"))
            $activity->setId(Utils::createUrl("api/activitypub/activity/id/" . $activityUUID));

        //retrieve the actor of the activity
        $actor = Type::createFromAnyValue($activity->get("actor"));
        //retrieve the activity object
        $object = Type::createFromAnyValue($activity->get("object"));

        //save object
        $objectUUID = ActivitypubObject::save($actor, $receivers, $object, $payload);

        //simplify the object of the activity using just the id
        $activity->setObject($object->get("id"));

        $data = [
            "uuid" => $activityUUID,
            "from" => $actor->get("id"),
            "to" => Utils::retrieveAllIdsFromObjects($receivers),
            "activity" => $activity->toArray(),
            "createAt" => new MongoDate(time())
        ];
        if ($payload)
            $data["payload"] = $payload;
        PHDB::insert(self::COLLECTION, $data);


        $UUIDS = [
            "activity" => $activityUUID,
            "object" => $objectUUID
        ];

        return $UUIDS;
    }

    public static function getByActivityId($id)
    {
        $res = PHDB::findOne(self::COLLECTION, ["activity.id" => $id]);
        if (!$res)
            return false;
        return $res["activity"];
    }

    public static function getByActivityByObjectId($id)
    {
        $res = PHDB::findOne(self::COLLECTION, ["activity.object" => $id]);
        if (!$res)
            return false;
        return $res["activity"];
    }
    public static function getByUUID($uuid): ?Activity
    {
        $activity = PHDB::findOne(self::COLLECTION, ["uuid" => $uuid]);
        if (!$activity)
            return null;

        $activity = Type::createFromAnyValue($activity["activity"]);
        $activity->set("object", Type::createFromAnyValue($activity->get("object")));

        return $activity;
    }

    public static function getLinkActivity($linkType, $localUserId, $invitor, $actorType): Activity
    {
        if ($actorType == "group") {
            $collection = Organization::COLLECTION;
        } else if ($actorType == "project") {
            $collection = Project::COLLECTION;
        } else {
            $collection = Person::COLLECTION;
        }
        $user = PHDB::findOne($collection, [
            "_id" => new MongoId($localUserId),
            "links.activitypub." . $linkType => [
                '$elemMatch' => ["invitorId" => $invitor->get("id")]
            ]
        ]);

        if (!$user)
            throw new Exception("Link not found");



        $link = null;
        foreach ($user["links"]["activitypub"][$linkType] as $follower) {
            if ($follower["invitorId"] == $invitor->get("id")) {
                $link = $follower;
                break;
            }
        }

        if (!$link)
            throw new Exception("Link not found");

        return self::getByUUID($link["activityId"]);
    }

    public static function getLinkActivityProject($linkType, $subject,  $invitor): Activity
    {
        $project = PHDB::findOne(Project::COLLECTION, [
            "objectId" => $subject->get("id")
        ]);
        if (!$project)
            throw new Exception("Link not found");



        $link = null;
        foreach ($project["links"]["activitypub"][$linkType] as $follower) {
            if ($follower["invitorId"] == $invitor) {
                $link = $follower;
                break;
            }
        }

        if (!$link)
            throw new Exception("Link not found");

        return self::getByUUID($link["activityId"]);
    }
    /**
     * Undocumented function
     *
     * @param [type] $elementType
     * @param [type] $linkTypes
     * @param AbstractActor $subject
     * @param AbstractActor $invitor
     * @return void
     */
    public static function getElementLinkActivity($elementType, $linkType,  AbstractActor $subject,  AbstractActor $invitor): Activity
    {
        $element = PHDB::findOne($elementType, [
            "objectId" => $subject->get("id")
        ]);
        if (!$element)
            throw new Exception("Link not found");
        $link = null;
        foreach ($element["links"]["activitypub"][$linkType] as $el) {
            if ($el["invitorId"] == $invitor) {
                $link = $el;
                break;
            }
        }

        if (!$link)
            throw new Exception("Link not found");

        return self::getByUUID($link["activityId"]);
    }

    public static function getAllCCRecipients($elementType, $linkType,   $subject,  $invitor,$isAdmin = false): array
    {
        if ($elementType == Person::COLLECTION) {
            $element = PHDB::findOne($elementType, [
                "slug" => $subject->get("preferredUsername")
            ]);
        } else {
            $element = PHDB::findOne($elementType, [
                "objectId" => $subject->get("id")
            ]);
        }
        if (!$element)
            throw new Exception("Link not found");
        $links = [];
        if (is_string($linkType)) {
            if (isset($element["links"]["activitypub"])) {

                foreach ($element["links"]["activitypub"][$linkType] as $el) {
                    if($isAdmin){
                        if ($el["invitorId"] != $invitor && isset($el["isAdmin"])) {
                            $links[] = $el['invitorId'];
                            break;
                        }
                    }else{
                        if ($el["invitorId"] != $invitor) {
                            $links[] = $el['invitorId'];
                            break;
                        }
                    }

                }
            }
        } else {
            if (count($linkType)) {
                foreach ($linkType as $lt) {
                    if (isset($element["links"]["activitypub"])) {

                        foreach ($element["links"]["activitypub"][$linkType] as $el) {
                            if ($el["invitorId"] != $invitor) {
                                $links[] = $el['invitorId'];
                                break;
                            }
                        }
                    }
                }
            }
        }
        return $links;
    }
    public static function getActivityByDateRange($startDate,$endDate,$domain, $isLocalActivity,$type) : array
    {
        if($isLocalActivity){
            if($type=='all'){

            $queryDate =  [
                '$and' => [
                    ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                    ['from' => ['$regex' => "^$domain", '$options' => 'i']]
                ]
            ];

            }else{
                $queryDate =  [
                    '$and' => [
                        ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                        ['from' => ['$regex' => "^$domain", '$options' => 'i']],
                        ['activity.type' => ucfirst(strtolower($type))]
                    ]
                ];

            }
        }else{
            $queryDate = self::getQueryDate($type, $startDate, $endDate, $domain);

        }
        return PHDB::find(self::COLLECTION,$queryDate);
    }

    public static function getInstanceInteractingByDateRange($startDate, $endDate)
    {
        return PHDB::distinct(self::COLLECTION, 'from', [
            'createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]
        ]);
    }
    public static function getInteractionByDateRange($startDate, $endDate,$domain,$refdomain) : array
    {
        $referDomain = $refdomain ? Config::SCHEMA() .'://'. $refdomain : Config::SCHEMA() .'://'. Config::HOST() ;
        return PHDB::find(self::COLLECTION,[
            '$and' => [
                ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                ['from' => ['$regex' => "^$referDomain", '$options' => 'i']],
                ['to' => [
                    '$elemMatch' => [
                        '$regex' => "^https:\/\/$domain",
                        '$options' => 'i'
                    ]
                ]]
            ]
        ]);
    }

    public static function getActivityByTypeAndDateRange($startDate,  $endDate,  $domain,$refdomain)
    {
        $referDomain = $refdomain ? Config::SCHEMA() .'://'. $refdomain : Config::SCHEMA() .'://'. Config::HOST() ;
        $matchStage = [
            '$match' => [
                'createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))],
                'from' => ['$regex' => "^$referDomain", '$options' => 'i'],
                'to' => [
                    '$elemMatch' => [
                        '$regex' => "^https:\/\/$domain",
                        '$options' => 'i'
                    ]
                ]
            ]
        ];
        $groupStage = [
            '$group' => [
                '_id' => '$activity.type',
                'documents' => ['$push' => '$$ROOT']
            ]
        ];
        $pipeline = [$matchStage, $groupStage];;
       return PHDB::aggregate(self::COLLECTION, $pipeline);
    }

    public static function getActivityByDateRangeAndType($startDate,$endDate,$domain,$type,$refDomain=null)
    {
        $activities = [];
            if($type == 'all'){
                if($refDomain!="all"){
                    $queryDateForLocalActivity =  [
                    '$and' => [
                        ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                        ['from' => ['$regex' => "^$domain", '$options' => 'i']],
                        ['to' => [
                            '$elemMatch' => [
                                '$regex' => "^https:\/\/$refDomain",
                                '$options' => 'i'
                            ]
                        ]]
                    ]
                ];
                }else{
                    $queryDateForLocalActivity =  [
                        '$and' => [
                            ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                            ['from' => ['$regex' => "^$domain", '$options' => 'i']]
                        ]
                    ];
                }
            }else{
                if($refDomain!="all"){

                    $queryDateForLocalActivity =  [
                        '$and' => [
                            ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                            ['from' => ['$regex' => "^$domain", '$options' => 'i']],
                            ['activity.type' => ucfirst(strtolower($type)) ],
                            ['to' => [
                                '$elemMatch' => [
                                    '$regex' => "^https:\/\/$refDomain",
                                    '$options' => 'i'
                                ]
                            ]]
                        ]
                    ];
                }else{
                    $queryDateForLocalActivity =  [
                        '$and' => [
                            ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                            ['from' => ['$regex' => "^$domain", '$options' => 'i']],
                            ['activity.type' => ucfirst(strtolower($type)) ]
                        ]
                    ];
                }
            }
            $activities["out"] = PHDB::find(self::COLLECTION,$queryDateForLocalActivity);


        $queryDateForIntercepted = self::getQueryDate($type, $startDate, $endDate, $domain);
        $activities["in"] = PHDB::find(self::COLLECTION,$queryDateForIntercepted);

        return $activities;
    }

    /**
     * @param $type
     * @param $startDate
     * @param $endDate
     * @param $domain
     * @return array[]
     */
    private static function getQueryDate($type, $startDate, $endDate, $domain)
    {
        if($type == 'all'){
            $queryDate = [
                '$and' => [
                    ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                    ['from' => [
                        '$not' => ['$regex' => "^$domain", '$options' => 'i']
                    ]]
                ]
            ];
        } else {
            $queryDate = [
                '$and' => [
                    ['createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]],
                    ['from' => [
                        '$not' => ['$regex' => "^$domain", '$options' => 'i']
                    ]],
                    ['activity.type' => ucfirst(strtolower($type))]
                ]
            ];
        }
        return $queryDate;
    }

}
