<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub;

use MongoDate;
use News;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\AbstractObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\TypeResolver;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;

class ActivitypubObject
{
    const COLLECTION = "activitypub_object";

    public static function save(AbstractActor $creator, array $targets, AbstractObject $object, $payload = null): ?string
    {
        $uuid = uniqid();

        //set object id if it doesn't have one
        if (!$object->get("id"))
            $object->set("id", Utils::createUrl("api/activitypub/object/id/" . $uuid));

        else {
            $res = PHDB::findOne(self::COLLECTION, ["object.id" => $object->get("id")]);
            if ($res) {
                $query = @$payload ? [
                    "object" => $object->toArray(),
                    "payload" => is_array($payload) ? Utils::addOrRemoveIntoArray((@$res['payload'] && is_array($res['payload'])) ? $res['payload'] : array(), $payload) : $payload
                ] : [
                    "object" => $object->toArray()
                ];
                PHDB::update(self::COLLECTION, ["object.id" => $object->get("id")], [
                    '$set' => $query
                ]);

                return $res["uuid"];
            }
        }

        //filter object to save
        $allowedObjectTypes = array_merge(TypeResolver::$objectTypes, TypeResolver::$actorTypes);
        $isAllowedObject = in_array($object->get("type"), $allowedObjectTypes);
        $isNotExist = !self::getObjectById($object->get("id"));
        if ($isNotExist && $isAllowedObject) {
            $data = [
                "uuid" => $uuid,
                "createAt" => new MongoDate(time()),
                "object" => $object->toArray()
            ];

            if (in_array($object->get("type"), TypeResolver::$objectTypes)) {
                $data["creator"] = $creator->get("id");
                $data["targets"] = Utils::retrieveAllIdsFromObjects($targets);
            }

            if ($payload)
                $data["payload"] = $payload;

            PHDB::insert(self::COLLECTION, $data);

            return $uuid;
        }

        return null;
    }

    //get object by id
    public static function getObjectById($id): ?AbstractObject
    {
        $res = PHDB::findOne(self::COLLECTION, ["object.id" => $id]);
        if (!$res)
            return null;
        return Type::createFromAnyValue($res["object"]);
    }

    public static function getObjectByIdAsArray($id)
    {
        $res = PHDB::findOne(self::COLLECTION, ["object.id" => $id]);
        if (!$res)
            return null;
        return $res["object"];
    }

    public static function getObjectByUUID($uuid): ?AbstractObject
    {
        // Si c'est un URL, passer directement à createFromAnyValue
        if (filter_var($uuid, FILTER_VALIDATE_URL)) {
            return Type::createFromAnyValue($uuid);
        }

        $res = PHDB::findOne(self::COLLECTION, ["uuid" => $uuid]);
        if (!$res)
            return null;
        return Type::createFromAnyValue($res["object"]);
    }

    public static function getRepliesOfNote($noteId, $index = null, $limit = 5): array
    {
        $res = [];
        if ($index) {
            $res = PHDB::findAndSortAndLimitAndIndex(
                self::COLLECTION,
                ['object.inReplyTo' => $noteId],
                ["createAt" => -1],
                $limit,
                $index
            );
        } else {
            $res = PHDB::findAndSort(self::COLLECTION, ['object.inReplyTo' => $noteId], ["createAt" => -1]);
        }

        $replies = [];
        foreach ($res as $row) {
            $replies[] = Type::createFromAnyValue($row["object"]);
        }

        return $replies;
    }

    public static function getNewsComments($newsId)
    {
        $res = PHDB::findOneById(News::CONTROLLER, $newsId);

        if (!$res && !isset($res["objectUUID"]))
            return [];

        $object = self::getObjectByUUID($res["objectUUID"]);
        if (!$object)
            return [];

        $res = PHDB::findAndSort(self::COLLECTION, ['object.inReplyTo' => $object->get("id")], ["createAt" => -1]);

        $comments = [];
        foreach ($res as $row) {
            $comment = Type::createFromAnyValue($row["object"]);
            $comment = ActivitypubTranslator::noteToNewsMedia($comment);
            $comment["created"] = $row["createAt"];
            $comment["uuid"] = $row["uuid"];

            $comments[] = $comment;
        }

        return $comments;
    }

    public static function getObjectByNewsId($newsId): ?AbstractObject
    {
        $res = PHDB::findOne(self::COLLECTION, ["payload.newsId" => $newsId]);
        if (!$res)
            return null;
        return Type::createFromAnyValue($res["object"]);
    }



    public static function removeByObjectId($objectId)
    {
        PHDB::remove(self::COLLECTION, ["object.id" => $objectId]);
    }

    public static function getNotes($userAdress)
    {
        $notes = [];
        $res = PHDB::findAndSort(self::COLLECTION, [
            "object.type" => "Note",
            "targets" => $userAdress
        ], ["createAt" => -1]);

        foreach ($res as $item) {
            $notes[] = [
                "note" => Type::createFromAnyValue($item["object"]),
                "creator" => Type::createFromAnyValue($item["creator"]),
                "date" => $item["createAt"]
            ];
        }

        return $notes;
    }
    public static function getProjects($user)
    {
        $res = PHDB::findAndSort(self::COLLECTION, [
            "object.type" => "Project",
            "object.attributedTo" => $user
        ], ["createAt" => -1]);
        return $res;
    }

    public static function getActivityByDateRange($startDate, $endDate)
    {
        $referDomain =  Config::SCHEMA() .'://'. Config::HOST() ;
        $matchStage = [
            '$match' => [
                'createAt' => ['$gte' => new MongoDate(strtotime($startDate)), '$lte' => new MongoDate(strtotime($endDate))]
            ]
        ];
        $groupStage = [
            '$group' => [
                '_id' => '$object.type',
                'documents' => ['$push' => '$$ROOT']
            ]
        ];
        $pipeline = [$matchStage, $groupStage];;
        $results = PHDB::aggregate(self::COLLECTION, $pipeline);
        $finalResult = [];
        $typesToEnsure = ['Note', 'Event', 'Project'];
        $categories = ['all', 'in', 'out'];
        foreach ($typesToEnsure as $type) {
            $finalResult[$type] = [];
            foreach ($categories as $category) {
                $finalResult[$type][$category] = [];
            }
        }
        foreach ($results['result'] as $result) {
            $type = $result['_id'];
            if($type != 'Person' && $type != 'Group' && $type != 'Offer'){
                foreach ($result['documents'] as $document) {
                    if (strpos($document['object']['id'], $referDomain) !== false) {
                        $finalResult[$type]['out'][] = $document;
                    } else {
                        $finalResult[$type]['in'][] = $document;
                    }
                    $finalResult[$type]['all'][] = $document;
                }
            }
        }
        return $finalResult;
    }
}
