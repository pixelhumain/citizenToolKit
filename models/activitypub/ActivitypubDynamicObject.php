<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub;

class ActivitypubDynamicObject {
    private $attributes = [];

    public function set($key, $value) {
        $this->attributes[$key] = $value;
    }
    public function get($key) {
        return isset($this->attributes[$key]) ? $this->attributes[$key] : null;
    }
    public function toArray() {
        $result = [];
        foreach ($this->attributes as $key => $value) {
            if ($value instanceof ActivitypubDynamicObject) {
                $result[$key] = $value->toArray();
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }
}
