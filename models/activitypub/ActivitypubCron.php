<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub;

use Exception;
use MongoDate;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use Yii;

class ActivitypubCron{
    const COLLECTION = "activitypub_cron";
    const EXEC_COUNT = 3;
    const MAX_ATTEMPT = 5;

    const STATUS_PENDING = "pending";
    const STATUS_DONE = "done";
    const STATUS_FAIL = "fail";

    public static function save($sender, $receiver, $data){
        $dataArray = [
            "sender" => $sender,
            "receiver" => $receiver,
            "data" => $data,
            "nbAttempt" => 0,
            "status" => self::STATUS_PENDING,
            "createAt" => new MongoDate()
        ];
        Yii::app()->mongodb->selectCollection(self::COLLECTION)->insert($dataArray);
    }

    public static function processCron(){
        $jobs = PHDB::findAndSort(self::COLLECTION, ["status"=>self::STATUS_PENDING], ["createAt" => 1], self::EXEC_COUNT);

        foreach($jobs as $job){
            $nbAttempt = intval($job["nbAttempt"])+1;

            if(in_array($job["data"]["type"], ["Create", "Update", "Undo"])){
                    $object = isset($job["data"]["object"]) ? Type::createFromAnyValue($job["data"]["object"]) : null;
                    if($object!=null){
                        $job["data"]["object"] = $object->toArray();
                        $job=Utils::jobToRunBeforePost($job);
                    }
            }
            $success = Request::post($job["sender"], $job["receiver"], $job["data"]);

            $data = [
                "updateAt" => new MongoDate(),
                "nbAttempt" => $nbAttempt
            ];
            if($success)
                $data["status"] = self::STATUS_DONE;
            else
                $data["status"] = $nbAttempt > self::MAX_ATTEMPT ? self::STATUS_FAIL : self::STATUS_PENDING;

            PHDB::update(self::COLLECTION, ["_id"=>$job["_id"]],['$set' => $data]);
        }
    }
}
