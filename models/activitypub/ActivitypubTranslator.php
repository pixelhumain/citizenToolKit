<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub;

use Exception;
use MongoDate;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Group;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Project;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object\Image;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object\Note;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\news\models\UrlExtractor;
use Yii;

class ActivitypubTranslator
{
    //get actor adress (ex: actorname@domain.com) from Actor
    public static function getActorAdress(AbstractActor $actor, $withoutDot = false): string
    {
        $address = $actor->get("preferredUsername") . "@" . parse_url($actor->get("id"), PHP_URL_HOST);
        if ($withoutDot)
            $address = str_replace(".", "_", $address);
        return $address;
    }
    public static function setActivityPubProperties($object, $type, $slug, $image_url = null)
    {
        $object->setPreferredUsername($slug);
        $object->setInbox(Utils::createUrl("api/activitypub/inbox/p/" . $slug));
        $object->set_extend_props("manuallyApprovesFollowers", true);
        $object->setPublicKey([
            "id" => Utils::createUrl("api/activitypub/" . $type . "\/p/" . $slug . "#main-key"),
            "owner" => Utils::createUrl("api/activitypub/" . $type . "/p/" . $slug),
            "publicKeyPem" => Utils::getPem("public")
        ]);
        $object->setEndpoints([
            "sharedInbox" => Utils::createUrl("api/activitypub/inbox")
        ]);

        if (isset($image_url)) {
            $fileExtension = pathinfo($image_url, PATHINFO_EXTENSION);
            $mediaType = ($fileExtension == "png") ? "image/png" : "image/jpeg";

            $icon = new Image();
            $icon->set("url", Utils::createUrl($image_url));
            $icon->set("mediaType", $mediaType);
            $object->set("icon", $icon);
        }
    }
    //convert CO user to activitystream actor
    public static function coPersonToActor($person): AbstractActor
    {

        $actor = new Person();
        $actor->set_context([
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1",
            [
                "manuallyApprovesFollowers" => "as:manuallyApprovesFollowers",
            ]
        ]);

        $actor->setId(Utils::createUrl("api/activitypub/users/u/" . $person["username"]));
        $actor->setName($person["name"]);
        $actor->setPreferredUsername($person["username"]);
        $actor->setInbox(Utils::createUrl("api/activitypub/inbox/u/" . $person["username"]));
        $actor->set_extend_props("manuallyApprovesFollowers", true);
        $actor->setPublicKey([
            "id" => Utils::createUrl("api/activitypub/users/u/" . $person["username"] . "#main-key"),
            "owner" => Utils::createUrl("api/activitypub/users/u/" . $person["username"]),
            "publicKeyPem" => Utils::getPem("public")
        ]);
        $actor->setEndpoints([
            "sharedInbox" => Utils::createUrl("api/activitypub/inbox"),
            "followers" => Utils::createUrl("api/activitypub/followers/u/" . $person["username"]),
            "projects" => Utils::createUrl("api/activitypub/projects/p/" . $person["username"]),
        ]);

        return self::getIcon($actor);
    }

    public static function noteToNewsMedia(Note $note)
    {
        $actor = Type::createFromAnyValue($note->get("attributedTo"));

        $media = [
            "id" => $note->get("id"),
            "content" => $note->get("content"),
            "text" => $note->get("content"),
            "attachments" => [],
            "nbReplies" => 0,
            "url" => $note->get("url") ? $note->get("url") : $note->get("id")
        ];

        if (is_array($note->get("attachment"))) {
            foreach ($note->get("attachment") as $attachment) {
                $isValidImageType = in_array($attachment->get("mediaType"), ['image/png', 'image/jpeg', 'image/jpg']);
                $isValidUrl = filter_var($attachment->get("url"), FILTER_VALIDATE_URL);

                if ($isValidImageType && $isValidUrl)
                    $media["attachments"][] = $attachment->get("url");
            }
        }

        $media["author"] = [
            "id" => $actor->get("id"),
            "name" => $actor->get("name"),
            "address" => self::getActorAdress($actor),
            "avatar" => Yii::$app->getModule('co2')->assetsUrl . "/images/avatar.jpg"
        ];
        if ($actor->get("icon") && $actor->get("icon")->get("type") == "Image") {
            $media["author"]["avatar"] = $actor->get("icon")->get("url");
        }

        //fetch provider info
        $actorIdParts = parse_url($actor->get("id"));
        $actorSite = $actorIdParts["scheme"] . "://" . $actorIdParts["host"];
        $noteArray = $note->toArray();
         if(isset($noteArray['providerInfo'])){
            $providerInfo= $noteArray['providerInfo'];
       }else{
         $providerInfo = UrlExtractor::extract($actorSite);
       }

        $media["providerInfo"] = [
            "title" => isset($providerInfo["title"]) ? $providerInfo["title"] : $actorIdParts["host"]
        ];
        if (isset($providerInfo["meta"]["description"]))
            $media["providerInfo"]["description"] = $providerInfo["meta"]["description"];
        if (isset($providerInfo["link"]["icon"]))
            $media["providerInfo"]["icon"] = $providerInfo["link"]["icon"];

        return $media;
    }
    //convert CO group to activitystream actor
    public static function coGroupToActor($group): AbstractActor
    {
        $gactor = new Group();
        $gactor->set_context([
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1",
            [
                "manuallyApprovesFollowers" => "as:manuallyApprovesFollowers",
            ]
        ]);
        $gactor->setId(Utils::createUrl("api/activitypub/groups/g/" . $group["slug"]));
        $gactor->setId(Utils::createUrl("api/activitypub/groups/g/" . $group["slug"]));
        $gactor->setName($group["slug"]);
        $gactor->setPreferredUsername($group["slug"]);
        $gactor->setInbox(Utils::createUrl("api/activitypub/inbox/g/" . $group["slug"]));
        $gactor->set_extend_props("manuallyApprovesFollowers", true);
        $gactor->setPublicKey([
            "id" => Utils::createUrl("api/activitypub/groups/g/" . $group["slug"] . "#main-key"),
            "owner" => Utils::createUrl("api/activitypub/groups/g/" . $group["slug"]),
            "publicKeyPem" => Utils::getPem("public")
        ]);
        $gactor->setEndpoints([
            "sharedInbox" => Utils::createUrl("api/activitypub/inbox")
        ]);

        return self::getIcon($gactor);
    }

    public static function  actorTocoUser($value)
    {
        $date = new MongoDate(time());
        if (!isset($value["invitorId"]) || !filter_var($value["invitorId"], FILTER_VALIDATE_URL)) return;
        $adress = Utils::getLinkToAdress($value["invitorId"]);
        $actor = ActivitypubActor::searchActorByRessourceAddress($adress);
        $actorArray = $actor->toArray();
        $data = array(
            "id" => $value['invitorId'],
            "rolesAndStatusLink" => array(
                "roles" => isset($value['roles']) ? $value['roles'] : []
            ), "activitypub" => true,
            "collection" => "citoyens",
            "name" => $actorArray['name'],
            'created' => $date,
            'updated' => $date,
            "sorting" => $date,
            "preferences" => array("activitypub" => true)
        );
        if (isset($value['isAdmin'])) {
            $data['rolesAndStatusLink']['isAdmin'] = true;
        }
        if (isset($value['isInviting'])) {
            $data['rolesAndStatusLink']['isInviting'] = true;
        }
        if (isset($value['isInviting'])) {
            $data['rolesAndStatusLink']['isInviting'] = true;
        }
        if (isset($value['isInviting'])) {
            $data['rolesAndStatusLink']['isInviting'] = true;
        }
        if (isset($value['isAdminPending'])) {
            $data['rolesAndStatusLink']['isAdminPending'] = true;
        }
        if (isset($value['toBeValidated'])) {
            $data['rolesAndStatusLink']['toBeValidated'] = true;
        }
        return $data;
    }
    public static function getIcon($actor)
    {
        if (isset($person["profilImageUrl"])) {
            $fileExtension = pathinfo($person["profilImageUrl"], PATHINFO_EXTENSION);
            $mediaType = ($fileExtension == "png") ? "image/png" : "image/jpeg";

            $icon = new Image();
            $icon->set("url", Utils::createUrl($person["profilImageUrl"]));
            $icon->set("mediaType", $mediaType);

            $actor->set("icon", $icon);
        }
        return $actor;
    }
}
