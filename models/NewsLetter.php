<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;

use CacheHelper;
use Cron;
use Element;
use Exception;
use Mail;
use MongoId;
use Person;
use PHDB;

class NewsLetter{
    const COLLECTION = "newsletter";
    const TYPE_TEMPLATE = "template";
    const TYPE_MAIL = "mail";
    private const URL_API_MJML = "https://api2.communecter.org/mjml/tohtml";

    public static function send($authorId, $contextType, $contextId, $mail){
        $author = Person::getById($authorId, false);
        if(!$author)
            throw new Exception("Person with id '".$authorId."' not found");

        $context = Element::getByTypeAndId($contextType, $contextId);
        if(!Element::getByTypeAndId($contextType, $contextId))
            throw new Exception("Element '".$contextType."' with id '".$contextId."' not found.");

        $communityEmails = [];
        $extenalEmails = isset($mail["receivers"]["external"]) ? $mail["receivers"]["external"]:[];

        if(isset($mail["receivers"]["community"])){
            foreach($mail["receivers"]["community"] as $id){
                $person = Person::getById($id, false);
                if($person && $person["email"]){
                    $communityEmails[] = $person["email"];
                }
            }
        }

        //save newsletter
        $newsletterUUID = uniqid("newsletter_");
        PHDB::insert(self::COLLECTION, [
            "uuid" => $newsletterUUID,
            "type" => self::TYPE_MAIL,
            "authorId" => $authorId,
            "context" => [
                "type" => $contextType,
                "id" => $contextId
            ],
            "receivers" => [
                "community" => $communityEmails,
                "external" => $extenalEmails
            ],
            "subject" => $mail["subject"],
            "data" => $mail["content"]
        ]);

        foreach(array_merge($communityEmails, $extenalEmails) as $receiver){
            $mailParams = [
                "type" => Cron::TYPE_MAIL,
                "tpl" => "newsletter",
                "subject" => $mail["subject"],
                "from" => $context["email"],
                "to" => $receiver,
                "tplParams" => [
                    "newsletter" => $newsletterUUID
                ]
            ];
            Mail::schedule($mailParams);
        }

        return $newsletterUUID;
    }

    public static function saveTemplate($authorId, $contextType, $contextId, $template){
        if(!Person::getById($authorId))
            throw new Exception("Person with id '".$authorId."' not found");

        if(!Element::getByTypeAndId($contextType, $contextId))
            throw new Exception("Element '".$contextType."' with id '".$contextId."' not found.");

        if(!isset($template["name"]) || !isset($template["page"]))
            throw new Exception("Missing template parameters");

        $uuid = uniqid("newsletter_");
        $data = [
            "uuid" => $uuid,
            "type" => self::TYPE_TEMPLATE,
            "name" => $template["name"],
            "authorId" => $authorId,
            "context" => [
                "type" => $contextType,
                "id" => $contextId
            ],
            "data" => $template["page"]
        ];
        if(isset($template["image"]))
            $data["image"] = $template["image"];
        if(isset($template["ressources"]))
            $data["ressources"] = $template["ressources"];

        PHDB::insert(self::COLLECTION, $data);

        return $uuid;
    }

    public static function getNewsLetterPage($uuid){
        if($page = CacheHelper::get($uuid))
            return $page;
        
        $newsletter = PHDB::findOne(self::COLLECTION, ["uuid" => $uuid]);
        if(!$newsletter)
            return "";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL_API_MJML);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch, 
            CURLOPT_POSTFIELDS, 
            json_encode([
                "data" => $newsletter["data"]
            ])
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $res = json_decode(curl_exec($ch), true);
        curl_close($ch); 

        $res = isset($res["html"]) ? $res["html"]:"";
        CacheHelper::set($uuid, $res);

        return $res;
    }

    public static function getTemplate($uuid=null){
        if($uuid){
            $template = PHDB::findOne(self::COLLECTION, ["uuid" => $uuid]);
            if($template){
                $template["author"] = Person::getById($template["authorId"]);
            }
            return $template;
        }
        else{
            $templates = PHDB::find(self::COLLECTION, ["type" => self::TYPE_TEMPLATE]);
            foreach($templates as $key => $template){
                $templates[$key]["author"] = Person::getById($template["authorId"]);
            }
            return $templates;
        }   
    }

    public static function updateTemplate($userId, $templateId, $data){
        $template = PHDB::findOneById(self::COLLECTION, $templateId);
        if(!$template)
            throw new Exception("Template not found.");
        
        if($template["authorId"] !== $userId)
            throw new Exception("You can't update this template.");

        PHDB::update(self::COLLECTION, [ "_id" => new MongoId($templateId) ], [
            '$set' => $data
        ]);

        return $templateId;
    }

    public static function deleteTemplate($userId, $templateId){
        $template = PHDB::findOneById(self::COLLECTION, $templateId);
        if(!$template)
            throw new Exception("Template not found.");
        
        if($template["authorId"] !== $userId)
            throw new Exception("You can't delete this template.");

        PHDB::remove(self::COLLECTION, ["_id" => new MongoId($templateId)]);

        return $templateId;
    }

    public static function getCommunityToSendMail($params){
        $low = [
            
        ];
    }
}