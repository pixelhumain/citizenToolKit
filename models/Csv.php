<?php
class Csv
{

	public static function getCsvAdmin($post) {
        //$costum = CacheHelper::getCostum();

		if(Costum::isSameFunction("csvElement")){
			$resS = Costum::sameFunction("csvElement", $post);
            $elements = $resS["results"];
		} else {

			// Recherche la données en fonction des filtres
			$resS = SearchNew::searchAdmin($post);
			$elements = $resS["results"];
		}

		$fields = array();
		$elements = array();
		foreach ($resS["results"] as $key => $value) {
			// On parse les champs pour restruturer la données ou pour vérifier les caracteère qui pourrait faire buguer le parser csv
			if (isset($_POST["roles"]) && is_array($_POST["roles"])) {
				$value = self::parseFieldsRoles($value, $_POST["roles"]);
			} else {
				$value = self::parseFields($value);
			}
			$elements[] = $value;

			// On récupere la liste des fields
			$fields = ArrayHelper::getAllPathJson(json_encode($value), $fields, false);

		}

		if(!empty($post["fields"])){
			$sortOrder = array();
			$fieldsMultiple=array();
			// On parcoure les fields afin de les remettre dans le bonne ordre
			foreach ($post["fields"] as $keysortOrder => $valsortOrder) {
				if(empty( $fieldsMultiple[$valsortOrder]))
					$fieldsMultiple[$valsortOrder] = array();
				foreach ($fields as $key => $value) {
					$pos = strpos($value, $valsortOrder);
					if($pos !== false)
						$fieldsMultiple[$valsortOrder][] = $value;
				}
				sort($fieldsMultiple[$valsortOrder], SORT_FLAG_CASE);
				$sortOrder = array_merge($sortOrder, $fieldsMultiple[$valsortOrder]);
			}
			$fields = $sortOrder;
		}
		// On retourne la donnée pour le parser csv 
		$res = array("results" => $elements, "fields"=> $fields);
		return $res;
    }

    public static function parseFields($elt) {
    	if(!empty($elt["_id"])){
    		$elt["id"] = (String)$elt["_id"];
			unset($elt["_id"]);
    	}
    	
    	// Restructuration des scopes
        if(!empty($elt["scope"])){
        	$scope = array();
        	foreach ($elt["scope"] as $keyS => $valScope) {
        		if(!empty($valScope["name"])) {
                    $scope[] = $valScope["name"];
                }elseif (!empty($valScope["cityName"]) && !empty($valScope["postalCode"])){
        		    $scope[] = $valScope["cityName"] . " " . $valScope["postalCode"];
                }
        	}
        	$elt["scope"] = $scope;
        }

        // Restructuration des parents
        if(!empty($elt["parent"])){
        	$parent = array();
        	foreach ($elt["parent"] as $keyS => $valScope) {
        		if(!empty($valScope["name"]))
        			$parent[] = $valScope["name"];
        	}
        	$elt["parent"] = $parent;
        }

        // On enleve les # qui emepche le fonctionne du parser csv
        foreach ($elt as $key => $value) {
        	if(is_string($value))
        		$elt[$key] = str_replace("#", "", $value);
        }

        return $elt;
    }

    public static function parseFieldsRoles($elt, $roles) {
    	$elt = self::parseFields($elt);

		// Restructuration des roles
        if(isset($elt["links"]) && isset($elt["links"]["contributors"]) && is_array($elt["links"]["contributors"])){

        	foreach ($elt["links"]["contributors"] as $keyS => $valScope) {
        		if(isset($valScope["roles"]) && is_array($valScope["roles"])){
        			
        			$result=array_intersect($valScope["roles"],$roles);
        			foreach ($result as $keyI => $valueI) {
        			    if (!empty(PHDB::findOneById( $valScope["type"] , $keyS ,["name"])["name"])){
                            ${$valueI}[] = PHDB::findOneById( $valScope["type"] , $keyS ,["name"])["name"];
                        }else{
        			        ${$valueI}[] = null;
                        }
        				
        			}
        			// $parent[] = $valScope["name"];
        		}
        	}

        	foreach ($roles as $keyR => $valueR) {
        		if (isset(${$valueR})) {
        			$elt[$valueR] = ${$valueR};
        		}
        	}
        }

        unset($elt["links"]);
        return $elt;
    }
}