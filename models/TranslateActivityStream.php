<?php
class TranslateActivityStream {

	//WEBFINGER
	public static $dataBinding_webfinger = [
		"subject" => ["valueOf"=>"subject"],
		"links" => [
			[
				"rel" => "self",
				"type" => "application/activity+json",
				"href" => [
					"valueOf"  	=> 'username', 
					"type" 	=> "url", 
					"prefix"   => "/api/activitypub/person/u/"
				]
			]
		]
	];

	//ACTOR
	public static $dataBinding_actor = [
		"@context"=> [
			"https://www.w3.org/ns/activitystreams",
			"https://w3id.org/security/v1",
			[
				"manuallyApprovesFollowers"=> "as:manuallyApprovesFollowers",
				"toot"=> "http://joinmastodon.org/ns#",
				"featured"=> [
					"@id"=> "toot:featured",
					"@type"=> "@id"
				],
				"featuredTags"=> [
					"@id"=> "toot:featuredTags",
					"@type"=> "@id"
				],
				"alsoKnownAs"=> [
					"@id"=> "as:alsoKnownAs",
					"@type"=> "@id"
				],
				"movedTo"=> [
					"@id"=> "as:movedTo",
					"@type"=> "@id"
				],
				"schema"=> "http://schema.org#",
				"PropertyValue"=> "schema:PropertyValue",
				"value"=> "schema:value",
				"IdentityProof"=> "toot:IdentityProof",
				"discoverable"=> "toot:discoverable",
				"Device"=> "toot:Device",
				"Ed25519Signature"=> "toot:Ed25519Signature",
				"Ed25519Key"=> "toot:Ed25519Key",
				"Curve25519Key"=> "toot:Curve25519Key",
				"EncryptedMessage"=> "toot:EncryptedMessage",
				"publicKeyBase64"=> "toot:publicKeyBase64",
				"deviceId"=> "toot:deviceId",
				"claim"=> [
					"@type"=> "@id",
					"@id"=> "toot:claim"
				],
				"fingerprintKey"=> [
					"@type"=> "@id",
					"@id"=> "toot:fingerprintKey"
				],
				"identityKey"=> [
					"@type"=> "@id",
					"@id"=> "toot:identityKey"
				],
				"devices"=> [
					"@type"=> "@id",
					"@id"=> "toot:devices"
				],
				"messageFranking"=> "toot:messageFranking",
				"messageType"=> "toot:messageType",
				"cipherText"=> "toot:cipherText",
				"suspended"=> "toot:suspended"
			]
		],
		"id" => [
			"valueOf"  	=> 'username', 
			"type" 	=> "url", 
			"prefix"   => "/api/activitypub/person/u/"
		],
		"type" => [
			"valueOf" => "type"
		],
		"name" => [
			"valueOf" => "name"
		],
		"preferredUsername" => [
			"valueOf" => "username"
		],
		"inbox" => [
			"valueOf" => "username",
			"type" => "url",
			"prefix" => "/api/activitypub/inbox/u/"
		],
		"manuallyApprovesFollowers" => true,
    	"discoverable" => false,
		"publicKey" => [
			"id" => [
				"valueOf"  	=> 'username', 
				"type" 	=> "url", 
				"prefix"   => "/api/activitypub/person/u/",
				"suffix" => "#main-key"
			],
			"owner" => [
				"valueOf"  	=> 'username', 
				"type" 	=> "url", 
				"prefix"   => "/api/activitypub/person/u/"
			],
			"publicKeyPem" => ["valueOf"=>"publicKeyPem"]
		],
		"endpoints" => [
			"sharedInbox" => "http://communecter-dev/api/activitypub/sharedinbox"
		]
	];

	//ORDERED COLLECTION
	public static $dataBinding_orderedcollection = [
		"@context" => "https://www.w3.org/ns/activitystreams",
		"id" => [
			"type" => "url",
			"valueOf" => "endpoint",
			"prefix" => "/api/activitypub"
		],
		"type" => "OrderedCollection",
		"totalItems" => [
			"valueOf" => "totalItems"
		],
		"first" => [
			"type" => "url",
			"valueOf" => "first",
			"prefix" => "/api/activitypub"
		],
		"last" => [
			"type" => "url",
			"valueOf" => "last",
			"prefix" => "/api/activitypub"
		]
	];

	//ORDERED COLLECTION PAGE
	public static $dataBinding_orderedcollectionpage = [
		"@context" => "https://www.w3.org/ns/activitystreams",
		"id" => [
			"type" => "url",
			"valueOf" => "endpoint",
			"prefix" => "/api/activitypub"
		],
		"type" => "OrderedCollectionPage",
		"next" => [
			"type" => "url",
			"valueOf" => "next",
			"prefix" => "/api/activitypub"
		],
		"prev" => [
			"type" => "url",
			"valueOf" => "prev",
			"prefix" => "/api/activitypub"
		],
		"partOf" => [
			"type" => "url",
			"valueOf" => "partOf",
			"prefix" => "/api/activitypub"
		],
		"orderedItems" => [
			"valueOf" => "items"
		]
	];

	//ACTIVITY
	public static $dataBinding_activity = [
		"@context" => "https://www.w3.org/ns/activitystreams",
		"id" => [
			"valueOf" => "id",
			"type" => "url",
			"prefix" => "/api/activitypub/outbox/id/"
		],
		"type" => ["valueOf"=>"type"],
		"actor" => [
			"valueOf" => "actor",
		],
		"object" => [
			"valueOf" => "object"
		],
		"to" => ["valueOf" => "to"],
		"cc" => ["valueOf" => "cc"]
	];

	//OBJECT
	public static $dataBinding_object = [
		"@context" => "https://www.w3.org/ns/activitystreams",
		"id" => [
			"valueOf" => "id",
			"type" => "url",
			"prefix" => "/api/activitypub/outbox/id/"
		],
		"type" => ["valueOf" => "type"],
		"content" => ["valueOf" => "content"],
		"attributedTo" => ["valueOf" => "attributedTo"],
		"published" => ["valueOf" => "published"],
		"to" => ["valueOf" => "to"],
		"attachment"=>["valueOf"=>"attachment"]
	];

	////PROPERTIES
	public static $dataBinding_attachment = [
		"type" => "Image",
		"content" => ["valueOf"=>"content"],
		"url" => ["valueOf" => "url"]
	];
}

