<?php

/*
badges collection : badges
    "tag" : "crowdfunder",
    "label" : "Crowdfunder",
    "icon" : [ 
        "fa-euro", 
        "fa-bookmark"
    ],
    "img" : "",
    color : ""
[x] add badges based on tags 
[x] add a new badge : connect a badge to a tag 
[ ] edit
	carefull about moderation and loosing what someone else cose 
	edting with consensus or versions
[x] count the number of people carrying a tag or badge 
[x] add color to dynform 
[ ] color picker
[x] add image type badges
[ ] badges == une facon d'unifier les tags 
[ ] clean up dans les tags
[ ] listing de tout les icone font awesome
[ ] communities and expert networks based on badges
[ ] page competence : in this community we do this (list of badges)
	see community by badges
[ ] how to connect badges and roles 
	maybe prefill roles with badges when inviting someone 
[ ] comment faire des badge ambiance dans un contexte
	ex : coecrire l'emotion autour d'un event 
	ce serait avec une interface real time
[ ] test duplicate badges based on duplicate tags
[ ] add this badge btn > simply adds the corresponding tag to the person
	on each badge btn "I am" > adds the tag to the persons profile
	[ ] une fois ajouter show all people with a badge , btn in my scope
	[ ] btn : show activity of a badge (of a tag behind a badge )
[ ] survey : who are you based on tags and badges
[ ] many tag badge : badges that only show when a all given tags exist
[ ] moderation ou notification de la création d'un neau badge
[ ] in index page if click a badge preview list of people, or activity of that tag

*/

use yii\helpers\VarDumper;

class Badge {
	
	const COLLECTION = "badges"; 
	const CONTEXT = "https://w3id.org/openbadges/v2";
	public static $dataBinding = array (
        "name" => array("name" => "name", "rules" => array("required")),
        "collection" => array("name" => "collection"),
        "icon" => array("name" => "icon"),
        "color" => array("name" => "color"),
        "tags" => array("name" => "tags", "rules" => array("required")),
        "img" => array("name" => "img"),
        "isParcours" => array("name" => "isParcours","rules" => array("required")),
        "parent" => array("name" => "parent"),
        "description" => array("name" => "description"),
        "criteria" => array("name" => "criteria"),
        "synergie" => array("name" => "synergie"),
        "structags" => array("name" => "structags"),
        "source" => array("name" => "source"),
        "category" => array("name" => "category"),
        "modified" => array("name" => "modified"),
        "updated" => array("name" => "updated"),
        "creator" => array("name" => "creator"),
        "created" => array("name" => "created"),
		"isOpenData" => array("name" => "isOpenData"),
		"preferences" => array("name" => "preferences"),
		"issuer" => array("name" => "issuer"),
		"assertion" => array("name" => "assertion"),
		"badgeClass" => array("name" => "badgeClass"),
		"type" => array("name" => "type"),
		"input" => array("name" => "input"),
    );

	public static function getById($id) {

	  	$badge = PHDB::findOne(self::COLLECTION,array("_id"=>new MongoId($id)));
	  	
	  	if (!empty($badge)) {
			$badge["typeSig"] = "badges";

        }
	  	return $badge;
	}

	public static function getBagdes($idItem, $typeItem) {
		$badges = array();
		$account = PHDB::findOneById($typeItem ,$idItem, array("badges"));
		if(!empty($account["badges"]))
			$badges = $account["badges"];
		return $badges;
	}
	public static function getByWhere($where, $img=false) {
		//$badges = array();
		return PHDB::find(self::COLLECTION,$where);
		/*if(!empty($account["badges"]))
			$badges = $account["badges"];
		return $badges;*/
	}
	public static function getOneByWhere($where, $img=false) {
		//$badges = array();
		return PHDB::findOne(self::COLLECTION,$where);
		/*if(!empty($account["badges"]))
			$badges = $account["badges"];
		return $badges;*/
	}
	public static function getBadgesByLinks($links) {
		$badges = array();
		foreach ($links as $sec => $l) {
			foreach ($l as $id => $v) {
				if(@$v["type"]){
					$el = Element::getByTypeAndId( $v["type"],$id );
					if( @$el["preferences"]["badge"] ){
						$b = array( "name"=>$el["name"],
								    "type"=>$v["type"]);
						if(!empty($el["profilThumbImageUrl"]))
							$b["img"] = $el["profilThumbImageUrl"];
						$badges[] = $b;
					}
				}
			}
		}
		
		return $badges;
	}

	public static function checkBadgeInListBadges($badge, $badges) {
		
		$res = false ;
		/*foreach ($badges as $key => $value) {
			if($badge == $value["name"]){
				$res = true ;
				break;
			}
		}*/
		return (@$badges[$badge]) ? true : false;
	}

	public static function addBadgeInListBadges($badge, $badges) {
		$res = array(	"result" => false, 
						"badges" => $badges, 
						"msg" => Yii::t("import","Le badge est déjà dans la liste"));
		if(is_array($badge)){
			$newListBadges = array();
			foreach ($badge as $key => $value) {
				if(!self::checkBadgeInListBadges((empty($value["name"])?$value:$value["name"]), $badges)){
					$newBadge["name"] = (empty($value["name"])?$value:$value["name"]);
					$newBadge["date"] = (empty($value["date"])?new MongoDate(time()):$value["date"]);
					$newListBadges[] = $newBadge;
				}
			}
			$badges = array_merge($badges, $newListBadges);
			$res = array("result" => true, "badges" => $badges);

		}else if(is_string($badge)){
			if(!self::checkBadgeInListBadges($badge, $badges)){
				$newBadge["name"] = $badge;
				$newBadge["date"] = new MongoDate(time());
				$badges[] = $newBadge;
				$res = array("result" => true, "badges" => $badges);
			}
		}
		return $res;
	}

	public static function updateBadges($badges, $idItem, $typeItem) {
		return Element::updatePathValue($typeItem, $idItem, "badges", $badges);
	}

	public static function addAndUpdateBadges($nameBadge, $idItem, $typeItem) {
		$badges = self::getBagdes($idItem, $typeItem);
		if(empty($badges))
           $badges = array();

       	$resAddBadge = self::addBadgeInListBadges($nameBadge, $badges);
		if($resAddBadge["result"] == true){
			$res = self::updateBadges($resAddBadge["badges"], $idItem, $typeItem);
		}else
			$res = array("result" => false, "msg" => $resAddBadge["msg"]);

		return $res;
	}


	public static function conformeBadges($badges) {
		$newListBadges = array();
		if(is_array($badges)){
			foreach ($badges as $key => $value) {
				$newBadge["name"] = (empty($value["name"])?$value:$value["name"]);
				$newBadge["date"] = (empty($value["date"])?new MongoDate(time()):$value["date"]);
				$newListBadges[] = $newBadge;
			}
		}else if(is_string($badges)){
			$newBadge["name"] = $badges;
			$newBadge["date"] = new MongoDate(time());
			$newListBadges[] = $newBadge;
		}
		return $newListBadges;
	}


	public static function delete($nameBadge, $idItem, $typeItem) {
		$badges = self::getBagdes($idItem, $typeItem);
		$newBadges = array();
		foreach ($badges as $key => $badge) {
			if($badge["name"] != $nameBadge)
				$newBadges[$key] = $badge;
		}
		$res = self::updateBadges($newBadges, $idItem, $typeItem);
		return $res ;
	}

	public static function getBadgeChildrenTree($idBadge, $badgeAssigned = null){
		$badge = Badge::getById((string) $idBadge);
		if($badge!=null){
			$badge["id"] = (string) $badge["_id"];
			$badge = Badge::dfsChildrenTree($badge, $badgeAssigned);
			return $badge;
		}else{
			return array();
		}
	}

	private static function dfsChildrenTree($badge, $badgeAssigned = null){
		if(!empty($badge)){
			$children = PHDB::find(Badge::COLLECTION, ['$and' => [['parent.'.((string) $badge["_id"] ) => ["\$exists" => true]], ["_id" => ['$ne' =>  new MongoId((string) $badge["_id"])]]]]);
			if(!empty($children)){
				$count = 0;
				$childrenLocked = 0;
				foreach ($children as $id => $child) {
					$child["id"] = (string) $id;
					$children[$id] = Badge::dfsChildrenTree($child, $badgeAssigned);
					$count += $children[$id]["childrenCount"] + 1;
					$childrenLocked += $children[$id]["childrenLocked"];
					if(!empty($badgeAssigned)){
						if(!in_array((string) $id ,$badgeAssigned)){
							$children[$id]["locked"] = true;
							$childrenLocked++;
						}
					}
				}
				$badge["children"] = array_values($children);
				$badge["childrenCount"] = $count;
				$badge["childrenLocked"] = $childrenLocked;
			}else{
				$badge["childrenCount"] = 0;
				$badge["childrenLocked"] = 0;
			}
		}
		return $badge;
	}
	public static function categorizeBadge ($badges,$category){
        $badgesOption = [];
        foreach ($badges as $kbg => $vbg) {
            if(isset($vbg["parent"])){
                $parentId = array_keys($vbg["parent"])[0];
                $parent = PHDB::findOneById(Badge::COLLECTION,$parentId);
            }elseif(!isset($vbg["parent"])){
				$parentId = $kbg;
				$parent = $vbg;
			}
			$bdg = PHDB::find(Badge::COLLECTION,["parent.".$parentId => array('$exists'=>  true),"category"=>$category],["name"]);
			$badgeName = [];
			foreach ($bdg as $kbdg => $vbdg) {
				$badgeName[] = $vbdg["name"];
			}
			$badgesOption[@$parent["name"]] =  $badgeName;
        }
        return $badgesOption;
    }

	public static function canEdit($id, $userId = null){
		$userId = $userId ?? Yii::app()->session["userId"];
		if(empty($userId)){
			return false;
		}
		$badge = PHDB::findOne(self::COLLECTION,array("_id"=>new MongoId($id)), ["issuer"]);
		if(empty($badge)){
			return false;
		}
		$typeIssuer = null;
		$idIssuer = null;
		foreach($badge['issuer'] as $key => $issuer){
			$typeIssuer = $issuer['type'];
			$idIssuer = $key;
		}
		return Authorisation::canEditItem($userId,$typeIssuer, $idIssuer);
	}

	public static function afterSave($badge){
		$target = [];

		if(isset($badge["issuer"])){
			foreach($badge["issuer"] as $id => $issuer){
				$target = [
					"id" => $id,
					"type" => $issuer["type"]
				];
				if(isset($issuer["name"]))
					$target["name"] = $issuer["name"];
			}
		}

		Notification::constructNotification(
			ActStr::VERB_ADD,
			[
				"id" => Yii::app()->session["userId"],
				"name" => Yii::app()->session["name"],
				"type" => Person::COLLECTION
			],
			$target,
			[
				"type" => Badge::COLLECTION,
				"id" => (string) $badge["_id"],
				"name" => $badge["name"]
			],
			Badge::COLLECTION
		);
	}
	public static function cleanAllElementsLinks($id){
		//REMOVE ASSIGNATION
		PHDB::update(Organization::COLLECTION, ['badges.' . $id => ['$exists' => true]], ['$unset' => ['badges.' . $id => true]]);
		PHDB::update(Person::COLLECTION, ['badges.' . $id => ['$exists' => true]], ['$unset' => ['badges.' . $id => true]]);
		PHDB::update(Project::COLLECTION, ['badges.' . $id => ['$exists' => true]], ['$unset' => ['badges.' . $id => true]]);
		PHDB::update(Badge::COLLECTION, ['parent.' . $id => ['$exists' => true]],  ['$unset' => ['parent.' . $id => true]]);
		//TODO: CLEAN ENDORSSEMENT 
	}

	public static function getChild($parent = array()) {
		$where = array(
			'$and' => array()
		);
		foreach($parent as $parentId){
			array_push($where['$and'], array("parent.".$parentId  => array('$exists'=>1)));
		}

		return self::getByWhere($where);
	}
}

?>