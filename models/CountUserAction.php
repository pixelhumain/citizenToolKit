<?php


class CountUserAction
{

    const COLLECTION = "countUserActions";


    public static function incNbAction($data)
    {

        $exists = PHDB::findOne(self::COLLECTION, array("costumSlug" => $data["costumSlug"], "userId" => $data["userId"]));
        if($exists == null) {
            $dataToSave["costumId"] = $data["costumId"];
            $dataToSave["costumSlug"] = $data["costumSlug"];
            $dataToSave["userId"] = $data["userId"];
            $dataToSave["userName"] = $data["userName"];
            $dataToSave["action"][$data["action"]] = 1;
            PHDB::insert(self::COLLECTION, $dataToSave);
        }else {
            if (isset($exists["action"])  && isset($exists["action"][$data["action"]])) {
                $incAction = $exists["action"][$data["action"]];
                $arraySet = array('$set' => array("action.".$data["action"] => $incAction+1));

            } else {
                $arraySet = array('$set' => array(	"action.".$data["action"] => 1 ));
            }
            $res = PHDB::update( self::COLLECTION,
                array("costumSlug" => $data["costumSlug"], "userId" => $data["userId"]),
                $arraySet
            );
        }
    }

}

