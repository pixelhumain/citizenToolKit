<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;
class Evidence extends AbstractOpenBadge{
    public $type;
    public $id;
    public $narrative;
    public $name;
    public $description;
    public $genre;
    public $audience;
}