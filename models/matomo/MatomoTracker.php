<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\matomo;

use yii\base\Exception;

class MatomoTracker
{
	private $matomoUrl;
	private $siteUrl;
	private $tokenAuth;
	private $siteId;
	private $isSiteRegistered = false;

	public function __construct($matomoUrl, $tokenAuth)
	{
		$this->matomoUrl = $matomoUrl;
		$this->tokenAuth = $tokenAuth;
		$this->siteUrl = $this->getCurrentSiteUrl();
		if ($this->siteUrl !== null) {
			$this->siteId = $this->getSiteId();
			$this->isSiteRegistered = true;
		}
	}
	public function getCurrentSiteUrl()
	{
		if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
			$protocol = 'https://';
		} else {
			$protocol = 'http://';
		}

		return $protocol . $_SERVER['HTTP_HOST'];
	}
	public function getSiteId()
	{
		$url = $this->matomoUrl . 'index.php';
		$data = array(
			'module' => 'API',
			'method' => 'SitesManager.getSitesIdFromSiteUrl',
			'url' => $this->siteUrl,
			'format' => 'json',
			'token_auth' => $this->tokenAuth
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		if ($response != false) {
			$json = json_decode($response);
			if (!empty($json) && property_exists($json[0], 'idsite')) {
				return $json[0]->idsite;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
