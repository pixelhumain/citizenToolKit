<?php

class Nominatim {

	const ZOOM_BY_LEVEL = array(
		'1' => "3",
		'2' => null,
		'3' => '5',
		'4' => '8',
	);

	const NAME_BY_LEVEL = array(
		'1' => "country",
		'2' => "state",
		'3' => "state",
		'4' => 'county',
	);

	public static function getAddressByNominatim($params){
        try{
	        $url = "https://nominatim.openstreetmap.org/search?format=json&addressdetails=1" ;
	        if(!empty($params['text'])){
	            $url .= "&q=".urlencode($params['text']);
	            if(!empty($params['countryCode']) && is_string($params['countryCode']))
	            	$url .= "&country=".$params['countryCode'];
	            else if(!empty($params['countryCode']) && is_array($params['countryCode'])){
	            	$stringCC = implode(",", $params['countryCode']) ;
	            	$url .= "&countrycodes=".$stringCC;
	            }

				if(!empty($params['state']))
	            	$url .= "&state=".$params['state'];

	            if(!empty($params['namedetails']) && $params['namedetails'] === true)
	            	$url .= "&namedetails=1";

	            if(!empty($params['extratags']) && $params['extratags'] === true)
	            	$url .= "&extratags=1";

	            if(!empty($params['geoShape']) && $params['geoShape'] === true)
	        		$url .= "&polygon_geojson=1";

	            $url .= "&email=".Yii::app()->params["contactEmail"];
	            //echo $url; exit;
		        return json_decode( SIG::getUrl($url), true );
	        }
	        return null;
	        
		} catch (CTKException $e){
            return null ;
        }
    }

    public static function getElementByIdNominatim($params){
        try{
	        if(!empty($params['type']) && !empty($params['id'])){
	        	$url = "https://nominatim.openstreetmap.org/details.php?format=json&addressdetails=1" ;
	            $url .= "&osmtype=".$params['type']."&osmid=".$params['id'];

	            if(!empty($params['geoShape']) && $params['geoShape'] === true)
	        		$url .= "&polygon_geojson=1";

	            $url .= "&email=".Yii::app()->params["contactEmail"];
		        return json_decode( SIG::getUrl($url), true );
	        }
	        return null;
	        
		} catch (CTKException $e){
            return null ;
        }
    }

    public static function getCityByLatLon($lat, $lon){
        try{
	        return self::getZoneByLatLon($lat, $lon, "10",  true);
		} catch (CTKException $e){
            return null ;
        }
    }

    public static function getZoneByLatLon($lat, $lon, $zoom, $geoShape = false){
        try{
	        $url = "https://nominatim.openstreetmap.org/reverse?format=json" ;
	        $url .= "&lat=".urlencode($lat);
	        $url .= "&lon=".urlencode($lon);
	        if(!empty($geoShape) && $geoShape === true)
	        	$url .= "&polygon_geojson=1";
	        $url .= "&extratags=1";
	        $url .= "&namedetails=1";
	        $url .= "&zoom=".urlencode($zoom);
	        $url .= "&email=".Yii::app()->params["contactEmail"];
	        //var_dump($url); exit;
	        //return $url;
			return json_decode( SIG::getUrl($url), true );
		} catch (CTKException $e){
            return null ;
        }
    }

    public static function parseAddress($data, $params){
    	$res = array();
        if(!empty($data)){
        	//Rest::json($data); exit;
        	$countryCode = ( !empty($params["countryCode"]) ? $params["countryCode"] : null );
        	foreach($data as $key => $val) {
        		$city = null;
        		$postalCode = ( !empty($val["address"]) && !empty($val["address"]["postcode"]) ? $val["address"]["postcode"] : null );

        		$street = "";
				if( !empty($val["address"]["house_number"]) ){
					$street .= $val["address"]["house_number"]." ";
				}
				if( !empty($val["address"]["road"]) ){
					$street .= $val["address"]["road"];
				}
        		
        		$city = City::getCityByLatLng($val["lat"], $val["lon"], $postalCode, $countryCode) ;
        		//var_dump(count($cities));
        		//Rest::json($city); exit;
        		if( !empty($city) ){
    				
    				$res[] = City::createLocality($city, $street, $val["lat"], $val["lon"], $postalCode);
        		} else {
        			//$cityNominatim = self::getCityByLatLon($val["lat"], $val["lon"]);
        			$level1 = Zone::getCountryByCountryCode($countryCode);
        			$address =  array(
        				'@type' => 'PostalAddress',
		                'addressCountry' => $countryCode,
		                'level1' =>  (String)$level1["_id"],
		                'level1Name' =>  $level1["name"],
		                "streetAddress" => $street
        			);

        			if( !empty($postalCode) )
			            $address["postalCode"] = $postalCode;
			        if( !empty($val["address"]["city"]) )
						$address["addressLocality"] = $val["address"]["city"];
					

			        $address["name"] = City::createNameAdress($address);

        			$geo = SIG::getFormatGeo($val["lat"], $val["lon"]);
					$geoPosition = SIG::getFormatGeoPosition($val["lat"], $val["lon"]);

        			$res[] = array(
						"address" => $address,
						"geo" => $geo,
						"geoPosition" => $geoPosition,
						"new" => true,
						"lat" => $val["lat"],
						"lon" => $val["lon"],
						"api" => $val,
						"countryCode" => $countryCode,
					);
        		}
        	}
        }
        return $res;
    }


    public static function createCity($params, $userId, $cityNominatim = null){
		if(empty($cityNominatim))
    		$cityNominatim = self::getCityByLatLon($params["lat"], $params["lon"]);

    	if( !empty($cityNominatim) ) {

			$name = null ;
			$fieldName = (!empty($cityNominatim["namedetails"]) ? "namedetails" : "names");

			if(!empty($cityNominatim[$fieldName])){
				if( !empty($cityNominatim[$fieldName]["name:".strtolower(Yii::app()->language)]) ) {
					$name = $cityNominatim[$fieldName]["name:".strtolower(Yii::app()->language)] ;
				} else if( !empty($cityNominatim[$fieldName]["name"] ) ) {
					$name = $cityNominatim[$fieldName]["name"] ;
				}
			}

			$name = (!empty($name) ? $name : $cityNominatim["address"]["city"]) ;

			$wikidata = (empty($cityNominatim["extratags"]["wikidata"]) ? null : $cityNominatim["extratags"]["wikidata"]);
			$newCity = array( "name" => $name,
								"alternateName" => mb_strtoupper($name),
								"country" => $params['countryCode'],
								"geo" => SIG::getFormatGeo($cityNominatim["lat"], $cityNominatim["lon"]),
								"geoPosition" => SIG::getFormatGeoPosition($cityNominatim["lat"], $cityNominatim["lon"]),
								"osmID" => $cityNominatim["osm_id"]);
			$newCity["modified"] = new MongoDate(time());
			$newCity["updated"] = time();
	        $newCity["creator"] = $userId;
	        $newCity["created"] = time();
	        $newCity["new"] = true;

	        if(!empty($cityNominatim["geojson"]))
			    $newCity["geoShape"] = $cityNominatim["geojson"];
			else if(!empty($cityNominatim["geometry"]))
			    $newCity["geoShape"] = $cityNominatim["geometry"];

			if(!empty($wikidata))
				$newCity = City::getCitiesWithWikiData($wikidata, $newCity);

			if(empty($newCity["insee"]))
				$newCity["insee"] = $cityNominatim["osm_id"]."*".$params['countryCode'];

			if(empty($newCity["postalCodes"]))
				$newCity["postalCodes"] = array();

			$postalCodes = array();
			if(!empty($newCity["postalCodes"])){
	    		foreach ($newCity["postalCodes"] as $keyCP => $cp) {
	    			$newCP = array();
		    		$newCP["postalCode"] = $cp["postalCode"];
		    		$newCP["name"] = $cp["name"];
		    		$newCP["geo"] = $cp["geo"];
		    		$newCP["geoPosition"] = SIG::getFormatGeoPosition($cp["geo"]["latitude"], $cp["geo"]["longitude"]);
		    		$postalCodes[] = $newCP;
		    	}
	    	}
	    	$newCity["postalCodes"] = $postalCodes;

			$level1 = Zone::getCountryByCountryCode($newCity["country"]);
	    	$newCity["level1"] = (String)$level1["_id"];
	    	$newCity["level1Name"] = Zone::getNameOrigin($newCity["level1"]);

	    	$levelParents = array(
	    		"level1" => $newCity["level1"],
	    		"level1Name" => $newCity["level1Name"],
	    	);

	    	if(!empty($cityNominatim["address"]["state"])){
	    		$level2 = Zone::getLevelByNameAndCountry($cityNominatim["address"]["state"], "2", $newCity["country"]);
		    	if(!empty($level2) && !empty($level2["_id"])){
		    		$newCity["level2"] = (String)$level2["_id"];
	    			$newCity["level2Name"] = Zone::getNameOrigin($newCity["level2"]);
	    			$levelParents["level2"] = $newCity["level2"];
	    			$levelParents["level2Name"] = $newCity["level2Name"];
		    	}else{
		    		$level3 = Zone::getLevelByNameAndCountry($cityNominatim["address"]["state"], "3", $newCity["country"]);
			    	if(empty($level3)){
			    		$level3 = self::createLevel($cityNominatim["lat"], $cityNominatim["lon"], $params['countryCode'], "3", $levelParents);
			    	}

			    	if(!empty($level3["_id"])){
			    		$newCity["level3"] = (String)$level3["_id"];
		    			$newCity["level3Name"] = Zone::getNameOrigin($newCity["level3"]);
		    			$levelParents["level3"] = $newCity["level3"];
		    			$levelParents["level3Name"] = $newCity["level3Name"];
			    	}
		    	}
	    	}
	    	
		    if(!empty($cityNominatim["address"]["county"])){
		    	$level4 = Zone::getLevelByNameAndCountry($cityNominatim["address"]["county"], "4", $newCity["country"]);
		    	if(empty($level4)){
		    		$level4 = self::createLevel($cityNominatim["lat"], $cityNominatim["lon"], $params['countryCode'], "4", $levelParents);
		    	}

		    	if(!empty($level4["_id"])){
		    		$newCity["level4"] = (String)$level4["_id"];
	    			$newCity["level4Name"] = Zone::getNameOrigin($newCity["level4"]);
	    			$levelParents["level4"] = $newCity["level4"];
					$levelParents["level4Name"] = $newCity["level4Name"];
		    	}
		    }

	    	$translate = array();
			$info = array();
			$origin = $newCity["name"];
			if(!empty($zoneNominatim[$fieldName])){
				foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
					$arrayName = explode(":", $keyName);
					if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName)
						$translate[strtoupper($arrayName[1])] = $valueName;
				}
			}
			Yii::app()->mongodb->selectCollection(City::COLLECTION)->insert($newCity);
			$info["countryCode"] = $params['countryCode'];
			$info["parentId"] = (String)$newCity["_id"];
			$info["parentType"] = City::COLLECTION;
			$info["translates"] = $translate;
			$info["origin"] = $origin;
			Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($info);
			PHDB::update(City::COLLECTION, 
						array("_id"=>new MongoId((String)$newCity["_id"])),
						array('$set' => array("translateId" => (String)$info["_id"]))
			);

			
			$params["address"] = City::createAddressByCity(
							$newCity, 
							$params['address']['streetAddress'], 
							( !empty($params['address']['postalCode']) ? $params['address']['postalCode'] : null )
						);

			$res = array(
				"address" => $params["address"],
				"geo" => $params["geo"],
				"geoPosition" => $params["geoPosition"]
			);
			return $res ;
		}
    }

	public static function createLevel($lat, $lon, $countryCode, $level, $levelParents, $zoneNominatim=null){
		$zone = array();

		if(empty($zoneNominatim)){
			$zoneNominatim = self::getZoneByLatLon($lat, $lon, self::ZOOM_BY_LEVEL[$level]);
		}

		
		if( !empty($zoneNominatim)) {

			$fieldName = (!empty($zoneNominatim["namedetails"]) ? "namedetails" : "names");

			if(!empty($zoneNominatim[$fieldName]) && 
				!empty($zoneNominatim[$fieldName]["name"]) )
				$zone["name"] = $zoneNominatim[$fieldName]["name"];
			else
				$zone["name"] = $zoneNominatim["address"][self::NAME_BY_LEVEL[$level]];

			$zone["countryCode"] = $countryCode;
			$zone["level"] = array($level);
			if(!empty($levelParents)) {
				if(!empty($levelParents["level1"])){
					$zone["level1"] = $levelParents["level1"];
					$zone["level1Name"] = $levelParents["level1Name"];
				}
				if(!empty($levelParents["level2"])){
					$zone["level2"] = $levelParents["level2"];
					$zone["level2Name"] = $levelParents["level2Name"];
				}
				if(!empty($levelParents["level3"])){
					$zone["level3"] = $levelParents["level3"];
					$zone["level3Name"] = $levelParents["level3Name"];
				}
			}

			$zone["geo"] = SIG::getFormatGeo($zoneNominatim["lat"], $zoneNominatim["lon"]);
			$zone["geoPosition"] = SIG::getFormatGeoPosition($zoneNominatim["lat"], $zoneNominatim["lon"]);

			if(!empty($zoneNominatim["osm_id"]))
				$zone["osmID"] = $zoneNominatim["osm_id"];
			if(!empty($zoneNominatim["extratags"]["wikidata"]))
				$zone["wikidataID"] = $zoneNominatim["extratags"]["wikidata"];

			$translate = array();
			$info = array();
			$origin = $zone["name"];
			if(!empty($zoneNominatim[$fieldName])){
				foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
					$arrayName = explode(":", $keyName);
					if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName)
						$translate[strtoupper($arrayName[1])] = $valueName;
				}
			}

			Yii::app()->mongodb->selectCollection(Zone::COLLECTION)->insert($zone);
			$info["countryCode"] = $countryCode;
			$info["parentId"] = (String)$zone["_id"];
			$info["parentType"] = Zone::COLLECTION;
			$info["translates"] = $translate;
			$info["origin"] = $origin;
			$zone["translate"] = $info;

			Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($info);
			PHDB::update(Zone::COLLECTION, 
						array("_id"=>new MongoId((String)$zone["_id"])),
						array('$set' => array("translateId" => (String)$info["_id"]))
			);
				
		}

		return $zone;
	}


	public static function createCityByNominatim($params, $userId, $cityNominatim = null){
		if(empty($cityNominatim))
    		$cityNominatim = self::getCityByLatLon($params["lat"], $params["lon"]);

    	if( !empty($cityNominatim) ) {

			$name = null ;
			$fieldName = (!empty($cityNominatim["namedetails"]) ? "namedetails" : "names");

			if(!empty($cityNominatim[$fieldName])){
				if( !empty($cityNominatim[$fieldName]["name:".strtolower(Yii::app()->language)]) ) {
					$name = $cityNominatim[$fieldName]["name:".strtolower(Yii::app()->language)] ;
				} else if( !empty($cityNominatim[$fieldName]["name"] ) ) {
					$name = $cityNominatim[$fieldName]["name"] ;
				}
			}

			$name = (!empty($name) ? $name : $cityNominatim["address"]["city"]) ;

			$wikidata = (empty($cityNominatim["extratags"]["wikidata"]) ? null : $cityNominatim["extratags"]["wikidata"]);
			$newCity = array( "name" => $name,
								"alternateName" => mb_strtoupper($name),
								"country" => $params['countryCode'],
								"geo" => SIG::getFormatGeo($cityNominatim["lat"], $cityNominatim["lon"]),
								"geoPosition" => SIG::getFormatGeoPosition($cityNominatim["lat"], $cityNominatim["lon"]),
								"osmID" => $cityNominatim["osm_id"]);
			$newCity["modified"] = new MongoDate(time());
			$newCity["updated"] = time();
	        $newCity["creator"] = $userId;
	        $newCity["created"] = time();
	        $newCity["new"] = true;

	        if(!empty($cityNominatim["geojson"]))
			    $newCity["geoShape"] = $cityNominatim["geojson"];
			else if(!empty($cityNominatim["geometry"]))
			    $newCity["geoShape"] = $cityNominatim["geometry"];

			if(!empty($wikidata))
				$newCity = City::getCitiesWithWikiData($wikidata, $newCity);

			if(empty($newCity["insee"]))
				$newCity["insee"] = $cityNominatim["osm_id"]."*".$params['countryCode'];

			if(empty($newCity["postalCodes"]))
				$newCity["postalCodes"] = array();

			$postalCodes = array();
			if(!empty($newCity["postalCodes"])){
	    		foreach ($newCity["postalCodes"] as $keyCP => $cp) {
	    			$newCP = array();
		    		$newCP["postalCode"] = $cp["postalCode"];
		    		$newCP["name"] = $cp["name"];
		    		$newCP["geo"] = $cp["geo"];
		    		$newCP["geoPosition"] = SIG::getFormatGeoPosition($cp["geo"]["latitude"], $cp["geo"]["longitude"]);
		    		$postalCodes[] = $newCP;
		    	}
	    	}
	    	$newCity["postalCodes"] = $postalCodes;

			$level1 = Zone::getCountryByCountryCode($newCity["country"]);
	    	$newCity["level1"] = (String)$level1["_id"];
	    	$newCity["level1Name"] = Zone::getNameOrigin($newCity["level1"]);

	    	$levelParents = array(
	    		"level1" => $newCity["level1"],
	    		"level1Name" => $newCity["level1Name"],
	    	);

	    	if(!empty($cityNominatim["address"]["state"])){
	    		$level2 = Zone::getLevelByNameAndCountry($cityNominatim["address"]["state"], "2", $newCity["country"]);
		    	if(!empty($level2) && !empty($level2["_id"])){
		    		$newCity["level2"] = (String)$level2["_id"];
	    			$newCity["level2Name"] = Zone::getNameOrigin($newCity["level2"]);
	    			$levelParents["level2"] = $newCity["level2"];
	    			$levelParents["level2Name"] = $newCity["level2Name"];
		    	}else{
		    		$level3 = Zone::getLevelByNameAndCountry($cityNominatim["address"]["state"], "3", $newCity["country"]);
			    	if(empty($level3)){
			    		$level3 = self::createLevel($cityNominatim["lat"], $cityNominatim["lon"], $params['countryCode'], "3", $levelParents);
			    	}

			    	if(!empty($level3["_id"])){
			    		$newCity["level3"] = (String)$level3["_id"];
		    			$newCity["level3Name"] = Zone::getNameOrigin($newCity["level3"]);
		    			$levelParents["level3"] = $newCity["level3"];
		    			$levelParents["level3Name"] = $newCity["level3Name"];
			    	}
		    	}
	    	}
	    	
		    if(!empty($cityNominatim["address"]["county"])){
		    	$level4 = Zone::getLevelByNameAndCountry($cityNominatim["address"]["county"], "4", $newCity["country"]);
		    	if(empty($level4)){
		    		$level4 = self::createLevel($cityNominatim["lat"], $cityNominatim["lon"], $params['countryCode'], "4", $levelParents);
		    	}

		    	if(!empty($level4["_id"])){
		    		$newCity["level4"] = (String)$level4["_id"];
	    			$newCity["level4Name"] = Zone::getNameOrigin($newCity["level4"]);
	    			$levelParents["level4"] = $newCity["level4"];
					$levelParents["level4Name"] = $newCity["level4Name"];
		    	}
		    }

	    	$translate = array();
			$info = array();
			$origin = $newCity["name"];
			if(!empty($zoneNominatim[$fieldName])){
				foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
					$arrayName = explode(":", $keyName);
					if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName)
						$translate[strtoupper($arrayName[1])] = $valueName;
				}
			}

			Yii::app()->mongodb->selectCollection(City::COLLECTION)->insert($newCity);
			$info["countryCode"] = $params['countryCode'];
			$info["parentId"] = (String)$newCity["_id"];
			$info["parentType"] = City::COLLECTION;
			$info["translates"] = $translate;
			$info["origin"] = $origin;
			Yii::app()->mongodb->selectCollection(Zone::TRANSLATE)->insert($info);
			PHDB::update(City::COLLECTION, 
						array("_id"=>new MongoId((String)$newCity["_id"])),
						array('$set' => array("translateId" => (String)$info["_id"]))
			);

			
			return $newCity ;
		}
    }

}


/*

Test Nominatim 

Reverse :
	BE :  https://nominatim.openstreetmap.org/reverse?format=json&lat=43.29441800&lon=5.35999000&extratags=1&zoom=10

	IT : https://nominatim.openstreetmap.org/reverse?format=json&lat=41.9076293&lon=12.4977726&polygon_geojson=1&extratags=1&namedetails=1&zoom=10

		https://nominatim.openstreetmap.org/search?format=json&addressdetails=1&q=Via+Flavia%2C+98&countrycodes=IT

		https://nominatim.openstreetmap.org/search?q=14+Rue+Albert+Briand&countrycodes=PM

*/

?>