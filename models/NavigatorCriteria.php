<?php

class NavigatorCriteria {
    const COLLECTION = "navigatorcriteria";

  public static $dataBinding = array(
    "name" => array("name" => "name"),
    "collection" => array("name" => "collection"),
    "description" => array("name" => "description"),
    "parent" => array("name" => "parent"),
    "parentId" => array("name" => "parentId"),
    "parentType" => array("name" => "parentType"),
    "modified" => array("name" => "modified"),
    "updated" => array("name" => "updated"),
    "creator" => array("name" => "creator"),
    "created" => array("name" => "created"),
  );
}