<?php

class TranslateMediaWiki
{
    private $type = '';

    public function dataBinding($categorie, $wikiName)
    {

        switch ($categorie) {
            case 'acteurs':
                $this->type = "Organization";
                break;
            case 'projets':
                $this->type = "Project";
                break;
            case 'utilisateurs':
                $this->type = "Person";
                break;
            case 'ressources':
                $this->type = "Classifieds";
                break;
            default:
                $this->type = "Classifieds";
                break;
        }
        $map = $this->{$wikiName}($this->type);
        if ($map === null) {
            return $this->baseMap($this->type);
        }
        return $map;
    }
    public function baseMap($type)
    {
        return array(
            "@type" => $type,
            "name" => array("valueOf" => "title"),
            "image" => array("valueOf" => "logo"),
            "slug" => array("valueOf" => "inCo.0"),
            "id" => array("valueOf" => "id"),
            "parrent" => array("valueOf" => "name"),
            "urls" => array(
                "webSite" => array("valueOf" => "Website.0"),
                "wiki" => array("valueOf" => "url"),
            ));
    }
    private function fabmob($type)
    {
        $map = $this->baseMap($type);
        $map += array(
            "type" => array("valueOf" => "CommunCategorie"),
            "type" => array("valueOf" => "Type"),
            "shortDescription" => array("valueOf" => "ShortDescription.0"),
            "description" => array("valueOf" => "Description.0"),
            "tags" => array("valueOf" => "Tags"),
            "themes" => array("valueOf" => "Theme"),
            "skill" => array(
                "object" => "Skills",
                "valueOf" => array(
                    "name" => array("valueOf" => "fulltext"),
                    "url" => array("valueOf" => "fullurl"),
                ),
            ),
            "themeUt" => array("valueOf" => "Themes"),
            "coInWiki" => array("valueOf" => "PageCo.0"),
            "city" => array("valueOf" => "Ville"),
            "country" => array("valueOf" => "Pays"),
            "country" => array("valueOf" => "Lieu projet"),
            "stateProject" => array("valueOf" => "Develop.0"),
            "geoPosition" => array("valueOf" => "Coordonnées géo.0"),
            "acteur" => array(
                "object" => "Acteur",
                "valueOf" => array(
                    "name" => array("valueOf" => "fulltext"),
                    "url" => array("valueOf" => "fullurl"),
                ),
            ),
            "communaute" => array(
                "object" => "Communauté",
                "valueOf" => array(
                    "name" => array("valueOf" => "fulltext"),
                    "url" => array("valueOf" => "fullurl"),
                ),
            ),
            "defi" => array(
                "object" => "Défi",
                "valueOf" => array(
                    "name" => array("valueOf" => "fulltext"),
                    "url" => array("valueOf" => "fullurl"),
                ),
            ),
            "ressource" => array(
                "object" => "Ressource",
                "valueOf" => array(
                    "name" => array("valueOf" => "fulltext"),
                    "url" => array("valueOf" => "fullurl"),
                ),
            ),
            "utilisateur" => array(
                "object" => "Utilisateur",
                "valueOf" => array(
                    "name" => array("valueOf" => "fulltext"),
                    "url" => array("valueOf" => "fullurl"),
                ),
            ),

        );
        return $map;
    }
    private function movilab($type)
    {
        $map = $this->baseMap($type);
        $map += array(
            // "type" => array("valueOf" => "CommunCategorie"),
            // "type" => array("valueOf" => "Type"),
            // "shortDescription" => array("valueOf" => "ShortDescription.0"),
            // "description" => array("valueOf" => "Description.0"),
            // "tags" => array("valueOf" => "Tags"),
            // "themes" => array("valueOf" => "Theme"),
            // "skill" => array(
            //     "object" => "Skills",
            //     "valueOf" => array(
            //         "name" => array("valueOf" => "fulltext"),
            //         "url" => array("valueOf" => "fullurl"),
            //     ),
            // ),
            // "themeUt" => array("valueOf" => "Themes"),
            // "coInWiki" => array("valueOf" => "PageCo.0"),
            // "city" => array("valueOf" => "Ville"),
            // "country" => array("valueOf" => "Pays"),
            // "country" => array("valueOf" => "Lieu projet"),
            // "stateProject" => array("valueOf" => "Develop.0"),
            // "geoPosition" => array("valueOf" => "Coordonnées géo.0"),
            // "acteur" => array(
            //     "object" => "Acteur",
            //     "valueOf" => array(
            //         "name" => array("valueOf" => "fulltext"),
            //         "url" => array("valueOf" => "fullurl"),
            //     ),
            // ),
            // "communaute" => array(
            //     "object" => "Communauté",
            //     "valueOf" => array(
            //         "name" => array("valueOf" => "fulltext"),
            //         "url" => array("valueOf" => "fullurl"),
            //     ),
            // ),
            // "defi" => array(
            //     "object" => "Défi",
            //     "valueOf" => array(
            //         "name" => array("valueOf" => "fulltext"),
            //         "url" => array("valueOf" => "fullurl"),
            //     ),
            // ),
            // "ressource" => array(
            //     "object" => "Ressource",
            //     "valueOf" => array(
            //         "name" => array("valueOf" => "fulltext"),
            //         "url" => array("valueOf" => "fullurl"),
            //     ),
            // ),
            // "utilisateur" => array(
            //     "object" => "Utilisateur",
            //     "valueOf" => array(
            //         "name" => array("valueOf" => "fulltext"),
            //         "url" => array("valueOf" => "fullurl"),
            //     ),
            // ),

        );
        return $map;
    }
}
