<?php 
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;

use Badge;
use DateTime;
use Exception;
use OpenBadgeException;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Recipient;

class Assertion extends AbstractOpenBadge{
    protected $mapVarClass = [
        'recipient' => Recipient::class,
        'verification' => Verification::class,
        'evidence' => Evidence::class
    ];
    protected $SKIP_KEY = [
        'sourceAssertion'
    ];
    public string $context = Badge::CONTEXT;
    public $type;
    public $id;
    public $recipient;
    public $image;
    public $evidence;
    public $issuedOn;
    public $expires;
    public $badge;
    public $verification;
    public $narrative;
    public $revoked;
    public $revocationReason;
    public $sourceAssertion = NULL;
    public static function fromBadgeAndEarnerElement($badgeElement, $earnerElement)
    {
        if(!isset($earnerElement["email"])){
            throw new OpenBadgeException("Issuer don't have email");
        }
        $baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
        $assertion = $earnerElement["badges"][(string) $badgeElement["_id"]];
		$issuedOn = $assertion["issuedOn"]->toDateTime()->format(DateTime::ATOM);
        if(isset($badgeElement["type"]) && $badgeElement["type"] == "byReference"){
            $array = $badgeElement["assertion"][(string) $earnerElement["_id"]];
        }else{
            $array = [
                "@context" => "https://w3id.org/openbadges/v2",
                "type" => "Assertion",
                "id" => $baseUrl . "/co2/badges/assertions/badge/" . (string) $badgeElement["_id"] . "/award/" . (string) $earnerElement["_id"],
                "badge" => $baseUrl . "/co2/badges/badges/id/" . (string) $badgeElement["_id"],
                "image" => @$badgeElement["profilMediumImageUrl"] ? $baseUrl . self::preprocessImage($badgeElement["profilMediumImageUrl"], (string) $earnerElement["_id"]) : null,
                "verification" => [
                    "type" => "HostedBadge"
                ],
                "issuedOn"  => $issuedOn,
                "recipient" => [
                    "hashed" => true,
                    "type" => "email",
                    "identity" => 'sha256$' . hash( 'sha256', $earnerElement["email"] . (string) $badgeElement["_id"]),
                    "salt" => (string) $badgeElement["_id"]
                ]
            ];
            if(isset($earnerElement["badges"][(string) $badgeElement["_id"]]["revoke"]) && $earnerElement["badges"][(string) $badgeElement["_id"]]["revoke"] == "true"){
                $array["revoked"] = true;
                if(isset($earnerElement["badges"][(string) $badgeElement["_id"]]["revokeReason"])){
                    $array["revocationReason"] = $earnerElement["badges"][(string) $badgeElement["_id"]]["revokeReason"];
                }
            }
        }
        if(!isset($array["image"])){
            throw new Exception("This array should contains image");
        }
        $assertionInstance = new Assertion();
        $assertionInstance->setFromAssociativeArray($array);
        $assertion->context = $array["@context"];
        $assertionInstance->recipient = new Recipient();
        $assertionInstance->recipient->setFromAssociativeArray($array["recipient"]);
        return $assertionInstance;
    }
    private static function preprocessImage($image, $id)
	{
		$split = explode('medium/', $image);
		$image = $split[0] . 'assertions/' . $id . '.png';
		return $image;
	}

}