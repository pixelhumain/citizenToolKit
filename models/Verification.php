<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;
class Verification extends AbstractOpenBadge{
    public $type;
    public $verificationProperty;
    public $startsWith;
    public $allowedOrigins;
    public $creator;
}