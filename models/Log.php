<?php
use CountUserAction;
/*
This Class manage define logs process
like : 
- Insert
- data analysis & statistic calculation 
- data clean ups
*/
class Log {

	const COLLECTION = "logs";				

	/**
	 * Set an array of parameters for actions we want to log
	 * @return $array : a set of actions with parameters
	*/
	public static function getActionsToLog(){
		return array(
	      "person/authenticate" => array('waitForResult' => true, "keepDuration" => 60),
	      "person/logout" => array('waitForResult' => false, "keepDuration" => 60),
	      "person/register" => array('waitForResult' => true, "keepDuration" => 60),
	      "news/save" => array('waitForResult' => true, "keepDuration" => 30),
	      "action/addaction" => array('waitForResult' => true, "keepDuration" => 60),
	      "organization/get" => array('waitForResult' => true, "keepDuration" => 60),
	      "costum/update" => array('waitForResult' => true, "keepDuration" => 20),
	    );
	}

	/**
	 * adds an entry into the logs collection
	 * @param $libAction : a set of information for a proper logs entry
	*/
	public static function setLogBeforeAction($libAction){

    	//Data by default
	    $logs =array(
			"userId" => @Yii::app()->session['userId'],
			"browser" => @$_SERVER["HTTP_USER_AGENT"],
			"ipAddress" => @$_SERVER["REMOTE_ADDR"],
			"created" => new MongoDate(time()),
			"action" => $libAction
	    );

	    //POST or GET
    	if(!empty($_REQUEST)) $logs['params'] = $_REQUEST;

    	//To avoid the clear password storage
    	if(isset($logs['params']['pwd'])) unset($logs['params']['pwd']);
	    return $logs;
	}


	/**
	 * Set the result answer of the logging action
	 * @param $id : the log id
	 * @param $result : the result information
	*/
	public static function setLogAfterAction($log, $result){

		$log['result']['result'] = @$result['result'];
		$log['result']['msg'] = @$result['msg'];
		self::save($log);

	}

	public static function save($log){
		//Update
		if(isset($log['_id'])){
			$id = $log['_id'];
			unset($log['_id']);
			PHDB::update(  self::COLLECTION, 
		     								    array("_id"=>new MongoId($id)),
		     									array('$set' => $log)
		     								);
		}//Insert
		else{
			Yii::app()->mongodb->selectCollection(self::COLLECTION)->insert($log);
		}
	}

    public static function saveAndClean ($log){
        if (isset($log["action"]) && $log["action"] == "costum/editBlock" && !isset($log["blockName"]) && !isset($log["page"]) && !isset($log["blockPath"])) {
            $findCms = PHDB::findOneById( $log["collection"], $log["idBlock"]);
            $log["blockName"] = isset($findCms["name"]) ? $findCms["name"] : "";
            $log["page"] = isset($findCms["advanced"]["persistent"]) ? $findCms["advanced"]["persistent"] : (isset($findCms["page"]) ? $findCms["page"] : substr($findCms["path"], strrpos($findCms["path"], '.') + 1));
            $log["blockPath"] = $findCms["path"];
        }
        if (isset($log["action"]) && $log["action"] == "costum/deleteBlock"  && isset($log["allBlockToDelete"]) ) {
            foreach($log["allBlockToDelete"] as $key => $idBlock){
                PHDB::remove(self::COLLECTION,array('idBlock' => $idBlock));
            }
        }
	    // count logs
        $exists = PHDB::find( self::COLLECTION , array('costumSlug' => $log['costumSlug']));
       if (count($exists) >= 100)  {
           $olderDate = min(array_column($exists, 'created'));
           PHDB::remove(self::COLLECTION, array('costumSlug' => $log['costumSlug'], 'created' => $olderDate));
       }
	    //insert log
        Yii::app()->mongodb->selectCollection(self::COLLECTION)->insert($log);
        CountUserAction::incNbAction($log);

    }

    public static function backupCms ($log){
        $timestamp    = time();
        $dataToSave = [];
	    $exists = PHDB::findOne( self::COLLECTION , array("costumId" => $log["costumId"], "userId" => $log["userId"]));
        if($exists == null) {
            $dataToSave["costumId"] = $log["costumId"];
            $dataToSave["costumSlug"] = $log["costumSlug"];
            $dataToSave["userId"] = $log["userId"];
            $dataToSave["userName"] = $log["userName"];
            $dataToSave["costumEditMode"] =  $log["costumEditMode"];
            $dataToSave["data"][$timestamp] =  $log["listAction"];
            PHDB::insert(self::COLLECTION, $dataToSave);
        }else {
            if (count($exists["data"]) >= 10 ) {
                $olderKey = min(array_keys($exists["data"]));
                $arraySet = array('$set' => array(	"data.".$timestamp => $log["listAction"] ),'$unset'=> array("data.".$olderKey => ""));

            } else {
                $arraySet = array('$set' => array(	"data.".$timestamp => $log["listAction"] ));
            }
            $res = PHDB::update( self::COLLECTION,
                array("costumId" => $log["costumId"], "userId" => $log["userId"]),
                $arraySet
            );
        }
    }

	public static function increment($actionPath,$uId){
		//Update
		$ct = PHDB::count(self::COLLECTION,["user"=>$uId,"action"=>$actionPath]);
		if( $ct ){
			PHDB::update(  self::COLLECTION, ["user"  => $uId,
		     								  "action"=> $actionPath ],
		     									 ['$inc' => ["views"=>1] ]
		     								);
		}//Insert
		else{
			$o = [ "user"  => $uId ,
			     "action" => $actionPath ,
			     "views" => 1 ];
			Yii::app()->mongodb->selectCollection( self::COLLECTION )->insert( $o ); 
		}
	}

	/**
	 * List all the log in the collection logs
	*/
	public static function getAll(){
		return PHDB::find(self::COLLECTION);
	}

	/**
	 * List the log in the collection logs depends Where
	*/
	public static function getWhere($where = array()){
		return PHDB::find(self::COLLECTION, $where);
	}

	/**
	 * Give a lift of IpAdress to block
	*/
	public static function getSummaryByAction(){
		
		$c = Yii::app()->mongodb->selectCollection(self::COLLECTION);
		$result = $c->aggregate(
			array(   
			  '$group'=> 
		  		array(
			    	'_id' => array(
			    		'action' => '$action'
			    		, 'ip' => '$ipAddress'
			    		, 'result' => '$result.result'
			    		, 'msg' => '$result.msg'
			    	),
				    'count' => array ('$sum' => 1),
				    'minDate' => array ( '$min' => '$created' ),
				    'maxDate' => array ( '$max' => '$created' )
				)
			
		));
		return $result;
	}

	/**
	 * Let to clean the logs depends to the rules defined in the array logs parameters
	*/
	public static function cleanUp(){
	    $actionsToLog = Log::getActionsToLog();

	    foreach($actionsToLog as $action => $param){
	    	$dateLimit = date('Y-m-d H:i:s', time()-(3600*24*$param['keepDuration']));
	    	echo "Action en traitement : ".$action." - Durée de vie de ".$param['keepDuration']." jour(s) soit < ".$dateLimit."<br/>";
	    	PHDB::remove(self::COLLECTION, array( 
	    		"action"=> $action
	    		, "created" => array('$lt' => new MongoDate(strtotime($dateLimit)))
	    	));
	    }
	}
}

?>