<?php

class AdresseDataGouv {
	public static function getAddressByDataGouv($params){
        try{
	        $url = "https://api-adresse.data.gouv.fr/search/" ;
	        if(!empty($params['text'])){
	            $url .= "?q=".urlencode($params['text']);
	            //$url .= "&type=housenumber";
	            //$url .= "&limit=30";
		        //var_dump($url); exit ;
		        return json_decode( SIG::getUrl($url), true );
	        }
	        return null;
	        
		}catch (CTKException $e){
            return null ;
        }
    }

    public static function parseAddress($data, $params){
    	$res = array();
        if(!empty($data) && !empty($data["features"])){

        	foreach ($data["features"] as $key => $val) {
        		$city = null;
        		if( !empty($val["properties"]) ){
	        		if( !empty($val["properties"]["citycode"]) ){
	        			$city = PHDB::findOne( City::COLLECTION, 
		        					array( 	"insee" => $val["properties"]["citycode"],
		        							"country" => array('$in' => $params["countryCode"]) ) ) ;
	        		}

	        		if( !empty($city) ){
	        			$res[] = City::createLocality($city, $val["properties"]["name"], $val["geometry"]["coordinates"][1], $val["geometry"]["coordinates"][0], $val["properties"]["postcode"]);
	        		}
	        	}
        	}
        }
        return $res;
    }
}

/* https://geo.api.gouv.fr/communes?code=13055&fields=code,nom,contour */
?>