<?php
/*
    
 */
class Import
{ 
    const MAPPINGS = "mappings";
    public static function parsing($post) {
        $params = array("result"=>false);
        if($post['typeFile'] == "json" || $post['typeFile'] == "js" || $post['typeFile'] == "geojson")
            $params = self::parsingJSON($post);
        else if($post['typeFile'] == "csv")
            $params = self::parsingCSV($post); 
        else if($post['typeFile'] == "xml")
            $params = self::parsingXML($post);
        return $params ;
    }

    public static function parsingCSV($post) {
        
        $attributesElt = ArrayHelper::getAllPathJson(file_get_contents("../../modules/co2/data/import/".Element::getControlerByCollection($post["typeElement"]).".json", FILE_USE_INCLUDE_PATH));
        if($post['idMapping'] != "-1"){
            $where = array("_id" => new MongoId($post['idMapping']));
            $fields = array();
            $mapping = self::getMappings($where, $fields);
            //Remplace les "_dot_" par des "."
            $mapping[$post['idMapping']]["fields"] = Mapping::replaceByRealDot($mapping[$post['idMapping']]["fields"]);
            $arrayMapping = $mapping[$post['idMapping']]["fields"];
        }
        else
            $arrayMapping = array();
        $params = array("result"=>true,
                        "attributesElt"=>$attributesElt,
                        "arrayMapping"=>$arrayMapping,
                        // "idMapping"=>$post['idMapping'],
                        // "mapping"=> $mapping[$post['idMapping']],
                        "typeFile"=>$post['typeFile']);

        if(!empty($mapping)){
            $params["mapping"] = $mapping[$post['idMapping']];
            $params["idMapping"] = $post['idMapping'];
        }
            
        return $params ;
    }

    public static function parsingJSON($post){
        header('Content-Type: text/html; charset=UTF-8');
        $params = array("result"=>false);
        if(isset($post['file'])) {
            
            $json = $post['file'][0];
            //(empty($post["path"]) ? $post['file'][0] : $post['file'][0][$post["path"]]);
            // if (isset($post['path'])) {
            //     $json = $post['file'][0][$post['path']][0];
            // } 
            if($post['idMapping'] != "-1"){
                
                $where = array("_id" => new MongoId($post['idMapping']));
                $fields = array();
                $mapping = self::getMappings($where, $fields);
                $mapping[$post['idMapping']]["fields"] = Mapping::replaceByRealDot($mapping[$post['idMapping']]["fields"]);
                $arrayMapping = $mapping[$post['idMapping']]["fields"];
            }
            else
                $arrayMapping = array();

            $json = json_decode($json,true);
            if(!empty($post["path"])){
                $json = $json[$post["path"]];
                $json = json_encode($json);
            }


            if(!empty($json)){
                $json = json_encode($json);
                if(substr($json, 0,1) == "{")
                    $arbre = ArrayHelper::getAllPathJson($json); 
                else{
                    $json = json_decode($json,true);
                    $arbre = array();
                    foreach ($json as $key => $value) {
                        $arbre = ArrayHelper::getAllPathJson(json_encode($value), $arbre); 
                    }
                }
                $attributesElt = ArrayHelper::getAllPathJson(file_get_contents("../../modules/co2/data/import/".Element::getControlerByCollection($post["typeElement"]).".json", FILE_USE_INCLUDE_PATH));
                $params = array("result"=>true,
                            "attributesElt"=>$attributesElt,
                            "arrayMapping"=>$arrayMapping,
                            // "idMapping"=>$post['idMapping'],
                            // "mapping"=> $mapping[$post['idMapping']],
                            "arbre"=>$arbre,
                            "typeFile"=>$post['typeFile']);  

                if(!empty($mapping)){
                    $params["mapping"] = $mapping[$post['idMapping']];
                    $params["idMapping"] = $post['idMapping'];
                }
            }else{
                $params = array("result"=>false,
                            "msg"=>"Il y a une erreur avec le JSON. Vérifiez que votre JSON soit valide.");  
            }
            
        }
        return $params ;
    }

     public static function parsingXML($post)
    {
        $params = array("result" => false);

        if(isset($post['file']))
        {

            $arbre = array();
            $contenu = array();
            $file = simplexml_load_string($post['file'][0]);
            $n = 0;
            $json = ['elements' => [$file]];
            $json= json_encode($json,true);

            foreach($file->children() as $child){
                $arbre[$n] = $child->getName();
                $n++;
            }

            if($post['idMapping'] != "-1"){
                $where = array("_id" => new MongoId($post['idMapping']));
                $fields = array("fields");
                $mapping = self::getMappings($where, $fields);
                $mapping[$post['idMapping']]["fields"] = Mapping::replaceByRealDot($mapping[$post['idMapping']]["fields"]);
                $arrayMapping = $mapping[$post['idMapping']]["fields"];
                $find = PHDB::findOne(self::MAPPINGS,$where);
            }
            else
                $arrayMapping = array();

            $attributesElt = ArrayHelper::getAllPathJson(file_get_contents("../../modules/co2/data/import/".Element::getControlerByCollection($post["typeElement"]).".json", FILE_USE_INCLUDE_PATH));

            if($post['idMapping'] != "-1"){
                $params = array("result" => true,
                    "attributesElt" => $attributesElt,
                    "arrayMapping" => $arrayMapping,
                    "arbre" => $arbre,
                    "json" => $json,
                    "typeFile" => $post['typeFile'],
                    "idMapping" => $post['idMapping'],
                    "nameUpdate" => $find['name']);
            }

            else{
                    $params = array("result" => true,
                    "attributesElt" => $attributesElt,
                    "arrayMapping" => $arrayMapping,
                    "arbre" => $arbre,
                    "json" => $json,
                    "typeFile" => $post['typeFile'],
                    "idMapping" => $post['idMapping']);
            }
    }
        return $params;
    }


    public static function getMappings($where=array(),$fields=null){
        $allMappings = PHDB::find(self::MAPPINGS, $where, $fields);
        $allMappings = self::getStandartMapping($allMappings);
        return $allMappings;
    }
   
    public static  function previewData($post, $notCheck=false, $newCsvParse=false){
        // Rest::json($post); exit;
        $params = array("result"=>false);
        $elements = array();
        $saveCities = array();
        $nb = 0 ;
        $elementsWarnings = array();
        if(!empty($post['infoCreateData']) && !empty($post['file']) && !empty($post['nameFile']) && !empty($post['typeFile'])){
            $mapping = json_decode(file_get_contents("../../modules/co2/data/import/".Element::getControlerByCollection($post["typeElement"]).".json", FILE_USE_INCLUDE_PATH), true);
            // mapping path with dots
            $attributesElt = ArrayHelper::getAllPathJson(file_get_contents("../../modules/co2/data/import/".Element::getControlerByCollection($post["typeElement"]).".json", FILE_USE_INCLUDE_PATH));
            if($post['typeFile'] == "csv"){
                $file = $post['file'];
                //CSV header (column names) 
                $headFile = $file[0];
                if($newCsvParse === false)
                    unset($file[0]);
            }elseif ((!isset($post['pathObject'])) || ($post['pathObject'] == "")) {
                $file = json_decode($post['file'][0], true);
            }else {
                $file = json_decode($post['file'][0], true);
                $file = @$file[$post["pathObject"]];
            }
            
            if(@$file)
            // Boucle sur toutes les lignes du fichier
            foreach ($file as $keyFile => $valueFile){
                $nb++;
                //if(!empty($valueFile)){
                    $element = array();
                    $elem=array();     
                    $res=[];
                    // Boucle sur l'en-tête du mapping de base de données ou entré à la main en étape 2
                    foreach ($post['infoCreateData'] as $key => $value) {
                        $valueData = null;
                        // vérifie si la clé du mapping est bien une colonne du fichier
                        if($post['typeFile'] == "csv" && in_array($value["idHeadCSV"], $headFile)){
                            // Récupère l'index de la clé du mapping dans le fichier (sa position, numéro de colonne)
                            $idValueFile = array_search($value["idHeadCSV"], $headFile);
                            // On récupère la valeur dans la bonne colonne grâce à $idValueFile
                            $valFile =  (!empty($valueFile[$idValueFile])?$valueFile[$idValueFile]:null);
                        }else if ($post['typeFile'] == "json" || $post['typeFile'] == "xml"){
                            $valFile =  ArrayHelper::getValueByDotPath($valueFile , $value["idHeadCSV"]);
                            // var_dump($valFile);
                        }
                        else{
                            $valFile =  (!empty($valueFile[$value["idHeadCSV"]])?$valueFile[$value["idHeadCSV"]]:null);
                        }
                        if(!empty($valFile)){
                            $valueData = (is_string($valFile)?trim($valFile):$valFile);
                            if(!empty($valueData)){
                                $typeValue = ArrayHelper::getValueByDotPath($mapping , $value["valueAttributeElt"]);
                                $element = ArrayHelper::setValueByDotPath($element , $value["valueAttributeElt"], $valueData, $typeValue);
                                if(Costum::isSameFunction("prepImportData")){
                                    $paramsCostum = array(
                                        "element" => $element,
                                        "value" => $valueData,
                                        "valueConfig" => $value,
                                        "valueFile" => $valueFile
                                    );
                                    $element = Costum::sameFunction("prepImportData", $paramsCostum);
                                }
                            }
                        }
                    }    
                    $element['source']['insertOrign'] = "import";
                    $element['source']['date'] = new MongoDate(time());
                    if(!empty($post['key'])){
                        $element['source']['keys'][] = $post['key'];
                        $element['source']['key'] = $post['key'];
                    }
                    if($notCheck != true)
                        $element = self::checkElement($element, $post['typeElement']);
                    if(!empty($element["saveCities"])){
                        $saveCities[] = $element["saveCities"];
                        unset($element["saveCities"]);
                    }
                    if($post['typeElement'] == Person::COLLECTION){
                        // $element['msgInvite'] = $post['msgInvite'];
                        // $element['nameInvitor'] = $post['nameInvitor'];
                    }
                    if(!empty($element['msgError']) || ($post['warnings'] == "false" && !empty($element['warnings'])))
                        $elementsWarnings[] = $element;
                    //else */
                    $elements[] = $element;
                //}
            }
            
            $params = array("result"=>true,
                            "elementsObj"=>$elements,
                            "elements"=>json_encode(json_decode(json_encode($elements),true)),
                            "elementsWarnings"=>json_encode(json_decode(json_encode($elementsWarnings),true)),
                            "saveCities"=>json_encode(json_decode(json_encode($saveCities),true)),
                            "listEntite"=>self::createArrayList(array_merge($elements, $elementsWarnings)));
        }
        return $params;
    }

    public static function getLinksImport($nameElem,$collecSearchEl,$elem,$searchEl,$value,$element){
        // var_dump($element);exit;
        $textRegExp =SearchNew::accentToRegex($nameElem);
  			$query = array(
                '$and' => array(

                    array('$or'=>array(
                        array( "name" => new MongoRegex("/.*{$textRegExp}.*/i")),
                        array( "title" => new MongoRegex("/.*{$textRegExp}.*/i")),
                        array( "slug" => new MongoRegex("/.*{$textRegExp}.*/i"))
                    ))
                )                 
            );
        $costum=CacheHelper::getCostum();
        $geoContext=PHDB::findOne($costum["contextType"],array("_id"=>new MongoId($costum["contextId"])),array("address"));
        $linkedElem=PHDB::findAndSortAndLimitAndIndex($collecSearchEl,$query,array("updated"=>-1,"created"=>-1));
        if(count($linkedElem)==1){
            $id=array_keys($linkedElem)[0];
            $nameElem=[];
            $elem[$id]=array("type"=>$linkedElem[$id]["collection"],"name"=>$linkedElem[$id]["name"]);
            if(!isset($element[$searchEl[0]])){
                $element[$searchEl[0]]=[];
            }
            if(!isset($element[$searchEl[0]][$collecSearchEl])){
                $element[$searchEl[0]][$collecSearchEl]=[];
            }
            $element[$searchEl[0]][$collecSearchEl]=array_merge($element[$searchEl[0]][$collecSearchEl],$elem);
        }else{
            // $element["error"]=!empty($element["error"]) ? $element["error"] : array();
            $baseMsg = "Problème sur le champ <b>".$value["idHeadCSV"]."</b>";
            $startMsg=(!empty($element["msgError"])) ? $element["msgError"]."<br/>".$baseMsg : $baseMsg;
            if(count($linkedElem)==2){
                $elementIds=array();
                $elemids=array_keys($linkedElem);
                $older=$elemids[0];
                $newer=$elemids[1];
                $startMsg.=', 2 '.$nameElem.' trouvé.e.s : <a href="/co2/element/mergedatadoublon/element/'.$collecSearchEl.'/id/'.$older.'/into/'.$newer.'" target="_blank">Les fusionner</a>';
                $element["msgError"]=$startMsg;
                // array_push($element["error"],$startMsg);
            }
            else if(count($linkedElem)>2){
                $i=1;
                $str='';
                foreach($linkedElem as $key=>$val){
                    
                    $str.='<a href="#page.type.'.$key.'.id.'.$val["collection"].'" target="_blank">'.$val["name"].'</a>';
                    if($i!==count($linkedElem)){
                        $str.=", ";
                    }
                    $i++;

                }
                $baseMsg=$startMsg.", plusieurs ".$nameElem." : ".$str;
                $element["msgError"]=$baseMsg;
                // array_push($element["error"],$baseMsg);
            }else{
                $startMsg.=", <b>".$nameElem."</b> n'a pas été trouvé.e. Ajouter l'ensemble des contacts correspondant à ce champ avant les autres.";
                $element["msgError"]=$startMsg;
                // array_push($element["error"],$startMsg);
            }
        }
        $res=array("element"=>$element,"elem"=>$elem);

        return $res;
    }
    public static function createArrayList($list) {
        $head = array("name", "address", "warnings", "msgError") ;
        $tableau = array($head);
        foreach ($list as $keyList => $valueList){
            $ligne = array();
            $ligne[] = (empty($valueList["name"])? "" : $valueList["name"]);
            if(!empty($valueList["address"])){
                $str = (empty($valueList["address"]["streetAddress"]) ? "" : $valueList["address"]["streetAddress"].",");
                $str .= (empty($valueList["address"]["postalCode"]) ? "" : $valueList["address"]["postalCode"].", ");
                $str .= (empty($valueList["address"]["addressLocality"]) ? "" : $valueList["address"]["addressLocality"].", ");
                $str .= (empty($valueList["address"]["addressCountry"]) ? "" : $valueList["address"]["addressCountry"]);
                $ligne[] = $str;
            }
            $ligne[] = (empty($valueList["warnings"])? "" : self::getMessagesWarnings($valueList["warnings"]));
            $ligne[] = (empty($valueList["msgError"])? "" : $valueList["msgError"]);
            $tableau[] = $ligne ;
        }
        return $tableau ;
    }
    public static function getMessagesWarnings($warnings){
        $msg = "";
        foreach ($warnings as $key => $codeWarning) {
            if($msg != "")
                $msg .= "<br/>";
            $msg .= Yii::t("import",$codeWarning);
        }
        return $msg;
    }
    public static function checkElement($element, $typeElement){
        

        $result = array("result" => true);
        $geo = (empty($element['geo']) ? null : $element['geo']);
        // if($typeElement != Person::COLLECTION ) {
            $address = (empty($element['address']) ? null : $element['address']);
            $geo = (empty($element['geo']) ? null : $element['geo']);

           

            if(!empty($address) && !empty($address["addressCountry"])  && !empty($address["addressLocality"]) ){
                // $address["postalCode"] = '0'.$address["postalCode"];
                $detailsLocality = self::getAndCheckAddressForEntity($address, $geo) ;
            }

            if(isset($detailsLocality) && $detailsLocality["result"] == true){
                $element["address"] = $detailsLocality["address"] ;
                $element["geo"] = $detailsLocality["geo"] ;
                $element["geoPosition"] = $detailsLocality["geoPosition"] ;
                if(!empty($detailsLocality["saveCities"]))
                    $saveCities = $detailsLocality["saveCities"] ; 

                //Rest::json($element); exit;
            }
        // }
        if($typeElement == Event::COLLECTION){
            //$params["allDay"]=(@$params["allDay"] && (!empty($params["allDay"]) || $params["allDay"]=="O")) ? true : false;
            date_default_timezone_set('UTC');
            //var_dump($element['startDate']);exit;
            //var_dump($datestart);exit;
            //$startDate=date('Y-m-d h:i:s', $datestart); 

            //$dateend=strtotime($element['endDate']);
            //$endDate=date('Y-m-d h:i:s', $dateend); 
            //$startDate = DataValidator::getDateTimeFromString(strtotime($element['startDate']), "start date");
            //$endDate = DataValidator::getDateTimeFromString(strtotime($element['endDate']), "end date");
           /* if(@$params["allDay"] && $params["allDay"]){
                $startDate=date_time_set($startDate, 00, 00);
                $endDate=date_time_set($endDate, 23, 59);
            }*/
            if(isset($element['startDate']))
                $element["startDate"] = new MongoDate(strtotime($element['startDate']));
            if(isset($element['endDate']))
                $element["endDate"]   = new MongoDate(strtotime($element['endDate']));
            if(!isset($element["type"]) || empty($element["type"])){
                $element["type"]="others";
            }
        }
        if(!empty($element["tags"]))
            $element["tags"] = self::checkTag($element["tags"]);
        
        
        if($typeElement == Organization::COLLECTION && !empty($element["type"]))
            $element["type"] = Organization::translateType($element["type"]);
        if(!empty($element["facebook"]))
            $element["socialNetwork"]["facebook"] = $element["facebook"];
        if (!empty($element['source']) && !empty($element['source']['keys']) && $element['source']['keys'][0] !== "convert_datagouv" && $element['source']['keys'][0] !== "convert_osm" && $element['source']['keys'][0] !== "convert_ods" && $element['source']['keys'][0] !== "convert_wiki" && $element['source']['keys'][0] !== "convert_datanova" && $element['source']['keys'][0] !== "convert_poleemploi" && $element['source']['keys'][0] !== "convert_educ_etab" && $element['source']['keys'][0] !== "convert_educ_membre" && $element['source']['keys'][0] !== "convert_educ_ecole" && $element['source']['keys'][0] !== "convert_educ_struct" && $element['source']['keys'][0] !== "convert_valueflows" && $element['source']['keys'][0] !== "convert_organcity") {
            $element = self::getWarnings($element, $typeElement, true) ;
        }
        
        $resDataValidator = DataValidator::validate(Element::getControlerByCollection($typeElement), $element, true);
        if($resDataValidator["result"] != true){
            //$element["msgError"] = ((empty($resDataValidator["msg"]->getMessage()))?$resDataValidator["msg"]:$resDataValidator["msg"]->getMessage());
            $element["msgError"] = (!empty($element["msgError"]) && $resDataValidator["msg"]) ? $resDataValidator["msg"]."<br/>".$element["msgError"] : $resDataValidator["msg"];    
        }
        if(!empty($saveCities))
            $element["saveCities"] = $saveCities ; 
        
        return $element;
    }
    public static function checkTag($tags){
        if(is_string($tags))
            $tags=[$tags];
        $newTags = array();
        foreach ($tags as $key => $tag) {
           $split = explode(",", $tag);
           foreach ($split as $keyS => $value) {
               $newTags[] = trim($value);
           }
        }
        return $newTags;
    }
    public static function getAddressConform($city, $address){
        //Rest::json($address); exit;

        $newA = array(
                '@type' => 'PostalAddress',
                'addressCountry' =>  $city["country"]);
        if(!empty($city["_id"])){
            $newA = array(
                '@type' => 'PostalAddress',
                'addressCountry' =>  strtoupper($city["country"]),
                'localityId' =>  (String) $city["_id"],
                'level1' =>  $city["level1"],
                'level1Name' =>  $city["level1Name"]);
        }else{
            $newA["osmID"] = $city["osmID"];
        }
        // Rest::json($address); 
        //  Rest::json($city["postalCodes"]); exit;
        if( !empty($address["postalCode"]) &&  !empty($city["postalCodes"]) ) {
            foreach ($city["postalCodes"] as $keyCp => $valueCp){
                if($valueCp["postalCode"] == $address["postalCode"]){
                    $newA['addressLocality'] = $valueCp["name"];
                    $newA['postalCode'] = $address["postalCode"];
                }
            }
            if(empty($newA['addressLocality'])){
                $newA['addressLocality'] = $address["addressLocality"];
                // TODO RAPHA : FAIRE EN SORT AJOUER LES CEDEXS DANS LES VILLES 
            }

        }else if( empty($address["postalCode"]) && !empty($city["postalCodes"]) ) {
            $newA['addressLocality'] = $city["postalCodes"][0]["name"];
            $newA['postalCode'] = $city["postalCodes"][0]["postalCode"];
            
        }else{
            $newA["addressLocality"] = $city["name"];
        }

        if( !empty($address["streetAddress"]) ) 
            $newA["streetAddress"] = $address["streetAddress"];
        if( !empty($city["insee"]) )
            $newA["codeInsee"] = $city["insee"];
                        
        if( !empty($city["level2"]) ) {
            $newA["level2"] = $city["level2"];
            $newA["level2Name"] = $city["level2Name"];
        }
        if( !empty($city["level3"]) ) {
            $newA["level3"] = $city["level3"];
            $newA["level3Name"] = $city["level3Name"];
        }
        if( !empty($city["level4"]) ) {
            $newA["level4"] = $city["level4"];
            $newA["level4Name"] = $city["level4Name"];
        }

        if( empty($newA["postalCode"]) && !empty($address["postalCode"]) ) {
            $newA["postalCode"] = $address["postalCode"];
        }

        //Rest::json($newA); exit;
        return $newA ;
    }
    public static function getLatLonBySIG($address){
        $street = (empty($address["streetAddress"])?null:$address["streetAddress"]);
        $cp = (empty($address["postalCode"])?null:$address["postalCode"]);
        $geo = array();
        $resultDataGouv = ( ( !empty($address["addressCountry"]) && $address["addressCountry"] == "FR" ) ? ( empty($cp) ? null : json_decode(SIG::getGeoByAddressDataGouv($street, $cp, $address["addressLocality"]), true) ) : null ) ;
        if(!empty($resultDataGouv["features"])){
            $geo["lat"] = strval($resultDataGouv["features"][0]["geometry"]["coordinates"][1]);
            $geo["lon"] = strval($resultDataGouv["features"][0]["geometry"]["coordinates"][0]);
        }else{
            $resultNominatim = json_decode(SIG::getGeoByAddressNominatim($street, $cp, $address["addressLocality"], $address["addressCountry"]), true);
            if(!empty($resultNominatim[0])){
                $geo["lat"] = $resultNominatim[0]["lat"];
                $geo["lon"] = $resultNominatim[0]["lon"];
            }else{
                $resultGoogle = json_decode(SIG::getGeoByAddressGoogleMap($street, $cp, $address["addressLocality"], $address["addressCountry"]), true);
                $resG = false ;
                if(!empty($resultGoogle["results"][0]["address_components"])){
                    foreach ($resultGoogle["results"][0]["address_components"] as $key => $value) {
                        if(in_array("locality", $value["types"]))
                            $resG = true ;
                    }
                }
                if(!empty($resultGoogle["results"]) && $resG == true){
                    $geo["lat"] = strval($resultGoogle["results"][0]["geometry"]["location"]["lat"]);
                    $geo["lon"] = strval($resultGoogle["results"][0]["geometry"]["location"]["lng"]);
                }
            }
        }
        return $geo ;
    }



	public static function getAndCheckAddressForEntity($address = null, $geo = null){
		$lat = null;
		$lon = null;
		$result = false;
		$saveCities = array();
        //Rest::json($address); exit;
        //Rest::json($geo); exit;
		if( !empty($address["addressLocality"]) && 
            !empty($address["addressCountry"]) ) {
            $city = null ;


//CHECK CITY EXISTS
            if(!empty($address["codeInsee"])){
                $where = array('$and' => array(
                                array("insee" => strtoupper($address["codeInsee"])), 
                                array("country" => strtoupper($address["addressCountry"])) ) );
                $city = PHDB::findOne(City::COLLECTION, $where, $fields);
            }
            if( stripos($address["addressLocality"], "CEDEX") !== false && $address["addressCountry"] == "FR" ){
                $local = trim(str_replace("CEDEX", "", $address["addressLocality"]));
                $regexCity = SearchNew::accentToRegex(strtolower($local));
            }
            else
                $regexCity = SearchNew::accentToRegex(strtolower($address["addressLocality"]));

// CHECK WITH OTHER PARAMS CITY EXIST
            if(empty($city)){


                $where = array('$or'=> 
                        array(  
                            array("name" => new MongoRegex("/^".$regexCity."/i")),
                            array("alternateName" => new MongoRegex("/^".$regexCity."/i")),
                            array("postalCodes.name" => new MongoRegex("/^".$regexCity."/i"))
                        ) );
                $where = array('$and' => array($where, array("country" => strtoupper($address["addressCountry"])) ) );
                
                if( !empty($address["postalCode"]) && !(stripos($address["addressLocality"], "CEDEX") !== false && $address["addressCountry"] == "FR") ){
                    $where = array('$and' => array($where, array("postalCodes.postalCode" => $address["postalCode"]) ) );
                }

                $fields = array("name", "geo", "country", "level1", "level1Name","level2", "level2Name","level3", "level3Name","level4", "level4Name", "osmID", "postalCodes", "insee");

                // Rest::json($where); exit;

                $city = PHDB::findOne(City::COLLECTION, $where, $fields);
                //Rest::json($city); exit;
            }

// avec lat long  
            if(!empty($city)){

                if(empty($geo["latitude"]) || empty($geo["longitude"])){
                    $resGeo = self::getLatLonBySIG($address);
                    $lat = ( empty($resGeo["lat"]) ? $city["geo"]["latitude"] : $resGeo["lat"] );
                    $lon = ( empty($resGeo["lon"]) ? $city["geo"]["longitude"] : $resGeo["lon"] );
                }else{
                    $lat = $geo["latitude"] ;
                    $lon = $geo["longitude"];
                }
                $newA = self::getAddressConform($city, $address);
                $newGeo = SIG::getFormatGeo($lat, $lon);
                $newGeoPosition = SIG::getFormatGeoPosition($lat, $lon);
                $result = true;
            }
        } 

        //Rest::json($newA); exit;
        //Rest::json($newGeo); exit;

// avec LAT LONG INSIDE GEOSHAPE
        if(empty($newA) && !empty($geo["latitude"]) && !empty($geo["longitude"])){
            $lat = ( is_numeric($geo["latitude"]) ? strval($geo["latitude"]) : $geo["latitude"] ) ;
            $lon = ( is_numeric($geo["longitude"]) ? strval($geo["longitude"]) : $geo["longitude"] ) ;
            $city = SIG::getCityByLatLngGeoShape( $lat, $lon, null, (!empty($address["addressCountry"]) ? $address["addressCountry"] : null ) ) ;

            
            if(!empty($city)){
                $newA = self::getAddressConform($city, $address);
                $newGeo = SIG::getFormatGeo($lat, $lon);
                $newGeoPosition = SIG::getFormatGeoPosition($lat, $lon);
                $result = true;
            }      
        }

//CHECK BY NOMINATIM
        if(empty($newA) ){
            $resNominatim = array();
            if($address != null && isset($address["addressLocality"]) && isset($address["addressCountry"]))
        	    $resNominatim = json_decode(SIG::getGeoByAddressNominatim(null, null, $address["addressLocality"], trim($address["addressCountry"]), false, true),true);
            if(!empty($resNominatim)){
                $typeCities = array("city", "village", "town") ;
                foreach ($resNominatim as $keyN=> $valueN) {
                    $break = false ;
                    foreach ($typeCities as $keyT=> $valueT) {
                        if( !empty($valueN["address"][$valueT]) && 
                            $address["addressCountry"] == strtoupper(@$valueN["address"]["country_code"])) {   
                                $saveCities = array(    "name" => $valueN["address"][$valueT],
                                                        "alternateName" => mb_strtoupper($valueN["address"][$valueT]),
                                                        "country" => (!empty($address["addressCountry"]) ? $address["addressCountry"] :  strtoupper($valueN["address"]["country_code"])),
                                                        "geo" => SIG::getFormatGeo($valueN["lat"], $valueN["lon"]),
                                                        "geoPosition" =>  SIG::getFormatGeoPosition($valueN["lat"], $valueN["lon"]),
                                                        "level3Name" => (empty($valueN["address"]["state"]) ? null : $valueN["address"]["state"] ),
                                                        "level3" => null,
                                                        "level4Name" => (empty($valueN["address"]["county"]) ? null : $valueN["address"]["county"] ),
                                                        "level4" => null,
                                                        "osmID" => $valueN["osm_id"],
                                                        "save" => true);
                                    if(!empty($valueN["extratags"]["wikidata"]))
                                    $saveCities["wikidataID"] = $valueN["extratags"]["wikidata"];
                                $newA = self::getAddressConform($saveCities, $address);

                                if(!empty($valueT["lat"]) && !empty($valueT["lon"])){
                                	$newGeo = SIG::getFormatGeo($valueT["lat"], $valueT["lon"]);
                                	$newGeoPosition = SIG::getFormatGeoPosition($valueT["lat"], $valueT["lon"]);
                                }
                                
                                $result = true;
                                break;
                        }
                    }
                    if($result == true)
                        break;
                }
            }

            if(empty($newA) && !empty($geo["latitude"]) && !empty($geo["longitude"]) ){
            	$lat = ( is_numeric($geo["latitude"]) ? strval($geo["latitude"]) : $geo["latitude"] ) ;
            	$lon = ( is_numeric($geo["longitude"]) ? strval($geo["longitude"]) : $geo["longitude"] ) ;
        		$resNominatim = json_decode(SIG::getLocalityByLatLonNominatim($lat, $lon),true, true);
               	if(!empty($resNominatim)){
                    $nameCity = self::getCityNameInNominatim($resNominatim["address"]);
                    if(!empty($nameCity)){
                        
                        $saveCities = array( "name" => $resNominatim["address"]["city"],
                                            "alternateName" => mb_strtoupper($resNominatim["address"]["city"]),
                                            "country" => (!empty($address["addressCountry"]) ? $address["addressCountry"] :  strtoupper($resNominatim["address"]["country_code"])),
                                            "geo" => SIG::getFormatGeo($lat, $lon),
                                            "geoPosition" =>  SIG::getFormatGeoPosition($lat, $lon),
                                            "level3Name" => (empty($resNominatim["address"]["state"]) ? null : $resNominatim["address"]["state"] ),
                                            "level3" => null,
                                            "level4Name" => (empty($resNominatim["address"]["county"]) ? null : $resNominatim["address"]["county"] ),
                                            "level4" => null,
                                            "osmID" => $resNominatim["osm_id"],
                                            "save" => true);
                        if(!empty($resNominatim["extratags"]["wikidata"]))
                            $saveCities["wikidataID"] = $resNominatim["extratags"]["wikidata"];
                        $newA = self::getAddressConform($saveCities, $address);
                        if(!empty($valueT["lat"]) && !empty($valueT["lon"])){
                        	$newGeo = SIG::getFormatGeo($valueT["lat"], $valueT["lon"]);
                        	$newGeoPosition = SIG::getFormatGeoPosition($valueT["lat"], $valueT["lon"]);
                        }
                        $result = true;
                    }
                }
            }
        }
        //Rest::json($newGeo); exit;
        $res = array(   "result" => $result,
                        "address" => ( empty($newA) ? null : $newA),
                        "geo" => ( empty($newGeo) ? null : $newGeo),
                        "geoPosition" => ( empty($newGeoPosition) ? null : $newGeoPosition),
                        "saveCities" => ( empty($saveCities) ? null : $saveCities) );
        //Rest::json($res); exit;
        return $res ;
    }
    public static function getCityNameInNominatim($address){
        $typeCities = array("city", "village", "town") ;
        foreach ($typeCities as $key => $value) {
            if(!empty($address[$value]))
                return $address[$value] ;
        }
        return null;
    }
    public static function getWarnings($element, $typeElement, $import = null){
        $warnings = (!empty($element["warnings"])) ? $element["warnings"] : array();
        if(empty($element['name']))
            $warnings[] = "201" ;
        if(empty($element['email']) && $typeElement == Person::COLLECTION)
            $warnings[] = "203" ;



        if(empty($element['type']) && ( $typeElement == Organization::COLLECTION || $typeElement == Event::COLLECTION) )
            $warnings[] = "300" ;
        if($typeElement != Person::COLLECTION){
            if(!empty($element['address'])){
                if(empty($element['address']['addressCountry']))$warnings[] = "104" ;
                if(empty($element['address']['addressLocality'])) $warnings[] = "105" ;
                if(!empty($element['geo'])){
                    if(empty($element['geo']['latitude'])) $warnings[] = "151" ;
                    if(empty($element['geo']['longitude'])) $warnings[] = "152" ;
                }else if(!empty($element['address']['localityId'])) {
                    $warnings[] = (empty($import)?"150":"154") ;
                }
            }else{
                $warnings[] = (empty($import)?"100":"110");
            }
        }
        
        //var_dump($warnings);
        if(!empty($warnings))
            $element["warnings"] = $warnings;
        return $element;
    }
    public static function initMappings(){
        $mappings = json_decode(file_get_contents("../../modules/co2/data/import/mappings.json", FILE_USE_INCLUDE_PATH), true);
        foreach ($mappings as $key => $value) {
            Yii::app()->mongodb->selectCollection( Import::MAPPINGS)->insert( $value );
        }
    }
     public static function addDataInDb($post)
    {
        $jsonString = $post["file"];
        $typeElement = $post["typeElement"];
        /*$pathFolderImage = $post["pathFolderImage"];
        $sendMail = ($post["sendMail"] == "false"?null:true);
        $isKissKiss = ($post["isKissKiss"] == "false"?null:true);
        $invitorUrl = (trim($post["invitorUrl"]) == ""?null:$post["invitorUrl"]);*/
        
        if(substr($jsonString, 0,1) == "{")
            $jsonArray[] = json_decode($jsonString, true) ;
        else
            $jsonArray = json_decode($jsonString, true) ;
        if(isset($jsonArray)){
            $resData =  array();
            foreach ($jsonArray as $key => $value){
                try{
                    if($typeElement == City::COLLECTION){
                        $exist = City::alreadyExists($value, $typeElement);
                        if(!$exist["result"]) {
                            $res = City::insert($value, Yii::app()->session["userId"]);
                            $element["name"] = $value["name"];
                            $element["info"] = $res["msg"];
                        }else{
                            $element["name"] = $exist["city"]["name"];
                            $element["info"] = "La ville existe déjà";
                        }
                    }else{
//<<<<<<< savMap
//                        if( !empty( $value["address"] ) ) {
//                            $good = true ;
//                            if(!empty($value["address"]["osmID"])){
//                                $city = City::getByOsmId($value["address"]["osmID"]);
//                                if(!empty($city)){
//                                    $value["address"] = self::getAddressConform($city, $value["address"]);
////                                    $resGeo = self::getLatLonBySIG($value["address"]);
//                                    $value["geo"] = SIG::getFormatGeo($resGeo["lat"], $resGeo["lon"]);
//                                    $value["geoPosition"] = SIG::getFormatGeoPosition($resGeo["lat"], $resGeo["lon"]);
//                                }
//                                else{
//                                    $good = false ;
//                                    $element["name"] = $exist["element"]["name"];
//                                    $element["info"] = "La commune n'existe pas, penser a l'ajouter avants"; 
//                                }
//=======

						if( (isset($value["address"]) && !empty($value["address"])) || $typeElement==Person::COLLECTION ) {
							$good = true ;
							if(isset($value["address"]["osmID"]) && !empty($value["address"]["osmID"])){
								$city = City::getByOsmId($value["address"]["osmID"]);
                                // $city = (isset($value["address"]["postalCode"]) && isset($value["address"]["addressCountry"])) ? City::getByPostalCode($value["address"]["postalCode"], $value["address"]["addressCountry"]) : $city ;

								if(!empty($city)){
									$value["address"] = self::getAddressConform($city, $value["address"]);
									$resGeo = self::getLatLonBySIG($value["address"]);
									if(!empty($resGeo)){
										$value["geo"] = SIG::getFormatGeo($resGeo["lat"], $resGeo["lon"]);
										$value["geoPosition"] = SIG::getFormatGeoPosition($resGeo["lat"], $resGeo["lon"]);
									}
									
								}
								else{
                                    // var_dump($value);exit;
									$good = false ;
									$element["name"] = $value["name"];
									$element["info"] = "La commune n'existe pas, pensez à l'ajouter avant"; 
								}
//>>>>>>> development
                            }
                            if($good == true){
                                $updateProcess=true;
                                $exist = Element::alreadyExists($value, $typeElement);
                                // var_dump($exist);exit;
                                if(!$exist["result"]){
                                    if(!empty($post["isLink"]) && $post["isLink"] == "true"){
                                        if($post["typeLink"] == Event::COLLECTION && $typeElement == Event::COLLECTION){
											$value["parentId"] = $post["idLink"];
											$value["parentType"] = $post["typeLink"];
                                        }
                                        else{
											$paramsLink["idLink"] = $post["idLink"];
											$paramsLink["typeLink"] = $post["typeLink"];
											$paramsLink["role"] = $post["roleLink"];
                                        }
                                        
                                    }
                                    if(!empty($value["urlImg"])){
                                        $paramsImg["url"] =$value["urlImg"];
                                        $paramsImg["module"] = "communecter";
                                        $split = explode("/", $value["urlImg"]);
                                        $paramsImg["name"] = $split[count($split)-1];
                                        unset($value["urlImg"]);
                                    }
                                    if(!empty($value["startDate"])){
                                        $startDate = DateTime::createFromFormat('Y-m-d H:i:s', $value["startDate"]);
                                        $value["startDate"] = $startDate->format('d/m/Y H:i');
                                    }
                                    if(!empty($value["endDate"])){
                                        $endDate = DateTime::createFromFormat('Y-m-d H:i:s', $value["endDate"]);
                                        $value["endDate"] = $endDate->format('d/m/Y H:i');
                                    }
                                    if(!empty($value["geo"])){
                                        if(gettype($value["geo"]["latitude"]) != "string" )
                                            $value["geo"]["latitude"] = strval($value["geo"]["latitude"]);
                                        if(gettype($value["geo"]["longitude"]) != "string" )
                                            $value["geo"]["longitude"] = strval($value["geo"]["longitude"]);
                                    }
                                    $value["collection"] = $typeElement ;
                                    $value["key"] = Element::getControlerByCollection($typeElement);
                                    $value["paramsImport"] = array( "link" => (empty($paramsLink)?null:$paramsLink),
                                                                    "img" => (empty($paramsImg)?null:$paramsImg ),
                                                                    // "links" => (empty($paramsLinks)? null : $paramsLinks)
                                                            );
                                    if($typeElement!==Person::COLLECTION){
                                        $value["preferences"] = array(  "isOpenData"=>true, 
                                                                    "isOpenEdition"=>true);

                                    }                        
                                    if($typeElement == Organization::COLLECTION)
                                        $value["role"] = "creator";
                                    if($typeElement == Event::COLLECTION && empty($value["organizerType"]))
                                        $value["organizerType"] = Event::NO_ORGANISER;
                                    
                                    if(!empty($value["organizerId"])){
                                        $eltSimple = Element::getElementSimpleById($value["organizerId"], @$value["organizerType"]);
                                        if(empty($eltSimple)){
                                            unset($value["organizerId"]);
                                            if(!empty($value["organizerType"])) 
                                                $value["organizerType"] = Event::NO_ORGANISER;
                                        }
                                    }
                                    $element = array();
                                    $res = Element::save($value);
                                    $element["name"] =  $value["name"];
                                    $element["info"] = $res["msg"];
                                    $element["type"] = $typeElement;
                                    if(!empty($res["id"])){
                                        $element["url"] = "/#page.type.".$typeElement.".id.".$res["id"] ;
                                        $element["id"] = $res["id"] ;
                                    }
                                    
                                } else {
                                    if(count($exist["element"])>1){
                                        // var_dump($exist,$exist["where"]['$or']);exit;
                                        $element["info"]="Plusieurs éléments au nom très proche";
                                        $element["type"] = $typeElement ;
                                        $element["url"]= "";
                                        $element["name"] = "";
                                        $element["email"] = "";                              
                                        $element["id"] ="";
                                        foreach($exist["element"] as $kd => $vd){
                                            $ld='#page.type.'.$vd["collection"].'.id.'.$kd;
                                            $element["url"]= (empty($element["url"])) ? $ld : $element["url"].", ".$ld;
                                            $element["name"] = (empty($element["name"])) ? $vd["name"] : $element["name"].", ".$vd["name"];
                                            $element["email"] = (empty($element["email"])) ? $vd["email"] : $element["email"].", ".$vd["email"];                                            
                                            $element["id"] = (empty($element["id"])) ? $kd : $element["id"].", ".$kd ;
                                            // $element["url"] = 

                                        }
                                    }else{
                                    $keyId=array_keys($exist["element"])[0];
                                    if(isset($exist["element"][$keyId]["collection"])==Person::COLLECTION){
                                        if($value["source"]){
                                            if(isset($exist["element"][$keyId]["reference"]["costum"])){
                                                array_push($exist["element"][$keyId]["reference"]["costum"], $value["source"]); 
                                            }else{
                                                $exist["element"][$keyId]["reference"]=["costum"=>[$value["source"]]];                                       
                                            }
                                            unset($value["source"]);
                                        }
                                        if($value["name"]){
                                            unset($value["name"]);
                                        }
                                        if($value["email"]){
                                            unset($value["email"]);
                                        }
                                        $value["id"]=$keyId;
                                        $value["collection"]=$exist["element"][$keyId]["collection"];
                                        if(!empty($value["tags"]) && !empty($exist["element"][$keyId]["tags"])){
                                            $value["tags"]=array_merge($value["tags"],$exist["element"][$keyId]["tags"]);
                                        }
                                        // $exist["element"][$keyId]["id"]=$keyId;
                                        // $mergeElem=array_merge_recursive($exist["element"][$keyId],$value);
                                        // $mergeElem=array_unique($mergeElem);
                                    // $newElem=[];
                                    //     array_walk($mergeElem, function(&$v) {
                                    //         $newElem[$v] = array_map('array_unique', $v);
                                    //     });
                                        // var_dump($value,$exist["element"][$keyId]);exit;
                                        Element::save($value);
                                    }
                                    $element["name"] = $exist["element"][$keyId]["name"];
                                    $element["info"] = "L'élément existe déjà";
                                    $element["url"] = "/#page.type.".$typeElement.".id.".(String)$exist["element"][$keyId]["_id"] ;
                                    $element["type"] = $typeElement ;
                                    $element["id"] = (String)$exist["element"][$keyId]["_id"] ;
                                    $element["data"] = [
                                        "former" => $exist["element"][$keyId],
                                        "new" => $value
                                    ];
                                    }
                                }
                            }
                        }else{
                            $element["name"] = @$value["name"];
                            if($typeElement !== Person::COLLECTION)
                                $element["info"] = "L'élément n'a pas d'adresse.";  
                        }
                    }
                }
                catch (CTKException $e){
                    $element["name"] =  $value["name"];
                    $element["info"] = $e->getMessage();
                }
                $resData[] = $element;     
            }
            $params = array("result" => true, 
                            "resData" => $resData);
        }
        else
        {
            $params = array("result" => false); 
        }
      
        return $params;
    }
    
    public static  function setCedex($post){        
        if($post['typeFile'] == "csv"){
            $file = $post['file'];
            //$headFile = $file[0];
            unset($file[0]);
        }else{
            $file = json_decode($post['file'][0], true);
        }
        $bon = "";
        $erreur = "";
        $nb = 0;
        foreach ($file as $keyFile => $valueFile){
            if( !empty($valueFile) && !empty($valueFile[1]) && strlen(trim($valueFile[1])) > 5 && isset($valueFile[9]) && isset($valueFile[10]) ){
                $newCP = array();
                $cp = substr(trim($valueFile[1]), 0,5);
                $cedex = substr(trim($valueFile[1]), 5);
                $lat = $valueFile[9];
                $lon = $valueFile[10];
                $where = array("postalCodes.postalCode" => $cp);
                $existes = PHDB::find(City::COLLECTION, $where);
                if(empty($existes)){
                    $city = SIG::getCityByLatLngGeoShape($lat, $lon,null);
                    if(!empty($city)){
                        $newCP["postalCode"] = $cp;
                        $newCP["complement"] = trim($cedex);
                        $newCP["name"] = mb_strtoupper(trim($valueFile[2])).$cedex;
                        $newCP["geo"] = array(   "@type"=>"GeoCoordinates", 
                                        "latitude" => $lat, 
                                        "longitude" => $lon);
                        $newCP["geoPosition"] = array(  "type"=>"Point", 
                                                        "coordinates" => array( floatval($lon), 
                                                                        floatval($lat)));
                        //var_dump($newCP);
                        $city["postalCodes"][] = $newCP;
                        
                        $res = PHDB::update( City::COLLECTION, 
                                array("_id"=>new MongoId((String)$city["_id"])),
                                array('$set' => array("postalCodes" => $city["postalCodes"])));
                        $nb++;
                        $bon .=  "<br> 'cp' : '".$cp."' , 'complement' : '".trim($cedex)."' , 'name' : '".trim($valueFile[2])."' ";
                    }else{
                        $erreur .=  "<br> 'error' : 'city not found' , cp' : '".$cp."' , 'complement' : '".trim($cedex)."' , 'name' : '".trim($valueFile[2])."' ";
                    }
                }else{
                    $erreur .=  "<br> 'error' : 'cp exist déjà' , cp' : '".$cp."' , 'complement' : '".trim($cedex)."' , 'name' : '".trim($valueFile[2])."' ";
                } 
            }
        }
        echo "Il y a ".$nb."update" ;
        echo "<br><br>---------------------<br><br>";
        echo $erreur ;
        echo "<br><br>---------------------<br><br>";
        echo $bon ;
    } 
    public static  function setWikiDataID($post){        
        if($post['typeFile'] == "csv"){
            $file = $post['file'];
            //$headFile = $file[0];
            unset($file[0]);
        }
        $bon = "";
        $erreur = "";
        $nb = 0;
        $elements = array();
        $elementsWarnings = array();
        foreach ($file as $keyFile => $valueFile){
            if( !empty($valueFile) && !empty($valueFile[0]) && isset($valueFile[2]) ){
                
                $insee = $valueFile[2];
                $split = explode("http://www.wikidata.org/entity/", $valueFile[0]) ;
                $wikidataID = $split[count($split)-1];
                
                $where = array("insee" => $insee);
                $city = PHDB::find(City::COLLECTION, $where);
                if(!empty($city)){
                    foreach ($city as $key => $value) {
                        $value["modifiedByBatch"][] = array("setWikiDataID" => new MongoDate(time()));
                        $res = PHDB::update( City::COLLECTION, 
                                    array("_id"=>new MongoId($key)),
                                    array('$set' => array(  "wikidataID" => $wikidataID,
                                                            "modifiedByBatch" => $value["modifiedByBatch"])));
                        $good = array();
                        $good["insee"] = $insee ;
                        $good["wikidataID"] = $wikidataID ;
                        $elements[] =  $good;
                    }
                    
                }else{
                    $error = array();
                    $error["insee"] = $insee ;
                    $error["wikidataID"] = $wikidataID ;
                    $elementsWarnings[] =  $error;
                } 
            }
        }
        $params = array("result"=>true,
                            "elements"=>json_encode($elements),
                            "elementsWarnings"=>json_encode($elementsWarnings),
                            "listEntite"=>array());
        return $params ;
    }
    public static function isUncomplete($idEntity, $typeEntity){
        $res = false ;
        $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)));
        
        if(!empty($entity["warnings"]) || (!empty($entity["state"]) && $entity["state"] == "uncomplete"))
            $res = true;
        return $res ;
    }
    public static function checkWarning($idEntity, $typeEntity ,$userId){
        $entity = PHDB::findOne($typeEntity,array("_id"=>new MongoId($idEntity)));
        unset($entity["warnings"]);
        /*$newEntity = self::getAndCheckFromImportData($entity, $userId, null, true, true);
        if(!empty($newEntity["warnings"]))
            Element::updatePathValue($typeEntity, $idEntity, "warnings", $newEntity["warnings"]);
        else
            Element::updatePathValue($typeEntity, $idEntity, "state", true);*/
    }
  
    public static function getParams($file, $type, $url) {
        $param = array();
        if ($type == Organization::COLLECTION) {
            $map = TranslateGeoJsonToPh::$mapping_organisation;
        } elseif ($type == Person::COLLECTION) {
            $map = TranslateGeoJsonToPh::$mapping_person;
        } elseif ($type == Event::COLLECTION) {
            $map = TranslateGeoJsonToPh::$mapping_event;
        } elseif ($type == Project::COLLECTION) {
            $map = TranslateGeoJsonToPh::$mapping_project;
        } 
        $param['typeElement'] = $map["type_elt"];
        foreach ($map as $key => $value) {
            $param['infoCreateData'][$key]["valueAttributeElt"] = $value;
            $param['infoCreateData'][$key]["idHeadCSV"] = $key;
        }
        $param['typeFile'] = 'json';
        $param['pathObject'] = 'features';
        $param['nameFile'] = 'geojson';
        $param['key'] = 'geojson';
        $param['warnings'] = false;
        $param['nbTest'] = "5";
        $url_length = strlen($url);
        if ((isset($url)) && 
            (((substr($url, 0, 35) == "http://umap.openstreetmap.fr/en/map")) || ((substr($url, 0, 35) == "http://umap.openstreetmap.fr/fr/map"))) &&
            (((substr($url, $url_length - 8, $url_length)) == "geojson/") || ((substr($url, $url_length - 7, $url_length)) == "geojson"))
            ) {
            $res = self::getUmapResult($url, $param);
        } elseif ((isset($url)) && 
            (((substr($url, 0, 35) == "http://umap.openstreetmap.fr/en/map")) || (substr($url, 0, 35) == "http://umap.openstreetmap.fr/fr/map"))
            ) {
            $pos_underscore = strpos($url, "_");
            $id_map = (substr($url, $pos_underscore + 1, strlen($url)))."/";
            $url = "http://umap.openstreetmap.fr/fr/map/".$id_map."geojson";
            $res = self::getUmapResult($url, $param);
        } elseif ((isset($url)) && 
            ((substr($url, 0, 21) == "http://u.osmfr.org/m/"))
            ) {
            $id_map = (substr($url, 21, strlen($url))); 
            $url = "http://umap.openstreetmap.fr/fr/map/".$id_map."geojson";
            $res = self::getUmapResult($url, $param);
        } elseif ((isset($file)) || (isset($url))) {
            $param['file'][0] = (isset($file)) ? $file : file_get_contents($url);
           
            $result = self::previewData($param);
            $res = json_decode($result['elements']);
        } 
        if (isset($res)) {
            return $res;
        } 
    }
    public static function getDatalayersUmap($url){
        $url_map = $url;
        $umap_data = file_get_contents($url_map);
        $umap_data = json_decode($umap_data, true);
        $list_id = array();
        $list_url_datalayers = array();
        foreach ($umap_data["properties"]["datalayers"] as $key => $value) {
            $url_datalayers = 'http://umap.openstreetmap.fr/en/datalayer/'.$value['id'];
            array_push($list_url_datalayers, $url_datalayers);
        }
        return $list_url_datalayers;
    }
    public static function getUmapResult($url, $param) {
        $umap_data = file_get_contents($url);
        $list_url_data = self::getDatalayersUmap($url);
        $param['nameFile'] = $url;
        $res = array();
        foreach ($list_url_data as $keyDatalayer => $valueDatalayer) {  
            $datalayers_data = file_get_contents($valueDatalayer);
            $param['file'][0] = $datalayers_data;
            $result = self::previewData($param);
            $result = $result['elements'];
            if (!empty(json_decode($result))) {
                array_push($res, json_decode($result));
            }
        }
        return $res;
    }
    public static function getStandartMapping($allMappings=array()) {
        // $orga_standart_mapping = PHDB::find(self::MAPPINGS, array("key" => "organizationStandart"));
        // $person_standart_mapping = PHDB::find(self::MAPPINGS, array("key" => "personStandart"));
        // $event_standart_mapping = PHDB::find(self::MAPPINGS, array("key" => "eventStandart"));
        // $projet_standart_mapping = PHDB::find(self::MAPPINGS, array("key" => "projetStandart"));
        // foreach ($orga_standart_mapping as $key => $value) {
        //     $orga_key_standart = $key;
        // }
        // foreach ($person_standart_mapping as $key => $value) {
        //     $person_key_standart = $key;
        // }
        // foreach ($event_standart_mapping as $key => $value) {
        //     $event_key_standart = $key;
        // }
        // foreach ($projet_standart_mapping as $key => $value) {
        //     $projet_key_standart = $key;
        // }
        // foreach ($orga_standart_mapping as $key => $value) {
        //     $allMappings[$orga_key_standart] = $value;
        // }
        // foreach ($person_standart_mapping as $key => $value) {
        //     $allMappings[$person_key_standart] = $value;
        // }
        // foreach ($event_standart_mapping as $key => $value) {
        //     $allMappings[$event_key_standart] = $value;
        // }
        // foreach ($projet_standart_mapping as $key => $value) {
        //     $allMappings[$projet_key_standart] = $value;
        // }
        $standarts = PHDB::find(self::MAPPINGS, array("init" => true));
        //var_dump($standarts);
        if(!empty($standarts)){
            foreach ($standarts as $key => $value) {
                $allMappings[$key] = $value;
            }
        }
        return $allMappings;
    }

        //SetMapping : Ajout & Update d'un mapping
    public static function setMappings($userid,$post)
    {
        //On stock les variables
        $params = array("result" => false);
        $name = @$post['name'];
        $attributeElt = @$post['attributeElt'];
        $attributeSource = @$post['attributeSource'];
        $key = trim(@$name);
        $typeElement = @$post['typeElement'];
        $idMapping = @$post['idMapping'];

        //On les stock dans un objets.
        for($i = 0; $i<count($attributeSource); $i++)
        {
            $fields[$attributeSource[$i]] = $attributeElt[$i];
        }

        $mapping = array(
            "key" => $key,
            "name" => $name,
            "typeElement" => $typeElement,
            "userId" => $userid,
            "fields" => $fields);

        //Dans le cas si c'est un ajout
        if($idMapping == "-1" ||  $idMapping  == "5b0d1b379eaf44ea598b4580" ||$idMapping == "5b0d1b379eaf44ea598b4581" || $idMapping ==  "5b0d1b379eaf44ea598b4582" || $idMapping ==  "5b0d1b379eaf44ea598b4583"|| $idMapping == "5b1654d39eaf4427171cd718") 
        {
            $request = Yii::app()->mongodb->selectCollection(self::MAPPINGS)->insert( $mapping);
            $find = PHDB::findOne(self::MAPPINGS, array("key" => $key, "name" => $name, "userId" => $userid, "typeElement" => $typeElement));

            $params = array("result" => true,
                    "name" => $name,
                    "key" => $key,
                    "typeElement" => $typeElement,
                    "attributeElt" => $attributeElt,
                    "attributeSource" => $attributeSource,
                    "_id" => (String)$find['_id']);
        }

        //Dans le cas si c'est un upload.
        else if($idMapping != "")
        {

       $request = PHDB::update(self::MAPPINGS,array("_id"=>new MongoId((String)$idMapping)), $mapping);
       $params = array("result" => true,
               "idMapping" => $idMapping,
               "name" => $name,
               "key" => $key,
               "typeElement" => $typeElement,
               "attributeElt" => $attributeElt,
               "attributeSource" => $attributeSource);
        }

        return $params;
    }

	//Suppression d'un mapping
	public static function deleteMapping($post){
		$params = array("result" => false);
		$request = PHDB::remove(self::MAPPINGS,array("_id"=>new MongoId((String)$post['idMapping'])));
		return $params = array(	"result" => true,
								"idMappingdelete" => $post['idMapping']);
	}

	public static function epci($post){
	
		$zones = array();
		$error = array();
		$res = array();
		$update = array();
		$translates = array();
		foreach ($post["file"] as $key => $value) {
			if($key != 0 ){

				if(empty($zones[$value[2]])){

					$epci = PHDB::findOne( Zone::COLLECTION, array("epci" => $value[2]) );

					if(empty($epci)){
							$zone = array(
							"name" => $value[3],
							"epci" => $value[2],
							"countryCode" => "FR",
							"level" => array("5"),
							"cities" => array(),
						);

						$translate = array(
							"countryCode" => "FR",
							"origin" => $value[3],
							"translates" => array(),
						);
						$zones[$value[2]] = $zone;
						$translates[$value[2]] = $translate;
					} else {
						$zones[$value[2]] = $epci;
						$zones[$value[2]]["exist"] = true;
					}

					
				}

				$city = PHDB::findOne(City::COLLECTION, array("insee" => $value[0]), array("name", "level1", "level1Name", "level2", "level2Name", "level3", "level3Name", "level4", "level4Name"));
				//Rest::json($city); exit;
				if(!empty($city)){

					if(!in_array((String) $city["_id"], $zones[$value[2]]["cities"]))
						$zones[$value[2]]["cities"][] = (String) $city["_id"];

					if(empty($zones[$value[2]]["level1"]) && !empty($city["level1"])){
						$zones[$value[2]]["level1"] = $city["level1"];
						$zones[$value[2]]["level1Name"] = @$city["level1Name"];
					}

					if(empty($zones[$value[2]]["level2"]) && !empty($city["level2"])){
						$zones[$value[2]]["level2"] = $city["level2"];
						$zones[$value[2]]["level2Name"] = @$city["level2Name"];
					}

					if(empty($zones[$value[2]]["level3"]) && !empty($city["level3"])){
						$zones[$value[2]]["level3"] = $city["level3"];
						$zones[$value[2]]["level3Name"] = @$city["level3Name"];
					}

					if(empty($zones[$value[2]]["level4"]) && !empty($city["level4"])){
						$zones[$value[2]]["level4"] = $city["level4"];
						$zones[$value[2]]["level4Name"] = @$city["level4Name"];
					}

				} else {
					$error[] = array("msg" => "Erreur City","val" => $value);
				}
				
			}
		}

		//return $zones ;
		foreach ($zones as $key => $value) {

			if(!empty($value["exist"]) && $value["exist"] == true){
				$update[] = PHDB::update( Zone::COLLECTION, 
                                    array("_id"=>new MongoId((String) $value["_id"])),
                                    array('$set' => array(  "cities" => $value["cities"])));
			}else{
				$res[] = Yii::app()->mongodb->selectCollection("zones")->insert( $value);
				$translates[$key]["parentId"] = (String)$value["_id"];
				$translates[$key]["parentType"] = "zones";

				Yii::app()->mongodb->selectCollection("translate")->insert( $translates[$key]);
			}
			

			
		}


		$v = array("error" => $error, "count" => count($res), "update"=> $update, "res" => $res);
		return $v;
	}

    public static function newStart($params) {
        Yii::import("parsecsv.parsecsvlib", true);
        //Yii::import(realpath(__DIR__ . '/../../vendor/parsecsv/php-parsecsv/parsecsvlib.php'), true);
        $csv = new ParseCsv\Csv();
        $csv->auto($params['file']);
        $params["file"] = $csv->data;
        $params['infoCreateData'] = json_decode($params['infoCreateData'], true);
        return $params;
    }
}
