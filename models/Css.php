<?php 

class Css {

    const COLLECTION = "cssFile";
    const CONTROLLER = "cssFile";

    	// create or update cssFile 
	public static function createORupdateCss($contextId, $css)
    {
        $params=array(
            "contextId"=>$contextId, 
            "css"=>$css, 
            "collection"=>"cssFile",
            "created"=>time(),
            "updated"=>time());

		$cssFile = PHDB::find(Css::COLLECTION, array("contextId"=>$contextId));

		if(empty($cssFile)){
			Yii::app()->mongodb->selectCollection("cssFile")->insert($params);
		}
		else{
			Yii::app()->mongodb->cssFile->findAndModify( array("contextId"=>$contextId), array('$set' => array("css"=> $css , "updated" => time() )));
		}
    }

	// get cssFile
	public static function getCssByCostumId($contextId)
    {
		return $cssFile = PHDB::find(Css::COLLECTION, array("contextId"=>$contextId));
    }
}

?>