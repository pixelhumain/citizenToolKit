<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\models\AbstractOpenBadge;

class CryptographicKey extends AbstractOpenBadge{
    public $type; 
    public $id; 
    public $owner; 
    public $publicKeyPem;
}