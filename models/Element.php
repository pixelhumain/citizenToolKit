<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Announce;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\PNGMetaDataHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use Yii,Preference;
class Element {

	const NB_DAY_BEFORE_DELETE = 5;
	const STATUS_DELETE_PEDING = "deletePending";
	const ERROR_DELETING = "errorTryingToDelete";

	public static $urlTypes = array (
        "chat" => "Chat",
        "decisionroom" => "Salle de decision",
        "website" => "Site web",
        "partner" => "Partenaire",
        "documentation" => "Documentation",
        "wiki" => "Wiki",
        "management" => "Gestion",
	    "funding" => "Financement",
	    "other" => "Autre"
	);

	public static $elementTypeCreateByCitoyen = array(
		"organisation" => Organization::COLLECTION,
		"poi" => Poi::COLLECTION,
		"project" => Project::COLLECTION,
		/*"action" => Action::COLLECTION, 
		"activityStream" => ActivityStream::COLLECTION,
		"document" => Document::COLLECTION, */
		"events" => Event::COLLECTION,
		"annonce" => Classified::COLLECTION,
		"ressources" => Ressource::COLLECTION,
	);

	public static $connectTypes = array(
		Organization::COLLECTION => "members",
		Project::COLLECTION => "contributors",
		Event::COLLECTION => "attendees",
		Person::COLLECTION => "friends"
	);

	public static $connectAs = [
		Organization::COLLECTION => "member",
		Project::COLLECTION => "contributor",
		Event::COLLECTION => "attendee",
		Person::COLLECTION => "friend"
	];

	public static $linkTypes = [
		"followers",
		"needs",
		"projects",
		"events",
		"memberOf",
		"follows",
		"proposals",
		"members",
		"contributors",
		"attendees",
		"organizer",
		"answers",
		"knows" ];

	public static function getControlerByCollection ($type=null) { 

		$ctrls = array(
	    	Organization::COLLECTION => Organization::CONTROLLER,
	    	Person::COLLECTION => Person::CONTROLLER,
	    	Event::COLLECTION => Event::CONTROLLER,
	    	Project::COLLECTION => Project::CONTROLLER,
			News::COLLECTION => News::COLLECTION,
	    	City::COLLECTION => City::CONTROLLER,
	    	Survey::COLLECTION => Survey::CONTROLLER,
	    	Poi::COLLECTION => Poi::CONTROLLER,
	    	Cms::COLLECTION => Cms::CONTROLLER,
	    	Template::COLLECTION => Template::CONTROLLER,
	    	Proposal::COLLECTION => Proposal::CONTROLLER,
	    	Action::COLLECTION => Action::CONTROLLER,
	    	Classified::COLLECTION => Classified::CONTROLLER,
	    	Classified::TYPE_RESSOURCES => Classified::TYPE_RESSOURCES_CONTROLLER,
	    	Classified::TYPE_JOBS => Classified::TYPE_JOBS_CONTROLLER,
	    	Answer::COLLECTION => Answer::CONTROLLER,
	    );	    
    	return (isset($type)) ? @$ctrls[$type] : $ctrls;
    }


    public static function getCollectionByControler ($type) { 

		$ctrls = array(
	    	Organization::CONTROLLER => Organization::COLLECTION,
	    	Person::CONTROLLER => Person::COLLECTION,
	    	Event::CONTROLLER => Event::COLLECTION,
	    	Project::CONTROLLER => Project::COLLECTION,
			News::COLLECTION => News::COLLECTION,
	    	City::CONTROLLER => City::COLLECTION,
	    	Survey::CONTROLLER => Survey::COLLECTION,
	    	Poi::CONTROLLER => Poi::COLLECTION,
	    	Cms::CONTROLLER => Cms::COLLECTION,
	    	Template::COLLECTION => Template::CONTROLLER,
	    	Proposal::CONTROLLER => Proposal::COLLECTION,
	    	Action::CONTROLLER => Action::COLLECTION,
	    );	    
    	return @$ctrls[$type];
    }

    public static function getModelByType($type) {
    	$models = array(
	    	Organization::COLLECTION => "Organization",
	    	Person::COLLECTION 		 => "Person",
	    	Event::COLLECTION 		 => "Event",
	    	Project::COLLECTION 	 => "Project",
			News::COLLECTION 		 => "News",
	    	City::COLLECTION 		 => "City",
	    	Thing::COLLECTION 		 => "Thing",
	    	Poi::COLLECTION 		 => "Poi",
	    	Cms::COLLECTION 		 => "Cms",		
			Template::COLLECTION 	 => "Template",
	    	Classified::COLLECTION   => "Classified",
	    	Crowdfunding::COLLECTION => "Crowdfunding",
	    	Product::COLLECTION 	 => "Product",
	    	Service::COLLECTION   	 => "Service",
	    	Survey::COLLECTION   	 => "Survey",
	    	Bookmark::COLLECTION   	 => "Bookmark",
	    	Proposal::COLLECTION   	 => "Proposal",
	    	Room::COLLECTION   	 	 => "Room",
	    	Action::COLLECTION   	 => "Action",
	    	Network::COLLECTION   	 => "Network",
	    	Url::COLLECTION   	 	 => "Url",
	    	Circuit::COLLECTION   	 => "Circuit",
	    	Risk::COLLECTION   => "Risk",
	    	Badge::COLLECTION   => "Badge",
			Endorsement::COLLECTION => "Endorsement",
	    	Form::COLLECTION => "Form",
	    	Form::INPUTS_COLLECTION => "Form",
            Document::COLLECTION => "Document",
			Lists::COLLECTION => "Lists",
			//PaymentMethod::COLLECTION => "PaymentMethod",
	    );	
	 	return @$models[$type];     
    }
    
    public static function getCommonByCollection ($type) { 

		$commons = array(
	    	Organization::COLLECTION => "organisation",
	    	Event::COLLECTION => "event",
	    	Project::COLLECTION => "project"
	    );	    
    	return @$commons[$type];
    }

    public static function getFaIcon ($type) { 

		$fas = array(
	    	Organization::COLLECTION 	=> "group",
	    	Person::COLLECTION 			=> "user",
	    	Event::COLLECTION 			=> "calendar",
	    	Project::COLLECTION 		=> "lightbulb-o",
			News::COLLECTION 			=> "rss",
	    	City::COLLECTION 			=> "university",
	    	ActionRoom::TYPE_ACTION		=> "cog",
	    	ActionRoom::TYPE_ENTRY		=> "archive",
	    	ActionRoom::TYPE_DISCUSS	=> "comment",
	    	ActionRoom::TYPE_VOTE		=> "archive",
	    	ActionRoom::TYPE_ACTIONS	=> "cogs",
	    	Proposal::COLLECTION 		=> "gavel",
	    	Organization::TYPE_NGO 		=> "group",
	    	Organization::TYPE_BUSINESS => "industry",
	    	Organization::TYPE_GROUP 	=> "circle-o",
	    	Organization::TYPE_GOV 		=> "university",
	    	Organization::TYPE_COOP 		=> "industry",
	    	Classified::COLLECTION 		=> "bullhorn",
	    	Classified::TYPE_RESSOURCES	=>"cubes",
	    	Classified::TYPE_JOBS=>"briefcase",
	    );	
	    
	    if(isset($fas[$type])) return $fas[$type];
	    else return false;
    }
    public static function getColorIcon ($type) { 
    	$colors = array(
	    	Organization::COLLECTION 	=> "green",
	    	Person::COLLECTION 			=> "yellow",
	    	Event::COLLECTION 			=> "orange",
	    	Project::COLLECTION 		=> "purple",
	    	Organization::TYPE_NGO 		=> "green",
	    	Organization::TYPE_BUSINESS => "azure",
	    	Organization::TYPE_GROUP 	=> "turq",
	    	Organization::TYPE_GOV 		=> "red",
	    	Organization::TYPE_COOP 	=> "grey",
	    	Classified::COLLECTION 		=> "azure",
	    	Classified::TYPE_RESSOURCES	=>"vine",
	    	Classified::TYPE_JOBS=>"yellow-k",
	    );	
	    if(isset($colors[$type])) return $colors[$type];
	    else return false;
	}

	public static function getColorMail ($type) { 
    	$colors = array(
	    	Organization::COLLECTION 	=> "green",
	    	Person::COLLECTION 			=> "#FFC600",
	    	Event::COLLECTION 			=> "orange",
	    	Project::COLLECTION 		=> "purple",
	    	Organization::TYPE_NGO 		=> "green",
	    	Organization::TYPE_BUSINESS => "azure",
	    	Organization::TYPE_GROUP 	=> "turq",
	    	Organization::TYPE_GOV 		=> "red",
	    	Organization::TYPE_COOP 	=> "grey",
	    	Classified::COLLECTION 		=> "#2BB0C6",
	    	Classified::TYPE_RESSOURCES	=>"#2BB0C6",
	    	Classified::TYPE_JOBS=>"#2BB0C6",
	    );	
	    if(isset($colors[$type])) return $colors[$type];
	    else return false;
	}
    
    public static function getElementSpecsByType ($type) { 
    	$ctrler = self::getControlerByCollection ($type);
    	$prefix = "#".$ctrler;
		$fas = array(

	    	Person::COLLECTION 			=> array("icon"=>"user","color"=>"#FFC600","text-color"=>"yellow",
	    										 "hash"=> Person::CONTROLLER.".detail.id.",
	    										 "collection"=>Person::COLLECTION),
	    	Person::CONTROLLER 			=> array("icon"=>"user","color"=>"#FFC600","text-color"=>"yellow",
	    										 "hash"=> Person::CONTROLLER.".detail.id.",
	    										 "collection"=>Person::COLLECTION),

	    	Organization::COLLECTION 	=> array("icon"=>"group", "color"=>"#93C020","text-color"=>"green",
	    										 "hash"=> Organization::CONTROLLER.".detail.id."),
	    	Organization::CONTROLLER 	=> array("icon"=>"group", "color"=>"#93C020","text-color"=>"green",
	    										 "hash"=> Organization::CONTROLLER.".detail.id.",
	    										 "collection"=>Organization::COLLECTION),

	    	
	    	Event::COLLECTION 			=> array("icon"=>"calendar","color"=>"#FFA200","text-color"=>"orange",
	    										 "hash"=> Event::CONTROLLER.".detail.id."),
	    	Event::CONTROLLER 			=> array("icon"=>"calendar","color"=>"#FFA200","text-color"=>"orange",
	    										 "hash"=> Event::CONTROLLER.".detail.id.",
	    										 "collection"=>Event::COLLECTION),

	    	Project::COLLECTION 		=> array("icon"=>"lightbulb-o","color"=>"#8C5AA1","text-color"=>"purple",
	    										 "hash"=> Project::CONTROLLER.".detail.id."),
	    	Project::CONTROLLER 		=> array("icon"=>"lightbulb-o","color"=>"#8C5AA1","text-color"=>"purple",
	    										 "hash"=> Project::CONTROLLER.".detail.id.",
	    										 "collection"=>Project::COLLECTION),

			Classified::COLLECTION 		=> array("icon"=>"bullhorn","color"=>"#2BB0C6","text-color"=>"azure",
	    										 "hash"=> Classified::CONTROLLER.".detail.id."),
	    	Classified::CONTROLLER 		=> array("icon"=>"bullhorn","color"=>"#2BB0C6","text-color"=>"azure",
	    										 "hash"=> Classified::CONTROLLER.".detail.id.",
	    										 "collection"=>Classified::COLLECTION),
			Ressource::COLLECTION 		=> array("icon"=>"cube","color"=>"#2BB0C6","text-color"=>"vine",
	    										 "hash"=> Ressource::CONTROLLER.".detail.id."),
	    	Ressource::CONTROLLER 		=> array("icon"=>"cube","color"=>"#2BB0C6","text-color"=>"vine",
	    										 "hash"=> Ressource::CONTROLLER.".detail.id.",
	    										 "collection"=>Ressource::COLLECTION),
			
			Product::COLLECTION 		=> array("icon"=>"gift","color"=>"#2BB0C6","text-color"=>"azure",
	    										 "hash"=> Product::CONTROLLER.".detail.id."),
	    	Product::CONTROLLER 		=> array("icon"=>"gift","color"=>"#2BB0C6","text-color"=>"azure",
	    										 "hash"=> Product::CONTROLLER.".detail.id.",
	    										 "collection"=>Product::COLLECTION),
	    	Service::COLLECTION 		=> array("icon"=>"gift","color"=>"#2BB0C6","text-color"=>"azure",
	    										 "hash"=> Service::CONTROLLER.".detail.id."),
	    	Service::CONTROLLER 		=> array("icon"=>"gift","color"=>"#2BB0C6","text-color"=>"azure",
	    										 "hash"=> Service::CONTROLLER.".detail.id.",
	    										 "collection"=>Service::COLLECTION),

			Poi::COLLECTION 			=> array("icon"=>"map-marker","color"=>"#2BB0C6","text-color"=>"green-poi",
	    										 "hash"=> Poi::CONTROLLER.".detail.id."),
	    	Poi::CONTROLLER 			=> array("icon"=>"map-marker","color"=>"#2BB0C6","text-color"=>"green-poi",
	    										 "hash"=> Poi::CONTROLLER.".detail.id.",
	    										 "collection"=>Poi::COLLECTION),

			News::COLLECTION 			=> array("icon"=>"rss","hash"=> $prefix.""),

	    	City::COLLECTION 			=> array("icon"=>"university","color"=>"#E33551","text-color"=>"red",
	    										 "hash"=> $prefix.".detail.insee."),
	    	City::CONTROLLER 			=> array("icon"=>"university","color"=>"#E33551","text-color"=>"red",
	    										 "hash"=> $prefix.".detail.insee."),
	    	
	    	ActionRoom::TYPE_VOTE		=> array("icon"=>"archive","color"=>"#3C5665", "text-color"=>"azure",
	    		 								 "hash"=> "survey.entries.id.",
	    		 								 "collection"=>ActionRoom::COLLECTION),
	    	ActionRoom::TYPE_VOTE."s"	=> array("icon"=>"archive","color"=>"#3C5665", "text-color"=>"azure",
	    		 								 "hash"=> "survey.entries.id.",
	    		 								 "collection"=>ActionRoom::COLLECTION),
	    	ActionRoom::TYPE_ACTIONS	=> array("icon"=>"cogs","color"=>"#3C5665", "text-color"=>"lightblue2",
	    		 								 "hash"=> "rooms.actions.id.",
	    		 								 "collection"=>ActionRoom::COLLECTION),
	    	ActionRoom::TYPE_ACTIONS."s"=> array("icon"=>"cogs","color"=>"#3C5665", "text-color"=>"lightblue2",
	    		 								 "hash"=> "rooms.actions.id.",
	    		 								 "collection"=>ActionRoom::COLLECTION),
	    	ActionRoom::TYPE_ACTION		=> array("icon"=>"cog","color"=>"#3C5665", "text-color"=>"lightblue2",
	    		 								 "hash"=> "rooms.action.id.",
	    		 								 "collection"=>ActionRoom::COLLECTION_ACTIONS),
	    	ActionRoom::TYPE_ACTION."s"	=> array("icon"=>"cog","color"=>"#3C5665", "text-color"=>"lightblue2",
	    		 								 "hash"=> "rooms.action.id.",
	    		 								 "collection"=>ActionRoom::COLLECTION_ACTIONS),
	    	ActionRoom::TYPE_ENTRY		=> array("icon"=>"archive","color"=>"#3C5665", "text-color"=>"dark",
	    										 "hash"=> "survey.entry.id.",
	    										 "collection"=>Survey::COLLECTION ),
	    	ActionRoom::TYPE_ENTRY."s"	=> array("icon"=>"archive","color"=>"#3C5665", "text-color"=>"dark",
	    										 "hash"=> "survey.entry.id.",
	    										 "collection"=>Survey::COLLECTION ),
	    	Survey::COLLECTION	=> array("icon"=>"archive","color"=>"#3C5665", "text-color"=>"dark",
	    										 "hash"=> "survey.entry.id.",
	    										 "collection"=>Survey::COLLECTION ),
	    	Survey::CONTROLLER	=> array("icon"=>"archive","color"=>"#3C5665", "text-color"=>"dark",
	    										 "hash"=> "survey.entry.id.",
	    										 "collection"=>Survey::COLLECTION ),
	    	ActionRoom::TYPE_DISCUSS	=> array("icon"=>"comment","color"=>"#3C5665", "text-color"=>"dark",
	    										 "hash"=> "comment.index.type.actionRooms.id.",
	    										 "collection"=>ActionRoom::COLLECTION),
	    	ActionRoom::TYPE_DISCUSS."s"=> array("icon"=>"comment","color"=>"#3C5665", "text-color"=>"dark",
	    										 "hash"=> "comment.index.type.actionRooms.id.",
	    										 "collection"=>ActionRoom::COLLECTION)
	    );	
	    //echo $type.Project::COLLECTION;
	    if( isset($fas[$type]) ) 
	    	return $fas[$type];
	    else 
	    	return false;
    }



    public static function checkIdAndType($id, $type, $actionType=null) {
		if ($type == Organization::COLLECTION) {
        	$res = Organization::getById($id); 
            if (@$res["disabled"] && $actionType != "disconnect") {
                throw new CTKException("Impossible to link something on a disabled organization");    
            }
        } 
        else if ( in_array($type, array( Person::COLLECTION, Project::COLLECTION,Event::COLLECTION, Classified::COLLECTION, Poi::COLLECTION,Network::COLLECTION,Proposal::COLLECTION, Cms::COLLECTION,Template::COLLECTION) ) ){
            $res = self::getByTypeAndId($type, $id);       
        } else if ($type== ActionRoom::COLLECTION_ACTIONS){
            $res = ActionRoom:: getActionById($id);
        } else if ( $type == Survey::COLLECTION) {
            $res = Survey::getById($id);
        } else if ( $type == Form::COLLECTION) {
            $res = Form::getByIdMongo($id);
        } else if ( $type == Form::ANSWER_COLLECTION) {
            $res = PHDB::findOne( Form::ANSWER_COLLECTION, array("_id"=>new MongoId($id)) );
        }  else {
        	throw new CTKException("Can not manage this type : ".$type);
        }
        if (empty($res)) throw new CTKException("The actor (".$id." / ".$type.") is unknown");

        return $res;
    }
    

    /**
     * Return a link depending on the type and the id of the element.
     * The HTML link could be kind of : <a href="" onclick="urlCtrl.loadByHash(...)">name</a>
     * If urlCtrl.loadByHashOnly is set : only the urlCtrl.loadByHash will be returned
     * @param String $type The type of the entity
     * @param String $id The id of the entity
     * @param type|null $loadByHashOnly if true, will return only the urlCtrl.loadByHash not surounded by the html link
     * @return String the link on the loaByHash to display the detail of the element
     */
    public static function getLink( $type, $id, $hashOnly=null ) {	    
    	$link = ""; 
    	$specs = self::getElementSpecsByType ($type);
    	if( @$specs["collection"] )
    		$type = $specs["collection"];

    	if(@$type && @$id && $type != City::COLLECTION){
    		if (!$hashOnly)
    			$el = PHDB::findOne ( $type , array( "_id" => new MongoId($id) ),array("name") );
	    	
	    	$link = $specs["hash"].$id;
	    }
	    else if($type == City::COLLECTION){
	    	$el = City::getByUnikey($id);
	    	$link = $specs["hash"].$el['insee'].".postalCode.".$el['cp'];
	    }
	    
	    //if ( !$hashOnly && @$el ) 
	    $link = '<a href="#'.$link.'" class="lbh add2fav">'.htmlspecialchars(@$el['name']).'</a>';
	    
    	return $link;
    }

	public static function getByWhere($type, $where) {
		return PHDB::find( $type,$where,null,null);
 	}

	public static function getByTypeAndId($type, $id,$what=null, $update=null){
		if(is_array($id) && count($id)!=0){
			$arrId = array();
            foreach ($id as $key => $value) {
                $arrId[] = new MongoId($value);
            }
			$element = PHDB::find($type,array("_id" =>['$in' => $arrId]));
		}elseif( @$what ) 
			$element = PHDB::findOneById($type, $id, $what);
		else if($type == Person::COLLECTION)
			$element = Person::getById($id);
		else if($type == Organization::COLLECTION || $type == Organization::CONTROLLER  )
			$element = Organization::getById($id);		
		else if($type == Project::COLLECTION || $type == Project::CONTROLLER)
			$element = Project::getById($id);
		else if($type == Event::COLLECTION || $type == Event::CONTROLLER )
			$element = Event::getById($id);	
		else if($type == Event::COLLECTION || $type == Event::CONTROLLER ){

			$eventElement = Event::getById($id);	
			if(@$eventElement["fromActivityPub"] && $eventElement["fromActivityPub"] == true){
				$user = Yii::app()->session["user"];
			    if(Preference::isActivitypubActivate($user["preferences"])){
        		$activity = Utils::activitypubObjectToEvent($eventElement["objectUUID"]);
					foreach ($activity as $activityKey => $activityValue) {
						$element[$activityKey] = $activityValue;
					}
					$element["_id"] = $id;
				}
				
				//$element["modified"] = $eventElement["modified"];
			}else{
				$element = $eventElement;
			}
		}
		else if($type == City::COLLECTION && !(is_string($id) && strlen($id) == 24 && ctype_xdigit($id)))
			$element = City::getIdByInsee($id);
		else if($type == Poi::COLLECTION)
			$element = Poi::getById($id);
		else if($type == Crowdfunding::COLLECTION)
			$element = Crowdfunding::getCampaignById($id);
		else if($type == Cms::COLLECTION)
			$element = Cms::getById($id);
		else if($type == Template::COLLECTION)
			$element = Template::getById($id);
		else if($type == Classified::COLLECTION || $type == Classified::MODULE)
			$element = Classified::getById($id);
		//else if($type == Ressource::COLLECTION || $type == Ressource::MODULE)
		//	$element = Ressource::getById($id);
		else if($type == ActionRoom::COLLECTION_ACTIONS)
			$element = PHDB::findOne( ActionRoom::COLLECTION_ACTIONS ,array("_id"=>new MongoId($id)));
		else if($type == Survey::CONTROLLER )
			$element = PHDB::findOne( Survey::COLLECTION ,array("_id"=>new MongoId($id)));
		else if($type == Proposal::COLLECTION )
			$element = PHDB::findOne( Proposal::COLLECTION ,array("_id"=>new MongoId($id)));
		else if($type == Action::COLLECTION )
			$element = PHDB::findOne( Action::COLLECTION ,array("_id"=>new MongoId($id)));
		else if($type == Room::COLLECTION )
			$element = PHDB::findOne( Room::COLLECTION ,array("_id"=>new MongoId($id)));
		else if($type == Network::COLLECTION )
			$element = Network::getById($id);
		else if($type == Service::COLLECTION)
			$element = Service::getById($id);
		else if($type == Product::COLLECTION)
			$element = Product::getById($id);
		else if($type == News::COLLECTION)
			$element = News::getById($id);
		else if($type == Costum::COLLECTION)
			$element = Costum::getBySlug($id);
		else if($type == Badge::COLLECTION)
            $element = Badge::getById($id);
		else
			$element = PHDB::findOne($type,array("_id"=>new MongoId($id)));
	  	
	  	if ($element == null) 
	  		$element = Element::getGhost($type);

		if(Costum::isSameFunction("getElement")){
			$element = Costum::sameFunction("getElement", $element);
		}
	  		//throw new CTKException("The element you are looking for has been moved or deleted");
		if(@$update && $update){
	  	 	if(!@$element["images"]){
		  		$typeEl=(in_array($type, [Event::CONTROLLER, Project::CONTROLLER, Organization::CONTROLLER])) ? Element::getCollectionByControler($type) : $type; 
		  		$where=array(
		  			"id"=>$id, "type"=>$typeEl, "doctype"=>"image", 
		  			/*"contentKey"=>"profil",*/ "current"=>array('$exists'=>true)
		  		);
		  		$element["images"] = Document::getListDocumentsWhere($where, "image");
		  	}
	  	 	if(!@$element["files"]){
		  		$typeEl=(in_array($type, [Event::CONTROLLER, Project::CONTROLLER, Organization::CONTROLLER])) ? Element::getCollectionByControler($type) : $type; 
		  		$where=array(
		  			"id"=>$id, "type"=>$typeEl, "doctype"=>"file"
		  		);
		  		$element["files"] = Document::getListDocumentsWhere($where, "image");
		  	}
		  	$finderToUp=array("parent","producer");
		  	foreach($finderToUp as $k){
			  	if(@$element[$k]){
			  		foreach($element[$k] as $key => $v){
			  			$elt=self::getElementById( $key, $v["type"], null, ["profilThumbImageUrl", "name", "slug"] );
			  			if(!empty($elt)){
			  				$element[$k][$key]=array_merge($element[$k][$key], $elt);
			  			}
			  		}
			  	}
		  	}
		  	//if($type==Classified::COLLECTION) $element["type"]=$element["typeClassified"];
		  	//if($type==Organization::COLLECTION && isset($element["typeOrga"])) $element["type"]=$element["typeOrga"];
		  	//if($type==Event::COLLECTION){
		  	 //	if(isset($element["typeEvent"])) $element["type"]=$element["typeEvent"];
		  	 	//if(isset($element["startDate"])) $element["startDate"]=date_format(date_create($element["startDate"]), 'd/m/Y H:i');
		  	 	//if(isset($element["endDate"])) $element["endDate"]=date_format(date_create($element["endDate"]), 'd/m/Y H:i');
		  	//}
		  	$element["public"]=(@$element["preferences"] && @$element["preferences"]["private"] && $element["preferences"]["private"]) ? false : true;

		  	if(Costum::isSameFunction("prepDataForUpdate")){
		  		$element = Costum::sameFunction("prepDataForUpdate", $element);
			}
	  	}
	  	$el = $element;
		if(@$el["links"]) foreach(array("followers", "follows", "members", "memberOf", "contributors" ,"friends") as $key)
			if(@$el["links"][$key])
			$element["counts"][$key] = count($el["links"][$key]);

	  	return $element;
	}
	public static function getSimpleByTypeAndId($type, $id,$what=null){

		if( @$what ) 
			$element = PHDB::findOneById($type, $id, $what);
		else if($type == Person::COLLECTION)
			$element = Person::getSimpleUserById($id);
		else if($type == Organization::COLLECTION)
			$element = Organization::getSimpleOrganizationById($id);		
		else if($type == Project::COLLECTION)
			$element = Project::getSimpleProjectById($id);	
		else if($type == Event::COLLECTION)
			$element = Event::getSimpleEventById($id);	
		else if($type == City::COLLECTION)
			$element = City::getIdByInsee($id);
		else if($type == Poi::COLLECTION)
			$element = Poi::getById($id);
		else if($type == "action")
			$element = PHDB::findOne("actions",array("_id"=>new MongoId($id)));
		else
			$element = PHDB::findOne($type,array("_id"=>new MongoId($id)));
	  	
	  	if ($element == null) 
	  		$element = Element::getGhost($type);
	  		//throw new CTKException("The element you are looking for has been moved or deleted");
	  	return $element;
	}

	public static function getGhost($type){
		return array("name"=>"Unknown (deleted)", 
					 "slug"=>"unknown",
					 "type"=>$type,
					 "typeSig"=>$type);
	}

	/**
	 * get all poi details of an element
	 * @param type $id : is the mongoId (String) of the parent
	 * @param type $type : is the type of the parent
	 * @return list of pois
	 */
	public static function getByIdAndTypeOfParent($collection, $id, $type, $orderBy,$where=null){
		$condition=array('$or' => array(
			array("parentId"=>$id,"parentType"=>$type),
			array("parent.".$id =>  array('$exists' => 1))
			));
		if(@$where && !empty($where))
			$condition=array_merge($condition, $where);
		$list = PHDB::findAndSort($collection,$condition, $orderBy);
	   	return $list;
	}
	/**
	 * get poi with limit $limMin and $limMax
	 * @return list of pois
	 */
	public static function getByTagsAndLimit($collection, $limitMin=0, $indexStep=15, $searchByTags=""){
		$where = array("name"=>array('$exists'=>1));
		if(@$searchByTags && !empty($searchByTags)){
			$queryTag = array();
			foreach ($searchByTags as $key => $tag) {
				if($tag != "")
					$queryTag[] = new MongoRegex("/".$tag."/i");
			}
			if(!empty($queryTag))
				$where["tags"] = array('$in' => $queryTag); 			
		}
		
		$list = PHDB::findAndSort( $collection, $where, array("updated" => -1));
	   	return $list;
	}

    public static function getInfos( $type, $id, $loadByHashOnly=null ) {	    
    	$link = ""; 
    	$name = ""; 
    	if(@$type && @$id && $type != City::COLLECTION){
    		$el = PHDB::findOne ( $type , array( "_id" => new MongoId($id) ) );
	    	$ctrl = self::getControlerByCollection($type);
	    	if( @$el && @$ctrl )
	    		$link = "urlCtrl.loadByHash('#".$ctrl.".detail.id.".$id."')";
	    }
	    else if($type == City::COLLECTION){
	    	$el = City::getByUnikey($id);
	    	$ctrl = self::getControlerByCollection($type);
	    	if( @$el && @$ctrl )
	    		$link = "urlCtrl.loadByHash('#".$ctrl.".detail.insee.".$el['insee'].".cp.".$el['cp']."')";
	    }
	    
	    if (! $loadByHashOnly) {
	    	$link = "<a href='javascript:;' onclick=\"".$link."\">".$el['name']."</a>";
	    }
	    
    	return array( "link" => $link , 
    					"name" => $el['name'], 
    					"profilThumbImageUrl" => @$el['profilThumbImageUrl'], 
    					"type"=>$type,
    					"id"=> $id);
    }



    private static function getDataBinding($collection) {
		$mod=self::getModelByType($collection);
		if(method_exists($mod, "getDataBinding")){
			return $mod::getDataBinding();
		}
    	if(function_exists($mod."::getDataBinding")){
			return $mod::getDataBinding();
		} else {
			return array();
		}
	}

    private static function getCollectionFieldNameAndValidate($collection, $elementFieldName, $elementFieldValue, $elementId) {
		return DataValidator::getCollectionFieldNameAndValidate(self::getDataBinding($collection), $elementFieldName, $elementFieldValue, $elementId);
	}


	/*
	collection de la mongo db
	id 
	path : geo.latitude.value
	value : 34.434343434 or obj
	arrayForm : if the path point to an array type data
	edit : for arrays to define when we are creating a new entry or updating an existing line
	pull : when remove element from an array form type 
	*/
	public static function updatePathValue($collection, $id, $path, $value, $arrayForm=null, $edit=null, $pull=null ,$formParentId = null, $updatePartial=null, $tplGenerator=null,$renameKey=null) {
		if ( !Authorisation::isInterfaceAdmin($id, $collection) && 
			!Authorisation::isParentAdmin($id, $collection , Yii::app()->session["userId"]) &&
			!Authorisation::isElementChildAdmin($id, $collection, Yii::app()->session["userId"], null, $formParentId) &&
			!Authorisation::canEditItemOrOpenEdition($id, $collection, Yii::app()->session['userId']) && 
			!Authorisation::specificCondition($collection, $id, $tplGenerator,$path)
			) {
			throw new CTKException(Yii::t("common","Can not update the element : you are not authorized to update that element !"));
		}

		$res=false;
        $msg=Yii::t("common","Please Login First");
        //var_dump(Yii::app()->session["userId"]);exit;

        if ( Person::logguedAndAuthorized() || Authorisation::specificCondition($collection, $id, $tplGenerator, $path) )
        {
			//make sure element exists
			$el = PHDB::findOne($collection , [ "_id" => new MongoId($id) ]);
			if(!empty($el))
			{
				$value = (!empty($value) || $value == "0" || $value === false ) ? $value : null;
				$verb = '$set';

				//creates a new entre in an array
				//otherwise edit just updates the value 
				if( @$arrayForm && !@$edit){
					//$verb = '$addToSet'; not accept dumplication of element in array
					$verb = '$push'; // accept dumplication of element in array
					if($value == null)
						$verb = '$unset';
				}   
				//echo "xxx".empty($value)."xxx"; exit;
				if($value === null)
					$verb = '$unset';
				if(!empty($renameKey))
					$verb = '$rename';
				// if( $value != null && !@$pull ) {
				//     $value["modified"] = time();
				//     $value["user"] = Yii::app()->session["userId"];
				// }
				
				//modify the value linked to the given path
				if($path == "allToRoot" && empty($updatePartial)){
					// PHDB::update( $collection,
					// 	[ "_id" => new MongoId($id) ], 
					// 	[ $verb => $value ]);

					if (is_array($value)) {
						$set = [];
						$unset = [];
				
						foreach ($value as $k => $v) {
							if ($v === null || $v === "") {
								$unset[$k] = "";
							} else {
								$set[$k] = $v;
							}
						}
				
						$updateData = [];
						if (!empty($set)) {
							$updateData['$set'] = $set;
						}
						if (!empty($unset)) {
							$updateData['$unset'] = $unset;
						}
				
						if (!empty($updateData)) {
							PHDB::update(
								$collection,
								["_id" => new MongoId($id)],
								$updateData
							);
						}
					} else {
						PHDB::update(
							$collection,
							["_id" => new MongoId($id)],
							[$verb => $value]
						);
					}
				}else if($updatePartial == true && !empty($path) && !empty($id)){
					// foreach ($value as $k => $v) {
					// 	$set = ($path == 'allToRoot') ? [$k => $v ] : [ $path.".".$k => $v ];
					// 	PHDB::update( $collection,
					// 		[ "_id" => new MongoId($id) ], 
					// 		[ '$set' => $set ]);							
					// }
					foreach ($value as $k => $v) {
						if ($v === null || $v === "") {
							$unset = ($path == 'allToRoot') ? [$k => ""] : [$path.".".$k => ""];
							PHDB::update(
								$collection,
								["_id" => new MongoId($id)],
								['$unset' => $unset]
							);
						} else {
							$set = ($path == 'allToRoot') ? [$k => $v] : [$path.".".$k => $v];
							PHDB::update(
								$collection,
								["_id" => new MongoId($id)],
								['$set' => $set]
							);
						}
					}
				}else if(is_array($value) && @$pull && @$arrayForm){
					PHDB::update(   $collection,
									[ "_id" => new MongoId($id) ],
									['$pullAll' => [$path => $value]]);
				}else{
					PHDB::update( $collection,
						[ "_id" => new MongoId($id) ], 
						[ $verb => [ $path => $value ] ]);
				}

				if($value === "updatedTime"){
					PHDB::update( $collection,
						[ "_id" => new MongoId($id) ], 
						[ $verb => [ $path => time()] ]);
				}

				PHDB::update( $collection,
					[ "_id" => new MongoId($id) ], 
					[ '$set'=> [ "updated" => time() ]] );

				//remove an element from an array
				if($value == null && @$pull ){
					PHDB::update(   $collection,
									[ "_id" => new MongoId($id) ],
									['$pull' => [ $pull => null ]]);
				}


				$msg=Yii::t("common","Everything is allRight");

				if( $collection == Event::COLLECTION || $collection == Project::COLLECTION || $collection == Organization::COLLECTION ){
					Slug::updateElemTime( $el['slug'], time() );
				}
				$res=true;
				//
				$el = PHDB::findOne($collection , [ "_id" => new MongoId($id) ] );
			} else
				$msg= "Element not found";
        } 

		//echo json_encode( array("result"=>$res, "msg"=>$msg, "elt"=>$el) );
		return array("result"=>$res, "msg"=>$msg, "elt"=>$el);
	}

    public static function updateField($collection, $id, $fieldName, $fieldValue, $allDay=null) {
    	if (!Authorisation::canEditItemOrOpenEdition($id, $collection, Yii::app()->session['userId'])) {
			throw new CTKException(Yii::t("common","Can not update the element : you are not authorized to update that element !"));
		}

		if(is_string($fieldValue))
			$fieldValue = trim($fieldValue);

		//xss clean up 
		$clean = DataValidator::clearUserInput([ $fieldName => $fieldValue ]);

			if(is_array($clean) && count($clean) === 1){
				$fieldValue = $clean[$fieldName];
			} else {
				$fieldValue = null;
			}
		
		//Manage boolean allDay. TODO SBAR - Trouver un autre moyen que de le mettre ici
		if ($fieldName == "allDay") {
			if ($fieldValue == "true") 
				$fieldValue = true;
			else
				$fieldValue = false;
		}
		// var_dump($collection);
		// var_dump($fieldName);
		$dataFieldName = self::getCollectionFieldNameAndValidate($collection, $fieldName, $fieldValue, $id);
		// var_dump($dataFieldName);
		// $dataFieldName  = $fieldName;

		$verb = (empty($fieldValue) ? '$unset' : '$set');
		
		if ($dataFieldName == "name") 
			$fieldValue = $fieldValue;
		if ($dataFieldName == "tags") {
			$fieldValue = Tags::filterAndSaveNewTags($fieldValue);
			$set = array($dataFieldName => $fieldValue);
			// if(!empty($fieldValue)){
			// 	$fieldValue=(gettype($fieldValue)=="string") ? [$fieldValue] : $fieldValue;
			// 	// var_dump($fieldValue['$pullAll']);exit;
			// 	if(isset($fieldValue['$pullAll'])){
			// 		$verb='$pullAll';
			// 		$set = array($dataFieldName => $fieldValue['$pullAll']);
			// 	}else{
			// 	    $fieldValue = Tags::filterAndSaveNewTags($fieldValue);
			// 		$verb='$addToSet';
			// 		$set=array($dataFieldName => array('$each'=>$fieldValue));
	
			// 	}
			// }
		}
		else if ( ($dataFieldName == "telephone.mobile"|| $dataFieldName == "telephone.fixe" || $dataFieldName == "telephone.fax")){
			if($fieldValue ==null)
				$fieldValue = array();
			else {
				$split = explode(",", $fieldValue);
				$fieldValue = array();
				foreach ($split as $key => $value) {
					$fieldValue[] = trim($value);
				}
			}
			$set = array($dataFieldName => $fieldValue);
		}
		else if ($fieldName == "locality") {
			//address
			try{
				if(!empty($fieldValue)){
					$verb = '$set';
					$address = array(
						"@type" => "PostalAddress",
						"codeInsee" => $fieldValue["address"]["codeInsee"],
						"addressCountry" => $fieldValue["address"]["addressCountry"],
						"postalCode" => $fieldValue["address"]["postalCode"],
						"addressLocality" => $fieldValue["address"]["addressLocality"],
						"streetAddress" => ((@$fieldValue["address"]["streetAddress"])?trim(@$fieldValue["address"]["streetAddress"]):""),
						"localityId" => $fieldValue["address"]["localityId"],
						"level1" => $fieldValue["address"]["level1"],
						"level1Name" => $fieldValue["address"]["level1Name"],
					);
					
					if(!empty($fieldValue["address"]["level2"])){
						$address["level2"] = $fieldValue["address"]["level2"];
						$address["level2Name"] =((@$fieldValue["address"]["level2Name"])?trim(@$fieldValue["address"]["level2Name"]):"");
					}

					if(!empty($fieldValue["address"]["level3"])){
						$address["level3"] = $fieldValue["address"]["level3"];
						$address["level3Name"] =((@$fieldValue["address"]["level3Name"])?trim(@$fieldValue["address"]["level3Name"]):"");
					}

					if(!empty($fieldValue["address"]["level4"])){
						$address["level4"] = $fieldValue["address"]["level4"];
						$address["level4Name"] =((@$fieldValue["address"]["level4Name"])?trim(@$fieldValue["address"]["level4Name"]):"");
					}

					if(!empty($fieldValue["address"]["level5"])){
						$address["level5"] = $fieldValue["address"]["level5"];
						$address["level5Name"] =((@$fieldValue["address"]["level5Name"])?trim(@$fieldValue["address"]["level5Name"]):"");
					}

					//Check address is well formated
					$valid = DataValidator::addressValid($address);
					if ( $valid != "") throw new CTKException($valid);

					SIG::updateEntityGeoposition($collection, $id, $fieldValue["geo"]["latitude"], $fieldValue["geo"]["longitude"]);
					
					if($collection == Person::COLLECTION && Yii::app()->session['userId'] == $id){
						$user = Yii::app()->session["user"];
						$user["codeInsee"] = $address["codeInsee"];
						$user["postalCode"] = $address["postalCode"];
						$user["addressCountry"] = $address["addressCountry"];
						//$user["address"] = $address;
						Yii::app()->session["user"] = $user;
						Person::updateCookieCommunexion($id, $address);
					}
					$firstCitizen = Person::isFirstCitizen($fieldValue["address"]["codeInsee"]) ;

					if(!empty($fieldValue["address"]["postalCode"]))
						City::checkAndAddPostalCode ($fieldValue["address"]["localityId"], $fieldValue["address"]["postalCode"]);

				}else{
					$verb = '$unset' ;
					SIG::updateEntityGeoposition($collection, $id, null, null);
					if($collection == Person::COLLECTION && Yii::app()->session['userId'] == $id){
						$user = Yii::app()->session["user"];
						unset($user["codeInsee"]);
						unset($user["postalCode"]);
						unset($user["addressCountry"]);
						//unset($user["address"]);
						Yii::app()->session["user"] = $user;
						//Person::updateCookieCommunexion($id, null);
					}
					$address = null ;
				}
				$set = array("address" => $address);
				
			}catch (Exception $e) {  
				throw new CTKException("Error updating  : ".$e->getMessage());		
			}
		}else if ($fieldName == "addresses") {
			//address
			try{
				if(isset($fieldValue["addressesIndex"])){
					$elt = self::getElementById($id, $collection, null, array("addresses"));
					if(!empty($fieldValue["address"])){
						$verb = '$set';
						$address = array(
					        "@type" => "PostalAddress",
					        "codeInsee" => $fieldValue["address"]["codeInsee"],
					        "addressCountry" => $fieldValue["address"]["addressCountry"],
					        "postalCode" => $fieldValue["address"]["postalCode"],
					        "addressLocality" => $fieldValue["address"]["addressLocality"],
					        "streetAddress" => ((@$fieldValue["address"]["streetAddress"])?trim(@$fieldValue["address"]["streetAddress"]):""),
					        "localityId" => $fieldValue["address"]["localityId"],
							"level1" => $fieldValue["address"]["level1"],
							"level1Name" => $fieldValue["address"]["level1Name"],
						);

						if(!empty($fieldValue["address"]["level2"])){
							$address["level2"] = $fieldValue["address"]["level2"];
							$address["level2Name"] =((@$fieldValue["address"]["level2Name"])?trim(@$fieldValue["address"]["level2Name"]):"");
						}

						if(!empty($fieldValue["address"]["level3"])){
							$address["level3"] = $fieldValue["address"]["level3"];
							$address["level3Name"] =((@$fieldValue["address"]["level3Name"])?trim(@$fieldValue["address"]["level3Name"]):"");
						}

						if(!empty($fieldValue["address"]["level4"])){
							$address["level4"] = $fieldValue["address"]["level4"];
							$address["level4Name"] =((@$fieldValue["address"]["level4Name"])?trim(@$fieldValue["address"]["level4Name"]):"");
						}

						if(!empty($fieldValue["address"]["level5"])){
							$address["level5"] = $fieldValue["address"]["level5"];
							$address["level5Name"] =((@$fieldValue["address"]["level5Name"])?trim(@$fieldValue["address"]["level5Name"]):"");
						}
						//Check address is well formated

						$valid = DataValidator::addressValid($address);
						if ( $valid != "") throw new CTKException($valid);

						
						if(empty($elt["addresses"]) || $fieldValue["addressesIndex"] >= count($elt["addresses"]) ){
							$geo = array("@type"=>"GeoCoordinates", "latitude" => $fieldValue["geo"]["latitude"], "longitude" => $fieldValue["geo"]["longitude"]);
							$geoPosition = array("type"=>"Point", "coordinates" => array(floatval($fieldValue["geo"]["longitude"]), floatval($fieldValue["geo"]["latitude"])));
							$locality = array(	"address" => $address,
												"geo" => $geo,
												"geoPosition" => $geoPosition);
							$addToSet = array("addresses" => $locality);
							$verbActivity = ActStr::VERB_ADD ;
						}
						else{
							SIG::updateEntityGeoposition($collection, $id, $fieldValue["geo"]["latitude"], $fieldValue["geo"]["longitude"], $fieldValue["addressesIndex"]);
							$headSet = "addresses.".$fieldValue["addressesIndex"].".address" ;
						}

					}else{
						$verb = '$unset' ;
						$verbActivity = ActStr::VERB_DELETE ;
						//SIG::updateEntityGeoposition($collection, $id, null, null, $fieldValue["addressesIndex"]);
						$address = null ;
						if(count($elt["addresses"]) == 1){
							$headSet = "addresses";
						}else{
							$headSet = "addresses.".$fieldValue["addressesIndex"] ;
							$updatePull = true ;
							$pull="addresses";
						}
					}

					if(!empty($headSet))
						$set = array($headSet => $address);

				}else{
					throw new CTKException("Error updating  : addressesIndex ");	
				}
			}catch (Exception $e) {  
				throw new CTKException("Error updating  : ".$e->getMessage());		
			}
		}else if ($fieldName == "geo" || $fieldName == "geoPosition") {
			try{
				if(!empty($fieldValue["addressesIndex"])){
					$headSet = "addresses.".$fieldValue["addressesIndex"].".".$fieldName ;
					unset($fieldValue["addressesIndex"]);
				}
				else
					$headSet = $fieldName ;

				$verb = (!empty($fieldValue)?'$set':'$unset');
				$geo = (!empty($fieldValue)?$fieldValue:null);

				$valid = (($fieldName == "geo")?DataValidator::geoValid($geo):DataValidator::geoPositionValid($geo));
				if ( $valid != "") throw new CTKException($valid);

				
				$set = array($headSet => $geo);

			}catch (Exception $e) {  
				throw new CTKException("Error updating  : ".$e->getMessage());		
			}
		}
		
		/*else if ($dataFieldName == "birthDate") {
			date_default_timezone_set('UTC');
			$dt = DateTime::createFromFormat('Y-m-d H:i', $fieldValue);
			if (empty($dt)) {
				$dt = DateTime::createFromFormat('Y-m-d', $fieldValue);
			}
			$newMongoDate = new MongoDate($dt->getTimestamp());
			$set = array($dataFieldName => $newMongoDate);

		//Date format
		}*/ else if ($dataFieldName == "startDate" || $dataFieldName == "endDate" || $dataFieldName == "birthDate") {
			// var_dump($dataFieldName);
			// var_dump($fieldValue);
			date_default_timezone_set('UTC');
			$dt = DataValidator::getDateTimeFromString($fieldValue, $dataFieldName);

			if ($dataFieldName == "startDate" && @$allDay && $allDay==true)
				$dt=date_time_set($dt, 00, 00);
			if ($dataFieldName == "endDate" && @$allDay && $allDay==true) 
				$dt=date_time_set($dt, 23, 59);
			$newMongoDate = new MongoDate($dt->getTimestamp());
			$set = array($dataFieldName => $newMongoDate);
		} else if ($dataFieldName == "organizer") {
			//get element and remove current organizer
			//var_dump($fieldValue);
			//exit;
			$element = self::getElementById($id, $collection);
			
			// if( !empty($element["organizerId"]) || !empty($element["links"]["organizer"]) ){
			// 	$oldOrganizerId = @$element["organizerId"] ? $element["organizerId"] : $element["links"]["organizer"];
			// 	$oldOrganizerType = @$element["organizerType"] ? $element["organizerType"] : $element["links"]["organizer"][$oldOrganizerId]["type"];
			// 	//remove the old organizer
			// 	$res = Link::removeOrganizer($oldOrganizerId, $oldOrganizerType, $id, Yii::app()->session["userId"]);
			// 	if (! @$res["result"]) throw new CTKException(@$res["msg"]);
			// }
			// if( !empty($element["organizer"]) ){
			// 	foreach($element["organizer"] as $key => $v){
			// 		//remove the old parent
			// 		$res = Link::removeOrganizer($key, $v["type"], $id, $collection, Yii::app()->session["userId"]);
			// 		if (! @$res["result"]) throw new CTKException(@$res["msg"]);
			// 	}
			// }
			//add new organizer
			if(@$fieldValue["organizerId"]){
				$set = array("organizerId" => $fieldValue["organizerId"], 
							 "organizerType" => $fieldValue["organizerType"]);
				if( $fieldValue["organizerId"] != 'dontKnow' && $fieldValue["organizerType"] != 'dontKnow'){
					$res = Link::addOrganizer($fieldValue["organizerId"], $fieldValue["organizerType"], $id, Yii::app()->session["userId"]);
					if (!empty($res) && ! @$res["result"]) throw new CTKException(@$res["msg"]);
				}
			}
			else if(!empty($fieldValue)){
				$set = array("organizer" => $fieldValue);
				foreach($fieldValue as $key => $v)	 
					$res = Link::addOrganizer($key, $v["type"], $id, Yii::app()->session["userId"]);
			}else{
				$verb == '$unset';
				$set = array(
						"organizer" => "",
						"organizerId" => "",
						"organizerType" => ""
				);
			}
		}else if ($dataFieldName == "parent") {
			//get element and remove current parent
			$element = Element::getElementById($id, $collection);

			if( !empty($element["parentId"]) || !empty($element["links"]["parent"]) ){
				$oldParentId = @$element["parentId"] ? $element["parentId"] : $element["links"]["parent"];
				$oldParentType = @$element["parentType"] ? $element["parentType"] : $element["links"]["parent"][$oldParentId]["type"];
				//remove the old parent
				$res = Link::removeParent($oldParentId, $oldParentType, $id, $collection, Yii::app()->session["userId"]);
				if (! @$res["result"]) throw new CTKException(@$res["msg"]);
			}
			if( !empty($element["parent"]) ){
				foreach($element["parent"] as $key => $v){
					//remove the old parent
					//var_dump($v); exit;
					$res = Link::removeParent($key, $v["type"], $id, $collection, Yii::app()->session["userId"]);
					if (! @$res["result"]) throw new CTKException(@$res["msg"]);
				}
			}
			//add new parent

			if(@$fieldValue["parentId"] && $fieldValue["parentId"] != 'dontKnow' && $fieldValue["parentType"] != 'dontKnow'){
				$set = array("parentId" => $fieldValue["parentId"],
							 "parentType" => $fieldValue["parentType"]);
				$res = Link::addParent($fieldValue["parentId"], $fieldValue["parentType"], $id, $collection, Yii::app()->session["userId"]);
			}else if(!empty($fieldValue)){
				$set = array("parent" => $fieldValue);
				foreach($fieldValue as $key => $v){
					//var_dump($key); var_dump($v); exit;
					$res = Link::addParent($key, $v["type"], $id, $collection, Yii::app()->session["userId"]);
				}			 
					
			}else{
				$verb == '$unset';
				$set = array(
						"parent" => "",
						"parentId" => "",
						"parentType" => ""
				);
			}

			if (!empty($res) && ! @$res["result"]) throw new CTKException(@$res["msg"]);
		}else if ($dataFieldName == "badges") {

			//get element and remove current badges
			$element = Element::getElementById($id, $collection);
			$issuedOn = array();
			if( !empty($element["badges"]) ){
				foreach($element["badges"] as $key => $v){
					if(array_key_exists("issuedOn", $v)){
						$issuedOn[$key] = $v["issuedOn"];
					}
					$res = Link::removeBadges($id, $collection, $key, Yii::app()->session["userId"]);
					if (! @$res["result"]) throw new CTKException(@$res["msg"]);
				}
			}
			// add new badges
			if(!empty($fieldValue)){
				foreach($fieldValue as $key => $v){
					$res = Link::addBadge($id, $collection,$key , $v["name"], array_key_exists($key, $issuedOn) ? $issuedOn[$key] : new MongoDate(time()), Yii::app()->session["userId"]);
					$fieldValue[$key]["issuedOn"] = array_key_exists($key, $issuedOn) ? $issuedOn[$key] : new MongoDate(time());
				}
				$set = array("badges" => $fieldValue);
			}else{
				$verb == '$unset';
				$set = array(
						"badges" => ""
				);
			}

			if (!empty($res) && ! @$res["result"]) throw new CTKException(@$res["msg"]);
		} else if ($dataFieldName == "seePreferences") {
			//var_dump($fieldValue);
			if($fieldValue == "false"){
				$verb = '$unset' ;
				$set = array($dataFieldName => "");
			}else{
				$set = array($dataFieldName => $fieldValue);
			}
		} else if ($dataFieldName == "contacts") {
			if(!isset($fieldValue["index"]))
				$addToSet = array("contacts" => $fieldValue);
			else{
				$headSet = "contacts.".$fieldValue["index"] ;
				unset($fieldValue["index"]);
				if(count($fieldValue) == 0){
					$verb = '$unset' ;
					$verbActivity = ActStr::VERB_DELETE ;
					$fieldValue = null ;
					$updatePull = true ;
					$pull="contacts";
				}
				$set = array($headSet => $fieldValue);
			}
		} else if ($dataFieldName == "urls") {
			if(!isset($fieldValue["index"]))
				$addToSet = array("urls" => $fieldValue);
			else{
				$headSet = "urls.".$fieldValue["index"] ;
				unset($fieldValue["index"]);
				if(count($fieldValue) == 0){
					$verb = '$unset' ;
					$verbActivity = ActStr::VERB_DELETE ;
					$fieldValue = null ;
					$updatePull = true ;
					$pull="urls";
				}
				$set = array($headSet => $fieldValue);

			}
		}else if( $dataFieldName ==  "email" ) {
			if(!empty($fieldValue)){
				$mail = Mail::authorizationMail($fieldValue);
				if($mail == false){
					$fieldValue = "";
					throw new CTKException(Yii::t("common","You cannot enter this email address : ").$fieldValue);
				}
				$set = array($dataFieldName => $fieldValue);
			}else{
				if($collection!=Person::COLLECTION){
					$set = array($dataFieldName => $fieldValue);
					$verb='$unset';
				}
				else
					throw new CTKException(Yii::t("common","You cannot empty your email because your log account is linked to it"));
			}
		} else
			$set = array($dataFieldName => $fieldValue);

		if ($verb == '$set') {
			$set["modified"] = new MongoDate(time());
			$set["updated"] = time();
		} else {
			$setModified = array();
			$setModified["modified"] = new MongoDate(time());
			$setModified["updated"] = time();
		}


		
		//Manage dateEnd field for survey
		if ($collection == Survey::COLLECTION) {
			$canUpdate = Survey::canUpdateSurvey($id, $dataFieldName, $fieldValue);
			if ($canUpdate["result"]) {
				if ($dataFieldName == "dateEnd") {
					$set = array($dataFieldName => strtotime($fieldValue));
				}
			} else {
				throw new CTKException($canUpdate["msg"]);
			}
		}
		//update 
		if(!empty($addToSet)){
			$resAddToSet = PHDB::update( $collection, array("_id" => new MongoId($id)), 
	                          array('$addToSet' => $addToSet));
		}

		if ($verb == '$set') {
			$resUpdate = PHDB::update( $collection, array("_id" => new MongoId($id)), 
			                          array($verb => $set));
		} else {
			$resUpdate = PHDB::update( $collection, array("_id" => new MongoId($id)), 
			                          array($verb => $set, '$set' => $setModified));
		}

		$el = PHDB::findOneById($collection,$id,array('slug'));
		if(!empty($el) && isset($el["slug"]))
        	Slug::updateElemTime( $el['slug'], time() );

		$res = array("result"=>false,"msg"=>"");
		if($resUpdate["ok"]==1){

			if(!empty($updatePull) && $updatePull == true){

				$resPull = PHDB::update( $collection, array("_id" => new MongoId($id)), 
		                          array('$pull' => array($pull => null)));
			}

			$fieldNames = array("badges", "geo", "geoPosition");
			if( $collection != Person::COLLECTION && !in_array($dataFieldName, $fieldNames)){
				// Add in activity to show each modification added to this entity
				if(empty($verbActivity))
					$verbActivity = ActStr::VERB_UPDATE ;
				ActivityStream::saveActivityHistory($verbActivity, $id, $collection, $dataFieldName, $fieldValue);
			}
			$msg=Yii::t(self::getControlerByCollection($collection),"The ".self::getControlerByCollection($collection)." has been updated");
			if ($fieldName == "language"){
				Yii::app()->language=$fieldValue;
				$msg=Yii::t("common","Changing language processing"); 
			}
			$res = array("result"=>true,"msg"=>$msg, "fieldName" => $fieldName, "value" => $fieldValue);
			if(isset($firstCitizen))
				$res["firstCitizen"] = $firstCitizen ;
		}else{
			throw new CTKException("Can not update the element!");
		}
		return $res;
	}

	public static function getImgProfil($person, $imgName, $assetUrl){
    	$url = "";
    	$testUrl = "";
    	if (isset($person) && !empty($person)) {
	        if(!empty($person[$imgName])){
				if(isset(Yii::app()->params["assetsUrl"]["imgUrl"])){
					$url = Yii::app()->params["assetsUrl"]["imgUrl"].$person[$imgName];
				} else {
					$url = Yii::app()->getRequest()->getBaseUrl(true).$person[$imgName];
				}
	          
	          $end = strpos($person[$imgName], "?");
	          if($end<0) $end = strlen($person[$imgName]);
	          $testUrl = substr($person[$imgName], 1, $end-1);
	        }
	        else{
	          $url = $assetUrl.'/images/thumbnail-default.jpg';
	          $testUrl = substr($url, 1);
	        }
	    }
	    return $url;
	    //echo $testUrl;
	    //error_log($testUrl);
	    //if(file_exists($testUrl)) return $url;
	    //else return $assetUrl.'/images/thumbnail-default.jpg';
    }

    public static function getAllLinksOld($links,$type, $id){
	    $contextMap = array();
		/*$contextMap["people"] = array();
		$contextMap["guests"] = array();
		$contextMap["attendees"] = array();
		$contextMap["organizations"] = array();
		$contextMap["projects"] = array();
		$contextMap["events"] = array();
		$contextMap["followers"] = array();*/


	    if($type == Organization::COLLECTION){
	    	$connectAs="members";
	    	$elt = Organization::getSimpleOrganizationById($id);
			//$newOrga["type"]=Organization::COLLECTION;
			$contextMap[$elt["id"]] = $elt;
	    }
	    else if($type == Project::COLLECTION){
	    	$connectAs="contributors";
	    	$elt = Project::getSimpleProjectById($id);
	    	$contextMap[$elt["id"]] = $elt;
	    }
		else if ($type == Event::COLLECTION){
			$connectAs="attendees";
			$elt = Event::getSimpleEventById($id);
			$contextMap[$elt["id"]] = $elt;
		}
		else if ($type == Person::COLLECTION){
			$connectAs="follows";
			$elt = Person::getSimpleUserById($id);
			$contextMap[$elt["id"]] = $elt;
		}

		
	    
		if(!empty($links) && 
			( (Preference::showPreference($elt, $type, "directory", Yii::app()->session["userId"]) && 
			  $type == Person::COLLECTION ) || 
			  $type != Person::COLLECTION) 
		  ) {
			if(isset($links[$connectAs])){
				foreach ($links[$connectAs] as $key => $aMember) {
					//if($type==Event::COLLECTION){
						$citoyen = Person::getSimpleUserById($key);
						if(!empty($citoyen)){
							if(@$aMember["invitorId"])  {
								$contextMap[$citoyen["id"]] = $citoyen;
							}
							else{
								if(@$aMember["isAdmin"]){
									if(@$aMember["isAdminPending"])
										$citoyen["isAdminPending"]=true;
									$citoyen["isAdmin"]=true;         
								}
								$contextMap[$citoyen["id"]] = $citoyen;
							}
						}else{
							if($aMember["type"]==Organization::COLLECTION){
								$newOrga = Organization::getSimpleOrganizationById($key);
								if(!empty($newOrga)){
									if ($aMember["type"] == Organization::COLLECTION && @$aMember["isAdmin"]){
										$newOrga["isAdmin"]=true;  				
									}
									$newOrga["type"]=Organization::COLLECTION;
									if (!@$newOrga["disabled"]) {
										$contextMap[$newOrga["id"]] = $newOrga;
									}
								}
							} 
							else if($aMember["type"]==Person::COLLECTION){
								$newCitoyen = Person::getSimpleUserById($key);
								if (!empty($newCitoyen)) {
									if (@$aMember["type"] == Person::COLLECTION) {
										if(@$aMember["isAdmin"]){
											if(@$aMember["isAdminPending"])
												$newCitoyen["isAdminPending"]=true;  
												$newCitoyen["isAdmin"]=true;  	
										}			
										if(@$aMember["toBeValidated"]){
											$newCitoyen["toBeValidated"]=true;  
										}
									}
									$newCitoyen["type"]=Person::COLLECTION;
									$contextMap[$newCitoyen["id"]] = $newCitoyen;
								}
							}
						}
					//}
				}
			}
			// Link with events
			if(isset($links["events"])){
				foreach ($links["events"] as $keyEv => $valueEv) {
					$event = Event::getSimpleEventById($keyEv);
					//if(!empty($event))
					if(!empty($event) && !empty($event["endDate"]) && strtotime($event["endDate"]) > strtotime("now") )
						$contextMap[$event["id"]] = $event;
				}
			}
	
			// Link with projects
			if(isset($links["projects"])){
				foreach ($links["projects"] as $keyProj => $valueProj) {
					$project = Project::getSimpleProjectById($keyProj);
					if(!empty($project))
						$contextMap[$project["id"]] = $project;
				}
			}

			if(isset($links["followers"])){
				foreach ($links["followers"] as $key => $value) {
					$newCitoyen = Person::getSimpleUserById($key);
					if(!empty($newCitoyen))
						$contextMap[$newCitoyen["id"]] = $newCitoyen;
				}
			}

			if(isset($links["memberOf"])){
				foreach ($links["memberOf"] as $key => $value) {
					$newOrga = Organization::getSimpleOrganizationById($key);
					if(!empty($newOrga))
						$contextMap[$newOrga["id"]] = $newOrga;
				}
			}

			if(isset($links["subEvents"])){
				foreach ($links["subEvents"] as $keyEv => $valueEv) {
					$event = Event::getSimpleEventById($keyEv);
					if(!empty($event) && !empty($event["endDate"]) && strtotime($event["endDate"]) > strtotime("now") )
						$contextMap[$event["id"]] = $event;
				}
			}

			/*$follows = array("citoyens"=>array(),
  					"projects"=>array(),
  					"organizations"=>array(),
  					"count" => 0
  			);*/
  			if ($type == Person::COLLECTION){
			    if (@$links["follows"]) {
			        foreach ( @$links["follows"] as $key => $member ) {
			          	if( $member['type'] == Person::COLLECTION ) {
				            $citoyen = Person::getSimpleUserById( $key );
				  	        if(!empty($citoyen)) {
				  	        	$contextMap[$citoyen["id"]] = $citoyen;
				            }
			        	}

						if( $member['type'] == Organization::COLLECTION ) {
							$organization = Organization::getSimpleOrganizationById($key);
							if(!empty($organization)) {
								$contextMap[$organization["id"]] = $organization;
							}
						}

						if( $member['type'] == Project::COLLECTION ) {
						    $project = Project::getSimpleProjectById($key);
						    if(!empty($project)) {
								$contextMap[$project["id"]] = $project;
							}
						}
		        	}
				}
			}
		}
		return $contextMap;	
    }


    public static function getRolesList($links,$type, $id){
		$rolesLists = [ 
			Yii::t("category","Financier"),
			Yii::t("category","Partner"),
			Yii::t("category","Organizor"),
			Yii::t("category","President"),
			Yii::t("category","Sponsor") ,
			Yii::t("category","Director"),
			Yii::t("category","Speaker"),
			Yii::t("category","Intervener")
		];
		if($type == Organization::COLLECTION ){
			if (isset($links["members"])) {  
				foreach ($links["members"] as $e => $v) {
					if (isset($v["roles"])) {
						foreach ($v["roles"] as $role) {
							if (!empty($role)) {
								if (!in_array( $role, $rolesLists)) {
									$rolesLists[] = $role;
								}
							}
						}
					}
				}
			}
		}else if($type == Project::COLLECTION ){
			if (isset($links["contributors"])) {
				foreach ($links["contributors"] as $e => $v) {
					if (isset($v["roles"])) {
						foreach ($v["roles"] as $role) {
							if (!empty($role)) {
								if (!in_array( $role, $rolesLists)) {
									$rolesLists[] = $role;
								} 
							}
						}
					}
				}
			}
		}
		return $rolesLists;		
	}
    public static function getAllLinks($links,$type, $id){
	    $contextMap = array();

	    if($type == Organization::COLLECTION){
	    	$connectAs="members";
	    	$elt = Organization::getSimpleOrganizationById($id);
			//$newOrga["type"]=Organization::COLLECTION;
			$contextMap[$elt["id"]] = $elt;
	    }
	    else if($type == Project::COLLECTION){
	    	$connectAs="contributors";
	    	$elt = Project::getSimpleProjectById($id);
	    	$contextMap[$elt["id"]] = $elt;
	    }
		else if ($type == Event::COLLECTION){
			$connectAs="attendees";
			$elt = Event::getSimpleEventById($id);
			$contextMap[$elt["id"]] = $elt;
		}
		else if ($type == Person::COLLECTION){
			$connectAs="follows";
			$elt = Person::getSimpleUserById($id);
			$contextMap[$elt["id"]] = $elt;
		}		
	    
		if(!empty($links) && 
			( (Preference::showPreference($elt, $type, "directory", Yii::app()->session["userId"]) && 
			  $type == Person::COLLECTION ) || 
			  $type != Person::COLLECTION) 
		  ) {
			if(isset($links[$connectAs])){
				foreach ($links[$connectAs] as $key => $aMember) {
					$citoyen = Person::getSimpleUserById($key);
					if(!empty($citoyen)){
						if(@$aMember["invitorId"])  {
							$contextMap[$citoyen["id"]] = $citoyen;
						}
						else{
							if(@$aMember["isAdmin"]){
								if(@$aMember["isAdminPending"])
									$citoyen["isAdminPending"]=true;
								$citoyen["isAdmin"]=true;         
							}
							$contextMap[$citoyen["id"]] = $citoyen;
						}
					}else{
						if(isset($aMember["type"]) && $aMember["type"]==Organization::COLLECTION){
							$valLink[Organization::COLLECTION][] = new MongoId($key) ;
						} 
						else if(isset($aMember["type"]) && $aMember["type"]==Person::COLLECTION){
							$valLink[Person::COLLECTION][] = new MongoId($key) ;
						}
					}
				}
			}

			$valLink = array();
			// Link with events
			if(isset($links["events"])){
				foreach ($links["events"] as $keyEv => $valueEv) {
					$valLink[Event::COLLECTION][] = new MongoId($keyEv) ;
				}
			}

			if(isset($links["subEvents"])){
				foreach ($links["subEvents"] as $keyEv => $valueEv) {
					$valLink[Event::COLLECTION][] = new MongoId($keyEv) ;
				}
			}

			// Link with projects
			if(isset($links["projects"])){
				foreach ($links["projects"] as $keyProj => $valueProj) {
					$valLink[Project::COLLECTION][] = new MongoId($keyProj) ;
				}
			}
	
			if(isset($links["followers"])){
				foreach ($links["followers"] as $key => $value) {
					$valLink[Person::COLLECTION][] = new MongoId($key) ;
				}
			}

			if(isset($links["memberOf"])){
				foreach ($links["memberOf"] as $key => $value) {
					$valLink[Organization::COLLECTION][] = new MongoId($key) ;
				}
			}

  			if ($type == Person::COLLECTION){
			    if (@$links["follows"]) {
			        foreach ( @$links["follows"] as $key => $member ) {
			          	if( $member['type'] == Person::COLLECTION )
				            $valLink[Person::COLLECTION][] = new MongoId($key) ;

						if( $member['type'] == Organization::COLLECTION )
							$valLink[Organization::COLLECTION][] = new MongoId($key) ;

						if( $member['type'] == Project::COLLECTION )
						    $valLink[Project::COLLECTION][] = new MongoId($key) ;
		        	}
				}
			}
			
			$fieldsPer =array("id", "name", "username", "email", "roles", "tags", "profilImageUrl", "profilThumbImageUrl", "profilMarkerImageUrl");

			$fieldsOrg = array("id" , "name" , "type" , "email" , "url" , "shortDescription" , "description" , "address" , "pending" , "tags" , "geo" , "updated" , "profilImageUrl" , "profilThumbImageUrl" , "profilMarkerImageUrl" ,"profilMediumImageUrl" , "addresses", "telephone", "slug");

			$fieldsPro = array("id", "name", "shortDescription", "description", "address", "geo", "tags", "profilImageUrl", "profilThumbImageUrl", "profilMarkerImageUrl", "profilMediumImageUrl", "addresses");

			$fieldEve = array("id", "name", "type",  "shortDescription", "description", "address", "geo", "tags", "profilImageUrl", "profilThumbImageUrl", "profilMarkerImageUrl", "profilMediumImageUrl", "startDate", "endDate", "addresses", "allDay");

			if( !empty($valLink) ) {
				foreach ($valLink as $type => $valLink) {
					$contactsComplet = null;
					if($type == Person::COLLECTION)
						$contactsComplet = Person::getByArrayId($valLink, $fieldsPer, true, true); 
					if($type == Organization::COLLECTION)
						$contactsComplet = Organization::getByArrayId($valLink, $fieldsOrg, true);
					if($type == Project::COLLECTION)
						$contactsComplet = Project::getByArrayId($valLink, $fieldsPro, true);
					if($type == Event::COLLECTION)
						$contactsComplet = Event::getByArrayId($valLink, $fieldEve, true);

					if(!empty($contactsComplet))
						$contextMap = array_merge($contextMap, $contactsComplet);					
				}
			}

			if(isset($links[$connectAs])){
				foreach ($links[$connectAs] as $key => $aMember) {
					if(!empty($contextMap[$key])){
						if(isset($aMember["type"]) && $aMember["type"] == Organization::COLLECTION && @$aMember["isAdmin"])
							$contextMap[$key]["isAdmin"]=true;

						else if (@$aMember["type"] == Person::COLLECTION) {
							if(@$aMember["isAdmin"]){
								if(@$aMember["isAdminPending"])
									$contextMap[$key]["isAdminPending"]=true;  
								$contextMap[$key]["isAdmin"]=true;  	
							}			
							if(@$aMember["toBeValidated"]){
								$contextMap[$key]["toBeValidated"]=true;  
							}
						}
					}
				}
			}

		}
		return $contextMap;	
    }

    public static function getActive($type){
        $list = PHDB::findAndSort( $type ,array("updated"=>array('$exists'=>1)),array("updated"=>1), 4);
        return $list;
     }


    /**
	 * answers to show or not to show a field by it's name
	 * @param String $id : is the mongoId of the action room
	 * @param String $person : is the mongoId of the action room
	 * @return "" or the value to be shown
	 */
	public static function showField($fieldName,$element, $isLinked) {
	  	
	  	$res = null;

	  	$attConfName = $fieldName;
	  	if($fieldName == "address.streetAddress") 	$attConfName = "locality";
	  	if($fieldName == "telephone") 				$attConfName =  "phone";

	  	if( Yii::app()->session['userId'] == (string)$element["_id"]
	  		||  ( isset($element["preferences"]) && isset($element["preferences"]["publicFields"]) && in_array( $attConfName, $element["preferences"]["publicFields"]) )  
	  		|| ( $isLinked && isset($element["preferences"]) && isset($element["preferences"]["privateFields"]) && in_array( $attConfName, $element["preferences"]["privateFields"]))  )
	  	{
	  		$res = ArrayHelper::getValueByDotPath($element,$fieldName);
	  	
	  	}
	  	
	  	return $res;
     
	}
	/**
		* Put last timestamp on element label 
		* label ex: update, lastInvitation
	**/
	public static function updateTimeElementByLabel($elementType,$elementId, $label) {
		PHDB::update($elementType, 
			array("_id" => $elementId) , 
            array('$set' => array( $label =>time()))
		);
		return true;
	}

	public static function getCommunityByParentTypeAndId($parent) {
		$communityLinks = array();

		if(!empty($parent)){
			foreach ($parent as $key => $value) {
				$cl= Element::getCommunityByTypeAndId($value["type"],$key);
				if(!empty($cl))
					$communityLinks = array_merge($communityLinks, $cl);
			}
		}
		return $communityLinks;
	}
	/**
		Get communtiy of an element, complete or linked to specific search
		* @param String $typeCommunity : get specific type of element in a community
		* @param String $attribute defined the kind of community : members / admin / pending
		* @param String $role : get sepecific member with role,
		* @param String $settings : in order to get specific community towards a notifications and emails settings
	**/
	public static function getCommunityByTypeAndId($type, $id, $typeCommunity="all", $attribute=null, $role=null, $settings=null, $fields=null) {
		$res = array();
	  	$element = self::getElementSimpleById($id, $type, null, array("links"));

	  	if (empty($element)) {
	  	    return $res;
	  	   // throw new CTKException(Yii::t("common", "The id : ".$id." of {what} is unkown : please contact us to fix this problem", array("{what}"=>Yii::t("common","this ".self::getControlerByCollection($type)))));
        }
        if($type==Person::COLLECTION){
			$res = Person::getPersonLinksByPersonId($_SESSION["userId"]);
			return isset($res[Person::COLLECTION]) ? $res[Person::COLLECTION]:[];
        }
	  	else if ( @$element && @$element["links"] && @Link::$linksTypes[$type] && @$element["links"][Link::$linksTypes[$type][Person::COLLECTION]] ) 
	  	{
	  		$community = array();
	  		foreach ($element["links"][Link::$linksTypes[$type][Person::COLLECTION]] as $key => $value) {
	  			$add=false;
	  			if(!@$value["isInviting"] || $attribute==Link::ALL_COMMUNITY){
	  			//if(!@$value["toBeValidated"] && (!@$value["isInviting"] || $attribute==Link::ALL_COMMUNITY)){
			        $add = ($typeCommunity=="all" || @$value['type'] == $typeCommunity) ? true : false;
			        if($add && $attribute !== null && $attribute!=Link::ALL_COMMUNITY){  
			        	if($attribute=="isAdmin" && @$value["isAdmin"] && !@$value["isAdminPending"]){
			        		if(gettype($role) == "string")
			        			$add = (empty($role) || (!empty($role) && @$value["roles"] && in_array($role, $value["roles"]))) ? true : false;
			        		elseif (gettype($role) == "array" && @$value["roles"] ) {
			        			$intersect = array_intersect($role, @$value["roles"]);
			        			$add = (count($intersect) > 0) ?  true : false;				        			
			        		}
			        	}
			        	else if($attribute=="onlyMembers" && !@$value["isAdmin"]){
			        		if(gettype($role) == "string")
			        			$add = (empty($role) || (!empty($role) && @$value["roles"] && in_array($role, $value["roles"]))) ? true : false;
			        		elseif (gettype($role) == "array" && @$value["roles"] ) {
			        			$intersect = array_intersect($role, @$value["roles"]);
			        			$add = (count($intersect) > 0) ?  true : false;				        			
			        		}
			        	}
			        	else
			        		$add=false;
			        	//$searchInAttribute=false;
			        }
			        if($add && $role !== null){
			        	if(gettype($role) == "string" && @$value["roles"] && in_array($role, $value["roles"]))
			        		$add=true;
			        	elseif (gettype($role) == "array" && @$value["roles"] ) {
			        			$intersect = array_intersect($role, @$value["roles"]);
			        			$add = (count($intersect) > 0) ?  true : false;		        			
			        	}
			        	else
			        		$add=false;
			        }
			        if($add && $settings !== null){
			        	if(@$value[$settings["type"]]){
			        		if($settings["value"]=="high" && $value[$settings["type"]]=="high")
			        			$add=true;
			        		else if($settings["value"]=="default" && in_array($value[$settings["type"]],["default","high"]))
			        			$add=true;
			        		else if($settings["value"]=="low" && $value[$settings["type"]] != "desactivated")
			        			$add=true;
			        		else
			        			$add=false;

			        	}
			        	else if(in_array($settings["value"], ["low", "default"]))
			        		$add=true;
			        	else
			        		$add=false;
			        }
			    }
			  	if($add){
		    		if(@$settings && $settings["type"]=="mails")
	        			$res[$key]= Element::getElementSimpleById($key, Person::COLLECTION, null, array("email", "username", "language")); 
	        		else{
	        			if(!empty($fields)) 
	        				$value=array_merge($value,Element::getElementSimpleById($key, $value["type"], null, $fields));
	        			$res[$key] = $value;
	        		}
	        	}
	  		}
		  }
		 
	  	return $res;
	}

	/**
	 * Demande la suppression d'un élément
	 * - Si creator demande la suppression et organisation vide (pas de links, pas de members) => suppression de l’orga
	 * - Si superadmin => suppression direct
	 * - Si edition libre sans admin 
	 * 		- Mail + notification envoyée aux super admins + creator
	 * - Si admins > 0 pour l’orga :
	 * 		- envoi d’un mail + notification aux admins
	 * 		- L’orga est en attente de validation de suppression pendant X jours. Un des admins peut venir et bloquer la suppression pendant ce laps de temps. 
	 * 		- Après X jours, un batch passe et supprime l’organisation
	 * 		- Notifications des admins après suppression
	 * @param String $elementType : element type
	 * @param String $elementId : element Id
	 * @param String $reason : reason why the element can be deleted
	 * @param String $userId : the userId asking to delete the element
	 * @return array : result : boolean, msg : String
	 */
	public static function askToDelete($elementType, $elementId, $reason, $userId,$elemTypes) {
		if (! Authorisation::canDeleteElement($elementId, $elementType, $userId)) {
			return array("result" => false, "msg" => "The user cannot delete this element !");
		}

		$res = array("result" => false, "msg" => "Something bad happend : impossible to delete this element");

		//What type of element i can delete
		$managedTypes = array(Organization::COLLECTION, 
							Project::COLLECTION, 
							Event::COLLECTION, 
							Classified::COLLECTION,
							Proposal::COLLECTION, 
							Action::COLLECTION, 
							Template::COLLECTION,
							Cms::COLLECTION, 
							Room::COLLECTION);
		
		if (!in_array($elementType, $managedTypes)) 
			return array( "result" => false, "msg" => "Impossible to delete this type of element" );

		$modelElement = self::getModelByType($elementType);

		$canBeDeleted = false;
		$element = self::getByTypeAndId($elementType, $elementId);
		
		if (@$element["status"]	== self::STATUS_DELETE_PEDING) 
			return array("result" => false, "msg" => "The element is already in delete pending status !");

		//retrieve admins of the element
		$admins = array();

		if (isset($element["links"])) {
			foreach (@$element["links"] as $type => $links) {
				if (is_array($links)) {
					foreach ($links as $id => $aLink) {
						if (@$aLink["type"] == Person::COLLECTION && @$aLink["isAdmin"] == true) {
							array_push($admins, $id);
						}
					}
				}
			}
		}
		
		$creator = empty($element["creator"]) ? "" : $element["creator"];

		//Check if the creator is the user asking to delete the element
		if ($creator == $userId) {
			// If almost empty element (no links expect creator as member) => delete the element
			if (count(@$element["links"]) == 0) {
				$canBeDeleted = true;
			} else if (count(@$element["links"]["members"]) == 1) {
				$canBeDeleted = isset($element["links"]["members"][$creator]);
			} else if(count($admins) == 1) {
				$canBeDeleted = in_array($creator, $admins);
			}
		//If open data without admin => the super admin will statut
		} else if ((@$element["preferences"]["isOpenData"] == true || 
					@$element["preferences"]["isOpenData"] == 'true' ) && 
					count($admins) == 0) {
			$canBeDeleted = false; 
		}

		$DDATypes = array(	Proposal::COLLECTION, 
							Action::COLLECTION, 
							Resolution::COLLECTION,
							Room::COLLECTION);

		if (in_array($elementType, $DDATypes)) 
			$canBeDeleted = true;

		// If the userId is superAdmin : element can be deleted as well
		if (Authorisation::isUserSuperAdmin($userId)) {
			$canBeDeleted = true;
		}else if(Authorisation::isCostumAdmin($userId)){
			$costum = CacheHelper::getCostum();
			if(isset($element["source"]) && isset($element["source"]["key"]) && $element["source"]["key"]==$costum["slug"])
				$canBeDeleted=true;
		}

		//var_dump($element["links"]); exit;

		//Try to delete the element
		if ($canBeDeleted) {
			$res = self::deleteElement($elementType, $elementId, $reason, $userId);
		} else {
			//If open data without admin
			if ((@$element["preferences"]["isOpenData"] == true || @$element["preferences"]["isOpenData"] == 'true' ) && count($admins) == 0)  {
				//Ask the super admins to act for the deletion of the element
				$adminsId = array();
				$superAdmins = Person::getCurrentSuperAdmins();
				foreach ($superAdmins as $id => $aPerson) {
					array_push($adminsId, $id);
				}
				error_log("Pour la suppression de l'élément ".$elementType."/".$elementId." : on demande aux super admins");
				$res = self::goElementDeletePending($elementType, $elementId, $reason, $adminsId, $userId,true);
			}

			//If at least one admin => ask if one of the admins want to stop the deletion. The element is mark as pending deletion. After X days, if no one block the deletion => the element if deleted
			if (count($admins) > 0) {
				error_log("Pour la suppression de l'élément ".$elementType."/".$elementId." : on demande aux admins de l'élément");
				$res = self::goElementDeletePending($elementType, $elementId, $reason, $admins, $userId, false);
			}
		}

		return $res;
	}

	public static function getInfoBeforeDelete($elementType, $elementId){
		$idCreator = $elementId;
		$res = array(
			"toDelete" => array(),
		);

		$element = self::getByTypeAndId($elementType, $elementId);
		if(!empty($element)){
			$res["element"] = $element;
			if(isset($res["element"]["dateSpamDetector"]) && isset($res["element"]["dateSpamDetector"]->sec))
				$res["element"]["dateSpamDetector"]->sec = date("d-m-Y", $res["element"]["dateSpamDetector"]->sec);
		}
			

		if($elementType != Citoyen::COLLECTION){
			$creator = self::getCreatorElement($elementType, $elementId);
			foreach($creator as $idC => $creatorElement){
				$idCreator = $idC;
			}
		}

		$profilImages = Document::listMyDocumentByIdAndType($elementId, $elementType, Document::IMG_PROFIL, Document::DOC_TYPE_IMAGE, array( 'created' => -1 ));
    	if(count($profilImages) > 0){
			$res["toDelete"]["document"] = array();
			$res["toDelete"]["document"]["count"]  = count($profilImages);
			foreach ($profilImages as $docId => $document) {
				if(isset($document["name"]) && isset($document["path"]))
					$res["toDelete"]["document"][$docId] = array("name" => $document["name"]);
				else
					$res["toDelete"]["document"][$docId] = array();
			}
		}

		$whereActivityStream = [
			"target.id"=>$elementId, 
			"target.type"=>$elementType, 
		];
		
		$activity = ActivityStream::getWhere($whereActivityStream);
		if(count($activity) > 0){
			$res["toDelete"]["activityStream"] = array();
			$res["toDelete"]["activityStream"]["count"]  = count($activity);
			foreach ($activity as $actId => $act) {
				$res["toDelete"]["activityStream"][$actId] = array();
			}
		}

		$whereANew = [
			"target.id"=>$elementId, 
			"object.id"=>$elementType, 
		];
		$new = News::getWhere($whereANew);
		if(count($new) > 0){
			$res["toDelete"]["New"] = array();
			$res["toDelete"]["New"]["count"]  = count($new);
			foreach ($new as $newId => $val) {
				$res["toDelete"]["New"][$newId] = array();
			}
		}

		$room = Room::getElementActionRooms($elementId, $elementType);
		if(count($room) > 0){
			$res["toDelete"]["Room"] = array();
			$res["toDelete"]["Room"]["count"]  = count($room);
			foreach ($room as $roomId => $val) {
				if(isset($val["name"]) && isset($val["path"]))
					$res["toDelete"]["Room"][$roomId] = array("name" => $val["name"]);
				else
					$res["toDelete"]["Room"][$roomId] = array();
			}
		}

		$childTypes = @self::$elementTypeCreateByCitoyen;
		foreach($childTypes as $childType){
			$res["toDelete"][$childType] = array();

			$whereGetChild = array(
				"creator" => $idCreator
			);

			$childs = self::getByWhere($childType, $whereGetChild);
			if(count($childs) > 0){
				$res["toDelete"][$childType] = $childs;
				$res["toDelete"][$childType]["count"] = count($childs);
			}
		}

		return $res;
	}

	/**
	 * Suppression de l'élément et de ses liens. 
	 * - Suppression des liens 
	 * 		- Persons : followers / member / memberOf
	 * 		- Projects :  links.contributor. Vider le parentId+parentType
	 *		- Event : links.events, vider le organizerId et organizerType
	 * 		- Organization : member / memberOf
	 * - Suppresion des Documents 
	 * 		- Supprimer les images de profil 
	 * - Vider le activityStream de type history
	 * - Suppression des News, Actions, Surveys, ActionRooms, Comments
	 * @param type $elementType : type d'élément
	 * @param type $elementId : id of the element
	 * @param type $reason : reason of the deletion
	 * @param type $userId : userId making the deletion
	 * @return array|void
	 */
	public static function deleteElement($elementType, $elementId, $reason, $userId, $elt = null) {
		
		if (! Authorisation::canDeleteElement($elementId, $elementType, $userId, $elt)) {
			return array("result" => false, "msg" => Yii::t('common', "You are not allowed to delete this element !"));
		}

		//array to know likeTypes to their backwards link. Ex : a person "members" type link got a memberOf link in his collection
		$linksTypes = array(
			Person::COLLECTION => 
				array(	"followers" => "follow", 
						"members" => "memberOf",
						"follow" => "followers",
						"attendees" => "events",
						"helpers" => "needs",
						"contributors" => "projects"),
			Organization::COLLECTION => 
				array(	"memberOf" => "members",
						"members" =>"memberOf",
						"follow" => "followers",
						"contributors" => "projects"),
			Event::COLLECTION => 
				array("events" => "organizer"),
			Project::COLLECTION =>
				array("projects" => "projects")
			
			);
		
		$elementToDelete = self::getByTypeAndId( $elementType, $elementId );

		$resError = array(
    		"result" => false, 
    		"action" => array( "start_delete" => array( 
    						"type"=>$elementType, 
    						"id"=>$elementId,
    						"name" => @$elementToDelete["name"]
    					)), 
    		"msg" => Yii::t('common',"Error trying to delete this element : please contact your administrator."),
    	);

		if(Costum::isSameFunction("deleteElement")){
			$paramsCostum = array(
				"elementToDelete" => $elementToDelete,
				"userId" => $userId,
				"elementType" => $elementType,
				"elementId" => $elementId,
				"reason" => $reason,

			);
			$res = Costum::sameFunction("deleteElement", $paramsCostum);
		}

		//Remove Documents => Profil Images
		//TODO SBAR : Remove other images ?
    	$profilImages = Document::listMyDocumentByIdAndType($elementId, $elementType, Document::IMG_PROFIL, Document::DOC_TYPE_IMAGE, array( 'created' => -1 ));
    	//error_log("count docs ".count( $profilImages ) );
    	foreach ($profilImages as $docId => $document) {
    		Document::removeDocumentById($docId, $userId);
    		//error_log("delete document id ".$docId);
    	}
    	$resError["action"]["deleted_Documents"] = count($profilImages);
    	
    	//Remove Activity of the Element
    	$res = ActivityStream::removeElementActivityStream($elementId, $elementType);
    	$resError["action"]["removeElementActivityStream"] = $res;
    	if (!$res) return $resError;
    	//Delete News
    	$res = News::deleteNewsOfElement($elementId, $elementType, $userId, true, true);
    	$resError["action"]["deleteNewsOfElement"] = $res;
    	$resError["res"] = $res;
    	if (!$res["result"]) {
    		error_log("error deleting News ".@$res["id"]." : ".$res["msg"]); 
    		return $resError;
    	}
    	//Delete Action Rooms
    	$res = Room::deleteElementActionRooms($elementId, $elementType, $userId, true);
    	$resError["action"]["deleteElementActionRooms"] = $res; 
    	if (!$res["result"]) return $resError;





		Slug::removeByParentIdAndType($elementId, $elementType);

		$listEventsId = array();
		$listProjectId = array();
		//Remove backwards links
		$resError["action"]["RemoveBackwardsLinks"] = $res;
		if (isset($elementToDelete["links"])) {
			foreach ($elementToDelete["links"] as $linkType => $aLink) {
				foreach ($aLink as $linkElementId => $linkInfo) {

                    if(isset($linkInfo["type"])){
                        $linkElementType = $linkInfo["type"];
                        if (!isset($linksTypes[$linkElementType][$linkType])) {
                            //error_log(print_r(@$linksTypes[$linkElementType]));
                            error_log("Unknown backward links for a link in a ".$elementType." of type ".$linkType." to a ".$linkElementType);
                            continue;
                        }
                        $linkToDelete = $linksTypes[$linkElementType][$linkType];

                        $collection = $linkElementType;
                        if ($collection == Event::COLLECTION) array_push($listEventsId, new MongoId($linkElementId));
                        if ($collection == Project::COLLECTION) array_push($listProjectId, new MongoId($linkElementId));

                        $where = array("_id" => new MongoId($linkElementId));
                        $action = array('$unset' => array('links.'.$linkToDelete.'.'.$elementId => ""));
                        PHDB::update($collection, $where, $action);
                        error_log("Because of deletion of element :".$elementType."/".$elementId." : delete a backward link on a element ".$linkElementId." of type ".$collection." of type ".$linkToDelete);
                    }
                }
			}
		}

		if($elementType == Room::COLLECTION){
			$DDATypes = array(	Proposal::COLLECTION, 
								Resolution::COLLECTION, 
								Action::COLLECTION);

			foreach ($DDATypes as $k => $ddaType) {
				$where = array("idParentRoom" => $elementId);
    			$ddaOfThisRoom = PHDB::find($ddaType, $where);
    			foreach ($ddaOfThisRoom as $kk => $dda) {
    				$whereNews = array("object.type" => $ddaType, "object.id" => (string)$dda["_id"]);
    				//echo "FOUND : ".$ddaType."::COLLECTION > "; var_dump($dda);
    				//echo "REMOVE : News::COLLECTION > "; var_dump($whereNews);
    				PHDB::remove(News::COLLECTION, $whereNews);
    			}
    			//echo "REMOVE : ".$ddaType."::COLLECTION > "; var_dump($where);
    			PHDB::remove($ddaType, $where);
			}

			//exit;
			/*$where = array("idParentRoom" => $elementId);
    		PHDB::remove(Proposal::COLLECTION, $where);
    		PHDB::remove(Action::COLLECTION, $where);
    		PHDB::remove(Resolution::COLLECTION, $where);*/
    		
		}
		
		//Unset the organizer for events organized by the element
		$resError["action"]["Unset_the_organizer_for_events_organized_by_the_element"] = $res;
		if (count($listEventsId) > 0) {
			$where = array('_id' => array('$in' => $listEventsId));
			$action = array('$set' => array("organizerId" => Event::NO_ORGANISER, "organizerType" => Event::NO_ORGANISER));
			PHDB::update(Event::COLLECTION, $where, $action);
		}

		//Unset the project with parent this element
		$resError["action"]["Unset_the_project_with_parent_this_element"] = $res;
		if (count($listProjectId) > 0) {
			$where = array('_id' => array('$in' => $listProjectId));
			$action = array('$unset' => array("parentId" => "", "parentType" => ""));
			PHDB::update(Project::COLLECTION, $where, $action);
		}
    	
		//Delete the element
		$where = array("_id" => new MongoId($elementId));

		if(!empty(Yii::app()->session["userId"])){
			$author = array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]);
		} else {
			$coAdmin = PHDB::findOne(Person::COLLECTION, array("slug"=> "coadmin"));
			$author = array("id" => (String) $coAdmin["_id"],"name"=> $coAdmin["name"]);
		}

    	// NOTIFY COMMUNITY OF DELETED ELEMENT
    	Notification::constructNotification(ActStr::VERB_DELETE, $author, array("type"=>$elementType,"id"=> $elementId), null, ActStr::VERB_DELETE);
		
		$resError["action"]["remove_element"] = $res;
    	PHDB::remove($elementType, $where);
    	
    	$res = array("result" => true, "status" => "deleted", "msg" => Yii::t('common',"The element {elementName} of type {elementType} has been deleted with success.", array("{elementName}" => @$elementToDelete["name"], "{elementType}" => @$elementType )));

		Log::save(array("userId" => $userId, "browser" => @$_SERVER["HTTP_USER_AGENT"], "ipAddress" => @$_SERVER["REMOTE_ADDR"], "created" => new MongoDate(time()), "action" => "deleteElement", "params" => array("id" => $elementId, "type" => $elementType)));
		$resError["action"]["final"] = $res;
		return $res;
	}

	public static function deleteSpam($elementType, $elementId, $reason, $userId, $elt = null){
		if ( !Person::logguedAndValid() ||  (!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) 
            return array( "result" => false, "msg" => Yii::t("common", "Access denied") );

		$allCollectionChild = @self::$elementTypeCreateByCitoyen;
		$res=array("result"=>false, "msg"=> "element not found");
            
		$element = self::getByTypeAndId( $elementType, new MongoId($elementId));
		//if((isset($element["slug"]) && $element["slug"] != "unknown") || (isset($element["collection"]) && $element["collection"] == "poi")){
			
			
			if($elementType == Citoyen::COLLECTION){
				foreach($allCollectionChild as $childType){
					$whereGetChild = array(
						"creator" => $elementId
					);
	
					$childs = self::getByWhere($childType, $whereGetChild);
					if(count($childs) > 0){
						foreach($childs as $childId => $child){
							self::deleteElement($childType, $childId, $reason, $userId, $elt);
						}
					}
				}

				
			}
			else{
				$creator = self::getCreatorElement($elementType, $elementId);
				foreach($creator as $idCreator => $creatorElement){
					self::deleteSpam(Citoyen::COLLECTION, $idCreator, $reason, $userId, $elt);
				}
			}

			
			
			$res = self::deleteElement($elementType, $elementId, $reason, $userId, $elt);
			$res["result"] = true;
			$res["type"] = $elementType;
			$res["reasonDelete"] = @$element["reasonDelete"];
			$res["userAskingToDelete"] = @$element["userAskingToDelete"];	

			$res["allCollectionChild"] = $allCollectionChild;	
			
		//}

		return $res;
	}

	//deletes elements with no strings attached
	//no particpants, no connected eleemnts ...etc
	//like POI, Ressources,Classifieds
	//deletes images by folders and 
	public static function deleteSimple($id,$type, $userId) {
		error_log("deleteSimple ".$id.",".$type);
		if ( !@$userId) 
            return array( "result" => false, "msg" => "You must be loggued to delete something" );
        
        
        $el = self::getByTypeAndId( $type, $id );
        if (! Authorisation::canDeleteElement($id, $type, $userId)) 
			return array("result" => false, "msg" => "The user cannot delete this element !");
		
		$res = array("result" => false, "msg" => "Something bad happend : impossible to delete this element");

		//CLEANING BADGE
		if($type == Badge::COLLECTION){
			Badge::cleanAllElementsLinks($id);
		}

        //Delete the comments
        $resComments = Comment::deleteAllContextComments($id,$type, $userId);
		if (@$resComments["result"]) {
			$resDocs = Document::removeDocumentByFolder($type."/".$id);
			if($type==Crowdfunding::COLLECTION && $el["type"]=="campaign")
				Crowdfunding::removePledgeAndDonation($id);
			PHDB::remove($type, array("_id"=>new MongoId($id)));
			$res = array("result" => true, 
						 "msg" => "The element has been deleted succesfully", 
						 "resDocs" => $resDocs);
		} else {
			return $resComments;
		}
		
		return $res;
	}

	/**
	 * The element is mark as pending deletion with a date.
	 * Send notification/mail to $admins (list of persons) to know if they accept the delete of the element
	 * After X days, if no one block the deletion => the element if deleted (this behavior is done with a batch)
	 * @param String $elementType : The element type
	 * @param String $elementId : the element Id
	 * @param String $reason : the reason why the element will be deleted
	 * @param array $admins : a list of person to sent notifications
	 * @param String $userId : the userId asking the deletion
	 * @return array result => bool, msg => String
	 */
	private static function goElementDeletePending($elementType, $elementId, $reason, $admins, $userId, $isSuperAdmin=false) {
		$res = array("result" => true, "status" => "deletePending", "msg" => Yii::t('common', "The element has been put in status 'delete pending', waiting the admin to confirm the delete."));
		
		//Mark the element as deletePending
		PHDB::update($elementType, 
					array("_id" => new MongoId($elementId)), array('$set' => array("status" => self::STATUS_DELETE_PEDING, "statusDate" => new MongoDate(), "reasonDelete" => $reason, "userAskingToDelete" => $userId)));
		
		//Send emails to admins
		Mail::confirmDeleteElement($elementType, $elementId, $reason, $admins, $userId);
		if($isSuperAdmin){
			/*Notification::actionToAdmin(
	            ActStr::VERB_RETURN, 
	            array("type" => Cron::COLLECTION), 
	            array("id" => $this->id, "type"=>self::COLLECTION, "event" => $this->event),
	            array("id" => $this->personId, "type"=>Person::COLLECTION, "email"=>$this->recipient)
        	);*/
		}else
			Notification::constructNotification(ActStr::VERB_DELETE, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$elementType,"id"=> $elementId), null, ActStr::VERB_ASK);
		
		return $res;
	}

	/**
	 * An admin of the element want to stop the process of delete of the element.
	 * Remove the pending status of the element
	 * @param String $elementType : The element type
	 * @param String $elementId : the element Id
	 * @param String $userId : the userId asking to stop
	 * @return array result => bool, msg => String
	 */
	public static function stopToDelete($elementType, $elementId, $userId) {
		$res = array("result" => true, "msg" => Yii::t('common',"The element is no more in 'delete pending' status"));
		//remove the status deletePending on the element
		PHDB::update($elementType, 
					array("_id" => new MongoId($elementId)), array('$unset' => array("status" => "", "statusDate" => "")));
		Notification::constructNotification(ActStr::VERB_DELETE, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$elementType,"id"=> $elementId), null, ActStr::VERB_REFUSE);

		
		//TODO SBAR => 
		// - send email to notify the admin : the element has been stop by the user 
		// - add activity Stream
		// - Notification
		
		return $res;
	}
    
    public static function isElementStatusDeletePending($elementType, $elementId) {
        $element = Element::getElementById($elementId, $elementType, null,["status"]);
        return @$element["status"] == Element::STATUS_DELETE_PEDING;
    }

	public static function save($params){
		//Rest::json($params); exit;
        $id = null;
        $data = null;
        

        if(empty($params["collection"]))
        	return array("result"=> false, "error"=>"400", "msg" => "Bad Request : Check you parameters");

        $collection = $params["collection"];

        if($collection == "cms"){
        	$userId = isset(Yii::app()->session["userId"]) ? Yii::app()->session["userId"] : "";
        	if( !(Authorisation::isUserSuperAdmin($userId) || Authorisation::isUserSuperAdminCms($userId)) && isset($params["type"]) && $params["type"] == "blockCms" )
        		return array("result"=> false, "error"=>"403", "msg" => "Forbidden : Only allowed for the super administrator");
        }
		// Check value of issuer

		if($collection == Badge::COLLECTION){
			if(isset($params['criteria']) && !is_array($params['criteria'])){
				$params['criteria'] = ['narrative' => $params['criteria']];
			}
			if(!isset($params['issuer']) || (isset($params['issuer']) && array_key_exists(Yii::app()->session["userId"] , $params['issuer']))){
				$user = PHDB::findOneById(Person::COLLECTION, Yii::app()->session["userId"]);
				$params['issuer'] = [
				  (string) $user["_id"] => [
					  "name" => $user["name"],
					  "type" => Person::COLLECTION
				  ]
				];
			}
		}

		// Check if issue already Endorse 


        if( !empty($params["id"]) )
        	$id = $params["id"];

		if($collection == Endorsement::COLLECTION){
			if(!isset($params['issuer']) || (isset($params['issuer']) && array_key_exists(Yii::app()->session["userId"] , $params['issuer']))){
				$user = PHDB::findOneById(Person::COLLECTION, Yii::app()->session["userId"]);
				$params['issuer'] = [
					(string) $user["_id"] => [
						"name" => $user["name"],
						"type" => Person::COLLECTION
					]
				];
			}
			$idIssuer = array_key_first($params['issuer']);
			$where = ['issuer.' . $idIssuer => ['$exists' => true], 'claimElementType' => $params['claimElementType']];
			$where['claimElementId'] = $params['claimElementId'];
			if(isset($params['claimBadge'])){
				$where['claimBadge'] = $params['claimBadge'];
			}else{
				$where['claimBadge'] = ['$exists' => false];
			}
			$one = PHDB::findOne(Endorsement::COLLECTION, $where, ['name']);
			$existe = PHDB::findOneById(Endorsement::COLLECTION, $id, ['name']);
			if(empty($existe) && !empty($one)){
				return array( "result" => false,
					"msg"=>Yii::t("badge","You've already endorse"),
					"reload"=>true,
					"map"=>$params,
					"id"=>$id);
			} 
		}
        $key = self::getModelByType($collection);

		//$paramsImport = (empty($params["paramsImport"])?null:$params["paramsImport"]);
		$paramsLinkImport = ( empty($params["paramsImport"] ) ? null : $params["paramsImport"]);
		//Rest::json($paramsLinkImport); exit;
		unset($params["paramsImport"]);
        
       
        $params = self::prepData( $params );

        // Specific case for edit dynform process
       	$unset=array();
        if(isset($params["unset"])){
        	$unset=$params["unset"];
        	unset($params['unset']);
        }
        unset($params['key']);
        //unset($params['collection']);
        unset($params['id']);

        $postParams = array();
        if( !in_array( $collection, array("poi", "actions", "proposals", "resolutions", "classified", "ressources","crowdfunding") ) && 
        	@$params["urls"] && @$params["medias"] ){
	        	$postParams["medias"] = $params["medias"];
	        	unset($params['medias']);
	        	$postParams["urls"] = $params["urls"];
	        	unset($params['urls']);
        }
        if(!empty($params["links"])){
        	$postParams["links"] = $params["links"];
        	unset($params['links']);
        }
        if(!empty($params["costumSlug"]))
        	unset($params['costumSlug']);
        
        if(!empty($params["roles"])){
        	$postParams["roles"] = $params["roles"];
        	unset($params['roles']);
        }

        if($collection == Room::COLLECTION){
        	if(isset($params["roles"])){
        		$params["roles"] = explode(",", @$params["roles"]);
        	}
        }

        if($collection == City::COLLECTION)
        	$params = City::prepCity($params);
        
        if(isset($params["price"]))
			$params["price"] = DataValidator::getfloat($params["price"]);
			
		if($collection == Action::COLLECTION){
			unset($params['collection']);
		}
        	
		/** Remove "costumEditMode". Useless for element's data**/
		if(isset($params["costumEditMode"]))
			unset( $params["costumEditMode"]);
        
        /*$microformat = PHDB::findOne(PHType::TYPE_MICROFORMATS, array( "key"=> $key));
        $validate = ( !isset($microformat )  || !isset($microformat["jsonSchema"])) ? false : true;
        //validation process based on microformat defeinition of the form
        */
        //validation process based on databind on each Elemnt Mode
        $valid = array("result"=>true);
        /* TODO ALL : Check if authorization on parent for all element (old stuff with parentId)
		/** =>	commented By Bouboule on 02 dec 2019
        if( $collection == Event::COLLECTION ){
            $valid = Event::validateFirst($params);
        } 
        */
        //error_log("KEY : ". $key);

        if( $valid["result"] ){
        	try {
        		$params = json_decode (json_encode ($params), true);
        		//xss clean up
			$params = DataValidator::clearUserInput($params);
			$validateData = $params;
			// enlever objectId,attributedTo de la validation
			unset($validateData['objectId']);
			unset($validateData['attributedTo']);
        	$valid = DataValidator::validate( ucfirst($key), $validateData, ( empty($paramsLinkImport) ? null : true), $id );

        	} catch (CTKException $e) {
        		$valid = array("result"=>false, "msg" => $e->getMessage());
        	}
        }

        
		$params = self::beforeSave($id, $collection, $params);
		
		if(!empty($params["id"]) && $id!=$params["id"]){
			$id=$params["id"];
		}
		
        if( $valid["result"]) {
			if( $collection == Event::COLLECTION ){
            	 $res = Event::formatBeforeSaving($params);
            	 if ($res["result"]) 
            	 	$params = $res["params"];
            	 else
            	 	throw new CTKException("Error processing before saving on event");
			}
			if( $collection == Action::COLLECTION ){
            	 $res = Action::formatBeforeSaving($params);
            	 if ($res["result"]) 
            	 	$params = $res["params"];
            	 else
            	 	throw new CTKException("Error processing before saving on action");
            }
			if( $collection == Project::COLLECTION ){
				$res = Action::formatBeforeSaving($params);
				if ($res["result"]) 
					$params = $res["params"];
				else
					throw new CTKException("Error processing before saving on action");
		   }

            if($id){ //var_dump($params); exit;

            	//update or insert 
            	$exists = PHDB::findOne($collection,array("_id"=>new MongoId($id)));
                if(!@$exists){
                	$params["creator"] = Yii::app()->session["userId"];
	        		$params["created"] = time();
	        		if(in_array($collection,[Organization::COLLECTION,Project::COLLECTION,Event::COLLECTION,Poi::COLLECTION])){
	        			$slug=Slug::checkAndCreateSlug($params["name"],$collection,$id);
	        			Slug::save($collection,$id,$slug);
	        			$params["slug"]=$slug;
	        		}

                	PHDB::updateWithOptions($collection,array("_id"=>new MongoId($id)), array('$set' => $params ),array('upsert' => true ));
                	
                	$params["_id"]=new MongoId($id);
                	if( $collection == Organization::COLLECTION )
                		$res["afterSave"] = Organization::afterSave($params, Yii::app()->session["userId"], $paramsLinkImport);
                	else if(in_array($collection, [ Proposal::COLLECTION, Action::COLLECTION, Room::COLLECTION] ))
                		$res["afterSave"] = Cooperation::afterSave($params, $collection);
                	else if( $collection == Network::COLLECTION )
                		$res["afterSave"] = Network::afterSave($params, Yii::app()->session["userId"]);
					else if( $collection == Badge::COLLECTION )
						$res["afterSave"] = Badge::afterSave($params);
                	
                	$res["afterSaveGbl"] = self::afterSave((string)$params["_id"],$collection,$params,$postParams);
                }
                else {
                	$params=Preference::beforeUpdateElement($exists, $params);
					$params["updated"] = time();

					if(count($unset) > 0){
						$modifierArray = array('$set' => $params, '$unset' => $unset);
					} else {
						$modifierArray = array('$set' => $params);
					}
                	PHDB::update($collection,array("_id"=>new MongoId($id)), $modifierArray);
                	// ATTENTION : bug quand update article par ex car pas de slug
                	$el = PHDB::findOneById($collection,$id,array('slug'));
                	if(!empty($el['slug'])){
                		Slug::updateElemTime( $el['slug'], $params["updated"] );
                		$params["slug"]=$el['slug'];
                	}

                	if(!empty($postParams["links"]))
                		$res = self::postLink($postParams, $collection, $id);
                	
					$params["_id"]=new MongoId($id);
					if( $collection == Organization::COLLECTION )
						$res["afterSave"] = Organization::afterSave($params, Yii::app()->session["userId"], $paramsLinkImport);
					else if(in_array($collection, [ Proposal::COLLECTION, Action::COLLECTION, Room::COLLECTION] ))
						$res["afterSave"] = Cooperation::afterSave($params, $collection);
					else if( $collection == Network::COLLECTION )
						$res["afterSave"] = Network::afterSave($params, Yii::app()->session["userId"]);
					else if( $collection == Badge::COLLECTION )
						$res["afterSave"] = Badge::afterSave($params);
                	$res["afterUpdate"] = self::afterUpdate($id,$collection,$params,$postParams, $paramsLinkImport);
					$res["afterSaveGbl"] = self::afterSave((string)$params["_id"],$collection,$params,$postParams);
                	if($collection==Event::COLLECTION)
                		$params=Event::formatDateRender($params);

                }

				if($collection == Project::COLLECTION)
					$params = Project::formatDateRender($params);

                $res = array( "result"=>true,
                             "msg"=>Yii::t("common","Your data are well updated"),
                             "reload"=>true,
                             "map"=>$params,
                             "id"=>$id);
            } 
            else 
            { 
				//insert a new elemnent into collection
                $params["created"] = time();
                Yii::app()->mongodb->selectCollection($collection)->insert( $params );
                $res = array("result"=>true,
                             "msg"=>Yii::t("common","Your data are well registred"),
                             "reload"=>true,
                             "map"=>$params,
                             "id"=>(string)$params["_id"]);
                // ***********************************
                //post process for specific actions
                // ***********************************
            
                if(in_array($collection,[Organization::COLLECTION,Project::COLLECTION,Event::COLLECTION,Poi::COLLECTION])){
        			$slug = Slug::checkAndCreateSlug( $params["name"],$collection, $res["id"] );
        			//var_dump($slug);
        			Slug::save($collection, $res["id"],$slug);
        			$params["slug"]=$slug;
        			self::updateField( $collection, $res["id"], "slug", $slug);
        		}

                if( $collection == Organization::COLLECTION )
                	$res["afterSave"] = Organization::afterSave($params, Yii::app()->session["userId"], $paramsLinkImport);
                else if( $collection == Proposal::COLLECTION || $collection == Action::COLLECTION )
                	$res["afterSave"] = Cooperation::afterSave($params, $collection);
                else if( $collection == Network::COLLECTION )
                	$res["afterSave"] = Network::afterSave($params, Yii::app()->session["userId"]);
				else if( $collection == Badge::COLLECTION )
					$res["afterSave"] = Badge::afterSave($params);
               $res["afterSaveGbl"] = self::afterSave((string)$params["_id"],$collection,$params,$postParams, $paramsLinkImport);
            }
            if(in_array($collection, array("poi","classified","crowdfunding"))){
		        	$url="?preview=".$collection.".".$id;
	        } else{
		        $url=false;
	        }
             
			$res["url"]=$url;
			//var_dump("url 1 ");
			if(Costum::isSameFunction("urlAfterSave")){
				//var_dump("url 2 ");
				$paramsCostum = array("id" => $id, "collection" => $collection, "elt" => $params);
				$u = Costum::sameFunction("urlAfterSave", $paramsCostum);
				//var_dump($u);
				if(!empty($u)){
					$res["url"]=$u;
					//var_dump("url 1 ");
				}
			}
        } else 
            $res = array( "result" => false, "error"=>"400",
                          "msg" => Yii::t("common","Something went really bad : ".$valid['msg']) );                        
        return $res;
    }

    public static function beforeSave ($id, $collection, $params) {
    	$paramsCostum = array("id" => $id, "collection" => $collection, "elt" => $params);
    	$paramsBefore=array();
    	if(Costum::isSameFunction("elementBeforeSave"))
    		$paramsBefore = Costum::sameFunction("elementBeforeSave", $paramsCostum);

    	if(!empty($paramsBefore) && !empty($paramsBefore["elt"]))
    		$params = $paramsBefore["elt"];
    	return $params;
    }

    public static function postLink ($postParams, $collection, $id) {
    	$res = array();
    	if(!empty($postParams["links"])){ 
			$list = array();
			$citizen_without_name=[];
			foreach ($postParams["links"] as $keyL => $valL) {
				if( in_array($keyL, array(Link::element2projects,Link::element2forms)) ){
					// if(isset($keyL)){
					// 	$arrayLink=(is_string($valL))? array($valL => array("type"=>$keyL)) : $valL;
						
					// 	$postParams["links"][$keyL] = $arrayLink;

					// }
					// PHDB::updateWithOptions($collection,array("_id"=>new MongoId($id)), array('$set' => $postParams ),array('upsert' => true ));
				}else{
					if (UtilsHelper::is_valid_mongoid($keyL)) {
						if(!empty($valL["email"])){
							$list["invites"][$keyL] = array("type" => $valL["type"], "name" => $valL["name"], "mail" => $valL["email"]);
	
							if(!empty($valL["roles"]))
								$list["invites"][$keyL]["roles"] = $valL["roles"];
	
						} else {
							$list["citoyens"][$keyL] = array("type" => $valL["type"], "name" => $valL["name"]);
							if(!empty($valL["roles"]))
								$list["citoyens"][$keyL]["roles"] = $valL["roles"];
						}
					} else {
						foreach($valL as $id => $element) {
							if(!empty($element["email"])){
								$list["invites"][$id] = array("type" => $element["type"], "name" => $element["name"], "mail" => $element["email"]);
		
							if(!empty($element["roles"]))
								$list["invites"][$id]["roles"] = $element["roles"];
							} else {
								$list[Person::COLLECTION][$id] = ["type" => $element["type"]];
								if(!empty($element["name"]))
									$list[Person::COLLECTION][$id]["name"] = $element["name"];
								UtilsHelper::push_array_if_not_exists($id, $citizen_without_name);
								if(!empty($element["roles"]))
									$list[Person::COLLECTION][$id]["roles"] = $element["roles"];
							}
						}
					}
				}
			}
			$parent = array("parentId" => $id, "parentType" => $collection);
			$db_names = PHDB::findByIds(Person::COLLECTION, $citizen_without_name,["name"]);
			foreach($db_names as $id => $db_name) {
				$list[Person::COLLECTION][$id]["name"] = $db_name["name"] ?? "";
			}
			if(count($list))
			$res = Link::invite($list, $parent);
			//Rest::json($res); exit;
		}
		return $res ;
    }

    public static function afterUpdate($id, $collection, $params, $postParams){
    	Costum::sameFunction("elementAfterUpdate", array("id"=>$id, "collection"=>$collection, "params"=>$params, "postParams"=>$postParams));
    }

    public static function afterSave ($id, $collection, $params, $postParams, $paramsImport=null) {
    	$res = array();
    	
    	// Mail reference inivite on communecter
        if(in_array($collection,[Organization::COLLECTION,Project::COLLECTION,Event::COLLECTION])){
        	if(@$params["email"] && $params["email"]!=@Yii::app()->session["userEmail"]){
        		Mail::referenceEmailInElement($collection, $id, $params["email"]);
        	}
        	Link::afterSave($collection, $params, $paramsImport);
        }

        $targetId=Yii::app()->session["userId"];
    	$targetType=Person::COLLECTION;
        if(!empty($params["parent"])){ 
			foreach($params["parent"] as $key => $v){
				$targetId=$key;
				$targetType=$v["type"];
				break;
			}
		}else if(!empty($params["organizer"])){
			foreach($params["organizer"] as $key => $v){
				$targetId=$key;
				$targetType=$v["type"];
				break;
			}
		}
        if (empty($paramsImport) && Preference::isPublicElement(@$params["preferences"]) && !in_array($collection, [Action::COLLECTION])){
        	Notification::createdObjectAsParam( Person::COLLECTION, 
												Yii::app()->session["userId"], 
												$collection, 
												$id, 
												$targetType, 
												$targetId, 
												( !empty($params["geo"]) ? $params["geo"] : "" ) , 
												( !empty($params["tags"]) ? $params["tags"] : null ),
												( ( !empty($organization["address"]) && !empty($organization["address"]["codeInsee"]) ) ? $organization["address"]["codeInsee"] : "" ) ) ;
        }

		if( in_array($targetType, array(Organization::COLLECTION, Project::COLLECTION)) )  {
	    	Notification::constructNotification(ActStr::VERB_ADD, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$targetType,"id"=> $targetId), array("id"=>$id,"type"=> $collection), $collection);
	    }
		if(isset($params["name"])){
			ActivityStream::saveActivityHistory( ActStr::VERB_CREATE, $id, $collection, self::getCommonByCollection($collection), $params["name"] ) ;
		}
		if(!empty($postParams["links"]))
			$res = self::postLink($postParams, $collection, $id);
		Costum::sameFunction( "elementAfterSave", array("id"=>$id, "collection"=>$collection, "params"=>$params, "paramsImport"=>$paramsImport, "postParams"=> @$postParams) ); 
    	return $res;
    }

    public static function prepData ($params) {
    	if( !empty($params["email"]) ) {
			$mail = Mail::authorizationMail($params["email"]);
			if($mail == false){
				unset($params["email"]);
				throw new CTKException("Vous ne pouvez pas renseigner cette adresse mail.");
			}
		}
		
		$params = Costum::prepData($params);

		
		if($params["collection"] == Network::COLLECTION){
			$params = Network::prepData ($params); 
		}else if($params["collection"] == Crowdfunding::COLLECTION && ($params["type"]=="donation" || $params["type"]=="pledge")){
			if(isset($params["iban"]))
				unset($params["iban"]);
			if(isset($params["donationPlatform"]))
				unset($params["donationPlatform"]);	
			if(isset($params["scope"]))
				unset($params["scope"]);			
		}else{
			//empty fields aren't properly validated and must be removed
	        $unset=(isset($params["unset"])) ? $params["unset"] : array() ;
	        foreach ($params as $k => $v) {
	            if(empty($v) && $v !== "0"){
	            	$unset[$k]=1;
	                unset($params[$k]);
	            }
	        }

	        if(!empty($unset))
	        	$params["unset"]=$unset;
	        $coordinates = @$params["geoPosition"]["coordinates"] ;
	        if(@$coordinates && (is_string($coordinates[0]) || is_string($coordinates[1])))
				$params["geoPosition"]["coordinates"] = array(floatval($coordinates[0]), floatval($coordinates[1]));

			if (!empty($params["tags"]))
				$params["tags"] = Tags::filterAndSaveNewTags($params["tags"]);
			if(isset($params["shortDescription"]))
				$params["shortDescription"] = strip_tags($params["shortDescription"]);
		
			$params["modified"] = new MongoDate(time());
			$params["updated"] = time();
			
			if( empty($params["id"]) ){
		        $params["creator"] = Yii::app()->session["userId"];
		        $params["created"] = time();
			}
			
			if($params["collection"] == Actions::COLLECTION){
				if( !empty($params["min"]) ){
					$params["min"] = (int) $params["min"];
				}
				if( !empty($params["max"]) ){
		        	$params["max"] = (int) $params["max"];
				}
				if( isset($params["credits"]) ){
		        	$params["credits"] = (int) $params["credits"];
				}
				if( !empty($params["group"]) ){
		        	$params["group"] = (int) $params["group"];
				}
			}

		    if(!empty($params["preferences"])){
		    	if(empty($params["preferences"]["isOpenData"]))
		    		$params["preferences"]["isOpenData"] = false;

		    	if(empty($params["preferences"]["isOpenEdition"]))
		    		$params["preferences"]["isOpenEdition"] = false;

		    	if(isset($params["preferences"]["privateFields"]) && $params["preferences"]["privateFields"]== "")
		    		unset($params["preferences"]["privateFields"]);

		    	if(isset($params["preferences"]["publicFields"]) && $params["preferences"]["publicFields"]== "")
		    		unset($params["preferences"]["publicFields"]);
		    }
		    // Rest::json($params["preferences"]);
		    // var_dump("HERE");
		    if(@$params["public"] /*&& in_array($params["collection"], [Event::COLLECTION, Project::COLLECTION, Classified::COLLECTION, Proposal::COLLECTION])*/){
        		//$params["preferences"]["public"]=$params["public"];
        		if(!is_bool($params["public"]))
		    		$params["public"] = ($params["public"] == "true") ? true : false;
		    	if($params["public"]==false)
		    		$params["preferences"]["private"]=true;
		    	else
		    		$params["preferences"]["private"]=false;
        		if(@$params["preferences"]["private"]){
        			$params["preferences"]["isOpenData"]=false;
        			$params["preferences"]["isOpenEdition"]=false;
        		} 
    //     		else {
				// 	$params["preferences"]["isOpenData"]=true;
    //     			$params["preferences"]["isOpenEdition"]=true;
				// }
        		unset($params["public"]);
        	}

        if(@$params["crowdfunding"] && in_array($params["collection"], [Project::COLLECTION])){
        		
        		//crowdfunding prepdata in the case of integrated crowdfundin fields in projects's dynform
        		//$params = Crowdfunding::prepData ($params);

        		if(!is_bool($params["crowdfunding"]))
		    		$params["crowdfunding"] = ($params["crowdfunding"] == "true") ? true : false;
		    	if($params["crowdfunding"]==true)
		    		$params["preferences"]["crowdfunding"]=true;
		    	else
		    		$params["preferences"]["crowdfunding"]=false;
    //     		
        		unset($params["crowdfunding"]);
        	}
        	// Rest::json($params["preferences"]);
        	//  var_dump("HERE2");
        	if(isset($params["answers"]) && $params["collection"]==Proposal::COLLECTION && count($params["answers"])<=1){
	        	unset($params["answers"]);
	        }

	        // if($params["collection"] == Event::COLLECTION){
	        // 	if(!empty($params['startDate'])){
	        // 		$date = new DateTime($params['startDate']);
	        // 		$params['timeZone'] = $date->getTimezone()->getName() ;
	        // 	}else if(!empty($params['created_at'])){
	        // 		$date = new DateTime($params['created_at']);
	        // 		$params['timeZone'] = $date->getTimezone()->getName() ;
	        // 	}
	        // 	//Rest::json($params); exit ;
	        // }

	        if(isset($params["recurrency"])){
	        	if($params["recurrency"] == "true" || $params["recurrency"]===true){
	        		unset($params["startDate"]);
	        		unset($params["endDate"]);
	        		$params["recurrency"]=true;
	        	} else {
	        		unset($params["openingHours"]);
	        		$params["recurrency"]=false;
	        	}
	        }

	        

		    /* TODO TO DELETE If not USed : 11/09/2010
		    if (isset($params["allDay"])) {
		    	if ($params["allDay"] == "true") {
					$params["allDay"] = true;
				} else {
					$params["allDay"] = false;
				}
			}*/

			if (isset($params["public"]) && !is_bool($params["public"])) {
		    	if ($params["public"] == "true")
					$params["public"] = true;
				else 
					$params["public"] = false;
				
			}

			if(isset($params["name"]))
				$params["name"] = $params["name"];

			if(isset($params["id"]) && !empty($params["id"]) && isset($params["slug"]) && !empty($params["slug"])){
				$params["slug"]=$params["slug"];
				if(!empty(Slug::getByTypeAndId($params["collection"],$params["id"])))
					Slug::update($params["collection"],$params["id"],$params["slug"]);
				else
					Slug::save($params["collection"],$params["id"],$params["slug"]);
			}
			

			if(!empty($params["parentType"])){
				$parentType = self::getCollectionByControler($params["parentType"]);
				if(!empty($parentType))
					$params["parentType"] = $parentType;
			}

			/* TODO DELETE IF NOT USED : 11/09/2020
			if(empty($params["idParentRoom"]) && 
				!empty($params["parentIdSurvey"]) && 
				$params["collection"] == Action::COLLECTION){
				$room = PHDB::findOne(ActionRoom::COLLECTION, array("parentIdSurvey" => $params["parentIdSurvey"]));

				if(empty($room)){

					$form = Form::getByIdMongo($params["parentIdSurvey"], array("title"));

					$paramsRoom = array(
						"parentId" => $params["parentId"],
						"parentType" => $params["parentType"],
						"parentIdSurvey" => $params["parentIdSurvey"],
						"status" => "open",
						"description" => "",
						"name" => $form["title"],
						"key" => ActionRoom::CONTROLLER,
						"collection" => ActionRoom::COLLECTION,
					);
					//
					//
					$room = self::save($paramsRoom);
					//Rest::json($room); exit ;
					$params["idParentRoom"] = $room["id"] ;
				}else{
					//var_dump($room); exit;
					$params["idParentRoom"] = (String) $room["_id"] ;
				}
			}*/


			if($params["collection"] == Action::COLLECTION && !empty($params["role"])){
				$params["role"] = array( InflectorHelper::slugify( $params["role"] ) => $params["role"] ) ;
			}
			if(isset($params["address"]["level4Name"]) && !ctype_upper($params["address"]["level4Name"])){
				$params["address"]["level4Name"] = strtoupper($params["address"]["level4Name"]);
			}
			$costum = CacheHelper::getCostum();
			if(isset($params["id"])){
				$exists = PHDB::findOne($params["collection"],array("_id"=>new MongoId($params["id"])));
				if(!@$exists){
					if(isset($costum) && isset($costum["slug"]))
					$params["source"]=array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]]);
				}else if($exists && isset($params["source"])){		
					unset($params["source"]);
				}
			}else{
				if(isset($costum) && isset($costum["slug"]))
					$params["source"]=array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]]);
			}

			// if(!isset($params["source"])){
			// 	$costum = CacheHelper::getCostum();
        	// 	if(isset($costum) && isset($costum["slug"]))
	        //  	   $params["source"]=array("insertOrign"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]]);
			// }

			// if(!empty($params["created_at"])){
			// 	unset($params["created_at"]);
			// }
		}
		//Rest::json($params["preferences"]);
        return $params;
    }

	public static function alreadyExists ($params, $collection) {
		$result = array("result" => false);

		if($collection != Event::COLLECTION){
			$where = array(	"name" => $params["name"],
							// "address.localityId" => @$params["address"]["localityId"]
						);
		    if($collection==Person::COLLECTION){
				$textRegExp =SearchNew::accentToRegex($params["name"]);
				$where = 
				    array(
					'$and' => array(
	
						array('$or'=>array(
							array( "name" => new MongoRegex("/.*{$textRegExp}.*/i")),
							array( "title" => new MongoRegex("/.*{$textRegExp}.*/i")),
							array( "slug" => new MongoRegex("/.*{$textRegExp}.*/i")),
							array("email"=>$params["email"])
						)),
						array('$or'=>array(
						    array(
							"slug" =>array('$exists' =>1),
							'$expr' => array('$ne'=>array(array('$strLenCP'=>'$slug'),32))
							
							),
							array("email"=>$params["email"])
							
						   )
						)   
						
					)                 
				    )
					;
  			
				
			}

			
			// if(!empty($params["startDate"])){
			// 	$startDate = DateTime::createFromFormat('Y-m-d H:i:s', $params["startDate"]);
			// 	$s = new MongoDate(strtotime($startDate->format('d/m/Y H:i')));
			// 	$where["startDate"] = array('$lte' => $s );
			// }

			// if(!empty($params["endDate"])){
			// 	$endDate = DateTime::createFromFormat('Y-m-d H:i:s', $params["endDate"]);
			// 	$s = new MongoDate(strtotime($endDate->format('d/m/Y H:i')));
			// 	$where["endDate"] = array('$lte' => $s );
			// }

			//Rest::json($where); exit;

			$element = PHDB::find($collection, $where);
			if(!empty($element))
				$result = array("result" => true ,
								"element" => $element,
							     "where" => $where
							);
			// var_dump($result);exit;				
		}
		
		return $result;
    }


    /**
	 * Retrieve a element by id from DB
	 * @param String $id of the event
	 * @return array with data id, name, type profilImageUrl
	 */
	public static function getElementById($id, $collection, $where=null, $fields=null){
		$element = null;
		$where = !empty($where) && is_array($where) ? $where : [];
		if(!empty($id)){
			$where["_id"] = new MongoId($id);
			$element = PHDB::findOne($collection, $where ,$fields);
		}
		return ($element);
	}

	public static function getElementByName($name, $collection, $source = ""){
		$where = array('name' => $name);
		if($source != "")
			$where["source.key"] =  new MongoDB\BSON\Regex($source, "i");
		$element =  PHDB::find($collection, $where);
		return @$element;
	}

	public static function getOnceElementByName($name, $collection){
		$element =  PHDB::findOne($collection, ['name' => $name]);
		return @$element;
	}

	public static function getElementByWhere($collection, $where=null, $fields=null){
		$element = PHDB::findOne($collection, $where ,$fields);
		return @$element;
	}


	public static function getElementSimpleById($id, $collection,$where=null, $fields=null){
		if(empty($fields))
			$fields = array("_id", "name");
		$element = self::getElementById($id, $collection, $where ,$fields);
		return ($element);
	}

	public static function getElementSimpleByIds($ids,$collection,$where=null, $fields=null){
		if(empty($fields))
			$fields = array("_id", "name");
		$where["_id"] = array('$in' => $ids);
		$elements = PHDB::find($collection, $where ,$fields);
		return @$elements;
	}
	
	public static function followPerson($params, $gmail=null){
		
		$invitedUserId = "";

        if (empty(Yii::app()->session["userId"])) {
        	return Rest::json(array("result" => false, "msg" => Yii::t("common","The current user is not valid : please login.")));
        	die();
        }
        
        //Case spécial : Vérifie si l'email existe et retourne l'id de l'utilisateur
        if (!empty($params["invitedUserEmail"]))
        	$invitedUserId = Person::getPersonIdByEmail($params["invitedUserEmail"]);

        if(Yii::app()->session["userId"]==$invitedUserId){
        	return Rest::json(array("result" => false, "msg" => Yii::t("common","You try to invite yourself")));
        	die();
        }
        //Case 1 : the person invited exists in the db
        if (!empty($params["connectUserId"]) || !empty($invitedUserId)) {
        	if (!empty($params["connectUserId"]))
        		$invitedUserId = $params["connectUserId"];
        	if(Link::isLinked(Yii::app()->session["userId"], Person::COLLECTION, (string)$invitedUserId))
        		$res["msg"]=Yii::t("common","This user is already connected and you already follow him");
        	else{
	        	$child["childId"] = Yii::app()->session["userId"] ;
    	    	$child["childType"] = Person::COLLECTION;

	        	$res = Link::follow((string)$invitedUserId, Person::COLLECTION, $child);
	            $actionType = ActStr::VERB_FOLLOW;
	            $msg=Yii::t("common","This user is already connected, but now you follow him");
	        }
		//Case 2 : the person invited does not exist in the db
		} else if (empty($params["invitedUserId"])) {
			$newPerson = array("name" => $params["invitedUserName"], "email" => $params["invitedUserEmail"], "invitedBy" => Yii::app()->session["userId"]);
			
			//if(!empty($params["msgEmail"]))
			$res = Person::createAndInvite($newPerson, @$params["msgEmail"], $gmail);
			//else
				//$res = Person::createAndInvite($newPerson);

            $actionType = ActStr::VERB_INVITE;
            if ($res["result"]) {
            	$invitedUserId = $res["id"];
                $child["childId"] = Yii::app()->session["userId"];
    			$child["childType"] = Person::COLLECTION;
                $res = Link::follow($invitedUserId, Person::COLLECTION, $child);
            }
            $msg=Yii::t("common","The invitation is sent with success");
		}
		
        if (@$res["result"] == true) {
            $person = Person::getSimpleUserById($invitedUserId);
            $res = array("result" => true, "msg"=>$msg,"invitedUser" => $person);
        } else {
            $res = array("result" => false, "msg" => $res["msg"]);
        }

		return $res;
	}

	public static function followPersonByListMails($listMails, $msgEmail=null, $gmail=null){
		$result = array("result" => false);
		$params["msgEmail"] = (empty($msgEmail)?null:$msgEmail) ;
		foreach ($listMails as $key => $value) {
			$result["result"] = true ;
			//if(!empty($value["mail"])){
				$params["invitedUserEmail"] = $key ;

				if(empty($value)){
					$split = explode("@", $key);
					$params["invitedUserName"] = $split[0];
				}else
					$params["invitedUserName"] = $value ;
				$result["data"][] = self::followPerson($params, $gmail);
			//}
		}
		return $result;
	}
	
    public static function saveChart($type, $id, $properties, $label){
	    //TODO SABR - Check the properties before inserting
	    PHDB::update($type,
			array("_id" => new MongoId($id)),
            array('$set' => array("properties.chart.".$label=> $properties))
        );
        return true;
    }
    
	public static function removeChart($type, $id, $label){
		PHDB::update($type, 
            array("_id" => new MongoId($id)) , 
            array('$unset' => array("properties.chart.".$label => 1))
        );
        return true;	
	}

	public static function afterSaveImport($eltId, $eltType, $paramsImport){
		if (@$paramsImport) {
			if(!empty($paramsImport["link"])){
				$idLink = $paramsImport["link"]["idLink"];
				$typeLink = $paramsImport["link"]["typeLink"];
				if (@$paramsImport["link"]["role"] == "admin"){
					$isAdmin = true;
				}else{
					$isAdmin = false;
				}


				/*const person2person = "follows";
			    const person2organization = "memberOf";
			    const organization2person = "members";
			    const person2events = "events";
			    const person2projects = "projects";
			    const event2person = "attendees";
			    const project2person = "contributors";
			    const need2Item = "needs";*/
				if($eltType == Organization::COLLECTION){
					if($typeLink == Organization::COLLECTION){
						$connectType1 = "members";
						$connectType2 = "memberOf";
						//Link::connect($idLink, $typeLink, $eltId, $eltType, $creatorId,"members", false);
						//Link::connect($eltId, $eltType, $idLink, $typeLink, $creatorId,"memberOf",false);
					}
					else if($typeLink == Person::COLLECTION){
						$connectType1 = "members";
						$connectType2 = "memberOf";
					}
				}else if($eltType == Person::COLLECTION){
					if($typeLink == Organization::COLLECTION){
						$connectType1 = "memberOf";
						$connectType2 = "members";
					}else if($typeLink == Person::COLLECTION){
						$connectType1 = "followers";
						$connectType2 = "follows";
					}
				}else if($eltType == Project::COLLECTION){
					if($typeLink == Organization::COLLECTION){
						$connectType1 = "contributors";
						$connectType2 = "projects";
					}else if($typeLink == Person::COLLECTION){
						$connectType1 = "contributors";
						$connectType2 = "projects";
					}
				}else if($eltType == Event::COLLECTION){
					if($typeLink == Organization::COLLECTION){
						//$connectType1 = "memberOf";
						//$connectType2 = "members";
					}else if($typeLink == Person::COLLECTION){
						$connectType1 = "attendees";
						$connectType2 = "events";
					}else if($typeLink == Event::COLLECTION){
						$connectType1 = "attendees";
						$connectType2 = "events";
					}
				}

				if(!empty($connectType1) && !empty($connectType2)){
					Link::connect($eltId, $eltType, $idLink, $typeLink, $creatorId, $connectType1,$isAdmin);
					Link::connect($idLink, $typeLink, $eltId, $eltType, $creatorId, $connectType2,$isAdmin);
				}
				
				
			}

			if(!empty($paramsImport["img"])){
		    	try{
		    		$paramsImg = $paramsImport["img"] ;
					$resUpload = Document::uploadDocumentFromURL(	$paramsImg["module"], $eltType, 
																	$eltId, "avatar", false, 
																	$paramsImg["url"], $paramsImg["name"]);
					if(!empty($resUpload["result"]) && $resUpload["result"] == true){
						$params = array();
						$params['id'] = $eltId;
						$params['type'] = $eltType;
						$params['moduleId'] = $paramsImg["module"];
						$params['folder'] = $eltType."/".$eltId;
						$params['name'] = $resUpload['name'];
						$params['author'] = Yii::app()->session["userId"] ;
						$params['size'] = $resUpload["size"];
						$params["contentKey"] = "profil";
						$resImgSave = Document::save($params);
						if($resImgSave["result"] == false)
							throw new CTKException("Impossible de sauvegarder l'image.");
					}else{
						throw new CTKException("Impossible uploader l'image.");
					}
				}catch (CTKException $e){
					throw new CTKException($e);
				}	
			}
		}
	}


	public static function saveContact($params){
		$id = $params["parentId"];
		$collection = $params["parentType"];


		$mail = Mail::authorizationMail($params["email"]);
		if($mail == false){
			unset($params["email"]);
			throw new CTKException("Vous ne pouvez pas renseigner cette adresse mail.");
		}

		
		if(!empty($params["phone"]))
			$params["telephone"] = explode(",", $params["phone"]);
		//if(!empty($params["idContact"]))
		//	$params["id"] = $params["idContact"];
		unset($params["parentId"]);
		unset($params["parentType"]);
		unset($params["phone"]);
		//unset($params["idContact"]);

		if(empty($params["name"]) && empty($params["email"]) && empty($params["role"]) && empty($params["telephone"]))
			$res = array("result" => false, "msg" => "Vous devez avoir au moins une information sur le contact");
		else
			$res = self::updateField($collection, $id, "contacts", $params);

		if($res["result"])
			$res["msg"] = Yii::t("common","Contacts are well updated");
		return $res;
	}

	public static function saveUrl($params){
		$id = $params["parentId"];
		$collection = $params["parentType"];
		$params["url"]=self::getAndCheckUrl($params["url"]);

		unset($params["parentId"]);
		unset($params["parentType"]);
		unset($params["key"]);
		unset($params["collection"]);
		$res = self::updateField($collection, $id, "urls", $params);
		if($res["result"])
			$res["msg"] = Yii::t("common","URLs are well updated");
		return $res;
	}

	public static function getAndCheckUrl($url){
		$needles = array("http://", "https://");
		$find=false;
	    foreach($needles as $needle) {
	    	if(stripos($url, $needle) == 0)
	    		$find = true;
	    }
	    if(!$find)
	    	$url="http://".$url;
	    return $url ;
	}

	public static function getUrls($id, $type){
		$res = array();
		$listElt = array(Organization::COLLECTION, Person::COLLECTION, Project::COLLECTION, Event::COLLECTION);
		if(in_array($type, $listElt) ){
			$res = PHDB::findOne( $type , array( "_id" => new MongoId($id) ) ,array("urls") );
			$res = (!empty($res["urls"]) ? $res["urls"] : array() );
		}
		return $res;
	}

	public static function getCuriculum($id, $type){
		$res = array();
		$listElt = array(Organization::COLLECTION, Person::COLLECTION, Project::COLLECTION, Event::COLLECTION);
		if(in_array($type, $listElt) ){
			$res = PHDB::findOne( $type , array( "_id" => new MongoId($id) ) ,array("curiculum") );
			$res = (!empty($res["curiculum"]) ? $res["curiculum"] : array() );
		}
		return $res;
	}

	public static function getContacts($id, $type){
		$res = array();
		$listElt = array(Organization::COLLECTION, Person::COLLECTION, Project::COLLECTION, Event::COLLECTION);
		if(in_array($type, $listElt) ){
			$res = PHDB::findOne( $type , array( "_id" => new MongoId($id) ) ,array("contacts") );
			$res = (!empty($res["contacts"]) ? $res["contacts"] : array() );

			foreach ($res as $key => $value) {
				$res[$key]["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $type, $id);
				$res[$key]["typeSig"] = "person";
			}

		}
		return $res;
	}

	public static function getContactsByMails($listMails){
		$res = array();
		foreach ($listMails as $key => $mail){
			$valid = DataValidator::email($mail) ;
			$resMail = Mail::authorizationMail($mail);
			if($resMail == false){
				throw new CTKException("Vous ne pouvez pas renseigner cette adresse mail : ".$mail);
			}else{
				if( $valid  == ""){
					$person = PHDB::findOne( Person::COLLECTION , array( "email" => $mail ), array("_id", "name", "profilThumbImageUrl", "slug") );
					if(!empty($person["_id"])){
						$person["id"] = (String) $person["_id"];
						$res[$mail] = $person;
					}else
						$res[$mail] = false ;
				}
				else
					$res[$mail] = false ;
			}

			
		}
		return $res;
	}

	public static function updateBlock($params){
		$block = $params["block"];
		$collection = $params["typeElement"];
		$id = $params["id"];
		$res = array();
		try {

			if($block == "info"){
				if(isset($params["name"])){
					$res[] = self::updateField($collection, $id, "name", $params["name"]);
					/*PHDB::update( $collection,  array("_id" => new MongoId($id)), 
			 										array('$unset' => array("hasRC"=>"") ));*/
				}
				if(isset($params["username"]) && $collection == Person::COLLECTION)
					$msgError = Yii::t("common","Username cannot be changed.");
					//$res[] = self::updateField($collection, $id, "username", $params["username"]);
				if(isset($params["avancement"]) && $collection == Project::COLLECTION)
					$res[] = self::updateField($collection, $id, "avancement", $params["avancement"]);
				if(isset($params["tags"]))
					$res[] = self::updateField($collection, $id, "tags", $params["tags"]);
				if(isset($params["type"])  && ( $collection == Event::COLLECTION || $collection == Organization::COLLECTION) )
					$res[] = self::updateField($collection, $id, "type", $params["type"]);
				if(isset($params["email"])){
					if(!empty($params["email"])){
						$mail = Mail::authorizationMail($params["email"]);
						if($mail == false){
							unset($params["email"]);
							throw new CTKException("Vous ne pouvez pas renseigner cette adresse mail.");
						}
						
					}

					$resEmail=self::updateField($collection, $id, "email", $params["email"]);
					$res[] = $resEmail;
					// Mail reference inivite on communecter
					if($resEmail["result"] && in_array($collection,[Organization::COLLECTION,Project::COLLECTION,Event::COLLECTION])){
						if(@$params["email"] && !empty($params["email"]) && $params["email"]!=@Yii::app()->session["userEmail"]){
							Mail::referenceEmailInElement($collection, $id, $params["email"]);
						}
					}
				}

				if(isset($params["slug"])){
					$el = PHDB::findOne($collection,array("_id"=>new MongoId($id)));
					$oldslug = @$el["slug"];
					if(!empty(Slug::getByTypeAndId($collection,$id)))
						Slug::update($collection,$id,$params["slug"]);
					else
						Slug::save($collection,$id,$params["slug"]);
					Self::updateElementSourceKeysAndReference($oldslug,$params["slug"]);
					$res[] = self::updateField($collection, $id, "slug", $params["slug"]);
				}
				//update RC channel name if exist
				if(@$el["hasRC"]){
					$group = RocketChat::rename( $oldslug, $params["slug"], @$el["preferences"]["private"] );
				if($group != null && @$group->rename->success ) {
					$pathRc = ( @$el["preferences"]["private"] === true ) ? "group":"channel";
				PHDB::update( $collection,  array("_id" => new MongoId($id), "tools.chat.int.name" => $oldslug), 
												 array('$set' => array("tools.chat.int.$.name"=> $params["slug"], "tools.chat.int.$.url"=> '/'.$pathRc.'/'.$params["slug"]) ));
											}
				}
				if(isset($params["url"]))
					$res[] = self::updateField($collection, $id, "url", self::getAndCheckUrl($params["url"]));
				if(isset($params["birthDate"]) && $collection == Person::COLLECTION)
					$res[] = self::updateField($collection, $id, "birthDate", $params["birthDate"]);
				if(isset($params["fixe"]))
					$res[] = self::updateField($collection, $id, "fixe", $params["fixe"]);
				if(isset($params["fax"]))
					$res[] = self::updateField($collection, $id, "fax", $params["fax"]);
				if(isset($params["mobile"]))
					$res[] = self::updateField($collection, $id, "mobile", $params["mobile"]);
				
				if( !empty($params["parentId"]) ){
					$parent["parentId"] = $params["parentId"] ;
					$parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
					$resParent = self::updateField($collection, $id, "parent", $parent);
					if($parent["parentType"] != "dontKnow" && $parent["parentId"] != "dontKnow")
						$resParent["value"]["parent"] = Element::getByTypeAndId( $params["parentType"], $params["parentId"]);
					$res[] = $resParent;
				}
				if(isset($params["parent"])){
					if( !empty($params["parent"]) ){
						//$parent["parentId"] = $params["parentId"] ;
						//$parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
						$resParent = self::updateField($collection, $id, "parent", $params["parent"]);
						foreach($resParent["value"] as $key => $value) {
							//var_dump($value["type"]); exit;
							$elt=Element::getElementById($key, $value["type"], null, array("name", "slug","profilThumbImageUrl"));
							$resParent["value"][$key]=array_merge($resParent["value"][$key], $elt);
						}
						$res[] = $resParent;
					}else{
						PHDB::update($collection, ["_id" => new MongoId($id)], ['$unset' => ["parent"=>true]]);
					}
				}
				if( !empty($params["badges"]) ){
					$resBadge = self::updateField($collection, $id, "badges", $params["badges"]);
					$baseDir = dirname(__FILE__) . "/../../../pixelhumain/ph/";
					foreach($resBadge["value"] as $key => $value) {
						$elt=Element::getElementById($key, $value["type"], null, array("name", "slug","profilThumbImageUrl", "profilMediumImageUrl"));
                		$resBadge["value"][$key]=array_merge($resBadge["value"][$key], $elt);
						if(isset($elt["profilMediumImageUrl"])){
							$baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
							$image = file_get_contents( $baseDir . $elt["profilMediumImageUrl"]);
							$meta = new PNGMetaDataHandler($image);
							$image_with_meta = $meta->add_chunks('iTXt', 'openbadges', $baseUrl . "/co2/badges/assertions/badge/" . $key . "/award/" . $id);
							// $image_with_meta = $meta->add_chunks('iTXt', 'openbadges', json_encode($assertion, JSON_UNESCAPED_SLASHES));
							$testeur = new PNGMetaDataHandler($image_with_meta);
							$first = substr($elt["profilMediumImageUrl"], 0, strripos($elt["profilMediumImageUrl"],"/"));
							$saveDir = $baseDir . substr($first, 0 , strripos($first,"/")) . "/". "assertions/";
							if(!is_dir($saveDir)){
								mkdir($saveDir, 0755, true);
							}
							file_put_contents($saveDir . $id . ".png", $image_with_meta);
						}
					}
					$res[] = $resBadge;
				}
				if(!empty($params["organizerId"]) ){
					$organizer["organizerId"] = $params["organizerId"] ;
					$organizer["organizerType"] = ( !empty($params["organizerType"]) ? $params["organizerType"] : "dontKnow" ) ;
					$resOrg = self::updateField($collection, $id, "organizer", $organizer);

					if($params["organizerType"]!="dontKnow" && $params["organizerId"] != "dontKnow"){
						$resOrg["value"]["organizer"] = Element::getByTypeAndId( $params["organizerType"], $params["organizerId"]);
					}
					$res[] = $resOrg;
				}
				if( !empty($params["organizer"]) ){
					//$parent["parentId"] = $params["parentId"] ;
					//$parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
					$resOrg = self::updateField($collection, $id, "organizer", $params["organizer"]);
					foreach($resOrg["value"] as $key => $value) {
						$elt=Element::getElementById($key, $value["type"], null, array("name", "slug","profilThumbImageUrl"));
                		$resOrg["value"][$key]=array_merge($resOrg["value"][$key], $elt);
					}
					$res[] = $resOrg;
				}
				if(isset($params["description"])){
					$res[] = self::updateField($collection, $id, "description", $params["description"]);
					self::updateField($collection, $id, "descriptionHTML", null);
				}

			}else if($block == "network"){
				if(isset($params["telegram"]) /*&& $collection == Person::COLLECTION*/)
					$res[] = self::updateField($collection, $id, "telegram", $params["telegram"]);
				if(isset($params["facebook"]))
					$res[] = self::updateField($collection, $id, "facebook", self::getAndCheckUrl($params["facebook"]));
				if(isset($params["twitter"]))
					$res[] = self::updateField($collection, $id, "twitter", self::getAndCheckUrl($params["twitter"]));
				if(isset($params["github"]))
					$res[] = self::updateField($collection, $id, "github", self::getAndCheckUrl($params["github"]));
				if(isset($params["gpplus"]))
					$res[] = self::updateField($collection, $id, "gpplus", self::getAndCheckUrl($params["gpplus"]));
				if(isset($params["gitlab"]))
					$res[] = self::updateField($collection, $id, "gitlab", self::getAndCheckUrl($params["gitlab"]));
				if(isset($params["diaspora"]))
					$res[] = self::updateField($collection, $id, "diaspora", self::getAndCheckUrl($params["diaspora"]));
				if(isset($params["mastodon"]))
					$res[] = self::updateField($collection, $id, "mastodon", self::getAndCheckUrl($params["mastodon"]));
				if(isset($params["instagram"]))
					$res[] = self::updateField($collection, $id, "instagram", self::getAndCheckUrl($params["instagram"]));
                if(isset($params["signal"]))
                    $res[] = self::updateField($collection, $id, "signal", self::getAndCheckUrl($params["signal"]));
                if(isset($params["linkedin"]))
                    $res[] = self::updateField($collection, $id, "linkedin", self::getAndCheckUrl($params["linkedin"]));

			}else if( $block == "when" && ( $collection == Event::COLLECTION || $collection == Project::COLLECTION) ) {
				//var_dump($params);
				if(isset($params["allDayHidden"]) && $collection == Event::COLLECTION)
					$res[] = self::updateField($collection, $id, "allDay", (($params["allDayHidden"] == "true") ? true : false));
				

					if(isset($params["startDate"])){
						// var_dump($params["startDate"]);
						// var_dump("STARTDATE");
						$res[] = self::updateField($collection, $id, "startDate", $params["startDate"],@$params["allDay"]);
					}

					if(isset($params["endDate"])){
						// var_dump($params["endDate"]);
						// var_dump("ENDATE");
						$res[] = self::updateField($collection, $id, "endDate", $params["endDate"],@$params["allDay"]);
					}

					// if(isset($params["startDate"])){
					// 	date_default_timezone_set('UTC');
					// 	$dt = DataValidator::getDateTimeFromString($fieldValue, $dataFieldName);

					// 	if (isset($params["startDate"]) && !empty($params["allDayHidden"]) && $params["allDayHidden"] == "true")
					// 		$dt=date_time_set($dt, 00, 00);
						
					// 	$newMongoDate = new MongoDate($dt->getTimestamp());
					// 	$set = array($dataFieldName => $newMongoDate);
					// 	PHDB::update($collection, 
					// 						array("_id" => new MongoId($id)), 
				 //                          	array('$set' => $set ) );
					// 	$res[] = array("result"=>true,"msg"=>"messagge", "fieldName" => "startDate", "value" => $params["startDate"]);
					// }

					// if(isset($params["endDate"])){
					// 	date_default_timezone_set('UTC');
					// 	$dt = DataValidator::getDateTimeFromString($fieldValue, $dataFieldName);

					// 	if (isset($params["endDate"]) && !empty($params["allDayHidden"]) && $params["allDayHidden"] == "true") 
					// 		$dt=date_time_set($dt, 23, 59);

					// 	$newMongoDate = new MongoDate($dt->getTimestamp());
					// 	$set = array($dataFieldName => $newMongoDate);
					// 	PHDB::update($collection, 
					// 						array("_id" => new MongoId($id)), 
				 //                          	array('$set' => $set ) );

					// 	$res[] = array("result"=>true,"msg"=>"messagge", "fieldName" => "endDate", "value" => $params["endDate"]);
					// }


				//}
			
			}else if($block == "toMarkdown"){

				$res[] = self::updateField($collection, $id, "description", $params["value"]);
				$res[] = self::updateField($collection, $id, "descriptionHTML", null);
			}else if($block == "companyProfile"){ 
				if(isset($params["siren"]))
					$res[] = self::updateField($collection, $id, "siren", $params["siren"]);
				if(isset($params["projectsInProgress"]))
					$res[] = self::updateField($collection, $id, "projectsInProgress", $params["projectsInProgress"]);
				if(isset($params["nombreETP"]))
					$res[] = self::updateField($collection, $id, "nombreETP", $params["nombreETP"]);
				if(isset($params["tags"]))
					$res[] = self::updateField($collection, $id, "tags", $params["tags"]);
				if(isset($params["adminFirstname"]))
					$res[] = self::updateField($collection, $id, "adminFirstname", $params["adminFirstname"]);
				if(isset($params["adminLastname"]))
					$res[] = self::updateField($collection, $id, "adminLastname", $params["adminLastname"]);
				if(isset($params["adminFunction"]))
					$res[] = self::updateField($collection, $id, "adminFunction", $params["adminFunction"]);
				if(isset($params["adminPhone"]))
					$res[] = self::updateField($collection, $id, "adminPhone", $params["adminPhone"]);
				if(isset($params["adminMail"]))
					$res[] = self::updateField($collection, $id, "adminMail", $params["adminMail"]);
				if(isset($params["url"]))
					$res[] = self::updateField($collection, $id, "url", $params["url"]);
				if(isset($params["email"]))
					$res[] = self::updateField($collection, $id, "email", $params["email"]);
				if(isset($params["skills"]))
					$res[] = self::updateField($collection, $id, "skills", $params["skills"]);
			}else if($block == "descriptions"){	

				if(isset($params["tags"]))
					$res[] = self::updateField($collection, $id, "tags", $params["tags"]);

				if(isset($params["description"])){
					$res[] = self::updateField($collection, $id, "description", $params["description"]);
					self::updateField($collection, $id, "descriptionHTML", null);
				}
				
				if(isset($params["shortDescription"]))
					$res[] = self::updateField($collection, $id, "shortDescription", strip_tags($params["shortDescription"]));
				
				//Test pour l'enregistrement d'une category lapossession
				// if(isset($params["category"]))
				// 	$res[] = self::updateField($collection, $id, "category", strip_tags($params["category"]));
			
			}else if($block == "activeCoop"){

				if(isset($params["status"]))
					$res[] = self::updateField($collection, $id, "status", $params["status"]);
				if(isset($params["voteActivated"]))
					$res[] = self::updateField($collection, $id, "voteActivated", $params["voteActivated"]);
				if(isset($params["amendementActivated"]))
					$res[] = self::updateField($collection, $id, "amendementActivated", $params["amendementActivated"]);
			
			}else if($block == "amendement"){

				if(isset($params["txtAmdt"]) && isset($params["typeAmdt"]) && isset($params["id"]) && @Yii::app()->session['userId']){
					$proposal = Proposal::getById($params["id"]);
					$amdtList = @$proposal["amendements"] ? $proposal["amendements"] : array();
					$rand = rand(1000, 100000);
					while(isset($amdtList[$rand])){ $rand = rand(1000, 100000); }

					$amdtList[$rand] = array(
										"idUserAuthor"=> Yii::app()->session['userId'],
										"typeAmdt" => $params["typeAmdt"],
										"textAdd"=> $params["txtAmdt"]);
					Notification::constructNotification ( ActStr::VERB_AMEND, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$proposal["parentType"],"id"=>$proposal["parentId"]),array( "type"=>Proposal::COLLECTION,"id"=> $params["id"] ) );
					$res[] = self::updateField($collection, $id, "amendements", $amdtList);
				}
			
			}else if($block == "curiculum.skills"){
				$parent = Element::getByTypeAndId($params["typeElement"], $params["id"]);
				$cv = @$parent["curiculum"] ? $parent["curiculum"] : array();

				$CVAttrs = array("competences", "mainQualification", "hasVehicle", "languages",
								"motivation", "driverLicense", "url");
				foreach ($CVAttrs as $att) {
					if(@$params[$att]) 
					$cv["skills"][$att] = @$params[$att];
				}
				$res[] = self::updateField($collection, $id, "curiculum", $cv);
				//var_dump($params);
			}else if($block == "curiculum.lifepath"){
				$parent = Element::getByTypeAndId($params["typeElement"], $params["id"]);
				$cv = @$parent["curiculum"] ? $parent["curiculum"] : array();
				$indexLP = @$cv["lifepath"] ? sizeof($cv["lifepath"]) : 0;
				
				$CVAttrs = array("title", "description", "startDate", "endDate",
								"location");
				foreach ($CVAttrs as $att) {
					if(@$params[$att]) 
					$cv["lifepath"][$indexLP][$att] = @$params[$att];
				}
				$res[] = self::updateField($collection, $id, "curiculum", $cv);
				//var_dump($params);
			}else if($block == "localities"){

				$set = array();
				$unset = array();
				if(!empty($params["address"])){
					$set["address"] = $params["address"];
				} else {
					$unset["address"] = array();
				}

				if(!empty($params["geo"])){

					$set["geo"] = SIG::getFormatGeo($params["geo"]["latitude"], $params["geo"]["longitude"]);
					$set["geoPosition"] = SIG::getFormatGeoPosition($params["geo"]["latitude"], $params["geo"]["longitude"]);
				} else {
					$unset["geo"] = array();
					$unset["geoPosition"] = array();
				}

				if(!empty($params["addresses"])){
					$set["addresses"] = $params["addresses"];
				} else {
					$unset["addresses"] = array();
				}

				//Rest::json($set) ; exit;
				if(!empty($set)){
					PHDB::update( 	$collection, 
										array("_id" => new MongoId($id)), 
			                          	array('$set' => $set ) );
				}

				if(!empty($unset)){
					PHDB::update( 	$collection, 
										array("_id" => new MongoId($id)), 
			                          	array('$unset' => $unset ) );
				}
				

				$res[] = array("result"=>true, "value" => $set, "fieldName"=> "localities");
			}

			if(Import::isUncomplete($id, $collection)){
				Import::checkWarning($id, $collection, Yii::app()->session['userId'] );
			}

			if( $collection == Event::COLLECTION || $collection == Project::COLLECTION || $collection == Organization::COLLECTION ){
				$el = PHDB::findOneById($collection,$id,array('slug'));
        		Slug::updateElemTime( $el['slug'], time() );
			}

			if(Costum::isSameFunction("updateBlock")){
				
				//var_dump($upBlock[0])
				$costumRes = Costum::sameFunction("updateBlock", $params);
				if(isset($costumRes[0])){
					foreach($costumRes as $index=>$field){
						array_push($res,$field);
					}
				}else{
					$res[] = Costum::sameFunction("updateBlock", $params);
				}		
			}


			$result = array("result"=>true);
			$resultGoods = array();
			$resultErrors = array();
			$values = array();
			$msg = "";
			$msgError = "";
			foreach ($res as $key => $value) {
				if($value["result"] == true){
					if($msg != "")
						$msg .= ", ";
					$msg .= Yii::t("common",$value["fieldName"]);
					$values[$value["fieldName"]] = $value["value"];
				}else{
					if($msgError != "")
						$msgError .= ". ";
					$msgError .= $value["mgs"];
				}
			}

			if($msg != ""){
				$resultGoods["result"]=true;
				$resultGoods["msg"]= Yii::t("common", "The following attributs has been updated :")." ".Yii::t("common",$msg);
				$resultGoods["values"] = $values ;
				$result["resultGoods"] = $resultGoods ;
				$result["result"] = true ;
			}

			if($msgError != ""){
				$resultErrors["result"]=false;
				$resultErrors["msg"]=Yii::t("common", $msgError);
				$result["resultErrors"] = $resultErrors ;
			}
		} catch (CTKException $e) {
			$resultErrors["result"]=false;
			$resultErrors["msg"]=$e->getMessage();
			$result["resultErrors"] = $resultErrors ;
		}
		return $result;
	}


	public static function updateElementSourceKeysAndReference($oldSlug,$newSlug){
		$collection = [
			Event::COLLECTION,
			Organization::COLLECTION,
			Project::COLLECTION,
			Poi::COLLECTION,
			Cms::COLLECTION,
			Template::COLLECTION,
			Proposal::COLLECTION,
			News::COLLECTION,
			Classified::COLLECTION,
			Answer::COLLECTION,
			Badge::COLLECTION,
			Person::COLLECTION,
			Crowdfunding::COLLECTION,
			Document::COLLECTION,
			Form::COLLECTION
		];
		foreach($collection as $kC => $col){
			$allEl = PHDB::find($col, array(
					'$or' => array(
						array("source.key" => $oldSlug),
						array( "source.keys" => array('$in' => array($oldSlug) ) ),
                        array( "reference.costum" => array('$in' => array($oldSlug) ) ) 
					)
				)
			);
			foreach($allEl as $kEl => $el){
				$set = array();
				if(isset($el["source"]["key"]) && $el["source"]["key"] == $oldSlug){
					$set["source.key"] = $newSlug;
				}
				if(isset($el["source"]["keys"]) && in_array($oldSlug, $el["source"]["keys"])){					
					$source = array();
					foreach ($el["source"]["keys"] as $k => $v) {
                        if($v != $oldSlug){
                            array_push($source,$v);
						}
                    }
					array_push($source,$newSlug);
					$set["source.keys"] = $source;
				}
				if(isset($el["reference"]["costum"]) && in_array($oldSlug, $el["reference"]["costum"])){	
					$reference = array();
					foreach ($el["reference"]["costum"] as $kr => $vr) {
                        if($vr != $oldSlug){
                            array_push($reference,$vr);
						}
                    }
					array_push($reference,$newSlug);
					$set["reference.costum"] = $reference;
				}
				PHDB::update( $col,
					[ "_id" => new MongoId($kEl) ], 
					[ '$set' => $set ]);
			}
		}
	}


	public static function getInfoDetail($params, $element, $type, $id){
		//$params["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $type, $id);
		$params["openEdition"] = Authorisation::isOpenEdition($id, $type, @$element["preferences"]);
		$params["controller"] = self::getControlerByCollection($type);
		if($type==Person::COLLECTION && !@$element["links"]){
			$fields=array("links");
			$links=Element::getElementSimpleById($id,$type,null,$fields);
			$links=@$links["links"];
		}
		else
			$links=@$element["links"];
		$connectType = @self::$connectTypes[$type];
		if(((!@$links[$connectType][Yii::app()->session["userId"]] && $type!=Event::COLLECTION) || (@$links[$connectType][Yii::app()->session["userId"]] && 
			@$links[$connectType][Yii::app()->session["userId"]][Link::TO_BE_VALIDATED])) && 
			@Yii::app()->session["userId"] && 
			($type != Person::COLLECTION || 
			(string)$element["_id"] != Yii::app()->session["userId"])){
				$params["linksBtn"]["followBtn"]=true;	
				if (@$links["followers"][Yii::app()->session["userId"]])
					$params["linksBtn"]["isFollowing"]=true;
				else if(!@$links["followers"][Yii::app()->session["userId"]] && 
						$type != Event::COLLECTION)   
					$params["linksBtn"]["isFollowing"]=false;       
		}
        if(Utils::isActivitypubEnabled()){
            if(Utils::isFediverseShareLocal($element)){
            if (@$links["activitypub"]) {
                $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
                if(!$actor) return;
                if (@$links["activitypub"]['followers']) {
                    $exists = array_reduce($links["activitypub"]['followers'], function ($carry, $item) use ($actor) {
                        return $carry || $item['invitorId'] == $actor->get('id');
                    }, false);
                    $params["linksBtn"]["isFollowing"] = $exists;
                }
            }
        }
	}
			
		$connectAs = @self::$connectAs[$type];
            
		$params["linksBtn"]["connectAs"]=$connectAs;
		$params["linksBtn"]["connectType"]=$connectType;
		// Si le user n'appartient à la communauté forte de l'élément ex : links.members, links.contributors, links.attendees, links.friends
			if( isset(Yii::app()->session["userId"]) && !Utils::checkIsContainLinks($links,$connectType)){
				$params["linksBtn"]["communityBn"]=true;                    
				$params["linksBtn"]["isMember"]=false;
			}else if(isset(Yii::app()->session["userId"])){
				//Ask Admin button
                if(Utils::isActivitypubEnabled()) {
                    $connectAs = "admin";
                    $params["linksBtn"]["communityBn"] = true;
                    $params["linksBtn"]["isMember"] = true;
                    if (Utils::isFediverseShareLocal($element)) {
                        if (Utils::checkByLinkType($links, $connectType, Link::TO_BE_VALIDATED))
                            $params["linksBtn"][Link::TO_BE_VALIDATED] = true;
                        if (Utils::checkByLinkType($links, $connectType, Link::IS_INVITING)) {
                            $params["linksBtn"][Link::IS_INVITING] = true;
                            $params["invitedMe"] = array(
                                "invitorId" => Utils::getLinksInfo($links, $connectType)["invitorId"],
                                "invitorName" => Utils::getLinksInfo($links, $connectType)["invitorName"]);
                        }
                        if (Utils::checkByLinkType($links, $connectType, Link::IS_ADMIN_INVITING)) {
                            $params["linksBtn"][Link::IS_ADMIN_INVITING] = true;
                            $params["invitedMe"] = array(
                                "invitorId" => Utils::getLinksInfo($links, $connectType)["invitorId"],
                                "invitorName" => Utils::getLinksInfo($links, $connectType)["invitorName"],
                                "isAdminInviting" => true);
                        }

                    }
                    $params["linksBtn"]["isAdmin"] = true;

                    if (Utils::isFediverseShareLocal($element)) {
                        if (Utils::checkByLinkType($links, $connectType, Link::IS_ADMIN_PENDING))
                            $params["linksBtn"][Link::IS_ADMIN_PENDING] = true;
                    }
                    //Test if user has already asked to become an admin

                    if (!in_array(Yii::app()->session["userId"], Authorisation::listAdmins($id, $type, true))) {
                        if (Utils::checkByLinkType($links, $connectType, Link::IS_ADMIN)) {
                            $params["linksBtn"]["isAdmin"] = true;
                        } else {
                            $params["linksBtn"]["isAdmin"] = false;
                        }
                    }
                }else{
                    $connectAs="admin";
                    $params["linksBtn"]["communityBn"]=true;
                    $params["linksBtn"]["isMember"]=true;
                    if(isset($links[$connectType][Yii::app()->session["userId"]][Link::TO_BE_VALIDATED]))
                        $params["linksBtn"][Link::TO_BE_VALIDATED]=true;
                    if(isset($links[$connectType][Yii::app()->session["userId"]][Link::IS_INVITING])){
                        $params["linksBtn"][Link::IS_INVITING]=true;
                        $params["invitedMe"]=array(
                            "invitorId"=>$links[$connectType][Yii::app()->session["userId"]]["invitorId"],
                            "invitorName"=>$links[$connectType][Yii::app()->session["userId"]]["invitorName"]);
                    }
                    if(isset($links[$connectType][Yii::app()->session["userId"]][Link::IS_ADMIN_INVITING])){
                        $params["linksBtn"][Link::IS_ADMIN_INVITING]=true;
                        $params["invitedMe"]=array(
                            "invitorId"=>$links[$connectType][Yii::app()->session["userId"]]["invitorId"],
                            "invitorName"=>$links[$connectType][Yii::app()->session["userId"]]["invitorName"],
                            "isAdminInviting"=>true);
                    }
                    $params["linksBtn"]["isAdmin"]=true;
                    if(isset($links[$connectType][Yii::app()->session["userId"]][Link::IS_ADMIN_PENDING]))
                        $params["linksBtn"][Link::IS_ADMIN_PENDING]=true;
                    //Test if user has already asked to become an admin
                    if(!in_array(Yii::app()->session["userId"], Authorisation::listAdmins($id, $type,true)))
                        $params["linksBtn"]["isAdmin"]=false;
                }
			}

			$params["isLinked"] = Link::isLinked($id,$type, 
									Yii::app()->session['userId'], 
									@$element["links"]);

			if($params["isLinked"]==true)
				$params["countNotifElement"]=ActivityStream::countUnseenNotifications(Yii::app()->session["userId"], $type, $id);
			if($type==Event::COLLECTION){
				$params["countStrongLinks"]= @$attendeeNumber;
				//$params["countLowLinks"] = @$invitedNumber;
			}
			else{
				$params["countStrongLinks"]= @$countStrongLinks;
				$params["countLowLinks"] = count(@$element["links"]["followers"]);
			}
			$params["countInvitations"]=@$invitedNumber;
			//$params["countries"] = OpenData::getCountriesList();

			if(@$_POST["modeEdit"]){
				$params["modeEdit"]=$_POST["modeEdit"];
			}
            
			if(@$_GET["network"])
				$params["networkJson"]=Network::getNetworkJson($_GET["network"]);
		//}

		return $params;
	}


	public static function getElementForJS($element, $type = null) {
		$newElement = $element ;
		unset($newElement["modified"]);
		unset($newElement["modifiedByBatch"]);
		unset($newElement["roles"]);
		unset($newElement["two_step_register"]);
		unset($newElement["lastLoginDate"]);
		if(!empty($newElement["preferences"]) && (array_search("forbiddenAdminRequest",array_keys($newElement["preferences"])) || array_search("showQr",array_keys($newElement["preferences"])))){
			foreach($newElement["preferences"] as $kpref=>$vpref){
				if($kpref !== "forbiddenAdminRequest" && $kpref !== "showQr"){
					unset($newElement["preferences"][$kpref]);
				}
			}
		}else{
			unset($newElement["preferences"]);
		}

		/*if(in_array(@$element["type"],array_keys( Organization::$types) ) )
			$newElement["typeOrga"] = $element["type"] ;
		if(in_array(@$element["type"],array_keys( Event::$types) ) )
			$newElement["typeEvent"] = $element["type"] ;
		*/
	
		$newElement["fixe"] = (!empty($element["telephone"]["fixe"])) ? ArrayHelper::arrayToString($element["telephone"]["fixe"]) : "" ;
		$newElement["mobile"] = (!empty($element["telephone"]["mobile"])) ? ArrayHelper::arrayToString($element["telephone"]["mobile"]) : "" ;
		$newElement["fax"] = (!empty($element["telephone"]["fax"])) ? ArrayHelper::arrayToString($element["telephone"]["fax"]) : "" ;
		$newElement["id"] = @(String) $element["_id"] ;

		$newElement["typeElement"]=$element["type"] ?? null;

		if(!empty($type))
			$newElement["type"] = $type;


		if(!empty($element["properties"]["avancement"]))
			$newElement["avancement"] = $element["properties"]["avancement"];


		if($type == Person::COLLECTION){
			if(empty($newElement["socialNetwork"]))
				$newElement["socialNetwork"] = array();

			$sNetwork = array("telegram", "github", "gitlab", "twitter", "facebook", "gpplus", "instagram", "diaspora", "mastodon");
			foreach ($sNetwork as $key => $value) {
				if(empty($newElement["socialNetwork"][$value]))
					$newElement["socialNetwork"][$value] = "";
			}
		}
		

		return $newElement;
	}

	public static function getParamsOnepage($type, $id){
		$members=array();
		//$list = Lists::get(array("eventTypes"));
		$events=array();
		$projects=array();
		$needs=array();
		$elementAuthorizationId=$id;
		$elementAuthorizationType=$type;
		if($type != Person::COLLECTION){
			$listsToRetrieveOrga = array("public", "typeIntervention", "organisationTypes", "NGOCategories", "localBusinessCategories", "CooperativeCatgories");
			$listsOrga = Lists::get($listsToRetrieveOrga);

			$listsToRetrieveEvent = array("eventTypes");
			$listsEvent = Lists::get($listsToRetrieveEvent);
		}
		


		if($type == Organization::COLLECTION){
			$element = Organization::getById($id);
			if (empty($element)) throw new CHttpException(404,Yii::t("organization","The organization you are looking for has been moved or deleted !"));
			$params["listTypes"] = isset($listsOrga["organisationTypes"]) ? $listsOrga["organisationTypes"] : null;
			$params["public"] 			 = isset($listsOrga["public"]) 			  ? $listsOrga["public"] : null;
			$params["typeIntervention"]  = isset($listsOrga["typeIntervention"])  ? $listsOrga["typeIntervention"] : null;
			$params["NGOCategories"] 	 = isset($listsOrga["NGOCategories"]) 	  ? $listsOrga["NGOCategories"] : null;
			$params["CooperativeCatgories"] 	 = isset($listsOrga["CooperativeCatgories"]) 	  ? $listsOrga["CooperativeCatgories"] : null;
			$params["localBusinessCategories"] = isset($listsOrga["localBusinessCategories"]) ? $listsOrga["localBusinessCategories"] : null;
			$connectType = "members";
			
			
		} else if ($type == Project::COLLECTION){
			$element = Project::getById($id);
			if (empty($element)) throw new CHttpException(404,Yii::t("projet","The project you are looking for has been moved or deleted !"));
			$params["eventTypes"] = $listsEvent["eventTypes"];
			$params["listTypes"] = @$listsEvent["eventTypes"];
			$connectType = "contributors";
			// Link with events
			
		} else if ($type == Event::COLLECTION){
			$element = Event::getById($id);
			if (empty($element)) throw new CHttpException(404,Yii::t("event","The event you are looking for has been moved or deleted !"));
			$params["listTypes"] = $listsEvent["eventTypes"];
			$connectType = "attendees";
			$invitedNumber=0;
			$attendeeNumber=0;
			if(@$element["links"][$connectType]){
				foreach ($element["links"][$connectType] as $uid => $e) {
					if(@$e["invitorId"]){
		  				if(@Yii::app()->session["userId"] && $uid==Yii::app()->session["userId"])
		  					$params["invitedMe"]=array("invitorId"=>$e["invitorId"],"invitorName"=>$e["invitorName"]);
		  				$invitedNumber++;
			  		} else
	  					$attendeeNumber++;

				}
			}
			//EventOrganizer
			if(@$element["links"]["organizer"]){
				foreach ($element["links"]["organizer"] as $uid => $e) {
            		$organizer["type"] = $e["type"];
            		if($organizer["type"] == Project::COLLECTION ){
                		$iconNav="fa-lightbulb-o";
                		$urlType="project";
                		$organizerInfo = Project::getSimpleProjectById($uid);
                		$organizer["type"]=$urlType;
            		}
            		else if($organizer["type"] == Organization::COLLECTION ){
		                $iconNav="fa-group";
		                $urlType="organization";	
		                $organizerInfo = Organization::getSimpleOrganizationById($uid);  
						$organizer["type"]=$urlType;
						$organizer["typeOrga"]=@$organizerInfo["type"];              
            		}
					else{
						$iconNav="fa-user";
		                $urlType="person";	
		                $organizerInfo = Person::getSimpleUserById($uid);  
						$organizer["type"]=$urlType;
					}
            		$organizer["id"] = $uid;
            		$organizer["name"] = @$organizerInfo["name"];
            		$organizer["profilImageUrl"] = @$organizerInfo["profilImageUrl"];
            		$organizer["profilThumbImageUrl"] = @$organizerInfo["profilThumbImageUrl"];
          		}
		  		$params["organizer"] = $organizer;
              		
            }
			//events can have sub events
	        $params["subEvents"] = PHDB::find(Event::COLLECTION,array("parentId"=>$id));
	        $params["subEventsOrganiser"] = array();
	        $hasSubEvents = false;
	        if(@$params["subEvents"]){
	        	$hasSubEvents = true;
	        	foreach ($params["subEvents"] as $key => $value) {
	        		if( @$value["links"]["organizer"] ){
		        		foreach ($value["links"]["organizer"] as $key => $value) {
		        			if( !@$params["subEventsOrganiser"][$key])
		        				$params["subEventsOrganiser"][$key] = Element::getInfos( $value["type"], $key);
		        		}
	        		}
	        	}
	        }

		} else if ($type == Person::COLLECTION){
			$element = Person::getById($id);
			if (empty($element)) throw new CHttpException(404,Yii::t("person","The person you are looking for has been moved or deleted !"));
	
			$connectType = "attendees";
	
		} else if ($type == Poi::COLLECTION){
			$element = Poi::getById($id);
			if (empty($element)) throw new CHttpException(404,Yii::t("poi","The poi you are looking for has been moved or deleted !"));
			$connectType = "attendees";
			$elementAuthorizationId=$element["parentId"];
			$elementAuthorizationType=$element["parentType"];
			if($element["parentType"]==Organization::COLLECTION){
				$params["parent"] = Organization::getSimpleOrganizationById($element["parentId"]);
			}else{
				$params["parent"] = Project::getSimpleProjectById($element["parentId"]); 
			}
		
		}
		$params["controller"] = Element::getControlerByCollection($type);
		if(	@$element["links"] ) {
			if(isset($element["links"][$connectType])){
				$countStrongLinks=0;//count($element["links"][$connectType]);
				$nbMembers=0;
				$invitedNumber=0;
				foreach ($element["links"][$connectType] as $key => $aMember) {
					if($nbMembers < 11){
						if($aMember["type"]==Organization::COLLECTION){
							$newOrga = Organization::getSimpleOrganizationById($key);
							if(!empty($newOrga)){
								if ($aMember["type"] == Organization::COLLECTION && @$aMember["isAdmin"]){
									$newOrga["isAdmin"]=true;  				
								}
								$newOrga["type"]=Organization::COLLECTION;
								//array_push($contextMap["organizations"], $newOrga);
								//array_push($members, $newOrga);
								$members[$key] = $newOrga ;
							}
						} else if($aMember["type"]==Person::COLLECTION){
							//if(!@$aMember["isInviting"]){
								$newCitoyen = Person::getSimpleUserById($key);
								if (!empty($newCitoyen)) {
									if (@$aMember["type"] == Person::COLLECTION) {
										if(@$aMember["isAdmin"]){
											if(@$aMember["isAdminPending"])
												$newCitoyen["isAdminPending"]=true;  
												$newCitoyen["isAdmin"]=true;  	
										}			
										if(@$aMember["toBeValidated"]){
											$newCitoyen["toBeValidated"]=true;  
										}
										if(@$aMember["isInviting"]){
											$newCitoyen["isInviting"]=true;
										}
										if(@$aMember["isAdminInviting"]){
											$newCitoyen["isAdminInviting"]=true;
										}		
					  				
									}
									$newCitoyen["type"]=Person::COLLECTION;
									//array_push($contextMap["people"], $newCitoyen);
									//array_push($members, $newCitoyen);
									$members[$key] = $newCitoyen ;
									$nbMembers++;
								}
							//}
						}
					} 
					if(!@$aMember["isInviting"]){
						$countStrongLinks++;
						if(@$aMember["isAdminInviting"] && @Yii::app()->session["userId"] && $key==Yii::app()->session["userId"])
		  					$params["invitedMe"]=array("invitorId"=>$aMember["invitorId"],"invitorName"=>$aMember["invitorName"],"admin"=>true);
					}
					else{
		  				if(@Yii::app()->session["userId"] && $key==Yii::app()->session["userId"])
		  					$params["invitedMe"]=array("invitorId"=>$aMember["invitorId"],"invitorName"=>$aMember["invitorName"]);
						$invitedNumber++;
					}
					//else {
						//break;
					//}
				}
			}
		}
		if(!@$element["disabled"]){
	        //if((@$config["connectLink"] && $config["connectLink"]) || empty($config)){ TODO CONFIG MUTUALIZE WITH NETWORK AND OTHER PLATFORM
        	if((!@$element["links"][$connectType][Yii::app()->session["userId"]] || (@$element["links"][$connectType][Yii::app()->session["userId"]] && @$element["links"][$connectType][Yii::app()->session["userId"]][Link::TO_BE_VALIDATED])) && @Yii ::app()->session["userId"] && ($type != Person::COLLECTION || $element["_id"] != Yii::app()->session["userId"])){
        		$params["linksBtn"]["followBtn"]=true;
                if (@$element["links"]["followers"][Yii::app()->session["userId"]])
                    	$params["linksBtn"]["isFollowing"]=true;
                 else if(!@$element["links"]["followers"][Yii::app()->session["userId"]]     
                    && $type != Event::COLLECTION)   
                    	$params["linksBtn"]["isFollowing"]=false; 	               
            }
            // Add member , contributor, attendee
            if($type == Organization::COLLECTION)
               $connectAs="member";
            else if($type == Project::COLLECTION)
                $connectAs="contributor";
            else if($type == Event::COLLECTION)
                $connectAs="attendee";
            else if($type==Person::COLLECTION)
            	$connectAs="friend";
           $params["linksBtn"]["connectAs"]=$connectAs;
           $params["linksBtn"]["connectType"]=$connectType;
            if( @Yii::app()->session["userId"] && $type!= Person::COLLECTION && !@$element["links"][$connectType][Yii::app()->session["userId"]]){
            	$params["linksBtn"]["communityBn"]=true;	            	
            	$params["linksBtn"]["isMember"]=false;
            }else if($type != Person::COLLECTION  && @Yii::app()->session["userId"]){
                //Ask Admin button
                $connectAs="admin";
                $params["linksBtn"]["communityBn"]=true;
               	$params["linksBtn"]["isMember"]=true;
               	if(@$element["links"][$connectType][Yii::app()->session["userId"]][Link::TO_BE_VALIDATED])
               		$params["linksBtn"][Link::TO_BE_VALIDATED]=true;
               	$params["linksBtn"]["isAdmin"]=true;
               	if(@$element["links"][$connectType][Yii::app()->session["userId"]][Link::IS_ADMIN_PENDING])
               		$params["linksBtn"][Link::IS_ADMIN_PENDING]=true;


				
                //Test if user has already asked to become an admin
                if(!in_array(Yii::app()->session["userId"], Authorisation::listAdmins($id, $type,true)))
                	$params["linksBtn"]["isAdmin"]=false;              
            }
        }

        // Link with projects
		if(isset($element["links"]["projects"])){
			foreach ($element["links"]["projects"] as $keyProj => $valueProj) {
				 $project = Project::getPublicData($keyProj);
	       		 $projects[$keyProj] = $project;
			}
		}
		
	

		// Link with events
		if(isset($element["links"]["events"])){
			foreach ($element["links"]["events"] as $keyEv => $valueEv) {
				 $event = Event::getSimpleEventById($keyEv);
				 if(!empty($event) && (@$event["endDateSec"] > time() || @$event["startDateSec"] > time()))
           		 	$events[$keyEv] = $event;
			}
		}
		//$lists = Lists::get($listsToRetrieve);
		//$params["eventTypes"] = $list["eventTypes"];
		//$params["subview"]=$view;
		$params["tags"] = array("TODO : écrire la liste de suggestion de tags"); Tags::getActiveTags();
		$params["element"] = $element;
		$params["members"] = $members;
		$params["type"] = $type;
		$params["events"]=$events;
		$params["projects"]=$projects;
		$params["needs"]=$needs;
		$params["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $elementAuthorizationType, $elementAuthorizationId);
		$params["openEdition"] = Authorisation::isOpenEdition($elementAuthorizationId, $elementAuthorizationType, @$element["preferences"]);
		if(@Yii::app()->session["network"]){
			$params["openEdition"] = false;
			$params["edit"] = false;
		}

		$params["isLinked"] = Link::isLinked($elementAuthorizationId,$elementAuthorizationType, 
									Yii::app()->session['userId'], 
									@$element["links"]);

		if($params["isLinked"]==true)
			$params["countNotifElement"]=ActivityStream::countUnseenNotifications(Yii::app()->session["userId"], $elementAuthorizationType, $elementAuthorizationId);
		if($type==Event::COLLECTION){
			$params["countStrongLinks"]= @$attendeeNumber;
			//$params["countLowLinks"] = @$invitedNumber;
		}
		else{
			$params["countStrongLinks"]= @$countStrongLinks;
			$params["countLowLinks"] = count(@$element["links"]["followers"]);
		}
		$params["countInvitations"]=@$invitedNumber;
		$params["countries"] = OpenData::getCountriesList();

		if(@$_POST["modeEdit"]){
			$params["modeEdit"]=$_POST["modeEdit"];
		}

		//manage delete in progress status
		$params["deletePending"] = Notification::isElementStatusDeletePending($type, $id);
		
		
		return $params;
	}

	public static function getUrlMyNetwork($id, $type) {
		$server = ((isset($_SERVER['HTTPS']) AND (!empty($_SERVER['HTTPS'])) AND strtolower($_SERVER['HTTPS'])!='off') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];
		$jsonNetwork = $server.Yii::app()->createUrl("network/default/index/id/".$id."/type/".$type);
		$urlNetwork = $server.Yii::app()->createUrl("/?network=".$jsonNetwork);

		return $urlNetwork;
    }



	public static function myNetwork($id, $type){
        $myN = json_decode(file_get_contents("../../modules/co2/data/myNetwork.json", FILE_USE_INCLUDE_PATH), true);
        if($type == Person::COLLECTION || $type == Organization::COLLECTION || $type == Event::COLLECTION || $type == Project::COLLECTION){
            $myN["request"]["sourceKey"][0] = $id."@".$type ;

        }

        return $myN;
    }


 	public static function getHash($element){
 		$id = @$element["id"] ? @$element["id"] : @$element["_id"];
 		return  (@$element["slug"]) ? 
				"#".$element["slug"] : 
				"#page.type.".@$element["type"].".id.".$id;
 	}



 	public static function getByArrayId($type, $valLink, $fieldsPer){
 		if( !empty($valLink) ) {
			foreach ($valLink as $type => $valLink) {
				$contactsComplet = null;
				if($type == Person::COLLECTION)
					$contactsComplet = Person::getByArrayId($valLink, $fieldsPer, true, true); 
				if($type == Organization::COLLECTION)
					$contactsComplet = Organization::getByArrayId($valLink, $fieldsOrg, true);
				if($type == Project::COLLECTION)
					$contactsComplet = Project::getByArrayId($valLink, $fieldsPro, true);
				if($type == Event::COLLECTION)
					$contactsComplet = Event::getByArrayId($valLink, $fieldEve, true);

				if(!empty($contactsComplet))
					$contextMap = array_merge($contextMap, $contactsComplet);					
			}
		}
 	}


 	public static function getDataByAsk($id = null, $email = null) {
 		$result = array("result" => false);
 		if(!empty($id)){
			$ask = PHDB::findOneById(Cron::ASK_COLLECTION, $id);
		}else if(!empty($email)) {
			$ask["email"] = $email;
		}

		if(!empty($ask)){
			$where = array( "email" => $ask["email"] );
			$person = PHDB::findOne(Person::COLLECTION, $where);

			$res["person"] = $person ;
			$col = array(
				Organization::COLLECTION,
				Project::COLLECTION,
				Event::COLLECTION,
				Poi::COLLECTION,
				Form::COLLECTION,
				Form::ANSWER_COLLECTION,
				Survey::COLLECTION,
				Proposal::COLLECTION,
				Classified::COLLECTION,
				Action::COLLECTION,
			);
			$eltsReferenceEmail = array() ;
			$eltsCreator = array() ;
			$eltsContacts = array() ;
			foreach ($col as $k => $v) {
				$where = array( "email" => $ask["email"]);
				$resElt = PHDB::find($v, $where, array("name", "email"));

				if(!empty($resElt))
					$eltsReferenceEmail[$v] = $resElt;

				if(!empty($person)){
					$resElt2 = PHDB::find($v, array("creator" => (String)$person["_id"] ), array("name", "email"));
					if(!empty($resElt2))
						$eltsCreator[$v] = $resElt2;
				}

				if(!empty($person)){
					$where3 = array('$or'=>array(
						array("contacts.email" => $ask["email"] ),
						array("contacts.id" => (String)$person["_id"] )
					));
				}else{
					$where3 = array('$or'=>array(
						array("contacts.email" => $ask["email"] )
					));
				}
				
				
				$resElt3 = PHDB::find($v, $where3 , array("name", "contacts"));
				if(!empty($resElt3)){
					
					foreach ($resElt3 as $key3 => $val3) {
						$c = array();
						if(!empty($val3["contacts"])){
							foreach ($val3["contacts"] as $keyC => $valC) {
								if( (!empty($valC["email"]) && $valC["email"] == $ask["email"]) ||
									(!empty($person) && !empty($valC["id"]) && $valC["id"] == (String)$person["_id"])	){
									$c[] = $valC;
								}
							}
						}
						
						$resElt3[$key3]["contacts"] = $c;
					}
					$eltsContacts[$v] = $resElt3;
				}
				
			}
			
			$res["eltsReferenceEmail"] = $eltsReferenceEmail ;
			$res["eltsCreator"] = $eltsCreator ;
			$res["eltsContacts"] = $eltsContacts ;

			$notsendmail = PHDB::find( ActivityStream::COLLECTION, 
											array(	"target.email" => $ask["email"],
													"verb" => ActStr::VERB_NOSENDING ));
			$res["notsendmail"] = $notsendmail ;

			if(!empty($person)){
				$activityStream = PHDB::find( ActivityStream::COLLECTION, 
											array('$or'=>array(
												array("target.email" => $ask["email"]),
												array("author.".(String)$person["_id"] => array('$exists' => true) ),
												array("object.".(String)$person["_id"] => array('$exists' => true) ),
												array('$and'=>array(
														array("target.type" => Person::COLLECTION),
														array("target.id" =>(String)$person["_id"])))
											)));
				$res["activityStream"] = $activityStream ;

				$news = PHDB::find( News::COLLECTION, 
												array('$or'=>array(
													array("author" => (String)$person["_id"] ),
													array('$and'=>array(
															array("target.type" => Person::COLLECTION),
															array("target.id" =>(String)$person["_id"])))
												)));
				$res["news"] = $news ;

				$comments = PHDB::find( Comment::COLLECTION, 
												array("author" => (String)$person["_id"] ));
				$res["comments"] = $comments ;
			}
			
			
			$res["result"] = true ;
			$result = array();
			foreach ($res as $key => $value) {
				if(!empty($value))
					$result[$key] = $value ;
			}
		}

		return $result;
 	}

 	public static function getSubEvent($element, $sub, $maxSub, $context, $arrayIdEvents=array()) {

 		if($sub <= $maxSub && isset($element["links"]["projects"])){
 			$sub++;
 			//Rest::json($element["links"]["projects"]); exit;
			foreach ($element["links"]["projects"] as $keyElt => $valueElt) {
				$elt = PHDB::findOneById(Project::COLLECTION, $keyElt, array("name", "links"));
				// var_dump($keyElt);
				if(isset($elt["links"]["events"])){
					// Rest::json("HERE");
					foreach (array_reverse($elt["links"]["events"]) as $keyEv => $valueEv) {

						$arrayIdEvents[] = new MongoId($keyEv) ;
						
					}
				}
				
				$arrayIdEvents = self::getSubEvent($elt, $sub, $maxSub, $context, $arrayIdEvents);
			}
		
		}
		
		return $arrayIdEvents;
 	}

 	public static function getLastEvents($col, $id, $nbEvent, $startDateUTC, $tags) {

 		$element = PHDB::findOneById($col, $id, array("name","slug", "links", "profilMediumImageUrl") );
 		$listType = array("projects");
 		$query = array(
 			array("links.organizer.".$id => array('$exists' => 1) ),
 			array("organizer.".$id => array('$exists' => 1) )

 		) ;
 		if(!empty($element["links"])){
 			foreach ($listType as $keyT => $valT) {
	 			if(!empty($element["links"][$valT])){
		 			foreach ($element["links"][$valT] as $keyElt => $valueElt) {
			 			$query[] = array("links.organizer.".$keyElt => array('$exists' => 1) );
			 			$query[] = array("organizer.".$keyElt => array('$exists' => 1) );
				 		
			 		}
		 		}
	 		}
 		}
 		$queryRes = array('$or' => $query );
 		if(!empty($tags)){
 			
 			$queryRes=array(
				'$and' =>array(
					$queryRes,
					array(
						"tags" => array( '$in' => $tags )
					)
				)
			);
 		}

 		
		$date1 = new DateTime($startDateUTC);
		$startD = $date1->getTimestamp();
		$queryFinal=array(
			'$and' =>array(
				$queryRes,
				array(
					"startDate" => array( '$gte' => new MongoDate( (float)$startD ))
				)
			)
		);
 		$events = PHDB::findAndSort( Event::COLLECTION, $queryFinal, array("startDate" => 1), $nbEvent, array("name","slug", "links","startDate", "endDate", "profilMediumImageUrl", "address", "tags"));
		return $events;
 	}

 	public static function getBySourceTagsAndZone($collection, $searchByTags=array(), $mainTag=null, $levelNb=null, $levelId=null, $costumSlug=null, $extraQuery=array()){
 		//var_dump($searchByTags..$levelNb.$levelId.$costumSlug);
 		// var_dump($searchByTags);
 		// var_dump($mainTag);
 		// var_dump($levelNb);
 		// var_dump($levelId);
 		// var_dump($costumSlug);
 		//$whereT=array();
 		// $searchByTags = isset($searchByTags) ? $searchByTags : array();
		$where = array();
		$whereT=array();
		if(!empty($searchByTags)){
			// $whereT = array('$and' => array(
			// 							array("tags"=> array('$in' => $searchByTags)),
			// 							array('$or' => array(
			// 								array("tags" => $mainTag),
			// 								array("mainTag" => $mainTag)
			// 							))	
			// 						)); 
			$whereT = array("tags"=> array('$in' => $searchByTags)); 

		}
		if(!empty($mainTag)){
			$whereMT = array('$or' => array(
										array("tags" => $mainTag),
										array("mainTag" => $mainTag)
									));	
		}

		//var_dump($whereT);
		if (isset($costumSlug) && !empty($costumSlug)) {
			$whereK = array('$or' => array(
									array("source.keys"=> $costumSlug),
									array("reference.costum" => $costumSlug) 	
								));
		}

		// Condition $whereMT à ajouter au besoin
		
		$whereOr = (!empty($whereT)) ? array('$and'=>array($whereT,$whereK)) : $whereK;

		

		if(isset($levelNb) && !empty($levelNb) && isset($levelId) && !empty($levelId)){
			$whereA=array("address.level".$levelNb => $levelId);
		}

		$whereP= array('$or'=>array(array("preferences.isOpenData" => 1),
									array("preferences.isOpenData" => "true")
								)
					);

		// var_dump($where);exit;
		$where = array('$and'=>array($whereOr,$whereA));
		// var_dump($where);
		
		$list = PHDB::find( $collection, $where );
		//var_dump($list);exit;
	   	return $list;
	}

	public static function checkDoublonInCollection($collection, $name = "", $source="") {
		$aggregate= array(
			array( '$group' => array("_id" => '$name', 'count'  => array('$sum' => 1)) ), //
			array( '$match' => array("_id" => array('$ne'  =>  null), 'count'  => array('$gt' => 1)) ),
			array( '$project' => array("name" => '$_id', '_id'  => 0, "count" => '$count') ),
		);

		if($name != "")
			$aggregate[1]= array( '$match' => array("_id" => array('$ne'  =>  null), 'count'  => array('$gt' => 1), "_id" => new MongoDB\BSON\Regex($name, 'i')) );
		
		if($source != ""){
			$aggregate= array(
				array( '$group' => array("_id" => array('name'  => '$name', 'key'  => '$source.key'), 'count'  => array('$sum' => 1)) ), //
				array( '$match' => array("_id.name" => array('$ne'  =>  null), 'count'  => array('$gt' => 1), '_id.key'  => new MongoDB\BSON\Regex($source, "i"))),
				array( '$project' => array("name" => '$_id.name', '_id'  => 0, "count" => '$count') ),
			);

			if($name != "")
				$aggregate[1]= array( '$match' => array("_id.name" => array('$ne'  =>  null), 'count'  => array('$gt' => 1), '_id.key'  => new MongoDB\BSON\Regex($source, "i") , "_id.name" => new MongoDB\BSON\Regex($name, 'i')));
		}
		
		/* else{
			$aggregate= array(
				array( '$group' => array("_id" => '$name', 'count'  => array('$sum' => 1)) ), //
				array( '$match' => array("_id" => array('$ne'  =>  null), 'count'  => array('$gt' => 1), "_id" => new MongoDB\BSON\Regex($name, 'i')) ),
				array( '$project' => array("name" => '$_id', '_id'  => 0, "count" => '$count') ),
			);
		} */
		
		
		$result = PHDB::aggregate($collection, $aggregate);
		
		return $result;
	}


	public static function checkAllDoublon($name = "", $source="") {

		$col = Self::getAllElementToDoublons();
		$result = array();
		
		foreach ($col as $key => $collection) {
			$result[$collection] = Element::checkDoublonInCollection($collection, $name, $source);
		}
		
		return $result;
	}

	public static function getAllElementToDoublons(){
		return array(
			Organization::COLLECTION,
			Project::COLLECTION,
			Event::COLLECTION,
			Poi::COLLECTION,
			/* Form::COLLECTION,
			Form::ANSWER_COLLECTION, */
			Survey::COLLECTION,
			Proposal::COLLECTION,
			Classified::COLLECTION,
			//Action::COLLECTION,
		);
	}

	public static function merge($elementType, $arrayToMerge, $arrayToDelete){
		if ( !Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) {
			return Rest::json( array( "result" => false, "msg" => "You must be logged as an Superadmin user to do this action !" ));
		}
		
		$linksTypes = array(
			Person::COLLECTION => 
				array(	"followers" => "follow", 
						"members" => "memberOf",
						"follow" => "followers",
						"attendees" => "events",
						"helpers" => "needs",
						"contributors" => "projects"),
			Organization::COLLECTION => 
				array(	"memberOf" => "members",
						"members" =>"memberOf",
						"follows" => "followers",
						"contributors" => "projects"),
			Event::COLLECTION => 
				array("events" => "organizer"),
			Project::COLLECTION =>
				array("projects" => "contributors"),

			"answers" =>  
				array("answers" =>  "answers"),
			
		);
		
		if (isset($arrayToDelete["links"])) {
			foreach ($arrayToDelete["links"] as $linkType => $aLink) {
				foreach ($aLink as $linkElementId => $linkInfo) {
					$linkElementType = $linkInfo["type"];
					if (!isset($linksTypes[$linkElementType][$linkType])) {
						error_log("Unknown backward links for a link in a ".$elementType." of type ".$linkType." to a ".$linkElementType);
						continue;
					}
					$linkToUpdate = $linksTypes[$linkElementType][$linkType];
						
					$collection = $linkElementType;
					if ($collection == Event::COLLECTION) array_push($listEventsId, new MongoId($linkElementId));
					if ($collection == Project::COLLECTION) array_push($listProjectId, new MongoId($linkElementId));

					$valueInlinks =  PHDB::find($collection, ['links.'.$linkToUpdate.".".$arrayToDelete["_id"] => array('$exists'=>1)]);

					if(!empty($valueInlinks)){
						$where = array("_id" => new MongoId($linkElementId));
						$action = array();
						
						if(!isset($valueInlinks[$linkElementId]["links"][$linkToUpdate][(string) $arrayToMerge["_id"]]))
							$action['$set'] = array('links.'.$linkToUpdate.".".$arrayToMerge["_id"] => $valueInlinks[$linkElementId]["links"][$linkToUpdate][(string) $arrayToDelete["_id"]]);
						
						$action['$unset'] = array('links.'.$linkToUpdate.".".$arrayToDelete["_id"] => "");
						
						$result = PHDB::update($collection, $where, $action);
					}
					
				}
			}
		}
		$where = array("_id" => $arrayToMerge["_id"]);
        $action= array();

		unset($arrayToMerge["_id"]);
		$unset = Self::getPathToUnsetInArray($arrayToMerge);;

		$set= array();
		$set['$set']=  $arrayToMerge;

		$res= PHDB::update($elementType, $where, $set);

		if(!empty($unset))
			$res= PHDB::update($elementType, $where, $unset);
		
		
		return $res;
	}

	public static function getPathToUnsetInArray($array){// for merge
		$result= Self::nested_values($array);
		$unset = array();
		$action = array();

		foreach($result as $k => $v ){
			if(is_string($v) && $v==='$unset'){
				$unset[$k]="";
			}
		}
		if(!empty($unset))
            $action['$unset']=$unset;

		return $action;
	}

	public static function getSet($stringPathWithoutUnset){ // for merge
		$output = array();
		foreach($stringPathWithoutUnset as $key => $value) {
			//$loop = 0;
			$explode = explode(".",$key);
			$count = count($explode);
			if($count == 0){}
			else if($count == 1) $output[$explode[0]] = $value;
			else if($count == 2) $output[$explode[0]][$explode[1]] = $value;
			else if($count == 3) $output[$explode[0]][$explode[1]][$explode[2]] = $value;
			else if($count == 4) $output[$explode[0]][$explode[1]][$explode[2]][$explode[3]] = $value;
			else if($count == 5) $output[$explode[0]][$explode[1]][$explode[2]][$explode[3]][$explode[4]] = $value;
			else if($count == 6) $output[$explode[0]][$explode[1]][$explode[2]][$explode[3]][$explode[4]][$explode[6]] = $value;
			else if($count == 7) $output[$explode[0]][$explode[1]][$explode[2]][$explode[3]][$explode[4]][$explode[6]][$explode[7]] = $value;
			else if($count == 8) $output[$explode[0]][$explode[1]][$explode[2]][$explode[3]][$explode[4]][$explode[6]][$explode[7]][$explode[8]] = $value;
			/* $first = array_shift($explode);
			if(!isset($output[$first]))
					$output[$first] =  Self::nested_value_set($explode, $value);
			else{
				$tmp = $output[$first];
				while(true && (is_array($tmp) || is_object($tmp))){
					$first = array_shift($explode);
					if(!isset($tmp[$first])){
						$tmp[$first] =  Self::nested_value_set($explode, $value);
						break;
					}	
					else{
						$tmp = $tmp[$first];
						$loop++;
					}

				}
			} */
			
			
		}
		
		return $output;
	}

	public static function nested_value_set($arrayPath, $value){// for merge
		$output = array();
		if(!isset($arrayPath[1]) && isset($arrayPath[0]) && is_array($arrayPath)){
			$output[$arrayPath[0]] =  $value;
		}
		else if(is_array($arrayPath) && isset($arrayPath[0]) ){
			$tmp = $arrayPath;
			array_shift($arrayPath);
			$output[$tmp[0]] =  array_merge($output, Self::nested_value_set($arrayPath, $value));
			unset($tmp);
		}
		else
			$output =  $value;

		return $output;
	}

	public static function nested_values($array,$path=""){// for merge
		$output = array();
		foreach($array as $key => $value) {
			if(is_array($value)) {
				$output = array_merge($output, Self::nested_values($value, (!empty($path)) ? $path.$key."." : $key."."));
			}
			else $output[$path.$key] = $value;
		}
		return $output;
	}

	public static function getCreatorElement($elementType, $elementId){
		$creator =null;
		
		$element = self::getByTypeAndId( $elementType, $elementId );

		if (isset($element["creator"])) {
			$creator = array();
			$creator[$element["creator"]] = self::getByTypeAndId( Citoyen::COLLECTION, $element["creator"]);
		}
		
		else if($elementType == Event::COLLECTION && isset($element["links"]) && isset($element["links"]["creator"]) ){
			$id = "";
			$creatorElement = "";
			foreach($element["links"]["creator"] as $idCreator => $infoCreatorInLink){
				$id =$idCreator;
				$creatorElement = self::getByTypeAndId( Citoyen::COLLECTION, $idCreator);
			}
			$creator[$id] = $creatorElement;
		}

		else if($elementType == Project::COLLECTION && isset($element["links"]) && isset($element["links"]["organizer"]) ){
			$id = "";
			$creatorElement = "";
			foreach($element["links"]["organizer"] as $idCreator => $infoCreatorInLink){
				$id =$idCreator;
				$creatorElement = self::getByTypeAndId( Citoyen::COLLECTION, $idCreator);
			}
			$creator[$id] = $creatorElement;
		}

		else if($elementType == News::COLLECTION && isset($element["author"]) ){
			$creator[$element["author"]] = self::getByTypeAndId( Citoyen::COLLECTION, $element["author"]);
		}

		return $creator;
	}

	public static function analyseSpamElement($elementType, $elementId){
		$result = array();
		
		$linksTypes = array(
			Person::COLLECTION => 
				array(	"followers" => "follow", 
						"members" => "memberOf",
						"follow" => "followers",
						"attendees" => "events",
						"helpers" => "needs",
						"contributors" => "projects"),
			Organization::COLLECTION => 
				array(	"memberOf" => "members",
						"members" =>"memberOf",
						"follows" => "followers",
						"contributors" => "projects"),
			Event::COLLECTION => 
				array("events" => "organizer"),
			Project::COLLECTION =>
				array("projects" => "contributors"),

			"answers" =>  
				array("answers" =>  "answers"),
			
		);
		
		$elementToDelete = self::getByTypeAndId( $elementType, $elementId );

    	$profilImages = Document::listMyDocumentByIdAndType($elementId, $elementType, Document::IMG_PROFIL, Document::DOC_TYPE_IMAGE, array( 'created' => -1 ));
    	if(count($profilImages) > 0){
			$result["document"] = array();
			$result["document"]["count"]  = count($profilImages);
			foreach ($profilImages as $docId => $document) {
				if(isset($document["name"]) && isset($document["path"]))
					$result["document"][$document["name"]] = $document["path"];
				else
					$result["document"][$docId] = "";
			}
		}

		$whereActivityStream = [
			"target.id"=>$elementId, 
			"target.type"=>$elementType, 
		];
		$activity = ActivityStream::getWhere($whereActivityStream);
		if(count($activity) > 0){
			$result["activityStream"] = array();
			$result["activityStream"]["count"]  = count($activity);
			foreach ($activity as $actId => $act) {
				$result["activityStream"][$actId] = "";
			}
		}

		$whereANew = [
			"target.id"=>$elementId, 
			"object.id"=>$elementType, 
		];
		$new = News::getWhere($whereANew);
		if(count($new) > 0){
			$result["New"] = array();
			$result["New"]["count"]  = count($new);
			foreach ($new as $newId => $val) {
				$result["New"][$newId] = "";
			}
		}

		$room = Room::getElementActionRooms($elementId, $elementType);
		if(count($room) > 0){
			$result["Room"] = array();
			$result["Room"]["count"]  = count($room);
			foreach ($room as $roomId => $val) {
				if(isset($val["name"]) && isset($val["path"]))
					$result["Room"][$val["name"]] = "";
				else
					$result["Room"][$roomId] = "";
			}
		}

			
		$listEventsId = array();
		$listProjectId = array();
		
		if (isset($elementToDelete["links"])) {
			$result["links"] = array();
			$result["links"]["count"] = 0;

			foreach ($elementToDelete["links"] as $linkType => $aLink) {
				$result["links"]["count"] += count($aLink);
				
				$result["links"][$linkType] = array();
				$result["links"][$linkType]["count"] = 0;
				foreach ($aLink as $linkElementId => $linkInfo) {
					$result["links"][$linkType]["count"] += 1;
					

					$linkElementType = $linkInfo["type"];
					if (!isset($linksTypes[$linkElementType][$linkType])) {
						//error_log(print_r(@$linksTypes[$linkElementType]));
						error_log("Unknown backward links for a link in a ".$elementType." of type ".$linkType." to a ".$linkElementType);
						continue;
					}
					$linkToDelete = $linksTypes[$linkElementType][$linkType];
					
					$collection = $linkElementType;

					$where = array("_id" => new MongoId($linkElementId));
					$collectionInLinks = PHDB::find($collection, $where);

					if(isset($collectionInLinks[$linkElementId]["name"]))
						$result["links"][$linkType][$collectionInLinks[$linkElementId]["name"]] = "";
					else
						$result["links"][$linkType][$linkElementId] = $collectionInLinks;

					
				}
			}
		}
		
		
		return $result;
	}


    public static function getCostumMsgNotAuthorized(){
        /* Get message d'autorisation s'il y a en dans un costum */
        $costum = CacheHelper::getCostum();

        $msgNotAuthorized = "";
        if(!isset($costum["msgNotAuthorized"])){
            $msgNotAuthorized = '<div class="col-lg-offset-1 col-lg-10 col-xs-12 margin-top-50 text-center text-red">	
                                    <i class="fa fa-lock fa-4x "></i><br/>
                                    <h1 class="">'.Yii::t("organization", "Unauthorized Access.").'</h1>
                                </div>';
        }else{
            $msgNotAuthorized = $costum["msgNotAuthorized"];
        }
        return $msgNotAuthorized;
    }
}