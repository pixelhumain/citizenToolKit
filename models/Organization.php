<?php
class Organization {

	const COLLECTION = "organizations";
	const CONTROLLER = "organization";
	const ICON = "fa-users";
	const ICON_BIZ = "fa-industry";
	const ICON_GROUP = "fa-circle-o";
	const ICON_GOV = "fa-university";
	const COLOR = "#93C020";

	const TYPE_NGO = "NGO";
	const TYPE_BUSINESS = "LocalBusiness";
	const TYPE_GROUP = "Group";
	const TYPE_GOV = "GovernmentOrganization";
	const TYPE_COOP = "Cooperative";

	public static $types = array(
		//"NGO" => Yii::t("common","NGO"),
		"NGO" => "NGO",
		"LocalBusiness" => "Local Business",
		"Group" => "Group",
		"GovernmentOrganization" => "Government Organization",
		"Cooperative" => "Cooperative"
	);

	//From Post/Form name to database field name
	//TODO : remove name   
	public static $dataBinding = array(
		"name" => array("name" => "name", "rules" => array("required", "organizationSameName")),
		"slug" => array("name" => "slug", "rules" => array("checkSlug")),
		"email" => array("name" => "email", "rules" => array("email")),
		"collection" => array("name" => "collection"),

		"referantName" => array("name" => "referantName"),
		"type" => array("name" => "type", "rules" => array("required", "typeOrganization")),
		"shortDescription" => array("name" => "shortDescription"),
		"description" => array("name" => "description"),
		"openingHours" => array("name" => "openingHours"),
		"category" => array("name" => "category"),
		"address" => array("name" => "address", "rules" => array("addressValid")),
		"sourceType" => array("name" => "source.type"),
		"source" => array("name" => "source"),
		"addresses" => array("name" => "addresses"),
		"streetAddress" => array("name" => "address.streetAddress"),
		"postalCode" => array("name" => "address.postalCode"),
		"city" => array("name" => "address.codeInsee"),
		"addressLocality" => array("name" => "address.addressLocality"),
		"addressCountry" => array("name" => "address.addressCountry"),
		"geo" => array("name" => "geo", "rules" => array("geoValid")),
		"geoPosition" => array("name" => "geoPosition", "rules" => array("geoPositionValid")),
		"tags" => array("name" => "tags"),
		"typeIntervention" => array("name" => "typeIntervention"),
		"typeOfPublic" => array("name" => "typeOfPublic"),
		"url" => array("name" => "url"),
		"telephone" => array("name" => "telephone"),
		"mobile" => array("name" => "telephone.mobile"),
		"fixe" => array("name" => "telephone.fixe"),
		"fax" => array("name" => "telephone.fax"),
		"modules" => array("name" => "modules"),
		"preferences" => array("name" => "preferences"),
		"video" => array("name" => "video"),
		"state" => array("name" => "state"),
		"warnings" => array("name" => "warnings"),
		"urlFacebook" => array("name" => "urlFacebook"),
		"urlTwitter" => array("name" => "urlTwitter"),
		"urlWiki" => array("name" => "urlWiki"),
		"isOpenData" => array("name" => "isOpenData"),
		"badges" => array("name" => "badges"),
		"source" => array("name" => "source", "rules" => array("source")),
		"role" => array("name" => "role"),
		"medias" => array("name" => "medias"),
		"urls" => array("name" => "urls"),
		"modified" => array("name" => "modified"),
		"updated" => array("name" => "updated"),
		"creator" => array("name" => "creator"),
		"created" => array("name" => "created"),
		"locality" => array("name" => "address"),
		"contacts" => array("name" => "contacts"),
		"urls" => array("name" => "urls"),
		"descriptionHTML" => array("name" => "descriptionHTML"),
		"socialNetwork" => array("name" => "socialNetwork"),
		"facebook" => array("name" => "socialNetwork.facebook"),
		"twitter" => array("name" => "socialNetwork.twitter"),
		"gpplus" => array("name" => "socialNetwork.googleplus"),
		"github" => array("name" => "socialNetwork.github"),
		"gitlab" => array("name" => "socialNetwork.gitlab"),
		"instagram" => array("name" => "socialNetwork.instagram"),
		"linkedin" => array("name" => "socialNetwork.linkedin"),
		"diaspora" => array("name" => "socialNetwork.diaspora"),
		"mastodon" => array("name" => "socialNetwork.mastodon"),
		"signal" => array("name" => "socialNetwork.signal"),
		"telegram" => array("name" => "socialNetwork.telegram"),
		"parent" => array("name" => "parent"),
		"parentId" => array("name" => "parentId"),
		"parentType" => array("name" => "parentType"),
		"onepageEdition" => array("name" => "onepageEdition"),
		"urlImg" => array("name" => "urlImg"),
		"scope" => array("name" => "scope"),
		"actionPrincipal" => array("name" => "actionPrincipal"),
		"admins" => array("name" => "admins"),
		"categoryNA" => array("name" => "categoryNA"),
		"typeStruct" => array("name" => "typeStruct"),
		"activityPlace" => array("name" => "activityPlace"),
		"extraInfo" => array("name" => "extraInfo"),
		"timetables" => array("name" => "timetables"),
		"certificationsAwards" => array("name" => "certificationsAwards"),
		"thematic" => array("name" => "thematic"),
		"template" =>  array("name" => "template"),
		"jobFamily" =>  array("name" => "jobFamily"),
		"typeFinancing" =>  array("name" => "typeFinancing"),
		"modality" =>  array("name" => "modality"),
		"legalStatus" =>  array("name" => "legalStatus"),
		"responsable" =>  array("name" => "responsable"),
		"link" =>  array("name" => "link"),
		"objective" =>  array("name" => "objective"),
		"linkFinancialDevice" =>  array("name" => "linkFinancialDevice"),
		"financialPartners" =>  array("name" => "financialPartners"),
		"maximumAmount" =>  array("name" => "maximumAmount"),
		"publicCible" =>  array("name" => "publicCible"),
		"strongPoints" =>  array("name" => "strongPoints"),
		"weaknesses" =>  array("name" => "weaknesses"),
		"conditionsEligibility" =>  array("name" => "conditionsEligibility"),
		"objectiveOdd" => array("name" => "objectiveOdd"),
		"toBeValidated" =>  array("name" => "toBeValidated"),
		"serviceOffers" => array("name" => "serviceOffers"),
		"areaOfIntervention" => array("name" => "areaOfIntervention"),
		"namesStartupers" => array("name" => "namesStartupers"),
		"yearEntryInpri" => array("name" => "yearEntryInpri"),
		"status" => array("name" => "status"),
		"statusActor" => array("name" => "statusActor"),
		"otherLocality" => array("name" => "otherLocality"),
		"projectsInProgress" => array("name" => "projectsInProgress"),
		"dateAdhesion" => array("name" => "dateAdhesion"),
		"nombreETP" => array("name" => "nombreETP"),
		"siren" => array("name" => "siren"),
		"adminFirstname" => array("name" => "contacts.firstname"),
		"adminLastname" => array("name" => "contacts.lastname"),
		"adminFunction" => array("name" => "contacts.function"),
		"adminPhone" => array("name" => "contacts.phone"),
		"adminMail" => array("name" => "contacts.mail"),
		"serviceOffersInnovativeCompanies" => array("name" => "serviceOffersInnovativeCompanies"),
		"linkWithInnovation" => array("name" => "linkWithInnovation"),
		"userSurname" => array("name" => "userSurname"),
		"userFirstName" => array("name" => "userFirstName"),
		"userDate" => array("name" => "userDate"),
		"userSelfEmployed" => array("name" => "userSelfEmployed"),
		"qualificationsStateDiplomas" => array("name" => "qualificationsStateDiplomas"),
		"qualificationsOtherCompetences" => array("name" => "qualificationsOtherCompetences"),
		"qualificationsOtherQualifications" => array("name" => "qualificationsOtherQualifications"),
		"companyInterventionMethods" => array("name" => "companyInterventionMethods"),
		"companyAddress" => array("name" => "companyAddress"),
		"companyLocationsFaceTofacesessions" => array("name" => "companyLocationsFaceTofacesessions"),
		"companyLocationsRemoteInterventions" => array("name" => "companyLocationsRemoteInterventions"),
		"companyDescription" => array("name" => "companyDescription"),
		"companyPublicCible" => array("name" => "companyPublicCible"),
		"documentsLogoCompany" => array("name" => "documentsLogoCompany"),
		"documentsBusinessCard" => array("name" => "documentsBusinessCard"),
		"documentsBrochure" => array("name" => "documentsBrochure"),
		"otherSociaNetworks" => array("name" => "otherSocialNetworks"),
		"registrationCertificate" => array("name" => "registrationCertificate"),
		"situation" => array("name" => "situation"),
		"trainingQualification" => array("name" => "trainingQualification"),
		"reference" => array("name" => "reference"),
		"features" => array("name" => "features"),
		"acronym" => array("name" => "acronym"),
		"teachingMethods" => array("name" => "teachingMethods"),
		"evaluation" => array("name" => "evaluation"),
		"certificationTraining" => array("name" => "certificationTraining"),
		"displayAuth" => array("name" => "displayAuth"),
		"operatingLocation" => array("name" => "operatingLocation"),
		"nameSign" => array("name" => "nameSign"),
		"surnameSign" => array("name" => "surnameSign"),
		"functionSign" => array("name" => "functionSign"),
		"locationSign" => array("name" => "locationSign"),
		"dateSign" => array("name" => "dateSign"),

	);

	//See findOrganizationByCriterias...
	public static function getWhere($params) {
		return PHDB::find(self::COLLECTION, $params);
	}

	//TODO SBAR - First test to validate data. Move it to DataValidator
	private static function getCollectionFieldNameAndValidate($organizationFieldName, $organizationFieldValue, $organizationId) {
		return DataValidator::getCollectionFieldNameAndValidate(self::$dataBinding, $organizationFieldName, $organizationFieldValue, $organizationId);
	}
	// @getDataBinding is a function used in save process in Element.php ou update process
	// It will return an array of available entry for organization collection
	// Most of accepted fields is correlate for schema.org 
	public static function getDataBinding() {
		return self::$dataBinding;
	}

	/** TO REFACTOR FOR IMPORT TRANSLATION COMMON FOR ALL ENTRIES
	 * _function_ @TranslateType is used in import process
	 * Perhaps impose a standard or genaralized this process for correlation by importer 
	 * Ex : A file with 1000 entries where there used a specific nomenclature for instance here for (string)type
	 * Importer user has to fullfil a list of correlation acording to our documentation API 
	 * _Array_ $correlateCsv=array(type=> array(
	 *					self::TYPE_NGO => ["Association", "association"],
						self::TYPE_BUSINESS => ["Entreprise", "CEQUON VEUT"],
						...
					)
	 *			);  
	 */
	public static function translateType($type) {
		if (trim($type) == "Association" || trim($type) == "association")
			$type = self::TYPE_NGO;
		else if (trim($type) == "Groupe Gouvernemental" || trim($type) == "Groupe gouvernemental" || trim($type) == "Organisation gouvernementale" || trim($type) == "Structure publique" || trim($type) == "Structure Publique" || trim($type) == "Service public")
			$type = self::TYPE_GOV;
		else if (trim($type) == "Entreprise")
			$type = self::TYPE_BUSINESS;
		else if (trim($type) == "Cooperative")
			$type = self::TYPE_COOP;
		else if (trim($type) == "Groupe" || trim($type) == "Groupe informel")
			$type = self::TYPE_GROUP;
		return $type;
	}
	/**
	 * insert a new organization in database
	 * @param array A well format organization 
	 * @param String $creatorId : an existing user id representing the creator of the organization
	 * @param String $adminId : can be ommited. user id representing the administrator of the organization
	 * @return array result as an array. 
	 */

	public static function afterSave($organization, $creatorId, $paramsImport = null) {
		$newOrganizationId = (string)$organization['_id'];
		if (@$paramsImport) {
			if (!empty($paramsImport["link"])) {
				$idLink = $paramsImport["link"]["idLink"];
				$typeLink = $paramsImport["link"]["typeLink"];
				if (@$paramsImport["link"]["role"] == "admin") {
					$isAdmin = true;
				} else {
					$isAdmin = false;
				}

				if ($typeLink == Organization::COLLECTION) {
					Link::connect($idLink, $typeLink, $newOrganizationId, self::COLLECTION, $creatorId, "members", false);
					Link::connect($newOrganizationId, self::COLLECTION, $idLink, $typeLink, $creatorId, "memberOf", false);
				} else if ($typeLink == Person::COLLECTION) {
					Link::connect($newOrganizationId, self::COLLECTION, $idLink, Person::COLLECTION, $creatorId, "members", $isAdmin);
					Link::connect($idLink, $typeLink, $newOrganizationId, self::COLLECTION, $creatorId, "memberOf", $isAdmin);
				}
			}

			if (!empty($paramsImport["img"])) {
				try {
					$paramsImg = $paramsImport["img"];
					$resUpload = Document::uploadDocumentFromURL(
						$paramsImg["module"],
						self::COLLECTION,
						$newOrganizationId,
						"avatar",
						false,
						$paramsImg["url"],
						$paramsImg["name"]
					);

					if (!empty($resUpload["result"]) && $resUpload["result"] == true) {
						$params = array();
						$params['id'] = $newOrganizationId;
						$params['type'] = self::COLLECTION;
						$params['moduleId'] = $paramsImg["module"];
						$params['folder'] = self::COLLECTION . "/" . $newOrganizationId;
						$params['name'] = $resUpload['name'];
						$params['author'] = Yii::app()->session["userId"];
						$params['size'] = $resUpload["size"];
						$params["contentKey"] = "profil";
						$params["doctype"]  = Document::DOC_TYPE_IMAGE;
						$resImgSave = Document::save($params);
						if ($resImgSave["result"] == false)
							throw new CTKException("Impossible de sauvegarder l'image.");
					} else {
						throw new CTKException("Impossible uploader l'image.");
					}
				} catch (CTKException $e) {
					throw new CTKException($e);
				}
			}
		}
		if (
			!empty($organization["parentType"]) &&
			!empty($organization["parentId"]) &&
			!empty($organization["source"]) &&
			!empty($organization["source"]["insertOrign"]) &&
			$organization["source"]["insertOrign"] == "network"
		) {

			//var_dump("ddsdsq");
			$child = array();
			$child[] = array(
				"childId" => $newOrganizationId,
				"childType" => Organization::COLLECTION,
				"childName" => $organization["name"],
				"roles" => array()
			);
			Link::multiconnect($child, $organization["parentId"], $organization["parentType"]);
			//var_dump($pepe);
		}

		//send Notification Email
		$creator = Person::getById($creatorId);
		//Mail::organization($creator,$organization);
		if (isset($organization["geo"]) && !empty($organization["geo"])) {
			$orgaGeo = $organization["geo"];
		} else
			$orgaGeo = "";

		$orgaTags = ((@$organization["tags"] && !empty($organization["tags"])) ? $organization["tags"] : null);

		if (@$organization["address"]["codeInsee"] && !empty($organization["address"]["codeInsee"]))
			$orgaCodeInsee = $organization["address"];
		else
			$orgaCodeInsee = "";
		$organization = Organization::getById($newOrganizationId);
		return array(
			"result" => true,
			"msg" => "Votre organisation est communectée.",
			"id" => $newOrganizationId,
			"organization" => $organization
		);
	}

	/**
	 * get an Organisation By Id
	 * @param type $id : is the mongoId of the organisation
	 * @return type
	 */
	public static function getById($id) {

		$organization = PHDB::findOne(Organization::COLLECTION, array("_id" => new MongoId($id)));

		if (empty($organization)) {
			$organization = Element::getGhost(self::COLLECTION);
			//throw new CTKException("The element you are looking for has been moved or deleted");
			//TODO Sylvain - Find a way to manage inconsistent data
			//throw new CommunecterException("The organization id ".$id." is unkown : contact your admin");
		} else {
			$organization = array_merge($organization, Document::retrieveAllImagesUrl($id, self::COLLECTION, $organization));
			//$organization["typeSig"] = "organizations";
			//$organization["typeOrganization"] = $organization["type"];

		}
		return $organization;
	}


	public static function  getByArrayId($arrayId, $fields = array(), $simply = false) {

		$organizations = PHDB::find(self::COLLECTION, array("_id" => array('$in' => $arrayId)), $fields);
		$res = array();
		foreach ($organizations as $id => $organization) {
			if (empty($organization)) {
				//TODO Sylvain - Find a way to manage inconsistent data
				//throw new CommunecterException("The organization id ".$id." is unkown : contact your admin");
			} else {
				if (isset($contactComplet["disabled"]) && $contactComplet["disabled"])
					$organization = null;
				else {
					if ($simply)
						$organization = self::getSimpleOrganizationById($id, $organization);
					else {
						$organization = array_merge($organization, Document::retrieveAllImagesUrl($id, self::COLLECTION, $organization));
						$organization["typeSig"] = "organizations";
					}
				}
			}
			$res[$id] = $organization;
		}

		return $res;
	}

	/**
	 * Retrieve a simple organization (id, name, profilImageUrl) by id from DB
	 * @param String $id of the organization
	 * @return array with data id, name, profilImageUrl, logoImageUrl
	 */
	public static function getSimpleOrganizationById($id, $orga = null) {

		$simpleOrganization = array();
		if (!$orga)
			$orga = PHDB::findOneById(self::COLLECTION, $id, array("id" => 1, "category" => 1, "name" => 1, "type" => 1, "email" => 1, "url" => 1, "shortDescription" => 1, "description" => 1, "address" => 1, "pending" => 1, "tags" => 1, "links" => 1, "geo" => 1, "updated" => 1, "profilImageUrl" => 1, "profilThumbImageUrl" => 1, "profilMarkerImageUrl" => 1, "profilMediumImageUrl" => 1, /*"openingHours" => 1,*/ "addresses" => 1, "telephone" => 1, "slug" => 1, "scope" => 1, "sourceType" => 1));
		if (!empty($orga)) {
			$simpleOrganization["id"] = $id;
			$simpleOrganization["category"] = @$orga["category"];
			$simpleOrganization["sourceType"] = @$orga["source"]["type"];
			$simpleOrganization["name"] = @$orga["name"];
			/*if(isset($orga["openingHours"]))
				$simpleOrganization["openingHours"]=$orga["openingHours"];*/
			$simpleOrganization["type"] = @$orga["type"];
			$simpleOrganization["email"] = @$orga["email"];
			$simpleOrganization["url"] = @$orga["url"];
			$simpleOrganization["telephone"] = @$orga["telephone"];
			$simpleOrganization["pending"] = @$orga["pending"];
			$simpleOrganization["tags"] = @$orga["tags"];
			$simpleOrganization["geo"] = @$orga["geo"];
			$simpleOrganization["shortDescription"] = @$orga["shortDescription"];
			$simpleOrganization["description"] = @$orga["description"];
			$simpleOrganization["updated"] = @$orga["updated"];
			$simpleOrganization["addresses"] = @$orga["addresses"];
			$simpleOrganization["slug"] = @$orga["slug"];
			$simpleOrganization["scope"] = @$orga["scope"];
			$simpleOrganization["typeSig"] = "organizations";

			$el = $orga;
			if (@$el["links"]) foreach (array("members", "followers") as $key)
				if (@$el["links"][$key])
					$simpleOrganization["counts"][$key] = count($el["links"][$key]);


			$simpleOrganization = array_merge($simpleOrganization, Document::retrieveAllImagesUrl($id, self::COLLECTION, $orga));

			$logo = Document::getLastImageByKey($id, self::COLLECTION, Document::IMG_LOGO);
			$simpleOrganization["logoImageUrl"] = $logo;

			$simpleOrganization["address"] = empty($orga["address"]) ? array("addressLocality" => Yii::t("common", "Unknown Locality")) : $orga["address"];
		}
		return $simpleOrganization;
	}


	/**
	 * Get an organization from an id and return filter data in order to return only public data
	 * @param type $id 
	 * @return organization structure
	 */
	public static function getPublicData($id) {
		//Public datas 
		$publicData = array(
			"imagePath",
			"name",
			"city",
			"socialAccounts",
			"url",
			"type",
			"coi"
		);

		//TODO SBAR = filter data to retrieve only public data	
		$organization = Organization::getById($id);
		if (empty($organization)) {
			//throw new CTKException("The organization id is unknown ! Check your URL");
		}

		return $organization;
	}

	public static function get_tls($fields = null, $user = '', $is_admin = 0) {
		$db_where = [
			'$or'	=> [
				['source.keys'		=> 'franceTierslieux'],
				['reference.costum'	=> 'franceTierslieux'],
			]
		];
		if (!empty($user)) {
			$db_and = [
				["links.members.$user.type" => Person::COLLECTION],
				[
					'$or' => [
						["links.members.$user.toBeValidated" => ['$exists' => 0]],
						["links.members.$user.toBeValidated" => false],
					]
				]
			];
			if ($is_admin)
				$db_and = array_merge($db_and, [
					["links.members.$user.isAdmin" => true],
					[
						'$or' => [
							["links.members.$user.isAdminPending" => ['$exists' => 0]],
							["links.members.$user.isAdminPending" => false],
						]
					]
				]);
			$db_where['$and'] = $db_and;
		}
		if (empty($fields))
			$fields = ['name', 'profilImageUrl'];
		return (PHDB::find(self::COLLECTION, $db_where, $fields));
	}
	
	public static function get_nontls($fields = null, $user = '', $is_admin = 0) {
		$db_where = [
			"source.keys" => ['$ne' => "franceTierslieux"],
			"reference.costum" => ['$ne' => "franceTierslieux"],
		];
		if (!empty($user)) {
			$db_and = [
				["links.members.$user.type" => Person::COLLECTION],
				[
					'$or' => [
						["links.members.$user.toBeValidated" => ['$exists' => 0]],
						["links.members.$user.toBeValidated" => false],
					]
				]
			];
			if ($is_admin)
				$db_and = array_merge($db_and, [
					["links.members.$user.isAdmin" => true],
					[
						'$or' => [
							["links.members.$user.isAdminPending" => ['$exists' => 0]],
							["links.members.$user.isAdminPending" => false],
						]
					]
				]);
			$db_where['$and'] = $db_and;
		}
		if (empty($fields))
			$fields = ['name', 'profilImageUrl'];
		return (PHDB::find(self::COLLECTION, $db_where, $fields));
	}
}
