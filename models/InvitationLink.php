<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;

use Element;
use Link;
use Person;
use PHDB;

class InvitationLink{
    const COLLECTION = "invitationLinks";

    public static function getLinks($targetType, $targetId){
        $invitationLinks = PHDB::find(self::COLLECTION, [
            "targetType"=>$targetType,
            "targetId" => $targetId
        ]);

        foreach($invitationLinks as $id => $invitationLink){
            $invitationLinks[$id]["joined"] = []; 
        }

        $element = Element::getElementById($targetId, $targetType);
        $elementLinks = [];
        
        if(isset($element["links"]) && isset(Link::$linksTypes[$targetType][Person::COLLECTION]) &&  isset($element["links"][Link::$linksTypes[$targetType][Person::COLLECTION]])){
            $elementLinks = $element["links"][Link::$linksTypes[$targetType][Person::COLLECTION]];
        }
        
        foreach($elementLinks as $personId => $elementLink){
            if(isset($elementLink["inviteByLink"]) && $elementLink["inviteByLink"]){
                $inviteLinkId = (string)$elementLink["inviteLinkId"];
                if(isset($invitationLinks[$inviteLinkId])){
                    $invitationLinks[$inviteLinkId]["joined"][] = $personId;
                }
            }
        }

        return $invitationLinks;
    }

    public static function delete($ref){
        return PHDB::remove(self::COLLECTION, ["ref"=>$ref]);
    }
}