<?php 
class Event {
	const COLLECTION = "events";
	const CONTROLLER = "event";
	const ICON = "fa-calendar";
	const COLOR = "#F9B21A";

	const NO_ORGANISER = "dontKnow";

	public static $types = array(
        "competition" => "Competition",
        "concert" => "Concert",
        "contest" => "Contest",
        "exhibition" => "Exhibition",
        "festival" => "Festival",
        "getTogether" => "Get together",
        "market" => "Market",
	    "meeting" => "Meeting",
	    "course"=>"Course",
		"workshop"=>"Workshop",
		"conference"=>"Conference",
		"debate"=>"Debate",
		"film"=>"Film",
		"stand"=>"Stand",
		"crowdfunding"=>"Crowdfunding",
		"internship" => "Internship",
        "spectacle" =>  "Spectacle",
		"others"=>"Others",
		"protest" => "Protest",
		"fair" => "Fair"
	);  
	      
	//From Post/Form name to database field name
	public static $dataBinding = array (
	    "name" => array("name" => "name", "rules" => array("required")),
	    "slug" => array("name" => "slug", "rules" => array("checkSlug")),
	    "email" => array("name" => "email", "rules" => array("email")),
	    "collection" => array("name" => "collection"),
	    "type" => array("name" => "type"),
	    "public"  => array("name" => "public"),
	    "parent" => array("name" => "parent"),
	    "parentId" => array("name" => "parentId"),
	    "parentType" => array("name" => "parentType"),
	    "organizerId" => array("name" => "organizerId"),
	    "organizerType" => array("name" => "organizerType"),
	    "organizer" => array("name" => "organizer", "rules" => array("validOrganizer")),
	    

	    "address" => array("name" => "address", "rules" => array("addressValid")),
	    "addresses" => array("name" => "addresses"),
	    "streetAddress" => array("name" => "address.streetAddress"),
	    "postalCode" => array("name" => "address.postalCode"),
	    "city" => array("name" => "address.codeInsee"),
	    "addressLocality" => array("name" => "address.addressLocality"),
	    "addressCountry" => array("name" => "address.addressCountry"),
	    "geo" => array("name" => "geo", "rules" => array("geoValid")),
	    "geoPosition" => array("name" => "geoPosition", "rules" => array("geoPositionValid")),
	    
	    "description" => array("name" => "description"),
	    "shortDescription" => array("name" => "shortDescription"),
	    "allDay" => array("name" => "allDay", "rules" => array("boolean")),
	    "modules" => array("name" => "modules"),
	    "recurrency" => array("name" => "recurency"),
	    "openingHours" => array("name" => "openingHours"),
	    "startDate" => array("name" => "startDate", "rules" => array("eventStartDate")),
	    "endDate" => array("name" => "endDate", "rules" => array("eventEndDate")),
	    "preferences" => array("name" => "preferences"),
		"profilImageUrl" => array("name" => "profilImageUrl"),


	    "warnings" => array("name" => "warnings"),
	    "source" => array("name" => "source"),
	    "badges" => array("name" => "badges"),
	    "tags" => array("name" => "tags"),
	    "medias" => array("name" => "medias"),
	    "urls" => array("name" => "urls"),
	    "url" => array("name" => "url"),
	    "contacts" => array("name" => "contacts"),
	    "modified" => array("name" => "modified"),
	    "updated" => array("name" => "updated"),
	    "creator" => array("name" => "creator"),
	    "created" => array("name" => "created"),
	    "locality" => array("name" => "address"),
	    "descriptionHTML" => array("name" => "descriptionHTML"),

	    "facebook" => array("name" => "socialNetwork.facebook"),
	    "twitter" => array("name" => "socialNetwork.twitter"),
	    "gpplus" => array("name" => "socialNetwork.googleplus"),
	    "github" => array("name" => "socialNetwork.github"),
	    "gitlab" => array("name" => "socialNetwork.gitlab"),
	    "diaspora" => array("name" => "socialNetwork.diaspora"),
	    "mastodon" => array("name" => "socialNetwork.mastodon"),
        "signal" => array("name" => "socialNetwork.signal"),
        "telegram" => array("name" => "socialNetwork.telegram"),
	    "onepageEdition" => array("name" => "onepageEdition"),
	    "instagram" => array("name" => "socialNetwork.instagram"),
	    "timeZone" => array("name" => "timeZone"),
	    "category" => array("name" => "category"),
	    "module" => array("name" => "module"),
	);

	//TODO SBAR - First test to validate data. Move it to DataValidator
  	private static function getCollectionFieldNameAndValidate($eventFieldName, $eventFieldValue, $eventId) {
		$res = "";
		if (isset(self::$dataBinding["$eventFieldName"])) 
		{
			$data = self::$dataBinding["$eventFieldName"];
			$name = $data["name"];
			//Validate field
			if (isset($data["rules"])) 
			{
				$rules = $data["rules"];
				foreach ($rules as $rule) {
					$isDataValidated = DataValidator::$rule($eventFieldValue, $eventId);
					if ($isDataValidated != "") {
						throw new CTKException($isDataValidated);
					}
				}	
			}
		} else {
			throw new CTKException("Unknown field :".$eventFieldName);
		}
		return $name;
	}

	/**
	 * get an event By Id
	 * @param type $id : is the mongoId of the event
	 * @return type
	 */
	public static function getById($id) {
		//var_dump($id);
		$event = PHDB::findOne(self::COLLECTION,array("_id"=>new MongoId($id)));
		if (!empty($event["startDate"]) && !empty($event["endDate"])) {
			if (gettype($event["startDate"]) == "object" && gettype($event["endDate"]) == "object") {
				//Set TZ to UTC in order to be the same than Mongo
				date_default_timezone_set('UTC');
				$event["startDate"] = date(DateTime::ISO8601, $event["startDate"]->sec);
				$event["endDate"] = date(DateTime::ISO8601, $event["endDate"]->sec);
			} else {
				//Manage old date with string on date event
				$now = time();
				$yesterday = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
				$yester2day = mktime(0, 0, 0, date("m")  , date("d")-2, date("Y"));
				$event["endDate"] = date('Y-m-d H:i:s', $yesterday);
				$event["startDate"] = date('Y-m-d H:i:s',$yester2day);

			}
		}

		if(!empty($event)){
			$event = array_merge($event, Document::retrieveAllImagesUrl($id, self::COLLECTION, $event));
			// $event["type"] = "events";
			//$event["typeSig"] = "events";
	  	}else{
	  		$event = Element::getGhost(self::COLLECTION);
			//throw new CTKException("The element you are looking for has been moved or deleted");
	  	}
	  	return $event;
	}

	public static function  getByArrayId($arrayId, $fields = array(), $simply = false) {
	  	
	  	$events = PHDB::find(self::COLLECTION, array( "_id" => array('$in' => $arrayId)), $fields);
	  	$res = array();
	  	foreach ($events as $id => $event) {
	  		if (empty($event)) {
            //TODO Sylvain - Find a way to manage inconsistent data
            //throw new CommunecterException("The organization id ".$id." is unkown : contact your admin");
	        } else {
	        	if($simply)
	        		$event = self::getSimpleEventById($id, $event);
	        	else{
		        	if (!empty($event["startDate"]) && !empty($event["endDate"])) {
						if (gettype($event["startDate"]) == "object" && gettype($event["endDate"]) == "object") {
							//Set TZ to UTC in order to be the same than Mongo
							date_default_timezone_set('UTC');
							$event["startDate"] = date(DateTime::ISO8601, $event["startDate"]->sec);
							$event["endDate"] = date(DateTime::ISO8601, $event["endDate"]->sec);
						} else {
							//Manage old date with string on date event
							$now = time();
							$yesterday = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
							$yester2day = mktime(0, 0, 0, date("m")  , date("d")-2, date("Y"));
							$event["endDate"] = date('Y-m-d H:i:s', $yesterday);
							$event["startDate"] = date('Y-m-d H:i:s',$yester2day);;
						}
					}
					if(!empty($event)){
						$event = array_merge($event, Document::retrieveAllImagesUrl($id, self::COLLECTION, $event));
						$event["typeSig"] = "events";
				  	}
				}
	        }
	  		$res[$id] = $event;
	  	}
	  
	  	return $res;
	}

	/**
	 * Retrieve a simple event (id, name, type profilImageUrl) by id from DB
	 * @param String $id of the event
	 * @return array with data id, name, type profilImageUrl
	 */
	public static function getSimpleEventById($id, $event=null) {
		
		$simpleEvent = array();
		if(empty($event))
			$event = PHDB::findOneById( self::COLLECTION ,$id, array("id" => 1, "name" => 1, "creator" => 1,  "shortDescription" => 1, "description" => 1, "address" => 1, "geo" => 1, "tags" => 1, "links" => 1, "profilImageUrl" => 1, "profilThumbImageUrl" => 1, "profilMarkerImageUrl" => 1, "profilMediumImageUrl" => 1, "recurrency" => 1, "openingHours" => 1, "startDate" => 1, "endDate" => 1, "addresses"=>1, "preferences" => 1, "collection" => 1, "type" => 1, "slug" => 1));
		//Rest::json($event); exit;
		if(!empty($event)){
			$simpleEvent["id"] = $id;
			$simpleEvent["_id"] = $event["_id"];
			$simpleEvent["name"] = @$event["name"];
			$simpleEvent["recurrency"] = @$event["recurrency"];
			if(isset($event["openingHours"]))
				$simpleEvent["openingHours"]=$event["openingHours"];
			if (isset($event["startDate"]) && gettype($event["startDate"]) == "object" /*&& gettype($event["endDate"]) == "object"*/) {
				//Set TZ to UTC in order to be the same than Mongo
				date_default_timezone_set('UTC');
				$simpleEvent["startDate"] = date(DateTime::ISO8601, $event["startDate"]->sec);
				$simpleEvent["startDateSec"] = $event["startDate"]->sec ;
				/*$simpleEvent["endDate"] = date(DateTime::ISO8601, $event["endDate"]->sec);
				$simpleEvent["endDateSec"] = $event["endDate"]->sec ;*/
			}
			if ( isset($event["endDate"]) && gettype($event["endDate"]) == "object") {
				//Set TZ to UTC in order to be the same than Mongo
				date_default_timezone_set('UTC');
				$simpleEvent["endDate"] = date(DateTime::ISO8601, $event["endDate"]->sec);
				$simpleEvent["endDateSec"] = $event["endDate"]->sec ;
			}
			$simpleEvent["geo"] = @$event["geo"];
			$simpleEvent["tags"] = @$event["tags"];
			$simpleEvent["preferences"] = @$event["preferences"];
			$simpleEvent["shortDescription"] = @$event["shortDescription"];
			$simpleEvent["description"] = @$event["description"];
			$simpleEvent["addresses"] = @$event["addresses"];
			$simpleEvent["creator"] = @$event["creator"];
			$simpleEvent["slug"] = @$event["slug"];
			
			$simpleEvent = array_merge($simpleEvent, 
									   Document::retrieveAllImagesUrl($id, self::COLLECTION, 
									   $event));
			
			$simpleEvent["address"] = empty($event["address"]) ? 
									  array("addressLocality" => Yii::t("common","Unknown Locality")) : 
									  $event["address"];

			$simpleEvent["collection"] = Event::COLLECTION;
			
			$el = $event;
			if(@$el["links"]) 
			foreach(array("attendees", "followers") as $key) 
				if(@$el["links"][$key])
				$simpleEvent["counts"][$key] = count(@$el["links"][$key]);

		}
		return @$simpleEvent;
	}

	public static function getWhere($params) 
	{
	  	$events =PHDB::findAndSort( self::COLLECTION,$params,array("created"),null);
	  	return Event::addInfoEvents($events);
	}
	
	/**
	 * get events with generic where param and limit $limMin and $limMax
	 * @return list of events
	 */
	public static function getEventByWhereSortAndLimit($where,$orderBy, $indexStep, $indexMin){
		$events = PHDB::findAndSortAndLimitAndIndex(self::COLLECTION, SearchNew::prepareTagsRequete($where), $orderBy, $indexStep, $indexMin);
	   	return Event::addInfoEvents($events);
	}	

	/**
	 * Get an event from an id and return filter data in order to return only public data
	 * @param type $id 
	 * @return event structure
	 */
	public static function getPublicData($id) {
		//Public datas 
		$publicData = array (
		);

		//TODO SBAR = filter data to retrieve only publi data	
		$event = Event::getById($id);
		if (empty($event)) {
			//throw new CTKException("The event id is unknown ! Check your URL");
		}

		return $event;
	}

	public static function formatDateRender($params){
		if (isset($params["startDate"]) && gettype($params["startDate"]) == "object" && isset($params["endDate"]) && gettype($params["endDate"]) == "object") {
			//Set TZ to UTC in order to be the same than Mongo
			date_default_timezone_set('UTC');
			$params["startDate"] = date(DateTime::ISO8601, $params["startDate"]->sec);
			$params["endDate"] = date(DateTime::ISO8601, $params["endDate"]->sec);
		}
		return $params;
	}

	public static function formatBeforeSaving($params) {
		if(@$params["startDate"] && !is_a($params["startDate"], 'MongoDate')){
			$startDate = DataValidator::getDateTimeFromString($params['startDate'], "start date");
			$endDate = DataValidator::getDateTimeFromString($params['endDate'], "end date");
			if(@$params["allDay"] && $params["allDay"]){
				$startDate=date_time_set($startDate, 00, 00);
				$endDate=date_time_set($endDate, 23, 59);
			}
			$params["startDate"] = new MongoDate($startDate->getTimestamp());
			$params["endDate"]   = new MongoDate($endDate->getTimestamp());
		}
		return array('result' => true, "params"=>$params );
	}


	public static function addInfoEvents($events){
		foreach ($events as $key => $value) {

	  		if (!empty($value["startDate"]) && !empty($value["endDate"])) 
	  		{
				if (gettype($value["startDate"]) == "object" && gettype($value["endDate"]) == "object") 
				{
					$events[$key]["startDate"] = date(DateTime::ISO8601, $value["startDate"]->sec);
					$events[$key]["endDate"] = date(DateTime::ISO8601, $value["endDate"]->sec);
				} 
				else 
				{
					//Manage old date with string on date value
					$now = time();
					$yesterday = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
					$yester2day = mktime(0, 0, 0, date("m")  , date("d")-2, date("Y"));
					$events[$key]["endDate"] = date('Y-m-d H:i:s', $yesterday);
					$events[$key]["startDate"] = date('Y-m-d H:i:s',$yester2day);
				}
			}

	  		$events[$key]["organizer"] = "";
	  		if( @$value["links"]["organizer"] )
	  		{
		  		foreach ( $value["links"]["organizer"] as $organizerId => $val ) 
		  		{
		  			$name = "";
  					if(@$val["type"]){
  						$organization = Element::getInfos( $val["type"], $organizerId );
  						$name = $organization["name"];
  					}
  					$events[$key]["organizer"] = $name;
		  		}
		  	}
 	  		$events[$key] = array_merge($events[$key], Document::retrieveAllImagesUrl($key, self::COLLECTION, $value));
	  	}
	  	return $events;
	}

	/* 	Get an event from an OpenAgenda ID 
	*	@param string OpenAgenda Id is to find event
	*   return Event
	*/
	public static function getEventsOpenAgenda($eventsIdOpenAgenda){
		$where=array("source.id" => $eventsIdOpenAgenda);
		$event=PHDB::find(self::COLLECTION, $where);
		return $event;
	}



	/* 	Get state an event from an OpenAgenda ID 
	*	@param string OpenAgenda ID
	*	@param string Date Update openAgenda
	*   return String ("Add", "Update" or "Delete")
	*/
	public static function getStateEventsOpenAgenda($eventsIdOpenAgenda, $dateUpdate, $endDateOpenAgende){
		$state = "";
		$event=Event::getEventsOpenAgenda($eventsIdOpenAgenda);
		if(empty($event)){
			$state = "Add";
		}else{
			foreach ($event as $key => $value) {
				if(!empty($endDateOpenAgende[0]["dates"])){
					$arrayTimeEnd = explode(":", $endDateOpenAgende[0]["dates"][0]["timeEnd"]);
					$arrayDate = explode("-", $endDateOpenAgende[0]["dates"][0]["date"]);
					$end = mktime($arrayTimeEnd[0], $arrayTimeEnd[1], $arrayTimeEnd[2], $arrayDate[1]  , $arrayDate[2], $arrayDate[0]);
					$endDateEvents = date('Y-m-d H:i:s', $end);
					$today = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-1, date("Y"));
					
					if(!empty($value["modified"]->sec))
						$lastUpDate = $value["modified"]->sec ;
					else
						$lastUpDate = $value["created"]->sec ;
					
					if(strtotime($dateUpdate) > $lastUpDate ){
						$state = "Update";
					}
				}
				break;
			}
			
		}

		return $state;
	}

	/* 	Get state an event from an OpenAgenda ID 
	*	@param string OpenAgenda ID
	*	@param string Date Update openAgenda
	*   return String ("Add", "Update" or "Delete")
	*/
	public static function createEventsFromOpenAgenda($eventOpenAgenda) {
		$newEvents["name"] = empty($eventOpenAgenda["title"]["fr"]) ? "" : $eventOpenAgenda["title"]["fr"];
		$newEvents["description"] = empty($eventOpenAgenda["description"]["fr"]) ? "" : $eventOpenAgenda["description"]["fr"];
		$newEvents["shortDescription"] = empty($eventOpenAgenda["freeText"]["fr"]) ? "" : $eventOpenAgenda["freeText"]["fr"];
		$newEvents["image"] = empty($eventOpenAgenda["image"]) ? "" : $eventOpenAgenda["image"];
		$newEvents["organizerId"] = Yii::app()->params['idOpenAgenda'];
		$newEvents["organizerType"] = Person::COLLECTION ;	
			
		if(!empty($eventOpenAgenda["locations"][0]["dates"])){
			$nbDates = count($eventOpenAgenda["locations"][0]["dates"]);
			if(!empty($eventOpenAgenda["locations"][0]["dates"][0]["timeStart"]) && !empty($eventOpenAgenda["locations"][0]["dates"][$nbDates-1]["timeEnd"])){
				$arrayTimeStart = explode(":", $eventOpenAgenda["locations"][0]["dates"][0]["timeStart"]);
				$arrayTimeEnd = explode(":", $eventOpenAgenda["locations"][0]["dates"][$nbDates-1]["timeEnd"]);
				
				$arrayDateStart = explode("-", $eventOpenAgenda["locations"][0]["dates"][0]["date"]);
				$arrayDateEnd = explode("-", $eventOpenAgenda["locations"][0]["dates"][$nbDates-1]["date"]);

				$start = mktime($arrayTimeStart[0], $arrayTimeStart[1], $arrayTimeStart[2], $arrayDateStart[1]  , $arrayDateStart[2], $arrayDateStart[0]);
				$end = mktime($arrayTimeEnd[0], $arrayTimeEnd[1], $arrayTimeEnd[2], $arrayDateEnd[1]  , $arrayDateEnd[2], $arrayDateEnd[0]);

				$newEvents["startDate"] = date('Y-m-d H:i:s', $start);
				$newEvents["endDate"] = date('Y-m-d H:i:s', $end);
			}
			
			$newEvents["dates"] = $eventOpenAgenda["locations"][0]["dates"];
		}


		$geo["latitude"] = (empty($eventOpenAgenda["locations"][0]["latitude"]) ? "" : $eventOpenAgenda["locations"][0]["latitude"]);
		$geo["longitude"] = (empty($eventOpenAgenda["locations"][0]["longitude"]) ? "" : $eventOpenAgenda["locations"][0]["longitude"]);
		$address['postalCode'] =  (empty($eventOpenAgenda["locations"][0]["postalCode"])? "" : $eventOpenAgenda["locations"][0]["postalCode"]);
		$address['streetAddress'] = (empty($eventOpenAgenda["locations"][0]["address"]) ? "" : $eventOpenAgenda["locations"][0]["address"]);
		$address['addressLocality'] = (empty($eventOpenAgenda["locations"][0]["city"]) ? "" : $eventOpenAgenda["locations"][0]["city"]);

		/*$address = (empty($organization['address']) ? null : $organization['address']);
		$geo = (empty($newEvents["geo"]) ? null : $newEvents["geo"]);*/
		//var_dump($newOrganization['name']);
		$details = Import::getAndCheckAddressForEntity($address, $geo, null) ;
		$newEvents['address'] = $details['address'];

		if(!empty($details['geo']))
			$newEvents['geo'] = $details['geo'] ;

		if(!empty($details['geoPosition']))
			$newEvents['geoPosition'] = $details['geoPosition'] ;

	
		$newEvents["tags"] = empty($eventOpenAgenda["tags"]["fr"]) ? "" : explode(",", $eventOpenAgenda["tags"]["fr"]);

		$newEvents["creator"] = Yii::app()->params['idOpenAgenda'];
		$newEvents["type"] = "other";
		$newEvents["public"] = true;
		$newEvents['allDay'] = true;

		$newEvents['source']["id"] = $eventOpenAgenda["uid"] ;
		$newEvents['source']["url"] = $eventOpenAgenda["link"] ;
		$newEvents['source']["key"] = "openagenda" ;

		return $newEvents;
	}

	public static function saveEventFromOpenAgenda($params, $moduleId) {
		$newEvent = self::getAndCheckEventOpenAgenda($params);
		

		if(!empty($newEvent["image"])){
			$arrrayNameImage = explode("/", $newEvent["image"]);
			$nameImage = $arrrayNameImage[count($arrrayNameImage)-1];
			$pathFolderImage = "https://cibul.s3.amazonaws.com/";
			unset($newEvent["image"]);
		}
	    


	    Yii::app()->mongodb->selectCollection(self::COLLECTION)->insert($newEvent);
	    if (isset($newEvent["_id"]))
	    	$newEventId = (String) $newEvent["_id"];
	    else
	    	throw new CTKException("Problem inserting the new event");
	    
		$creator = true;
		$isAdmin = true;
		Link::attendee($newEvent["_id"], Yii::app()->params['idOpenAgenda'], $isAdmin, $creator);
	    Link::addOrganizer($params["organizerId"],$params["organizerType"], $newEvent["_id"], Yii::app()->params['idOpenAgenda']);

	    $msgErrorImage = "" ;
	    if(!empty($nameImage)){
			try{
				$res = Document::uploadDocument($moduleId, self::COLLECTION, $newEventId, "avatar", false, $pathFolderImage, $nameImage);
				if(!empty($res["result"]) && $res["result"] == true){
					$params = array();
					$params['id'] = $newEventId;
					$params['type'] = self::COLLECTION;
					$params['moduleId'] = $moduleId;
					$params['folder'] = self::COLLECTION."/".$newEventId;
					$params['name'] = $res['name'];
					$params['author'] = Yii::app()->session["userId"] ;
					$params['size'] = $res["size"];
					$params["contentKey"] = "profil";
					$res2 = Document::save($params);
					if($res2["result"] == false)
						throw new CTKException("Impossible de d'enregistrer le fichier.");

				}else{
					$msgErrorImage = "Impossible uploader le document." ; 
				}
			}catch (CTKException $e){
				throw new CTKException($e);
			}
		}

				
		return array("result"=>true, "msg"=>Yii::t("event","Your event has been connected.")." ".$msgErrorImage, "id"=>$newEvent["_id"], "event" => $newEvent );
	

	}

	
	public static function getAndCheckEventOpenAgenda($event) {
		$newEvent = array();
		
		if (empty($event['name'])) {
			throw new CTKException(Yii::t("import","001", null, Yii::app()->controller->module->id));
		}else
			$newEvent['name'] = $event['name'];



		$newEvent['created'] = new MongoDate(time()) ;
		
		if(!empty($event['email'])) {
			if (! preg_match('#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#',$organization['email'])) { 
				throw new CTKException(Yii::t("import","205", null, Yii::app()->controller->module->id));
			}
			$newEvent["email"] = $event['email'];
		}

		if(empty($event['type'])) {
			throw new CTKException(Yii::t("import","208", null, Yii::app()->controller->module->id));
		}else{
			$newEvent["type"] = $event['type'];
		}
				  
		
		if(!empty($event['address'])) {
			if(empty($event['address']['postalCode'])){
				throw new CTKException(Yii::t("import","101", null, Yii::app()->controller->module->id));
			}
			if(empty($event['address']['codeInsee'])){
				throw new CTKException(Yii::t("import","102", null, Yii::app()->controller->module->id));
			}
			if(empty($event['address']['addressCountry'])){
				throw new CTKException(Yii::t("import","104", null, Yii::app()->controller->module->id));
			}
			if(empty($event['address']['addressLocality']))
				throw new CTKException(Yii::t("import","105", null, Yii::app()->controller->module->id));
			
			$newEvent['address'] = $event['address'] ;

		}else {
			throw new CTKException(Yii::t("import","100", null, Yii::app()->controller->module->id));
		}

		if(!empty($event['geo']) && !empty($event["geoPosition"])){
			$newEvent["geo"] = $event['geo'];
			$newEvent["geoPosition"] = $event['geoPosition'];

		}else if(!empty($event["geo"]['latitude']) && !empty($event["geo"]["longitude"])){
			$newEvent["geo"] = 	array(	"@type"=>"GeoCoordinates",
						"latitude" => $event["geo"]['latitude'],
						"longitude" => $event["geo"]["longitude"]);

			$newEvent["geoPosition"] = array("type"=>"Point",
													"coordinates" =>
														array(
															floatval($event["geo"]['latitude']),
															floatval($event["geo"]['longitude']))
												 	  	);
		}
		else
			throw new CTKException(Yii::t("import","150", null, Yii::app()->controller->module->id));
			
		
		if (isset($event['tags'])) {
			if ( gettype($event['tags']) == "array" ) {
				$tags = $event['tags'];
			} else if ( gettype($event['tags']) == "string" ) {
				$tags = explode(",", $event['tags']);
			}
			$newEvent["tags"] = $tags;
		}
		
		if (!empty($event['description']))
			$newEvent["description"] = $event['description'];

		if (!empty($event['shortDescription']))
			$newEvent["shortDescription"] = $event['shortDescription'];

		if(!empty($event['creator'])){
			$newEvent["creator"] = $event['creator'];
		}

		if(!empty($event['source'])){
			$newEvent["source"] = $event['source'];
		}

		//url by ImportData
		if(!empty($event['url'])){
			$newEvent["url"] = $event['url'];
		}

		if(!empty($event['allDay'])){
			$newEvent["allDay"] = $event['allDay'];
		}

		if(!empty($event['startDate'])){	
			$m = new MongoDate(strtotime($event['startDate']));
			$newEvent['startDate'] = $m;
		}	
		if(!empty($event['image'])){
			$newEvent["image"] = $event['image'];
		}

		if(!empty($event['endDate'])){
			$m = new MongoDate(strtotime($event['endDate']));
			$newEvent['endDate'] = $m;
		}		
		
		return $newEvent;
	}
	

	public static function getDataBinding() {
	  	return self::$dataBinding;
	}

}
?>