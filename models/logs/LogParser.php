<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models\logs;
class LogParser {

    /*
     * recuperer tous les log dans le log path
     */
    public static function getApplicationLog($logpath ='') : string
    {
        $target_dir = scandir($logpath);
        $file       = '';
        foreach($target_dir as $value)  {
            if($value === '.' || $value === '..') {
                continue;
            }
            if(is_file("$logpath/$value") && $value ==='application.log') {
                $file="$logpath/$value";
            }
        }
        return $file;
    }
    public static function openLog($log) : string
    {
                $log_to_open = fopen($log, "rb");// lecture binaire
                $detail      = fread($log_to_open, filesize($log));
                fclose($log_to_open);
                return $detail;
    }
    public static function getLogOutput($log) : array
    {
        $regex = '/(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}) \[([^]]+)\]\[[^]]+\]\[[^]]+\]\[(error|info)\]\[(.*?)\] (.+?)(?=(?:\n\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})|\z)/ms';

        if (preg_match_all($regex, addslashes($log), $matches, PREG_SET_ORDER)) {
            $info = [];
            foreach ($matches as $match) {
                $timestamp = $match[1];
                $ipAddress = $match[2];
                $type = $match[3];
                $level = $match[4];
                $message = $match[5];
                $stackTrace = '';
                $errorMessage = '';
                $messageRegex = '/^(.*?)(?:\nStack trace:\n([\s\S]+))?$/m';
                $stackTraceRegex = '/Stack trace:\n([\s\S]+)/m';
                $messageMatch = [];
                $stackTraceMatch = [];
                if (preg_match($messageRegex, $message, $messageMatch)) {
                    $errorMessage = $messageMatch[1];
                    if (preg_match($stackTraceRegex, $message, $stackTraceMatch)) {
                        $stackTrace = $stackTraceMatch[1];
                    }
                }
                if ($type == 'error' && (stripos($errorMessage, 'activitypub') !== false || stripos($stackTrace, 'activitypub') !== false)) {
                    $info[] = [
                        'time' => $timestamp,
                        'ip' => $ipAddress,
                        'Level' => $level,
                        'type' => $type,
                        'message' => $errorMessage,
                        'stackTrace' => $stackTrace
                    ];
                }
            }
            return $info;
        } else {
            return [];
        }
    }

}