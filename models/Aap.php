<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;

use cebe\markdown\Markdown;
use Citoyen;
use Comment;
use DateTime;
use Parsedown;
use PhpParser\Node\Expr\AssignOp\Div;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Pdf;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Rest, PHDB, Form, ActStr, Ctenat, Element, Organization, Link, Person, Project, Authorisation, Yii, Zone, SearchNew, MongoId, Action, Answer, Document, CacheHelper, Badge, MongoRegex, Mail, Slug, Notification;
use Input;
use Utils;
use yii\helpers\Json;
use DateTimeZone;

class Aap {
	const COLLECTION = "aap";
	const CONTROLLER = "aap";
	const MODULE = "costum";

	public static function badgelabel() {
		return [
			"progress"          => Yii::t('common', "Progress"),
			"vote"              => Yii::t('common', "Vote"),
			"funded"            => Yii::t('common', "Funded"),
			"call"              => Yii::t('common', "Call"),
			"newaction"         => Yii::t('common', "New action"),
			"prevalided"        => Yii::t('common', "Prevalided"),
			"validproject"      => Yii::t('common', "Valid project"),
			"voted"             => Yii::t('common', "Voted"),
			"finished"          => Yii::t('common', "Finish"),
			"suspend"           => Yii::t('common', "Suspend"),
			"underconstruction" => Yii::t('common', "Under construction"),
			"projectstate"      => Yii::t('common', "Projectstate"),
			"notified"          => Yii::t('common', "Notified"),
			"notificationSent"  => Yii::t('common', "Notification Sent"),
			"unnotified"        => Yii::t('common', "Unnotified"),
			"renewed"           => Yii::t('common', "Renewed")
		];
	}

	public static $statusicon = [
		"progress"          => "hourglass",
		"finished"          => "check-circle",
		"abandoned"         => "times-circle",
		"newaction"         => "plus-circle",
		"call"              => "tag",
		"projectstate"      => "rocket",
		"funded"            => "euro",
		"vote"              => "gavel",
		"underconstruction" => "flag",
		"prevalided"        => "archive",
		"validproject"      => "check",
		"voted"             => "suitcase",
		"notified"          => "envelope",
		"notificationSent"  => "paper-plane",
		"renewed"           => "check"
	];

	public static function aapGraph(string $slug) {
		$graph = file_get_contents("../../modules/costum/data/aapGraph.json", FILE_USE_INCLUDE_PATH);
		$graph = json_decode($graph, true);

		if (!empty($graph[$slug]))
			return $graph[$slug];
		else
			return $graph["default"];
	}

	public static function generateConfig($post) {
		if (isset($post["aapConfig"]))
			$addAapConfig = PHDB::insert(Form::COLLECTION, $post["aapConfig"]);
		if (isset($post["formParent"]))
			$addFormParent = PHDB::insert(Form::COLLECTION, $post["formParent"]);
		if (isset($post["stepInputs"])) {
			$steps = array();
			foreach ($post["stepInputs"] as $ks => $vs) {
				$steps[$ks] = $vs;
			}
			if (!empty($steps))
				$addSteps = PHDB::batchInsert("inputs", $steps);
		}

		if ($addAapConfig && $addFormParent && $addSteps)
			return Rest::json(array("result" => true, "msg" => Yii::t("common", "Information updated")));
	}

	public static function actions($answer) {
		$answerId = (is_array($answer["_id"]) && !empty($answer["_id"]['$id'])) ? $answer["_id"]['$id'] : (!empty($answer["_id"]) ? (string)$answer["_id"] : "");
		$totalActions = PHDB::find(Action::COLLECTION, ["answerId" => $answerId]);
		$done = 0;
		//$closed = 0;
		foreach ($totalActions as $key => $value) {
			if (!empty($value["status"]) && $value["status"] == "done")
				$done++;
		}
		$res = array(
			"done" => $done,
			"total" => count($totalActions),
			"html" => count($totalActions) != 0 ? "<div class='padding-left-5 padding-right-5 text-center'>Actions<br><b>" . $done . "/" . count($totalActions) . "</b></div>" : ""
		);
		return $res;
	}

	public static function get_string_between($string, $start, $end) {
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	public static function get_value($indexes, $arrayToAccess) {
		if (count($indexes) > 1)
			return self::get_value(array_slice($indexes, 1), $arrayToAccess[$indexes[0]]);
		else
			return !empty($arrayToAccess[$indexes[0]]) ? $arrayToAccess[$indexes[0]] : "";
	}

	public static function listItemCoFormButtons($p_active = false, $adminRight = false, $canEditEachotherAnswer = false, $canReadEachOtherAnswer = false, $answerId = '', $useranswer = false) {
		$socialBtn = '';
		if (isset(Yii::app()->session["userId"]) && ($p_active || $adminRight)) {
			$btnclass = "";
			$btndataw = "";
			$btndatar = "";
			$btnpdfclass = "";
			$btndeleteclass = "";
			$btndatatype = "";

			$btnclass = "getanswer";
			$btndataw = "data-mode='w' data-ansid='" . $answerId . "'";
			$btndatar = "data-mode='r' data-ansid='" . $answerId . "'";
			$btnpdfclass = "exportanswer";
			$btndeleteclass = "deleteanswer";
			$socialBtn = '
			  <div class="col-md-12">
				<div class="social-links justify-content-center">';
			if ($adminRight || $canEditEachotherAnswer || (!$canEditEachotherAnswer && $useranswer)) {
				$socialBtn .= '<div class="social-btn flex-center ' . $btnclass . '" ' . $btndataw . $btndatatype . '>
						<i class="fa fa-pencil-square-o editdeleteicon"></i>
						<span style="font-size: 14px;">' . Yii::t('common', 'Edit') . '</span>
					  </div>';
			}

			if ($adminRight || $canReadEachOtherAnswer || (!$canReadEachOtherAnswer && $useranswer)) {
				$socialBtn .= '<div class="social-btn flex-center ' . $btnclass . '" ' . $btndatar . '>
						<i class="fa fa-sticky-note-o editdeleteicon"></i><span style="font-size: 14px;">' . Yii::t('common', 'Read') . '</span>
					  </div>';

				if ($adminRight) {
					$socialBtn .= '<div class="social-btn flex-center ' . $btndeleteclass . '" ' . $btndataw . '>
						  <i class="fa fa-trash-o editdeleteicon" style="color: #ff5722"></i><span style="font-size: 14px;">' . Yii::t('common', 'Delete') . '</span>
						</div>';
				}

				//   if ($opalContextId != null) {
				// 	$socialBtn .= '<div class="social-btn flex-center getansweropal " data-ansid="'. $answerId .'">
				// 	  <i class="fa fa-area-chart editdeleteicon"></i><span style="font-size: 14px;">'. Yii::t('common', 'Dashboard') .'</span>
				// 	</div>';
				//   }
			}
			$socialBtn .= '
				  </div>
				</div>';
		}
		return $socialBtn;
	}

	public static function answerCoremuVariables($answer) {
		$coremu = false;
		$needCandidate = false;

		$candidateNeed = 0;
		$coremuLDD = array();
		$totalprice = 0;
		$totalvalid = 0;
		if (!empty($answer["answers"]["aapStep1"]["depense"])) {
			$coremu = true;

			foreach ($answer["answers"]["aapStep1"]["depense"] as $depId => $dep) {
				$candidatNumber = 0;

				if (!empty($dep["price"])) {
					$totalprice += $dep["price"];
				}

				if (!empty($dep["estimates"])) {
					$needCandidate = true;
					foreach ($dep["estimates"] as $estimatekey => $estimate) {
						if (MongoId::isValid($estimatekey) && (empty($estimate["deny"]) || $estimate["deny"] == false)) {
							$needCandidate = false;
							$candidatNumber++;
							if (!empty($estimate["validate"]) && $estimate["validate"] == "validated") {
								if (isset($dep["switchAmountAttr"]) && $dep["switchAmountAttr"] == "assignBudget" && !empty($estimate["assignBudget"])) {
									$totalvalid += $estimate["assignBudget"];
								} else {
									$candidatperc = 100 / intval(Aap::countAvalaiblecandidate($dep));
									if (!empty($estimate["percentage"])) {
										$candidatperc = $estimate["percentage"];
									}
									$totalvalid += round(($dep["price"] * $candidatperc) / 100, 2);
								}
							}
						}
					}
					if (!empty($dep["candidateNumber"]) && intval($dep["candidateNumber"]) < $candidatNumber) {
						$needCandidate = true;
					}
				} else {
					$needCandidate = true;
				}

				$dep["candidateTotal"] = $candidatNumber;
				$coremuLDD[] = $dep;
			}
		}

		return array(
			"totalprice"    => $totalprice,
			"totalvalid"    => $totalvalid,
			"coremu"        => $coremu,
			"needCandidate" => $needCandidate,
			//"candidatNumber" => $candidatNumber,
			"coremuLDD"     => $coremuLDD
			//"candidateNeed" => $candidateNeed
		);
	}

	public static function answerCoremuTableVariables($answers, $onefield, $tableonly) {
		$tableAnswers = array();
		$tableprice = array();
		$tablestatus = array();
		$tablecandidatename = array();
		if (!$tableonly) {
			$tableSankey = array();
			$tablepercentage = array();
			$tableprice = array();
			$tablestatus = array();
			$tablecandidatename = array();
			$tablecandidatestatus = array();
			$propopiedata = array();
			$propopielabels = array();
			$propobarlabel = array("validé", "non validé", "pas mentioné");
			$tableSankeyLinks = array();
			$tableSankeyNodes = array();
			$notificationbar = array();
			$realpropositiondata = array();
			$propobardata = array();
			$totalcandidattovalid = 0;
			$totalpricetovalid = 0;
			$totalneedcandidat = 0;
			$totalansweraction = 0;
		}
		$communityname = [];
		$badgename = [];

		//$communityLinks = Element::getCommunityByTypeAndId(,);
		foreach ($answers as $idanswer => $answer) {
			if (isset($answer['answers']["aapStep1"]["titre"]) && $answer['answers']["aapStep1"]["titre"] != "" && isset($answer['answers']["aapStep1"]["depense"])) {
				foreach ($answer['answers']["aapStep1"]["depense"] as $iddepense => $depense) {
					$candidatebtn = !empty($depense["poste"]) ? $depense["poste"] : "(vide)";
					if (empty($depense["estimates"][Yii::app()->session["userId"]])) {
						$candidatebtn = "<div class='dropdown'>
                                          <a class='dropdown-toggle coremudrbtn coremubtninfo dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                            " . @$depense['poste'] . "
                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                          </a>
                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                            <li role='presentation'><a class='btn btn-sm candidatebtn' type='button' data-pos='" . $iddepense . "' data-id='" . $idanswer . "' data-action='validated'> candidater</a></li>
                                          </ul>
                                        </div>";
					}
					$haveghostcandidate = false;
					if (!empty($depense["estimates"])) {
						$haveghostcandidate = true;
						foreach ($depense["estimates"] as $bg_id => $bdg) {
							if (MongoId::isValid($bg_id) && (empty($bdg["deny"]) || $bdg["deny"] == false)) {
								$haveghostcandidate = false;
							}
						}
					}

					if (!empty($depense["estimates"]) && !$haveghostcandidate) {
						$badge = [];
						if (isset($depense["badge"])) {
							foreach ($depense["badge"] as $bdg) {
								if (isset($badgename[$bdg])) {
									$badge[] = $badgename[$bdg];
								} else {
									$addbadgeinfo = PHDB::findOneById(Badge::COLLECTION, $bdg);
									if (!empty($addbadgeinfo["name"])) {
										$addbadge = $addbadgeinfo["name"];
										$badgename[$bdg] = $addbadge;
										$badge[] = $addbadge;
									}
								}
								//$badge[] = PHDB::findOneById(Badge::COLLECTION , $bdg)["name"];
							}
							//$badge[] = $depense["badge"];
						}

						$candidateCount = self::countAvalaiblecandidate($depense);

						$candidatePart = $candidateCount;
						if ($candidatePart != 0) {
							$candidatperc = 100 / intval($candidatePart);
						} else {
							$candidatperc = 0;
						}

						foreach ($depense["estimates"] as $bg_id => $bdg) {
							if (!empty($bdg["percentage"])) {
								$candidatperc = $bdg["percentage"];
							}

							/*if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
								$candidatassignBudget = 0;
								if (!empty($bdg["assignBudget"])) {
									$candidatassignBudget = $bdg["assignBudget"];
								}
							}*/

							$candidatassignBudget = 0;
							if (!empty($bdg["AssignBudgetArray"])) {
								foreach ($bdg["AssignBudgetArray"] as $index => $value) {
									//if(!empty($value["check"]) && $value["check"] == "true") {
									if (isset($value["price"])) {
										$candidatassignBudget += $value['price'];
									}
									//}
								}
							}

							if (MongoId::isValid($bg_id) && (empty($bdg["deny"]) || $bdg["deny"] == false)) {

								if (isset($communityname[$bg_id])) {
									$personname = $communityname[$bg_id];
								} else {
									$personname = PHDB::findOneById(Person::COLLECTION, $bg_id)["name"];
									$communityname[$bg_id] = $personname;
								}
								$tablecandidatename[] = $personname;

								if (!empty($bdg["validate"]) && $bdg["validate"] == "validated") {
									$tablecandidatestatus[] = "validé";
									if (!empty($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "percentage") {
										$personnamecell = "<div class='dropdown'>
                                          <a class='dropdown-toggle coremudrbtn coremubtnsuccess dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                            " . $personname . "
                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                          </a>
                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                            <li role='presentation'><a class='coremudrline valideline ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".validate' data-id='" . $idanswer . "' data-action=''>invalider candidat</a></li>
                                            <li role='presentation'><a class='coremudrline paybudgetperc ' data-candidat='". $bg_id ."' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".status' data-id='" . $idanswer . "' data-action=''>payer ce candidat</a></li>
                                          </ul>
                                        </div>";
									} else {
										$personnamecell = "<div class='dropdown'>
                                          <a class='dropdown-toggle coremudrbtn coremubtnsuccess dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                            " . $personname . "
                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                          </a>
                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                            <li role='presentation'><a class='coremudrline btnAssignBudgetArrayT' data-max='" . $candidatassignBudget . "' data-assignpricelist='" . json_encode(@$bdg["AssignBudgetArray"]) . "' data-uid='" . $bg_id . "' data-key='depense' data-form='aapStep1' data-pos='" . $iddepense . "' data-id='" . $idanswer . "' data-action='validated'> ajouter corénumeration</a></li>
                                            <li role='presentation' class='divider'></li>
                                            <li role='presentation'><a class='coremudrline valideline ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".validate' data-id='" . $idanswer . "' data-action=''>invalider candidat</a></li>
                                            <li role='presentation' class='divider'></li>
                                            <li role='presentation'><a class='coremudrline paybudgetperc ' data-candidat='". $bg_id ."' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".status' data-id='" . $idanswer . "' data-action=''>payer ce candidat</a></li>
                                          </ul>
                                        </div>";
									}
								} else {
									$tablecandidatestatus[] = "non validé";
									$personnamecell = "<div class='dropdown'>
                                          <a class='dropdown-toggle coremudrbtn coremubtnerror dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                            " . $personname . "
                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                          </a>
                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                            <li role='presentation'><a class='coremudrline valideline ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".validate' data-id='" . $idanswer . "' data-action='validated'>valider candidat</a></li>
                                          </ul>
                                        </div>";
								}

								if (!empty($bdg["validate"]) && $bdg["validate"] == "validated") {
									if (!empty($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "percentage") {
										$arrLine = array();

										$sousMontant = "<div class='dropdown'>
                                                          <a class='dropdown-toggle coremudrbtn coremubtnsuccess dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                                            " . round(($depense["price"] * $candidatperc) / 100, 2) . "
                                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                                          </a>
                                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                                            <li role='presentation'><a class='coremudrline validebudgetperc ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".percentageState' data-id='" . $idanswer . "' data-action=''>invalider corému</a></li>
                                                          </ul>
                                                        </div>";

										if (!empty($bdg["percentageState"]) && $bdg["percentageState"] == "validated") {
											if ($onefield) {
												$arrLine = array(
													"poste de depense"               => $candidatebtn,
													//"badge requis"                   => (!empty($badge) ? $badge : " "),
													"montant du depense"             => @$depense["price"],
													"candidat(e)"                    => $personnamecell,
													"montant total pour le candidat" => round(($depense["price"] * $candidatperc) / 100, 2),
													"Action"                     => $candidatperc . "%",
													"credit"                         => $sousMontant,
												);
											} else {
												$arrLine = array(
													"ligne de depense"               => $answer['answers']["aapStep1"]["titre"],
													"poste de depense"               => $candidatebtn,
													//"badge requis"                   => (!empty($badge) ? $badge : " "),
													"montant du depense"             => @$depense["price"],
													"candidat(e)"                    => $personnamecell,
													"montant total pour le candidat" => round(($depense["price"] * $candidatperc) / 100, 2),
													"Action"                     => $candidatperc . "%",
													"credit"                         => $sousMontant,
												);
											}

											$tablestatus[] = "validé";
										} else {
											if ($onefield) {
												$arrLine = array(
													"Milestone"               => $candidatebtn,
													//"badge requis"                   => (!empty($badge) ? $badge : " "),
													"montant du depense"             => @$depense["price"],
                                                    "Action"                     => $candidatperc . "%",
                                                    "credit"                         => $sousMontant,
													"candidat(e)"                    => $personnamecell,
													"montant total pour le candidat" => round(($depense["price"] * $candidatperc) / 100, 2),

												);
											} else {
												$arrLine = array(
													"ligne de depense"               => $answer['answers']["aapStep1"]["titre"],
													"Milestone"               => $candidatebtn,
													//"badge requis"                   => (!empty($badge) ? $badge : " "),
													"montant du depense"             => @$depense["price"],
                                                    "Action"                     => $candidatperc . "%",
                                                    "credit"                         => $sousMontant,
													"candidat(e)"                    => $personnamecell,
													"montant total pour le candidat" => round(($depense["price"] * $candidatperc) / 100, 2),

												);
											}

											$tablestatus[] = "non validé";
										}
										$tableAnswers[] = $arrLine;
									} else {
										if (!empty($bdg["AssignBudgetArray"])) {
											foreach ($bdg["AssignBudgetArray"] as $index => $value) {
                                                $arrLine = array();
                                                if (empty($value["price"])) {
                                                    $value["price"] = 0;
                                                }
                                                if (empty($value["label"])) {
                                                    $value["label"] = "";
                                                }
                                                if (isset($value["check"]) && $value["check"] == "true" && (empty($value["status"]) || $value["status"] != "paid")) {
                                                    $sousMontant = "<div class='dropdown'>
                                                      <a class='dropdown-toggle coremudrbtn coremubtnsuccess dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                                        " . $value["price"] . "
                                                        <i class='fa fa-add-user'></i> <span class='caret'></span>
                                                      </a>
                                                      <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                                        <li role='presentation'><a class='coremudrline validebudgetasg ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".AssignBudgetArray." . $index . ".check' data-id='" . $idanswer . "' data-action='false'>invalider corému</a></li>
                                                      </ul>
                                                    </div>";

                                                    if ($onefield) {
                                                        $arrLine = array(
                                                            "Milestone" => $candidatebtn,
                                                            //"badge requis" => (!empty($badge) ? $badge : " "),
                                                            "montant du depense" => @$depense["price"],
                                                            "Action" => $value["label"],
                                                            "credit" => $sousMontant,
                                                            "candidat(e)" => $personnamecell,
                                                            "montant total pour le candidat" => $candidatassignBudget,

                                                        );

                                                        $tablestatus[] = "validé";
                                                    } else {
                                                        $arrLine = array(
                                                            "ligne de depense" => $answer['answers']["aapStep1"]["titre"],
                                                            "Milestone" => $candidatebtn,
                                                            //"badge requis" => (!empty($badge) ? $badge : " "),
                                                            "montant du depense" => @$depense["price"],
                                                            "Action" => $value["label"],
                                                            "credit" => $sousMontant,
                                                            "candidat(e)" => $personnamecell,
                                                            "montant total pour le candidat" => $candidatassignBudget,

                                                        );

                                                        $tablestatus[] = "validé";
                                                    }
                                                } else if (!empty($value["status"]) && $value["status"] == "paid") {
                                                    $sousMontant = $value["price"];

                                                    if ($onefield) {
                                                        $arrLine = array(
                                                            "Milestone" => $candidatebtn,
                                                            //"badge requis" => (!empty($badge) ? $badge : " "),
                                                            "montant du depense" => @$depense["price"],
                                                            "Action" => $value["label"],
                                                            "credit" => $sousMontant,
                                                            "candidat(e)" => $personnamecell,
                                                            "montant total pour le candidat" => $candidatassignBudget,

                                                        );

                                                        $tablestatus[] = "validé";
                                                    } else {
                                                        $arrLine = array(
                                                            "ligne de depense" => $answer['answers']["aapStep1"]["titre"],
                                                            "Milestone" => $candidatebtn,
                                                            //"badge requis" => (!empty($badge) ? $badge : " "),
                                                            "montant du depense" => @$depense["price"],
                                                            "Action" => $value["label"],
                                                            "credit" => $sousMontant,
                                                            "candidat(e)" => $personnamecell,
                                                            "montant total pour le candidat" => $candidatassignBudget,

                                                        );

                                                        $tablestatus[] = "validé";
                                                    }
                                                } else {
													$sousMontant = "<div class='dropdown'>
                                                      <a class='dropdown-toggle coremudrbtn coremubtnerror dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                                        " . $value["price"] . "
                                                        <i class='fa fa-add-user'></i> <span class='caret'></span>
                                                      </a>
                                                      <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                                        <li role='presentation'><a class='coremudrline validebudgetasg ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".AssignBudgetArray." . $index . ".check' data-id='" . $idanswer . "' data-action='true'>valider corému</a></li>
                                                      </ul>
                                                    </div>";

													if ($onefield) {
														$arrLine = array(
															"Milestone"               => $candidatebtn,
															//"badge requis"                   => (!empty($badge) ? $badge : " "),
															"montant du depense"             => @$depense["price"],
                                                            "Action"                     => $value["label"],
                                                            "credit"                         => $sousMontant,
															"candidat(e)"                    => $personnamecell,
															"montant total pour le candidat" => $candidatassignBudget,

														);

														$tablestatus[] = "non validé";
													} else {
														$arrLine = array(
															"ligne de depense"               => $answer['answers']["aapStep1"]["titre"],
															"Milestone"               => $candidatebtn,
															//"badge requis"                   => (!empty($badge) ? $badge : " "),
															"montant du depense"             => @$depense["price"],
                                                            "Action"                     => $value["label"],
                                                            "credit"                         => $sousMontant,
															"candidat(e)"                    => $personnamecell,
															"montant total pour le candidat" => $candidatassignBudget,

														);

														$tablestatus[] = "non validé";
													}
												}
												$tableAnswers[] = $arrLine;
											}
										} else {

											if ($onefield) {
												$arrLine = array(
													"Milestone"               => $candidatebtn,
													//"badge requis"                   => (!empty($badge) ? $badge : " "),
													"montant du depense"             => @$depense["price"],
                                                    "Action"                     => "(aucun)",
                                                    "credit"                         => "(aucun)",
													"candidat(e)"                    => $personnamecell,
													"montant total pour le candidat" => $candidatassignBudget,

												);

												$tablestatus[] = "non assigné";
											} else {
												$arrLine = array(
													"ligne de depense"               => $answer['answers']["aapStep1"]["titre"],
													"Milestone"               => $candidatebtn,
													//"badge requis"                   => (!empty($badge) ? $badge : " "),
													"montant du depense"             => @$depense["price"],
                                                    "Action"                     => "(aucun)",
                                                    "credit"                         => "(aucun)",
													"candidat(e)"                    => $personnamecell,
													"montant total pour le candidat" => $candidatassignBudget,
												);

												$tablestatus[] = "non assigné";
											}
											$tableAnswers[] = $arrLine;
										}
									}

									if (!$tableonly) {
										if (!in_array($personname, $propopielabels)) {
											$propopielabels[] = $personname;
											$propopiedata[array_search($personname, $propopielabels)] = 0;
											$propobardata[0][array_search($personname, $propopielabels)] = 0;
											$propobardata[1][array_search($personname, $propopielabels)] = 0;
										}

										if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
											$propobardata[0][array_search($personname, $propopielabels)] += $candidatassignBudget;
										} else {
											$propobardata[0][array_search($personname, $propopielabels)] += round(($depense["price"] * $candidatperc) / 100, 2);
										}
									}
								} else {
									if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "percentage") {
										if ($onefield) {
											$arrLine = array(
												"Milestone"               => $candidatebtn,
												//"badge requis"                   => (!empty($badge) ? $badge : " "),
												"montant du depense"             => @$depense["price"],
                                                "Action"                     => "(candidat(e) pas validé(e))",
                                                "credit"                         => "(vide)",
												"candidat(e)"                    => $personnamecell,
												"montant total pour le candidat" => " "
											);

											$tablestatus[] = "non assigné";
										} else {
											$arrLine = array(
												"ligne de depense"               => $answer['answers']["aapStep1"]["titre"],
												"Milestone"               => $candidatebtn,
												//"badge requis"                   => (!empty($badge) ? $badge : " "),
												"montant du depense"             => @$depense["price"],
                                                "Action"                     => "(candidat(e) pas validé(e))",
                                                "credit"                         => "(vide)",
												"candidat(e)"                    => $personnamecell,
												"montant total pour le candidat" => " ",

											);

											$tablestatus[] = "non assigné";
										}
										$tableAnswers[] = $arrLine;
									} else {
										if ($onefield) {
											$arrLine = array(
												"Milestone"               => $candidatebtn,
												//"badge requis"                   => (!empty($badge) ? $badge : " "),
												"montant du depense"             => @$depense["price"],
                                                "Action"                     => "(candidat(e) pas validé(e))",
                                                "credit"                         => "(vide)",
												"candidat(e)"                    => $personnamecell,
												"montant total pour le candidat" => $candidatassignBudget,

											);

											$tablestatus[] = "non assigné";
										} else {
											$arrLine = array(
												"ligne de depense"               => $answer['answers']["aapStep1"]["titre"],
												"Milestone"               => $candidatebtn,
												//"badge requis"                   => (!empty($badge) ? $badge : " "),
												"montant du depense"             => @$depense["price"],
                                                "Action"                     => "(candidat(e) pas validé(e))",
                                                "credit"                         => "(vide)",
												"candidat(e)"                    => $personnamecell,
												"montant total pour le candidat" => $candidatassignBudget,

											);

											$tablestatus[] = "non assigné";
										}
										$tableAnswers[] = $arrLine;
									}

									if (!$tableonly) {
										if (!in_array($personname, $propopielabels)) {
											$propopielabels[] = $personname;
											$propopiedata[array_search($personname, $propopielabels)] = 0;
											$propobardata[0][array_search($personname, $propopielabels)] = 0;
											$propobardata[1][array_search($personname, $propopielabels)] = 0;
										}

										if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
											$propobardata[1][array_search($personname, $propopielabels)] += $candidatassignBudget;
										} else {
											$propobardata[1][array_search($personname, $propopielabels)] += round(($depense["price"] * $candidatperc) / 100, 2);
										}
									}
								}

								if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
									$tableprice[] = $candidatassignBudget;
								} elseif (!empty($depense["price"])) {
									$tableprice[] = round(($depense["price"] * $candidatperc) / 100, 2);
								} else {
									$tableprice[] = 0;
								}
								if (!$tableonly) {
									$tablepercentage[] = $candidatperc;
									if (!in_array($personname, $propopielabels)) {
										$propopielabels[] = $personname;
										$propopiedata[array_search($personname, $propopielabels)] = 0;
										$propobardata[0][array_search($personname, $propopielabels)] = 0;
										$propobardata[1][array_search($personname, $propopielabels)] = 0;
									}

									if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
										$propopiedata[array_search($personname, $propopielabels)] += $candidatassignBudget;
									} else {
										$propopiedata[array_search($personname, $propopielabels)] += round(($depense["price"] * $candidatperc) / 100, 2);
									}

									if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
										$tableSankeyLinks[] = array(
											"target" => str_replace(["'", '"'], "&#039", @$depense["poste"]),
											"source" => str_replace(["'", '"'], "&#039", @$depense["poste"]),
											"value"  => $candidatassignBudget
										);
									} else {
										$tableSankeyLinks[] = array(
											"target" => str_replace(["'", '"'], "&#039", @$depense["poste"]),
											"source" => str_replace(["'", '"'], "&#039", @$depense["poste"]),
											"value"  => round(($depense["price"] * $candidatperc) / 100, 2)
										);
									}

									if (!in_array(array(
										"name" => str_replace(["'", '"'], "&#039", @$depense["poste"]),
										"level" => 0
									), $tableSankeyNodes, true)) {
										$tableSankeyNodes[] = array("name" => str_replace(["'", '"'], "&#039", @$depense["poste"]), "level" => 0);
									}

									if (!in_array(array(
										"name" => str_replace(["'", '"'], "&#039", @$depense["poste"]),
										"level" => 1
									), $tableSankeyNodes, true)) {
										$tableSankeyNodes[] = array("name" => str_replace(["'", '"'], "&#039", @$depense["poste"]), "level" => 1);
									}
								}
							}
						}
					} else {
						if ($onefield) {
							$arrLine = array(
								"Milestone"               => $candidatebtn,
								//"badge requis"                   => (!empty($badge) ? $badge : " "),
								"montant du depense"             => @$depense["price"],
								"candidat(e)"                    => " ",
								"montant total pour le candidat" => " ",
								"Action"                     => " ",
								"credit"                         => "(vide)",
							);

							$tablestatus[] = "non assigné";
						} else {
							$arrLine = array(
								"ligne de depense"               => $answer['answers']["aapStep1"]["titre"],
								"Milestone"               => $candidatebtn,
								//"badge requis"                   => (!empty($badge) ? $badge : " "),
								"montant du depense"             => @$depense["price"],
								"candidat(e)"                    => " ",
								"montant total pour le candidat" => " ",
								"Action"                     => " ",
								"credit"                         => "(vide)",
							);

							$tablestatus[] = "non assigné";
						}
						$tableAnswers[] = $arrLine;
					}

					if (empty($depense["estimates"][Yii::app()->session["userId"]])) {

						//candidater

						$badge = [];
						$candidate = [];
						if (isset($depense["badge"])) {
							foreach ($depense["badge"] as $bdg) {
								$badge[] = PHDB::findOneById(Badge::COLLECTION, $bdg)["name"];
							}
							//$badge[] = $depense["badge"];
						}

						if (!empty($depense["price"])) {
							$tableprice[] = $depense["price"];
						}
						if (!$tableonly) {
							if (!in_array('non spécifié', $propopielabels)) {
								$propopielabels[] = 'non spécifié';
								$propopiedata[array_search('non spécifié', $propopielabels)] = 0;
							}

							if (!empty($depense["price"])) {
								$propopiedata[array_search('non spécifié', $propopielabels)] += $depense["price"];
							}

							$tableSankeyLinks[] = array(
								"target" => str_replace(["'", '"'], "&#039", @$depense["poste"]),
								"source" => "non spécifié",
								"value"  => !empty($depense["price"]) ? $depense["price"] : 0
							);

							if (!in_array(array("name" => "non spécifié", "level" => 0), $tableSankeyNodes, true)) {
								$tableSankeyNodes[] = array("name" => "non spécifié", "level" => 0);
							}

							if (!in_array(array(
								"name" => str_replace(["'", '"'], "&#039", @$depense["poste"]),
								"level" => 1
							), $tableSankeyNodes, true)) {
								$tableSankeyNodes[] = array("name" => str_replace(["'", '"'], "&#039", @$depense["poste"]), "level" => 1);
							}
						}
					}
				}
				if (!empty($answer["answers"]["aapStep1"]["depense"])) {
					$depenseAction = Aap::getDepenseAction($answer["answers"]["aapStep1"]["depense"]);
					$totalcandidattovalid = $depenseAction["totalcandidattovalid"];
					$totalpricetovalid = $depenseAction["totalpricetovalid"];
					$totalneedcandidat = $depenseAction["totalneedcandidat"];
				}

				$totalansweraction = $totalcandidattovalid + $totalpricetovalid + $totalneedcandidat;
			}
		}

		if (!$tableonly) {
			return array(
				"tableAnswers"         => $tableAnswers,
				"tableSankey"          => $tableSankey,
				"tablepercentage"      => $tablepercentage,
				"tableprice"           => $tableprice,
				"tablestatus"          => $tablestatus,
				"tablecandidatename"   => $tablecandidatename,
				"tablecandidatestatus" => $tablecandidatestatus,
				"propopiedata"         => $propopiedata,
				"propopielabels"       => $propopielabels,
				"propobarlabel"        => $propobarlabel,
				"tableSankeyLinks"     => $tableSankeyLinks,
				"tableSankeyNodes"     => $tableSankeyNodes,
				"notificationbar"      => $notificationbar,
				"realpropositiondata"  => $realpropositiondata,
				"propobardata"         => $propobardata,
				"totalcandidattovalid" => $totalcandidattovalid,
				"totalpricetovalid"    => $totalpricetovalid,
				"totalneedcandidat"    => $totalneedcandidat,
				"totalansweraction"    => $totalansweraction,

			);
		} else {
			return array(
				"tableAnswers"       => $tableAnswers,
				"tableprice"         => $tableprice,
				"tablestatus"        => $tablestatus,
				"tablecandidatename" => $tablecandidatename,
			);
		}
	}

	public static function answerCoremuFinancerVariables($answers) {
		$tableAnswers = array();

		$tableSankeyLinks = array();
		$tableSankeyNodes = array(array("name" => "sans financeur"));
		foreach ($answers as $idanswer => $answer) {

			$answerform = PHDB::findOneById(Form::ANSWER_COLLECTION, (string)$answer["_id"]);
			$formAns = PHDB::findOneById(Form::COLLECTION, $answerform["form"]);
			$contextAns = PHDB::findOneById($formAns["parent"][array_keys($formAns["parent"])[0]]["type"], array_keys($formAns["parent"])[0]);

			$tableSankeyNodes[0] = array("name" => $contextAns["name"]);

			if (isset($answer['answers']["aapStep1"]["titre"]) && isset($answer['answers']["aapStep1"]["depense"])) {
				foreach ($answer['answers']["aapStep1"]["depense"] as $iddepense => $depense) {
					if (!empty($depense["financer"])) {

						foreach ($depense["financer"] as $bg_id => $bdg) {

							$tableAnswers[] = array(
								"Milestone"   => $depense["poste"],
								"depense"            => !empty($depense["price"]) ? $depense["price"] : 0,
								"libelé financeur"   => (!empty($bdg["name"]) ? $bdg["name"] : $contextAns["name"]),
								"libelé financement" => @$bdg["line"],
								"recette"            => @$bdg["amount"]
							);
						}
					}
				}
			}
		}

		return array(
			"tableAnswers" => $tableAnswers,
		);
	}

	public static function matchBadge($depenseLine, $communityLine) {
		$haveBadges = false;
		if (!empty($depenseLine["badge"])) {
			$haveBadges = false;
			if (!empty($communityLine["badge"])) {
				$haveBadges = true;
				foreach ($depenseLine["badge"] as $badge) {
					if (empty($communityLine["badge"][$badge])) {
						$haveBadges = false;
					}
				}
			} else {
				$haveBadges = false;
			}
		} else {
			$haveBadges = true;
		}
		return $haveBadges;
	}

	public static function countAvalaiblecandidate($depenseLine) {
		$candidatecount = 0;
		if (!empty($depenseLine["estimates"])) {
			foreach ($depenseLine["estimates"] as $depenseEstimateId => $depenseEstimate) {
				if (MongoId::isValid($depenseEstimateId) && (empty($depenseEstimate["deny"]) || $depenseEstimate["deny"] == false)) {
					$candidatecount++;
				}
			}
		}
		return $candidatecount;
	}

	public static function countValidecandidate($depenseLine) {
		$candidatecount = 0;
		if (!empty($depenseLine["estimates"])) {
			foreach ($depenseLine["estimates"] as $depenseEstimateId => $depenseEstimate) {
				if (MongoId::isValid($depenseEstimateId) && (empty($depenseEstimate["deny"]) || $depenseEstimate["deny"] == false) && (!empty($depenseEstimate["validate"]) && $depenseEstimate["validate"] == "validated")) {
					$candidatecount++;
				}
			}
		}
		return $candidatecount;
	}

	public static function countPriceEstimate($depenseLine) {
		$countpriceestimate = 0;
		if (isset($depenseLine["estimates"])) {

			foreach ($depenseLine["estimates"] as $estimateId => $estimate) {
				if (!MongoId::isValid($estimateId) && (empty($estimate["ispriceselected"]) || $estimate["ispriceselected"] != true)) {
					$countpriceestimate += 1;
				}
			}
		}
		return $countpriceestimate;
	}

	public static function getpriceEstimateSelected($depenseLine) {
		if (isset($depenseLine["estimates"])) {
			foreach ($depenseLine["estimates"] as $estimateId => $estimate) {
				if (!empty($estimate["ispriceselected"]) && $estimate["ispriceselected"] == true) {
					return $estimateId;
				}
			}
		}
		return false;
	}

	public static function getTotalDep($depenseLine) {
		$total = 0;
		if (isset($depenseLine["switchAmountAttr"]) && $depenseLine["switchAmountAttr"] == "assignBudget") {
			if (!empty($depenseLine["price"])) {
				if (!empty($depenseLine["estimates"])) {
					foreach ($depenseLine["estimates"] as $index => $estimate) {
						if (!empty($estimate["AssignBudgetArray"])) {
							foreach ($estimate["AssignBudgetArray"] as $index2 => $value2) {
								$total += $value2["price"];
							}
						}
					}
					return $total;
				} else {
					return $total;
				}
			}
		}
		return false;
	}

	public static function getEstimateInfo($estimateId, $estimate) {
		$actualCandidate = array();
		$profilPicture = "";
		$uid = null;
		if (MongoId::isValid($estimateId) && (empty($estimate["deny"]) || $estimate["deny"] == false)) {
			$actualCandidate = PHDB::findOneById(Person::COLLECTION, $uid);
			if (isset($actualCandidate["profilImageUrl"])) {
				$profilPicture = Yii::app()->createUrl('/' . $actualCandidate["profilImageUrl"]);
			} else {
				$profilPicture = Yii::app()->getModule("co2")->assetsUrl . '/images/thumb/default_citoyens.png';
			}
		}

		return array(
			"actualCandidate" => $actualCandidate,
			"profilPicture"   => $profilPicture,
		);
	}

	public static function getDepenseAction($answer) {
		$totalAction = 0;

		$totalcandidattovalid = 0;
		$totalpricetovalid = 0;
		$totalneedcandidat = 0;

		$candidattovalid = array();
		$pricetovalid = array();
		$needcandidat = array();

		foreach ($answer as $depId => $depense) {
			$candidattovalid[$depId] = 0;
			$pricetovalid[$depId] = 0;
			$needcandidat[$depId] = 0;

			if (!empty($depense["estimates"])) {
				if (
					self::countAvalaiblecandidate($depense) == 0
					|| (!empty($depense["candidateNumber"]) && self::countValidecandidate($depense) != $depense["candidateNumber"])
				) {
					$totalneedcandidat++;
					$needcandidat[$depId]++;
				}

				foreach ($depense["estimates"] as $esti => $estimate) {
					if (MongoId::isValid($esti)) {
						if (empty($estimate["validate"]) || $estimate["validate"] != "validated") {
							$candidattovalid[$depId]++;
							$totalcandidattovalid++;
						}
						if (!empty($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "percentage") {
							if (empty($estimate["percentageState"]) || $estimate["percentageState"] != "validated") {
								$pricetovalid[$depId]++;
								$totalpricetovalid++;
							}
						} else {
							if (!empty($estimate["AssignBudgetArray"])) {
								foreach ($estimate["AssignBudgetArray"] as $bdgarray) {
									if (empty($bdgarray["check"]) || $bdgarray["check"] != "true") {
										$pricetovalid[$depId]++;
										$totalpricetovalid++;
									}
								}
							}
						}
					}
				}
			} else {
				$totalneedcandidat++;
				$needcandidat[$depId]++;
			}
		}

		return array(
			'totalcandidattovalid' => $totalcandidattovalid,
			'totalpricetovalid'    => $totalpricetovalid,
			'totalneedcandidat'    => $totalneedcandidat,
			'candidattovalid'      => $candidattovalid,
			'pricetovalid'         => $pricetovalid,
			'needcandidat'         => $needcandidat,
		);
	}
	//****************************************************************************************//
	//***********************************REFACTOR FUNCTION ***********************************//
	//***********************************REFACTOR FUNCTION ***********************************//
	//****************************************************************************************//
	private static function globalAutocompleteProposalQuery($form, $searchParams, $newcounter, $moreParams = []) {
		$form = PHDB::findOneById(Form::COLLECTION, $form);
		$searchParams["indexMin"] = (isset($searchParams["indexMin"])) ? $searchParams["indexMin"] : 0;
		$searchParams["indexStep"] = (isset($searchParams["indexStep"])) ? $searchParams["indexStep"] : 100;
		$mappingData = (isset($form["mapping"])) ? $form["mapping"] : array(
			"name" => "name",
			"address" => "address",
			"answers.aapStep1.titre" => "answers.aapStep1.titre"
		);

		$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
		$elId = array_keys($form["parent"])[0];
		$elType = $form["parent"][$elId]["type"];
		$el = PHDB::findOneById($elType, $elId);
		$isAdmin = (Authorisation::isElementAdmin($elId, $elType, Yii::app()->session['userId']) || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]));

		$formStandalone = false;
		if (!empty($searchParams["filters"]["formStandalone"])) {
			$formStandalone = (bool)$searchParams["filters"]["formStandalone"];
			unset($searchParams["filters"]["formStandalone"]);
		}


		$linkType = "members";
		if ($elType == Project::COLLECTION)
			$linkType = "contributors";
		$isMembers = !empty(Yii::app()->session['userId']) && Authorisation::isElementMember((string)$el["_id"], @$el["collection"], Yii::app()->session['userId']);


		$roles = [];
		$aapStep1Roles = (isset($form["params"]["aapStep1"]["haveEditingRules"]) && $form["params"]["aapStep1"]["haveEditingRules"] == "true") ? explode(",", $form["params"]["aapStep1"]["canEdit"]) : [];
		$aapStep2Roles = (isset($form["params"]["aapStep2"]["haveEditingRules"]) && $form["params"]["aapStep2"]["haveEditingRules"] == "true") ? explode(",", $form["params"]["aapStep2"]["canEdit"]) : [];
		$aapStep3Roles = (isset($form["params"]["aapStep3"]["haveEditingRules"]) && $form["params"]["aapStep3"]["haveEditingRules"] == "true") ? explode(",", $form["params"]["aapStep3"]["canEdit"]) : [];
		$aapStep4Roles = (isset($form["params"]["aapStep4"]["haveEditingRules"]) && $form["params"]["aapStep4"]["haveEditingRules"] == "true") ? explode(",", $form["params"]["aapStep4"]["canEdit"]) : [];

		if (!empty($el["links"][$linkType][Yii::app()->session['userId']]["roles"]))
			$roles = $el["links"][$linkType][Yii::app()->session['userId']]["roles"];

		$query = array();
		$fieldPrefix = isset($moreParams["fieldPrefix"]) ? $moreParams["fieldPrefix"] . "." : "";

		if (!empty($searchParams["name"])) {
			if (!empty($searchParams["textPath"]))
				$query = SearchNew::searchText($searchParams["name"], $query, array("textPath" => $searchParams["textPath"]));
			else
				$query = SearchNew::searchText($searchParams["name"], $query);
		}

		if (!empty($searchParams["filters"]["form"])) {
			$query = SearchNew::addQuery(
				$query,
				[
					'$or' => [
						["form" => $searchParams["filters"]["form"]],
						["answers.aapStep2.choose.$elId.value" => "selected"],
						["links.aacForm.".(string) $form["_id"] => ['$exists' => true]]
					]
				]
			);
			unset($searchParams["filters"]["form"]);
		}

		if (!empty($searchParams["filters"]["address"])) {
			$pathToAddress = (isset($mappingData["address"]["path"])) ? $mappingData["address"]["path"] : $mappingData["address"];
			$searchRegExp = SearchNew::accentToRegex(trim(urldecode($searchParams["filters"]['address'])));
			$query = SearchNew::addQuery(
				$query,
				array(
					'$or' => array(
						array($fieldPrefix . $pathToAddress . ".postalCode" => new MongoRegex("/.*{$searchRegExp}.*/i")),
						array($fieldPrefix . $pathToAddress . ".name" => new MongoRegex("/.*{$searchRegExp}.*/i"))
					)
				)
			);
			unset($searchParams["filters"]["address"]);
		}

		if (!empty($searchParams["filters"]["views"][0])) {
			$views = $searchParams["filters"]["views"][0];
			if ($views === "notSeen")
				$query = SearchNew::addQuery($query, array($fieldPrefix . "views." . Yii::app()->session['userId'] => ['$exists' => false]));
			if ($views === "seen")
				$query = SearchNew::addQuery($query, array($fieldPrefix . "views." . Yii::app()->session['userId'] => ['$exists' => true]));
			unset($searchParams["filters"]["views"]);
		}

		if (!empty($searchParams["filters"]["quartiers"])) {
			$quartiers = $searchParams["filters"]["quartiers"];
			$query = SearchNew::addQuery($query, array($fieldPrefix . 'answers.aapStep1.interventionArea' => ['$in' => $quartiers]));
			unset($searchParams["filters"]["quartiers"]);
		}

		if (!empty($searchParams["filters"])) {
			foreach ($searchParams["filters"] as $filtersKey => $filtersValue) {
				if (strpos($filtersKey, '.choose.') !== false) {
					$queryPar = [
						'$or' => [
							[$fieldPrefix . $filtersKey => ['$in' => $filtersValue]]
						]
					];
					foreach ($searchParams["filters"][$filtersKey] as $fKey => $fValue) {
						if ($fValue == "notselected") {
							$queryPar['$or'][] = [$fieldPrefix . $filtersKey => ['$exists' => false]];
						}
					}
					$query = SearchNew::addQuery($query, $queryPar);
					unset($searchParams["filters"][$filtersKey]);
				}
			}
		}

		/*if ($searchParams["searchType"][0] == "answers" && empty($searchParams["filters"]["oneSubOrganization"]))
				$query = SearchNew::addQuery($query, array("form" => (string)@$form["_id"]));*/

		if (!empty($form["params"]["onlyAdminCanSeeList"]) && $form["params"]["onlyAdminCanSeeList"] && !$formStandalone) {
			if (!$isAdmin) {
				if (!$isMembers || ($isMembers && count(array_intersect($roles, $aapStep2Roles)) == 0) || ($isMembers && count(array_intersect($roles, $aapStep3Roles)) == 0)) {
					$query = SearchNew::addQuery($query, array($fieldPrefix . "user" => Yii::app()->session['userId']));
				} else {
					$params = array(
						'$or' => array(
							array($fieldPrefix . "user" => Yii::app()->session['userId'])
						)
					);
					if ($isMembers && count(array_intersect($roles, $aapStep2Roles)) > 0)
						$params['$or'][] = array($fieldPrefix . "status" => "vote");

					if ($isMembers && count(array_intersect($roles, $aapStep3Roles)) > 0)
						$params['$or'][] = array($fieldPrefix . "status" => "vote");

					$query = SearchNew::addQuery($query, $params);
				}
			}
		}

		if (!empty($searchParams["filters"]["admissibility"])) {
			$admissibility = $searchParams["filters"]["admissibility"][0];
			$q = array(
				'$and' => [
					array('$or' => [])
				]
			);
			$members = Element::getCommunityByTypeAndId($elType, $elId, "citoyens");
			foreach ($members as $kMemb => $vMemb) {
				if ($admissibility == "admissible")
					$q['$and'][0]['$or'][] = array($fieldPrefix . "answers.aapStep2.admissibility." . $kMemb => ['$ne' => "inadmissible"]);
				elseif ($admissibility == "inadmissible") {
					$q['$and'][0]['$or'][] = array($fieldPrefix . "answers.aapStep2.admissibility." . $kMemb => "inadmissible");
				}
			}
			if (!empty($members))
				$query = SearchNew::addQuery($query, $q);
			unset($searchParams["filters"]["admissibility"]);
		}

		if (!empty($searchParams["filters"]["answers.aapStep1.depense.financer.idAndName"])) {
			$dataFinancors = explode("-idAndName-", $searchParams["filters"]["answers.aapStep1.depense.financer.idAndName"][0]);
			$idFinancor = $dataFinancors[0];
			$nameFinancor = $dataFinancors[1];
			$queryFinancors = array(
				'$and' => [
					array(
						'$or' => [
							array($fieldPrefix . "answers.aapStep1.depense.financer.name" => $nameFinancor),
							array($fieldPrefix . "answers.aapStep1.depense.financer.id" => $idFinancor),
						]
					)
				]
			);

			$query = SearchNew::addQuery($query, $queryFinancors);
			unset($searchParams["filters"]["answers.aapStep1.depense.financer.idAndName"]);
		}

		if (!empty($searchParams["filters"]["oneSubOrganization"]) && !empty($searchParams["filters"]["form"])) {
			$prms = $searchParams["filters"]["oneSubOrganization"];
			$parseParams = [];
			foreach ($prms as $kprms => $vprms) {
				if (strpos($vprms, "-")) {
					$vprms = explode('-', $vprms);
					foreach ($vprms as $kk => $vv) {
						$parseParams[] = $vv;
					}
				} else {
					$parseParams[] = $vprms;
				}
			}
			$query = SearchNew::addQuery($query, array($fieldPrefix . "form" => ['$in' => $parseParams]));
			unset($searchParams["filters"]["oneSubOrganization"]);
			if (!empty($searchParams["filters"]["allSubOrganisation"]))
				unset($searchParams["filters"]["allSubOrganisation"]);
			unset($searchParams["filters"]["form"]);
		}

		if (!empty($searchParams["filters"]["allSubOrganisation"])) {
			$prms = $searchParams["filters"]["allSubOrganisation"];
			$prms[] = $searchParams["filters"]["form"];

			$query = SearchNew::addQuery($query, array($fieldPrefix . "form" => ['$in' => $prms]));
			unset($searchParams["filters"]["allSubOrganisation"]);
			unset($searchParams["filters"]["form"]);
		}


		if (!empty($searchParams["userId"])) {
			$query = SearchNew::addQuery($query, array($fieldPrefix . "user" => $searchParams["userId"]));
		}

		if (!empty($searchParams["filters"]["status"]) && is_array($searchParams["filters"]["status"])) {
			if (in_array("unnotified", $searchParams["filters"]["status"])) {
				$query = SearchNew::addQuery($query, [
					'$and' => [
						array($fieldPrefix . "status" => ['$ne' => "notified"]),
						array($fieldPrefix . "status" => ['$ne' => "notificationSent"])
					]
				]);
				if (count($searchParams["filters"]["status"]) == 1) {
					unset($searchParams["filters"]["status"]);
				}
			} elseif (in_array("unnotified", $searchParams["filters"]["status"])) {
			}
		}

		if (
			!empty($searchParams["searchTags"]) &&
			(count($searchParams["searchTags"]) > 1 || count($searchParams["searchTags"]) == 1 && $searchParams["searchTags"][0] != "")
		) {
			$operator = (!empty($options) && isset($options["tags"]) && isset($options["tags"]["verb"])) ? $options["tags"]["verb"] : '$in';
			if (!empty($searchParams["tagsPath"])) {
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query, $fieldPrefix . $searchParams["tagsPath"]);
				unset($searchParams["tagsPath"]);
			} else
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query, $fieldPrefix . "tags");
			unset($searchParams["searchTags"]);
		}

		if (!empty($searchParams['filters']['vote'])) {
			$query = SearchNew::addQuery($query, array($fieldPrefix . "vote." . Yii::app()->session['userId'] => ['$exists' => true]));
			unset($searchParams['filters']['vote']);
		}

		if (!empty($searchParams['filters']['inproject'])) {
			unset($searchParams['filters']['project.id']);
			$where = ['$or' => array()];
			if (in_array("inproject", $searchParams['filters']['inproject']))
				$where['$or'][] = array($fieldPrefix . "project.id" => ['$exists' => true]);
			if (in_array("inproposal", $searchParams['filters']['inproject']))
				$where['$or'][] = array($fieldPrefix . "project.id" => ['$exists' => false]);

			$query = SearchNew::addQuery($query, $where);
			unset($searchParams['filters']['inproject']);
		}

		if (!empty($searchParams['sortBy'])) {
			if (Api::isAssociativeArray($searchParams['sortBy'])) {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$key] = (int)$value;
				}
				$searchParams["sortBy"] = $sortBy;
			} else {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$value] = 1;
				}
				$searchParams["sortBy"] = $sortBy;
			}
		} else
			$searchParams['sortBy'] = array("updated" => -1);

		if (empty($searchParams["fields"])) {
			$searchParams["fields"] = array();
		}

		if (!empty($searchParams['filters'])) {
			$query = SearchNew::searchFilters($searchParams['filters'], $query, $moreParams);
		}

		return array("query" => $query, "searchParams" => $searchParams);
	}

	public static function globalAutocompleteProposal($form, $searchParams, $newcounter, $countOnly = false) {
		$queryParams = self::globalAutocompleteProposalQuery($form, $searchParams, $newcounter);
		$query = $queryParams["query"];
		$searchParams = $queryParams["searchParams"];

		$res = array();
		if ($newcounter) {
			if (!empty(Yii::app()->session['userId'])) {
				$query = SearchNew::addQuery($query, array("views." . Yii::app()->session['userId'] => ['$exists' => false]));
				$res["newCounter"] = PHDB::count($searchParams["searchType"][0], $query);
			} else $res["newCounter"] = 0;
		} else {
			if ($countOnly) {
				$res["count"][$searchParams["searchType"][0]] = PHDB::count($searchParams["searchType"][0], $query);
			} else {
				if ($searchParams["indexStep"] == "all") {
					$res["results"] = PHDB::findAndSort(
						$searchParams["searchType"][0],
						$query,
						$searchParams['sortBy'],
						0,
						$searchParams["fields"],
					);
				} else {
					$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex(
						$searchParams["searchType"][0],
						$query,
						$searchParams["fields"],
						$searchParams['sortBy'],
						$searchParams["indexStep"],
						$searchParams["indexMin"]
					);
				}
			}
			if (isset($searchParams["count"]))
				$res["count"][$searchParams["searchType"][0]] = PHDB::count($searchParams["searchType"][0], $query);
			$res = Aap::parsePropositionData($res);
		}

		return $res;
	}

	public static function communsQuery($searchParams, $moreParams = []) {
		$searchParams["indexMin"] = (isset($searchParams["indexMin"])) ? $searchParams["indexMin"] : 0;
		$searchParams["indexStep"] = (isset($searchParams["indexStep"])) ? $searchParams["indexStep"] : 100;
		$parentForms = PHDB::find(Form::COLLECTION, ["type" => "aap", "aapType" => "aac"], ["name", "parent", "type", "config", "aapType"]);
		$query = [];
		$query = SearchNew::addQuery(
			$query, 
			[
				"form" => ['$in' => array_keys($parentForms)],
				"answers" => ['$exists' => true]
			]
		);
		$aapFieldMapping = [
			"title" => "answers.aapStep1.titre"
		];
		if (empty($searchParams["emptyTitle"])) {
			$query = SearchNew::addQuery($query, [$aapFieldMapping["title"] => ['$exists' => true]]);
		}
		if (!empty($searchParams["filters"]["formFilter"])) {
			$aacForms = PHDB::findByIds(Form::COLLECTION, $searchParams["filters"]["formFilter"], ["links", "name", "parent", "type", "aapType"]);
			if (!empty($aacForms)) {
				$aacQuery = [];
				foreach ($aacForms as $aacFormId => $aacForm) {
					$parentFormId = array_keys($aacForm["parent"])[0];
					$aacQuery[] = ["answers.aapStep2.choose.$parentFormId.value" => "selected"];
				}
				$query = SearchNew::addQuery(
					$query,
					[
						'$or' => $aacQuery,
					]
				);
			}
			if (!empty($searchParams["filters"]["form"])) {
				unset($searchParams["filters"]["form"]);
			}
			unset($searchParams["filters"]["formFilter"]);
		}
		if (!empty($searchParams["name"])) {
			if (empty($searchParams["textPath"])) {
				$searchParams["textPath"] = $aapFieldMapping["title"];
			}
			$query = SearchNew::searchText($searchParams["name"], $query, array("textPath" => $searchParams["textPath"]));
		}
		if (
			!empty($searchParams["searchTags"]) &&
			(count($searchParams["searchTags"]) > 1 || count($searchParams["searchTags"]) == 1 && $searchParams["searchTags"][0] != "")
		) {
			$operator = (!empty($options) && isset($options["tags"]) && isset($options["tags"]["verb"])) ? $options["tags"]["verb"] : '$in';
			if (!empty($searchParams["tagsPath"])) {
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query, $searchParams["tagsPath"]);
				unset($searchParams["tagsPath"]);
			} else
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query, "tags");
			unset($searchParams["searchTags"]);
		}
		if (!empty($searchParams['sortBy'])) {
			if (Api::isAssociativeArray($searchParams['sortBy'])) {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$key] = (int)$value;
				}
				$searchParams["sortBy"] = $sortBy;
			} else {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$value] = 1;
				}
				$searchParams["sortBy"] = $sortBy;
			}
		} else {
			$searchParams['sortBy'] = array("updated" => -1);
		}
		if (empty($searchParams["fields"])) {
			$searchParams["fields"] = [];
		}
		if (!empty($searchParams['filters'])) {
			$query = SearchNew::searchFilters($searchParams['filters'], $query, $moreParams);
		}
		return array("query" => $query, "searchParams" => $searchParams);
	}

	public static function myOthersCommunsQuery($searchParams, $moreParams = []) {
		$searchParams["indexMin"] = (isset($searchParams["indexMin"])) ? $searchParams["indexMin"] : 0;
		$searchParams["indexStep"] = (isset($searchParams["indexStep"])) ? $searchParams["indexStep"] : 100;
		$parentForms = PHDB::find(Form::COLLECTION, ["type" => "aap", "aapType" => "aac"], ["name", "parent", "type", "config", "aapType"]);
		$currentFormContext = [];
		$currentFormId = "null";
		if (!empty($searchParams["currentForm"])) {
			if (isset($parentForms[$searchParams["currentForm"]])) {
				$thisContextId = array_keys($parentForms[$searchParams["currentForm"]]["parent"])[0];
				$currentFormContext = [
					"id" => $thisContextId,
					"type" => $parentForms[$searchParams["currentForm"]]["parent"][$thisContextId]["type"]
				];
				unset($parentForms[$searchParams["currentForm"]]);
			}
			$currentFormId = $searchParams["currentForm"];
			unset($searchParams["currentForm"]);
		}
		$query = [];
		$query = SearchNew::addQuery(
			$query, 
			[
				"form" => ['$in' => array_keys($parentForms)],
				"answers" => ['$exists' => true],
				"answers.aapStep1.titre" => ['$exists' => true],
				"user" => Yii::app()->session['userId'],
				"links.aacForm.$currentFormId" => ['$exists' => false]
			]
		);
		if (!empty($currentFormContext)) {
			$query = SearchNew::addQuery(
				$query,
				[
					'$or' => [
						["answers.aapStep2.choose.".$currentFormContext["id"] => ['$exists' => false]],
						["answers.aapStep2.choose.".$currentFormContext["id"].".value" => "notselected"]
					]
				]
			);
		}
		$aapFieldMapping = [
			"title" => "answers.aapStep1.titre",
			"image" => "answers.aapStep1.image",
			"choose" => "answers.aapStep2.choose",
		];
		$searchParams['sortBy'] = array("updated" => -1);
		if (empty($searchParams["fields"])) {
			$searchParams["fields"] = ["user", "links", "form", "source", "created", $aapFieldMapping["title"], $aapFieldMapping["image"], $aapFieldMapping["choose"]];
		}
		return array("query" => $query, "searchParams" => $searchParams);
	}

	public static function aacQuery($searchParams, $moreParams = []) {
		$searchParams["indexMin"] = (isset($searchParams["indexMin"])) ? $searchParams["indexMin"] : 0;
		$searchParams["indexStep"] = (isset($searchParams["indexStep"])) ? $searchParams["indexStep"] : 100;
		$query = [];
		if (!empty($searchParams["name"])) {
			if (!empty($searchParams["textPath"]))
				$query = SearchNew::searchText($searchParams["name"], $query, array("textPath" => $searchParams["textPath"]));
			else
				$query = SearchNew::searchText($searchParams["name"], $query);
		}
		if (
			!empty($searchParams["searchTags"]) &&
			(count($searchParams["searchTags"]) > 1 || count($searchParams["searchTags"]) == 1 && $searchParams["searchTags"][0] != "")
		) {
			$operator = (!empty($options) && isset($options["tags"]) && isset($options["tags"]["verb"])) ? $options["tags"]["verb"] : '$in';
			if (!empty($searchParams["tagsPath"])) {
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query, $searchParams["tagsPath"]);
				unset($searchParams["tagsPath"]);
			} else
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query, "tags");
			unset($searchParams["searchTags"]);
		}
		if (!empty($searchParams['sortBy'])) {
			if (Api::isAssociativeArray($searchParams['sortBy'])) {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$key] = (int)$value;
				}
				$searchParams["sortBy"] = $sortBy;
			} else {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$value] = 1;
				}
				$searchParams["sortBy"] = $sortBy;
			}
		} else {
			$searchParams['sortBy'] = array("updated" => -1);
		}
		if (empty($searchParams["fields"])) {
			$searchParams["fields"] = ["name", "description", "parent", "type", "aapType", "subType", "created", "updated"];
		}
		if (!empty($searchParams['filters'])) {
			$query = SearchNew::searchFilters($searchParams['filters'], $query, $moreParams);
		}
		return array("query" => $query, "searchParams" => $searchParams);
	}

	public static function parseAacData($elements) {
		$allAnswers = [];
		$allFormsKeys = [];
		$parentsImages = [
			"aacsId" => [],
			"ids" => [],
			"data"  => []
		];
		$aacData = [];
		if (!empty($elements["results"])) {
			$allFormsKeys = array_keys($elements["results"]);
			foreach ($elements["results"] as $k => $elemValue) {
				if (!empty($elemValue["parent"])) {
					$ctx = array_keys($elemValue["parent"])[0];
					$parentsImages["aacsId"][] = new MongoId($ctx);
					$parentsImages["ids"][] = $ctx;
					$elements["results"][$k]["parentId"] = $ctx;
					isset($elemValue["parent"][$ctx]["type"]) ? $parentsImages["elType"] = $elemValue["parent"][$ctx]["type"] : "";
				}
			}
		}
		$pattern = implode('|', ["jpeg", "jpg", "gif", "png"]);
		$imageRegex = new MongoRegex("/($pattern)$/i");
		$whereDocument = [
			'id'		 => ['$in' => $parentsImages["ids"]],
			'type'		 => ['$in' => [Organization::COLLECTION, Project::COLLECTION]],
			'contentKey' => 'profil',
			'name' 		 => $imageRegex,
			'current' 	 => true,
			'folder'	 => ['$exists' => 1]
		];
		$parentsImages["data"] = PHDB::find(Document::COLLECTION, $whereDocument);
		$allAnswers = PHDB::find(
			Answer::COLLECTION,
			[
				"answers" => ['$exists' => true],
				"answers.aapStep2.choose" => ['$exists' => true],
				'$expr' => [
					'$gt' => [
						[
							'$size' => [
								'$filter' => [
									'input' => [
										'$objectToArray' => '$answers.aapStep2.choose'
									],
									'as' => 'item',
									'cond' => [
										'$eq' => ['$$item.v.value', 'selected']
									]
								]
							]
						],
						0
					]
				]
			],
			["form", "answers.aapStep1", "answers.aapStep2.choose", "links", "user"]
		);
		if (!empty($allAnswers)) {
			foreach ($allAnswers as $ansKey => $ansVal) {
				foreach ($ansVal["answers"]["aapStep2"]["choose"] as $aacParentKey => $aacParent) {
					if (!empty($aacParent["value"]) && $aacParent["value"] == "selected") {
						if (!isset($aacData[$aacParentKey])) {
							$aacData[$aacParentKey] = [
                                "communs" => 0,
                                "participants" => 0,
                                "collectedFond" => 0,
                                "solicitedFund" => 0,
                                "previousData" => []
                            ];
						}
						$thisAnswerData = self::getAnswerData($ansVal, $aacData[$aacParentKey]["previousData"] ?? []);
						$aacData[$aacParentKey]["communs"]++;
						$aacData[$aacParentKey]["participants"] += $thisAnswerData["financerNb"] + $thisAnswerData["contributorNb"];
						$aacData[$aacParentKey]["collectedFond"] += $thisAnswerData["financementAmount"];
						$aacData[$aacParentKey]["solicitedFund"] += $thisAnswerData["allFinAmount"];
						$aacData[$aacParentKey]["previousData"] = $thisAnswerData;
					}
				}
			}
		}
		$contents = [
			'parentsImages'
		];
		foreach ($elements["results"] as $k => $elemValue) {
			if (!empty($elemValue["parent"])) {
				$ctx = array_keys($elemValue["parent"])[0];
				if (!empty($parentsImages["data"])) {
					$currentParentImage = array_filter($parentsImages["data"], function($item) use ($ctx) {
						return $item["id"] == $ctx;
					});
					$currentParentImage = reset($currentParentImage);
					if (!empty($currentParentImage)){
						$imgPath = "/upload/". @$currentParentImage["moduleId"] ."/". @$currentParentImage["folder"] . "/medium/". @$currentParentImage["name"];
						$elements["results"][$k]["profilMediumImageUrl"] = $imgPath;
						$elements["results"][$k]["imageSameAsContext"] = true;
					}
				}
				if (isset($aacData[$ctx])) {
					// unset($aacData[$ctx]["previousData"]);
					$elements["results"][$k]["stat"] = $aacData[$ctx];
				}
			}
		}
		return $elements;
	}

	public static function getAnswerData($answer, $previousData = []) {
		$answerData = [
			"financerNb" => 0,
			"financementAmount" => 0,
			"allFinAmount" => 0,
			"financers" => $previousData["financers"] ?? [],
			"contributorNb" => 0,
			"contributors" => $previousData["contributors"] ?? []
		];
		if (isset($answer["answers"]["aapStep1"]["depense"])) {
			foreach ($answer["answers"]["aapStep1"]["depense"] as $depKey => $depValue) {
				if ((isset($depValue["include"]) && filter_var($depValue["include"], FILTER_VALIDATE_BOOLEAN)) || !isset($depValue["include"])) {
					if (isset($depValue["price"])) {
						$answerData["allFinAmount"] += (float)$depValue["price"];
					}
					if (isset($depValue["financer"])) {
						foreach ($depValue["financer"] as $finKey => $finValue) {
							if (isset($finValue["id"]) && !isset($answerData["financers"][$finValue["id"]])) {
								$answerData["financerNb"]++;
								$answerData["financers"][$finValue["id"]] = $finValue;
							}
							$answerData["financementAmount"] += (float)$finValue["amount"];
						}
					}
				}
			}
		}
		if (isset($answer["links"]["contributors"])) {
			foreach ($answer["links"]["contributors"] as $contKey => $contValue) {
                if (!isset($answerData["contributors"][$contKey])) {
                    $answerData["contributorNb"]++;
                    $answerData["contributors"][$contKey] = $contValue;
                }
            }
		}
		return $answerData;
	}

	public static function parseMyAacData($elements) {
		$parentsImages = [
			"aacsId" => [],
			"ids" => [],
			"data"  => []
		];
		if (!empty($elements["results"])) {
			foreach ($elements["results"] as $k => $elemValue) {
				if (!empty($elemValue["parent"])) {
					$ctx = array_keys($elemValue["parent"])[0];
					$parentsImages["aacsId"][] = new MongoId($ctx);
					$parentsImages["ids"][] = $ctx;
					isset($elemValue["parent"][$ctx]["type"]) ? $parentsImages["elType"] = $elemValue["parent"][$ctx]["type"] : "";
					if (!Authorisation::isInterfaceAdmin($ctx, $parentsImages["elType"])) {
						unset($elements["results"][$k]);
					}
				}
			}
		}
		$pattern = implode('|', ["jpeg", "jpg", "gif", "png"]);
		$imageRegex = new MongoRegex("/($pattern)$/i");
		$whereDocument = [
			'id'		 => ['$in' => $parentsImages["ids"]],
			'type'		 => ['$in' => [Organization::COLLECTION, Project::COLLECTION]],
			'contentKey' => 'profil',
			'name' 		 => $imageRegex,
			'current' 	 => true,
			'folder'	 => ['$exists' => 1]
		];
		$parentsImages["data"] = PHDB::find(Document::COLLECTION, $whereDocument);
		$contents = [
			'parentsImages'
		];
		foreach ($elements["results"] as $k => $elemValue) {
			if (!empty($elemValue["parent"])) {
				$ctx = array_keys($elemValue["parent"])[0];
				$elements["results"][$k]["type"] = $elements["results"][$k]["parent"][$ctx]["type"];
				$elements["results"][$k]["id"] = $ctx;
				$elements["results"][$k]["contextName"] = $elements["results"][$k]["parent"][$ctx]["name"];
				if (!empty($parentsImages["data"])) {
					$currentParentImage = array_filter($parentsImages["data"], function($item) use ($ctx) {
						return $item["id"] == $ctx;
					});
					$currentParentImage = reset($currentParentImage);
					if (!empty($currentParentImage)){
						$imgPath = "/upload/". @$currentParentImage["moduleId"] ."/". @$currentParentImage["folder"] . "/medium/". @$currentParentImage["name"];
						preg_match('/\.[^.]+$/', @$currentParentImage["name"], $imageExt);
						$thumbPath = "/upload/". @$currentParentImage["moduleId"] ."/". @$currentParentImage["folder"] . "/thumb/profil-resized". $imageExt[0] ."?t=".@$currentParentImage["created"];
						$elements["results"][$k]["profilMediumImageUrl"] = $imgPath;
						$elements["results"][$k]["profilThumbImageUrl"] = $thumbPath;
						$elements["results"][$k]["imageSameAsContext"] = true;
					}
				}
			}
		}
		return $elements;
	}

    private static function isCommunSelected($selectData) {
		$res = false;
		foreach ($selectData as $k => $v) {
			if (isset($v["value"]) && $v["value"] == "selected") {
				$res = true;
				break;
			}
		}
		return $res;
	}

	public static function parseCommunData($allAnswers) {
		$users = [
			"ids"  => [],
			"data" => [],
		];
		$projects = [
			"ids"  => [],
			"data" => [],
		];
		$actionsMetrics = [];
		$allImages = [
			"ansId" => [],
			"data"  => []
		];
		if (!empty($allAnswers["results"])) {
			$db_images = self::getPropositionThumbnail(array_keys($allAnswers["results"]));
			$aapFieldMapping = [
				"title" => "answers.aapStep1.titre",
				"step1" => "aapStep1",
				"step2" => "answers.aapStep2",
			];
			foreach ($allAnswers["results"] as $answerKey => $answerVal) {
				$as1 = $answerVal["answers"][$aapFieldMapping["step1"]];
				$contributors = !empty($answerVal['links']['contributors']) && is_array($answerVal['links']['contributors']) ? $answerVal['links']['contributors'] : [];
				$description = !empty($as1['description']) ? $as1['description'] : '';
				$depensesnf = !empty($as1['depense']) && is_array($as1['depense']) ? $as1['depense'] : [];
				$depenses = [];
                foreach ($depensesnf as $depense){
                    if(!isset($depense["include"]) || $depense["include"] != false) {
                        array_push($depenses, $depense);
                    }
                }
                $depenses = array_map(function ($depense) {
                    $ret = [
                        'price' => isset($depense['price']) ? intval($depense['price']) : 0,
                        'financer' => !empty($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : []
                    ];
                    $ret['financer'] = array_map(fn($financer) => (isset($financer['amount']) ? intval($financer['amount']) : 0), $ret['financer']);
                    return ($ret);
				}, $depenses);
				$interrested = array_filter($answerVal['vote'] ?? [], fn($item) => $item['status'] === 'love');

				$allAnswers['results'][$answerKey]['name'] = $as1['titre'] ?? "(No title)";
				$allAnswers['results'][$answerKey]['image'] = $db_images[$answerKey];
				$allAnswers['results'][$answerKey]['descriptionStr'] = $description;
				$allAnswers['results'][$answerKey]['tags'] = !empty($as1['tags']) && is_array($as1['tags']) ? array_values($as1['tags']) : [];
				$allAnswers['results'][$answerKey]['funds'] = $depenses;
				$allAnswers['results'][$answerKey]['interrest_count'] = count($interrested);
				if (isset($answerVal["project"]["id"])) {
					UtilsHelper::push_array_if_not_exists($answerVal["project"]["id"], $projects['ids']);
					$allAnswers["results"][$answerKey]["projectId"] = $answerVal["project"]["id"];
				}

				if (!empty($answerVal["user"])) {
					UtilsHelper::push_array_if_not_exists($answerVal["user"], $users['ids']);
				}
				if (!empty($answerVal["links"]["contributors"])) {
					foreach ($answerVal["links"]["contributors"] as $kc => $vc)
						$users["ids"][] = new MongoId($kc);
				}
			}
			$answersActions = PHDB::find(
                Action::COLLECTION,
                [
                    '$and' => [
                        ["parentId" => ['$in' => $projects['ids']]],
                        ["status" => ['$ne' => 'disabled']],
						["created" => ['$exists' => true]]
					]
				],
				["parentId", "status", "tracking"]
            );
			$actionsMetrics = self::formatCommunActionData($answersActions);
		}
		$users["data"] = PHDB::findByIds(Person::COLLECTION, $users["ids"], ['name', 'slug', 'profilImageUrl', 'collection']);
		$users['data'] = array_map(function ($mapUser) {
			if (empty($mapUser['profilImageUrl']))
				$mapUser['profilImageUrl'] = Yii::app()->getModule('co2')->assetsUrl . '/images/thumb/default_citoyens.png';
			return $mapUser;
		}, $users['data']);
		if (!empty($actionsMetrics)) {
			foreach ($allAnswers["results"] as $ansId => $ansVal) {
				if (!empty($ansVal["projectId"]) && !empty($actionsMetrics[$ansVal["projectId"]])) {
					$allAnswers["results"][$ansId]["actionsData"] = $actionsMetrics[$ansVal["projectId"]];	
				}
			}
		}
		$contents = [
			'users'
		];
		foreach ($contents as $content) $allAnswers[$content] = $$content['data'];
		return $allAnswers;
	}

	/**
	 * This function is used to categorize actions by parentid into 'completed', 'in progress', 'to do', 'All'.
	 */
	public static function formatCommunActionData($actionData) {
		$res = [];
		foreach ($actionData as $actionId => $actionVal) {
			if (!isset($res[$actionVal["parentId"]])) {
				$res[$actionVal["parentId"]] = [
					"completed" => 0,
					"inprogress" => 0,
					"todo" => 0,
					"all" => 0
				];
			}
			$res[$actionVal["parentId"]]["all"]++;
			if ($actionVal["status"] == "todo" && ((isset($actionVal["tracking"]) && !filter_var($actionVal["tracking"], FILTER_VALIDATE_BOOL)) || !isset($actionVal["tracking"]))) {
				$res[$actionVal["parentId"]]["todo"]++;
			}
			if ($actionVal["status"] == "todo" && !empty($actionVal["tracking"])) {
				$res[$actionVal["parentId"]]["inprogress"]++;
			}
			if ($actionVal["status"] == "done") {
                $res[$actionVal["parentId"]]["completed"]++;
            }
		}
		return $res;
	}

	public static function getAacElements () {
		$res = [];
		$res["data"] = PHDB::find(Form::COLLECTION, ["type" => "aap", "aapType" => "aac"], ["name", "parent", "type", "config", "aapType"]);
		return $res;
	}

	/**
	 * You must pass the path of the input parameters to retrieve.
	 */
	public static function getAacDefi ($inputDefiKey = "checkboxNewaapStep1m03ot9qymmfgashp7l.list") {
		$res = [];
		$res["aac"] = PHDB::find(Form::COLLECTION, ["type" => "aap", "aapType" => "aac"], ["name", "parent", "type", "params.$inputDefiKey", "config", "aapType"]);
		$res["data"] = [];
		if (!empty($res["aac"])) {
			foreach ($res["aac"] as $aacKey => $aacValue) {
				$paramsData = UtilsHelper::array_val_at_path($aacValue, "params.$inputDefiKey");
				foreach ($paramsData as $key => $value) {
					UtilsHelper::push_array_if_not_exists($value, $res["data"]);
				}
			}
		}
		return $res;
	}

	public static function globaleAutocompleteMyAac($searchParams) {
		$queryParams = self::aacQuery($searchParams);
		$query = $queryParams["query"];
		$searchParams = $queryParams["searchParams"];
		$res = [];
		$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex(
			Form::COLLECTION,
			$query,
			$searchParams["fields"],
			$searchParams['sortBy'],
			$searchParams["indexStep"],
			$searchParams["indexMin"]
		);
		if (isset($searchParams["count"]))
			$res["count"]["answers"] = PHDB::count("answers", $query);
		$res = self::parseMyAacData($res);
		return $res;
	}

	public static function globalAutocompleteElement($form, $searchParams, $newcounter, $eltType = "contributors") {
		$queryParams = self::globalAutocompleteProposalQuery($form, $searchParams, $newcounter);
		$query = $queryParams["query"];
		$searchParams = $queryParams["searchParams"];

		$res = array();
		if ($newcounter) {
			if (!empty(Yii::app()->session['userId'])) {
				$query = SearchNew::addQuery($query, array("views." . Yii::app()->session['userId'] => ['$exists' => false]));
				$res["newCounter"] = PHDB::count($searchParams["searchType"][0], $query);
			} else $res["newCounter"] = 0;
		} else {
			if ($searchParams["indexStep"] == "all") {
				$res["results"] = PHDB::findAndSort(
					$searchParams["searchType"][0],
					$query,
					$searchParams['sortBy'],
					0,
					$searchParams["fields"],
				);
			} else {
				$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex(
					$searchParams["searchType"][0],
					$query,
					$searchParams["fields"],
					$searchParams['sortBy'],
					$searchParams["indexStep"],
					$searchParams["indexMin"]
				);
			}
			if (isset($searchParams["count"]))
				$res["count"][$searchParams["searchType"][0]] = PHDB::count($searchParams["searchType"][0], $query);

			if (isset($res["results"]) && !empty($eltType)) {
				$res["elements"][$eltType] = [];
				if ($eltType == "answerfinancers") {
					$allTls = Organization::get_tls();
				}
				foreach ($res["results"] as $ansKey => $ansValue) {
					if ($eltType == "answerfinancers") {
						if (isset($ansValue["answers"]["aapStep1"]["depense"]) && is_array($ansValue["answers"]["aapStep1"]["depense"])) {
							foreach ($ansValue["answers"]["aapStep1"]["depense"] as $depKey => $depValue) {
								if (isset($depValue["financer"]) && is_array($depValue["financer"])) {
									foreach ($depValue["financer"] as $finKey => $finValue) {
										if (isset($finValue["id"]) && isset($allTls[$finValue["id"]])) {
											UtilsHelper::push_array_if_not_exists($finValue["id"], $res["elements"][$eltType]);
										}
										if (isset($finValue["tlid"]) && isset($allTls[$finValue["tlid"]])) {
											UtilsHelper::push_array_if_not_exists($finValue["tlid"], $res["elements"][$eltType]);
										}
									}
								}
							}
						}
					} else {
						if (isset($ansValue["links"][$eltType])) {
							foreach ($ansValue['links'][$eltType] as $contributorsId => $contributorsVal) UtilsHelper::push_array_if_not_exists($contributorsId, $res["elements"][$eltType]);
						}
					}
				}
			}
		}

		return $res;
	}

    public static function changefintype($answer , $financeurid ) {
        $answers = PHDB::findOneById(Form::ANSWER_COLLECTION , $answer );

        $depenses = [];

        if(!empty($answers) && !empty($answers['answers']['aapStep1']['depense'])){
            $depenses = $answers['answers']['aapStep1']['depense'];
            foreach ($answers['answers']['aapStep1']['depense'] as $depkey => $depense){
                if(isset($depense["financer"])){
                    foreach ($depense["financer"] as $finkey => $finance){
                        if($finance["id"] == $financeurid ) {
                            $depenses[$depkey]["financer"][$finkey]["type"] = "tl";
                        }
                    }
                }
            }

            return PHDB::update(
                Form::ANSWER_COLLECTION,
                array("_id" => new MongoId($answer)),
                array('$set' => array("answers.aapStep1.depense" => $depenses))
            );
        }else{
            return [
                "result" => false
            ];
        }
    }

    public static function doublonnement($answer , $financeurid , $action ) {
        $answers = PHDB::findOneById(Form::ANSWER_COLLECTION , $answer );

        $depenses = [];

        if(!empty($answers) && !empty($answers['answers']['aapStep1']['depense'])){
            $depenses = $answers['answers']['aapStep1']['depense'];
            foreach ($answers['answers']['aapStep1']['depense'] as $depkey => $depense){
                if(isset($depense["financer"])){
                    foreach ($depense["financer"] as $finkey => $finance){
                        if($finance["id"] == $financeurid ) {
                            if (filter_var($action, FILTER_VALIDATE_BOOLEAN)) {
                                $depenses[$depkey]["financer"][$finkey]["iscofinancementftl"] = true;

                                $depenses[$depkey]["financer"][] = [
                                    "line" => "",
                                    "amount" => $finance["amount"],
                                    "user" => Yii::app()->session['userId'],
                                    "date" => new DateTimeZone("UTC"),
                                    "iscofinancementftl" => true,
                                    "name" => $answers["context"][array_key_first($answers["context"])]["name"],
                                    "id" => array_key_first($answers["context"]),
                                    "tlname" => $finance["name"],
                                    "tlid" => $finance["id"],
                                    "status" => "pending",
                                    "finkey" => $finance["finkey"]
                                ];

                            } else {
                                $depenses[$depkey]["financer"][$finkey]["iscofinancementftl"] = false;

                                foreach ($depenses[$depkey]["financer"] as $key => $value) {
                                    if (isset($value['finkey']) && $value['finkey'] === $finance['finkey'] && isset($value['tlid']) && $value['tlid'] === $finance['id'] ) {
                                        unset($depenses[$depkey]["financer"][$key]);
                                        $depenses[$depkey]["financer"] = array_values($depenses[$depkey]["financer"]);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return PHDB::update(
                Form::ANSWER_COLLECTION,
                array("_id" => new MongoId($answer)),
                array('$set' => array("answers.aapStep1.depense" => $depenses))
            );
        }else{
            return [
                "result" => false
            ];
        }
    }

	public static function financerLogs($form , $answer = null) {
		$tableAnswers = array();
        $isAdmin = Form::isFormAdmin($form);
        $canEdit = false;
        $stepFinancer = PHDB::findOne(Form::INPUTS_COLLECTION , ["formParent" => $form , "step" => "aapStep3"] );
        $tiersLieux = Organization::get_tls(["email",'links']);

        //vue detaillé
        $aggrOpt = [
            "answers.aapStep1.depense.financer.name" => ['$exists' => true],
            "answers.aapStep1.depense.financer.amount" => ['$exists' => true],
            "answers.aapStep1.titre" => ['$exists' => true]
        ];
        if(!empty($answer)){
            $aggrOpt["_id"] = new MongoId($answer);
            $prOpt = [
                _id => 0,
                'poste de depense' => '$answers.aapStep1.depense.poste',
                'depense' => '$answers.aapStep1.depense.price',
                'financeur' => '$answers.aapStep1.depense.financer.name',
                'date du financement' => '$answers.aapStep1.depense.financer.date',
                'financeurId' => '$answers.aapStep1.depense.financer.id',
                'financement TL' => '$answers.aapStep1.depense.financer.amount',
                'financement FTL' => '$answers.aapStep1.depense.financer.amount',
                'statut' => '$answers.aapStep1.depense.financer.status',
                'tlid' => '$answers.aapStep1.depense.financer.tlid',
                'data-answerid' => '$_id',
                'data-depenseid' => '$depenseId',
                'data-financeid' => '$financeId',
                'data-user' => '$answers.user',
                'data-include' => '$answers.depense.include',
                'data-form' => '$answers.form',
            ];
        }else{
            $aggrOpt["form"] = $form;
            $prOpt = [
                _id => 0,
                'commun' => '$answers.aapStep1.titre',
                'poste de depense' => '$answers.aapStep1.depense.poste',
                'depense' => '$answers.aapStep1.depense.price',
                'financeur' => '$answers.aapStep1.depense.financer.name',
                'date du financement' => '$answers.aapStep1.depense.financer.date',
                'financeurId' => '$answers.aapStep1.depense.financer.id',
                'financement TL' => '$answers.aapStep1.depense.financer.amount',
                'financement FTL' => '$answers.aapStep1.depense.financer.amount',
                'statut' => '$answers.aapStep1.depense.financer.status',
                'tlid' => '$answers.aapStep1.depense.financer.tlid',
                'data-answerid' => '$_id',
                'data-depenseid' => '$depenseId',
                'data-financeid' => '$financeId',
                'data-user' => '$answers.user',
                'data-archived' => '$answers.aapStep1.depense.include',
                'data-form' => '$answers.form',
            ];
        }
        $tableAnswers = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => $aggrOpt
            ],
            [
                '$unwind' => [
                    'path' => '$answers.aapStep1.depense',
                    'includeArrayIndex' => "depenseId"
                ]
            ],
            [
                '$unwind' => '$answers.aapStep1.depense.poste'
            ],
            [
                '$unwind' => [
                    'path' => '$answers.aapStep1.depense.financer',
                    'includeArrayIndex' => "financeId"
                ]
            ],
            [
                '$unwind' => '$answers.aapStep1.titre'
            ],
            [
                '$project' => $prOpt
            ]
        ]);
        for ($i = count($tableAnswers["result"]) - 1; $i >= 0; $i--) {
			// Si l'objet contient "financeurId", on cherche une correspondance avec "tlid"
			if (isset($tableAnswers["result"][$i]['financeurId'])) {
				$financeurId = $tableAnswers["result"][$i]['financeurId'];
				$found = false; // Flag pour vérifier la correspondance

				// Recherche dans le $tableAnswers["result"] un objet ayant "tlid" égal à "financeurId"
				foreach ($tableAnswers["result"] as $key => $obj) {
					if (isset($obj['tlid']) && $obj['tlid'] == $financeurId) {
						// Remplissage de "financement FTL" avec la "financement TL" correspondante
						$tableAnswers["result"][$i]['financement FTL'] = $obj['financement TL'];

                        $tableAnswers["result"][$i]['data-financeftlid'] = $obj['data-financeid'];

						// Suppression de la ligne qui contient le "tlid"
						unset($tableAnswers["result"][$key]);
						$found = true;
						break; // On arrête la recherche après avoir trouvé la correspondance
					}
				}

				// Si aucune correspondance n'est trouvée, mettre "financement FTL" à 0
				if (!$found) {
					$tableAnswers["result"][$i]['financement FTL'] = 0;
				}

				// Suppression de l'attribut "financeurId"
				unset($tableAnswers["result"][$i]['financeurId']);
			}
		}
        foreach ($tableAnswers["result"] as $key => $obj) {
            // Verifier canEdit
            if (isset($obj['data-user']) && $obj['data-user'] == Yii::app()->session['userId']) {
                $canEdit = true;
            }
            // Supprimer la ligne contenant "tlid" qui n'a pas de correspondance
            if (isset($obj['tlid'])) {
                unset($tableAnswers["result"][$key]);
            }
        }

        //vue groupé
        $tableFinancersFiltered = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => $aggrOpt
            ],
            [
                '$unwind' => [
                    'path' => '$answers.aapStep1.depense',
                    'includeArrayIndex' => 'depenseIndex'
                ]
            ],
            [
                '$unwind' => '$answers.aapStep1.depense.poste'
            ],
            [
                '$unwind' => [
                    'path' => '$answers.aapStep1.depense.financer',
                    'includeArrayIndex' => 'financeIndex'
                ]
            ],
            [
                '$unwind' => '$answers.aapStep1.titre'
            ],
            [
                '$group' => [
                    '_id' => [
                        'commun' => '$answers.aapStep1.titre',
                        'financeur' => '$answers.aapStep1.depense.financer.name',
                        'financeurId' => '$answers.aapStep1.depense.financer.id',
                        'financeurTlid' => '$answers.aapStep1.depense.financer.tlid',
                        'financeurtype' => '$answers.aapStep1.depense.financer.type',
                    ],
                    'totalFinancement' => [
                        '$sum' => [ '$toDouble' => '$answers.aapStep1.depense.financer.amount' ]
                    ],
                    'details' => [
                        '$push' => [
                            'path' => [
                                '$concat' => [
                                    'answers.aapStep1.depense.',
                                    [ '$toString' => '$depenseIndex' ],
                                    '.financer.',
                                    [ '$toString' => '$financeIndex' ],
                                    ''
                                ]
                            ],
                            'id' => '$_id',
                            'originalAmount' => [ '$toDouble' => '$answers.aapStep1.depense.financer.amount' ],
                            'type' => 'financement',
                            'data' => '$answers.aapStep1.depense.financer',
                            'typeorga' => '$answers.aapStep1.depense.financer.type',
                            'include' => '$answers.aapStep1.depense.include'
                        ]
                    ]
                ]
            ],
            [
                '$project' => [
                    'commun' => '$_id.commun',
                    'financeur' => '$_id.financeur',
                    'financeurId' => '$_id.financeurId',
                    'financeurTlid' => '$_id.financeurTlid',
                    'financertype' => '$_id.financeurtype',
                    'totalFinancement' => 1,
                    'details' => 1
                ]
            ]
        ]);
        $orgaid = array_reduce($tableFinancersFiltered["result"] , function ($carry , $item) {
           if(isset($item["financertype"]) && $item["financertype"] == "organization"){
               $carry[] = $item["financeurId"];
           }
           return $carry;
        });
        $citid = array_reduce($tableFinancersFiltered["result"] , function ($carry , $item) {
            if(isset($item["financertype"]) && $item["financertype"] == "person"){
                $carry[] = $item["financeurId"];
            }
            return $carry;
        });
        $orgaemail = PHDB::findByIds(Organization::COLLECTION, $orgaid , ['email','links']);
        $citemail = !empty($citid) ? PHDB::findByIds(Person::COLLECTION, $citid , ['email']) : [];
        $emailarray = array_merge($tiersLieux,$orgaemail,$citemail);

        $noemail = array_reduce($emailarray, function ($carry, $item){
            if(!empty($item["links"]["members"]) && empty($item["email"])){
                $haveAdmin = false;
                $list = array_values($item["links"]["members"]);
                $listid = array_keys($item["links"]["members"]);
                for ($i = 0; $i < sizeof($list); $i++){
                    if(!$haveAdmin && isset($list[$i]["isAdmin"])){
                        $haveAdmin = true;
                        $carry[(string)$item["_id"]] =  $listid[$i];
                    }
                    if(!$haveAdmin && !isset($list[$i]["isAdmin"]) && $i == (sizeof($list) - 1)){
                        $carry[(string)$item["_id"]] =  $listid[$i];
                    }
                }
            }
            return $carry;
        });

        $memberemail = PHDB::findByIds(Person::COLLECTION, array_values($noemail) , ['email']);

        $tableFinancersFiltered = $tableFinancersFiltered['result'];
        foreach ($tableFinancersFiltered as $key => &$financeurData) {
            if (is_array($financeurData)) {
                $financeurId = $financeurData['financeurId'];
                $commun = $financeurData['commun']; // Identifie le commun actuel
                $totalBonus = 0;

                // Recherche des bonus parmi les autres financeurs dans le même commun
                foreach ($tableFinancersFiltered as $bonusFinanceur) {
                    // Vérifie si le bonus appartient au même commun et si le financeur a un `tlid` correspondant
                    if (
                        $bonusFinanceur['commun'] === $commun &&
                        ($bonusFinanceur['financeurTlid'] ?? null) === $financeurId
                    ) {
                        foreach ($bonusFinanceur['details'] as $bonusDetail) {
                            if ($bonusDetail['type'] === 'financement') {
                                $bonusAmount = $bonusDetail['originalAmount'] ?? 0;

                                // Ajouter le bonus aux détails du financeur actuel
                                $financeurData['details'][] = [
                                    'path' => $bonusDetail['path'],
                                    'originalAmount' => $bonusAmount,
                                    'type' => 'bonus'
                                ];

                                // Ajouter au total des bonus
                                $totalBonus += $bonusAmount;
                            }
                        }
                    }
                }

                // Ajouter le total des bonus pour ce financeur dans le même commun
                $financeurData['totalBonus'] = round($totalBonus);
            }
        }
        $tableFinancersFiltered = array_values(array_filter($tableFinancersFiltered, function ($financeur) {
            return !isset($financeur['financeurTlid']); // Exclure ceux qui contiennent `tlid`
        }));
        $tlid = array_map(function ($tl){
            return (string)$tl["_id"];
        }, $tiersLieux);
        $tlid = array_keys($tlid);
        $tableFinancersFiltered = array_map(function ($data) use ($tlid,$emailarray,$memberemail,$noemail) {
            $financertype = "organization";
            if(!empty($data["financertype"])){
                $financertype = $data["financertype"] ;
            }

            $needbtn = false;

            if(!empty($data["financeurId"]) && in_array($data["financeurId"] , $tlid) && $financertype != "tl" ){
                $needbtn = true;
            }

            $stat = false;
            //var_dump($data["details"][0]);

            if(!empty($data["details"][0]["data"]["status"]) ){
                if( $data["details"][0]["data"]["status"] == "pending"){
                    $stat = "en attente";
                }else if($data["details"][0]["data"]["status"] == "paid"){
                    $stat = "payée";
                }else if($data["details"][0]["data"]["status"] == "distributed"){
                    $stat =  "rédistribué";
                }
            }

            $email = "";

            if(isset($emailarray[$data["financeurId"]])){
                if(isset($emailarray[$data["financeurId"]]["email"])){
                    $email = $emailarray[$data["financeurId"]]["email"];
                }
                if(isset($noemail[$data["financeurId"]]) && isset($memberemail[$noemail[$data["financeurId"]]])){
                    $email = $memberemail[$noemail[$data["financeurId"]]]["email"]." (membre)" ;
                }
            }else{
                $email = "";
            }

            return [
                "data-answerid" => $data["details"][0]['id'],
                "commun" => $data["commun"],
                "financeur" => $data["financeur"],
                "financement" => (int)$data["totalFinancement"],
                "financements FTL" => (int)$data["totalBonus"],
                "data-financeurid" => $data["financeurId"],
                "data-financertype" => $financertype,
                "data-path" => JSON::encode($data["details"][0]['path']),
                "data-needbtn" => $needbtn,
                "etat" => $stat,
                "email" => $email
                /*'contributeurs' => $data["totalContributeurs"],
                'tiers-lieux' => $data["totalTls"],
                'votes' => $data["totalVotes"],
                'vues' => $data["totalViews"]*/
            ];
        }, $tableFinancersFiltered);
        usort($tableFinancersFiltered , function($a, $b){
            return strcmp($a['commun'], $b['commun']);
        });

        //vue stat
        //$aggrOpt["form"] = $form;
        $tableFinancerStat = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => [
                    "form" => $form
                ]
            ],
            [
                '$addFields' => [
                    'answers.aapStep1.depense' => [
                        '$ifNull' => ['$answers.aapStep1.depense', []]
                    ]
                ]
            ],
            [
                '$unwind' => [
                    'path' => '$answers.aapStep1.depense',
                    'preserveNullAndEmptyArrays' => true
                ]
            ],
            [
                '$addFields' => [
                    'answers.aapStep1.depense.financer' => [
                        '$ifNull' => ['$answers.aapStep1.depense.financer', []]
                    ]
                ]
            ],
            [
                '$unwind' => [
                    'path' => '$answers.aapStep1.depense.financer',
                    'preserveNullAndEmptyArrays' => true
                ]
            ],
            [
                '$unwind' => [
                    'path' => '$answers.aapStep1.titre',
                    'preserveNullAndEmptyArrays' => true
                ]
            ],
            [
                '$addFields' => [
                    'answers.aapStep1.depense.financer.type' => [
                        '$ifNull' => ['$answers.aapStep1.depense.financer.type', 'Aucun']
                    ],
                    'answers.aapStep1.depense.financer.name' => [
                        '$ifNull' => ['$answers.aapStep1.depense.financer.name', 'Aucun']
                    ],
                    'answers.aapStep1.depense.financer.amount' => [
                        '$ifNull' => ['$answers.aapStep1.depense.financer.amount', 0]
                    ]
                ]
            ],
            [
                '$group' => [
                    '_id' => [
                        'commun' => '$answers.aapStep1.titre',
                        'financeurtype' => '$answers.aapStep1.depense.financer.type'
                    ],
                    'totalFinancement' => [
                        '$sum' => ['$toDouble' => '$answers.aapStep1.depense.financer.amount']
                    ],
                    'contributorsArray' => ['$addToSet' => '$links.contributors'],
                    'tlsArray' => ['$addToSet' => '$links.tls'],
                    'votesArray' => ['$addToSet' => '$vote'],
                    'viewsArray' => ['$addToSet' => '$views'],
                    'distinctFinanceurs' => ['$addToSet' => '$answers.aapStep1.depense.financer.name'],
                    'distinctDepense' => ['$addToSet' => '$answers.aapStep1.depense']
                ]
            ],
            [
                '$group' => [
                    '_id' => '$_id.commun',
                    'totalFinancementGlobal' => ['$sum' => '$totalFinancement'],
                    'financeurs' => [
                        '$push' => [
                            'k' => '$_id.financeurtype',
                            'v' => '$totalFinancement'
                        ]
                    ],
                    'contributorsArray' => ['$push' => '$contributorsArray'],
                    'tlsArray' => ['$push' => '$tlsArray'],
                    'votesArray' => ['$push' => '$votesArray'],
                    'viewsArray' => ['$push' => '$viewsArray'],
                    'distinctFinanceursCount' => ['$sum' => ['$size' => '$distinctFinanceurs']],
                    'distinctDepenseCount' => ['$sum' => ['$size' => '$distinctDepense']]
                ]
            ],
            [
                '$project' => [
                    '_id' => 0,
                    'commun' => '$_id',
                    'totalFinancementGlobal' => 1,
                    'financeurs' => ['$arrayToObject' => '$financeurs'],
                    'totalContributeurs' => '$contributorsArray',
                    'totalTls' => '$tlsArray',
                    'totalVotes' => '$votesArray',
                    'totalViews' => '$viewsArray',
                    'distinctFinanceursCount' => 1,
                    'distinctDepenseCount' => 1,
                ]
            ]
        ]);
        $tableFinancerStat = $tableFinancerStat['result'];
        $tableFinancerStat = array_map(function ($data) use ($tlid) {
            $financertype = "organization";
            if(!empty($data["financertype"])){
                $financertype = $data["financertype"] ;
            }

            return [
                //"data-answerid" => (string)$data["id"],
                "commun" => $data["commun"],
                "nb besoins financiers" => $data["distinctDepenseCount"],
                "nb financeur" => $data["distinctFinanceursCount"],
                'financement ftl' => $data['financeurs']["Aucun"] ?? 0,
                'financement tl' => $data['financeurs']["tl"] ?? 0,
                'financement organisation' => $data['financeurs']["organization"] ?? 0,
                'financement citoyen' => $data['financeurs']["person"] ?? 0,
                'financement total' => $data["totalFinancementGlobal"],
                'nb contributeurs' => isset($data["totalContributeurs"][0][0]) ? sizeof($data["totalContributeurs"][0][0]) : 0,
                'nb tiers-lieux' => isset($data["totalTls"][0][0]) ? sizeof($data["totalTls"][0][0]) : 0,
                'nb votes' => isset($data["totalVotes"][0][0]) ? sizeof($data["totalVotes"][0][0]) : 0,
                'nb compte qui ont visité' => isset($data["totalViews"][0][0]) ? sizeof($data["totalViews"][0][0]) : 0,
            ];
        }, $tableFinancerStat);

        $total = array();
        array_walk_recursive($tableFinancerStat, function($item,$key) use (&$total){
            //var_dump($key);
            if($key != "commun") {
                $total[$key] = isset($total[$key]) ? $item + $total[$key] : $item;
            }else{
                $total[$key] = 'total';
            }
        });

        $tableFinancerStat[] = $total;

        return array(
            "tableFinancersFiltered" => $tableFinancersFiltered,
            "tableAnswers" => $tableAnswers["result"],
            "tableStat" => $tableFinancerStat,
            "autorisation" => [
                "canEdit" => $canEdit || $isAdmin,
                "isAdmin" =>  $isAdmin
            ],
            "form" => $form,
            "stepFinancerId" => isset($stepFinancer["_id"]) ? (string)$stepFinancer["_id"] : "",
            "outputtest1" => $noemail,
            "outputtest2" => $memberemail
        );
    }

	private static function globalAutocompleteProjectIds($form, $context, $searchParams, $userId = null) {
		$answers = PHDB::find(Form::ANSWER_COLLECTION, array(
			"answers.aapStep1.titre" => ['$exists' => true],
			"project.id"             => ['$exists' => true],
			"form"                   => $form
		), array("_id" => 1, "project.id" => 1));

		$project_ids = array();
		foreach ($answers as $answer) {
			if (isset($answer["project"]["id"]))
				UtilsHelper::push_array_if_not_exists($answer["project"]["id"], $project_ids);
		}

		$project_ids = array_map(fn($id) => new MongoId($id), $project_ids);
		$query = [
			'$or' => [
				[
					'_id' => ['$in' => $project_ids],
					//'parent' => ['$exists' => true]
				],
			],
		];

		if (!empty($searchParams["name"])) {
			$query = SearchNew::searchText($searchParams["name"], $query);
		}

		if (!empty($searchParams["filters"]["properties.avancement"])) {
			$query = SearchNew::addQuery($query, array('properties.avancement' => ['$in' => $searchParams["filters"]["properties.avancement"]]));
		}

		if (!empty(Yii::app()->session['userId']) && !empty($searchParams["filters"]['$or']["links.contributors." . Yii::app()->session['userId']])) {
			$query = SearchNew::addQuery($query, array("links.contributors." . Yii::app()->session['userId'] => $searchParams["filters"]['$or']["links.contributors." . Yii::app()->session['userId']]));
		}

		$res = array();
		$res = PHDB::distinct(Project::COLLECTION, "_id", $query);
		return $res;
	}

	public static function globalAutocompleteProject($form, $context, $searchParams, $userId = null) {

		$result = [];
		// $formData = PHDB::findOneById(Form::COLLECTION, $form);
		$projectQuery = [
			"queryAvancement" => [],
			"queryContributors" => []
		];
		if (isset($searchParams["filters"]["properties.avancement"])) {
			$projectQuery["queryAvancement"]["properties.avancement"] = $searchParams["filters"]["properties.avancement"];
			unset($searchParams["filters"]["properties.avancement"]);
		}

		if (isset($searchParams["name"])) {
			$projectQuery["queryName"]["name"] = $searchParams["name"];
			unset($searchParams["name"]);
		}

		if (!empty(Yii::app()->session['userId']) && isset($searchParams["filters"]['$or']["links.contributors." . Yii::app()->session['userId']])) {
			$projectQuery["queryContributors"]["links.contributors." . Yii::app()->session['userId']] = $searchParams["filters"]['$or']["links.contributors." . Yii::app()->session['userId']];
			unset($searchParams["filters"]['$or']["links.contributors." . Yii::app()->session['userId']]);
		}


		if (isset($searchParams["filters"]['$or']["user"]))
			unset($searchParams["filters"]['$or']["user"]);
		if (empty($searchParams["filters"]['$or']))
			unset($searchParams["filters"]['$or']);

		$query = self::globalAutocompleteProposalQuery($form, $searchParams, null, ["fieldPrefix" => "answerVal.0"]);

		$projectsIds = [];
		$haveProjectFields = false;
		// if (!empty($searchParams["name"]) || isset($queryAvancement["properties.avancement"]) || (!empty(Yii::app()->session['userId']) && isset($queryContributors["links.contributors.".Yii::app()->session['userId']]))) {
		// 	if (isset($queryAvancement["properties.avancement"])) {
		// 		$searchParams["filters"]["properties.avancement"] = $queryAvancement["properties.avancement"];
		// 	}
		// 	if (!empty(Yii::app()->session['userId']) && isset($queryContributors["links.contributors.".Yii::app()->session['userId']])) {
		// 		$searchParams["filters"]['$or']["links.contributors.".Yii::app()->session['userId']] = $queryContributors["links.contributors.".Yii::app()->session['userId']];
		// 	}
		// 	$projectsIds = self::globalAutocompleteProjectIds($form, $context, $searchParams, $userId = null);
		// 	if (isset($searchParams["filters"]["properties.avancement"])) {
		// 		unset($searchParams["filters"]["properties.avancement"]);
		// 	}
		// 	$projectsIds = array_map(fn($v) => (string)$v, $projectsIds);
		// 	$haveProjectFields = true;

		// 	if(!empty(Yii::app()->session['userId']) && isset($searchParams["filters"]['$or']["links.contributors.".Yii::app()->session['userId']]))
		// 		unset($searchParams["filters"]['$or']["links.contributors.".Yii::app()->session['userId']]);
		// 	if(empty($searchParams["filters"]['$or']))
		// 		unset($searchParams["filters"]['$or']);

		// 	// if(!empty($query["query"]['$and'][0]['$or']) && !empty($projectsIds)) {
		// 	if (!empty($query["query"]['$and']) && !empty($projectsIds)) {
		// 		// $query["query"]['$and'][0]['$or'][] = array('project.id' => ['$in' => $projectsIds]);
		// 		$query["query"]['$and'][] = array('project.id' => ['$in' => $projectsIds]);
		// 	}
		// }

		// $projectFieldName = "projectsAnswers";
		// $searchParamsSortBy = $query["searchParams"]['sortBy'];
		// if (isset($query["searchParams"]['sortBy']) && !empty(array_key_first($query["searchParams"]['sortBy']))) {
		// 	$keySortBy = array_key_first($query["searchParams"]['sortBy']);
		// 	$valueSortBy = $query["searchParams"]['sortBy'][$keySortBy];
		// 	switch ($keySortBy) {
		// 		case 'answers.aapStep1.titre':
		// 			$searchParamsSortBy = [];
		// 			$searchParamsSortBy["$projectFieldName.lowerName"] = $valueSortBy;
		// 			break;
		// 		case 'created':
		// 			$searchParamsSortBy = [];
		// 			$searchParamsSortBy["$projectFieldName.created"] = $valueSortBy;
		// 			break;
		// 		case 'updated':
		// 			$searchParamsSortBy = [];
		// 			$searchParamsSortBy["$projectFieldName.updated"] = $valueSortBy;
		// 			break;

		// 		default:
		// 			$searchParamsSortBy = [];
		// 			$searchParamsSortBy[$keySortBy] = $valueSortBy;
		// 			break;
		// 	}
		// }
		// $queryProposal = [
		// 	array(
		// 		'$match' => $query["query"]
		// 	),
		// 	array(
		// 		'$set' => [
		// 			"project" => [
		// 				'$cond' => [
		// 					'if' => [ '$isArray' => '$project' ],
		// 					'then' => [
		// 						'$arrayElemAt' => ['$project', 0] 
		// 					],
		// 					'else' => '$project'
		// 				]
		// 			]
		// 		]
		// 	),
		// 	array(
		// 		'$addFields' => [
		// 			"project"   => [
		// 				'$cond' => [
		// 					'if'   => [
		// 						'$regexMatch' => [
		// 							'input' => '$project.id',
		// 							'regex' => new MongoRegex("/^[0-9a-fA-F]{24}$/"),
		// 						]
		// 					],
		// 					'then' => ['$toObjectId' => '$project.id'],
		// 					'else' => '$project.id'
		// 				]
		// 			],
		// 			"lowerName" => [
		// 				'$toLower' => '$projectsAnswers.name'
		// 			]
		// 		]
		// 	),
		// 	array(
		// 		'$lookup' => [
		// 			"from"         => "projects",
		// 			"localField"   => 'project',
		// 			"foreignField" => '_id',
		// 			"as"           => "$projectFieldName",
		// 		]
		// 	),
		// 	array(
		// 		'$unwind' => ['path' => '$projectsAnswers', 'preserveNullAndEmptyArrays' => true]
		// 	),
		// 	array(
		// 		'$group' => [
		// 			"_id"               => '$project',
		// 			"$projectFieldName" => ['$first' => '$projectsAnswers']
		// 		]
		// 	),
		// 	array(
		// 		'$addFields' => [
		// 			'projectsAnswers.lowerName' => [
		// 				'$toLower' => '$projectsAnswers.name'
		// 			]
		// 		]
		// 	),
		// 	array('$sort' => $searchParamsSortBy),
		// 	array('$skip' => (int)$query["searchParams"]["indexMin"]),
		// 	array('$limit' => (int)$query["searchParams"]["indexStep"]),
		// 	array(
		// 		'$project' => [
		// 			"_id"               => 1,
		// 			//"answers" => '$answers',
		// 			"$projectFieldName" => 1,
		// 			"lowerName"         => 1,
		// 		]
		// 	)
		// ];
		$answerFieldName = "answerVal";
		$searchParamsSortBy = $query["searchParams"]['sortBy'];
		if (isset($query["searchParams"]['sortBy']) && !empty(array_key_first($query["searchParams"]['sortBy']))) {
			$keySortBy = array_key_first($query["searchParams"]['sortBy']);
			$valueSortBy = $query["searchParams"]['sortBy'][$keySortBy];
			switch ($keySortBy) {
				case 'answers.aapStep1.titre':
					$searchParamsSortBy = [];
					$searchParamsSortBy["lowerName"] = $valueSortBy;
					break;
				case 'answers.aapStep2.allVotes':
					$searchParamsSortBy = [];
					$searchParamsSortBy[$answerFieldName . ".0." . $keySortBy] = $valueSortBy;
					break;
				default:
					$searchParamsSortBy = [];
					$searchParamsSortBy[$keySortBy] = $valueSortBy;
					break;
			}
		}
		$queryProjects = [
			[
				'$match' => [
					'$and' => [
						[
							'$or' => [
								[
									"parent." . $context ?? "notdefined" => [
										'$exists' => true
									]
								],
								[
									"parentId" => $context ?? "notdefined"
								]
							]
						],
						[
							"name" => [
								'$exists' => true
							]
						]
					]
				]
			],
			// [
			//   '$addFields' => [
			// 		"projectId" => [
			// 			'$toString' => '$_id'
			// 		]
			//   ]
			// ],
			// [
			// 	'$lookup' => [
			// 		"from" => "answers",
			// 		"localField" => 'projectId',
			// 		"foreignField" => "project.id",
			// 		"as" => "answerVal"
			// 	]
			// ],
			[
				'$addFields' => [
					'lowerName' => [
						'$toLower' => '$name'
					]
				]
			],
			['$sort' => $searchParamsSortBy],
		];

		if (!empty($projectQuery["queryName"]["name"]) && !empty($queryProjects[0]['$match']['$and'])) {
			$textRegExp = SearchNew::accentToRegex($projectQuery["queryName"]["name"]);
			$arrayName = ["name" => new MongoRegex("/.*{$textRegExp}.*/i")];
			$queryProjects[0]['$match']['$and'][] = $arrayName;
		}

		if (!empty($projectQuery["queryContributors"]) && !empty($queryProjects[0]['$match']['$and'])) {
			$queryProjects[0]['$match']['$and'][] = $projectQuery["queryContributors"];
		}

		if (!empty($projectQuery["queryAvancement"]) && !empty($queryProjects[0]['$match']['$and'])) {
			if (array_search('notSpecified', $projectQuery["queryAvancement"]["properties.avancement"], true) !== false) {
				$queryProjects[0]['$match']['$and'][] = [
					'$or' => [
						["properties.avancement" => ['$in' => $projectQuery["queryAvancement"]["properties.avancement"]]],
						["properties.avancement" => ['$exists' => false]]
					]
				];
			} else
				$queryProjects[0]['$match']['$and'][] = ["properties.avancement" => ['$in' => $projectQuery["queryAvancement"]["properties.avancement"]]];
		}

		if (isset($query["query"]['$and']) && count($query["query"]['$and']) > 2) {
			$queryProjects[] = [
				'$addFields' => [
					"projectId" => [
						'$toString' => '$_id'
					]
				]
			];
			$queryProjects[] = [
				'$lookup' => [
					"from" => "answers",
					"localField" => 'projectId',
					"foreignField" => "project.id",
					"as" => "answerVal"
				]
			];
			$queryProjects[] = ['$match' => $query["query"]];
		}

		$projectsCountQuery = $queryProjects;
		$projectsCountQuery[] = [
			'$group' => [
				"_id" => null,
				"count" => ['$sum' => 1]
			]
		];
		$projectsCount = PHDB::aggregate(Project::COLLECTION, $projectsCountQuery);
		array_push($queryProjects, ['$skip' => (int)$query["searchParams"]["indexMin"]]);
		if ((int)$query["searchParams"]["indexStep"] > 0)
			array_push($queryProjects, ['$limit' => (int)$query["searchParams"]["indexStep"]]);
		$countProposals = []; //PHDB::distinct(Form::ANSWER_COLLECTION, "project.id", $query["query"]);

		$proposals = []; //PHDB::aggregate(Form:: ANSWER_COLLECTION, $queryProposal);
		$projects = [];
		// foreach ($proposals["result"] as $kp => $vp) {
		// 	if ($haveProjectFields && !empty($projectsIds)) {
		// 		if (!empty($vp["$projectFieldName"]["_id"]) && in_array((string)$vp["$projectFieldName"]["_id"], $projectsIds)) {
		// 			$id = (string)$vp["$projectFieldName"]["_id"];
		// 			$projects[$id] = $vp["$projectFieldName"];
		// 			if (empty($projects[$id]["parent"]))
		// 				$projects[$id]["parent"] = $formData["parent"];
		// 		}
		// 	} else if ($haveProjectFields == false) {
		// 		if (!empty($vp["$projectFieldName"]["_id"])) {
		// 			$id = (string)$vp["$projectFieldName"]["_id"];
		// 			$projects[$id] = $vp["$projectFieldName"];
		// 			if (empty($projects[$id]["parent"]))
		// 				$projects[$id]["parent"] = $formData["parent"];
		// 		}
		// 	}
		// }

		// if ($haveProjectFields && !empty($projectsIds)) {
		// 	$proposalWithProject = array_intersect($countProposals, $projectsIds);
		// 	$countProposals = count(array_unique($proposalWithProject));
		// } else if ($haveProjectFields && empty($projects)) {
		// 	$countProposals = 0;
		// } else if ($haveProjectFields == false) {
		// 	$countProposals = count($countProposals);
		// }
		$projectsResult = PHDB::aggregate(Project::COLLECTION, $queryProjects);
		// array_map(function($val) use (&$projects) {
		// 	$projects[(string) $val["_id"]] = $val;
		// }, $projectsResult["result"]);
		$markdown_parser = new Markdown();
		$markdown_parser->html5 = true;
		foreach ($projectsResult['result'] as $db_project) {
			$id = (string) $db_project["_id"];
			$description = $db_project['description'] ?? '';
			$depenses = [];
			$interrested = [];
			$contributors = !empty($db_project['links']['contributors']) && is_array($db_project['links']['contributors']) ? $db_project['links']['contributors'] : [];

			$parent = [
				'parentId'		=> $id,
				'parentType'	=> Project::COLLECTION
			];
			$status = 'totest';
			$db_project['image'] = !empty($db_project['profilImageUrl']) ? $db_project['profilImageUrl'] : Yii::app()
				->getModule('co2')
				->getAssetsUrl() . '/images/thumb/default_projects.png';
			$db_project['descriptionStr'] = $description;
			$db_project['funds'] = $depenses;
			$db_project['tags'] = !empty($db_project['tags']) && is_array($db_project['tags']) ? array_values($db_project['tags']) : [];
			$db_project['user_count'] = count($contributors);
			$db_project['interrest_count'] = count($interrested);
			$db_project['action_count'] = Action::countByStatus($parent, $status);
			$projects[$id] = $db_project;
		}
		// $projects = iterator_to_array($projectsResult);
		$result["results"] = $projects;
		$result["count"] = [
			"projects" => !empty($projectsCount["result"][0]["count"]) ? $projectsCount["result"][0]["count"] : 0
		];
		$result['data'] = Aap::parse_project_data($result);
		return $result;
	}

	public static function prepareAapGlobalData($entries) {
		/*if(
				!empty(CacheHelper::get("prepareAapGlobalData".$entries['formid']))
			){
				return CacheHelper::get("prepareAapGlobalData".$entries['formid']);
			}*/
		$userSession = Yii::app()->session['userId'] ?? '';
		$costum = CacheHelper::getCostum();
		$params = [
			"isAdmin"           => false,
			"page"              => $entries["page"],
			"pageDetail"        => [
				"#" . $entries["page"] . "Aap" => $costum["app"]["#" . $entries["page"] . "Aap"] ?? ""
			],
			"stepAccess"        => [],
			"listAccess"        => false,
			"defaultProject"    => null,
			"myDefaultProject"  => null,
			"defaultAnswer"     => null,
			"defaultProposal"   => null,
			"myDefaultProposal" => null,
			"aapSession"        => null,
			"renderThisStep"    => $entries["renderThisStep"],
			"haveAnswer"        => false,
			"admin_projects"	=> [],
		];
		if (!empty($entries['answerId'])) {
			if (MongoId::isValid($entries['answerId'])) {
				$params['answer'] = PHDB::findOne(Answer::COLLECTION, ['_id' => new MongoId($entries['answerId'])], ["answers.aapStep1.titre"]);
				//$params['answer'] = self::parse_proposition_data(['results' => $params['answer']]);
			}
			$params['answerId'] = $entries['answerId'];
		}
		if (!empty($entries['projectId'])) {
			if (MongoId::isValid($entries['projectId'])) {
				$params['project'] = PHDB::findOne(Project::COLLECTION, ['_id' => new MongoId($entries['projectId'])], ['name']);
			}
			$params['projectId'] = $entries['projectId'];
		}
		$db_projects = [];
		if (!empty($userSession))
			$db_projects = PHDB::find(Project::COLLECTION, [
				'$or'	=> [
					["creator"								=> $userSession],
					["links.contributors.$userSession.type"	=> Person::COLLECTION],
				]
			], ["_id", "name"]);
		foreach ($db_projects as $id => $db_project)
			$params["admin_projects"][] = [
				"id"	=> $id,
				"name"	=> $db_project["name"]
			];
		if (!empty($entries["context"])) {
			$params["canEdit"] = false;
			$params["canParticipate"] = false;
			$params["canSee"] = false;

			$contextSlug = Slug::getBySlug($entries["context"]);

			$params["context"] = PHDB::findOne($contextSlug['type'], ["slug" => $entries["context"]]/*,["name","slug","profilImageUrl","collection","oceco","links.members"]*/);

			$params["context"] = Element::getElementForJS($params["context"], $params["context"]["collection"]);
			$type = $params["context"]["collection"];
			$id = (string)$params["context"]["_id"];
			if (Authorisation::canEditItem(Yii::app()->session["userId"], $type, $id) || Authorisation::userOwner(Yii::app()->session["userId"], $type, $id)) {
				$params["canEdit"] = true;
				$params["canParticipate"] = true;
				$params["canSee"] = true;
			} elseif (Authorisation::canParticipate(Yii::app()->session["userId"], $type, $id)) {
				$params["canParticipate"] = true;
				$params["canSee"] = true;
			} elseif (Authorisation::canSee($type, $id)) {
				$params["canSee"] = true;
			}

			$dbProjectList = PHDB::findAndSort(
				Project::COLLECTION,
				[
					"parent." . $contextSlug["id"] => ['$exists' => 1]
				],
				["updated" => -1],
				0,
				["name", "collection", "slug", 'links']
			);
			$project_ids = [];
			foreach ($dbProjectList as $key => $project) {
				array_push($project_ids, $key);
			}
			$dbProjectAnswerList = [];
			$queryAnswerList = [
				"answers.aapStep1.titre" => ['$exists' => 1],
				"project.id" => ['$exists' => 1],
				"project.id" => ['$in' => $project_ids]
			];
			isset($entries['formid']) ? $queryAnswerList["form"] = ['$in' => [$entries['formid']]] : "";
			$dbProjectAnswerList = PHDB::aggregate(
				Answer::COLLECTION,
				[
					array(
						'$match' => $queryAnswerList
					),
					array(
						'$set' => [
							"project" => [
								'$cond' => [
									'if' => ['$isArray' => '$project'],
									'then' => [
										'$arrayElemAt' => ['$project', 0]
									],
									'else' => '$project'
								]
							]
						]
					),
					array(
						'$addFields' => [
							"project"   => [
								'$cond' => [
									'if'   => [
										'$regexMatch' => [
											'input' => '$project.id',
											'regex' => new MongoRegex("/^[0-9a-fA-F]{24}$/"),
										]
									],
									'then' => ['$toObjectId' => '$project.id'],
									'else' => '$project.id'
								]
							],
							"answer" => '$_id'
						]
					),
					array(
						'$lookup' => [
							"from"         => "projects",
							"localField"   => 'project',
							"foreignField" => '_id',
							"as"           => "projectsAnswers",
						]
					),
					array(
						'$unwind' => ['path' => '$projectsAnswers', 'preserveNullAndEmptyArrays' => true]
					),
					array(
						'$addFields' => [
							'projectsAnswers.lowerName' => [
								'$toLower' => '$projectsAnswers.name'
							],
							'answer' => '$_id'
						]
					),
					array(
						'$project' => [
							"_id"             => 1,
							"projectsAnswers" => 1,
							"answer"          => 1,
						]
					),
					array('$sort' => ["projectsAnswers.updated" => -1]),
				]
			);
			$dbProjectList = [];
			if (isset($dbProjectAnswerList["result"]))
				foreach ($dbProjectAnswerList["result"] as $key => $project) {
					$dbProjectList[(string) $project["_id"]] = [
						"_id" => $project["projectsAnswers"]["_id"],
						"collection" => isset($project["projectsAnswers"]["collection"]) ? $project["projectsAnswers"]["collection"] : "projects",
						"links" => isset($project["projectsAnswers"]["links"]) ? $project["projectsAnswers"]["links"] : [],
						"name" => isset($project["projectsAnswers"]["name"]) ? $project["projectsAnswers"]["name"] : "",
						"answer" => isset($project["answer"]) ? $project["answer"] : "",
					];
				}
			if (!empty($dbProjectList)) {
				if (!empty(Yii::app()->session['userId'])) {
					foreach ($dbProjectList as $dbProject) {
						if (!empty($dbProject['links']['contributors'][$userSession])) {
							$params["myDefaultProject"] = $dbProject;
							break;
						}
					}
				}
				$params["defaultProject"] = array_values($dbProjectList)[0];
				$dbProjectRoom = PHDB::findOne(\Room::COLLECTION, [
					'$or' => [
						[
							'parentId'   => (string)$params['defaultProject']['_id'],
							'parentType' => Project::COLLECTION
						],
						[
							'parentId'   => $contextSlug['id'],
							'parentType' => $contextSlug['type']
						]
					]
				], ['_id']);

				if (!empty($dbProjectRoom)) {
					$params["defaultProject"]['room'] = (string)$dbProjectRoom['_id'];
				}
				$defaultAnswer = PHDB::findOne(Form::ANSWER_COLLECTION, [
					'form'       => $entries['formid'],
					"project.id" => (string)$params["defaultProject"]["_id"]
				]);
			}
			if (!empty($userSession)) {
				$myProposals = PHDB::findAndSort(Form::ANSWER_COLLECTION, [
					'form' => $entries['formid'],
					'$and' => [
						['answers.aapStep1.titre' => ['$exists' => 1]],
						[
							'$or' => [
								['user' => $userSession],
								["links.contributors.$userSession" => ['$exists' => 1]],
							]
						]
					]
				], ['updated' => -1], 1, ['_id', 'answers.aapStep1.titre', "project.id"]);
				if (!empty($myProposals)) {
					$params['myDefaultProposal'] = array_values($myProposals)[0];
				}
			}
			if (!empty($defaultAnswer))
				$params["defaultAnswer"] = $defaultAnswer;
		}
		if (!empty($entries["aapview"]))
			$params["aapview"] = $entries["aapview"];

		if (!empty($entries["formid"])) {
			$params["form"] = PHDB::findOneById(Form::COLLECTION, $entries["formid"]);
			if (!empty($params["form"]["config"])) {
				$params["formConfig"] = PHDB::findOneById(Form::COLLECTION, $params["form"]["config"]);
			}

			if (!empty($params["formConfig"]["parent"])) {
				$id = array_keys($params["formConfig"]["parent"])[0];
				$type = $params["formConfig"]["parent"][$id]["type"];
				$params["elConfig"] = PHDB::findOneById($type, $id, array("slug", "name"));
			}

			if (isset($params["elConfig"]) && isset($params["elConfig"]["slug"]) && $params["elConfig"]["slug"] != $costum["slug"]) {
				$params["incompatible"] = true;
			}

			$params["inputs"] = PHDB::find(Form::INPUTS_COLLECTION, array("formParent" => $entries["formid"]));
			$params["showMap"] = false;
			if (!empty($params["inputs"])) {
				if (isset(array_values($params["inputs"])[0]["inputs"]["adress"])) {
					$params["showMap"] = true;
				}
			}

			if (!empty($params["form"])) {
				$timezone = !empty(Yii::app()->session["timezone"]) ? Yii::app()->session["timezone"] : "UTC";
				date_default_timezone_set($timezone);
				$startDate = !empty($params["form"]["startDate"]) ? $params["form"]["startDate"]->toDateTime()->format('c') : null;
				$endDate = !empty($params["form"]["endDate"]) ? $params["form"]["endDate"]->toDateTime()->format('c') : null;
				$now = date('d-m-Y H:i', time());
				$checkDate = Api::checkDateStatus($now, $startDate, $endDate);
				if (!empty($params['form']['startDate'])) $startDateTimestamp = UtilsHelper::get_as_timestamp(['startDate'], $params["form"]);
				if (!empty($params['form']['endDate'])) $endDateTimestamp = UtilsHelper::get_as_timestamp(['endDate'], $params["form"]);

				$showStartDate = !empty($startDateTimestamp) ? UtilsHelper::timestampToLocalDateString($startDateTimestamp, ['getHours' => true]) : $startDate;
				$showEndDate = !empty($endDateTimestamp) ? UtilsHelper::timestampToLocalDateString($endDateTimestamp, ['getHours' => true]) : $endDate;

				if (!empty($startDate) && !empty($endDate)) $message = " ($showStartDate => $showEndDate)";
				elseif (!empty($startDate) && empty($endDate)) $message = " (" . Yii::t('common', 'date.From') . " $showStartDate)";
				elseif (empty($startDate) && !empty($endDate)) $message = " (" . Yii::t('common', 'date.Till') . " $showEndDate)";
				else $message = "";

				if ($checkDate === "include") {
					$params["session"] = array(
						"status" => "include",
						"msg"    => Yii::t("common", "Open deposit session") . $message,
						"class"  => "text-aap-primary"
					);
				} elseif ($checkDate === "late")
					$params["session"] = array(
						"status" => "late",
						"msg"    => Yii::t("common", "Deposit session not started") . $message,
						"class"  => "letter-red"
					);
				else
					$params["session"] = array(
						"status" => "past",
						"msg"    => Yii::t("common", "Closed deposit session") . $message,
						"class"  => "letter-red"
					);

				if (!empty(Yii::app()->session["aapSession-" . $entries["formid"]])) {
					$params["aapSession"] = Yii::app()->session["aapSession-" . $entries["formid"]];
				} elseif (!empty($params["form"]["actualSession"])) {
					$params["aapSession"] = $params["form"]["actualSession"];
				} else {
					$params["aapSession"] = date('Y');
				}
			}
		}

		if (
			!empty((string)$params["context"]["_id"]) && !empty($params["context"]["collection"]) && !empty($params["formConfig"])
			&& !empty($params["context"]["oceco"]["subOrganization"]) && $params["context"]["oceco"]["subOrganization"]
		) {
			$params["subOrganizations"] = self::getSubOrganization(
				(string)$params["context"]["_id"],
				$params["context"]["collection"],
				(string)$params["formConfig"]["_id"]
			);
		}
		$params["status"] = Aap::badgelabel();

		$params["stepAccess"] = Form::getParamsWizard($params['form'], @$params['answer'], false, null);

		if (
			!empty(Yii::app()->session["userId"]) &&
			(
				(isset($params["context"]["links"]["members"][Yii::app()->session["userId"]]["isAdmin"]) && $params["context"]["links"]["members"][Yii::app()->session["userId"]]["isAdmin"] == true) ||
				(
					(empty($params["form"]["onlyMembersCanSeeListAnswer"]) ||
						$params["form"]["onlyMembersCanSeeListAnswer"] == false ||
						($params["form"]["onlyMembersCanSeeListAnswer"] == true && isset($params["context"]["links"]["members"][Yii::app()->session["userId"]]))
					) &&
					(
						empty($params["form"]["listAccessHaveRules"]) ||
						empty($params["form"]["listAccessRules"]) ||
						$params["form"]["listAccessHaveRules"] == false ||
						(isset($params["context"]["links"]["members"][Yii::app()->session["userId"]]["roles"]) && $params["form"]["listAccessHaveRules"] == true && sizeof(array_intersect($params["form"]["listAccessRules"], $params["context"]["links"]["members"][Yii::app()->session["userId"]]["roles"])) > 0)
					)
				)
			)
		) {
			$params["listAccess"] = true;
		}

		if (!empty($entries["formid"])) {
			$defaultProposal = PHDB::findAndSort(
				Form::ANSWER_COLLECTION,
				array("form" => $entries["formid"]),
				array("updated" => -1),
				1,
				array(
					"_id",
					"answers.aapStep1.titre",
					"project.id"
				)
			);
			if (!empty($defaultProposal))
				$params["defaultProposal"] = array_values($defaultProposal)[0];
			$params['haveAnswer'] = !empty(Yii::app()->session['userId']) && PHDB::count(Form::ANSWER_COLLECTION, [
				'user' => Yii::app()->session['userId'],
				'form' => $entries["formid"],
				"answers.aapStep1.titre" => ['$exists' => 1]
			]) > 0;
		}

		if (!empty($entries["formid"])) {
			//intervention area(quartier)
			$params["quartiers"] = PHDB::distinct(Form::ANSWER_COLLECTION, "answers.aapStep1.interventionArea", array(
				"form"                   => $entries["formid"],
				"answers.aapStep1.titre" => ['$exists' => true],
				"answers.aapStep1.year"  => $params["aapSession"]
			));
			$params["quartiers"] = array_map(function ($q) {
				if (Api::isValidMongoId($q))
					return new MongoId($q);
			}, $params["quartiers"]);
			$params["quartiers"] = PHDB::find(Zone::COLLECTION, array(
				'$or' => array(
					array("_id" => ['$in' => $params["quartiers"]]),
					array(
						"insee" => "97411",
						"codeQPV" => array('$exists' => true)
					)
				)
			), array("name"));
			//fonds
			$params["fonds"] = PHDB::distinct(Form::ANSWER_COLLECTION, "answers.aapStep1.depense.financer.line", array(
				"form"                                   => $entries["formid"],
				"answers.aapStep1.titre"                 => ['$exists' => true],
				"answers.aapStep1.depense.financer.line" => ['$exists' => true],
				"answers.aapStep1.year"                  => $params["aapSession"]
			));
			//associations
			$params["associations"] = PHDB::distinct(Form::ANSWER_COLLECTION, "answers.aapStep1.association", array(
				"form"                         => $entries["formid"],
				"answers.aapStep1.titre"       => ['$exists' => true],
				"answers.aapStep1.association" => ['$exists' => true],
				"answers.aapStep1.year"        => $params["aapSession"]
			));
		}
		CacheHelper::set("prepareAapGlobalData" . $entries['formid'], $params);

		return $params;
	}

	public static function parse_project_data(array &$__input) {
		$elements = ['users', 'organizations', 'additional_data', 'answer'];
		foreach ($elements as $element) $$element = ['ids' => [], 'data' => []];

		if (!empty($__input['results']) && is_array($__input['results'])) {
			$answer['data'] = PHDB::find(Form::ANSWER_COLLECTION, ["project.id" => ['$in' => array_keys($__input['results'])]]);
			foreach ($__input['results'] as $id => $input) {
				$parent = [
					'parentId'		=> $id,
					'parentType'	=> Project::COLLECTION
				];
				$status = 'totest';
				$additional_data['data'][$id] = [
					'action_badge'	=> Action::countByStatus($parent, $status),
					'total_task'	=> 0,
					'task_done'		=> 0,
					'urgency'		=> count(array_filter($answer['data'], function ($answer) use ($id) {
						$project_id = $answer['project']['id'] ?? '';
						$urgency = !empty($answer['answers']['aapStep1']['urgency']) && is_array($answer['answers']['aapStep1']['urgency']) ? $answer['answers']['aapStep1']['urgency'] : [];
						return $project_id === $id && in_array('Urgent', $urgency);
					}))
				];

				if (!empty($input['creator'])) UtilsHelper::push_array_if_not_exists($input['creator'], $users['ids']);
				if (!empty($input['links']['contributors'])) {
					foreach ($input['links']['contributors'] as $contributorId => $oneContributor) {
						if (!empty($oneContributor['type']) && $oneContributor['type'] === Person::COLLECTION) UtilsHelper::push_array_if_not_exists($contributorId, $users['ids']);
					}
				}
				if (!empty($input['parent'])) {
					foreach ($input['parent'] as $parent_id => $project_parent) {
						if (!empty($project_parent['type']) && $project_parent['type'] === Organization::COLLECTION) UtilsHelper::push_array_if_not_exists($parent_id, $organizations['ids']);
					}
				}

				$db_actions = PHDB::find(Action::COLLECTION, ['parentId' => $id], ['_id', 'tasks']);
				if (count($db_actions) === 0) {
					$additional_data['data'][$id]['total_task'] = 1;
					if (!empty($input['endDate'])) $additional_data['data'][$id]['task_done'] = 1;
				} else {
					foreach ($db_actions as $db_action) {
						$count_tasks = !empty($db_action['tasks']) ? count($db_action['tasks']) : 0;
						if ($count_tasks > 0) {
							$additional_data['data'][$id]['total_task'] += $count_tasks;
							foreach ($db_action['tasks'] as $db_task) {
								if (!empty($db_task['checkedAt'])) $additional_data['data'][$id]['task_done'] += 1;
							}
						} else {
							$additional_data['data'][$id]['total_task'] += 1;
							if (!empty($db_action['endDate'])) $additional_data['data'][$id]['task_done'] += 1;
						}
					}
				}
			}
		}

		$users['data'] = PHDB::findByIds(Person::COLLECTION, $users['ids'], ['name', 'profilImageUrl']);
		$users['data'] = array_map(function ($mapUser) {
			if (empty($mapUser['profilImageUrl'])) {
				$mapUser['profilImageUrl'] = Yii::app()->getModule('co2')->assetsUrl . '/images/thumb/default_citoyens.png';
			}
			return $mapUser;
		}, $users['data']);
		$organizations['data'] = PHDB::findByIds(Organization::COLLECTION, $organizations['ids'], ['name']);

		$output = [];
		foreach ($elements as $element) $output[$element] = $$element['data'];
		return $output;
	}

	public static function parsePropositionData($allAnswers) {
		$markdown_parser = new Markdown();
		$markdown_parser->html5 = true;
		$users = [
			"ids"  => [],
			"data" => [],
		];
		$usersStatus = [
			"ids"  => [],
			"data" => [],
		];
		$sousOrga = [
			"idForm" => [],
			"data"   => []
		];
		$checkSeen = [
			"ansId" => [],
			"data"  => []
		];
		$allActions = [
			"ansId"      => [],
			"projectId"  => [],
			"projectId2" => [],
			"data"       => []
		];
		$allImages = [
			"ansId" => [],
			"data"  => []
		];

		$allDocuments = [
			"ansId" => [],
			"data"  => []
		];

		$elements = [
			"elId" => [],
			"data" => []
		];

		$inputs = [
			'ids'  => [],
			'data' => []
		];
		$allNotSeenComments = [
			"ids" => [],
			"ansId" => [],
			"data" => [],
		];
		if (!empty($allAnswers["results"])) {
			$db_images = self::getPropositionThumbnail(array_keys($allAnswers["results"]));
			foreach ($allAnswers["results"] as $k => $v) {
				// start preformated
				$as1 = $v['answers']['aapStep1'] ?? [];
				$contributors = !empty($v['links']['contributors']) && is_array($v['links']['contributors']) ? $v['links']['contributors'] : [];
				// $description = !empty($as1['description']) ? substr($as1['description'], 0, 50) : '';
				$description = !empty($as1['description']) ? $as1['description'] : '';
				$depensesnf = !empty($as1['depense']) && is_array($as1['depense']) ? $as1['depense'] : [];
                /*$depenses = array_filter($depenses,function ($depense) {
                    if(!isset($depense["include"]) || $depense["include"] != false) {
                        return true;
                    }else{
                        return false;
                    }
                });*/
                $depenses = [];
                foreach ($depensesnf as $depense){
                    if(!isset($depense["include"]) || $depense["include"] != false) {
                        array_push($depenses, $depense);
                    }
                }
                $depenses = array_map(function ($depense) {
                    $ret = [
                        'price' => isset($depense['price']) ? intval($depense['price']) : 0,
                        'financer' => !empty($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : []
                    ];
                    $ret['financer'] = array_map(fn($financer) => (isset($financer['amount']) ? floatval($financer['amount']) : 0), $ret['financer']);
                    return ($ret);
				}, $depenses);
				$interrested = array_filter($v['vote'] ?? [], fn($item) => $item['status'] === 'love');

				$allAnswers['results'][$k]['name'] = $as1['titre'] ?? "(No title)";
				$allAnswers['results'][$k]['image'] = $db_images[$k];
				$allAnswers['results'][$k]['descriptionStr'] = $description;
				$allAnswers['results'][$k]['tags'] = !empty($as1['tags']) && is_array($as1['tags']) ? array_values($as1['tags']) : [];
				$allAnswers['results'][$k]['funds'] = $depenses;
				$allAnswers['results'][$k]['user_count'] = count($contributors);
				$allAnswers['results'][$k]['interrest_count'] = count($interrested);
				// end preformated
				if (!empty($v['form']))
					UtilsHelper::push_array_if_not_exists($v['form'], $inputs['ids']);
				if (!empty($v["user"]))
					UtilsHelper::push_array_if_not_exists($v["user"], $users['ids']);
				if (!empty($v['links']['contributors']))
					foreach (array_keys($v['links']['contributors']) as $id) UtilsHelper::push_array_if_not_exists($id, $users['ids']);
				if (!empty($v["statusInfo"])) {
					foreach ($v["statusInfo"] as $statInfoValue) {
						if (!empty($statInfoValue["user"]))
							$usersStatus["ids"][] = new MongoId($statInfoValue["user"]);
					}
				}
				if (!empty($v["links"]["contributors"])) {
					foreach ($v["links"]["contributors"] as $kc => $vc)
						$users["ids"][] = new MongoId($kc);
				}
				if (!empty($v["form"])){ 
					$sousOrga["idForm"][] = new MongoId($v["form"]);
					$allNotSeenComments["ids"][] = $v["form"];
				}
				if (!empty($v["project"]["id"])) {
					$allActions["projectId"][] = $v["project"]["id"];
					$allActions["projectId2"][] = new MongoId($v["project"]["id"]);
				}
				if (!empty($v["context"])) {
					$ctx = array_keys($v["context"])[0];
					$elements["elId"][] = new MongoId($ctx);
					isset($v["context"][$ctx]["type"]) ? $elements["elType"] = $v["context"][$ctx]["type"] : "";
				}
				$allActions["ansId"][] = $k;

				$allImages["ansId"][] = $k;
				$allDocuments["ansId"][] = $k;
				$checkSeen["ansId"][] = $k;
			}
		}
		$users['ids'] = array_map(fn($__id) => new MongoId($__id), $users['ids']);
		$users["data"] = PHDB::find(Person::COLLECTION, array("_id" => ['$in' => $users["ids"]]), array(
			'name',
			'slug',
			'profilImageUrl',
			'collection'
		));
		$usersStatus["data"] = PHDB::find(Person::COLLECTION, array("_id" => ['$in' => $usersStatus["ids"]]), array(
			'name',
			'slug',
			'collection'
		));

		$sousOrga["data"] = PHDB::find(Form::COLLECTION, array("_id" => ['$in' => $sousOrga["idForm"]]), array("parent"));
		$checkSeen["data"] = PHDB::find("views", array("parentId" => ['$in' => $checkSeen["ansId"]], "userId" => Yii::app()->session['userId']));
		if (isset(Yii::app()->session['userId']) && isset($allAnswers["results"])) {
			$allNotSeenComments["data"] = PHDB::find(
				Comment::COLLECTION, [
					'$or' => [
						['contextId' => ['$in' => array_keys($allAnswers["results"])]],
						['contextId' => ['$in' => $allNotSeenComments["ids"]]]
					],
					'views.'.Yii::app()->session['userId'] => ['$exists' => false]
				],
				["contextId", "contextType", "path"]
			);
			$allNotSeenComments["data"] = self::formatNotSeenComments($allNotSeenComments["data"]);
		}
		$allActions["data"] = PHDB::find(Action::COLLECTION, [
			'$or'        => [
				array("parentId" => ['$in' => $allActions["projectId"]]),
				array("parentId" => ['$in' => $allActions["projectId2"]]),
				array("answerId" => ['$in' => $allActions["ansId"]])
			],
			"parentType" => Project::COLLECTION,
		]);
		$pattern = implode('|', ["jpeg", "jpg", "gif", "png"]);
		$imageRegex = new MongoRegex("/($pattern)$/i");
		$whereDocument = [
			'id'		=> ['$in' => $allImages["ansId"]],
			'type'		=> Form::ANSWER_COLLECTION,
			'subKey' => 'aapStep1.image',
			'name' => $imageRegex,
			'folder'	=> ['$exists' => 1]
		];
		$allImages["data"] = PHDB::find(Document::COLLECTION, $whereDocument);
		$whereDocument['subKey'] = ['$ne' => 'aapStep1.image'];
		$whereDocument['$or'] = [['doctype' => 'file'], ['docType' => 'file']];
		$whereDocument['name'] = ['$not' => $imageRegex];
		$allDocuments["data"] = PHDB::find(Document::COLLECTION, $whereDocument);
		$elements["data"] = PHDB::find( isset($elements["elType"]) ? $elements["elType"]: Organization::COLLECTION, array("_id" => ['$in' => $elements['elId']]), array(
			"name",
			"slug",
			"collection"
		));
		$inputs['data'] = PHDB::find(Form::INPUTS_COLLECTION, ['formParent' => ['$in' => $inputs['ids']]], ['step', 'inputs']);
		$users['data'] = array_map(function ($mapUser) {
			if (empty($mapUser['profilImageUrl']))
				$mapUser['profilImageUrl'] = Yii::app()->getModule('co2')->assetsUrl . '/images/thumb/default_citoyens.png';
			return $mapUser;
		}, $users['data']);

		$contents = [
			'users',
			'usersStatus',
			'sousOrga',
			'checkSeen',
			'allActions',
			'allImages',
			'allDocuments',
			'allImages',
			'elements',
			'inputs',
			'allNotSeenComments'
		];
		foreach ($contents as $content) $allAnswers[$content] = $$content['data'];
		return $allAnswers;
	}

	public static function formatNotSeenComments($comments) {
		$res = [];
		foreach ($comments as $commentKey => $commentVal) {
			$contextId = $commentVal["contextId"];
			if ($commentVal["contextType"] == Form::COLLECTION && isset($commentVal["path"])) {
				$splitedPath = explode(".", $commentVal["path"]);
				isset($splitedPath[0]) ? $contextId = $splitedPath[2] : "";
			}
			if (!isset($res[$contextId])) {
				$res[$contextId] = 1;
			} else {
				$res[$contextId]++;
			}
		}
		return $res;
	}

	public static function formList($costumSlug) {
		$elementConfig = PHDB::findOne(Organization::COLLECTION, array("slug" => $costumSlug), array("name", "slug", "collection", "oceco"));
		$config = PHDB::findOne(Form::COLLECTION, array(
			"parent." . (string)$elementConfig["_id"] => ['$exists' => true],
			"type" => "aapConfig"
		), array("parent", "hiddenAap"));

		if (!empty($config["_id"])) {
			$parentForms = PHDB::find(Form::COLLECTION, array("config" => (string)$config["_id"]), array("parent", "name"));
			$elemtsIds = [];
			foreach ($parentForms as $id => $val) {
				if (!empty($val["parent"])) {
					$tempId = array_keys($val["parent"])[0];
					$elemtsIds[] = new MongoId($tempId);
				}
			}


			if (!empty($elementConfig["oceco"]["subOrganization"]) && $elementConfig["oceco"]["subOrganization"] && Authorisation::isElementAdmin((string)$elementConfig["_id"], $elementConfig["collection"], Yii::app()->session["userId"])) {
				$subOrg = PHDB::find(Organization::COLLECTION, array("parentId" => (string)$elementConfig["_id"]), array(
					"name",
					"slug",
					"profilImageUrl",
					"collection"
				));
				$elements = PHDB::find(Organization::COLLECTION, array('_id' => ['$in' => $elemtsIds]), array("name", "slug", "profilImageUrl"));
				foreach ($elements as $key => $value) {
					unset($subOrg[$key]);
				}

				$parentFormTemp = PHDB::findOne(Form::COLLECTION, array("config" => (string)$config["_id"]));
				unset($parentFormTemp["_id"]);
				$newForm = [];
				if (!empty($subOrg)) {
					foreach ($subOrg as $kc => $vc) {
						$parentFormTemp["parent"] = [
							$kc => [
								"type" => $vc["collection"],
								"name" => $vc["name"],
							]
						];
						$parentFormTemp["name"] = $vc["name"];
						$newForm[] = $parentFormTemp;
					}

					Yii::app()->mongodb->selectCollection(Form::COLLECTION)->batchInsert($newForm);
					$parentForms = PHDB::find(Form::COLLECTION, array("config" => (string)$config["_id"]), array("parent", "name"));
					$elemtsIds = [];
					foreach ($parentForms as $id => $val) {
						if (!empty($val["parent"])) {
							$tempId = array_keys($val["parent"])[0];
							$elemtsIds[] = new MongoId($tempId);
						}
					}
				}
			}

			$elements = PHDB::find(Organization::COLLECTION, array('_id' => ['$in' => $elemtsIds]), array("name", "slug", "profilImageUrl"));
			return array(
				"parentForms"   => $parentForms,
				"elements"      => $elements,
				"elementConfig" => $elementConfig,
				"config"        => $config,
				"cacs"          => isset($subOrg) ? $subOrg : null
			);
		} else {
			return Yii::t("common", "No data");
		}
	}

	/** generate default url if context and formid missed in aap url*/
	public static function generateDefautlUrl($costumSlug) {
		$info = Slug::getBySlug($costumSlug);
		$elementConfig = PHDB::findOne($info["type"], array("slug" => $costumSlug), array("name", "slug", "collection", "oceco"));
		$config = PHDB::findOne(Form::COLLECTION, array(
			"parent." . (string)$elementConfig["_id"] => ['$exists' => true],
			"type" => "aapConfig"
		), array("parent", "hiddenAap"));
		$parentForms = null;

		if (!empty($config))
			$parentForms = PHDB::findOne(Form::COLLECTION, array(
				"config"                                  => (string)$config["_id"],
				"type"                                    => "aap",
				"parent." . (string)$elementConfig["_id"] => ['$exists' => true]
			), array("parent", "name"));
		if (!empty($parentForms))
			$parentForms["context"] = $elementConfig["slug"];

		if (empty($parentForms) && !empty($config)) {
			$parentForms = PHDB::findOne(Form::COLLECTION, array(
				"config" => (string)$config["_id"],
				"type"   => "aap",
			), array("parent", "name"));

			if (!empty($parentForms)) {
				$parentId = array_keys($parentForms["parent"])[0];
				$parentType = $parentForms["parent"][$parentId]["type"];
				$el = PHDB::findOneById($parentType, $parentId, array("name", "slug", "collection", "oceco"));
				$parentForms["context"] = $el["slug"];
			}
		}

		return $parentForms;
	}

	public static function generateNotificationUrl($answerId, $db_answer = [], $db_form = [], $db_form_config = []) {
		$answer = $db_answer;
		$form = $db_form;
		$formConfig = $db_form_config;
		if (empty($answer))
			$answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerId, array("form"));
		if (empty($form))
			$form = PHDB::findOneById(Form::COLLECTION, $answer["form"], array("config", "parent"));
		if (empty($formConfig))
			$formConfig = PHDB::findOneById(Form::COLLECTION, $form["config"], array("parent"));
		$contextId = array_keys($form["parent"])[0];
		$contextType = $form["parent"][$contextId]["type"];
		$context = PHDB::findOneById($contextType, $contextId, ["slug"]);
		$contextSlug = $context["slug"];

		$costumElementId = array_keys($formConfig["parent"])[0];
		$costumElementType = $formConfig["parent"][$costumElementId]["type"];
		$costumElement = PHDB::findOneById($costumElementType, $costumElementId, ["slug"]);
		$costumElementSlug = $costumElement["slug"];
		$url = "/costum/co/index/slug/" . $costumElementSlug . "#detailProposalAap.context." . $contextSlug . ".formid." . (string)$form["_id"] . ".answerId." . $answerId . ".aapview.summary";
		return $url;
	}

	public static function globalAutocompleteOrganismChooser2($c, $searchParams) {
		$query = array();
		$res = array();
		$types = [Organization::COLLECTION, Project::COLLECTION];

		$allForms = PHDB::find(Form::COLLECTION, array("type" => "aap", "config" => ['$exists' => true]), array("parent", "config"));
		$allFormsId = []; //get all form id that i am members
		$allParentId = [];
		$allConfigId = [];
		$allParentofConfigFormsId = [];
		$allParentofForms = [];
		$allConfigForms = [];
		$allParentofConfigForms = [];
		foreach ($allForms as $kform => $form) {
			$parentId = array_keys($form["parent"])[0];
			$parentType = $form["parent"][$parentId]["type"];
			//if(Authorisation::canParticipate(Yii::app()->session['userId'],$parentType,$parentId)){
			if (Authorisation::isElementMember($parentId, $parentType, Yii::app()->session['userId'])) {
				$allFormsId[] = new MongoId($kform);
				$allParentId[] = new MongoId($parentId);
				$allConfigId[] = new MongoId($form["config"]);
			}
		}
		foreach ($types as $vt) {
			$p = PHDB::find($vt, array("_id" => ['$in' => $allParentId]), array("name", "slug", "collection", "profilImageUrl"));
			$allParentofForms = array_merge($p, $allParentofForms);
		}

		$allConfigForms = PHDB::find(Form::COLLECTION, array("_id" => ['$in' => $allConfigId]), array("parent"));
		foreach ($allConfigForms as $kform => $form) {
			$parentId = array_keys($form["parent"])[0];
			$parentType = $form["parent"][$parentId]["type"];
			$allParentofConfigFormsId[] = new MongoId($parentId);
		}
		foreach ($types as $vt) {
			$p = PHDB::find($vt, array("_id" => ['$in' => $allParentofConfigFormsId]), array("name", "slug", "collection"));
			$allParentofConfigForms = array_merge($p, $allParentofConfigForms);
		}

		if (!empty($searchParams["name"])) {
			$textRegExp = SearchNew::accentToRegex($searchParams["name"]);
			$tempEl = array();
			foreach ($types as $vt) {
				$temp = PHDB::find($vt, array("name" => new MongoRegex("/.*{$textRegExp}.*/i")), array("name", "collection"));
				$tempEl = array_merge($temp, $tempEl);
			}
			$where = [
				'$or' =>
				[array("name" => new MongoRegex("/.*{$textRegExp}.*/i"), "_id" => ['$in' => $allFormsId])]
			];

			foreach ($tempEl as $ktEl => $vtEl) {
				//if(Authorisation::canParticipate(Yii::app()->session['userId'],$vtEl["collection"],$ktEl)){
				if (Authorisation::isElementMember($ktEl, $vtEl["collection"], Yii::app()->session['userId'])) {
					$where['$or'][] = array("parent." . $ktEl => ['$exists' => true], "_id" => ['$in' => $allFormsId]);
				}
			}
			$query = SearchNew::addQuery($query, $where);
		}

		if (!empty($allFormsId)) {
			$searchParams['filters'] = array("_id" => ['$in' => $allFormsId]);
			$query = SearchNew::searchFilters($searchParams['filters'], $query);
		}

		if (!empty($searchParams['sortBy'])) {
			if (Api::isAssociativeArray($searchParams['sortBy'])) {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$key] = (int)$value;
				}
				$searchParams["sortBy"] = $sortBy;
			} else {
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$value] = 1;
				}
				$searchParams["sortBy"] = $sortBy;
			}
		} else
			$searchParams['sortBy'] = array("updated" => -1);


		if (!empty($query)) {
			$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex(
				$searchParams["searchType"][0],
				$query,
				$searchParams['fields'],
				$searchParams['sortBy'],
				$searchParams["indexStep"],
				$searchParams["indexMin"]
			);

			$res["count"] = array();
			$res["count"][$searchParams["searchType"][0]] = PHDB::count(
				$searchParams["searchType"][0],
				$query
			);

			foreach ($res["results"] as $key => $v) {
				if (isset($v["config"]) && isset($allConfigForms[$v["config"]])) {
					$config = $allConfigForms[$v["config"]];
					$pConfigId = array_keys($config["parent"])[0];
					if (isset($allParentofConfigForms[$pConfigId]["slug"])) {
						$pConfigSlug = $allParentofConfigForms[$pConfigId]["slug"];
						$res["results"][$key]["configSlug"] = $pConfigSlug;
						$res["results"][$key]["configFormId"] = $pConfigId;
					} else {
						$res["results"][$key]["configSlug"] = "";
					}
				}
				if (isset($v["parent"])) {
					$parentId = array_keys($v["parent"])[0];
					if (isset($allParentofForms[$parentId]["slug"])) {
						$parentSlug = $allParentofForms[$parentId]["slug"];
						$parentProfilImageUrl = $allParentofForms[$parentId]["profilImageUrl"] ?? "";
						$parentName = $allParentofForms[$parentId]["name"] ?? "";
						$parentType = $allParentofForms[$parentId]["collection"] ?? "";
						$res["results"][$key]["parentSlug"] = $parentSlug;
						$res["results"][$key]["parentProfilImageUrl"] = $parentProfilImageUrl;
						$res["results"][$key]["parentName"] = $parentName;
						$res["results"][$key]["parentId"] = $parentId;
						$res["results"][$key]["parentType"] = $parentType;
					}
				}
			}
		}

		return $res;
	}

	private static function getSubOrganization($parentId, $parentType, $formConfig) {
		$subOrganization = PHDB::find($parentType, array(
			'$or' => [
				array("parentId" => $parentId),
				array("parent." . $parentId => ['$exists' => true]),
				array("_id" => new MongoId($parentId))
			]
		), array("slug", "name", "profilImageUrl", "parentId", "parent"));

		$formConfig = PHDB::findOne(Form::COLLECTION, array("_id" => new MongoId($formConfig)));
		$where = array('$or' => []);
		$where["config"] = (string)$formConfig["_id"];
		if (!empty($subOrganization)) {
			foreach ($subOrganization as $key => $value) {
				$where['$or'][] = array("parent." . $key => ['$exists' => true]);
			}

			$form = PHDB::find(
				Form::COLLECTION,
				$where,
				array("_id", "parent")
			);

			foreach ($form as $key => $value) {
				$keyParent = array_keys($value["parent"])[0];
				$subOrganization[$keyParent]["formId"] = $key;
			}
		}
		return $subOrganization;
	}

	public static function templateEmail($controller, $params = array()) {
		$templates = array();
		if (!empty($params["formId"])) {
			$form = PHDB::findOneById(Form::COLLECTION, $params["formId"], ["params.templateEmails"]);
			if (!empty($form["params"]["templateEmails"])) {
				$templates = $form["params"]["templateEmails"];
				foreach ($templates as $key => $value) {
					$templates[$key]["html"] = $controller->renderPartial($value["html"], $params);
				}
			}
		}
		return $templates;
	}

	public static function parseEmailVariable($str, $answer, $inputs, $financors = null, $params = null) {
		$matches = [];
		preg_match_all('/{(.*?)}/', $str, $matches);
		foreach ($matches[1] as $km => $vm) {
			switch ($vm) {
				case "prop.title":
					$str = str_replace("{" . $vm . "}", self::get_value(["answers", "aapStep1", "titre"], $answer), $str);
					break;
				case "prop.association":
					$str = str_replace("{" . $vm . "}", self::get_value(["answers", "aapStep1", "association"], $answer), $str);
					break;
				case "prop.table.subvention":
					$str = str_replace("{" . $vm . "}", self::getSubventionTable($answer, $financors)["table"], $str);
					break;
				case "prop.subvention":
					$str = str_replace("{" . $vm . "}", self::getSubventionTable($answer, $financors)["total"], $str);
					break;
				case "prop.today":
					$str = str_replace("{" . $vm . "}", date("d/m/Y"), $str);
					break;
				case "prop.id":
					$str = str_replace("{" . $vm . "}", (string)$answer['_id'], $str);
					break;
				case "prop.answerid":
					$str = str_replace("{" . $vm . "}", (string)$answer['_id'], $str);
					break;
				case "prop.html":
					$str = str_replace("{" . $vm . "}", htmlspecialchars("<h1>TRALALALALALALA</h1>"), $str);
					break;
				case "prop.formid":
					$str = str_replace("{" . $vm . "}", $answer["form"], $str);
					break;
				case "prop.signature":
					$str = str_replace("{" . $vm . "}", $answer["signature"], $str);
					break;
				default:
					echo " ";
			}

			if ($params["preview"] == false && $vm == "prop.fakeImg" && isset($params["notificationid"]) && isset($params["template"])) {
				$img = Yii::app()
					->getRequest()
					->getBaseUrl(true) . "/co2/aap/addnotified/answerid/" . (string)$answer['_id'] . "/notificationid/" . $params["notificationid"] . "/template/" . $params["template"];
				$str = str_replace("{" . $vm . "}", $img, $str);
			}
		}
		return $str;
	}

	public static function sendMail($params) {
		if (is_array($params["emailValues"])) {
			$idToMongoId = [];
			foreach ($params["emailValues"] as $kAns => $vans) {
				$idToMongoId[] = new MongoId($kAns);
			}
			$inputs = PHDB::findOne(Form::INPUTS_COLLECTION, array("formParent" => $params["formId"], "step" => "aapStep1"));
			$proposition = PHDB::find(Form::ANSWER_COLLECTION, array("_id" => ['$in' => $idToMongoId]));
			$previewHtml = "";
			$count = 1;
			$html = $params["template"];
			$controller = $params["controller"];
			$emailValues = $params["emailValues"];

			if ($params["useTemplate"] == "true") {
				$templateId = $params["template"];
				$templates = self::templateEmail($controller, $params);
				$html = $templates[$templateId]["html"];

				$params['tplObject'] = $templates[$templateId]["object"];
				$params["noLogoHeader"] = true;
			}
			unset($params["emailValues"]);
			unset($params["controller"]);

			$initImage = Document::getListDocumentsWhere(
				array(
					"id"     => $params["formId"],
					"type"   => 'form',
					"subKey" => $params["template"],
				),
				"image"
			);

			$financors = self::getAllFinancorsByFormId($params["formId"], false);
			foreach ($proposition as $key => $value) {
				$temp = $html;
				$value["signature"] = !empty($initImage[0]["imagePath"]) ? Yii::app()
					->getRequest()
					->getBaseUrl(true) . $initImage[0]["imagePath"] : "empty";
				$params['tplMail'] = array_values($emailValues[$key]);
				if (!empty($params["preview"]) && $params["preview"]) {
					$temp = self::parseEmailVariable($temp, $value, $inputs, $financors, $params);
					$params['temp'] = $temp;
					$params['html'] = $params['temp'];
					$params['propostion'] = $value;
					$previewHtml .= self::previewEmail($params, $count);
				} else {
					$random = rand() . time();
					$params["notificationid"] = $random;
					$temp = self::parseEmailVariable($temp, $value, $inputs, $financors, $params);
					$params["html"] = $temp;
					$params["answerId"] = $key;
					Mail::createAndSend($params);

					$params["status"] = [
						"sent"     => true,
						"sentDate" => time()
					];
					if ("custom-email" != $params["template"]) {
						PHDB::update(
							Form::ANSWER_COLLECTION,
							array("_id" => new MongoId($key)),
							array(
								'$set'      => ["notifications." . $random => $params],
								'$addToSet' => ["status" => $params["template"] . "Sent"]
							),
						);
					}
				}
				$count++;
			}
			return $previewHtml;
		}
	}

	public static function previewEmail($params, $count = 0) {
		$html = "<style>
		.email-template-container{
			padding: 50px !important
		}
		.email-template-container p,.email-template-container span,.email-template-container td,.email-template-container li{
			font-size : 13px !important;
		}
		</style>";
		if ($params["useTemplate"]) {
			$html .= "
			<div class='preview-item'>
				<div class='count'>{$count}</div>
				<div class='preview-item-content'>
					<p>{$params['temp']}</p>
				</div>
			</div>
			";
		} else {
			$html .= "
			<div class='preview-item'>
				<div class='count'>{$count}</div>
				<h6>{$params['propostion']['answers']['aapStep1']['titre']}</h6>
				<div class='preview-item-content'>
					<p>Objet : {$params['tplObject']}</p>
					<p>{$params['temp']}</p>
				</div>
			</div>
			";
		}
		return $html;
	}

	public static function getSubventionTable($answer, $financors) {
		$total = 0;
		$table = "";
		$table .= '<table border="1" cellpadding="2" style="border-color : grey;width: 100%;font-size:12px;"> <tbody>';
		if (!empty($answer["answers"]["aapStep1"]["depense"])) {
			foreach ($answer["answers"]["aapStep1"]["depense"] as $kdep => $vdep) {
				if (!empty($vdep["poste"])) {
					if (strpos(strtolower($vdep["poste"]), strtolower("Subvention AAP Politique de la Ville")) !== false) {
						$table .= "<tr style='padding-top:5px'>
						";
						if (!empty($vdep["financer"])) {
							foreach ($vdep["financer"] as $kfin => $vfin) {
								if (!empty($vfin["name"]) && !empty($financors[$vfin['id']])) {
									$table .= "<td style='font-size:8px;padding-top:5px'><small>" . $financors[$vfin['id']] . "</small></td>";
								} else
									$table .= "<td style='font-size:8px;padding-top:5px'><small>&nbsp;</small></td>";
							}
							$table .= "<td style='font-size:8px;padding-top:5px'><small>TOTAL</small></td>";
						} else {
							$table .= "<td style='font-size:8px;padding-top:5px'><small>&nbsp;</small></td>";
						}
						$table .= "</tr>";

						$table .= "<tr>
						";
						if (!empty($vdep["financer"])) {
							foreach ($vdep["financer"] as $kfin => $vfin) {
								if (!empty($vfin["amount"])) {
									$total += (float)$vfin['amount'];
									$table .= "<td style='font-size:8px;padding-top:5px'><small>" . (float)$vfin['amount'] . " €</small></td>";
								} else
									$table .= "<td style='font-size:8px;padding-top:5px'><small>0 €</small></td>";
							}
							$table .= "<td style='font-size:8px;padding-top:5px'><small>" . $total . " €</small></td>";
						} else {
							$table .= "<td style='font-size:8px;padding-top:5px'><small>&nbsp;</small></td>";
						}

						$table .= "</tr>";
					}
				}
			}
		}
		$table .= '</tbody></table>';
		$res = array("total" => $total . " €", "table" => $table);
		return $res;
	}

	public static function getAllFinancorsByFormId($formId, $getAllItems) {
		$parentForm = PHDB::findOneById(Form::COLLECTION, $formId, ["parent", "type", "params"]);
		$paramsData = [
			"financerTypeList" => Ctenat::$financerTypeList,
			"limitRoles" => ["Financeur"],
			"openFinancing" => true,
			"limitTypes"       => ["organizations"]
		];
		if (isset($parentForm["type"]) && ($parentForm["type"] == "aapConfig" || $parentForm["type"] == "aap")) {
			foreach ($parentForm["parent"] as $parId => $parValue) {
				$contextId = $parId;
				$contextType = $parValue["type"];
			}
		}

		$contextIdType = $parentForm["parent"];
		$communityLinks = Element::getCommunityByParentTypeAndId($contextIdType);

		if (isset($parentForm["params"]["financerLimitRoles"])) $paramsData["limitRoles"] = $parentForm["params"]["financerLimitRoles"];
		if (isset($parentForm["params"]["limitTypes"])) $paramsData["limitTypes"] = $parentForm["params"]["limitTypes"];

		$financors = [
			Organization::COLLECTION => Link::groupFindByType(Organization::COLLECTION, $communityLinks),

			Person::COLLECTION => Link::groupFindByType(Person::COLLECTION, $communityLinks),

			Project::COLLECTION => Link::groupFindByType(Project::COLLECTION, $communityLinks),
		];

		$orgs = [];
		foreach ($financors as $kfin => $vfin) {
			if (in_array($kfin, $paramsData["limitTypes"])) {
				foreach ($financors[$kfin] as $id => $or) {
					$roles = null;

					if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

					if ($paramsData["limitRoles"] && !empty($roles)) {
						foreach ($roles as $i => $r) {
							if ($getAllItems) {
								if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or;
							} else {
								if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or["name"];
							}
						}
					}
				}
			}
		}
		return $orgs;
	}

	public static function getAnswerAuthorization($answer, $user) {
		$dbPerson = PHDB::findOneById(Person::COLLECTION, $user, ['roles']);
		if (!empty($dbPerson['roles']['superAdmin']) && filter_var($dbPerson['roles']['superAdmin'], FILTER_VALIDATE_BOOLEAN)) {
			$output = [
				'reason'  => 'superadmin',
				'canEdit' => true,
				'canRead' => true
			];
		} else {
			$dbAnswer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answer, ['user', 'form']);
			$dbForm = PHDB::findOneById(Form::COLLECTION, $dbAnswer['form'], [
				'parent',
				'startDate',
				'endDate',
				'active',
				'onlyusercanread'
			]);
			$parentKey = array_keys($dbForm['parent'])[0];
			$dbFormParent = PHDB::findOneById($dbForm['parent'][$parentKey]['type'], $parentKey, [
				'links.contributors',
				'links.memberOf',
				'links.members',
			]);

			$dateNow = new DateTime();
			$isStartBefore = !empty($dbForm['startDate']) && $dbForm['startDate']->toDateTime() <= $dateNow;
			$isEndAfter = !empty($dbForm['endDate']) && $dbForm['endDate']->toDateTime() >= $dateNow;
			$isFormAvailable = $isStartBefore && $isEndAfter || $isStartBefore && empty($dbForm['endDate']) || $isEndAfter && empty($dbForm['startDate']) || empty($dbForm['startDate']) && empty($dbForm['endDate']);
			$isFormAvailable = $isFormAvailable && !empty($dbForm['active']) && filter_var($dbForm['active'], FILTER_VALIDATE_BOOLEAN);

			$isUserAdmin = array_filter(['contributors', 'memberOf', 'members'], function ($linkType) use ($dbFormParent, $user) {
				return !empty($dbFormParent['links'][$linkType][$user]['isAdmin']) && filter_var($dbFormParent['links'][$linkType][$user]['isAdmin'], FILTER_VALIDATE_BOOLEAN);
			});
			$isCreator = !empty($dbAnswer['user']) && $dbAnswer['user'] === $user;
			if (count($isUserAdmin) > 0) {
				$output = [
					'reason'  => 'admin',
					'canEdit' => true,
					'canRead' => true
				];
			} // Dates constraints
			elseif ($isCreator) {
				if ($isFormAvailable) {
					$output = [
						'reason'  => 'creator',
						'canEdit' => true,
						'canRead' => true
					];
				} else {
					$output = [
						'reason'  => 'creator',
						'canEdit' => false,
						'canRead' => true
					];
				}
			} else {
				$isCommununity = array_filter(['contributors', 'memberOf', 'members'], function ($linkType) use ($dbFormParent, $user) {
					return !empty($dbFormParent['links'][$linkType][$user]);
				});
				if ($isFormAvailable && count($isCommununity) > 0) {
					$output = [
						'reason'  => 'community',
						'canRead' => true,
						'canEdit' => false
					];
				} else {
					$output = [
						'reason'  => $isFormAvailable ? 'visitor' : 'unavailable',
						'canRead' => empty($dbForm['onlyusercanread']) || !filter_var($dbForm['onlyusercanread'], FILTER_VALIDATE_BOOLEAN),
						'canEdit' => false
					];
				}
			}
		}
		return $output;
	}

	public static function countUserAnswer(string $form, string $user): int {
		// return PHDB::count(Form::ANSWER_COLLECTION, ['user' => $user, 'form' => $form, 'answers.aapStep1.titre' => ['$exists' => 1]]);
		return PHDB::count(Form::ANSWER_COLLECTION, ['user' => $user, 'form' => $form]);
	}

	public static function canCreateProposal(string $form, string $user): bool {
		$dbPerson = PHDB::findOneById(Person::COLLECTION, $user, ['roles']);
		if (!empty($dbPerson['roles']['superAdmin']) && filter_var($dbPerson['roles']['superAdmin'], FILTER_VALIDATE_BOOLEAN)) {
			return true;
		} else {
			$userAnswerCount = self::countUserAnswer($form, $user);
			$dbForm = PHDB::findOneById(Form::COLLECTION, $form, [
				'parent',
				'startDate',
				'endDate',
				'active',
				'onlymemberaccess',
				'oneAnswerPerPers',
			]);

			$parentKey = array_keys($dbForm['parent'])[0];
			$dbFormParent = PHDB::findOneById($dbForm['parent'][$parentKey]['type'], $parentKey, [
				'links.contributors',
				'links.memberOf',
				'links.members',
			]);

			$dateNow = new DateTime();
			$isStartBefore = !empty($dbForm['startDate']) && $dbForm['startDate']->toDateTime() <= $dateNow;
			$isEndAfter = !empty($dbForm['endDate']) && $dbForm['endDate']->toDateTime() >= $dateNow;
			$isFormAvailable = $isStartBefore && $isEndAfter || $isStartBefore && empty($dbForm['endDate']) || $isEndAfter && empty($dbForm['startDate']) || empty($dbForm['startDate']) && empty($dbForm['endDate']);
			$isFormAvailable = $isFormAvailable && !empty($dbForm['active']) && filter_var($dbForm['active'], FILTER_VALIDATE_BOOLEAN);

			$isUserAdmin = array_filter(['contributors', 'memberOf', 'members'], function ($linkType) use ($dbFormParent, $user) {
				return !empty($dbFormParent['links'][$linkType][$user]['isAdmin']) && filter_var($dbFormParent['links'][$linkType][$user]['isAdmin'], FILTER_VALIDATE_BOOLEAN);
			});
			$isCommununity = array_filter(['contributors', 'memberOf', 'members'], function ($linkType) use ($dbFormParent, $user) {
				return !empty($dbFormParent['links'][$linkType][$user]);
			});
			$onlyCommunityCanAnswer = !empty($dbForm['onlymemberaccess']) && filter_var($dbForm['onlymemberaccess'], FILTER_VALIDATE_BOOLEAN);
			$isOneAnswerPerson = !empty($dbForm['oneAnswerPerPers']) && filter_var($dbForm['oneAnswerPerPers'], FILTER_VALIDATE_BOOLEAN);
			return count($isUserAdmin) > 0 || $isFormAvailable && (count($isCommununity) > 0 && $onlyCommunityCanAnswer || count($isCommununity) === 0 && !$onlyCommunityCanAnswer) && ($isOneAnswerPerson && $userAnswerCount === 0 || !$isOneAnswerPerson && $userAnswerCount > 0);
		}
		return false;
	}
	public static function notificationCreateProposal(string $answerId): void {
		$answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerId, ["answers.aapStep1.titre", "context", "project"]);
		$contextId = array_keys($answer["context"])[0];
		$contextType = $answer["context"][$contextId]["type"];
		Notification::constructNotification(
			ActStr::VERB_ADD,
			array(
				"id" => Yii::app()->session["userId"],
				"name" => Yii::app()->session["user"]["name"]
			),
			array(
				"id" => $contextId,
				"type" => $contextType
			),
			array(
				"type" => Form::ANSWER_COLLECTION,
				"id" => $answerId,
				"name" => "",
				"isAap" => true,
				"isConvertedToProject" => !empty($answer["project"]["id"]),
				"url" => Aap::generateNotificationUrl($answerId)
			),
			Form::ANSWER_COLLECTION,
			null,
			null
		);
	}
	/**
	 * Undocumented function
	 *
	 * @param [string|array] $proposition
	 * @return string|array
	 */
	public static function getPropositionThumbnail($proposition) {
		$ids = [];
		$where_image = [
			'type'		=> Form::ANSWER_COLLECTION,
			'id'		=> null,
			'folder'	=> ['$exists'	=> 1],
			'moduleId'	=> ['$exists'	=> 1],
			'name'		=> ['$exists'	=> 1],
			'subKey'	=> 'aapStep1.image',
			'$or'		=> [
				['doctype'	=> 'image'],
				['docType'	=> 'image']
			]
		];
		if (is_string($proposition))
			$ids[] = $proposition;
		elseif (is_array($proposition))
			$ids = $proposition;
		else
			return ([]);
		$where_image['id'] = ['$in' => $ids];
		$db_doc_list = PHDB::findAndSort(Document::COLLECTION, $where_image, ['created'	=> -1], 0, ['folder', 'id', 'moduleId', 'name']);
		$ret = [];
		foreach ($db_doc_list as $db_doc_item)
			$ret[$db_doc_item['id']] = '/upload/' . $db_doc_item['moduleId'] . '/' . $db_doc_item['folder'] . '/' . $db_doc_item['name'];
		foreach ($ids as $id) :
			if (!array_key_exists($id, $ret))
				$ret[$id] = Yii::app()
					->getModule('co2')
					->getAssetsUrl() . '/images/thumbnail-default.jpg';
		endforeach;
		if (is_string($proposition))
			return ($ret[$proposition]);
		return ($ret);
	}

	/**
	 * Undocumented function
	 *
	 * @param [string|array] $proposition
	 * @return string|array
	 */
	public static function getPropositionImages($proposition) {
		$ids = [];
		$where_image = [
			'type'		=> Form::ANSWER_COLLECTION,
			'id'		=> null,
			'folder'	=> ['$exists'	=> 1],
			'moduleId'	=> ['$exists'	=> 1],
			'name'		=> ['$exists'	=> 1],
			'subKey'	=> 'aapStep1.image',
			'$or'		=> [
				['doctype'	=> 'image'],
				['docType'	=> 'image']
			]
		];
		if (is_string($proposition))
			$ids[] = $proposition;
		elseif (is_array($proposition))
			$ids = $proposition;
		else
			return ([]);
		$where_image['id'] = ['$in' => $ids];
		$db_doc_list = PHDB::findAndSort(Document::COLLECTION, $where_image, ['created'	=> -1], 0, ['folder', 'id', 'moduleId', 'name']);
		$ret = [];
		foreach ($db_doc_list as $db_doc_item) {
			if (!isset($ret[$db_doc_item['id']]))
				$ret[$db_doc_item['id']] = [];
			$ret[$db_doc_item['id']][] = '/upload/' . $db_doc_item['moduleId'] . '/' . $db_doc_item['folder'] . '/' . $db_doc_item['name'];
		}
		foreach ($ids as $id) :
			if (!array_key_exists($id, $ret))
				$ret[$id] = [];
		endforeach;
		if (is_string($proposition))
			return ($ret[$proposition]);
		return ($ret);
	}

	public static function get_campagne_config($form) {
		$db_form = PHDB::findOneById(Form::COLLECTION, $form, ["config", "parent"]);
		if (!empty($db_form["config"])) {
			$key = array_keys($db_form["parent"]);
			$key = end($key);
			$costum = CacheHelper::getCostum($key, $db_form["parent"][$key]["type"]);
			$db_form = Form::getConfig($form);
			if (!empty($db_form["campagne"][$costum["campagne"]]))
				return ($db_form["campagne"][$costum["campagne"]]);
		}
		return ([]);
	}

	public static function funding_by_tls($tls, $form) {
		$tls_keys = array_keys($tls);
		$financer_in_cart = function (array $financer, string $tl) {
			if (!empty($financer['bill']) || empty($financer['id']) || $financer['id'] !== $tl)
				return (false);
			if (!empty($financer["payment_method"]) || empty($financer['status']))
				return (false);
			if (!empty($financer["type"]) && !in_array($financer["type"], ["tl", "organization"]))
				return (false);
			return (true);
		};
		$output = [
			'funds'		=> [],
			'campagne'	=> []
		];
		$db_where = [
			'form' => $form,
			'answers.aapStep1.titre' => ['$exists'	=> 1],
			'answers.aapStep1.depense.financer' => [
				'$elemMatch' => [
					'id' => ['$in' => $tls_keys]
				]
			]
		];
		$db_answers = PHDB::find(Form::ANSWER_COLLECTION, $db_where, ['answers.aapStep1.titre', 'answers.aapStep1.depense']);
		$db_answers_pending = array_filter($db_answers, function ($db_answer) {
			$depenses = $db_answer['answers']['aapStep1']['depense'];
			$financers = array_filter($depenses, function ($depense) {
				$pendings = $depense['financer'] ?? [];
				$pendings = array_filter($pendings, function ($financer) {
					$is_pending = empty($financer['status']) || $financer['status'] === 'pending';
					return ($is_pending && empty($financer['bill']));
				});
				return (count($pendings));
			});
			return (count($financers));
		});
		$db_form_config = PHDB::findOneById(Form::COLLECTION, $form, ['config', 'parent']);
		$db_context = PHDB::findOneById(end($db_form_config['parent'])['type'], array_keys($db_form_config['parent'])[0], ['costum.campagne']);
		$campagne_id = $db_context['costum']['campagne'] ?? '';
		$db_form_config = PHDB::findOneById(Form::COLLECTION, $db_form_config['config']);
		if (!empty($campagne_id)) {
			$camp_fund = $db_form_config['campagne'][$campagne_id];
			$camp_fund = intval($camp_fund['campFinanc']);
		} else
			$camp_fund = 0;
		foreach ($tls_keys as $tl) {
			if (!isset($output['campagne'][$tl]))
				$output['campagne'][$tl] = $camp_fund;
		}
		// remaining campagne
		foreach ($db_answers as $db_answer) {
			$depenses = $db_answer['answers']['aapStep1']['depense'];
			foreach ($depenses as $depense) {
				$financers = $depense['financer'] ?? [];
				$financers = array_filter($financers, fn($financer) => !empty($financer['tlid']) && in_array($financer['tlid'], $tls_keys));
				foreach ($financers as $financer) {
					$id = $financer['tlid'];
					$output['campagne'][$id] -= $financer['amount'];
					$output['campagne'][$id] = max(0, $output['campagne'][$id]);
				}
			}
		}
		// get data by tls
		foreach ($tls as $tl => $db_tl) {
			$fund = [
				'id' => $tl,
				'name' => $db_tl['name'],
				'communs'	=> []
			];
			$communs = array_filter($db_answers_pending, function ($ans) use ($financer_in_cart, $tl) {
				$depenses = $ans['answers']['aapStep1']['depense'];
				$financers = array_filter($depenses, function ($d) use ($financer_in_cart, $tl) {
					$financers = !empty($d['financer']) && is_array($d['financer']) ? $d['financer'] : [];
					$financers = array_filter($financers, fn($fin) => $financer_in_cart($fin, $tl));
					return (count($financers));
				});
				return (count($financers));
			});
			$communs = array_map(function ($db_answer) use ($financer_in_cart, $tl) {
				$as1 = $db_answer['answers']['aapStep1'];
				$depenses = $as1['depense'];
				$depenses = array_filter($depenses, function ($depense) use ($tl) {
					$financers = !empty($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : [];
					$financers = array_filter($financers, fn($financer) => !empty($financer['id']) && $financer['id'] === $tl);
					return (count($financers));
				});
				$depenses = array_map(function ($depense) use ($financer_in_cart, $tl) {
					$financers = !empty($depense['financer']) && is_array($depense['financer']) ? $depense['financer'] : [];
					$doubles = array_filter($financers, fn($f) => !empty($f["tlid"]) && $f["tlid"] === $tl && !empty($f["finkey"]));
					$financers = array_filter($financers, fn($fin) => $financer_in_cart($fin, $tl));
					$financers = array_map(function ($financer) use ($doubles) {
						$financer["iscofinancementftl"] = isset($financer["iscofinancementftl"]) ? filter_var($financer["iscofinancementftl"], FILTER_VALIDATE_BOOL) : true;
						$financer["double"] = 0;
						if (!empty($financer["finkey"])) {
							$doubles = array_filter($doubles, fn($f) => $f["finkey"] === $financer["finkey"]);
							foreach ($doubles as $double)
								$financer["double"] += floatval($double["amount"]);
						}
						if (!isset($financer['amount']))
							$financer['amount'] = 0;
						else
							$financer['amount'] = floatval($financer['amount']);
						return ($financer);
					}, $financers);
					$depense['financer'] = array_values($financers);
					foreach (['date', 'poste', 'price'	=> 0] as $required => $default) {
						if (is_numeric($required) && !isset($depense[$default]))
							$depense[$default] = null;
						else if (is_string($required) && !isset($depense[$required]))
							$depense[$required] = $default;
					}
					$depense['date'] = $depense['date'] ?? null;
					return ($depense);
				}, $depenses);
				$depenses = array_values($depenses);
				return ([
					'id' => (string)$db_answer['_id'],
					'titre' => $as1['titre'],
					'depense' => $depenses
				]);
			}, $communs);
			$fund['communs'] = array_values($communs);
			if (!empty($fund['communs']))
				$output['funds'][] = $fund;
		}
		return ($output);
	}
    public static function funding_by_candidat($form ,$financeurId, $candidatId){

        /*$result = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            ['$match' => ['form' => $form]],  // Filtrer par "form"
            ['$unwind' => '$answers.aapStep1.depense'],  // Décomposer les dépenses
            ['$project' => [
                '_id' => 1,
                'poste' => '$answers.aapStep1.depense.poste',
                'price' => '$answers.aapStep1.depense.price',
                'financer' => '$answers.aapStep1.depense.financer',
                'validatedEstimates' => [
                    '$filter' => [
                        'input' => ['$objectToArray' => '$answers.aapStep1.depense.estimates'],
                        'as' => 'estimate',
                        'cond' => [
                            '$and' => [
                                ['$eq' => ['$$estimate.v.validate', 'validated']],  // Estimations validées
                                ['$eq' => ['$$estimate.k', $candidateIdFilter]]  // Filtrer par ID de candidat
                            ]
                        ]
                    ]
                ]
            ]],
            ['$addFields' => [
                'validatedTotal' => [
                    '$sum' => [
                        '$map' => [
                            'input' => '$validatedEstimates',
                            'as' => 'estimate',
                            //m
                            'in' => ['$toDouble' => '$$estimate.v.totalestimateConfirmed']
                            //'in' => ['$toDouble' => '$$estimate.v.price']
                        ]
                    ]
                ]
            ]],
            ['$project' => [
                'poste' => 1,
                'price' => 1,
                'validatedEstimates' => 1,
                'validatedTotal' => 1,
                'payments' => [
                    '$map' => [
                        'input' => [
                            '$filter' => [
                                'input' => '$financer',
                                'as' => 'finance',
                                'cond' => ['$eq' => ['$$finance.id', $financeurIdFilter]]  // Filtrer par ID de financeur
                            ]
                        ],
                        'as' => 'finance',
                        'in' => [
                            'financeur' => '$$finance.id',
                            'amount' => '$$finance.amount',
                            'owedToCandidates' => [
                                '$map' => [
                                    'input' => '$validatedEstimates',
                                    'as' => 'estimate',
                                    'in' => [
                                        'candidateId' => '$$estimate.k',
                                        'candidateName' => '$$estimate.v.name',
                                        'owedAmount' => [
                                            '$multiply' => [
                                                ['$cond' => [
                                                    'if' => ['$eq' => ['$validatedTotal', 0]],
                                                    'then' => 0,
                                                    'else' => [
                                                        '$divide' => [
                                                            ['$toDouble' => '$$finance.amount'],
                                                            ['$toDouble' => '$validatedTotal']
                                                        ]
                                                    ]
                                                ]],
                                                ['$toDouble' => '$$estimate.v.totalestimateConfirmed']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]]
        ]);*/

        $result = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            // Étape 1 : Filtrer les documents ayant form = $form
            ['$match' => ['form' => $form]],

            // Étape 2 : Décomposer les dépenses
            ['$unwind' => '$answers.aapStep1.depense'],

            // Étape 3 : Filtrer les financeurs par leur ID
            ['$addFields' => [
                'filteredFinancer' => [
                    '$filter' => [
                        'input' => '$answers.aapStep1.depense.financer',
                        'as' => 'finance',
                        'cond' => ['$eq' => ['$$finance.id', $financeurId]]
                    ]
                ]
            ]],

            // Étape 4 : Filtrer les estimates par candidat ID
            ['$addFields' => [
                'filteredEstimates' => [
                    '$filter' => [
                        'input' => ['$objectToArray' => '$answers.aapStep1.depense.estimates'],
                'as' => 'estimate',
                'cond' => ['$eq' => ['$$estimate.k', $candidatId]]
            ]
        ]
    ]],

    // Étape 5 : Calculer le montant total validé pour les candidats
    ['$addFields' => [
        'validatedTotal' => [
            '$sum' => [
                '$map' => [
                    'input' => '$filteredEstimates',
                    'as' => 'estimate',
                    'in' => [
                        '$sum' => [
                            '$map' => [
                                'input' => '$$estimate.v.AssignBudgetArray',
                                'as' => 'budget',
                                'in' => [
                                    '$cond' => [
                                        ['$eq' => ['$$budget.check', true]],
                                        ['$toDouble' => '$$budget.price'],
                                        0
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]],

    // Étape 6 : Calculer le montant total financé par les financeurs filtrés
    ['$addFields' => [
        'totalFinanced' => [
            '$sum' => [
                '$map' => [
                    'input' => '$filteredFinancer',
                    'as' => 'finance',
                    'in' => ['$toDouble' => '$$finance.amount']
                ]
            ]
        ]
    ]],

    // Étape 7 : Calculer les montants dus pour chaque financeur
    ['$addFields' => [
        'financeurPayments' => [
            '$map' => [
                'input' => '$filteredFinancer',
                'as' => 'finance',
                'in' => [
                    'financeurId' => '$$finance.id',
                    'name' => '$$finance.name',
                    'amountOwed' => [
                        '$cond' => [
                            // Si le montant total validé <= total financé
                            ['$lte' => ['$validatedTotal', '$totalFinanced']],
                            [
                                '$min' => [
                                '$$finance.amount',
                                    [
                                        '$multiply' => [
                                        ['$divide' => ['$validatedTotal', '$totalFinanced']],
                                        '$$finance.amount'
                                    ]
                                    ]
                                ]
                            ],
                            // Sinon, répartir proportionnellement
                            [
                                '$min' => [
                                '$$finance.amount',
                                    [
                                        '$multiply' => [
                                        ['$divide' => ['$validatedTotal', '$totalFinanced']],
                                        '$$finance.amount'
                                    ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]],

    // Étape 8 : Structurer les résultats
    ['$project' => [
        '_id' => 0,
        'poste' => '$answers.aapStep1.depense.poste',
        'validatedTotal' => 1,
        'totalFinanced' => 1,
        'financeurPayments' => 1,
        'candidates' => [
            '$map' => [
                'input' => '$filteredEstimates',
                'as' => 'estimate',
                'in' => [
                    'title' => '$answers.aapStep1.titre',
                    'poste' => '$answers.aapStep1.depense.poste',
                    'name' => '$$estimate.v.name',
                    'validatedAmount' => [
                        '$sum' => [
                            '$map' => [
                                'input' => '$$estimate.v.AssignBudgetArray',
                                'as' => 'budget',
                                'in' => [
                                    '$cond' => [
                                        ['$eq' => ['$$budget.check', true]],
                                        ['$toDouble' => '$$budget.price'],
                                        0
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]]
]);

        return $result["result"];
    }

    public static function funding_by_financer($form){

        $result = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            // Étape 1 : Filtrer les documents ayant form = $form
            ['$match' => ['form' => $form]],

            // Étape 2 : Décomposer les dépenses
            ['$unwind' => '$answers.aapStep1.depense'],

            // Étape 3 : Décomposer les `AssignBudgetArray` pour chaque estimation
            ['$unwind' => [
                'path' => '$answers.aapStep1.depense.estimates',
                'preserveNullAndEmptyArrays' => true
            ]],
            ['$unwind' => [
                'path' => '$answers.aapStep1.depense.estimates.v.AssignBudgetArray',
                'preserveNullAndEmptyArrays' => true
            ]],

            // Étape 4 : Décomposer les financeurs
            ['$unwind' => '$answers.aapStep1.depense.financer'],

            // Étape 5 : Calculer les montants financés, payés, et en attente par financeur
            ['$group' => [
                '_id' => '$answers.aapStep1.depense.financer.id', // Groupement par ID du financeur
                'name' => ['$first' => '$answers.aapStep1.depense.financer.name'],
                'totalFinanced' => ['$sum' => '$answers.aapStep1.depense.financer.amount'], // Somme totale financée
                'totalPaid' => [
                    '$sum' => [
                        '$cond' => [
                            ['$and' => [
                                ['$eq' => ['$answers.aapStep1.depense.estimates.v.AssignBudgetArray.payed', true]],
                                ['$eq' => ['$answers.aapStep1.depense.estimates.v.AssignBudgetArray.financer', '$answers.aapStep1.depense.financer.id']]
                            ]],
                            '$answers.aapStep1.depense.estimates.v.AssignBudgetArray.price', // Montant payé
                            0
                        ]
                    ]
                ],
                'totalPending' => [
                    '$sum' => [
                        '$cond' => [
                            ['$and' => [
                                ['$eq' => ['$AssignBudgetArrayArray.v.check', true]], // `check: true`
                                ['$or' => [
                                    ['$eq' => ['$AssignBudgetArrayArray.v.payed', false]], // Non payé explicitement
                                    ['$not' => ['$AssignBudgetArrayArray.v.payed']] // `payed` absent
                                ]]
                            ]],
                            '$answers.aapStep1.depense.estimates.v.AssignBudgetArray.price', // Montant en attente
                            0
                        ]
                    ]
                ]
            ]],

            // Étape 6 : Structurer les résultats
            ['$project' => [
                '_id' => 0,
                'financeurId' => '$_id',
                'name' => 1,
                'totalFinanced' => 1,
                'totalPaid' => 1,
                'totalPending' => 1
            ]]
        ]);

        return $result["result"];
    }

    public static function getFinNR($fundings){
        $return = [];
        foreach ($fundings as $idfund => $fund){
            if(isset($fund["financeurId"])){
                $fin = Organization::getById($fund["financeurId"]);
                if(empty($fin)){
                    $fin = Person::getById($fund["financeurId"]);
                }
                if(!empty($fin)){
                    $return[] = $fin;
                }
            }
        }
        return $return;
    }
	public static function camp_can_open_cart($form) {
		$db_form = PHDB::findOneById(Form::COLLECTION, $form, ["config", "parent"]);
		if (!empty($db_form["config"])) {
			$key = array_keys($db_form["parent"]);
			$key = end($key);
			$costum = CacheHelper::getCostum($key, $db_form["parent"][$key]["type"]);
			$db_form = Form::getConfig($form);
			$db_campagne = !empty($db_form["campagne"][$costum["campagne"]]) ? $db_form["campagne"][$costum["campagne"]] : [];
			$today = time();
			if (!empty($db_campagne["panierdate"]) && method_exists($db_campagne["panierdate"], "toDateTime")) {
				$cart_time = $db_campagne["panierdate"]->toDateTime()->getTimestamp();
				if ($cart_time > $today)
					return (0);
			}
		}
		return (1);
	}

    public static function filterdepense($answer,$form,$getArchived=false) {
        $request = [];
        if(!empty($answer)){
            $request["_id"] = new MongoId($answer);
        }elseif(!empty($form)){
            $request["form"] = $form;
        }

        $condition = [
            '$ne' => [
                '$$item.include',
                false
            ]
        ];

        if($getArchived){
            $condition = [
                '$eq' => [
                    '$$item.include',
                    false
                ]
            ];
        }

        $project = [
            "collection" => 1,
            "context" => 1,
            "created" => 1,
            "cterSlug" => 1,
            "form" => 1,
            "formId" => 1,
            "formList" => 1,
            "links" => 1,
            "source" => 1,
            "user" => 1,
            "vote" => 1,
			"project" => 1,
			"status" => 1,
			
            'answers.aapStep2' => 1,
            'answers.aapStep3' => 1,
			'answers.aapStep1.aapStep1m1esnlbwekc71uotpf4' => 1,
			'answers.aapStep1.aapStep1lzi62x3etw49gyc424d' => 1,
			'answers.aapStep1.aapStep1m03ot9qymmfgashp7l' => 1,
			'answers.aapStep1.aapStep1m0w3thzyk9109to9f1' => 1,
			'answers.aapStep1.aapStep1lzi6plod1j27tg99qa2' => 1,
			'answers.aapStep1.aapStep1lzi6qmw1y98vifhvcjl' => 1,
			'tags' => 1,
			'answers.aapStep1.aapStep1lusoklfzokkn4svl1ei' => 1,
			'answers.aapStep1.aapStep1m0w49vpl5jm001xwvsv' => 1,
			'answers.aapStep1.aapStep1lqb428ajrbhwdbmg6qi' => 1,
			'answers.aapStep1.aapStep1m06fzlu743blirhkqsn' => 1,
			'answers.aapStep1.aapStep1lpvipvzp13vf2jypqgxq' => 1,
			'answers.aapStep1.aapStep1lpvip7f9pa6et762ysa' => 1,
			'answers.aapStep1.aapStep1lpvioouzejcnjy7ffw' => 1,
			'answers.aapStep1.aapStep1lpvinn7ld70wbk7w339' => 1,

            /*'answers.aapStep1.depense' => [
                '$filter' => [
                    'input' => '$answers.aapStep1.depense',
                    'as' => 'item',
                    'cond' => [
                        '$ne' => [
                            '$$item.include',
                            false
                        ]
                    ]
                ]
            ]*/
            'answers.aapStep1.depense' => [
                '$map' => [
                    'input' => [
                        '$filter' => [
                            'input' => '$answers.aapStep1.depense',
                            'as' => 'item',
                            'cond' => $condition
                        ]
                    ],
                    'as' => 'filteredItem',
                    'in' => [
                        /*'item' => '$$filteredItem',
                        'depenseindex' => [
                            '$indexOfArray' => ['$answers.aapStep1.depense','$$filteredItem']
                        ]*/
                        '$mergeObjects' => [
                            '$$filteredItem', [
                                'depenseindex' => [
                                    '$indexOfArray' => ['$answers.aapStep1.depense','$$filteredItem']
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $parentForm = PHDB::findOneById(Form::COLLECTION, $form);
        $config = isset($parentForm["config"]) ? $parentForm["config"] : null;

        $inputlist1 = PHDB::findOne(Form::INPUTS_COLLECTION, ["formParent" => $form, "step" => "aapStep1"]);
        $inputlist2 = PHDB::findOne(Form::INPUTS_COLLECTION, ["formParent" => $config, "step" => "aapStep1"]);

        $inputlist = array_merge($inputlist1,$inputlist2);
        if(!empty($inputlist['inputs'])){
            foreach ($inputlist["inputs"] as $idinput => $input){
                if($idinput != "depense") {
                    $project['answers.aapStep1.'.$idinput] = 1;
                }
            }
        }

        $depensesList = PHDB::aggregate(Form::ANSWER_COLLECTION, [
            [
                '$match' => $request
            ],
            [
                '$project' => $project
            ]
        ]);

        if(!empty($answer)){
            $depensesList = $depensesList["result"][array_key_first($depensesList["result"])];
        }elseif(!empty($form)){
            $depensesList = $depensesList["result"];
        }

        return $depensesList;
    }

    public static function getcoremudata($answer){
        $depensesList = [
            "answer" => null,
            "actions" => null,
            "form" => null,
            "context" => null,
        ];
        $depensesList["answer"] = PHDB::findOneById(Form::ANSWER_COLLECTION ,$answer );
        $depensesList["form"] = PHDB::findOneById(Form::COLLECTION ,$depensesList["answer"]["form"] );
        $parentid = array_key_first($depensesList["form"]["parent"]);
        $depensesList["links"] =  Element::getCommunityByTypeAndId($depensesList["form"]["parent"][$parentid]["type"], $parentid);
        $communityMembers = Person::getByArrayId(array_keys($depensesList["links"]));

        if(isset($depensesList["answer"]["project"]["id"])){
            $depensesList["answer"]["project"]["data"] = PHDB::findOneById(Project::COLLECTION , $depensesList["answer"]["project"]["id"] );
            $depensesList["answer"]["project"]["action"] = PHDB::find( Action::COLLECTION , ["parentId" => $depensesList["answer"]["project"]["id"]] );
            foreach ($depensesList["answer"]["project"]["action"] as $action){
                if(isset($action["links"]['contributors'])){
                    foreach ($action["links"]['contributors'] as $key => &$contributor) {
                        if (isset($communityMembers[$key])) {
                            $action["links"]['contributors'][$key]["name"] = $communityMembers[$key]["name"];
                            $action["links"]['contributors'][$key]["profilThumbImageUrl"] = $communityMembers[$key]["profilThumbImageUrl"] ?? "/images/news/profile_default_l.png";
                        } else {
                            $action["links"]['contributors'][$key] = $communityMembers[$key] = PHDB::findOneById(Person::COLLECTION, $key);
                        }
                    }
                }
                $depensesList["actions"][$action["milestone"]["milestoneId"]][] = $action;
            }
        }

        return $depensesList;
    }

    public static function generateMilestones($answerId, $projectId , $depenseKey){
        $project = PHDB::findOneById(Project::COLLECTION , $projectId);
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION , $answerId);

        $bytes = random_bytes(ceil(17 * 3 / 4)); // Ajuste la longueur pour base64
        $key = substr(base64_encode($bytes), 0, 17); // Tronque à la longueur souhaitée
        $key = str_replace(['+', '/', '='], '', $key); // Retire les caractères non souhaités
        $name = isset($answer["answers"]["aapStep1"]["depense"][$depenseKey]["poste"]) ? $answer["answers"]["aapStep1"]["depense"][$depenseKey]["poste"] : "vide";
        $description = isset($answer["answers"]["aapStep1"]["depense"][$depenseKey]["description"]) ? $answer["answers"]["aapStep1"]["depense"][$depenseKey]["description"] : "vide";

        $foundMil = false;
        $allMil = [];

        if(isset($project["oceco"]["milestones"])){
            $allMil = $project["oceco"]["milestones"];
            foreach ($project["oceco"]["milestones"] as $miles) {
                if($miles["name"] == $name){
                    $foundMil = true;
                    $milestone = $miles;
                }
            }
        }

        if(!$foundMil){
            $milestone = [
                "milestoneId" => $key,
                "name" => $name,
                "description" => $description,
                "status" => "open"
            ];

            array_push($allMil, $milestone);

            PHDB::update(
                Project::COLLECTION,
                array("_id" => new MongoId($projectId)),
                array(
                    '$set'      => ["oceco.milestones" => $allMil]
                )
            );
        }

        return PHDB::update(
            Form::ANSWER_COLLECTION,
            array("_id" => new MongoId($answerId)),
            array(
                '$set'  => ["answers.aapStep1.depense.".$depenseKey.".milestone" => $milestone["milestoneId"]]
            )
        );

        return $milestone;
    }
}
