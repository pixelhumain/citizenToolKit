<?php
    
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\Application;
    
    class Convert {
        
        public static function getPrimaryParam($map) {
            
            $param = array();
            $param['typeElement'] = Event::COLLECTION;
            
            foreach ($map as $key => $value) {
                $p = array(
                    "valueAttributeElt" => $value,
                    "idHeadCSV"         => $key,
                );
                $param['infoCreateData'][] = $p;
                //$param['infoCreateData'][$key]["idHeadCSV"] = $key;
            }
            
            $param['typeFile'] = 'json';
            $param['warnings'] = false;
            
            return $param;
        }
        
        public static function getCorrectUrlForOdsAndDatanova() {
            
            $url_final = "";
            $url_ods_head = "";
            $url_ods_params = "";
            
            foreach ($_GET as $key => $value) {
                if ($key == "url") {
                    $url_ods_head = $value;
                } else {
                    if (is_array($value)) {
                        foreach ($value as $key2 => $value2) {
                            $url_ods_params .= "&" . $key . "=" . $value2;
                        }
                    } else {
                        $url_ods_params .= "&" . $key . "=" . $value;
                    }
                }
                $url_final = $url_ods_head . $url_ods_params;
            }
            
            $pos = strpos($url_ods_head, "?");
            
            $url_ods_head_final = substr($url_ods_head, 0, $pos) . "?";
            $url_param_dataset = substr($url_ods_head, ($pos + 1));
            
            $url_ods_params = $url_param_dataset . $url_ods_params;
            
            $url_ods_params = str_replace("@", "%40", $url_ods_params);
            
            $url_complete = $url_ods_head_final . $url_ods_params;
            
            return $url_complete;
        }
        
        public static function convertOdsToPh($url) {
            
            $map = TranslateOdsToPh::$mapping_activity;
            
            $url_complete = self::getCorrectUrlForOdsAndDatanova();
            
            $url_complete = str_replace("_", ".", $url_complete);
            
            $param = self::getPrimaryParam($map);
            
            $param['key'] = 'convert_ods';
            $param['nameFile'] = 'convert_ods';
            $param['pathObject'] = 'records';
            
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url_complete);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            $return = curl_exec($ch);
            curl_close($ch);
            
            if (isset($url)) {
                $param['file'][0] = $return;
            }
            
            $result = Import::previewData($param);
            
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function convertEducMembreToPh($url) {
            $map = TranslateEducMembreToPh::$mapping;
            
            $url_complete = self::getCorrectUrlForOdsAndDatanova();
            $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);
            
            $param = self::getPrimaryParam($map);
            
            $param['key'] = 'convert_educ_membre';
            $param['nameFile'] = 'convert_educ_membre';
            $param['pathObject'] = 'records';
            
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url_complete);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            $return = curl_exec($ch);
            curl_close($ch);
            
            if (isset($url)) {
                $param['file'][0] = $return;
            }
            
            $result = Import::previewData($param);
            
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function convertEducEcoleToPh($url) {
            
            $map = TranslateEducEcoleToPh::$mapping;
            
            $url_complete = self::getCorrectUrlForOdsAndDatanova();
            $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);
            
            $param = self::getPrimaryParam($map);
            
            $param['key'] = 'convert_educ_ecole';
            $param['nameFile'] = 'convert_educ_ecole';
            $param['pathObject'] = 'records';
            
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url_complete);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            $return = curl_exec($ch);
            curl_close($ch);
            
            if (isset($url)) {
                $param['file'][0] = $return;
            }
            
            $result = Import::previewData($param);
            
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function convertEducEtabToPh($url) {
            
            $map = TranslateEducEtabToPh::$mapping;
            $url_complete = self::getCorrectUrlForOdsAndDatanova();
            $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);
            
            // var_dump($url_complete);
            
            $param = self::getPrimaryParam($map);
            
            $param['key'] = 'convert_educ_etab';
            $param['nameFile'] = 'convert_educ_etab';
            $param['pathObject'] = 'records';
            
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url_complete);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            $return = curl_exec($ch);
            curl_close($ch);
            
            if (isset($url)) {
                $param['file'][0] = $return;
            }
            
            $result = Import::previewData($param);
            
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function convertEducStructToPh($url) {
            $map = TranslateEducStructToPh::$mapping;
            
            $url_complete = self::getCorrectUrlForOdsAndDatanova();
            $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);
            
            $param = self::getPrimaryParam($map);
            
            $param['key'] = 'convert_educ_struct';
            $param['nameFile'] = 'convert_educ_struct';
            $param['pathObject'] = 'records';
            
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url_complete);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            $return = curl_exec($ch);
            curl_close($ch);
            
            if (isset($url)) {
                $param['file'][0] = $return;
            }
            
            $result = Import::previewData($param);
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function convertDatanovaToPh($url) {
            
            $map = TranslateDatanovaToPh::$mapping_activity;
            
            $url_complete = self::getCorrectUrlForOdsAndDatanova();
            
            $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);
            
            $param = self::getPrimaryParam($map);
            
            $param['key'] = 'convert_datanova';
            $param['nameFile'] = 'convert_datanova';
            $param['pathObject'] = 'records';
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url_complete);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            $return = curl_exec($ch);
            curl_close($ch);
            
            if (isset($url)) {
                $param['file'][0] = $return;
            }
            
            $result = Import::previewData($param);
            
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function convertOsmToPh($url, $httpOptions = null) {
            
            $map = TranslateOsmToPh::$mapping_element;
            
            $param = self::getPrimaryParam($map);
            
            $param['pathObject'] = 'elements';
            $param['nameFile'] = 'convert_osm';
            $param['key'] = 'convert_osm';
            
            $pos = strpos($url, "=");
            
            $url_head = substr($url, 0, ($pos + 1));
            $url_param = substr($url, ($pos + 1));
            
            $url_osm = $url_head . urlencode($url_param);
            
            if (isset($url_osm)) {
                if($httpOptions == null)
                    $streamContext = stream_context_create($httpOptions);
                
                $param['file'][0] =  (isset($streamContext)) ? file_get_contents($url_osm, false, $streamContext)  : file_get_contents($url_osm); 
                if(!$param['file'][0])
                    $param['file'][0] =  (isset($streamContext)) ? file_get_contents($url_head . $url_param, false, $streamContext)  : file_get_contents($url_head . $url_param); 
            }
            
            $result = Import::previewData($param);
            if ($result['result'] !== false) {
                $res = [];
                //if(isset($result["elements"]) && count(json_decode($result["elements"])) > 0)
                   $res = json_decode($result['elements']);
                //else if(isset($result["elementsWarnings"]) && count(json_decode($result["elementsWarnings"])) > 0)
                    //$res = json_decode($result['elementsWarnings']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function convertDatagouvToPh($url) {
            
            $all_data = array();
            
            $map = TranslateDatagouvToPh::$mapping_datasets;
            $param = self::getPrimaryParam($map);
            
            $param['typeFile'] = 'json';
            $param['key'] = 'convert_datagouv';
            $param['nameFile'] = 'convert_datagouv';
            $list_dataset = json_decode(file_get_contents($url), true);
            
            foreach ($list_dataset as $key => $value) {
                
                $url_orga = "https://www.data.gouv.fr/api/1/datasets/" . $value['id'] . "/";
                $data_orga = json_decode((file_get_contents($url_orga)), true);
                
                array_push($all_data, $data_orga);
            }
            
            $param['file'][0] = json_encode($all_data);
            
            $result = Import::previewData($param);
            
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }

        public static function fetchDataWithCurl($url) {
            set_time_limit(50);
            $ch = curl_init($url);
        
            // Configuration de cURL
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, "Communoter");
        
            $data = curl_exec($ch);
        
            if ($data === false) {
                echo 'Erreur cURL : ' . curl_error($ch);
            }
    
            curl_close($ch);
        
            return $data;
        }

        public static function convertWikiToPathEtab($title) {
            $etablissements = array();
            $endpointUrl = 'http://dbpedia.org/sparql';

            $sparqlQuery = "
            SELECT DISTINCT ?school ?schoolLabel ?type ?thumbnail ?description
                WHERE {
                    ?school rdf:type ?type.
                    ?school dbo:location <http://dbpedia.org/resource/".$title.">.
                    ?school rdfs:label ?schoolLabel.
                    OPTIONAL { ?school dbo:thumbnail ?thumbnail. }
                    OPTIONAL { ?school dbo:abstract ?description. FILTER(LANG(?description) = 'fr') }
                    FILTER (
                        ?type = dbo:PrimarySchool ||
                        ?type = dbo:SecondarySchool ||
                        ?type = dbo:HighSchool ||
                        ?type = dbo:University
                    )
                } 
            ";
            $queryParams = [
                'query' => $sparqlQuery,
                'format' => 'application/json',
            ];
            $url = $endpointUrl . '?' . http_build_query($queryParams);
            $response = file_get_contents($url);
            $dbpedia_data = json_decode($response, true);

            if($dbpedia_data['results']['bindings'] != null){
                foreach($dbpedia_data['results']['bindings'] as $key => $value) {
                    $objet = array(
                        "links" => isset($value["school"]["value"]) ? $value["school"]["value"] : "",
                        "name" => isset($value["schoolLabel"]["value"]) ? $value["schoolLabel"]["value"] : "",
                        "type" => isset($value["type"]["value"]) ? $value["type"]["value"] : "",
                        "urlImg" => isset($value["thumbnail"]["value"]) ? $value["thumbnail"]["value"] : "",
                        "description" => isset($value["description"]["value"]) ? $value["description"]["value"] : ""
                    );

                    if(!in_array($objet, $etablissements))
                    {
                        array_push($etablissements, $objet);
                    }
                }
            }

            return $etablissements;
        }

        public static function sparqlQuery($query) {
            $url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=' . urlencode($query) . '&format=json';
            $response = file_get_contents($url);
            return json_decode($response, true);
        }

        public static function convertWikiToPath($url, $type, $title, $wikidataID, $text_filter){

            $all_data = array();

            if($type == "elements"){
                $sparqlDetail = "https://query.wikidata.org/sparql?format=json&query=SELECT%20DISTINCT%20?item%20?itemLabel%20?itemDescription%20?coor%20?range%20?image%20WHERE%20{?item%20wdt:P131%20wd:".$wikidataID.".?item%20?range%20wd:".$wikidataID.".?item%20wdt:P625%20?coor.?item%20wdt:P18%20?image.SERVICE%20wikibase:label%20{%20bd:serviceParam%20wikibase:language%20%27fr%27.%20}}";
                $repons_wiki = self::fetchDataWithCurl($sparqlDetail);
                $wikidata_article = json_decode($repons_wiki, true);

                foreach ($wikidata_article['results']['bindings'] as $key => $value) {
                
                    if ($text_filter !== null) {
                        
                        if (stristr($value['itemLabel']['value'], $text_filter) == true) {
                            
                            if (@$value['coor']) {
                                $coor = self::getLatLongWikidataItem($value);
                                $objet = array(
                                    "label" => $value['itemLabel']['value'],
                                    "latitude" => $coor["latitude"],
                                    "longitude" => $coor["longitude"],
                                    "item" => $value['item']['value'],
                                    "description" => @$value['itemDescription']['value'],
                                    "urlimage" => @$value['image']["value"]
                                );
                                if(!in_array($objet, $all_data))
                                {
                                    array_push($all_data, $objet);
                                }
                            }
                        }
                    } else {
                        
                        if (@$value['coor']) {
                            $coor = self::getLatLongWikidataItem($value);
                            $objet = array(
                                "label" => $value['itemLabel']['value'],
                                "latitude" => $coor["latitude"],
                                "longitude" => $coor["longitude"],
                                "item" => $value['item']['value'],
                                "description" => @$value['itemDescription']['value'],
                                "urlimage" => @$value['image']["value"]
                            );
                            if(!in_array($objet, $all_data))
                            {
                                array_push($all_data, $objet);
                            }
                        }
                    }
                }
            } else if($type == "abstract") {
                
                $objet = array();
                $apiUrl = "https://www.wikidata.org/w/api.php";
                $params = [
                    'action' => 'wbgetentities',
                    'ids' => $wikidataID,
                    'format' => 'json',
                    'props' => 'claims|descriptions|sitelinks'
                ];
                
                $url = $apiUrl . '?' . http_build_query($params);
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                
                $response = curl_exec($curl);
                
                if (curl_errno($curl)) {
                    echo json_encode(['error' => 'Erreur cURL : ' . curl_error($curl)]);
                    http_response_code(500);
                    exit();
                }
                
                curl_close($curl);
                
                $data = json_decode($response, true);
                
                if (isset($data['entities'][$wikidataID])) {
                    $entity = $data['entities'][$wikidataID];
                    $description = isset($entity['descriptions']['fr']['value']) ? (string)$entity['descriptions']['fr']['value'] : null;
                
                    // Récupération de l'image
                    $image = null;
                    if (isset($entity['claims']['P18'])) {
                        $imageFileName = $entity['claims']['P18'][0]['mainsnak']['datavalue']['value'];
                        $image = 'https://commons.wikimedia.org/wiki/Special:FilePath/' . urlencode($imageFileName);
                    }
                
                    // Récupération des coordonnées
                    $latitude = null;
                    $longitude = null;
                    if (isset($entity['claims']['P625'])) {
                        $coordinatesData = $entity['claims']['P625'][0]['mainsnak']['datavalue']['value'];
                        if (is_array($coordinatesData) && isset($coordinatesData['latitude'], $coordinatesData['longitude'])) {
                            $latitude = (string)$coordinatesData['latitude'];
                            $longitude = (string)$coordinatesData['longitude'];
                        }
                    }
                
                    // Récupération de la surface
                    $surface = null;
                    if (isset($entity['claims']['P2046'])) {
                        $surfaceValue = $entity['claims']['P2046'][0]['mainsnak']['datavalue']['value'];
                        if (isset($surfaceValue['amount'])) {
                            $surface = $surfaceValue['amount'];
                        }
                    }
                
                    // Récupération de la population
                    $population = null;
                    if (isset($entity['claims']['P1082'])) {
                        $populationValue = $entity['claims']['P1082'][0]['mainsnak']['datavalue']['value'];
                        $population = number_format($populationValue);
                    }
                
                    // Récupération du pays
                    // $country = null;
                    // if (isset($entity['claims']['P17'])) {
                    //     $countryData = $entity['claims']['P17'][0]['mainsnak']['datavalue']['value']['id'];
                    //     $countryUrl = $apiUrl . '?' . http_build_query([
                    //         'action' => 'wbgetentities',
                    //         'ids' => $countryData,
                    //         'format' => 'json',
                    //         'props' => 'labels'
                    //     ]);
                        
                    //     $curl = curl_init();
                    //     curl_setopt($curl, CURLOPT_URL, $countryUrl);
                    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        
                    //     $countryResponse = curl_exec($curl);
                    //     if (!curl_errno($curl)) {
                    //         $countryData = json_decode($countryResponse, true);
                    //         if (isset($countryData['entities'][$countryData]['labels']['fr']['value'])) {
                    //             $country = $countryData['entities'][$countryData]['labels']['fr']['value'];
                    //         }
                    //     }
                        
                    //     curl_close($curl);
                    // }

                    $objet['apiData'] = [
                        'description' => mb_strtoupper(mb_substr($description, 0, 1)) . mb_substr($description, 1),
                        'image' => $image,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'surface' => $surface,
                        'population' => $population,
                        // 'country' => $country
                    ];
                }

                $endpointUrl = 'http://dbpedia.org/sparql';

                $sparqlQuery = "
                SELECT ?description ?image
                    WHERE {
                        <http://dbpedia.org/resource/" . urlencode($title) . "> dbo:abstract ?description.
                        <http://dbpedia.org/resource/" . urlencode($title) . "> dbo:thumbnail ?image.    
                        FILTER (LANG(?description) = 'fr')
                    }
                ";
                $queryParams = [
                    'query' => $sparqlQuery,
                    'format' => 'application/json',
                ];
                $url = $endpointUrl . '?' . http_build_query($queryParams);
                $response = file_get_contents($url);
                $dbpedia_data = json_decode($response, true);

                $querywikidata = "https://query.wikidata.org/sparql?format=json&query=SELECT%20?population%20?area%20?countryLabel%20?coor%20WHERE%20{%20wd:".$wikidataID."%20wdt:P1082%20?population;%20wdt:P2046%20?area;%20wdt:P17%20?country;%20wdt:P625%20?coor.%20SERVICE%20wikibase:label%20{%20bd:serviceParam%20wikibase:language%20'[AUTO_LANGUAGE],fr'.%20}%20}";
                $repons_wiki = self::fetchDataWithCurl($querywikidata);
                $wikidata_article = json_decode($repons_wiki, true);

                if($dbpedia_data["results"]["bindings"] != null) {
                    $val = $dbpedia_data['results']['bindings'][0];
                    $objet["descriptions"] = $val["description"]["value"];
                    $objet["urlimage"] = $val["image"]["value"];
                }

                if($wikidata_article["results"]["bindings"] != null) {
                    $wiki = $wikidata_article['results']['bindings'][0];
                    $coor = self::getLatLongWikidataItem($wiki);
                    
                    $objet["population"] = isset($wiki["population"]["value"]) ? $wiki["population"]["value"]: "";
                    $objet["area"] = isset($wiki["area"]["value"]) ? $wiki["area"]["value"] : "";
                    $objet["country"] = isset($wiki["countryLabel"]["value"]) ? $wiki["countryLabel"]["value"] : "";
                    $objet["latitude"] = isset($coor["latitude"]) ? $coor["latitude"] : "";
                    $objet["longitude"] = isset($coor["longitude"]) ? $coor["longitude"] : "";
                }

                $all_data = $objet;
                
            } else if($type == "personnes") {

                // $endpointUrl = 'http://dbpedia.org/sparql';

                // $sparqlQuery = "
                // SELECT DISTINCT ?person ?personLabel ?thumbnail ?description ?birthdate ?deathdate ?birthplaceLabel ?deathplaceLabel ?occupationLabel ?genderLabel
                //     WHERE {
                //         ?person dbo:birthPlace <http://dbpedia.org/resource/" . urlencode($title) . ">.
                //         OPTIONAL { ?person foaf:name ?personLabel. }
                //         OPTIONAL { ?person dbo:thumbnail ?thumbnail. }
                //         OPTIONAL { ?person dbo:abstract ?description. FILTER(LANG(?description) = 'fr') }
                //         OPTIONAL { ?person dbo:birthDate ?birthdate. }
                //         OPTIONAL { ?person dbo:deathDate ?deathdate. }
                //         OPTIONAL { ?person dbo:birthPlace ?birthplaceLabel. }
                //         OPTIONAL { ?person dbo:deathPlace ?deathplaceLabel. }
                //         OPTIONAL { ?person dbo:occupation ?occupationLabel. }
                //         OPTIONAL { ?person foaf:gender ?genderLabel. }
                //     }
                // ";
                // $queryParams = [
                //     'query' => $sparqlQuery,
                //     'format' => 'application/json',
                // ];

                // $url = $endpointUrl . '?' . http_build_query($queryParams); 
                // $context = stream_context_create(['http' => ['timeout' => 40]]);
                // $response = file_get_contents($url, false, $context);

                $queryEvents = "https://query.wikidata.org/sparql?format=json&query=SELECT%20DISTINCT%20?person%20?personLabel%20?thumbnail%20?description%20?birthdate%20?deathdate%20?birthplaceLabel%20?deathplaceLabel%20?occupationLabel%20?genderLabel%20?spouseLabel%20WHERE%20{%20?person%20wdt:P19%20wd:".$wikidataID.";%20OPTIONAL%20{%20?person%20rdfs:label%20?personLabel.%20FILTER(LANG(?personLabel)%20=%20%27fr%27)%20}%20OPTIONAL%20{%20?person%20wdt:P18%20?thumbnail.%20}%20OPTIONAL%20{%20?person%20wdt:P569%20?birthdate.%20}%20OPTIONAL%20{%20?person%20wdt:P570%20?deathdate.%20}%20OPTIONAL%20{%20?person%20wdt:P19%20?birthplace.%20?birthplace%20rdfs:label%20?birthplaceLabel.%20FILTER(LANG(?birthplaceLabel)%20=%20%27fr%27)%20}%20OPTIONAL%20{%20?person%20wdt:P20%20?deathplace.%20?deathplace%20rdfs:label%20?deathplaceLabel.%20FILTER(LANG(?deathplaceLabel)%20=%20%27fr%27)%20}%20OPTIONAL%20{%20?person%20wdt:P106%20?occupation.%20?occupation%20rdfs:label%20?occupationLabel.%20FILTER(LANG(?occupationLabel)%20=%20%27fr%27)%20}%20OPTIONAL%20{%20?person%20wdt:P21%20?gender.%20?gender%20rdfs:label%20?genderLabel.%20FILTER(LANG(?genderLabel)%20=%20%27fr%27)%20}%20OPTIONAL%20{%20?person%20wdt:P241%20?description.%20FILTER(LANG(?description)%20=%20%27fr%27)%20}%20OPTIONAL%20{%20?person%20wdt:P26%20?spouse.%20?spouse%20rdfs:label%20?spouseLabel.%20FILTER(LANG(?spouseLabel)%20=%20%27fr%27)%20}%20}%20LIMIT%204261";
                $response = self::fetchDataWithCurl($queryEvents);
                $dbpedia_data = json_decode($response, true);

                if($dbpedia_data['results']['bindings'] != null) {
                    foreach ($dbpedia_data['results']['bindings'] as $key => $value) {
                        $objet = array(
                            "links" => isset($value["person"]["value"]) ? $value["person"]["value"] : "",
                            "name" => isset($value["personLabel"]["value"]) ? $value["personLabel"]["value"] : "",
                            "urlImg" => isset($value["thumbnail"]["value"]) ? $value["thumbnail"]["value"] : "",
                            "description" => isset($value["description"]["value"]) ? $value["description"]["value"] : "",
                            "datedebut" => isset($value["birthdate"]["value"]) ? $value["birthdate"]["value"] : "",
                            "datefin" => isset($value["deathdate"]["value"]) ? $value["deathdate"]["value"] : "",
                            "lieudebut" => isset($value["birthplaceLabel"]["value"]) ? $value["birthplaceLabel"]["value"] : "",
                            "lieufin" => isset($value["deathplaceLabel"]["value"]) ? $value["deathplaceLabel"]["value"] : "",
                            "occupation" => isset($value["occupationLabel"]["value"]) ? $value["occupationLabel"]["value"] : "",
                            "genre" => isset($value["genderLabel"]["value"]) ? $value["genderLabel"]["value"] : "",
                            "compagnon" => isset($value["spouseLabel"]["value"]) ? $value["spouseLabel"]["value"] : ""
                        );

                        if(!in_array($objet, $all_data))
                        {
                            array_push($all_data, $objet);
                        }
                    }
                }
            } else if($type == 'orga') {

                $sparqlQueryDbpedia = "
                
                        SELECT DISTINCT ?organization ?organizationLabel ?thumbnail ?description
                        WHERE {
                            ?organization dbo:location <http://dbpedia.org/resource/".urlencode($title).">.
                            ?organization rdf:type <http://dbpedia.org/ontology/Organisation>.
                            OPTIONAL { 
                                ?organization rdfs:label ?organizationLabel.
                                FILTER(LANG(?organizationLabel) = 'fr') 
                            }
                            OPTIONAL { ?organization dbo:thumbnail ?thumbnail. }
                            OPTIONAL { ?organization dbo:abstract ?description. FILTER(LANG(?description) = 'fr') }
                        }
                ";

                $dbpediaEndpoint = 'http://dbpedia.org/sparql';
                $queryParamsDbpedia = [
                    'query' => $sparqlQueryDbpedia,
                    'format' => 'application/json',
                ];
                $urlDbpedia = $dbpediaEndpoint . '?' . http_build_query($queryParamsDbpedia);

                $responseDbpedia = file_get_contents($urlDbpedia);
                $dbpediaData = json_decode($responseDbpedia, true);

                foreach ($dbpediaData['results']['bindings'] as $key => $value) {
                    $objet = array(
                        "links" => isset($value["organization"]["value"]) ? $value["organization"]["value"] : "",
                        "name" => isset($value["organizationLabel"]["value"]) ? $value["organizationLabel"]["value"] : "",
                        "urlImg" => isset($value["thumbnail"]["value"]) ? $value["thumbnail"]["value"] : "",
                        "description" => isset($value["description"]["value"]) ? $value["description"]["value"] : ""
                    );

                    if(!in_array($objet, $all_data))
                    {
                        array_push($all_data, $objet);
                    }
                }
                
            } else if($type == "event") {

                $queryEvents = "https://query.wikidata.org/sparql?format=json&query=SELECT%20?event%20(COALESCE(?eventLabel,%20?altLabel)%20AS%20?label)%20?date%20?eventDescription%20?eventImage%20WHERE%20{%20?event%20wdt:P31/wdt:P279*%20wd:Q1656682;%20wdt:P276%20wd:".$wikidataID.";%20wdt:P585%20?date.%20OPTIONAL%20{%20?event%20rdfs:label%20?eventLabel.%20FILTER(LANG(?eventLabel)%20=%20%22en%22)%20}%20OPTIONAL%20{%20?event%20skos:altLabel%20?altLabel.%20FILTER(LANG(?altLabel)%20=%20%22en%22)%20}%20OPTIONAL%20{%20?event%20wdt:P6216%20?eventDescription.%20}%20OPTIONAL%20{%20?event%20wdt:P18%20?eventImage.%20}%20SERVICE%20wikibase:label%20{%20bd:serviceParam%20wikibase:language%20%22[AUTO_LANGUAGE],fr%22.%20}%20}%20ORDER%20BY%20?date";

                $repons_wiki = self::fetchDataWithCurl($queryEvents);
                $wikidata_event = json_decode($repons_wiki, true);
                
                foreach ($wikidata_event['results']['bindings'] as $event){
                    $objet = array(
                        "links" => isset($event['event']['value']) ? $event['event']['value'] : "",
                        "label" => isset($event['label']['value']) ? $event['label']['value'] : "",
                        "description" => isset($event['eventDescription']['value']) ? $event['eventDescription']['value'] : "",
                        "imageUrl" => isset($event['eventImage']['value']) ? $event['eventImage']['value'] : "",
                        "date" => isset($event['date']['value']) ? $event['date']['value'] : ""
                    );
                    
                    if(!in_array($objet, $all_data))
                    {
                        array_push($all_data, $objet);
                    }
                }

               
            }

            return $all_data;
        }
        
        public static function convertWikiToPh($url, $text_filter) {
            
            $all_data = array();
            
            $map = TranslateWikiToPh::$mapping_element;
            
            $param = self::getPrimaryParam($map);
            
            $param['key'] = 'convert_wiki';
            $param['nameFile'] = 'convert_wiki';
            
            $wikidata_page_city = json_decode(file_get_contents($url), true);
            
            $pos_wikidataID = strrpos($url, "Q");
            
            $wikidataID = substr($url, $pos_wikidataID);
            $wikidataID = substr($wikidataID, 0, strpos($wikidataID, "."));
            
            $label_dbpedia = $wikidata_page_city["entities"][$wikidataID]["sitelinks"]["frwiki"]["title"];
            
            // $url_wikipedia = "https://fr.wikipedia.org/wiki/".str_replace(" ", "_", $label_dbpedia);
            // var_dump($url_wikipedia);
            
            $wikidata_article = json_decode(file_get_contents("https://query.wikidata.org/sparql?format=json&query=SELECT%20DISTINCT%20%3Fitem%20%3FitemLabel%20%3FitemDescription%20%3Fcoor%20%3Frange%20WHERE%20{%0A%20%3Fitem%20wdt%3AP131%20wd%3A" . $wikidataID . ".%0A%20%3Fitem%20%3Frange%20wd%3A" . $wikidataID . ".%0A%20%3Fitem%20wdt%3AP625%20%3Fcoor.%0A%20SERVICE%20wikibase%3Alabel%20{%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20}%0A}"), true);
            
            $key_wikidata_item = 0;
            
            foreach ($wikidata_article['results']['bindings'] as $key => $value) {
                
                if ($text_filter !== null) {
                    
                    if (stristr($value['itemLabel']['value'], $text_filter) == true) {
                        
                        $all_data[$key_wikidata_item] = array();
                        
                        if (@$value['coor']) {
                            $coor = self::getLatLongWikidataItem($value);
                            array_push($all_data[$key_wikidata_item], $value['itemLabel']['value'], $coor, $value['item']['value'], @$value['itemDescription']['value'], @$value['itemDescription']['value']);
                        }
                        $key_wikidata_item++;
                    }
                } else {
                    
                    $all_data[$key_wikidata_item] = array();
                    
                    if (@$value['coor']) {
                        $coor = self::getLatLongWikidataItem($value);
                        array_push($all_data[$key_wikidata_item], $value['itemLabel']['value'], $coor, $value['item']['value'], @$value['itemDescription']['value'], @$value['itemDescription']['value']);
                    }
                    
                    $key_wikidata_item++;
                }
            }
            
            $param['file'][0] = json_encode($all_data);
            
            $result = Import::previewData($param);
            
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function poleEmploi($url) {
            $curl = curl_init();
            
            curl_setopt($curl, CURLOPT_URL, "https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=%2Fpartenaire");
            
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=PAR_communecter_9cfae83c352184eff02df647f08661355f3be7028c7ea4eda731bf8718efbfff&client_secret=62a4a6aa2d82fa201eca1ebb3df639882d2ed7cd75284486aaed3a436df67e55&scope=application_PAR_communecter_9cfae83c352184eff02df647f08661355f3be7028c7ea4eda731bf8718efbfff api_infotravailv1");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $token = curl_exec($curl);
            
            curl_close($curl);
            //echo $token; //exit;
            $token_final = json_decode($token, true);
            
            $curl2 = curl_init();
            
            $pos = strpos($url, "=");
            
            $url_head = substr($url, 0, ($pos + 1));
            $url_param = substr($url, ($pos + 1));
            
            $url = $url_head . urlencode($url_param);
            //var_dump($url);
            curl_setopt($curl2, CURLOPT_URL, $url);
            curl_setopt($curl2, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $token_final["access_token"]));
            
            curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
            $offres = curl_exec($curl2);
            //echo $offres;  exit;
            curl_close($curl2);
            
            $offres_final = json_decode($offres, true);
            return $offres_final;
        }
        
        public static function getToken($apiName) {
            
            // if(!empty(Yii::app()->params["token".$apiName])){
            //     $res = Yii::app()->params["token".$apiName];
            // } else {
            $res = Application::getToken($apiName);
            if (empty($res) || (!empty($res) && ($res["expireToken"] - time()) <= 180)) {
                
                if ($apiName == "poleEmploi") {
                    $res["token"] = self::poleEmploiToken();
                }
                
            }
            //}
            return (empty($res["token"]) ? null : $res["token"]);
        }
        
        //Yii::app()->params["poleEmploi"]["client_id"]
        public static function poleEmploiToken() {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, "https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=%2Fpartenaire");
            curl_setopt($curl, CURLOPT_POST, true);
            // curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=PAR_communectertest_c46ea89b19688d7d3364badae07f308f722f83b0cd9bd040ecc5a468c6f1d07a&client_secret=de3f5d98dcefef02d98c239b3973878320ec7815005dff553afc35ae067f3dc9&scope=application_PAR_communectertest_c46ea89b19688d7d3364badae07f308f722f83b0cd9bd040ecc5a468c6f1d07a api_offresdemploiv1 o2dsoffre api_infotravailv1");
            //var_dump(Yii::app()->params["poleEmploi"]["client_secret"]);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=" . Yii::app()->params["poleEmploi"]["client_id"] . "&client_secret=" . Yii::app()->params["poleEmploi"]["client_secret"] . "&scope=" . Yii::app()->params["poleEmploi"]["scope"]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $token = curl_exec($curl);
            $token_final = json_decode($token, true);
            Application::saveToken("poleEmploi", $token_final);
            curl_close($curl);
            return $token_final;
        }
        
        public static function poleEmploi2($url, $params) {
            
            // $curl = curl_init();
            // curl_setopt($curl, CURLOPT_URL, "https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=%2Fpartenaire");
            // curl_setopt($curl, CURLOPT_POST, true);
            // curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=PAR_communectertest_c46ea89b19688d7d3364badae07f308f722f83b0cd9bd040ecc5a468c6f1d07a&client_secret=de3f5d98dcefef02d98c239b3973878320ec7815005dff553afc35ae067f3dc9&scope=application_PAR_communectertest_c46ea89b19688d7d3364badae07f308f722f83b0cd9bd040ecc5a468c6f1d07a api_offresdemploiv1 o2dsoffre api_infotravailv1");
            // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            // $token = curl_exec($curl);
            // $token_final = json_decode($token, true);
            // curl_close($curl);
            
            $token_final = self::getToken("poleEmploi");
            
            $res = array(
                "httpCode" => "0",
                "result"   => array(),
                "token"    => $token_final,
            );
            
            if (!empty($token_final) && !empty($token_final["access_token"])) {
                $curl2 = curl_init();
                curl_setopt($curl2, CURLOPT_URL, $url);
                curl_setopt(
                    $curl2,
                    CURLOPT_HTTPHEADER,
                    array(
                        'Authorization: Bearer ' . $token_final["access_token"],
                        'Content-type: application/json',
                    )
                );
                curl_setopt($curl2, CURLOPT_POST, true);
                $dataCurl = json_encode($params);
                curl_setopt($curl2, CURLOPT_POSTFIELDS, $dataCurl);
                curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
                $offres = curl_exec($curl2);
                $res = array(
                    "httpCode" => curl_getinfo($curl2, CURLINFO_HTTP_CODE),
                    "result"   => $offres,
                    "token"    => $token_final,
                );
                //
                // echo curl_getinfo ( $curl2, CURLINFO_HTTP_CODE  )."<br/>";
                // // echo curl_getinfo ( $curl2, CURLINFO_EFFECTIVE_URL  )."<br/>";
                // // echo curl_getinfo ( $curl2, CURLINFO_HEADER_OUT  )."<br/>";
                // // exit ;
                // echo $offres;  exit;
                curl_close($curl2);
            }
            return $res;
        }
        
        public static function convertPoleEmploiToPh($url, $params = array(), $activity_letters = null) {
            
            if (empty($params)) {
                $params = array(
                    'technicalParameters' => array(
                        'page' => 1,
                        'per_page' => 20,
                        'sort' => 1,
                    ),
                );
            }
            
            $resPoleEmploie = self::poleEmploi2($url, $params);
            //var_dump($resPoleEmploie);
            if ($resPoleEmploie["httpCode"] == "200") {
                $offres_final = json_decode($resPoleEmploie["result"], true);
                $map = TranslatePoleEmploiToPh::$mapping_offres;
                
                $param = self::getPrimaryParam($map);
                
                $param['pathObject'] = '';
                $param['key'] = 'convert_poleemploi';
                $param['nameFile'] = 'convert_poleemploi';
                
                $offres_array = [];
                $offres_array['records'] = [];
                
                if (isset($url) && !empty($offres_final["results"])) {
                    $param['file'][0] = json_encode($offres_final["results"]);
                }
                //var_dump($param);
                $result = Import::previewData($param, true);
                $result["count"] = $offres_final["technicalParameters"]["totalNumber"];
                //var_dump($result);
                if ($result['result'] !== false) {
                    $result['elements'] = json_decode($result['elements']);
                }
                
                // } else {
                //     $res = [];
                // }
            } else {
                $result['result'] = false;
                $result['msg'] = "Il y a un soucis avec l'api du site Pole Emploi. Veuillez rééssayer dans quelque instant";
                $result['return'] = $resPoleEmploie;
                $result['data'] = $resPoleEmploie["result"];
            }
            
            return $result;
        }
        
        public static function ConvertValueFlowsToPh($url) {
            
            $map = TranslateValueFlowsToPh::$mapping_valueflows;
            
            $param = self::getPrimaryParam($map);
            
            $param['nameFile'] = 'convert_valueflows';
            $param['key'] = 'convert_valueflows';
            
            if (isset($url)) {
                $param['file'][0] = file_get_contents($url);
            }
            
            $result = Import::previewData($param);
            
            if ($result['result'] !== false) {
                $res = json_decode($result['elements']);
            } else {
                $res = [];
            }
            
            return $res;
        }
        
        public static function ConvertOrgancityToPh($url) {
            
            $map = TranslateOrgancityToPh::$mapping_organcity;
            $param = self::getPrimaryParam($map);
            
            $param['nameFile'] = 'convert_organcity';
            $param['key'] = 'convert_organcity';
            
            if (isset($url)) {
                $param['file'][0] = file_get_contents($url);
            }
            
            $result = Import::previewData($param);
            
            var_dump($result);
            
            //       if ($result['result'] !== false) {
            //     $res = json_decode($result['elements']);
            // } else {
            //     $res = [];
            // }
            
            //       return $res;
        }
        
        public static function getLatLongWikidataItem($wikidata_item) {
            
            if (@$wikidata_item['coor']) {
                $coor = explode(" ", $wikidata_item['coor']['value']);
                $coor["longitude"] = substr($coor[0], 6);
                $coor["latitude"] = rtrim($coor[1], ')');
                
                unset($coor[0]);
                unset($coor[1]);
            }
            
            return $coor;
        }
        
        public static function convertGogoCarto($url, $text_filter) {
            $res = array();
            
            if (!empty($url)) {
                $data = SIG::getUrl($url);
                $data = json_decode($data, true);
            }
            
            //$res = $url ;
            
            $map = TranslateGogoCarto::$dataBinding_network;
            //var_dump($map);
            //foreach ($data["data"] as $key => $value) {
            // var_dump($value);
            //             echo "<br/>";
            //$res[] = Translate::convert($value , $map);
            //}
            
            $res = Translate::convert($data["data"], $map);
            
            $result = array('entities' => $res);
            //exit;
            //       $param = self::getPrimaryParam($map);
            
            //       $param['key'] = 'convert_wiki';
            //       $param['nameFile'] = 'convert_wiki';
            
            return $result;
        }
        
        public static function convertFtl($url, $text_filter) {
            $res = array();
            
            if (!empty($url)) {
                $data = SIG::getUrl($url);
                $data = json_decode($data, true);
            }
            
            $map = TranslateFtl::$dataBinding_allOrganization;
            
            $res = Translate::convert($data["data"], $map);
            
            $result = array('entities' => $res);
            
            return $result;
        }
        
        public static function convertWikiMediaToPh($data, $categorie, $wikiName) {
            $res = array();
            //var_dump([$data, $categorie, $wikiName]);exit;
            $init = new TranslateMediaWiki();
            $map = $init->dataBinding($categorie, $wikiName);
            //return Translate::convert($data, $map);
            //var_dump([$wikiName]);/*, $categorie, $wikiName])*///exit;
            $res += Translate::convert($data, $map);
            //  var_dump($res);//exit;
            //     var_dump($data);
            if (isset($data['data']['Type']) && is_array($data['data']['Type'])) {
                $type = '';
                foreach ($res['data']['type'] as $k => $v) {
                    $type = $type . " " . $v;
                }
                $res['data']['type'] = $type;
                //var_dump($res['type']); //exit;
            } else {
                if ($res['data']['@type'] == "Organization") {
                    if (isset($res['data']['type'])) {
                        foreach ($res['data']['type'] as $v) {
                            if ($v == "Association") {
                                $res['data']['type'] = "NGO";
                                break;
                            } else if ($v == "Groupe public") {
                                $res['data']['type'] = "GovernmentOrganization";
                                break;
                            } else if ($v == "Startup" || $v == "Collectivité" || $v == "Indépendant") {
                                $res['data']['type'] = "LocalBusiness";
                                break;
                            }
                        }
                    } else {
                        $res['data']['type'] = "Group";
                    }
                } else if ($res['data']['@type'] == "Project") {
                    $res['data'] += ['type' => "projet"];
                } else if ($res['data']['@type'] == "Classifieds") {
                    $res['data'] += ['type' => "ressource"];
                } else if ($res['data']['@type'] == "Person") {
                    $res['data'] += ['type' => "citoyen"];
                }
            }
            $res['data'] += $data['data'];
            return $res;
            // exit;
        }
        
        public static function ConvertOpenAgendaToPh($parameters = []) {
            // récupération des liens
            $simple_fields = [
                'key',
                'limit',
                'after',
                'size',
                'search',
                'official',
                'network'
            ];
            $array_fields = [
                'fields',
                'slug',
                'uid'
            ];
            $url = 'https://api.openagenda.com/v2/me/agendas?';
            $url_constructor = [];
            
            // vérification des paramètres simples
            foreach ($simple_fields as $simple_field) {
                if (!empty($parameters[$simple_field])) $url_constructor[] = $simple_field . '=' . $parameters[$simple_field];
            }
            
            // vérirification des paramètres avec des tableaux
            foreach ($array_fields as $array_field) {
                if (!empty($parameters[$array_field])) {
                    foreach ($parameters[$array_field] as $simple_field) {
                        $url_constructor[] = $array_field . "[]=" . $simple_field;
                    }
                }
            }
            
            // assemblage des liens
            $url .= implode('&', $url_constructor);
            
            // recueillir les données distantes
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
            $output = curl_exec($curl);
            curl_close($curl);
            return json_decode($output, true);
        }
        
        public static function ConvertOpenAgendaEventToPh($agenda, $key) {
            $url = "https://api.openagenda.com/v2/agendas/$agenda/events?key=$key";
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($curl);
            
            // mapping
            return $output;
        }

        public static function ConvertICalendarToPh($http) {
            $curl = curl_init($http);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($curl);
            
            // mapping
            return $output;
        }
    }
