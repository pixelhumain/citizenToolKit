<?php
class RocketChat
{

	public static function getToken($email, $pwd)
	{
		$res = array(
			"loginToken" => null,
			"rocketUserId" => null,
			"msg" => ""
		);

		if (@$_SESSION["loginToken"] && @$_SESSION["rocketUserId"])
		{
			$res["loginToken"] = $_SESSION["loginToken"];
			$res["rocketUserId"] = $_SESSION["rocketUserId"];
			$res["msg"] = "user logged in from session";
		}
		else
		{
			if (!defined('REST_API_ROOT'))
				define('REST_API_ROOT', '/api/v1/');

			if (!defined('ROCKET_CHAT_INSTANCE'))
				define('ROCKET_CHAT_INSTANCE', Yii::app()->params['rocketchatURL']);

			try
			{
				Yii::import('rocketchat.RocketChatClient', true);
				Yii::import('rocketchat.RocketChatUser', true);
				Yii::import('rocketchat.RocketChatChannel', true);
				Yii::import('rocketchat.RocketChatGroup', true);

				Yii::import('httpful.Request', true);
				Yii::import('httpful.Bootstrap', true);
				$user = new \RocketChat\User($email, $pwd);

				$log = $user->login();
				//var_dump($log);
				if (@$log->status == "success")
				{
					$res["loginToken"] = $user->authToken;
					$res["rocketUserId"] = $user->id;
					$res["msg"] = "user logged in";
					unset(Yii::app()->session["pwd"]);
				}
				else
				{
					$res["msg"] = "mail or pwd don't exist or match,can't log into RC : " . @$log->message;
					$res["error"] = "unauthorised";
				}
			}
			catch (Exception $e)
			{
				$res["msg"] = $e->getMessage();
				$res["error"] = "noHost";
			}
		}
		return $res;
	}

	public static function getTokenService($email, $serviceEncrypt)
	{
		$res = array(
			"loginToken" => null,
			"rocketUserId" => null,
			"msg" => ""
		);

		if (@$_SESSION["loginToken"] && @$_SESSION["rocketUserId"])
		{
			$res["loginToken"] = $_SESSION["loginToken"];
			$res["rocketUserId"] = $_SESSION["rocketUserId"];
			$res["msg"] = "user logged in from session";
		}
		else
		{
			if (!defined('REST_API_ROOT'))
				define('REST_API_ROOT', '/api/v1/');

			if (!defined('ROCKET_CHAT_INSTANCE'))
				define('ROCKET_CHAT_INSTANCE', Yii::app()->params['rocketchatURL']);

			try
			{
				Yii::import('rocketchat.RocketChatClient', true);
				Yii::import('rocketchat.RocketChatUser', true);
				Yii::import('rocketchat.RocketChatChannel', true);
				Yii::import('rocketchat.RocketChatGroup', true);

				Yii::import('httpful.Request', true);
				Yii::import('httpful.Bootstrap', true);
				$user = new \RocketChat\User($email, null, null, false, $serviceEncrypt);

				$log = $user->login();
				//var_dump($log);
				if (@$log->status == "success")
				{
					$res["loginToken"] = $user->authToken;
					$res["rocketUserId"] = $user->id;
					$res["msg"] = "user logged in";
				}
				else
				{
					$res["msg"] = "mail or pwd don't exist or match,can't log into RC : " . @$log->message;
					$res["error"] = "unauthorised";
				}
			}
			catch (Exception $e)
			{
				$res["msg"] = $e->getMessage();
				$res["error"] = "noHost";
			}
		}
		return $res;
	}

	public static function createGroup($name, $type = null, $username, $inviteOnly = null, $owner = false)
	{
		defined('REST_API_ROOT') or define('REST_API_ROOT', '/api/v1/');
		defined('ROCKET_CHAT_INSTANCE') or define('ROCKET_CHAT_INSTANCE', Yii::app()->params['rocketchatURL']);

		try
		{
			Yii::import('rocketchat.RocketChatClient', true);
			Yii::import('rocketchat.RocketChatUser', true);
			Yii::import('rocketchat.RocketChatChannel', true);
			Yii::import('rocketchat.RocketChatGroup', true);
			Yii::import('httpful.Request', true);
			Yii::import('httpful.Bootstrap', true);

			$channel = ($type == "channel") ? new \RocketChat\Channel($name, array(), true) : new \RocketChat\Group($name, array(), true);


			$res = (object)array(
				"name" => $name,
				"type" => ($type == "channel") ? "channel" : "group",
				"username" => $username,
				/*"adminLoginToken" => Yii::app()->params["adminLoginToken"],
		    	"adminRocketUserId" => Yii::app()->params["adminRocketUserId"]*/
			);
			if (!$inviteOnly)
			{
				$res->create = $channel->create();
				if (!@$res->create->success)
				{
					$res->info = $channel->info();
				}


				if (!@$res->info->success)
				{
					//check if channel in opposite Security
					$channel2 = ($type == "channel") ? new \RocketChat\Group($name, array(), true) : new \RocketChat\Channel($name, array(), true);
					$res->info2 = $channel2->info();
					//print_r($res->info2);exit;

					if (@$res->info2->success)
					{
						$switchType = ($type == "channel") ? "c" : "p";
						$res->settype = $channel2->setType($name, $switchType);
					}
				}
			}

			if (@$username)
			{
				if (@$res->settype)
				{
					$channelType = (@$res->settype->channel) ? new \RocketChat\Group($name, array(), true) : new \RocketChat\Channel($name, array(), true);
					$res->invite = $channelType->invite($username);
					if (@$res->invite->channel->usernames)
						$channel->members = $res->invite->channel->usernames;

					if ($owner)
					{
						$res->owner = $channelType->addOwner($username);
					}
				}
				else
				{
					$res->invite = $channel->invite($username);
					if (@$res->invite->channel->usernames)
						$channel->members = $res->invite->channel->usernames;

					if ($owner)
					{
						$res->owner = $channel->addOwner($username);
					}
				}
			}
		}
		catch (Exception $e)
		{
			$res->msg = $e->getMessage();
		}
		return $res;
	}

	public static function rename($name, $new, $type)
	{
		defined('REST_API_ROOT') or define('REST_API_ROOT', '/api/v1/');
		defined('ROCKET_CHAT_INSTANCE') or define('ROCKET_CHAT_INSTANCE', Yii::app()->params['rocketchatURL']);

		try
		{
			Yii::import('rocketchat.RocketChatClient', true);
			Yii::import('rocketchat.RocketChatUser', true);
			Yii::import('rocketchat.RocketChatChannel', true);
			Yii::import('rocketchat.RocketChatGroup', true);
			Yii::import('httpful.Request', true);
			Yii::import('httpful.Bootstrap', true);

			$channel = ($type === true) ? new \RocketChat\Group($name, array(), true) : new \RocketChat\Channel($name, array(), true);

			$res = (object)array(
				"name" => $name,
				"type" => ($type === true) ? "group" : "channel"
			);
			$res->info = $channel->info();

			$res->rename = $channel->rename($new);
		}
		catch (Exception $e)
		{
			$res->msg = $e->getMessage();
		}
		return $res;
	}

	public static function setCustomFields($name, $customFields, $type)
	{
		defined('REST_API_ROOT') or define('REST_API_ROOT', '/api/v1/');
		defined('ROCKET_CHAT_INSTANCE') or define('ROCKET_CHAT_INSTANCE', Yii::app()->params['rocketchatURL']);

		try
		{
			Yii::import('rocketchat.RocketChatClient', true);
			Yii::import('rocketchat.RocketChatUser', true);
			Yii::import('rocketchat.RocketChatChannel', true);
			Yii::import('rocketchat.RocketChatGroup', true);
			Yii::import('httpful.Request', true);
			Yii::import('httpful.Bootstrap', true);

			$channel = ($type === true) ? new \RocketChat\Group($name, array(), true) : new \RocketChat\Channel($name, array(), true);

			$res = (object)array(
				"name" => $name,
				"type" => ($type === true) ? "group" : "channel"
			);
			$res->info = $channel->info();

			$res->setCustomFields = $channel->setCustomFields($customFields);
		}
		catch (Exception $e)
		{
			$res->msg = $e->getMessage();
		}
		return $res;
	}

	public static function post($to, $msg, $url = "", $type = false,  $alias = "oceco", $avatar = "https://oce.co.tools/avatar.png", $lien = "Lien oceco", $is_api = false)
	{
		defined('REST_API_ROOT') or define('REST_API_ROOT', '/api/v1/');
		defined('ROCKET_CHAT_INSTANCE') or define('ROCKET_CHAT_INSTANCE', Yii::app()->params['rocketchatURL']);
		$is_channel = filter_var($type, FILTER_VALIDATE_BOOL);
		try
		{
			Yii::import('rocketchat.RocketChatClient', true);
			Yii::import('rocketchat.RocketChatUser', true);
			Yii::import('rocketchat.RocketChatChannel', true);
			Yii::import('rocketchat.RocketChatGroup', true);
			Yii::import('httpful.Request', true);
			Yii::import('httpful.Bootstrap', true);

			$channel = $is_channel ? new \RocketChat\Channel($to, array(), true) : new \RocketChat\Group($to, array(), true);

			$res = array(
				"name"		=> $to,
				"type"		=> $is_channel ? "channel" : "group",
				"alias"		=> $alias,
				"avatar"	=> $avatar,
				"text"		=> $msg
			);
			if (!empty($url) && !empty($lien))
			{
				$res["attachments"] = array(
					array(
						"color" => '#E33551',
						"button_alignment" => 'horizontal',
						"actions" => [
							array(
								"type" => 'button',
								"text" => $lien,
								"url" => $url,
							)
						]
					)
				);
			}
			$res['info'] = $channel->info();
			$res['post'] = $channel->postMessage($res, $is_api);
		}
		catch (Exception $e)
		{
			$res['msg'] = $e->getMessage();
		}
		return $res;
	}

	public static function listUserChannels()
	{

		defined('REST_API_ROOT') or define('REST_API_ROOT', '/api/v1/');
		defined('ROCKET_CHAT_INSTANCE') or define('ROCKET_CHAT_INSTANCE', Yii::app()->params['rocketchatURL']);

		Yii::import('rocketchat.RocketChatClient', true);
		Yii::import('rocketchat.RocketChatUser', true);
		Yii::import('httpful.Request', true);
		Yii::import('httpful.Bootstrap', true);
		$api = new \RocketChat\Client();
		$logged = new \RocketChat\User(Yii::app()->session['userEmail']);

		return $logged->listJoined();
	}

	public static function inviteGroup($name, $username)
	{
		defined('REST_API_ROOT') or define('REST_API_ROOT', '/api/v1/');
		defined('ROCKET_CHAT_INSTANCE') or define('ROCKET_CHAT_INSTANCE', Yii::app()->params['rocketchatURL']);

		try
		{
			Yii::import('rocketchat.RocketChatClient', true);
			Yii::import('rocketchat.RocketChatUser', true);
			Yii::import('rocketchat.RocketChatChannel', true);
			Yii::import('rocketchat.RocketChatGroup', true);
			Yii::import('httpful.Request', true);
			Yii::import('httpful.Bootstrap', true);

			$channel = new \RocketChat\Group($name, array(), true);

			$res = (object)array(
				"name" => $name,
				"type" => "group"
			);
			$res->info = $channel->info();

			$res->invite = $channel->invite($username);
		}
		catch (Exception $e)
		{
			$res->msg = $e->getMessage();
		}
		return $res;
	}
}
