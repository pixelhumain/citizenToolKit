<?php

class Crowdfunding {
	const COLLECTION = "crowdfunding";
	const CONTROLLER = "crowdfunding";
	const MODULE = "crowdfunding";
	const ICON = "fa-map-money";
	const TYPE_CAMPAIGN = "campaign";
	const TYPE_DONATION = "donation";
	const TYPE_PLEDGE = "pledge";
	
	public static $types = array (
		"pledge" =>"pledge",
		"donation" => "donation",
		"campaign" =>"campaign"
	);

	//From Post/Form name to database field name
	public static $dataBinding = array (
	    "name" => array("name" => "name"),
	    //"targetCampaign" => array("name" => "goal"),
	    // "preferences" => array("name" => "preferences"),
	    // "key" => array("name" => "key"),
	    // //"id" => array("name" => "id"),
	    // "public" => array("name" => "public"),
	    "type" => array("name" => "type"),
	    "subtype" => array("name" => "subtype"),
	    "collection" => array("name" => "collection"),
	    // "preferences" => array("name" => "preferences"),
	    "description" => array("name" => "description"),
	    "receiver" => array("name" => "receiver"),
	    "parent" => array("name" => "parent"),
	    "parentId" => array("name" => "parentId"),
	    "parentType" => array("name" => "parentType"),
	    "media" => array("name" => "media"),
	    "urls" => array("name" => "urls"),
	    "medias" => array("name" => "medias"),
	    "tags" => array("name" => "tags"),
	    "structags" => array("name" => "structags"),
	    "shortDescription" => array("name" => "shortDescription"),
	    "donators" => array("name" => "donators"),
	    "modified" => array("name" => "modified"),
	    "source" => array("name" => "source"),
	    "updated" => array("name" => "updated"),
	    "creator" => array("name" => "creator"),
	    "created" => array("name" => "created"),
	    "nameCampaign" => array("name" => "name"),
		"startDateCampaign" => array("name" => "startDate"),
		"endDateCampaign" => array("name" => "endDate"),
		"targetCampaign" => array("name" => "goal"),
		"goal" => array("name" => "goal"),
		"startDate" => array("name" => "startDate"),
		"endDate" => array("name" => "endDate"),
		"amount" => array("name" => "amount"),
		"civility" => array("name" => "civility"),
		"surname" => array("name" => "surname"),
		"invoice" => array("name" => "invoice"),
		"donatorName" => array("name" => "donatorName"),
		"iban" => array("name" => "iban"),
		"donationPlatform" => array("name" => "donationPlatform"),
		"telephone" => array("name" => "telephone"),
		"email" => array("name" => "email"),
		"directDonation" => array("name" => "directDonation"),
		"donationMean" => array("name" => "donationMean"),
		"invoiceDetails" => array("name" => "invoiceDetails"),
		"publicDonationData" => array("name" => "publicDonationData"),
		"behalf" => array("name" => "behalf"),
		"video" => array("name" => "video"),
		"medias" => array("name" => "medias"),

	);

//From Post/Form name to database field name

	public static function getConfig(){
		return array(
			"collection"    => self::COLLECTION,
            "controller"   => self::CONTROLLER,
            //"module"   => self::MODULE,
			//"init"   => Yii::app()->getModule( self::MODULE )->assetsUrl."/js/init.js" ,
			//"form"   => Yii::app()->getModule( self::MODULE )->assetsUrl."/js/dynForm.js" ,
           // "categories" => CO2::getContextList(self::MODULE),
           // "lbhp"=>true
		);
	}
	/**
	 * get all poi details of an element
	 * @param type $id : is the mongoId (String) of the parent
	 * @param type $type : is the type of the parent
	 * @return list of pois
	 */

	//crowdfunding prepdata and Save in the case of integrated crowdfundin fields in projects's dynform

	// public static function save($params){
	// 	 //var_dump('bala');exit;
	// 	 //var_dump(Yii::app()->session["user"]["name"]);exit;
	// 	$valid = DataValidator::validate( ucfirst(self::CONTROLLER), json_decode (json_encode ($params), true), null);
	// 	if( $valid["result"]){ 
	// 		$params["created"]=time();
	// 		$params["updated"]=time();
	// 		$params["creator"]=[];
	// 		$params["creator"][Yii::app()->session["userId"]]["type"]=Person::COLLECTION;
	// 		$params["creator"][Yii::app()->session["userId"]]["name"]=Yii::app()->session["user"]["name"];
	// 		Yii::app()->mongodb->selectCollection(self::COLLECTION)->insert( $params);
	//     	$res = array('result'=>true, "msg" => Yii::t("common","The crowdfunding is succesfully registered"), "value" => $params);
	//     	//var_dump($res);exit;
	// 	}else
	// 		$res = array( "result" => false, "error"=>"400",
 //                          "msg" => Yii::t("common","Something went really bad : ".$valid['msg']) );

 //        return $res;

	// }

	// public static function prepData($params){
	// 	$crowdfunding = [];
	// 	//var_dump($params);
	// 	foreach ($params as $key => $value){
	// 		//var_dump(self::$dataBinding[$key]);
	// 		if(isset(self::$dataBinding[$key])){
	// 			//var_dump(self::$dataBinding[$key]);
	// 			if($key!="collection" && $key!=self::$dataBinding[$key]["name"]) {
	// 			//var_dump("expression");exit;
	// 			//$crowdfunding[$key]=$value;
	// 				$crowdfunding[self::$dataBinding[$key]["name"]]=$value;
	// 				unset($params[$key]);
	// 			}				
	// 		}
	// 	}	
	// 	$crowdfunding["parent"][$params["id"]]["type"]=$params["collection"];
	// 	$crowdfunding["parent"][$params["id"]]["name"]=$params["name"];

	// 	$crowdfunding["collection"]=self::COLLECTION;

	// 	//var_dump($crowdfunding);exit;
	// 	self::save($crowdfunding);
	// 	return $params;	        
	// }
	public static function removePledgeAndDonation($id){
		PHDB::remove(self::COLLECTION, array("parent.".$id =>array('$exists'=>true)));
	}

	public static function getCampaignAndCountersByParentId($id){	
		$campaign=self::getCampaignByParentId($id);
		$counters=self::getCounters($campaign["_id"]);	
		$campaign=array_merge($campaign,$counters);
		return $campaign;
	}

	public static function getCampaignById($id){	
		$campaign = PHDB::findOne(self::COLLECTION,array("_id"=>new MongoId($id)));
		//var_dump($campaign);exit;

	  	$counters=self::getCounters($id);	
	  	$campaign=array_merge($campaign,$counters);



	  	// $campaign["profilImg"]=Document::getLastImageByKey($id, self::COLLECTION, Document::IMG_PROFIL);

	  	$campaign = array_merge($campaign, Document::retrieveAllImagesUrl($id, self::COLLECTION, $campaign));
			$campaign["files"] = Document::getListDocumentsWhere(array("type"=>self::COLLECTION,"id"=>$id, "doctype"=>"file"),"file");
	  	
	  	//var_dump($imgProfil);exit;
	 //  	if (!empty($campaign)) {
		// 	$campaign = array_merge($campaign,"");
		// }	
	
		return $campaign;
	}

	public static function getCounters($id){	
	  		
		$donationAndPledgeCounter=self::getPledgeAndDonationCounter($id);

		//var_dump($donationAndPledgeCounter);exit;
	
		return $donationAndPledgeCounter;
	}

	public static function getCampaignByParentId($id){
		$campaign = PHDB::findOne(self::COLLECTION,array("parent.".$id =>array('$exists'=>true)),array("_id","name","goal","startDate","endDate","shortDescription","parent","description"));
	   	return $campaign;
	}

	public static function getPledgeAndDonationCounter($id){
		$counter = array();
		$countP =self::getPledgesCounter($id);
		$counter["pledgeNumber"]=$countP["amount"];
		$counter["pledgeIteration"]=$countP["number"];

		$countD= self::getDonationsCounter($id);
		$counter["donationNumber"]=$countD["amount"];
		$counter["donationIteration"]=$countD["number"];
		return $counter;
	}

	public static function getPledgesCounter($id){
		$pledges=self::getPledgesFromCampaignId($id);
		$counter= self::PledgeAndDonationCounter($pledges);
		return $counter;
	}	

	public static function getDonationsCounter($id){
		$donations=self::getDonationsFromCampaignId($id);
		$counter= self::PledgeAndDonationCounter($donations);
		return $counter;
	}

	public static function getPledgesFromCampaignId($id){
		$pledges = PHDB::find(self::COLLECTION,array("parent.".$id =>array('$exists'=>true),"type"=>"pledge"),array("amount"));
	   	return $pledges;
	}

	public static function getDonationsFromCampaignId($id){
		$pledges = PHDB::find(self::COLLECTION,array("parent.".$id =>array('$exists'=>true),"type"=>"donation"),array("amount"));
	   	return $pledges;
	}

	public static function PledgeAndDonationCounter($pledges){
		$amount=0;
		$number=0;
		foreach ($pledges as $key=>$value){				
			$amount=$amount+(integer)$value["amount"];
			$number=$number+1;
		}
		$counter=[];
		$counter["amount"]=$amount;
		$counter["number"]=$number;
	   	return $counter;
	}

	public static function validatePledge($id,$userId){
		//var_dump($id);exit;
		if ( !@$userId) {
            return array( "result" => false, "msg" => "You must be loggued to delete something" );
        }
        PHDB::update(self::COLLECTION,
        			array("_id"=>new MongoId($id)),
        			array('$set'=>array("type" => "donation"))
        );
        return array("result"=>true,
		    			"msg"=>"La promesse a été validée.", 
		    			"id"=>$id 
		    			);

		
	}



	
}
?>