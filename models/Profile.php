<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;

use Badge;

class Profile extends AbstractOpenBadge {
    public $context = Badge::CONTEXT; 
    public $type = 'profile';
    public $id;
    public $name;
    public $image;
    public $url;
    public $email;
    public $publicKey;
    public $revocationList;
    public static function fromElement($element)
    {
        $baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
		$array = [
			"@context" => "https://w3id.org/openbadges/v2",
			"id" => $baseUrl . "/co2/badges/issuers/id/" . (string) $element["_id"],
			"type" => "Issuer",
			"name" => $element["name"],
            "url" => $baseUrl . "/#page.type." . $element["collection"] . ".id." . $element["_id"],
            "image" => @$element["profilMediumImageUrl"] ? $baseUrl . $element["profilMediumImageUrl"] : null,
            "description" => $element["collection"] . " " . $element["_id"],
		];
		if(isset($element["email"])){
			$array["email"] = $element["email"];
		}
        $profileInstance = new Profile();
        $profileInstance->setFromAssociativeArray($array);
        return $profileInstance;
    }
}