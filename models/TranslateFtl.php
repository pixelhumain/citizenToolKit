<?php 
class TranslateFtl {
/*

	----------------- COMMUNECTER ----------------- 
*/
	

	public static $dataBinding_allOrganization  = [
		"ID_UNIQUE" => array("valueOf"  => 'idRecensementTL2023'),
	    "NOM" 	=> array("valueOf" => "name"),
		// "Insee Com" => array("valueOf" => "address.codeInsee"),
		"DECOUP"    => array("valueOf" => "address.inseeDensity"),
		"INSEE" 	=> array("valueOf" => "address.codeInsee"),
		"DATE_OUV" 	=> array("valueOf" => "openingDate"),
		"DESCRI_COU" 	=> array("valueOf" => "shortDescription"),
		"NOM_STRC_PORT" 	=> array("valueOf" => "holderOrganization"),
		"MODE_GEST" 	=> array("valueOf" => "tags","type" => "manageModel"),
		"AUTRE_MG" 	=> array("valueOf" => "extraManageModel"),
		"FAM_TL" 	=> array("valueOf" => "tags","type" => "typePlace"),
		"AUTRE_FAM_TL" 	=> array("valueOf" => "extraTypePlace"),
		"COMP_FTL" 	=> array("valueOf" => "tags","type" => "compagnon"),
		"SURF_BATI" 	=> array("valueOf" => "buildingSurfaceArea"),
		"SURF_AREA" 	=> array("valueOf" => "siteSurfaceArea"),
		"ADRESSE" 	=> array("valueOf" => "address.streetAddress"),
		"CODPOST" 	=> array("valueOf" => "address.postalCode"),
		"VILLE" 	=> array("valueOf" => "address.addressLocality"),
		"REGION" 	=> array("valueOf" => "address.level3Name"),
		"LATITUDE" 	=> array("valueOf" => "geoPosition.coordinates.1"),
		"LONGITUDE" 	=> array("valueOf" => "geoPosition.coordinates.0"),
		// "LOGO" 	=> array("valueOf" => "image" , "type"=>"url","toArray"=>true),
		"LOGO" 	=> array("valueOf" => "image" , "type"=>"url"),
		"SOCIAUX" 	=> array("valueOf" => "socialNetwork","type"=>"socialNetwork"),
		"INTERNET" 	=> array("valueOf" => "url"),
		"HOR_OUV" 	=> array("valueOf" => 'openingHours',"type" => "openingHours"),
		"EMAIL" 	=> array("valueOf" => "email"),
		"TELEPHONE" 	=> array("valueOf" => "telephone","type" => "telephone"),
		"VIDEO" 	=> array("valueOf" => "video.0"),
		"DESCRI_LONG" 	=> array("valueOf" => "description"),
		// "ID_COMMUNECTER" => array("valueOf"  	=> '_id.$id'),
		"LIEN_PAGE_FTL" => array("prefix"   => "https://cartographie.francetierslieux.fr/#@","valueOf"   => "slug")
	];

	public static $dataBinding_organization = array(
		"id_communecter" => array("valueOf"  	=> '_id.$id'),
	    "nom" 	=> array("valueOf" => "name"),
	    "statut" => array("valueOf" => "state"),
	    "type_lieu" => array("valueOf" => "tags","type" => "typePlace"), 
	    "description_courte"		=> array("valueOf" => "shortDescription"),
	   	"description"		=> array("valueOf" => "description"),
	   	"logo" => "A définir",
        "photo"	=> [ "valueOf" => "image" , "type"=>"url","toArray"=>true],
        "video" => "A définir",
        "reseau_sociaux" => "A définir",
        "site_internet" => array("valueOf" => 'url'),
	    // "geo" 	=> array("parentKey"=>"geoPosition", 
    	// 				 "valueOf" => array(
					// 			"latitude" 			=> array("valueOf" => "coordinates.1"),
					// 			"longitude" 			=> array("valueOf" => "coordinates.0")
			 	// 				)),
		"address" 	=> array( 
	    					 "valueOf" => array(
									"streetAddress" 	=> array("valueOf" => "address.streetAddress"),
									"code_insee" => array("valueOf" => "address.codeInsee"),
									"code_postal" 		=> array("valueOf" => "address.postalCode"),
									"commune"   => array("valueOf" => "address.addressLocality"),
									"pays" 	=> array("valueOf" => "address.addressCountry"),
                                    "latitude" 			=> array("valueOf" => "geoPosition.coordinates.1"),
                            		"longitude" 		=> array("valueOf" => "geoPosition.coordinates.0")      
						      )),
	   	"horaires_ouverture" => array("valueOf" => 'openingHours',"type" => "openingHours"),
	   	"nature_juridique" => array("valueOf" => "manageModel"),
	   	"contact_email" => array("valueOf" => 'email'),
	   	"contact_telephone" => array("valueOf" => 'telephone.fixe.0'),
	   	"reseaux" => array("valueOf" => 'network'),
	   	"taille" => array("valueOf" => 'spaceSize')
	);

	public static function openingHours($hours='') {
		$res="";
		if(!empty($hours)){
			$hours=array_values(array_filter($hours));
			$dayArr=[];
			foreach ($hours as $ind => $dayHours){
				$day = Yii::t("translate",$dayHours["dayOfWeek"]);
				
				$dayArr[$ind]=$day;
				$range="";

				$rangeDay[$ind]="";
				foreach($dayHours["hours"] as $i => $time){
					$rangeDay[$ind]=(!empty($rangeDay[$ind])) ? $rangeDay[$ind]."|".$time["opens"]."-".$time["closes"] : $rangeDay[$ind].$time["opens"]."-".$time["closes"];
					
				}
		        // --- other way to concatenate repetitive schedules ----
				// if ($ind>0 && $rangeDay[$ind]==$rangeDay[$ind-1]){
				// 			$dayAndRange[$ind]=str_replace(" ".$rangeDay[$ind-1],", ".$dayArr[$ind]." ".$rangeDay[$ind],$dayAndRange[$ind-1]);
				// 			$res=str_replace($dayAndRange[$ind-1],$dayAndRange[$ind],$res);		
			    if ($ind>0 && $rangeDay[$ind]==$rangeDay[$ind-1]){
				    $sameDay="";
					$i=$ind;
					while ($i>0 && $rangeDay[$i] ==$rangeDay[$i-1]) { 
						$sameDay=$dayArr[$i-1]."-".$dayArr[$ind];
						$i--;
					}
					$dayAndRange[$ind]=$sameDay." ". $rangeDay[$ind];
					$res=str_replace($dayAndRange[$ind-1],$dayAndRange[$ind],$res);

				}
				else{
					$dayAndRange[$ind]=$dayArr[$ind]." ".$rangeDay[$ind];
					$res=$res.$dayAndRange[$ind].";";
				}
			}
			$res=preg_replace('/;$/',"",$res);
		}
		return $res;
    } 

	public static function socialNetwork($val){
    	$arr=[];
    	foreach($val as $sn => $url){
			// if(is_array($sn) || is_array($url)){
			// 	var_dump($sn,$url);exit;
			// }
			$arr[]=$sn.":".$url;
            // if(in_array($value, self::$TypePlaceList)){
            // 	array_push($res,$value);
            // }
    	}

		$str=implode(";", $arr);
		// $str=(!empty($arr)) ? implode(" ; \n", $arr) : null;


    	return $str;
    }

    public static $TypePlaceList = [
		"Ateliers artisanaux partagés", 
		"Bureaux partagés / Coworking", 
		"Cuisine partagée / Foodlab", 
		"Fablab / Makerspace / Hackerspace (Espaces du Faire)", 
		"LivingLab / Laboratoire d'innovation sociale", 
		"Tiers-lieu nourricier", 
		"Tiers-lieu culturel / Lieux intermédiaires et indépendants", 
		"Autre famille de tiers-lieux"
        ];

    public static function typePlace($val){
    	$res=[];
    	foreach($val as $ind => $value){
            if(in_array($value, self::$TypePlaceList)){
            	array_push($res,$value);
            }
    	}

		$str=implode(";", array_unique($res));
		// $res=(!empty($res) ? array_unique($res) : "";

    	return $str;
    }

	public static $manageModelList = [
		"Association", 
        "Collectif citoyen", 
        "Universités / Écoles d’ingénieurs ou de commerce / EPST", 
        "Établissements scolaires (Lycée, Collège, Ecole)", 
        "Collectivités (Département, Intercommunalité, Région, etc)", 
        "SARL-SA-SAS", 
        "SCIC", 
        "SCOP", 
        "Autre mode de gestion"
    ];

    public static function manageModel($val){
    	$res=[];
    	foreach($val as $ind => $value){
            if(in_array($value, self::$manageModelList)){
            	array_push($res,$value);
            }
    	}
		$str=implode(";", array_unique($res));

    	return $str;
    }

	public static $compagnonList = [
		"Compagnon France Tiers-Lieux"
    ];

    public static function compagnon($val){
    	$res=[];
    	foreach($val as $ind => $value){
            if(in_array($value, self::$compagnonList)){
            	array_push($res,$value);
            }
    	}
		$str=implode(";", array_unique($res));

    	return $str;
    }

	public static function telephone($val){
    	$res="";
		
		if(!empty($val) && is_string($val)){
			$phone=$val;
			
			$phone = (strlen($phone)==9 && substr($phone,1,0)!="0") ? "0".$phone : str_replace("(0)","",$phone) ;
			$phone=(preg_match('/^[+0-9]/',$phone)) ? $phone : "";
			$numIndex=2;
		    $phone=preg_replace('~[/\s\.(.*?\-)]~','',$phone);
		    $phone=(substr($phone,0,2)=="00") ? "+".substr($phone,2) : 
			((substr($phone,0,1)!="+" && isset($v["address"]) && isset($v["address"]["addressCountry"]) && isset($mapIndicTel[$v["address"]["addressCountry"]])) 
			   ? "+".$mapIndicTel[$v["address"]["addressCountry"]].substr($phone,1) : $phone );

			$res=$phone;
		}
		if(!empty($val["mobile"])){
			$res=$val["mobile"][0];

		}else if(!empty($val["fixe"])){
			$res=$val["fixe"][0];
		}
		// if(isset($val[]))
    	// foreach($val as $ind => $value){
        //     if(in_array($value, self::$compagnonList)){
        //     	array_push($res,$value);
        //     }
    	// }
		// $str=implode(" ; \n", array_unique($res));

    	return $res;
    }

	public static $dataBinding_organization_symply = array(
		array("valueOf"  	=> '_id.$id'),
		array("valueOf" => "name"),
		array("valueOf" => "geoPosition.coordinates.1"),
	    array("valueOf" => "geoPosition.coordinates.0"),
	   	array(10507,10513,10512),
	);

	public static $dataBinding_network = array(
		
		"@type"		=> "Organization",
		"typeSig"=>	"organizations",
		"typeElement"=>	"organization",
		"typeOrganization"=>	"NGO",
		"type"=>	"NGO",

	    "name" 		=> array("valueOf" => "name"),

	    "geo" 	=> array("parentKey"=>"geo", 
    					 "valueOf" => array(
								"latitude" 			=> array("valueOf" => "latitude"),
								"longitude" 			=> array("valueOf" => "longitude")
			 					)),

	    "geoPosition" 	=> array("parentKey"=>"geo", 
    					 "valueOf" => array(
								"latitude" 			=> array("valueOf" => "latitude"),
								"longitude" 			=> array("valueOf" => "longitude")
			 					)),

		"address" 	=> array("parentKey"=>"address", 
	    					 "valueOf" => array(
									"streetAddress" 	=> array("valueOf" => "streetAddress"),
									"postalCode" 		=> array("valueOf" => "postalCode"),
									"addressLocality"   => array("valueOf" => "addressLocality"),
									"addressCountry" 	=> array("valueOf" => "addressCountry"))),
	   	"shortDescription"		=> array("valueOf" => "description"),
	   	"description"		=> array("valueOf" => "descriptionMore"),
	   	"url" 		=> array("valueOf" => 'website'),
	   	"source" 		=> array(	"valueOf" => 'id',
			   						"type" 	=> "url", 
									"prefix"   => "http://presdecheznous.fr/annuaire#/fiche/pdcn/",
									"suffix"   => ""),
	   	//"email"		=> array("valueOf" => "email"),
	);
}
