<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\models;
use Element;
use Costum;
use MongoId;
use PHDB;
use Yii;

class Admin {

	
  public static function addSourceInElement($id = null, $type = null, $keySource=null, $setKey=null, $origin="costum"){
    $fields=[$setKey];
    $elt=Element::getElementById($id, $type, null, $fields);
    //print_r($elt);exit;
    if($setKey=="source")
      return array("result" => false, "msg" => Yii::t("common","You can't add existed element as sourceKey but you can reference it"));
    if(!empty($elt[$setKey]) && is_array($elt[$setKey])){
      $set=$elt[$setKey];
      if(@$set[$origin])
        array_push($set[$origin], $keySource);
      else
        $set[$origin]=[$keySource];
    }else{
      $set=array($origin=>array($keySource));
    }
    PHDB::update($type,
            array("_id" => new MongoId($id)),
            array('$set' => array($setKey=> $set))
        );
    return array("result" => true, "msg" => Yii::t("common","The element is well refering"));
  }
    public static function removeSourceFromElement($id = null, $type = null, $keySource=null, $setKey=null, $origin="costum"){
      $fields=[$setKey];
      $elt=Element::getElementById($id, $type, null, $fields);
      if(!empty($elt[$setKey])){
        if($setKey=="source"){ 
          if(count($elt[$setKey]["keys"])==1){
            $update=array('$unset'=>array($setKey=>1));
          }else{
            $getNewKey=false;
            if($elt[$setKey]["key"]==$keySource)
              $getNewKey=true;
            foreach($elt[$setKey]["keys"] as $k => $v){
              if($v==$keySource)
                unset($elt[$setKey]["keys"][$k]);
              else if($getNewKey)
                $getNewKey=$v;
            }
            if(!empty($getNewKey))
              $elt[$setKey]["key"]=$getNewKey;
            $update=array('$set'=>array($setKey=>$elt[$setKey]));
          }
          $msg="Element is well removed from sources";
        }else{
            if(count($elt[$setKey][$origin])==1){
              if(count($elt[$setKey]) == 1)
                $update=array('$unset'=>array($setKey=>1));
              else
                $update=array('$unset'=>array($setKey.".".$origin=>1));
          
            }else{
              foreach($elt[$setKey][$origin] as $k => $v){
                  if($v==$keySource)
                    unset($elt[$setKey][$origin][$k]);
              }
              $update=array('$set'=>array($setKey=>$elt[$setKey]));
            }
            if(Costum::isSameFunction("removeSourceFromElement")){
              $params = [
                "id" => $id,
                "type" => $type
              ];
              Costum::sameFunction("removeSourceFromElement", $params);
            }
          $msg= Yii::t("common","Element is well removed from reference");
        }
        PHDB::update($type,
          array("_id" => new MongoId($id)), $update);
        return array("result" => true, "msg" => Yii::t("common",$msg));
      }
  }
}