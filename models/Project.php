<?php 
class Project {

	const COLLECTION = "projects";
	const CONTROLLER = "project";
	const ICON = "fa-lightbulb-o";
	const COLOR = "#8C5AA1";
	
	//From Post/Form name to database field name
	public static $dataBinding = array(
	    "name" => array("name" => "name", "rules" => array("required")),
	    "slug" => array("name" => "slug", "rules" => array("checkSlug")),
	    "email" => array("name" => "email", "rules" => array("email")),
	    "collection" => array("name" => "collection"),
	    "address" => array("name" => "address", "rules" => array("addressValid")),
	    "addresses" => array("name" => "addresses"),
	    "streetAddress" => array("name" => "address.streetAddress"),
	    "postalCode" => array("name" => "address.postalCode"),
	    "city" => array("name" => "address.codeInsee"),
	    "public"  => array("name" => "public"),
	    "addressCountry" => array("name" => "address.addressCountry"),
	    "geo" => array("name" => "geo", "rules" => array("geoValid")),
	    "geoPosition" => array("name" => "geoPosition", "rules" => array("geoPositionValid")),
	    "description" => array("name" => "description"),
	    "shortDescription" => array("name" => "shortDescription"),
	    "startDate" => array("name" => "startDate" ),
	    "endDate" => array("name" => "endDate"),
	    "tags" => array("name" => "tags"),
	    "url" => array("name" => "url"),
	    "licence" => array("name" => "licence"),
	    "avancement" => array("name" => "properties.avancement"),
	    "state" => array("name" => "state"),
	    "warnings" => array("name" => "warnings"),
	    "modules" => array("name" => "modules"),
	    "badges" => array("name" => "badges"),
	    "category" => array("name" => "category"),
	    "source" => array("name" => "source"),
	    "preferences" => array("name" => "preferences"),
	    "medias" => array("name" => "medias"),
	    "urls" => array("name" => "urls"),
	    "type" => array("name" => "type"),
	    "contacts" => array("name" => "contacts"),
	    "parent" => array("name" => "parent"),
		"parentId" => array("name" => "parentId"),
		"parentType" => array("name" => "parentType"),
		"modified" => array("name" => "modified"),
	    "updated" => array("name" => "updated"),
	    "creator" => array("name" => "creator"),
	    "created" => array("name" => "created"),
	    "locality" => array("name" => "address"),
	    "descriptionHTML" => array("name" => "descriptionHTML"),
	    "facebook" => array("name" => "socialNetwork.facebook"),
	    "twitter" => array("name" => "socialNetwork.twitter"),
	    "gpplus" => array("name" => "socialNetwork.googleplus"),
	    "github" => array("name" => "socialNetwork.github"),
	    "gitlab" => array("name" => "socialNetwork.gitlab"),
	    "diaspora" => array("name" => "socialNetwork.diaspora"),
	    "mastodon" => array("name" => "socialNetwork.mastodon"),
        "signal" => array("name" => "socialNetwork.signal"),
        "telegram" => array("name" => "socialNetwork.telegram"),
	    "onepageEdition" => array("name" => "onepageEdition"),
	    "instagram" => array("name" => "socialNetwork.instagram"),
	    "scope" => array("name" => "scope"),

        "expected" => array("name" => "expected"),
        "costum" => array("name" => "costum"),

        "categ" => array("name" => "categ"),
        "subcateg" => array("name" => "subcateg"),
        "subsubcateg" => array("name" => "subsubcateg"),

        "timeZone" => array("name" => "timeZone"),

        "actionPrincipal" => array("name" => "actionPrincipal"),
        "cibleDDPrincipal" => array("name" => "cibleDDPrincipal"),
        "indicateur" => array("name" => "indicateur"),

        "innovativeProject" => array("name" => "innovativeProject"),
        "financialHelp" => array("name" => "financialHelp"),
        "prototype" => array("name" => "prototype"),
        "nameStructure" => array("name" => "nameStructure"),
        "siret" => array("name" => "siret"),

        "codeCARIF" => array("name" => "codeCARIF"),
        "objectives" => array("name" => "objectives"),
        "program" => array("name" => "program"),
        "program" => array("name" => "program"),
        "levels" => array("name" => "levels"),
        "urlsDoc" => array("name" => "urlsDoc"),
	    
	);

	public static $avancement = array(
		"" => "Not specified",
        "idea" => "idea",
        "concept" => "concept",
        "started" => "started",
        "development" => "development",
        "testing" => "testing",
        "mature" => "mature",
		"finished" => "finished",
        "abandoned" => "abandoned",
		"production" => "En Production",
		"qa" => "Qualité Assurance"
	);

	private static function getCollectionFieldNameAndValidate($projectFieldName, $projectFieldValue, $projectId) {
		return DataValidator::getCollectionFieldNameAndValidate(self::$dataBinding, $projectFieldName, $projectFieldValue, $projectId);
	}

	/**
	 * get an project By Id
	 * @param type $id : is the mongoId of the project
	 * @return type
	 */
	public static function getById($id) {
	  	$project = PHDB::findOne( self::COLLECTION,array("_id"=>new MongoId($id)));
	  	if ($project !=null) {
		  	$project = self::formatDateRender($project);
		}

		if (!empty($project)) {
			$project = array_merge($project, Document::retrieveAllImagesUrl($id, self::COLLECTION, $project));
			$project["files"] = Document::getListDocumentsWhere(array("type"=>self::COLLECTION,"id"=>$id, "doctype"=>"file"),"file");
	  
			$project["typeSig"] = "projects";
			$project["type"] = self::COLLECTION;
		}else{
			$project = Element::getGhost(self::COLLECTION);
			//throw new CTKException("The element you are looking for has been moved or deleted");
		}
	  	return $project;
	}

	public static function getDataBinding() {
	  	return self::$dataBinding;
	}

	public static function  getByArrayId($arrayId, $fields = array(), $simply = false) {
	  	
	  	$projects = PHDB::find(self::COLLECTION, array( "_id" => array('$in' => $arrayId)), $fields);
	  	$res = array();
	  	foreach ($projects as $id => $project) {
	  		if (empty($project)) {
            //TODO Sylvain - Find a way to manage inconsistent data
            //throw new CommunecterException("The organization id ".$id." is unkown : contact your admin");
	        } else {

	        	if($simply)
	        		$project = self::getSimpleProjectById($id, $project);
	        	else{
	        		if ($project !=null) {
					  	if (!empty($project["startDate"]) || !empty($project["endDate"])) {
					  		$now = time();
					  		
					  		if(isset($project["startDate"])) {
								$yester2day = mktime(0, 0, 0, date("m")  , date("d")-2, date("Y"));
								if (gettype($project["startDate"]) == "object") {
									//Set TZ to UTC in order to be the same than Mongo
									date_default_timezone_set('UTC');
									if (!empty($project["startDate"]))
										$project["startDate"] = date('Y-m-d H:i:s', $project["startDate"]->sec);
								} else {
									$project["startDate"] = date('Y-m-d H:i:s',$yester2day);;
								}
							}

					  		if(isset($project["startDate"]) && isset($project["endDate"])) {
								$yesterday = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
								if (gettype($project["endDate"]) == "object") {
									date_default_timezone_set('UTC');
									if (!empty($project["endDate"]))
										$project["endDate"] = date('Y-m-d H:i:s', $project["endDate"]->sec);
								} else {
									$project["endDate"] = date('Y-m-d H:i:s', $yesterday);
								}
							}
						}
					}

					if (!empty($project)) {
						$project = array_merge($project, Document::retrieveAllImagesUrl($id, self::COLLECTION, $project));
						$project["typeSig"] = "projects";
					}
	        	}
	        	
	        }
	  		$res[$id] = $project;
	  	}
	  
	  	return $res;
	}

	/**
	 * Retrieve a simple project (id, name, profilImageUrl) by id from DB
	 * @param String $id of the project
	 * @return array with data id, name, profilImageUrl
	 */
	public static function getSimpleProjectById($id, $project=null) {
		
		$simpleProject = array();
		if(empty($project))
			$project = PHDB::findOneById( self::COLLECTION ,$id, array("id" => 1, "collection"=> 1, "name" => 1, "shortDescription" => 1, "description" => 1, "address" => 1, "geo" => 1, "tags" => 1, "links" => 1, "profilImageUrl" => 1, "profilThumbImageUrl" => 1, "profilMarkerImageUrl" => 1, "profilMediumImageUrl" => 1, "addresses"=>1) );
		
		if(!empty($project)){
			$simpleProject["id"] = $id;
			$simpleProject["name"] = @$project["name"];
			$simpleProject = array_merge($simpleProject, Document::retrieveAllImagesUrl($id, self::COLLECTION, $project));
			$simpleProject["address"] = empty($project["address"]) ? array("addressLocality" => Yii::t("common","Unknown Locality")) : $project["address"];
			$simpleProject["addresses"] = @$project["addresses"];
			$simpleProject["geo"] = @$project["geo"];
			$simpleProject["tags"] = @$project["tags"];
			$simpleProject["shortDescription"] = @$project["shortDescription"];
			$simpleProject["description"] = @$project["description"];
			$el = $project;
			if(@$el["links"]) foreach(array("contributors", "followers") as $key)
				if(@$el["links"][$key])
				$simpleProject["counts"][$key] = count($el["links"][$key]);
			
		}

		return $simpleProject;
	}
	
	//TODO SBAR => should be private ?
	public static function getWhere($params) {
	  	return PHDB::findAndSort( self::COLLECTION, $params, array("created"),null);
	}
	/**
	 * Get an project from an id and return filter data in order to return only public data
	 * @param type $id 
	 * @return project structure
	 */
	public static function getPublicData($id) {
		//Public datas 
		$publicData = array ();

		//TODO SBAR = filter data to retrieve only publi data	
		$project = Project::getById($id);
		$project["type"] = Project::COLLECTION;
		if (empty($project)) {
			//throw new CommunecterException("The project id is unknown ! Check your URL");
		}

		return $project;
	}



	public static function formatDateRender($project)
	{
		if (!empty($project["startDate"]) || !empty($project["endDate"])) {
			$now = time();

			if (isset($project["startDate"])) {
				//$project["startDateDB"] = $project["startDate"];
				$yester2day = mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"));

				if (gettype($project["startDate"]) == "array")
				$project["startDate"] = (object)$project["startDate"];

				if (gettype($project["startDate"]) == "object") {
					//Set TZ to UTC in order to be the same than Mongo
					date_default_timezone_set('UTC');
					if (!empty($project["startDate"])) {

						$project["startDate"] = date('Y-m-d H:i:s', $project["startDate"]->sec);
						//$project["startDateDB"] = $project["startDate"];

					}
				} else {
					$project["startDate"] = date('Y-m-d H:i:s', $yester2day);
				}
			}

			if (isset($project["startDate"]) && isset($project["endDate"])) {
				if (gettype($project["endDate"]) == "array")
				$project["endDate"] = (object)$project["endDate"];
				//$project["endDateDB"] = $project["endDate"];
				$yesterday = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));
				if (gettype($project["endDate"]) == "object") {
					date_default_timezone_set('UTC');
					if (!empty($project["endDate"])) {
						$project["endDate"] = date('Y-m-d H:i:s', $project["endDate"]->sec);
						//$project["endDateDB"] = $project["endDate"];
					}
				} else {
					$project["endDate"] = date('Y-m-d H:i:s', $yesterday);
				}
			}
		}
		return $project;
	}
   

}
?>