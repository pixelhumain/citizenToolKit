<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\matomo\MatomoTracker;
use Yii;
use yii\base\Widget;

class MatomoTrackerWidget extends Widget
{
	public $costum;
	public function init()
	{
		parent::init();
	}
	public function run()
	{
		$matomoConfig = $this->getMatomoConfig($this->costum);
		if ($matomoConfig != null) {
			$matomoTracker = new MatomoTracker($matomoConfig["matomoUrl"], $matomoConfig["tokenAuth"]);
			$mySiteId = $matomoTracker->getSiteId();
			if ($mySiteId != null) {
				return $this->render("costum.views.matomo.index", [
					"matomoUrl" => $matomoConfig["matomoUrl"],
					"siteId" => $matomoTracker->getSiteId()
				]);
			} else {
				return "";
			}
		} else return "";
	}
	public function isMatomoDevActive()
	{
		$matoco = Yii::app()->params['matomo'];
		if (isset($matoco) && isset($matoco['active']) && $matoco['active'] == true) return true;
		else false;
	}
	public function getDefaultConfigMatomo()
	{
		$matoco = Yii::app()->params['matomo'];
		if (isset($matoco) && $this->isMatomoDevActive() && isset($matoco['serverUrl']) && isset($matoco['tokenAuth'])) {
			return [
				"matomoUrl" => $matoco['serverUrl'],
				"tokenAuth" => $matoco['tokenAuth']
			];
		} else
			return null;
	}
	public function getMatomoConfig($costum)
	{

		if (isset($costum) && isset($costum['matomo'])) {
			$matomoConfig = $costum['matomo'];
			if ($matomoConfig['active'] == true) {
				if (isset($matomoConfig['server']['externe']) && $matomoConfig['server']['externe'] == true) {
					return [
						"matomoUrl" => $matomoConfig['server']['adresse'],
						"tokenAuth" => $matomoConfig['server']['token']
					];
				} else return $this->getDefaultConfigMatomo();
			} else return null;
		} else {
			return $this->getDefaultConfigMatomo();
		};
	}
}
