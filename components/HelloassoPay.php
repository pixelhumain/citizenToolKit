<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components;

use Exception;

class HelloassoPay {
	private string	$_access_token;
	private int		$_expires_in;
	private string	$_refresh_token;
	private string	$_token_type;
	private string	$_slug;
	private string	$_url;
	private int		$_amount;
	private string	$_item_name;
	private string	$_on_success;
	private string	$_on_error;
	private string	$_on_cancel;
	private bool	$_is_donation;
	private array	$_metadata;
	private array	$_payer;

	private function __construct(array $args = []) {
		foreach ($args as $name => $arg) {
			$private = "_$name";
			$this->$private = $arg;
		};
		$this->_url = "https://api.helloasso.com";
		if (strpos($_SERVER["SERVER_NAME"], "qa.communecter.org") !== false || defined("YII_DEBUG") && YII_DEBUG)
			$this->_url = "https://api.helloasso-sandbox.com";
	}

	/**
	 * @param string $arg
	 * @return HelloassoPay|string
	 */
	public function access_token(string $arg = null) {
		if ($arg === null)
			return ($this->_access_token ?? null);
		$this->_access_token = $arg;
		return ($this);
	}

	/**
	 * @param int $arg
	 * @return HelloassoPay|int
	 */
	public function expires_in(int $arg = -1) {
		if ($arg < 0)
			return ($this->_expires_in ?? -1);
		$this->_expires_in = $arg;
		return ($this);
	}

	/**
	 * @param string $arg
	 * @return HelloassoPay|string
	 */
	public function refresh_token(string $arg = null) {
		if ($arg === null)
			return ($this->_refresh_token ?? null);
		$this->_refresh_token = $arg;
		return ($this);
	}

	/**
	 * @param string $arg
	 * @return HelloassoPay|string
	 */
	public function token_type(string $arg = null) {
		if ($arg === null)
			return ($this->_token_type ?? null);
		$this->_token_type = $arg;
		return ($this);
	}

	/**
	 * @param string $arg
	 * @return HelloassoPay|string
	 */
	public function slug(string $arg = null) {
		if ($arg === null)
			return ($this->_slug ?? null);
		$this->_slug = $arg;
		return ($this);
	}

	/**
	 * @param string $arg
	 * @return HelloassoPay|string
	 */
	public function item_name(string $arg = null) {
		if ($arg === null)
			return ($this->_item_name ?? null);
		$this->_item_name = $arg;
		return ($this);
	}

	/**
	 * @param array $arg
	 * @return HelloassoPay|array
	 */
	public function metadata(array $arg = null) {
		if ($arg === null)
			return ($this->_metadata ?? []);
		$this->_metadata = $arg;
		return ($this);
	}

	/**
	 * @param array $arg
	 * @return HelloassoPay|array
	 */
	public function payer(array $arg = null) {
		if ($arg === null)
			return ($this->_payer ?? []);
		$this->_payer = $arg;
		return ($this);
	}

	/**
	 * @param bool $arg
	 * @return HelloassoPay|bool
	 */
	public function is_donation(bool $arg = null) {
		if ($arg === null)
			return ($this->_is_donation ?? false);
		$this->_is_donation = $arg;
		return ($this);
	}

	/**
	 * redirect the checkout page to this url when it's done
	 * @param string $arg
	 * @return HelloassoPay|string
	 */
	public function on_success(string $arg = null) {
		if ($arg === null)
			return ($this->_on_success ?? null);
		$this->_on_success = $arg;
		return ($this);
	}

	/**
	 * redirect the checkout page to this url when error occured
	 * @param string $arg
	 * @return HelloassoPay|string
	 */
	public function on_error(string $arg = null) {
		if ($arg === null)
			return ($this->_on_error ?? null);
		$this->_on_error = $arg;
		return ($this);
	}

	/**
	 * redirect the checkout page to this url if the customer abort the action
	 * @param string $arg
	 * @return HelloassoPay|string
	 */
	public function on_cancel(string $arg = null) {
		if ($arg === null)
			return ($this->_on_cancel ?? null);
		$this->_on_cancel = $arg;
		return ($this);
	}

	/**
	 * @param int $arg
	 * @return HelloassoPay|int
	 */
	public function amount(int $arg = -1) {
		if ($arg < 0)
			return ($this->_amount ?? -1);
		$this->_amount = $arg;
		return ($this);
	}

	private function do_refresh_token() {
		$ch = curl_init($this->_url . "/oauth2/token");
		$post = [
			"refresh_token"	=> $this->refresh_token(),
			"grant_type"	=> "refresh_token",
		];
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/x-www-form-urlencoded',
			"accept: application/json"
		]);
		$resp = curl_exec($ch);
		$err = curl_error($ch);
		curl_close($ch);
		if (!$err)
			$this->access_token($resp["access_token"])
				->expires_in(time() + $resp["expires_in"])
				->refresh_token($resp["refresh_token"])
				->token_type($resp["token_type"]);
	}

	public function init_checkout_intent() {
		if ($this->expires_in() < time())
			$this->do_refresh_token();
		if (!empty($this->slug())) {
			$ch = curl_init($this->_url . "/v5/organizations/" . $this->slug() . "/checkout-intents");
			$post = [
				"totalAmount"		=> $this->amount(),
				"initialAmount"		=> $this->amount(),
				"itemName"			=> $this->item_name(),
				"backUrl"			=> $this->on_cancel(),
				"errorUrl"			=> $this->on_error(),
				"returnUrl"			=> $this->on_success(),
				"containsDonation"	=> $this->is_donation(),
			];
			if (!empty($this->metadata()))
				$post["metadata"] = $this->metadata();
			if (!empty($this->payer()))
				$post["payer"] = $this->payer();
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, [
				"Content-Type: application/json",
				"Authorization: Bearer " . $this->access_token()
			]);
			$resp = curl_exec($ch);
			$err = curl_error($ch);
			$info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			$ret = [
				"success"	=> 1,
				"content"	=> json_decode($resp, 1),
			];
			if (!empty($err) || $info !== 200)
				$ret["success"] = 0;
			return ($ret);
		}
		return ([
			"success"	=> 0,
			"content"	=> "Empty organization slug"
		]);
	}

	public function get_checkout(int $id) {
		if ($this->expires_in() < time())
			$this->do_refresh_token();
		if (!empty($this->slug())) {
			$ch = curl_init($this->_url . "/v5/organizations/" . $this->slug() . "/checkout-intents/$id");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, [
				"Content-Type: application/json",
				"Authorization: Bearer " . $this->access_token()
			]);
			$resp = curl_exec($ch);
			$err = curl_error($ch);
			$info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			$ret = [
				"success"	=> 1,
				"content"	=> json_decode($resp, 1),
			];
			if (!empty($err) || $info !== 200)
				$ret["success"] = 0;
			return ($ret);
		}
		return ([
			"success"	=> 0,
			"content"	=> "Empty organization slug",
		]);
	}

	/**
	 * Undocumented function
	 *
	 * @param string $client_id Your Client Id
	 * @param string $client_secret Your Client Secret
	 * @param string $grant grant_type Defaults to client_credentials
	 * @return HelloassoPay
	 */
	public static function create(string $client_id, string $client_secret, string $grant = "client_credentials") {
		$url = "https://api.helloasso.com";
		if (strpos($_SERVER["SERVER_NAME"], "qa.communecter.org") !== false || defined("YII_DEBUG") && YII_DEBUG)
			$url = "https://api.helloasso-sandbox.com";
		$ch = curl_init("$url/oauth2/token");
		$post = [
			"client_id"		=> $client_id,
			"client_secret"	=> $client_secret,
			"grant_type"	=> $grant,
		];
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/x-www-form-urlencoded',
			"accept: application/json"
		]);
		$resp = curl_exec($ch);
		$err = curl_error($ch);
		$info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if ($err || $info !== 200)
			return null;
		$resp = json_decode($resp, 1);
		$ret = new self($resp);
		$ret->expires_in(time() + $ret->expires_in());
		return ($ret);
	}
}
