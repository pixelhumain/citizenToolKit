<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components;

use DateTime;
use DateTimeZone;
use MongoRegex;
use Yii;

class UtilsHelper
{
	public static function className(array $classList): string
	{
		$output = [];
		foreach ($classList as $classKey => $classValue)
		{
			if (is_string($classKey))
			{
				if (filter_var($classValue, FILTER_VALIDATE_BOOLEAN))
					$output[] = $classKey;
			}
			elseif (is_string($classValue))
				$output[] = $classValue;
		}
		return implode(' ', $output);
	}

	public static function timestampToLocalDateString(int $timestamp, array $options = ['getHours' => false]): string
	{
		$language = $options['local'] ?? Yii::app()->language;
		switch ($language)
		{
			case 'en':
				$day = date("j", $timestamp);
				$suffix = date("S", $timestamp);
				$suffix = '\\' . implode('\\', str_split($suffix));
				$output = date("F $day$suffix, Y", $timestamp);
				break;
			case 'de':
				$month = date("F", $timestamp);
				$month = Yii::t('month', $month);
				$month = '\\' . implode('\\', str_split($month));
				$output = date("j. $month Y", $timestamp);
				break;
			case 'es':
				$month = date("F", $timestamp);
				$month = Yii::t('month', $month);
				$month = '\\' . implode('\\', str_split($month));
				$de = '\\' . implode('\\', str_split('de'));
				$output = date("j $de $month $de Y", $timestamp);
				break;
			default:
				$month = date("F", $timestamp);
				$month = Yii::t('month', $month);
				$month = '\\' . implode('\\', str_split($month));
				$output = date("j $month Y", $timestamp);
				break;
		}
		if ($options['getHours']) $output .= date(' H:i', $timestamp);
		return $output;
	}

	public static function parse_values($__params, $excludes = [])
	{
		if (!empty($__params))
		{
			if (is_array($__params))
			{
				foreach ($__params as $params_index => $params_value)
				{
					if (!in_array($params_index, $excludes)) $__params[$params_index] = self::parse_values($params_value, $excludes);
				}
			}
			else if (is_string($__params))
			{
				if (preg_match('/^[0-9]+$/', str_replace(' ', '', $__params))) $__params = intval(str_replace(' ', '', $__params));
				else if (strtolower(str_replace(' ', '', $__params)) === 'true') $__params = true;
				else if (strtolower(str_replace(' ', '', $__params)) === 'false') $__params = false;
			}
		}
		return $__params;
	}

	public static function get_as_timestamp(array $__fields, array $__provider, $__default = null): int
	{
		foreach ($__fields as $field)
		{
			if (!empty($__provider[$field]))
			{
				$date = $__provider[$field];
				$exploded = explode('/', $date);
				if (!empty($exploded[2]) && count(explode(' ', $exploded[2])) > 1)
				{
					$reexplode = explode(' ', $exploded[2]);
					$exploded[2] = $reexplode[0];
					$exploded[] = $reexplode[1];
				}
				$date = in_array(count($exploded), [
					3, 4
				]) ? $exploded[2] . '-' . $exploded[1] . '-' . $exploded[0] . (count($exploded) === 4 ? ' ' . $exploded[3] : '') : $date;
				$date = !is_integer($date) ? (is_string($date) ? strtotime($date) : (is_array($date) && array_key_exists('sec', $date) ? intval($date['sec']) : $date->sec)) : $date;
				return $date > 0 ? $date : (is_integer($__default) ? $__default : strtotime($__default));
			}
		}
		if (!empty($__default))
		{
			$date = $__default;
			$exploded = explode('/', $date);
			$date = count($exploded) === 3 ? $exploded[2] . '-' . $exploded[1] . '-' . $exploded[0] : $date;
			return !is_integer($date) ? (is_string($date) ? strtotime($date) : (is_array($date) && array_key_exists('sec', $date) ? intval($date['sec']) : $date->sec)) : $date;
		}
		return 0;
	}

	public static function get_iso_date_from(array $__input = ['fields' => [], 'provider' => [], 'default' => '', 'timezone' => '']): string
	{
		if (empty($__input['fields']) || empty($__input['provider'])) return date('c');
		$datetime = new DateTime();
		if (!empty($__input['timestamp']) && is_integer($__input['timestamp'])) $datetime->setTimestamp($__input['timestamp']);
		else $datetime->setTimestamp(self::get_as_timestamp($__input['fields'], $__input['provider'], $__input['default']));

		if (!empty($__input['timezone'])) $datetime->setTimezone(new DateTimeZone($__input['timezone']));
		return $datetime->format('c');
	}

	public static function mongo_regex(string $__text): MongoRegex
	{
		$text = $__text;
		$specials = ['.', '+', '*', '?', '^', '$', '(', ')', '[', ']', '{', '}', '|', '\\'];
		foreach ($specials as $special) $text = str_replace($special, '\\' . $special, $text);
		return new MongoRegex("/$text/i");
	}

	public static function array_val_at_path(array &$array, string $path, string $separator = '.')
	{
		$exploded_path = explode($separator, $path);
		if (count($exploded_path) > 1 && !empty($array[$exploded_path[0]]))
		{
			$new_path = implode($separator, array_slice($exploded_path, 1));
			return (self::array_val_at_path($array[$exploded_path[0]], $new_path, $separator));
		}
		return ($array[$exploded_path[0]] ?? '');
	}

	public static function look_at_array_in_depth(array &$__array, array &$__collection, int $__depth = 0)
	{
		if (empty($__collection) || empty($__collection[$__depth]) || empty($__array[$__collection[$__depth]])) return '';
		else if ($__depth < count($__collection) - 1 && is_array($__array[$__collection[$__depth]])) return self::look_at_array_in_depth($__array[$__collection[$__depth]], $__collection, ++$__depth);
		else return $__array[$__collection[$__depth]];
	}

	public static function push_array_if_not_exists($__value, array &$__target): void
	{
		if (is_numeric($__value) || is_string($__value))
		{
			if (!in_array($__value, $__target)) $__target[] = $__value;
		}
		else if (is_array($__value))
		{
			$exists = false;
			foreach ($__target as $target)
			{
				if (is_array($target) && $target === $__value)
				{
					$exists = true;
					break;
				}
			}
			if (!$exists) $__target[] = $__value;
		}
	}

	public static function social_to_string(array &$__social_network = []): string
	{
		$output = '';
		$regexes = [
			'instagram' => 'Instagram',
			'facebook'  => 'Facebook',
			'github'    => 'Github',
			'linkedin'  => 'LinkedIn',
			'twitter'   => 'Twitter',
			'movilab'   => 'Movilab',
		];
		$predicates = [
			'Instagram' => '',
			'Facebook'  => '',
			'Github'    => '',
			'LinkedIn'  => '',
			'Movilab'   => '',
		];
		foreach ($__social_network as $platforms)
		{
			if (is_array($platforms))
			{
				if (!empty($platforms['url']))
				{
					$urls = $platforms['url'];
					if (is_string($urls))
					{
						foreach ($regexes as $regex => $predicate)
						{
							if (preg_match("/$regex/", $urls)) $predicates[$predicate] = urldecode($urls);
						}
					}
					else if (is_array($urls))
					{
						foreach ($urls as $url)
						{
							foreach ($regexes as $regex => $predicate)
							{
								if (preg_match("/$regex/", $url)) $predicates[$predicate] = urldecode($url);
							}
						}
					}
				}
			}
		}

		foreach ($predicates as $name => $address)
		{
			if (!empty($address)) $output .= "$name : $address ;\n";
		}
		$output = substr($output, 0, -3);
		return $output;
	}

	public static function array_to_string(array $__input, int $depth = 0): string
	{
		$tab = str_repeat("    ", $depth);
		$output = $tab . "[\n";
		foreach ($__input as $key => $input)
		{
			if (is_array($input)) $output .= self::array_to_string($input, $depth + 1);
			else if (is_string($key))
			{
				if (is_string($input)) $output .= "$tab$tab\"$key\" => \"$input\",\n";
				else if (is_numeric($input)) $output .= "$tab$tab\"$key\" => $input,\n";
			}
			else $output .= "$tab$tab\"$input\",\n";
		}
		if (strrpos($output, ",\n") === strlen($output) - 1) $output = substr($output, 0, -3);
		return $output . "\n$tab]";
	}

	public static function in_associative_array(string $key, $value, array &$arrays): bool
	{
		foreach ($arrays as $array)
		{
			if (array_key_exists($key, $array) && $array[$key] === $value) return true;
		}
		return false;
	}

	public static function html_br_for_at(string $__input, int $__breakpoint, bool $__is_strict = true, string $__break_tag = '<br>'): string
	{
		$length = strlen($__input);
		$output = '';
		$counter = 0;
		for ($index = 0; $index < $length; $index++, $counter++)
		{
			$character = $__input[$index];
			if ($counter >= $__breakpoint && ($__is_strict || in_array($character, [' ', '-'])))
			{
				$output .= $__break_tag;
				$counter = 0;
			}
			$output .= $character;
		}
		return $output;
	}

	public static function array_first(array &$__array)
	{
		if (count($__array))
		{
			$array_keys = array_keys($__array);
			return $__array[$array_keys[0]];
		}
		return null;
	}

	public static function array_sort_by(array &$haystack, $comparison)
	{
		$length = count($haystack);
		do
		{
			$sort = 0;
			for ($i = 0; $i < $length - 1; $i++)
			{
				if ($comparison($haystack[$i], $haystack[$i + 1]))
				{
					$tmp = $haystack[$i];
					$haystack[$i] = $haystack[$i + 1];
					$haystack[$i + 1] = $tmp;
					$sort = 1;
				}
			}
		} while ($sort);
		return ($haystack);
	}

	public static function is_valid_mongoid($id): bool
	{
		return (is_string($id) && preg_match("/^[0-9a-z]{24}$/", $id));
	}

	public static function timezone_from_offset(int $offset): DateTimeZone
	{
		return (new DateTimeZone((timezone_name_from_abbr('', $offset * 60, 0))));
	}
}
