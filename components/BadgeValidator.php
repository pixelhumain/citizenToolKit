<?php

class BadgeValidator {
    public static function checkData($data, $profile)
    {
        $response = self::check($data, NULL, $profile);
         $response = self::analyseResponse($response);
        return $response;
    }
    public static function checkImage($image, $profile)
    {
        $response = self::check(NULL, $image, $profile);
        self::analyseResponse($response);
        return $response;
    }
    private static function check($data, $image, $profile)
    {
        $curl = curl_init();
        $url = Yii::app()->params['openbadge']['url']. '/results';
        $post_data = '';
        $json_profile = '{"email":"'.$profile.'"}';
        $boundary = uniqid();
        $delimiter = '------WebKitFormBoundary' . $boundary;
        $eol = "\r\n";

        if(isset($data)){
            $post_data .= $delimiter . $eol
                . 'Content-Disposition: form-data; name="data"' . $eol 
                . $eol
                . $data . $eol;
        }else if(isset($image)){
            $post_data .= $delimiter . $eol
                . 'Content-Disposition: form-data; name="image"; filename="'. $image['name'] .'"' . $eol
                . 'Content-Type: application/octet-stream' . $eol
                . $eol
                . file_get_contents($image['tmp_name']) . $eol;
        }else{
            throw new OpenBadgeException("Error data and image null");
        }
        if(!isset($data)){
            $post_data .= $delimiter . $eol
                . 'Content-Disposition: form-data; name="data"' . $eol 
                . $eol
                . $eol
                . $eol;
        }
        if(!isset($image)){
            $post_data .= $delimiter . $eol
                . 'Content-Disposition: form-data; name="image"; filename=""' . $eol
                . 'Content-Type: application/octet-stream' . $eol
                . $eol
                . $eol;
        }

        $post_data .= $delimiter . $eol
                . 'Content-Disposition: form-data; name="profile"' . $eol
                . $eol
                . $json_profile . $eol;

        $post_data .= $delimiter . "--" . $eol;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 120,
            //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data; boundary=----WebKitFormBoundary" . $boundary,
                "accept: application/json"
            ),
        ));
        //
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    private static function analyseResponse($response)
    {
        try{

            $response = json_decode($response, true);
            if(!$response['report']['valid']){
                $text = '';
                foreach ($response['report']['messages'] as $message) {
                    $text .= $message['result'];
                }
                throw new OpenBadgeException($text);
            }
        }catch(\Throwable $th){
            throw new OpenBadgeException("Invalid badge");
        }
        return $response;
    }
}