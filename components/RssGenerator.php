<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components;

use PHDB;

class RssGenerator {
    public static function generateRss($id, $rssParametre, $urlRss, $result) {
        $rssItems = [];
        foreach (explode(",", $rssParametre) as $value) {
            switch ($value) {
                case 'organizations':
                    $rssItems = array_merge($rssItems, self::processOrganizations($id, $urlRss, $result));
                    break;
                case 'projects':
                    $rssItems = array_merge($rssItems, self::processProjects($id, $urlRss, $result));
                    break;
                case 'events':
                    $rssItems = array_merge($rssItems, self::processEvents($id, $urlRss, $result));
                    break;
                case 'classifieds':
                    $rssItems = array_merge($rssItems, self::processClassifieds($id, $urlRss, $result));
                    break;
                case 'poi':
                    $rssItems = array_merge($rssItems, self::processPoi($id, $urlRss, $result));
                    break;
                case 'news':
                    $rssItems = array_merge($rssItems, self::processNews($id, $urlRss, $result));
                    break;
            }
        }

        return self::formatRssOutput($rssItems, $result, $urlRss);
    }

    private static function processOrganizations($id, $urlRss, $result) {
        $rssItems = [];
        $organizations = PHDB::findAndSortAndLimitAndIndex('organizations', ["creator" => $id], ['_id' => -1], 1);
        if (!empty($organizations)) {
            $org = reset($organizations);
            $description = self::generateDescription($org, $urlRss);
            $rssItems[] = self::createRssItem($org["name"], $result[0]["url"]["communecter"] . '.view.directory.dir.projects', $org['_id'], $org["created"], $description);
        }
        return $rssItems;
    }

    private static function processProjects($id, $urlRss, $result) {
        $rssItems = [];
        $projects = PHDB::findAndSortAndLimitAndIndex('projects', ["links.contributors.$id" => ['$exists' => true]], ['_id' => -1], 1);
        if (!empty($projects)) {
            $project = reset($projects);
            $description = self::generateDescription($project, $urlRss);
            $rssItems[] = self::createRssItem(
                $project["name"],
                $result[0]["url"]["communecter"] . '.view.directory.dir.projects',
                $project['_id'],
                $project["created"],
                $description
            );
        }
        return $rssItems;
    }
    

    private static function processEvents($id, $urlRss, $result) {
        $rssItems = [];
        $events = PHDB::findAndSortAndLimitAndIndex('events', ["creator" => $id], ['_id' => -1], 1);
        if (!empty($events)) {
            $event = reset($events);
            $description = self::generateDescription($event, $urlRss);
            $rssItems[] = self::createRssItem(
                $event["name"] . " - Événements",
                $result[0]["url"]["communecter"] . '.view.agenda',
                $event['_id'],
                $event["created"],
                $description
            );
        }
        return $rssItems;
    }
    

    private static function processClassifieds($id, $urlRss, $result) {
        $rssItems = [];
        $classifieds = PHDB::findAndSortAndLimitAndIndex('classifieds', ["creator" => $id], ['_id' => -1], 1);
        if (!empty($classifieds)) {
            $classified = reset($classifieds);
            $description = self::generateDescription($classified, $urlRss);
            $rssItems[] = self::createRssItem(
                $classified["name"],
                $result[0]["url"]["communecter"] . '.view.directory.dir.classifieds',
                $classified['_id'],
                $classified["created"],
                $description
            );
        }
        return $rssItems;
    }
    

    private static function processPoi($id, $urlRss, $result) {
        $rssItems = [];
        $pois = PHDB::findAndSortAndLimitAndIndex('poi', ["creator" => $id], ['_id' => -1], 1);
        if (!empty($pois)) {
            $poi = reset($pois);
            $description = self::generateDescription($poi, $urlRss);
            $rssItems[] = self::createRssItem(
                $poi["name"] . " - " . $poi["type"],
                $result[0]["url"]["communecter"],
                $poi['_id'],
                $poi["created"],
                $description
            );
        }
        return $rssItems;
    }
    

    private static function processNews($id, $urlRss, $result) {
        $rssItems = [];
        $news = PHDB::findAndSortAndLimitAndIndex('news', ["target.id" => $id], ['_id' => -1], 1);
        if (!empty($news)) {
            foreach ($news as $item) {
                $author = PHDB::findOneById('citoyens', strval($item["author"]));
                $description = $item["text"] ?? '';
                $rssItems[] = self::createRssItem(
                    $author["name"] . " a publié un message",
                    $result[0]["url"]["communecter"] . '.view.newspaper',
                    $item['_id'],
                    $item["created"]->sec,
                    $description
                );
            }
        }
        return $rssItems;
    }
    

    private static function generateDescription($entity, $urlRss) {
        $description = '';
        if (isset($entity['profilImageUrl'])) {
            $description .= '<img class="center" src="' . $urlRss . '/' . $entity['profilImageUrl'] . '" />';
        }
        if (isset($entity['shortDescription'])) {
            $description .= $entity['shortDescription'];
        }
        return $description;
    }

    private static function createRssItem($title, $link, $guid, $pubDate, $description) {
        $formattedDate = gmdate("D, d M Y H:i:s \G\M\T", $pubDate);
        return '
            <item>
                <title>' . htmlspecialchars($title) . '</title>
                <link>'.$link.'</link>
                <guid isPermaLink="false">' . htmlspecialchars($guid) . '</guid>
                <pubDate>' . $formattedDate . '</pubDate>
                <description><![CDATA[' . $description . ']]></description>
            </item>';
    }

    private static function formatRssOutput($rssItems, $result, $urlRss) {
        $xmlContent = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xmlContent .= '
            <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
            <channel>
                <title>' . htmlspecialchars($result[0]["name"]) . '</title>
                <link>'.$urlRss.'</link>
                <description>' . htmlspecialchars($result[0]["shortDescription"] ?? '') . '</description>'
                . implode('', $rssItems) . '
            </channel>
            </rss>';
        ob_end_clean();
    
        return $xmlContent;
    }
    
}
