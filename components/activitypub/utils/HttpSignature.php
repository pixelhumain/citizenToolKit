<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils;

use DateTime;
use DateTimeZone;

class HttpSignature{
    public static function sign($target, $data, $keyId){
        $targetDomain = parse_url($target, PHP_URL_HOST);
        $targetPath = parse_url($target, PHP_URL_PATH);

        //get current date
        $utcDate =  new DateTime("now", new DateTimeZone("UTC"));
        $utcDateString = $utcDate->format(DateTime::RFC7231);

        //digest data
        $digest = base64_encode(hash("sha256", json_encode($data), true));

        //define content of signature
        $stringToSign = "(request-target): post $targetPath\nhost: $targetDomain\ndate: $utcDateString\ndigest: SHA-256=$digest";
        openssl_sign($stringToSign, $signature, Utils::getPem("private"), "sha256");
        $signature_b64 = base64_encode($signature);

        //define the header signature
        $headerSignature = 'keyId="'.$keyId.'",headers="(request-target) host date digest",signature="'.$signature_b64.'"';

        return [
            "Host: ".$targetDomain,
            "Date: ".$utcDateString,
            "Digest: SHA-256=".$digest,
            "Signature: ".$headerSignature,
            "Content-Type: ".Request::HEADER_ACCEPT
        ];
    }
}