<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils;

use CacheHelper;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubCron;

class Request{
    const HEADER_ACCEPT = 'application/activity+json';

    public static function post($sender, $receiver, $data, $addToCron=false){
        if((filter_var($receiver, FILTER_VALIDATE_URL) && !self::pingUrl($receiver)) || $receiver == null) return false;
        if($addToCron){
            ActivitypubCron::save($sender, $receiver, $data);
        }else{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $receiver);
            curl_setopt($ch, CURLOPT_HTTPHEADER, HttpSignature::sign($receiver, $data, $sender));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $error = curl_error($ch);
            curl_close($ch);
            if (!empty($error)) {

                return false;
            } else {
                return ($httpcode >= 200 && $httpcode <= 299);
            }
        }
    }
    public static function get($url, $accept=self::HEADER_ACCEPT){
        if((filter_var($url, FILTER_VALIDATE_URL) && !self::pingUrl($url)) || $url == null) return null;
        $isLocalUrl = parse_url($url, PHP_URL_HOST) == Config::HOST();

        if($output = CacheHelper::get($url))
            return $output;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Accept: ".$accept]);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        if(!$output || empty($output))
            return null;
        $output = Utils::decodeJson($output);
        if(!$isLocalUrl)
            CacheHelper::set($url, $output);
        return $output;
    }
    private static function pingUrl($url){
        $host = parse_url($url, PHP_URL_HOST);
        $port = parse_url($url, PHP_URL_PORT) ?: 80;
        $timeout = 10; // seconds

        $fp = @fsockopen($host, $port, $errno, $errstr, $timeout);

        if ($fp) {
            fclose($fp);
            return true;
        } else {
            return false;
        }
    }
}
