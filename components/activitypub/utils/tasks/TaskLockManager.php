<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\tasks;

use Yii;
use PHDB;

class TaskLockManager
{
    private $collectionName = 'activitypub_task_locks';

    public function acquireLock($taskId)
    {
        $lockData = [
            '_id' => $taskId,
            'locked_at' => new \MongoDB\BSON\UTCDateTime(),
            'status' => 'locked'
        ];

        try {
            $result = PHDB::update(
                $this->collectionName,
                ['_id' => $taskId],
                ['$set' => $lockData],
                ['upsert' => true]
            );

            return true;
        } catch (\Exception $e) {
            var_dump("Failed to acquire lock: " . $e->getMessage(), __METHOD__);
            return false;
        }
    }

    public function releaseLock($taskId)
    {
        try {
            $result = PHDB::remove($this->collectionName, ['_id' => $taskId]);

            return $result->getDeletedCount() > 0;
        } catch (\Exception $e) {
            Yii::error("Failed to release lock: " . $e->getMessage(), __METHOD__);
            return false;
        }
    }

    public function isLocked($taskId)
    {
        try {
            $document = PHDB::findOne($this->collectionName, ['_id' => $taskId]);

            return $document && $document['status'] === 'locked';
        } catch (\Exception $e) {
            Yii::error("Failed to check lock status: " . $e->getMessage(), __METHOD__);
            return false;
        }
    }
}
