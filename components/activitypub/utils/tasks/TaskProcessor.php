<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\tasks;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;

class TaskProcessor
{
    private $taskQueue;
    private $callback;
    private $lockManager;

    public function __construct(TaskQueue $taskQueue, callable $callback = null)
    {
        $this->taskQueue = $taskQueue;
        $this->callback = $callback;
        $this->lockManager = new TaskLockManager();
    }

    public function isHasCompleteTasks()
    {
        $tasks = $this->taskQueue->getTasks();
        return count($tasks) > 0;
    }

    public function processAllTasks()
    {
        $tasks = $this->taskQueue->getTasks();

        foreach ($tasks as $task) {
            $taskId = $task['_id'];
           // if ($this->lockManager->acquireLock($taskId)) {
                try {
                    $this->processTask($task);
                } finally {
               //     $this->lockManager->releaseLock($taskId);
                }
            /**} else {
                var_dump("Failed to acquire lock for task $taskId");
            }**/
        }

        if ($this->callback) {
            call_user_func($this->callback);
        }
    }

    public function processTask($task)
    {
        try {
            $methodName = 'process' . ucfirst($task['type']);
            if (method_exists($this, $methodName)) {
                $this->$methodName($task);
            } else {
                throw new \Exception("Type de tâche inconnu : " . $task['type']);
            }
        } catch (\Exception $e) {
            error_log("Erreur lors du traitement de la tâche : " . $e->getMessage());
        } finally {
            $this->taskQueue->removeTask($task['_id']);
        }
    }

    public function processCreateNoteActivity($task)
    {
       Handler::handle(ActivitypubActivityCreator::createNoteActivity($task['data'],(string)$task['user']), ["newsId"=>$task['newsId']]);
    }
}
