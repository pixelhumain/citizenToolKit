<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\tasks;
use Yii;
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\tasks;
use PHDB;

class TaskQueue
{
    private $collectionName;
    public function __construct($collectionName = 'activitypub_tasks')
    {
        $this->collectionName = $collectionName;
    }

    public function addTask(array $taskData)
    {
        $taskData['timestamp'] = time();
        try {
            PHDB::insert($this->collectionName, $taskData);
        } catch (\Exception $e) {
            Yii::error("Failed to add task: " . $e->getMessage(), __METHOD__);
        }
    }
    public function getTasks()
    {
        try {
            return PHDB::find($this->collectionName);
        } catch (\Exception $e) {
            Yii::error("Failed to retrieve tasks: " . $e->getMessage(), __METHOD__);
            return [];
        }
    }
    public function removeTask($taskId)
    {
        try {
            PHDB::remove($this->collectionName, ['_id' => $taskId]);
        } catch (\Exception $e) {
            Yii::error("Failed to remove task: " . $e->getMessage(), __METHOD__);
        }
    }
}


