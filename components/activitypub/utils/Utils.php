<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils;

use CTKException;
use DataValidator;
use DateTime;
use DateTimeZone;
use Exception;
use MongoDate;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\AbstractObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\news\models\UrlExtractor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use Preference;
use Organization;
use Person;
use Slug;
use Yii;
use PHDB;
use Event;
use MongoId;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;
use Project;

/**
 *
 */
class Utils
{
    const PUBLIC_INBOX = "https://www.w3.org/ns/activitystreams#Public";
    const URL_REGEX = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|(([^\s()<>]+|(([^\s()<>]+)))*))+(?:(([^\s()<>]+|(([^\s()<>]+)))*)|[^\s`!()[]{};:'\".,<>?«»“”‘’]))/";
    public static function createUrl($endpoint)
    {
        return Config::SCHEMA() . "://" . Config::HOST() . "/" . $endpoint;
    }

    public static function decodeJson($value)
    {
        $json = json_decode($value, true);

        if ($error = json_last_error() !== JSON_ERROR_NONE)
            throw new Exception(json_encode(["code" => $error, "message" => 'JSON decoding failed for string.', "value" => $value]));

        return $json;
    }

    public static function getPem($type)
    {
        if ($type == "private")
            return file_get_contents(Yii::getAlias('@app') . "/config/certificate/key.pem");
        else if ($type == "public")
            return file_get_contents(Yii::getAlias('@app') . "/config/certificate/public.pem");
        else
            return null;
    }

    public static function getCurrentUTCDate()
    {
        $utcDate =  new DateTime("now", new DateTimeZone("UTC"));
        return $utcDate->format(DateTime::RFC7231);
    }

    public static function retrieveAllIdsFromObjects(array $objects)
    {
        $ids = [];
        foreach ($objects as $object) {
            if ($object instanceof AbstractObject && ($id = $object->get("id")))
                $ids[] = $id;
        }
        return $ids;
    }
    public static function isTextHasUrl($str)
    {
        $match = preg_match_all(self::URL_REGEX, $str);
        return $match > 0;
    }
    public static function getWebsiteFavicon($url)
    {
        $elems = parse_url($url);
        $url = $elems['scheme'] . '://' . $elems['host'] . '/favicon.ico';
        return $url;
    }
    public static function getWebsiteBaseUrl($url)
    {
        $result = parse_url($url);
        return $result['scheme'] . "://" . $result['host'];
    }
    public static function getWebsiteDomain($url)
    {
        $result = parse_url($url);
        return $result['host'];
    }
    public static function warpUrlIntoLinkTag($str)
    {
        $text = preg_replace_callback(
            self::URL_REGEX,
            function ($v) {
                return '<a href="' . $v[0] . '" target="_blank">' . $v[0] . '</a>';
            },
            explode(" ", $str)
        );
        return implode(" ", $text);
    }
    public static function stringDateToMongoDate($myDate, $label)
    {
        $result = DataValidator::getDateTimeFromString($myDate, $label);
        return new MongoDate($result->getTimestamp());
    }

    public static function substrwords($text, $maxchar, $end = '...')
    {
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);
            $output = '';
            $i      = 0;
            while (1) {
                $length = strlen($output) + strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                } else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        } else {
            $output = $text;
        }
        return strip_tags($output);
    }
    public static function get_image_mime_type($image_path)
    {
        $mimes  = array(
            IMAGETYPE_GIF => "image/gif",
            IMAGETYPE_JPEG => "image/jpg",
            IMAGETYPE_PNG => "image/png",
            IMAGETYPE_BMP => "image/bmp"
        );

        if (($image_type = exif_imagetype($image_path))
            && (array_key_exists($image_type, $mimes))
        ) {
            return $mimes[$image_type];
        } else {
            return $mimes[IMAGETYPE_JPEG];
        }
    }
    public static function genUuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }
    public static function parseToEvent($object, $uuid)
    {

        $date = new MongoDate(time());
        $data = [
            "collection" => "events",
            "name" => $object->get('name'),
            "created" => strtotime($object->get('published')),
            "updated" => strtotime($object->get('updated')),
            "startDate" => self::stringDateToMongoDate($object->get("startTime"), 'startTime'),
            "endDate" => self::stringDateToMongoDate($object->get("endTime"), 'endTime'),
            "public" => true,
            "fromActivityPub" => true,
            "attributedTo" => $object->get("attributedTo"),
            "objectId" => $object->get("id")
        ];

        if (@$uuid) {
            $data = array_merge($data, array('objectUUID' => $uuid));
        }
        if ($object->get('_extend_props')) {
            $objectExtendProps = $object->get('_extend_props');
            if ($objectExtendProps['category']) {
                $data = array_merge($data, array('type' => ucfirst(strtolower($objectExtendProps['category']))));
            }
            if ($objectExtendProps['timezone']) {
                $data = array_merge($data, array('timeZone' => $objectExtendProps['timezone']));
            }
        }
        if (@$object->get('location') && $object->get('location') != null) {
            $location  = $object->get('location');
            $locationExtendProps = $location->get('_extend_props');
            $addressExtendProps = $locationExtendProps['address']->get('_extend_props');

            if (@$addressExtendProps && $addressExtendProps != null) {
                $geo = array(
                    "@type" => "GeoCoordinates",
                    "latitude" => $location->get('latitude'),
                    "longitude" => $location->get('longitude')
                );
                $geoPosition = array(
                    "type" => $location->get('type'),
                    "coordinates" => array(
                        $location->get('longitude'),
                        $location->get('latitude')
                    )
                );

                $data = array_merge($data, array("address" =>
                array(
                    '@type' => $addressExtendProps['type'],
                    'streetAddress' => $location->get('name') . " " . @$addressExtendProps['addressRegion'] && $addressExtendProps['addressRegion'] != null  ?  @$addressExtendProps['addressLocality'] &&  $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] . " " . $addressExtendProps['addressRegion'] : $addressExtendProps['addressRegion'] : "", 'postalCode' => @$addressExtendProps['postalCode'] && $addressExtendProps['postalCode'] != null ? $addressExtendProps['postalCode'] : ""
                ), 'addressLocality' => @$addressExtendProps['addressLocality'] &&  $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] : "", "geo" => $geo));
            }
        }
        return $data;
    }
    public static function parseToProject($object, $uuid)
    {
        $data = [
            "collection" => "projects",
            "name" => $object->get('name'),
            "slug" => Slug::checkAndCreateSlug($object->get('name')),
            "created" => strtotime($object->get('published')),
            "updated" => strtotime($object->get('updated')),
            "public" => true,
            "fromActivityPub" => true,
            "attributedTo" => $object->get("attributedTo"),
            "objectId" => $object->get("id")
        ];

        if (!empty($object->get('startTime'))) {
            $data["startDate"] = $object->get('startTime');
        }

        if (!empty($object->get('endTime'))) {
            $data["endDate"] = $object->get('endTime');
        }
        if (!empty($object->get('content'))) {
            $data["description"] = $object->get('content');
        }
        if (!empty($object->get('progress'))) {
            $data["properties"] = array('avancement' => $object->get('progress'));
        }

        if (!empty($object->get('tag'))) {
            $data["tags"] = $object->get('tag');
        }
        if (!empty($object->get('email'))) {
            $data["email"] = $object->get('email');
        }

        if (@$object->get('tag') && $object->get('tag') != null) {
            $tags = $object->get('tag');
            $tagList = [];
            foreach ($tags as $tagData) {
                $extendTagProps = $tagData->get('_extend_props');
                array_push($tagList, substr($extendTagProps['name'], 1));
            }
            $data = array_merge($data, array('tags' => $tagList));
        }

        if (@$object->get('attachment') && $object->get('attachment') != null) {
            $attachements = $object->get('attachment');
            foreach ($attachements as $attachement) {
                $extendAttachementsProps = $attachement->get('_extend_props');
                if ($extendAttachementsProps == null) {
                    if ($attachement->get('name') == 'Banner') {
                        if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                            $urlImg =  $attachement->get('url');
                            $data = array_merge($data, array('profilBannerUrl' =>  $urlImg, 'profilRealUrl' => $urlImg));
                        }
                    } else {
                        if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                            $urlImg =  $attachement->get('url');
                            $data = array_merge($data, array('profilImageUrl' =>  $urlImg, 'profilMediumImageUrl' => $urlImg));
                        }
                    }
                }
            }
        }

        $location = $object->get('location');
        if (isset($location) && $location != null) {
            $location  = $object->get('location');
            $locationExtendProps = $location->get('_extend_props');
            $addressExtendProps = $locationExtendProps['address']->get('_extend_props');

            if (isset($addressExtendProps) && $addressExtendProps != null) {
                $geo = array(
                    "@type" => "GeoCoordinates",
                    "latitude" => $location->get('latitude'),
                    "longitude" => $location->get('longitude')
                );
                $geoPosition = array(
                    "type" => $location->get('type'),
                    "coordinates" => array(
                        $location->get('longitude'),
                        $location->get('latitude')
                    )
                );

                $data = array_merge($data, array("address" =>
                array(
                    '@type' => $addressExtendProps['type'],
                    'streetAddress' => $location->get('name') . " " .  @$addressExtendProps['addressRegion'] && $addressExtendProps['addressRegion'] != null  ?  @$addressExtendProps['addressLocality'] &&  $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressRegion'] . " " . $addressExtendProps['addressLocality'] : $addressExtendProps['addressRegion'] : "", 'postalCode' => @$addressExtendProps['postalCode'] && $addressExtendProps['postalCode'] != null ? $addressExtendProps['postalCode'] : ""
                ), 'addressLocality' => @$addressExtendProps['addressLocality'] &&  $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] : ""));
            }
            $data = array_merge($data, array('geo' => $geo));
            //$data = array_merge($data,array('geoPosition' => $geoPosition));
        }
        return $data;
    }
    public static function getCCRecipients($elementType, $linkType,  AbstractActor $subject,  AbstractActor $invitor)
    {
        $element = PHDB::findOne($elementType, [
            "objectId" => $subject->get("id")
        ]);
        if (!$element)
            throw new Exception("Link not found");
        $links = [];
        if (is_string($linkType)) {
            foreach ($element["links"]["activitypub"][$linkType] as $el) {
                if ($el["invitorId"] == $invitor) {
                    $links[] = $el;
                    break;
                }
            }
        } else {
            if (count($linkType)) {
                foreach ($linkType as $lt) {
                    foreach ($element["links"]["activitypub"][$linkType] as $el) {
                        if ($el["invitorId"] == $invitor) {
                            $links[] = $el;
                            break;
                        }
                    }
                }
            }
        }
        return $links;
    }
    public static function activitypubObjectToEvent($objectUUID)
    {
        $connectedUserId = Yii::app()->session["userId"];
        $connectedUser = Person::getById($connectedUserId);
        $actorConnected = ActivitypubTranslator::coPersonToActor($connectedUser);
        $activitypubEnable = Preference::isActivitypubActivate(@$connectedUser["preferences"]);
        $data = [];


        if ($activitypubEnable) {
            $object = ActivitypubObject::getObjectByUUID($objectUUID);
            if ($object) {
                $organizer = Type::createFromAnyValue($object->get("attributedTo"));
                $res = PHDB::findOne(ActivitypubObject::COLLECTION, ["uuid" => $objectUUID]);
                $date = new MongoDate(time());
                $participant = @$res['payload'] && is_array($res['payload']) ? $res['payload'] : array();
                $data = [
                    "collection" => "events",
                    "name" => $object->get('name'),
                    "url" => $object->get('id'),
                    "created" => strtotime($object->get('published')),
                    "updated" => strtotime($object->get('updated')),
                    "shortDescription" => $object->get("content") != "" ? self::substrwords($object->get("content"), 140) : "",
                    "providerInfo" => [
                        "name" => self::getWebsiteDomain($object->get('id')),
                        "icon" => self::getWebsiteFavicon($object->get('id')),
                        "url" => self::getWebsiteBaseUrl($object->get('id'))
                    ],
                    "slug" => Slug::checkAndCreateSlug($object->get('name')),
                    "startDate" => self::stringDateToMongoDate($object->get("startTime"), 'startTime'),
                    "endDate" =>  self::stringDateToMongoDate($object->get("endTime"), 'endTime'),
                    "description" => $object->get("content"),
                    "objectId" => $object->get("id"),
                    "hasParticipate" => in_array($actorConnected->get("id"), $participant),
                ];
                if (@$object->get('_extend_props')) {
                    $objectExtendProps = $object->get('_extend_props');
                    if (@$objectExtendProps['category']) {
                        $data = array_merge($data, array('type' => ucfirst(strtolower($objectExtendProps['category']))));
                    }
                    if (@$objectExtendProps['timezone']) {
                        $data = array_merge($data, array('timeZone' => $objectExtendProps['timezone']));
                    }
                    $data = array_merge($data, array('participantCount' => count($participant)));
                }
                if (@$object->get('attachment') && $object->get('attachment') != null) {
                    $attachements = $object->get('attachment');

                    foreach ($attachements as $attachement) {
                        $extendAttachementsProps = $attachement->get('_extend_props');
                        if ($extendAttachementsProps == null) {
                            //if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                            if ($attachement->get('category') == 'socialNetwork') {
                                $data = array_merge($data, array('socialNetwork' => array($attachement->get('name') => $attachement->get('url'))));
                            } else {
                                $urlImg =  $attachement->get('url');
                                $data = array_merge($data, array('profilImageUrl' =>  $urlImg, 'profilMediumImageUrl' => $urlImg, 'profilRealBannerUrl' => $urlImg));
                            }
                            //}
                        }
                    }
                }

                $location = $object->get('location');
                if (isset($location) && $location != null) {
                    $location  = $object->get('location');
                    $locationExtendProps = $location->get('_extend_props');
                    $addressExtendProps = $locationExtendProps['address']->get('_extend_props');

                    if (isset($addressExtendProps) && $addressExtendProps != null) {
                        $geo = array(
                            "@type" => "GeoCoordinates",
                            "latitude" => $location->get('latitude'),
                            "longitude" => $location->get('longitude')
                        );
                        $geoPosition = array(
                            "type" => $location->get('type'),
                            "coordinates" => array(
                                $location->get('longitude'),
                                $location->get('latitude')
                            )
                        );

                        $data = array_merge($data, array("address" =>
                        array(
                            '@type' => $addressExtendProps['type'],
                            'streetAddress' => $location->get('name') . " " .  @$addressExtendProps['addressRegion'] && $addressExtendProps['addressRegion'] != null  ?  @$addressExtendProps['addressLocality'] &&  $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressRegion'] . " " . $addressExtendProps['addressLocality'] : $addressExtendProps['addressRegion'] : "", 'postalCode' => @$addressExtendProps['postalCode'] && $addressExtendProps['postalCode'] != null ? $addressExtendProps['postalCode'] : ""
                        ), 'addressLocality' => @$addressExtendProps['addressLocality'] &&  $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] : ""));
                    }
                    $data = array_merge($data, array('geo' => $geo));
                    //$data = array_merge($data,array('geoPosition' => $geoPosition));
                }
                if (@$object->get('tag') && $object->get('tag') != null) {
                    $tags = $object->get('tag');
                    $tagList = [];
                    foreach ($tags as $tagData) {
                        $extendTagProps = $tagData->get('_extend_props');
                        //  if($tag->get('type')=='Hashtag'){
                        array_push($tagList, substr($extendTagProps['name'], 1));
                        //  }
                    }
                    $data = array_merge($data, array('tags' => $tagList));
                }
                //$organizer->get('icon')->get('url')
                $data = array_merge(
                    $data,
                    array(
                        'organizer' =>
                        array(
                            'name' => $organizer->get('preferredUsername'),
                            'url' => $object->get("attributedTo"),
                            'profilThumbImageUrl' => $organizer->get('icon') ? $organizer->get('icon')->get('url') : Yii::$app->getModule('co2')->assetsUrl . "/images/avatar.jpg"
                        )
                    )
                );
            }
        }
        return $data;
    }

    public static function deduplicateArray($array)
    {
        $uniqueArray = [];
        foreach ($array as $element) {
            if (!in_array($element, $uniqueArray)) {
                $uniqueArray[] = $element;
            }
        }
        return $uniqueArray;
    }
    public static function addOrRemoveIntoArray($my_array, $value)
    {
        $result = array();
        if (in_array($value[0], $my_array)) {
            $result = array_diff($my_array, $value);
        } else {
            $result = array_merge($my_array, $value);
        }
        return $result;
    }
    public static function addIfNotInArray($my_array, $value)
    {
        $result = array();
        if (!in_array($value[0], $my_array)) {
            $result = array_merge($my_array, $value);
        } else {
            $result = $my_array;
        }
        return $result;
    }
    public static function removeIfExistInArray($my_array, $value)
    {
        $result = array();
        if (in_array($value[0], $my_array)) {
            $result = array_diff($my_array, $value);
        } else {
            $result = $my_array;
        }
        return $result;
    }

    public static function slugifyTag($text, $length = null)
    {
        $replacements = [
            '<' => '', '>' => '', '-' => ' ', '&' => '', '"' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae', 'Ä' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae', 'Ç' => 'C', "'" => '', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I', 'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'L', 'Ľ' => 'L', 'Ĺ' => 'L', 'Ļ' => 'L', 'Ŀ' => 'L', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N', 'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'Oe', 'Ö' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O', 'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S', 'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T', 'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U', 'Ü' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U', 'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z', 'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'ae', 'ä' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a', 'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c', 'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e', 'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h', 'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i', 'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j', 'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l', 'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n', 'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe', 'ö' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe', 'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ś' => 's', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'ue', 'ū' => 'u', 'ü' => 'ue', 'ů' => 'u', 'ű' => 'u', 'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y', 'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'α' => 'a', 'ß' => 'ss', 'ẞ' => 'b', 'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I', 'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', '.' => '-', '€' => '-eur-', '$' => '-usd-'
        ];
        $text = strtr($text, $replacements);
        $text = preg_replace('~[^\pL\d.]+~u', '-', $text);
        $text = preg_replace('~[^-\w.]+~', '-', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (isset($length) && $length < strlen($text))
            $text = rtrim(substr($text, 0, $length), '-');
        return $text;
    }
    public static function coLocationToActivitypubLocation($location, $uuid)
    {

        $array = array();
        if (isset($location['address'])) {
            if (isset($uuid)) {
                $array['id'] = "https://somemobilizon.instance/address/" . $uuid;
            }
            if (isset($location['address']['addressCountry']))
                $array['address']['addressCountry'] = $location['address']['addressCountry'];
            if (isset($location['address']['addressLocality']))
                $array['address']['addressLocality'] = $location['address']['addressLocality'];
            if (isset($location['address']['level1Name']))
                $array['address']['addressRegion'] = $location['address']['level1Name'];

            if (isset($location['address']['postalCode']))
                $array['address']['postalCode'] = $location['address']['postalCode'];
            if (isset($location['address']['streetAddress']))
                $array['address']['streetAddress'] = $location['address']['streetAddress'];
            if (isset($location['address']['@type']))
                $array['address']['type'] = $location['address']['@type'];

            if (isset($location['geoPosition']['latitude']))
                $array['latitude'] = $location['geoPosition']['latitude'];
            if (isset($location['geoPosition']['longitude']))
                $array['longitude'] =  $location['geoPosition']['longitude'];
            if (isset($location["addresses"])) {
                $array['name'] =  $location["addresses"];
            }
            $array['type'] =  "Place";
        }
        return $array;
    }

    public static function coTagWithTypeToActivitypubTag($tags = null, $type= null)
    {

        $tagArray = [];
        if (isset($tags) && is_array($tags)) {
            foreach ($tags as $tag) {
                $tagArray[] = [
                    "href" => "https://somemobilizon.instance/tags/" . self::slugifyTag($tag),
                    "name" => "#" . $tag,
                    "type" => "Hashtag",
                    "category" => "tag"
                ];
            }
        }
        if($type != null){
            $tagArray[] = [
                "href" => "https://somemobilizon.instance/tags/" . self::slugifyTag($type),
                "name" => "#" . $type,
                "type" => "Hashtag",
                "category" => "type"
            ];
        }
        return $tagArray;
    }

    public static function coTagToActivitypubTagEdit($exist, $tags)
    {
        if ($exist == null) {
            $exist = array();
        }
        $tagArray = [];
        foreach ($tags as $tag) {
            $tagArray[] = [
                "href" => "https://somemobilizon.instance/tags/" . self::slugifyTag($tag),
                "name" => "#" . $tag,
                "type" => "Hashtag",
                "category" => "tag"
            ];
        }

        foreach ($exist as $key => $value) {
            foreach ($tagArray as $k => $v) {
                if (isset($v['href']) && $v['href'] === $value['href']) {
                    unset($exist[$key]);
                }
            }
        }
        // Supprimer les tags existants qui ne sont plus présents dans $tags
        foreach ($exist as $key => $value) {
            $found = false;
            foreach ($tagArray as $k => $v) {
                if (isset($v['href']) && $v['href'] === $value['href'] && $v['category'] === 'tag' && $v['type'] === 'Hashtag') {
                    $found = true;
                    break;
                }
            }
            if (!$found && $value['type'] === 'Hashtag' && $value['category'] === 'tag') {
                unset($exist[$key]);
            }
        }
        foreach ($tagArray as $value) {
            array_push($exist, $value);
        }
        return self::deduplicateArray($exist);
    }
    public static function coTypeCombineToActivitypubTagEdit($exist, $type)
    {
        if ($exist == null) {
            $exist = array();
        }
        // Supprimer les tags existants avec une catégorie "type"
        foreach ($exist as $key => $value) {
            if (isset($value['category']) && $value['category'] === 'type') {
                unset($exist[$key]);
            }
        }
        $tagArray[] = array(
            "href" => "https://somemobilizon.instance/tags/" . self::slugifyTag($type),
            "name" => "#" . $type,
            "type" => "Hashtag",
            "category" => "type"
        );
        foreach ($exist as $key => $value) {
            foreach ($tagArray as $k => $v) {
                if (isset($v['href']) && $v['href'] === $value['href']) {
                    unset($exist[$key]);
                }
            }
        }
        foreach ($tagArray as $value) {
            array_push($exist, $value);
        };

        return self::deduplicateArray($exist);
    }

    public static function coTagWithTypeToActivitypubTagEdit($exist, $tags, $type)
    {
        if ($exist == null) {
            $exist = array();
        }
        $tagArray = [];
        foreach ($tags as $tag) {
            $tagArray[] = [
                "href" => "https://somemobilizon.instance/tags/" . self::slugifyTag($tag),
                "name" => "#" . $tag,
                "type" => "Hashtag",
                "category" => "tag"
            ];
        }
        foreach ($exist as $key => $value) {
            if (@$value['category'] && $value['category'] === "type") {
                unset($exist[$key]);
            }
        }
        $tagArray[] = [
            "href" => "https://somemobilizon.instance/tags/" . self::slugifyTag($type),
            "name" => "#" . $type,
            "type" => "Hashtag",
            "category" => "type"
        ];

        foreach ($exist as $key => $value) {
            foreach ($tagArray as $k => $v) {
                if (isset($v['href']) && $v['href'] === $value['href']) {
                    unset($exist[$key]);
                }
            }
        }
        foreach ($tagArray as $value) {
            array_push($exist, $value);
        }
        $tags = array_map('strtolower', $tags);
        foreach ($exist as $key => $value) {
            $existTagName = strtolower(substr($value['name'], 1));
            if ($existTagName != $type && !in_array($existTagName, $tags)) {
                unset($exist[$key]);
            }
        }
        return self::deduplicateArray($exist);
    }


    public static function coSocialNetworkToActivitypubLink($socialNetwork)
    {
        $array = [];
        if (@$socialNetwork["gitlab"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "gitlab",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["gitlab"]
            ];
        if (@$socialNetwork["github"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "github",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["github"]
            ];
        if (@$socialNetwork["diaspora"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "diaspora",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["diaspora"]
            ];
        if (@$socialNetwork["mastodon"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "mastodon",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["mastodon"]
            ];
        if (@$socialNetwork["telegram"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "telegram",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["telegram"]
            ];
        if (@$socialNetwork["signal"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "signal",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["signal"]
            ];
        if (@$socialNetwork["facebook"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "facebook",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["facebook"]
            ];
        if (@$socialNetwork["twitter"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "twitter",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["twitter"]
            ];
        if (@$socialNetwork["instagram"])
            $array[] = [
                "mediaType" => "text/html",
                "name" => "instagram",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["instagram"]
            ];
        return $array;
    }
    public static function coAttachmentToActivitypubAttachment($data, $exist = null)
    {
        $attachment = [];
        if (@$data['email']) {
            if (isset($exist) && is_array($exist)) {
                foreach ($exist as $key => $value) {
                    if (isset($value['url']) && strpos($value['url'], 'mailto') !== false) {
                        unset($exist[$key]);
                    }
                }
            }
            array_push($attachment, array(
                "mediaType" => "text/html",
                "name" => "Website",
                "type" => "Link",
                "category" => "mailto",
                "url" => "mailto:" . $data['email']
            ));
        }

        if (@$data['url']) {
            $url[] = array(
                "mediaType" => "text/html",
                "name" => "Website",
                "type" => "Link",
                "category" => "url",
                "url" => $data['url']
            );
            if (isset($exist) && is_array($exist)) {
                foreach ($exist as $key => $value) {
                    if (isset($url['url']) && $url['url'] === $value['url']) {
                        unset($exist[$key]);
                    }
                }
            }
            array_push($attachment, array(
                "mediaType" => "text/html",
                "name" => "Website",
                "type" => "Link",
                "category" => "url",
                "url" => $data['url']
            ));
        }
        if (isset($data['profilBannerUrl'])) {
            array_push($attachment, array(
                "mediaType" => self::get_image_mime_type($data['profilBannerUrl']),
                "name" => "Banner",
                "type" => "Document",
                "category" => "image",
                "url" => $data['profilBannerUrl']
            ));
        }
        if (count(self::coSocialNetworkToActivitypubLink($data)) > 0) {
            $socialNetwork = self::coSocialNetworkToActivitypubLink($data);
            if (isset($exist) && is_array($exist)) {
                foreach ($exist as $key => $value) {
                    foreach ($socialNetwork as $k => $v) {
                        if (isset($v['url']) && $v['url'] === $value['url']) {
                            unset($exist[$key]);
                        }
                    }
                }
            }
            foreach ($socialNetwork as $sn) {
                array_push($attachment, $sn);
            }
        }
        if (isset($exist) && is_array($exist)) {
            return self::mergeArrayByDomain($exist, $attachment);
        } else {
            return $attachment;
        }
    }
    public static function coImageToActivityPubImage($res)
    {
        $attachment = [];
        if (isset($res['profilImageUrl'])) {
            $arr = array_merge($attachment, array(
                "mediaType" => self::get_image_mime_type($res['profilImageUrl']),
                "name" => "Banner",
                "type" => "Document",
                "category" => "image",
                "url" => $res['profilImageUrl']
            ));
        } else {
            if (isset($res['docPath'])) {
                $imageUrl = self::createUrl(substr($res['docPath'], 1));
                $arr = array_merge($attachment, array(
                    "mediaType" => self::get_image_mime_type($imageUrl),
                    "name" => "Banner",
                    "type" => "Document",
                    "category" => "image",
                    "url" => $imageUrl
                ));
            }
        }

        return $arr;
    }

    public static function coImageBannerToActivityPubImage($objectArray, $el)
    {
        $attachment = @$objectArray ? $objectArray : array();
        $imageUrl = self::createUrl(substr($el, 1));

        $arr = [];
        foreach ($attachment as $key => $value) {
            if (isset($value['name']) && $value['name'] == 'Banner') {
                unset($attachment[$key]);
            }
        }
        if (isset($el['profilBannerUrl'])) {
            $attachment[] = array(
                "mediaType" => self::get_image_mime_type($el['profilBannerUrl']),
                "name" => "Banner",
                "type" => "Document",
                "category" => "image",
                "url" => $el['profilBannerUrl']
            );
        } else {
            if (isset($el['docPath'])) {
                $imageUrl = self::createUrl(substr($el['docPath'], 1));
                $attachment[] = array(
                    "mediaType" => self::get_image_mime_type($imageUrl),
                    "name" => "Banner",
                    "type" => "Document",
                    "category" => "image",
                    "url" => $imageUrl
                );
            }
        }
        return $attachment;
    }

    public static function coImageProfilToActivityPubImage($objectArray, $el)
    {
        $attachment = @$objectArray ? $objectArray : array();
        $imageUrl = self::createUrl(substr($el, 1));
        foreach ($attachment as $key => $value) {
            if (isset($value['name']) && $value['name'] == 'Image') {
                unset($attachment[$key]);
            }
        }
        if (isset($el['profilImageUrl'])) {
            $attachment[] = array(
                "mediaType" => self::get_image_mime_type($el['profilImageUrl']),
                "name" => "Image",
                "type" => "Document",
                "category" => "image",
                "url" => $el['profilImageUrl']
            );
        } else {
            if (isset($el['docPath'])) {
                $imageUrl = self::createUrl(substr($el['docPath'], 1));
                $attachment[] = array(
                    "mediaType" => self::get_image_mime_type($imageUrl),
                    "name" => "Image",
                    "type" => "Document",
                    "category" => "image",
                    "url" => $imageUrl
                );
            }
        }
        return $attachment;
    }

    public static function coThumbToActivityPubImage($objectArray, $el)
    {
        $attachment = @$objectArray ? $objectArray : array();
        $imageUrl = self::createUrl(substr($el, 1));
        foreach ($attachment as $key => $value) {
            if (isset($value['name']) && $value['name'] == 'Banner') {
                unset($attachment[$key]);
            }
        }
        $arr = array_merge($attachment, array(
            "mediaType" => self::get_image_mime_type($el),
            "name" => "Banner",
            "type" => "Document",
            "category" => "image",
            "url" => $imageUrl
        ));
        return $arr;
    }

    public static function mergeArrayByDomain($exist, $newData)
    {
        $existDomains = array_map(function ($value) {
            $parsedUrl = parse_url($value['url']);
            return $parsedUrl['host'] ?? '';
        }, $exist);

        $mergedArray = [];

        foreach ($newData as $value) {
            $parsedUrl = parse_url($value['url']);
            $domain = $parsedUrl['host'] ?? '';

            $existingKey = array_search($domain, $existDomains);
            if ($existingKey !== false) {
                if ($exist[$existingKey]['url'] != $value['url']) {
                    $exist[$existingKey]['url'] = $value['url'];
                }
            } else {
                $existDomains[] = $domain;
                $exist[] = $value;
            }
        }

        return $exist;
    }
    public static function isMobilizonInstance($instance)
    {

        if(is_object($instance)){
            if(!empty($instance->get('object'))){
                return self::isMobilizonInstance($instance->get('object'));
            }else{
                return self::isMobilizonInstance($instance->get('id'));
            }
        }
        $url = 'https://instances.joinmobilizon.org/api/v1/instances?start=0&count=10&sort=totalLocalGroups&search=' . self::getWebsiteDomain($instance);
       $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $res = curl_exec($curl);
        curl_close($curl);
        $json = self::decodeJson($res);
        return $json['total'] > 0;
    }
    /**public static function isActorIsMemberOfMobilizon($invitorId)
    {
        $count = 0;
        $contextData = Yii::app()->session["contextData"];
        $collection = $contextData["type"] == "group" ? Organization::COLLECTION : Person::COLLECTION;
        $actorId = $contextData["type"] == "group" ? $contextData["id"] : $_SESSION["userId"];
        $user = PHDB::findOneById($collection, $actorId);
        if (isset($user["links"]["activitypub"]["follows"]) && sizeof($user["links"]["activitypub"]["follows"]) > 0) {
            foreach ($user["links"]["activitypub"]["follows"] as $follow) {
                if ($follow["invitorId"] == $invitorId) {
                    $count += 1;
                }
            }
        } else {
            $count = 0;
        }
        return $count > 0;
    }**/
     public static function jobToRunBeforePost($job)
    {
        if (self::isMobilizonInstance(preg_replace('/\/inbox$/', '', $job["receiver"]))) {
            $job["data"]["id"] = str_replace('somemobilizon.instance', self::getWebsiteDomain($job["receiver"]), $job["data"]["id"]);
            $job["data"]["object"]["attributedTo"] = preg_replace('/\/inbox$/', '', $job["receiver"]);
            $job["data"]["object"]["id"] = str_replace('somemobilizon.instance', self::getWebsiteDomain($job["receiver"]), $job["data"]["object"]["id"]);
            if(isset($job["data"]["object"]["url"])){
                $job["data"]["object"]["url"] = str_replace('somemobilizon.instance', self::getWebsiteDomain($job["receiver"]), $job["data"]["object"]["url"]);
            }
            if (@$job["data"]["object"]["tag"]) {
                $tags = $job["data"]["object"]["tag"];
                foreach ($tags as $x => $val) {
                    $job["data"]["object"]["tag"][$x]["href"] = str_replace('somemobilizon.instance', self::getWebsiteDomain($job["receiver"]), $job["data"]["object"]["tag"][$x]["href"]);
                }
            }
            if (@$job["data"]["object"]["location"] && count($job["data"]["object"]["location"]) > 0) {
                $job["data"]["object"]["location"]["id"] = str_replace('somemobilizon.instance', self::getWebsiteDomain($job["receiver"]), $job["data"]["object"]["location"]["id"]);
            } else {
                unset($job["data"]["object"]["location"]);
            }
        }
        return $job;
    }

    public static function federateElement($toSave)
    {
        $activity = null;
        $toSave['isEdit'] = false;
        if (isset($toSave['typeElement'])) {
            $toSave['collection'] = $toSave['typeElement'];
            $toSave['isEdit'] = true;
        }

        if ($toSave['collection'] == 'projects' && (isset($toSave['startDate'])  || isset($toSave['endDate']))) {
            $toSave['isEdit'] = true;
        }

        switch ($toSave['collection']) {
            case "events":
                $activity = ActivitypubActivityCreator::createOrUpdateEventActivity($toSave);
                break;
            case "projects":
                $activity = ActivitypubActivityCreator::createOrUpdateProjectActivity($toSave);
                break;
        }

        if($activity instanceof Activity){
            Handler::handle($activity);
            return $activity->toArray();
        }

    }
    public static function actorFollowsList()
    {
        $follows = array();
        /**if($contextData["type"] != "all"){
          $collection = $contextData["type"] == "group" ? Organization::COLLECTION : Person::COLLECTION;
          $actorId = $contextData["type"] == "group" ? $contextData["id"]: $_SESSION["userId"];
          $user = PHDB::findOneById($collection, $actorId);
              if (isset($user["links"]["activitypub"]["follows"])) {
                  foreach ($user["links"]["activitypub"]["follows"] as $follow) {
                      if(!isset($follow["pending"])){
                             array_push($follows,$follow["invitorId"]);
                      }
                  }
              }
      }else{**/
        $userId = Yii::app()->session["userId"];
        $user = PHDB::findOneById(Person::COLLECTION, $userId);
        if (isset($user["links"]["activitypub"]["follows"])) {
            foreach ($user["links"]["activitypub"]["follows"] as $follow) {
                if (!isset($follow["pending"])) {
                    array_push($follows, $follow["invitorId"]);
                }
            }
        }
        $groups = PHDB::find(Organization::COLLECTION, array("creator" => $userId));
        foreach ($groups as $v) {
            if (isset($v["links"]["activitypub"]["follows"])) {
                foreach ($v["links"]["activitypub"]["follows"] as $follow) {
                    if (!isset($follow["pending"])) {
                        array_push($follows, $follow["invitorId"]);
                    }
                }
            }
        }
        // }
        return $follows;
    }
    public static function isActivitypubEnabled()
    {
        $connectedUserId = Yii::app()->session["userId"];
        $connectedUser = Person::getById($connectedUserId);
        if (!isset($connectedUser)) {
            $activitypubEnable = false;
        }
        $activitypubEnable = Preference::isActivitypubActivate(@$connectedUser["preferences"]);
        return $activitypubEnable;
    }
    public static function isLocalActor($actor = null)
    {
        if ($actor == null) {
            return $_SERVER['HTTP_HOST'] == Config::HOST();
        } else {
            $domain = parse_url($actor->get('id'), PHP_URL_HOST);
            return $domain  == Config::HOST();
        }
    }

    public static  function getLinkToAdress($url)
    {
        // Vérifier si l'URL utilise la méthode "u" ou le format "username@domain"
        if (strpos($url, '/u/') !== false) {
            $parts = explode('/u/', $url);
            if (count($parts) !== 2) {
                return null;
            }
            $username = $parts[1];
        } else if (strpos($url, '/p/') !== false) {
            $parts = explode('/p/', $url);
            if (count($parts) !== 2) {
                return null;
            }
            $username = $parts[1];
        } else {
            preg_match('/^https?:\/\/(.+)@(.+)$/', $url, $matches);
            if (count($matches) !== 3) {
                return null;
            }
            $username = $matches[1];
        }

        // Construire l'adresse ActivityPub en utilisant le nom d'utilisateur et le domaine
        $domain = parse_url($url, PHP_URL_HOST);
        return '@' . $username . '@' . $domain;
    }


    public static  function getLinkToUsername($url)
    {
        // Vérifier si l'URL utilise la méthode "u" ou le format "username@domain"
        if (strpos($url, '/u/') !== false) {
            $parts = explode('/u/', $url);
            if (count($parts) !== 2) {
                return null;
            }
            $username = $parts[1];
        } else if (strpos($url, '/p/') !== false) {
            $parts = explode('/p/', $url);
            if (count($parts) !== 2) {
                return null;
            }
            $username = $parts[1];
        } else {
            preg_match('/^https?:\/\/(.+)@(.+)$/', $url, $matches);
            if (count($matches) !== 3) {
                return null;
            }
            $username = $matches[1];
        }
        return $username;
    }

    public static function checkIsContainLinks($links, $connectType)
    {
        $exists = true;

        if (@$links["activitypub"] && isset($links[$connectType][Yii::app()->session["userId"]]) == false) {
            $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
            if (@$links["activitypub"][$connectType]) {
                    $exists = array_reduce($links["activitypub"][$connectType], function ($carry, $item) use ($actor) {
                        return $carry || @$item['invitorId'] == @$actor->get('id');
                    }, false);
            } else $exists = false;
            } else $exists = isset($links[$connectType][Yii::app()->session["userId"]]);

        return $exists;
    }
    public static function checkByLinkType($links, $connectType, $linkType)
    {
        $exists = false;
        if (@$links["activitypub"]  && isset(Yii::app()->session["userId"])) {
            $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);


            if (!$actor) return false;
            if (@$links["activitypub"][$connectType]) {
                foreach ($links["activitypub"][$connectType] as $key => $value) {
                    if (@$value['invitorId'] == $actor->get('id')) {
                        $exists = isset($value[$linkType]);
                    }
                }
            }
        } else {
            $exists = isset($links[$connectType][Yii::app()->session["userId"]][$linkType]);
        }
        return $exists;
    }

    public static function getLinksInfo($links, $connectType)
    {
        if (@$links["activitypub"]  && isset(Yii::app()->session["userId"])) {
            $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
            if (!$actor) return false;
            if (@$links["activitypub"][$connectType]) {
                foreach ($links["activitypub"][$connectType] as $key => $value) {
                    if (@$value['invitorId'] == $actor->get('id')) {
                        $exists = $value;
                    }
                }
            }
        } else {
            $exists = $links[$connectType][Yii::app()->session["userId"]];
        }
        return $exists;
    }
    public static function listAdmins($parentId, $parentType)
    {
        $project = array();
        $contributors = array();
        if (in_array($parentType, [Project::COLLECTION])) {
            $project = PHDB::findOne($parentType, [
                '$or' => [
                    ["_id" => new MongoId($parentId)],
                    ["objectId" => $parentId]
                ]
            ]);
        } else {
            return false;
        }
        if (!$project) return array();
        if (@$project["links"]["activitypub"]) {
            if (@$project["links"]["activitypub"]["contributors"]) {
                foreach ($project["links"]["activitypub"]["contributors"] as $key => $value) {
                    if (@$value['isAdmin'] == true) {
                        if (@$value["isAdminPending"] == null || @$value["isAdminPending"] == false) {
                            array_push($contributors, $value['invitorId']);
                        }
                    }
                }
            }
        }
        return $contributors;
    }
    public static function checkIfIsAdmin($parentId, $parentType)
    {
        if (isset(Yii::app()->session["userId"])) {
            $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
            $admins = self::listAdmins($parentId, $parentType);
            return in_array($actor->get('id'), $admins);
        } else {
            return false;
        }
    }

    public static function buildUrlFromParts($parts)
    {
        $scheme = isset($parts['scheme']) ? $parts['scheme'] . '://' : '';
        $host = isset($parts['host']) ? $parts['host'] : '';
        $port = isset($parts['port']) ? ':' . $parts['port'] : '';
        $user = isset($parts['user']) ? $parts['user'] : '';
        $pass = isset($parts['pass']) ? ':' . $parts['pass']  : '';
        $pass = ($user || $pass) ? "$pass@" : '';
        $path = isset($parts['path']) ? $parts['path'] : '';
        $query = isset($parts['query']) ? '?' . $parts['query'] : '';
        $fragment = isset($parts['fragment']) ? '#' . $parts['fragment'] : '';

        return "$scheme$user$pass$host$port$path$query$fragment";
    }
    public static function checkIfCoUserIsFollower($person)
    {
        $exists = false;
        if (isset(Yii::app()->session["userId"])) {
            $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
            if (!$user || !isset($user["links"]["activitypub"]["follows"])) return false;
            $exists = array_reduce($user["links"]["activitypub"]["follows"], function ($carry, $item) use ($person) {
                return $carry || @$item['invitorId'] == $person;
            }, false);
        } else {
            return false;
        }
        return $exists;
    }
    /**
     * checker si l'element est un element federé ou un element venant du fediverse
     *
     * @param [type] $element
     * @return boolean
     */
    public static function isFediverseShareLocal($element)
    {
        if (isset($element['fromActivityPub'])) {
            if (isset(Yii::app()->session["userId"]) &&  isset($element['attributedTo'])) {
                $actor = ActivitypubActor::getCoPersonAsActorByUserId(Yii::app()->session["userId"]);
                if ($element['attributedTo'] ==  $actor->get('id')) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
    }
    public static function activitypubToElement($objectId)
    {

        $connectedUserId = Yii::app()->session["userId"];
        $connectedUser = Person::getById($connectedUserId);
        $activitypubEnable = Preference::isActivitypubActivate(@$connectedUser["preferences"]);
        $data = [];
        $socialNetwork=array();
        if ($activitypubEnable) {
            $object = Type::createFromAnyValue($objectId);
            $data = [
                "collection" => self::getCollection($object),
                "name" => $object->get('name'),
                "created" => strtotime($object->get('published')),
                "updated" => strtotime($object->get('updated')),
                "shortDescription" => $object->get("content") != "" ? self::substrwords($object->get("content"), 140) : "",
                "providerInfo" => [
                    "name" => self::getWebsiteDomain($object->get('id')),
                    "icon" => self::getWebsiteFavicon($object->get('id')),
                    "url" => self::getWebsiteBaseUrl($object->get('id'))
                ],
                "slug" => Slug::checkAndCreateSlug($object->get('name')),
                "description" => $object->get("content"),
                "objectId" => $objectId

            ];
            if (!empty($object->get("attributedTo"))) {
                $collectionType = self::getCollectionTypeByUrl($object->get("attributedTo"));
                $actor = Type::createFromAnyValue($object->get("attributedTo"));
                $parent['type'] = $actor->get('type');
                $parent['id'] = $object->get("attributedTo");
                if (!empty($actor->get('icon'))) {
                    $parent['profilThumbImageUrl'] = $actor->get('icon')->get('url');
                }
                $parent['name'] = $actor->get('name');
                $parent['activitypub'] = true;

                $data = array_merge($data, array('parent' =>
                array($object->get("attributedTo") => $parent)));
            }
            if ($object->get('_extend_props')) {
                $objectExtendProps = $object->get('_extend_props');

                if (isset($objectExtendProps['category'])) {
                    $data = array_merge($data, array('type' => ucfirst(strtolower($objectExtendProps['category']))));
                }
                if (isset($objectExtendProps['timezone'])) {
                    $data = array_merge($data, array('timeZone' => $objectExtendProps['timezone']));
                }


                if (isset($objectExtendProps['progress'])) {
                    $data = array_merge($data, array('properties' => array('avancement' => $objectExtendProps['progress'])));
                }
                if (isset($objectExtendProps['email'])) {
                    $data = array_merge($data, array('email' => $objectExtendProps['email']));
                }
            }
            if (!empty($object->get('startTime'))) {
                $data["startDate"] = $object->get('startTime');
            }

            if (!empty($object->get('endTime'))) {
                $data["endDate"] = $object->get('endTime');
            }
            if (!empty($object->get('content'))) {
                $data["description"] = $object->get('content');
            }
            if (@$object->get('location') && $object->get('location') != null) {
                $location  = $object->get('location');
                $locationExtendProps = $location->get('_extend_props');
                $addressExtendProps = $locationExtendProps['address']->get('_extend_props');

                if (@$addressExtendProps && $addressExtendProps != null) {
                    $geo = array(
                        "@type" => "GeoCoordinates",
                        "latitude" => $location->get('latitude'),
                        "longitude" => $location->get('longitude')
                    );
                    $geoPosition = array(
                        "type" => $location->get('type'),
                        "coordinates" => array(
                            $location->get('longitude'),
                            $location->get('latitude')
                        )
                    );

                    $data = array_merge($data, array("address" =>
                    array(
                        '@type' => $addressExtendProps['type'],
                        'streetAddress' => $location->get('name') . " " . @$addressExtendProps['addressRegion'] && $addressExtendProps['addressRegion'] != null  ?  @$addressExtendProps['addressLocality'] &&  $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] . " " . $addressExtendProps['addressRegion'] : $addressExtendProps['addressRegion'] : "", 'postalCode' => @$addressExtendProps['postalCode'] && $addressExtendProps['postalCode'] != null ? $addressExtendProps['postalCode'] : ""
                    ), 'addressLocality' => @$addressExtendProps['addressLocality'] &&  $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] : "", "geo" => $geo));
                }
            }

            if (@$object->get('tag') && $object->get('tag') != null) {
                $tags = $object->get('tag');
                $tagList = [];
                foreach ($tags as $tagData) {
                    $extendTagProps = $tagData->get('_extend_props');
                    array_push($tagList, substr($extendTagProps['name'], 1));
                }
                $data = array_merge($data, array('tags' => $tagList));
            }
            if (!empty($object->get('attachment'))) {
                $attachements = $object->get('attachment');
                foreach ($attachements as $attachement) {
                    $extendAttachementsProps = $attachement->get('_extend_props');
                    if ($extendAttachementsProps != null) {
                        if ($extendAttachementsProps['category'] == 'url') {
                            $data['url'] = $extendAttachementsProps['url'];
                        }
                        else if ($extendAttachementsProps['category'] == 'socialNetwork') {
                            $socialNetwork[$attachement->get('name')]=$extendAttachementsProps['url'];
                            $data = array_merge($data, array('socialNetwork' => $socialNetwork));
                        } else {
                            if ($attachement->get('name') == 'Banner') {
                                if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                                    $urlImg =  $attachement->get('url');
                                    $data = array_merge($data, array('profilBannerUrl' =>  $urlImg, 'profilRealUrl' => $urlImg));
                                }
                            } else {
                                if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                                    $urlImg =  $attachement->get('url');
                                    $data = array_merge($data, array('profilImageUrl' =>  $urlImg, 'profilMediumImageUrl' => $urlImg));
                                }
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }
    public static function getCollection($object)
    {
        switch ($object->get('type')) {
            case 'Project':
                return 'projects';
                break;
            case 'Event':
                return 'events';
                break;
            case 'Person':
                return 'person';
                break;
        }
    }
    public static  function getCollectionTypeByUrl($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return array(
                "name" => $basename,
                "type" => "group"
            );
        } else if (strpos($url, 'projects') !== false) {
            return array(
                "name" => $basename,
                "type" => "projects"
            );
        } else {
            return array(
                "name" => $basename,
                "type" => "person"
            );
        }
    }
    public static function filterResultsForActivityPubFlux($results): array
    {
        if (self::isActivitypubEnabled() ) {
            foreach ($results["results"] as $key => $value) {
                if (isset($value['fromActivityPub']) && isset($value['attributedTo'])) {
                        if (self::checkIfCoUserIsFollower($value['attributedTo'])) {
                            $results["results"][$key]["providerInfo"] = [
                                "name" => self::getWebsiteDomain($results["results"][$key]["objectId"]),
                                "icon" => self::getWebsiteFavicon($results["results"][$key]["objectId"]),
                                "url" => self::getWebsiteBaseUrl($results["results"][$key]["objectId"])
                            ];
                        } else {
                            unset($results["results"][$key]);
                        }

                }
            }
        }else{
            // si activitypub ne pas activé on retire tous les elements qui vient du fediverse
            foreach ($results["results"] as $key => $value) {
                if (isset($value['fromActivityPub'])) {
                    unset($results["results"][$key]);
                }
            }
        }
        return $results;
    }
    public static function filterSearchAdminDataForActivityPubFlux($type,$id,$post,$results): array
    {
        if(self::isActivitypubEnabled()){
            if ($type == 'projects') {
                $projectInfo = PHDB::findOneById(Project::COLLECTION, $id);

                if (isset($projectInfo['links']['activitypub'])) {

                    if (isset($post['filters']) && isset($post['filters']['links.projects.' . $id])) {
                        if (isset($projectInfo['links']['activitypub']['contributors'])) {
                            $contributors = $projectInfo['links']['activitypub']['contributors'];
                            $results['count']['citoyens'] = count($contributors);
                            foreach ($contributors as $key => $value) {
                                $results['results'][$value['invitorId']] = ActivitypubTranslator::actorTocoUser($value);
                                $results['results'][$value['invitorId']]['projectCreator'] = $projectInfo['attributedTo'];
                            }
                        }
                    }
                    if (isset($post['filters']) && isset($post['filters']['links.follows.' . $id])) {

                        if (isset($projectInfo['links']['activitypub']['followers'])) {
                            $followers = $projectInfo['links']['activitypub']['followers'];
                            $results['count']['citoyens'] = count($followers);
                            foreach ($followers as $key => $value) {
                                $results['results'][$value['invitorId']] = ActivitypubTranslator::actorTocoUser($value);
                                $results['results'][$value['invitorId']]['projectCreator'] = $projectInfo['attributedTo'];
                            }
                        }
                    }
                }
            }
        }
        return $results;
    }
    public static function displayFirst30Words($text) {
        $text = trim($text);
        $words = preg_split('/\s+/', $text);
        $first30Words = array_slice($words, 0, 30);
        $displayText = implode(' ', $first30Words);
        return $displayText;
    }

    public static function parseToBadge($object)
    {
        $data = [
            "collection" => "badges",
            "name" => $object->get('name'),
            "created" => strtotime($object->get('published')),
            "updated" => strtotime($object->get('updated')),
            "public" => true,
            "fromActivityPub" => true,
            "creator" => $object->get('actor'),
            "attributedTo" => $object->get("attributedTo"),
            "objectId" => $object->get("id"),
            "preferences" => array("private" => false)
        ];
        $objectToArray = $object->toArray();
        if (!empty($object->get('icon'))) {
            $data["icon"] = $object->get('icon');
        }
        if (!empty($object->get('content'))) {
            $data["description"] = $object->get('content');
        }
        if(isset($objectArray['criteria'])){
            $data['criteria']["narrative"] = $objectArray['criteria'];
        }
        if (@$object->get('tag') && $object->get('tag') != null) {
            $tags = $object->get('tag');
            $tagList = [];
            foreach ($tags as $tagData) {
                $extendTagProps = $tagData->get('_extend_props');
                array_push($tagList, substr($extendTagProps['name'], 1));
            }
            $data = array_merge($data, array('tags' => $tagList));
        }

        if (@$object->get('attachment') && $object->get('attachment') != null) {
            $attachements = $object->get('attachment');
            foreach ($attachements as $attachement) {
                $extendAttachementsProps = $attachement->get('_extend_props');
                if ($extendAttachementsProps == null) {
                    if ($attachement->get('name') == 'Banner') {
                        if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                            $urlImg =  $attachement->get('url');
                            $data = array_merge($data, array('profilBannerUrl' =>  $urlImg, 'profilRealUrl' => $urlImg));
                        }
                    } else {
                        if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                            $urlImg =  $attachement->get('url');
                            $data = array_merge($data, array('profilImageUrl' =>  $urlImg, 'profilMediumImageUrl' => $urlImg));
                        }
                    }
                }
            }
        }
        return $data;
    }

    /**
     * @throws Exception
     */
    public static function toElement($element)
    {
        if (@$element["fromActivityPub"] && $element["fromActivityPub"] == true) {
            $activities = Utils::activitypubToElement($element["objectId"]);
            foreach ($activities as $activityKey => $activityValue) {
                if ($activityKey != 'parent') {
                    $element[$activityKey] = $activityValue;
                }
            }
            $element["creator"] =  Type::createFromAnyValue($element['attributedTo']);
        }

        return $element;
    }

    /**
     * @param $chaine
     * @return string
     * adress activitypub cannot set as key of list so this function transform @ to -- and vis versa
     */
    public static function reverseChar($string)
    {
        return strtr($string,['@' => '--', '--' => '@']);
    }
    public static function isValidObjectId($id)
    {
        return (is_string($id) && preg_match('/^[0-9a-fA-F]{24}$/', $id) === 1);
    }
    public static function isInSameDomain($objectId, $actorId) {
        $domain1 = parse_url($objectId, PHP_URL_HOST);
        $domain2 = parse_url($actorId, PHP_URL_HOST);
        if (!$domain1 || !$domain2) {
            // throw new InvalidArgumentException("Les URLs ne sont pas valides");
        }
        return $domain1 === $domain2;
    }
    public static function createUrlByObject($object,$url){
        return Utils::getWebsiteBaseUrl($object) . $url;
    }

}
