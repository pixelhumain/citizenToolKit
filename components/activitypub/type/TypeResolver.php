<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type;

use Exception;

abstract class TypeResolver{
    public static $coreTypes = [
        'Activity', 'Collection', 'CollectionPage',
        'IntransitiveActivity', 'Link', 'ObjectType',
        'OrderedCollection', 'OrderedCollectionPage',
        'Object'
    ];

    public static $actorTypes = [
        'Application', 'Group', 'Organization', 'Person', 'Service', 'Project'
    ];

    public static $activityTypes = [
        'Accept', 'Add', 'Announce', 'Arrive', 'Block',
        'Create', 'Delete', 'Dislike', 'Flag', 'Follow',
        'Ignore', 'Invite', 'Join', 'Leave', 'Like', 'Listen',
        'Move',  'Offer', 'Question', 'Read', 'Reject', 'Remove',
        'TentativeAccept', 'TentativeReject', 'Travel', 'Undo',
        'Update', 'View',
    ];

    public static $objectTypes = [
        'Article', 'Audio', 'Document', 'Event', 'Image',
        'Mention', 'Note', 'Page', 'Place', 'Profile',
        'Relationship', 'Tombstone', 'Video', 'Badge'
    ];

    public static function getClass($type){
        $namespace = __NAMESPACE__;

        if($type == "Object")
            $type .= "Type";

        if(in_array($type, self::$coreTypes))
            $namespace .= "\core";
        else if(in_array($type, self::$activityTypes))
            $namespace .= '\extended\activity';
        else if(in_array($type, self::$actorTypes))
            $namespace .= '\extended\actor';
        else if(in_array($type, self::$objectTypes))
            $namespace .= '\extended\object';
        else{
            $type = "AnonymousType";
            $namespace .= "\core";
            //throw new Exception("Undefined scope for type '$type'");
        }

        return $namespace.'\\'.$type;
    }

    public static function isAnonymousType($type){
        return !(
            $type == "Object" ||
            in_array($type, array_merge(
                self::$coreTypes,
                self::$actorTypes,
                self::$activityTypes,
                self::$objectTypes
            )));
    }
}
