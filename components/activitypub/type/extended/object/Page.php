<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object;

class Page extends Document
{
    /**
     * @var string
     */
    protected $type = 'Page';
}
