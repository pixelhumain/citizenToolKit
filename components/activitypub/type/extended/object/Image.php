<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object;

class Image extends Document
{
    /**
     * @var string
     */
    protected $type = 'Image';
}
