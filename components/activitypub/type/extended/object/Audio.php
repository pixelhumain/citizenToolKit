<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object;

class Audio extends Document
{
    /**
     * @var string
     */
    protected $type = 'Audio';
}
