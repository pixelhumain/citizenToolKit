<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\ObjectType;

class Event extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Event';
}
