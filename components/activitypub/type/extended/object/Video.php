<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object;

class Video extends Document
{
    /**
     * @var string
     */
    protected $type = 'Video';
}
