<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Link;

class Mention extends Link
{
    /**
     * @var string
     */
    protected $type = 'Mention';
}
