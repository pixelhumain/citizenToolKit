<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\ObjectType;

/**
 * https://forgefed.org/ns#components 
 */
class Project extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Project';
    

}
