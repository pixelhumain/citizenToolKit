<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\IntransitiveActivity;

class Question extends IntransitiveActivity
{
    /**
     * @var string
     */
    protected $type = 'Question';

    /**
     * An exclusive option for a Question
     * Use of oneOf implies that the Question can have only a
     * single answer.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-oneof
     *
     * @var  array<\ActivityPhp\Type\Core\ObjectType>
     *     | array<\ActivityPhp\Type\Core\Link>
     *     | null
     */
    protected $oneOf;

    /**
     * An inclusive option for a Question.
     * Use of anyOf implies that the Question can have multiple answers.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-anyof
     *
     * @var  array<\ActivityPhp\Type\Core\ObjectType>
     *     | array<\ActivityPhp\Type\Core\Link>
     *     | null
     */
    protected $anyOf;

    /**
     * Indicates that a question has been closed, and answers are no
     * longer accepted.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-closed
     *
     * @var  \ActivityPhp\Type\Core\ObjectType
     *     | \ActivityPhp\Type\Core\Link
     *     | \DateTime
     *     | bool
     *     | null
     */
    protected $closed;

    public function setOneOf($value){
        $this->set("oneOf", $value);
    }

    public function setAnyOf($value){
        $this->set("anyOf", $value);
    }

    public function setClosed($value){
        $this->set("closed", $value);
    }
}
