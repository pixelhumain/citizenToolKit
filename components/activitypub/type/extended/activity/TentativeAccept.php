<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity;

class TentativeAccept extends Accept
{
    /**
     * @var string
     */
    protected $type = 'TentativeAccept';
}
