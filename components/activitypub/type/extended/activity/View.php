<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;

class View extends Activity
{
    /**
     * @var string
     */
    protected $type = 'View';
}
