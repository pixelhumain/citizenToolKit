<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity;

class Invite extends Offer
{
    /**
     * @var string
     */
    protected $type = 'Invite';
}
