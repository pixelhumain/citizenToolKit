<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\IntransitiveActivity;

class Travel extends IntransitiveActivity
{
    /**
     * @var string
     */
    protected $type = 'Travel';
}
