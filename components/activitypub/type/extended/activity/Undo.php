<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;

class Undo extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Undo';
}
