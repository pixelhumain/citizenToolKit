<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity;

class TentativeReject extends Reject
{
    /**
     * @var string
     */
    protected $type = 'TentativeReject';
}
