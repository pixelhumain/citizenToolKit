<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;


/**
 * https://forgefed.org/ns#components 
 */
class Project extends AbstractActor
{
    /**
     * @var string
     */
    protected $type = 'Project';
}
