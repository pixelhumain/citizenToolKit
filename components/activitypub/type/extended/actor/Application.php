<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;

class Application extends AbstractActor
{
    /**
     * @var string
     */
    protected $type = 'Application';
}
