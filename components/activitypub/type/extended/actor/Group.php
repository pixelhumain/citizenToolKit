<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;

class Group extends AbstractActor
{
    /**
     * @var string
     */
    protected $type = 'Group';
}
