<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type as TypeType;
class Activity extends AbstractActivity{
    /**
     * @var string
     */
    protected $type = 'Activity';

    /**
     * Describes the direct object of the activity.
     * For instance, in the activity "John added a movie to his
     * wishlist", the object of the activity is the movie added.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-object-term
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | null
     */
    protected $object;

    public function setObject($value){
        $this->set("object", $value);
    }

    public function getObject(){
        return TypeType::createFromAnyValue($this->get("object"));
    }
}