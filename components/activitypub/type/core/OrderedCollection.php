<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core;

class OrderedCollection extends Collection{
    /**
     * @var string
     */
    protected $type = 'OrderedCollection';
}