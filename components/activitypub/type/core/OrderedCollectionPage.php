<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core;

class OrderedCollectionPage extends CollectionPage{
    /**
     * @var string
     */
    protected $type = 'OrderedCollectionPage';

    /**
     * A non-negative integer value identifying the relative position
     * within the logical view of a strictly ordered collection.
     *
     * @var int
     */
    protected $startIndex;

    public function setStartIndex($value){
        $this->set("startIndex", $value);
    }
}