<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core;

class CollectionPage extends Collection{
    /**
     * @var string
     */
    protected $type = 'CollectionPage';

    /**
     * @var string
     */
    protected $id;

    /**
     * Identifies the Collection to which CollectionPage objects items
     * belong.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-partof
     *
     * @var string
     *    | Link
     *    | Collection
     *    | null
     */
    protected $partOf;

    /**
     * Indicates the next page of items.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-next
     *
     * @var string
     *    | Link
     *    | CollectionPage
     *    | null
     */
    protected $next;

    /**
     * Identifies the previous page of items.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-prev
     *
     * @var string
     *    | Link
     *    | CollectionPage
     *    | null
     */
    protected $prev;

    public function setPartOf($value){
        $this->set("partOf", $value);
    }

    public function setNext($value){
        $this->set("next", $value);
    }

    public function setPrev($value){
        $this->set("prev", $value);
    }
}