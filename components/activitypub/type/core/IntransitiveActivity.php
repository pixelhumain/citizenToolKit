<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core;

class IntransitiveActivity extends AbstractActivity{
    /**
     * @var string
     */
    protected $type = 'IntransitiveActivity';
}