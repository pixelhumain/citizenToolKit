<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type;

abstract class AbstractObject{
    protected $_context = "https://www.w3.org/ns/activitystreams";
    protected $_extend_props = [];

    public function set_context($value){
        $this->set("_context", $value);
    }

    public function set_extend_props($name, $value){
        $this->_extend_props[$name] = $this->transform($value);
    }

    private function transform($value){
        $is_multi = is_int(key($value));
        
        if(is_array($value)){
            if(isset($value["type"]))
                return Type::create($value);
            else if($is_multi)
                return array_map(
                    static function($item){
                        return (is_array($item) && isset($item["type"]) 
                            ? Type::create($item)
                            :$item
                        );
                    },
                    $value
                );
            else
                return $value;
        }else
            return $value;
    }

    public function set($name, $value){
        if(property_exists($this, $name))
            $this->$name = $this->transform($value);
        else if($name == "@context")
            $this->_context = $value;
        else
            $this->_extend_props[$name] = $this->transform($value);

        return $this;
    }

    public function setMultiple($values){
        foreach($values as $name => $value)
            $this->set($name, $value);
    }

    public function get($name){
        return property_exists($this, $name)?$this->$name:null;
    }

    public function toArray(){
        $allProperties = array_merge(get_object_vars($this), $this->_extend_props);
        unset($allProperties["_extend_props"]);
        unset($allProperties["_context"]);

        $output = [];
        if(!is_null($this->_context))
            $output["@context"] = $this->_context;

        foreach($allProperties as $name=>$value){
            if(is_null($value))
                continue;
            else if($value instanceof self)
                $output[$name] = $value->toArray();
            else if(is_array($value) && is_int(key($value))){
                $output[$name] = array_map(
                    static function($item_value){
                        return $item_value instanceof self
                               ? $item_value->toArray()
                               : $item_value;
                    },
                    $value
                );
            }else
                $output[$name] = $value;
                
        }

        return $output;
    }
}