<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type;

use Exception;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;

abstract class Type
{
    public static function create($type, array $attributes = []): AbstractObject
    {
        if (is_array($type)) {
            if (!isset($type['type'])) {
                throw new Exception("Type parameter must have a 'type' key");
            }
            $attributes = $type;
        }

        if (isset($attributes["id"]) && !isset($attributes["url"])) {
            $attributes["url"] = $attributes["id"];
        }
        if (!is_string($type) && !is_array($type)) {
            throw new Exception('Type parameter must be a string or an array. Given=' . gettype($type));
        }

        if (is_array($type)) {
            if (!isset($type['type'])) {
                throw new Exception("Type parameter must have a 'type' key");
            }
            $attributes = $type;
        }

        $type = is_array($type) ? $type["type"] : $type;
        $class = TypeResolver::getClass($type);
        $class = new $class();
        if (TypeResolver::isAnonymousType($type))
            $class->set("type", $type);

        foreach ($attributes as $name => $value) {
            if ($name == "content")
                $value = str_replace("\n", "</br>", $value);

            $class->set($name, $value);
        }

        return $class;
    }

    public static function createFromAnyValue($value)
    {
        if ($value instanceof AbstractObject)
            return $value;
        if (is_object($value) && property_exists($value, 'object')) {
            return self::createFromAnyValue($value->get('object'));
        }
        if (is_array($value) && isset($value["type"]))
            return self::create($value["type"], $value);

        if (is_string($value) && filter_var($value, FILTER_VALIDATE_URL)) {
            if ($localObject = ActivitypubObject::getObjectByIdAsArray($value)) {
                $typeArray = $localObject;
            } else if ($localActivity = ActivitypubActivity::getByActivityId($value)) {
                $typeArray = $localActivity;
            } else
                $typeArray = Request::get($value);

            if (isset($typeArray["type"]))
                return self::create($typeArray["type"], $typeArray);
        }
        return null;
    }
}
