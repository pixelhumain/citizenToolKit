<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub;

use Yii;

class Config
{
    public static function SCHEMA()
    {
        return self::getValue('schema', 'https');
    }

    public static function HOST()
    {
        return self::getValue('host', 'communecter.org');
    }

    private static function getValue($key, $default)
    {

        if (isset(Yii::app()->params['activitypub'][$key])) {
            return Yii::app()->params['activitypub'][$key];
        }
        return $default;
    }
}
