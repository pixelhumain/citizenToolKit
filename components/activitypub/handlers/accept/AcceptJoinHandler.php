<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\accept;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Yii;

class AcceptJoinHandler extends AbstractHandler
{
    public function __construct(Activity $activity)
    {
        parent::__construct($activity);
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== Utils::PUBLIC_INBOX) {
                $targets[] = Type::createFromAnyValue($id);
            }
        }

        $object = Type::createFromAnyValue($this->activity->get("object"));
        $target = Type::createFromAnyValue($object->get("target"));
        $actor = Type::createFromAnyValue($object->get("actor"));
        $instrument = $this->activity->get('instrument');
        $UUIDS = ActivitypubActivity::save($this->activity, $targets);

        ActivitypubLink::acceptProjectLink("contributors", $target,  $actor, $UUIDS["activity"], $instrument);
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        if (Utils::isMobilizonInstance($this->activity->get("actor"))) {

            // $mocked_payload = self::mockPayload($this->activity->get("target"));
            $targets = [];
            $object = $this->activity->get("object");
            $url = parse_url($object->get('object'));
            $host = $url['host'];
            $objectId = str_replace($host, 'somemobilizon.instance', $object->get("object"));
            $res = PHDB::findOne(ActivitypubObject::COLLECTION, ["object.id" => $objectId]);
            $participant = [];
            if (!isset($res["payload"])) {
                $participant = array($object->get("actor"));
                PHDB::update(ActivitypubObject::COLLECTION, ["object.id" => $objectId], [
                    '$push' => [
                        "payload" => $participant
                    ]
                ]);
            } else {
                $participant = Utils::addIfNotInArray($res["payload"], array($object->get("actor")));
                PHDB::update(ActivitypubObject::COLLECTION, ["object.id" => $objectId], [
                    '$set' => [
                        "payload" => $participant
                    ]
                ]);
            }
            ActivitypubActivity::save($this->activity, $targets);
        } else {
            $acceptActivity = Type::createFromAnyValue($this->activity->get("object"));
            $actor = Type::createFromAnyValue($acceptActivity->get("actor"));
            $target = Type::createFromAnyValue($this->activity->get("target"));
            $UUIDS = ActivitypubActivity::save($this->activity, []);
            ActivitypubLink::acceptProjectLink("contributors", $target, $actor, $UUIDS["activity"], $this->activity->get("instrument"));
            // notify user
            ActivityNotification::send($this->activity, $this->actor);
        }
    }
    private function mockPayload($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return array(
                "name" => $basename,
                "type" => "group"
            );
        } else if (strpos($url, 'projects') !== false) {
            return array(
                "name" => $basename,
                "type" => "projects"
            );
        } else {
            return array(
                "name" => $basename,
                "type" => "person"
            );
        }
    }
}
