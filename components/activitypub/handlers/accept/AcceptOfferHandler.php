<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\accept;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Badge, PHDB,Person;
class AcceptOfferHandler extends AbstractHandler{
    public function __construct(Activity $activity, $payload = NULL){
        parent::__construct($activity);
        $object = Type::createFromAnyValue($activity->get("object"));
        $this->target = Type::createFromAnyValue($object->get("actor"));
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {


        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== Utils::PUBLIC_INBOX) {
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $target = Type::createFromAnyValue($object->get("target"));
        $activty = Type::createFromAnyValue($object->get("object"));
        ActivitypubActivity::save($this->activity, $targets);
        $instrument = $this->activity->get('instrument');
        $entity = PHDB::findOne(Badge::COLLECTION, [
            "objectId" => $activty->get('id')
        ]);


        if(isset($entity['activitypub']['community']) && count($entity['activitypub']['community']) > 0) {
            $communities = $entity['activitypub']['community'];
            foreach($communities as $key => $value)
            {
                if(($value['receptor'] == $target->get('id') && $value['object'] == $object->get('id')) || ($value['receptor'] == $target->get('id') && $value['object'] == $activty->get('id'))){
                    if($instrument['confirmType'] == 'emetteur'){
                        unset($communities[$key]['attenteEmetteur']);
                        unset($communities[$key]['attenteRecepteur']);
                    }else if($instrument['confirmType'] == 'recepteur'){
                        unset($communities[$key]['attenteRecepteur']);
                    }
                }
            }
            PHDB::update(
                Badge::COLLECTION,
                [
                    "objectId" => $activty->get('id'),
                ],
                [
                    '$set' => [
                        'activitypub.community' => $communities
                    ]
                ]
            );
        }
        $user = PHDB::findOne(Person::COLLECTION, [
            'username' => $target->get('preferredUsername')
        ]);
        if(isset($user['activitypub']['badges'])) {
            $userBadges = $user['activitypub']['badges'];


            foreach($userBadges as $key => $value)
            {
                if(($value['receptor'] == $target->get('id') && $value['object'] == $activty->get('id')) || ($value['receptor'] == $target->get('id') && $value['object'] == $object->get('id'))){
                    if($instrument['confirmType'] == 'emetteur'){
                        unset($userBadges[$key]['attenteEmetteur']);
                        unset($userBadges[$key]['attenteRecepteur']);
                    }else if($instrument['confirmType'] == 'recepteur'){
                        unset($userBadges[$key]['attenteRecepteur']);
                    }
                }

            }
            PHDB::update(
                Person::COLLECTION,
                [
                    'username' => $target->get('preferredUsername')
                ],
                [
                    '$set' => [
                        'activitypub.badges' => $userBadges
                    ]
                ]
            );
        }
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer(){
        $acceptActivity = Type::createFromAnyValue($this->activity->get("object"));
        $object = Type::createFromAnyValue($acceptActivity->get("object"));
        $actvity = Type::createFromAnyValue($object->get("object"));
        $target = Type::createFromAnyValue($acceptActivity->get('target'));
        $UUIDS = ActivitypubActivity::save($this->activity, []);
        $instrument = $this->activity->get('instrument');
        $entity = PHDB::findOne(Badge::COLLECTION, [
            "objectId" => $object->get('id')
        ]);
        if(isset($entity['activitypub']['community']) && count($entity['activitypub']['community']) > 0) {
            $communities = $entity['activitypub']['community'];
            foreach($communities as $key => $value)
            {
                if(($value['receptor'] == $acceptActivity->get('target') && $value['object'] == $object->get('id')) && ($value['receptor'] == $acceptActivity->get('target') && $value['object'] == $actvity->get('id'))){

                    if($instrument['confirmType'] == 'emetteur'){
                        unset($communities[$key]['attenteEmetteur']);
                        unset($communities[$key]['attenteRecepteur']);
                    }else if($instrument['confirmType'] == 'recepteur'){
                        unset($communities[$key]['attenteRecepteur']);
                    }
                }
            }
            PHDB::update(
                Badge::COLLECTION,
                [
                    "objectId" => $object->get('id'),
                ],
                [
                    '$set' => [
                        'activitypub.community' => $communities
                    ]
                ]
            );
        }
        $user = PHDB::findOne(Person::COLLECTION, [
            'username' => $target->get('preferredUsername')
        ]);
        if(isset($user['activitypub']['badges'])) {
            $userBadges = $user['activitypub']['badges'];
            foreach($userBadges as $key => $value)
            {
                if(($value['receptor'] == $target->get('id') && $value['object'] == $object->get('id'))  || $value['receptor'] == $target->get('id') && $value['object'] == $actvity->get('id')){
                    if($instrument['confirmType'] == 'emetteur'){
                        unset($userBadges[$key]['attenteEmetteur']);
                        unset($userBadges[$key]['attenteRecepteur']);
                    }else if($instrument['confirmType'] == 'recepteur'){
                        unset($userBadges[$key]['attenteRecepteur']);
                    }
                }
            }
            PHDB::update(
                Person::COLLECTION,
                [
                    'username' => $target->get('preferredUsername')
                ],
                [
                    '$set' => [
                        'activitypub.badges' => $userBadges
                    ]
                ]
            );
        }
        ActivityNotification::send($this->activity, $this->actor);
    }
}
