<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\accept;

use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Preference;

class AcceptFollowHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
        $object = Type::createFromAnyValue($activity->get("object"));
        $this->target = Type::createFromAnyValue($object->get("actor"));
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        ActivitypubActivity::save($this->activity, [$this->target]);

        ActivitypubLink::approveLink(
            "followers",
            $this->actor->get("preferredUsername"),
            $this->target,
            $this->payload
        );

        //federate activity
        Request::post(
            $this->actor->get("id"),
            $this->target->get("inbox"),
            $this->activity->toArray(),
            true
        );
    }

    protected function handleActivityFromServer()
    {  
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
