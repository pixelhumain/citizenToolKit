<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\accept;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Person;
use PHDB;
class AcceptBadgeHandler extends AbstractHandler{
    public function __construct(Activity $activity, $payload = NULL){
        parent::__construct($activity);
        $this->object = Type::createFromAnyValue($activity->get("object"));
        $this->target = Type::createFromAnyValue($activity->get("target"));
        $this->payload = $payload;
    }

    protected function handleActivityFromClient(){
        $actor =  Type::createFromAnyValue($this->object->get("actor"));
        ActivitypubActivity::save($this->activity, []);


        PHDB::update(
            Person::COLLECTION,
            [
                "username" => $this->target->get('preferredUsername'),
                "activitypub.badges.object" => $this->object->get("id")
            ],
            [
                '$unset' => [
                    'activitypub.badges.$.attenteRecepteur' => ''
                ]
            ]
        );
        //federate activity
        Request::post(
            $this->actor->get("id"),
            $actor->get("inbox"),
            $this->activity->toArray(),
            true
        );
    }

    protected function handleActivityFromServer(){
        ActivityNotification::send($this->activity, $this->actor);
    }
}
