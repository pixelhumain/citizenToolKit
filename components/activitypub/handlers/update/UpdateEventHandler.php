<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\update;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
class UpdateEventHandler extends UpdateHandler{
    public function __construct(Activity $activity){
        parent::__construct($activity);
    }
}