<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\update;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;

class UpdatePersonHandler extends UpdateHandler{
    public function __construct(Activity $activity){
        parent::__construct($activity);
    }
}