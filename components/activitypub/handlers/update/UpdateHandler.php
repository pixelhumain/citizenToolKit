<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\update;
use Event,PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;

class UpdateHandler extends AbstractHandler{
    public function __construct(Activity $activity){
        parent::__construct($activity);
    }

    protected function handleActivityFromClient(){
        $targets = [];
        $targetIds = [];
        if(is_array($this->activity->get("to")))
            $targetIds = array_merge($this->activity->get("to"));
        if(is_array($this->activity->get("cc")))
            $targetIds = array_merge($this->activity->get("cc"));

        foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX){
                $targets[] = Type::createFromAnyValue($id);
            }
        }

        ActivitypubActivity::save($this->activity, $targets);

        //group targets by domain name
        $targetInboxes = [];
        foreach($targets as $target){
            if($target){
                if(is_array($target->get("endpoints")) && isset($target->get("endpoints")["sharedInbox"])  && strpos($target->get("endpoints")["sharedInbox"], "@")!==false){
                    if(!in_array($target->get("endpoints")["sharedInbox"], $targetInboxes))
                        $targetInboxes[] = $target->get("endpoints")["sharedInbox"];
                }else{
                    $targetInboxes[] = $target->get("inbox");
                }
            }
        }

        foreach($targetInboxes as $inbox){
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer(){
        $targets = [];
        $targetIds = [];
        if(is_array($this->activity->get("to")))
            $targetIds = array_merge($this->activity->get("to"));
        if(is_array($this->activity->get("cc")))
            $targetIds = array_merge($this->activity->get("cc"));

        foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX){
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("object"));
       if ($object->get("type") == "Event") {
            $data = Utils::parseToEvent($object,null);
             PHDB::update(Event::COLLECTION, ["objectId" => $object->get("id")],array('$set' => $data ) );
        }
        ActivitypubActivity::save($this->activity, $targets);
         // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
