<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers;

use Exception;

class Webfinger{
     /**
     * @var string
     */
    private $subject;

    /**
     * @var string[]
     */
    private $aliases = [];

    /**
     * @var array
     */
    private $links = [];

    /**
     * Set subject property
     *
     * @param string $subject
     */
    public function setSubject($subject)
    {
        if (!is_string($subject)) {
            throw new Exception(
                "WebFinger subject must be a string"
            );
        }

        $this->subject = $subject;
    }

    /**
     * Set aliases property
     *
     * @param array $aliases
     */
    public function setAliases(array $aliases)
    {
        foreach ($aliases as $alias) {
            if (!is_string($alias)) {
                throw new Exception(
                    "WebFinger aliases must be an array of strings"
                );
            }

            $this->aliases[] = $alias;
        }
    }

    /**
     * Set links property
     *
     * @param array $links
     */
    public function setLinks(array $links)
    {
        foreach ($links as $link) {
            if (!is_array($link)) {
                throw new Exception(
                    "WebFinger links must be an array of objects"
                );
            }

            if (!isset($link['rel'])) {
                throw new Exception(
                    "WebFinger links object must contain 'rel' property"
                );
            }

            $tmp = [];
            $tmp['rel'] = $link['rel'];

            foreach (['type', 'href', 'template'] as $key) {
                if (isset($link[$key]) && is_string($link[$key])) {
                    $tmp[$key] = $link[$key];
                }
            }

            $this->links[] = $tmp;
        }
    }

    /**
     * Get aliases
     *
     * @return array
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * Get links
     *
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Get subject fetched from profile
     *
     * @return null|string Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Get WebFinger response as an array
     *
     * @return array
     */
    public function toArray()
    {

        return [
            'subject' => $this->subject,
            'aliases' => $this->aliases,
            'links'   => $this->links
        ];
    }

    public static function parseResource($resource){
        $pattern = '/^@?(?P<user>[\w\-\.]+)@(?P<host>[\w\.\-]+)(?P<port>:[\d]+)?$/';
        if(!preg_match($pattern, $resource, $matches))
            throw new Exception("WebFinger handle is malformed '{$resource}'", 400);
        return $matches;
    }
}
