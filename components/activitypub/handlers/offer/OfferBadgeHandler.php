<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\offer;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Badge;
use Rest;
use Person;
class OfferBadgeHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX && $id !==$this->activity->get("id")){
                $targets[] = Type::createFromAnyValue($id);
            }
        }

        $object = Type::createFromAnyValue($this->activity->get("object"));
        $target = Type::createFromAnyValue($this->activity->get("target"));
        $UUIDS = ActivitypubActivity::save($this->activity, $targets);
        // save badge in activitypub collection

        $instrument = $this->activity->get("instrument");

        $data = array(
            'activityId' =>  $UUIDS["activity"],
            'show' => true,
            'isParcours' => false,
            'receptor' => $target->get('id'),
            'object' => $object->get('id'),
            'type' => 'badges'
        );
        if(isset($instrument['narrative'])) $data['narrative'] = $instrument['narrative'];
        if(isset($instrument['evidences'])) $data['evidences'] = $instrument['evidences'];
        if(isset($instrument['expiredOn'])) $data['expiredOn'] = $instrument['expiredOn'];
        if(isset($instrument['attenteEmetteur'])) $data['attenteEmetteur'] = $instrument['attenteEmetteur'];
        if(isset($instrument['attenteRecepteur'])) $data['attenteRecepteur'] = $instrument['attenteRecepteur'];
        if(!Utils::isInSameDomain($object->get('id'), $this->actor->get('id'))){
            PHDB::update(
                Person::COLLECTION,
                [
                    'username' => $this->actor->get("preferredUsername")
                ],
                [
                    '$push' => [
                        'activitypub.badges'  => $data
                    ]
                ]
            );
        }
        PHDB::update(
            Badge::COLLECTION,
            [
                "objectId" => $object->get('id'),
            ],
            [
                '$push' => [
                    'activitypub.community'  => $data
                ]
            ]
        );
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }

    }

    protected function handleActivityFromServer()
    {
        $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
        $target = Type::createFromAnyValue($this->activity->get("target"));
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $UUIDS = ActivitypubActivity::save($this->activity, [$targets]);
        $instrument = $this->activity->get("instrument");
        $data = array(
            'activityId' =>  $UUIDS["activity"],
            'show' => true,
            'isParcours' => false,
            'object' => $object->get('id'),
            'receptor' => $target->get('id'),
            'type' => 'badges'
        );
        if(isset($instrument['narrative'])) $data['narrative'] = $instrument['narrative'];
        if(isset($instrument['evidences'])) $data['evidences'] = $instrument['evidences'];
        if(isset($instrument['expiredOn'])) $data['expiredOn'] = $instrument['expiredOn'];
        if(isset($instrument['attenteEmetteur'])) $data['attenteEmetteur'] = $instrument['attenteEmetteur'];
        if(isset($instrument['attenteRecepteur'])) $data['attenteRecepteur'] = $instrument['attenteRecepteur'];
        PHDB::update(
            Person::COLLECTION,
            [
                'username' => $target->get("preferredUsername")
            ],
            [
                '$push' => [
                    'activitypub.badges'  => $data
                ]
            ]
        );
        PHDB::update(
            Badge::COLLECTION,
            [
                "objectId" => $object->get('id'),
            ],
            [
                '$push' => [
                    'activitypub.community'  => $data
                ]
            ]
        );

        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
