<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\offer;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Rest;

class OfferPersonHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX){
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $target = Type::createFromAnyValue($this->activity->get("target"));
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $instrument = $this->activity->get('instrument');
        $UUIDS = ActivitypubActivity::save($this->activity, $targets);
        ActivitypubLink::acceptProjectLink("contributors",   $target, $object, $UUIDS["activity"],$instrument);
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }

    }

    protected function handleActivityFromServer()
    {
        $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
         //  $mocked_payload = self::mockPayload($this->activity->get("target"));
         $target = Type::createFromAnyValue($this->activity->get("target"));
         $object = Type::createFromAnyValue($this->activity->get("object"));
         $UUIDS = ActivitypubActivity::save($this->activity, [$target]);
         ActivitypubLink::acceptProjectLink("contributors",  $target, $object, $UUIDS["activity"], $this->activity->get("instrument"));
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
