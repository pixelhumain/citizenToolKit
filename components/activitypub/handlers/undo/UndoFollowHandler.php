<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\undo;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;

class UndoFollowHandler extends AbstractHandler{
    public function __construct(Activity $activity,$payload = NULL){
        parent::__construct($activity);
        $object = Type::createFromAnyValue($activity->get("object"));
        $this->target = Type::createFromAnyValue($object->get("object"));
        $this->payload = $payload;
    }

    protected function handleActivityFromClient(){
        ActivitypubActivity::save($this->activity, [$this->target]);

        ActivitypubLink::deleteLink(
            "follows",
            $this->actor->get("preferredUsername"),
            $this->target,
            $this->payload
        );
        Request::post(
            $this->actor->get("id"),
            $this->target->get("inbox"),
            $this->activity->toArray(),
            true
        );
    }

    protected function handleActivityFromServer(){
        $activty= $this->activity->toArray();
        $object=$activty['object'];
        $objectValue= $object['object'];
        $mocked_payload = self::mockPayload($object['object']);
        ActivitypubActivity::save($this->activity, [$this->target]);
        ActivitypubLink::deleteLink(
            "followers",
            $this->target->get("preferredUsername"),
            $this->actor,
            $mocked_payload
        );
        ActivityNotification::send($this->activity, $this->actor);
    }
    private function mockPayload($url){
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url,'groups') !== false) {
            return array(
                "name" => $basename,
                "type" => "group"
            );
        } else {
            return array(
                "name" => $basename,
                "type" => "person"
            );
        }
    }
}
