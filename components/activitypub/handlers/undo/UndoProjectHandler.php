<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\undo;

use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;

class UndoProjectHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);

        $this->payload = $payload;
    }
    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $actor = ActivitypubTranslator::coPersonToActor($user);

        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");

        foreach ($targetIds as $id) {
            if ($id !== $actor->get('id')) {
                $x = Type::createFromAnyValue($id);
                $targets[] = $x;
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("object")); // project as object
        $targetInboxes = [];
        foreach ($targets as $t) {
            $targetInboxes[] = $t->get("inbox");
        }
        ActivitypubActivity::save($this->activity, []);
        if (!empty($this->activity->get("target"))) {
            $person = $this->activity->get("target");
            ActivitypubLink::deleteProjectLink("contributors",  $object, $person);
        }
       
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }
    protected function handleActivityFromServer()
    {
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $actor = $this->activity->get("target");
        ActivitypubActivity::save($this->activity, []);
        if (!empty($this->activity->get("target"))) {
            $person = $this->activity->get("target");
            ActivitypubLink::deleteProjectLink("contributors",  $object, $person);
        }
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
