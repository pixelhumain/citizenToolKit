<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\undo;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;

class UndoAcceptHandler extends AbstractHandler{
    public function __construct(Activity $activity,$payload = NULL){
        parent::__construct($activity);
        $this->target = Type::createFromAnyValue($this->activity->get("target"));
         $this->payload = $payload;
    }

    protected function handleActivityFromClient(){
        ActivitypubActivity::save($this->activity, [$this->target]);

        ActivitypubLink::pendingLink(
            "followers",
            $this->actor->get("preferredUsername"),
            $this->target,
            $this->payload
        );

        Request::post(
            $this->actor->get("id"), 
            $this->target->get("inbox"), 
            $this->activity->toArray(), 
            true
        );
    }

    protected function handleActivityFromServer(){
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}