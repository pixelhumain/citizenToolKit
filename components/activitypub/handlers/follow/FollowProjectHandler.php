<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\follow;

use Exception;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Accept;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Yii;

class FollowProjectHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== Utils::PUBLIC_INBOX) {
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $target = Type::createFromAnyValue($object->get("target"));
        $actor = Type::createFromAnyValue($this->activity->get("actor"));
        $UUIDS = ActivitypubActivity::save($this->activity, $targets);
        $pending = boolval($object->get("manuallyApprovesFollowers"));

        ActivitypubLink::saveLinkProject("followers", $target,  $actor, $UUIDS["activity"], $this->payload, $pending);
        foreach ($targetInboxes as $inbox) {
                Request::post(
                    $this->actor->get("id"),
                    $inbox,
                    $this->activity->toArray(),
                    true
                );
            }
    }

    protected function handleActivityFromServer()
    {

       
        $target = Type::createFromAnyValue($this->activity->get("object"));
        $actor = Type::createFromAnyValue($this->activity->get("actor"));
        $UUIDS = ActivitypubActivity::save($this->activity, [$target]);
        $mock_payload = self::mockPayload($this->activity->get("object"));
        ActivitypubLink::saveLinkProject("followers", $target, $actor, $UUIDS["activity"], $mock_payload);

        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
    private function mockPayload($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return array(
                "name" => $basename,
                "type" => "group"
            );
        } else if (strpos($url, 'projects') !== false) {
            return array(
                "name" => $basename,
                "type" => "projects"
            );
        } else {
            return array(
                "name" => $basename,
                "type" => "person"
            );
        }
    }
}
