<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\follow;

use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\NotificationTransformer;
use Yii;

class FollowPersonHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $actor = Type::createFromAnyValue($this->activity->get("actor"));
        $target = $object;

        //save activity to inbox
        $UUIDS = ActivitypubActivity::save($this->activity, [$target]);
        $pending = boolval($object->get("manuallyApprovesFollowers"));
        ActivitypubLink::saveLink("follows", $actor, $object, $UUIDS["activity"], $this->payload, $pending);
        //federate activity

        Request::post(
            $this->actor->get("id"),
            $target->get("inbox"),
            $this->activity->toArray(),
            true
        );
    }

    protected function handleActivityFromServer()
    {
        $target = Type::createFromAnyValue($this->activity->get("object"));
        $UUIDS = ActivitypubActivity::save($this->activity, [$target]);
        $mocked_payload = self::mockPayload($this->activity->get("object"));
        ActivitypubLink::saveLink("followers", $target, $this->actor, $UUIDS["activity"], $mocked_payload);
         // notify user
        if(empty($this->activity->get('summary'))){
            $summary = Yii::t(
                "activitypub",
                "{who} follow you",
                array(
                    "{who}" =>$this->actor->get('preferredUsername')
                )
            );
            $this->activity->set('summary',$summary);
        }
         ActivityNotification::send($this->activity, $this->actor);
         
    }
    private function mockPayload($url): array
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return array(
                "name" => $basename,
                "type" => "group"
            );
        } else {
            return array(
                "name" => $basename,
                "type" => "person"
            );
        }
    }
}
