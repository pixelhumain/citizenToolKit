<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\remove;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;

class RemoveInviteHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
        $object = Type::createFromAnyValue($activity->get("object"));
        $this->target = Type::createFromAnyValue($object->get("object"));
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
    }

    protected function handleActivityFromServer()
    {
        $activity = ActivitypubActivity::getByActivityId($this->activity->get("object"));
        $targets = [];
        $targetIds = [];
        if (is_array($this->activity->get("to")))
            $targetIds = array_merge($this->activity->get("to"));

        foreach ($targetIds as $id) {
            if ($id !== Utils::PUBLIC_INBOX) {
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $mocked_payload = self::mockPayload($activity["target"]);
        ActivitypubLink::deleteLink(
            "followers",
            $mocked_payload['name'],
            $this->target,
            $mocked_payload
        );
        ActivitypubActivity::save($this->activity, $targets);
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
    private function mockPayload($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return array(
                "name" => $basename,
                "type" => "group"
            );
        } else {
            return array(
                "name" => $basename,
                "type" => "person"
            );
        }
    }
}
