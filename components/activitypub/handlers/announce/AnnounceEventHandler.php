<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\create;

use Event;
use PHDB;
use MongoDate;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Preference;
use Yii;

class AnnounceEventHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
        $this->payload = $payload;
    }
    protected function handleActivityFromClient()
    {
         $UUIDS = ActivitypubActivity::save($this->activity, []);
    }
    protected function handleActivityFromServer()
    {
        $targets = [];
        $to = $this->activity->get("to");
        if(Utils::isMobilizonInstance($this->actor->get("id"))){
            
        }
        if (is_array($to) && in_array(Utils::PUBLIC_INBOX, $to))
            $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $summary = Yii::t(
            "activitypub",
            "{who} has create new event '{what}'",
            array(
                "{who}" =>$this->actor->get('preferredUsername'),
                "{what}" => $object->get('name')
            )
        );
        $this->activity->set('summary',$summary);
        $UUIDS = ActivitypubActivity::save($this->activity, $targets);
        $data = Utils::parseToEvent($object, $UUIDS["object"]);
        PHDB::insert(Event::COLLECTION, $data);
        $this->pingTargets($targets);
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
    private function pingTargets($targets)
    {
        $targetIds = [];
        foreach ($targets as $target) {
            $user = PHDB::findOne(Person::COLLECTION, ["username" => $target->get("preferredUsername")]);
            if ($user && isset($user["preferences"]) && Preference::isActivitypubActivate($user["preferences"]))
                $targetIds[] = (string)$user["_id"];
        } 
       
        
        $coWs = Yii::app()->params['cows'];
        $curl = curl_init($coWs["pingNewPostUrl"]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(["users" => $targetIds]));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_exec($curl);
    }
}
