<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\invite;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;

class InvitePersonHandler extends AbstractHandler
{
  public function __construct(Activity $activity)
  {
    parent::__construct($activity);
  }

  protected function handleActivityFromClient()
  {
    $targets = [];
    $targetIds = [];
    $targetInboxes = [];
    if (is_array($this->activity->get("cc")))
      $targetIds = $this->activity->get("cc");
    foreach ($targetIds as $id) {
      if (isset($id)) {
        $x = Type::createFromAnyValue($id);
        $targetInboxes[] = $x->get('inbox');
      }
    }
    foreach ($targetIds as $id) {
      if ($id !== Utils::PUBLIC_INBOX) {
        $targets[] = Type::createFromAnyValue($id);
      }
    }
    $object = Type::createFromAnyValue($this->activity->get("target"));
    $actor = Type::createFromAnyValue($this->activity->get("object"));
    $instrument = $this->activity->get('instrument');
    $UUIDS = ActivitypubActivity::save($this->activity, $targets);
    ActivitypubLink::saveLinkForProject("contributors", $object,  $actor, $UUIDS["activity"], $instrument);
    foreach ($targetInboxes as $inbox) {
      Request::post(
        $this->actor->get("id"),
        $inbox,
        $this->activity->toArray(),
        true
      );
    }
  }
  protected function handleActivityFromServer()
  {

    // check if invitation from mobilizon
    if (Utils::isMobilizonInstance($this->activity->get("actor"))) {
      $target = Type::createFromAnyValue($this->activity->get("object"));
      $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->activity->get("attributedTo"));
      if(empty($this->activity->get('summary'))){
            $summary = \Yii::t(
                "activitypub",
                "{who} send invitation for become member of '{what}'",
                array(
                    "{who}" =>$this->actor->get('preferredUsername'),
                    "{what}" => $target->get("name")
                )
            );
            $this->activity->set('summary',$summary);
      }
      $UUIDS = ActivitypubActivity::save($this->activity, [$target]);
      $mocked_payload = self::mockPayload($this->activity->get("target"));
      ActivitypubLink::saveLink("followers", $target, $target, $UUIDS["activity"], $mocked_payload);
    } else {
      $target = Type::createFromAnyValue($this->activity->get("target"));
      $person = Type::createFromAnyValue($this->activity->get("object"));
      $mocked_payload = self::mockPayload($this->activity->get("target"));

      $UUIDS = ActivitypubActivity::save($this->activity, [$target]);
      //Todo: check type of element before save
     // if ($mocked_payload['type'] == "projects") {
        ActivitypubLink::saveLinkForProject("contributors", $target,  $person, $UUIDS["activity"], $this->activity->get("instrument"));
      //} else {
      //  return;
     // }
    }
    // notify user
    ActivityNotification::send($this->activity, $this->actor,$targets);
  }
  private function mockPayload($url)
  {
    $url_path = parse_url($url, PHP_URL_PATH);
    $basename = pathinfo($url_path, PATHINFO_BASENAME);
    if (strpos($url, 'groups') !== false) {
      return array(
        "name" => $basename,
        "type" => "group"
      );
    } else if (strpos($url, 'projects') !== false) {
      return array(
        "name" => $basename,
        "type" => "projects"
      );
    } else {
      return array(
        "name" => $basename,
        "type" => "person"
      );
    }
  }
}
