<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\join;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Rest;

class JoinPersonHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
    }

    protected function handleActivityFromClient()
    {

        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== Utils::PUBLIC_INBOX) {
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("target"));
        $actor = Type::createFromAnyValue($this->activity->get("actor"));
        $instrument = $this->activity->get('instrument');
        $UUIDS = ActivitypubActivity::save($this->activity, $targets);
        if($instrument != "follow"){
            ActivitypubLink::saveLinkForProject("contributors", $object,  $actor, $UUIDS["activity"], $instrument);
        }else{
            ActivitypubLink::saveLinkForProject("followers", $object,  $actor, $UUIDS["activity"], $instrument);
        }
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    /**
     * @throws \Exception
     */
    protected function handleActivityFromServer()
    {
        //  $mocked_payload = self::mockPayload($this->activity->get("target"));
        $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
        $target = Type::createFromAnyValue($this->activity->get("target"));
        $person = Type::createFromAnyValue($this->activity->get("object"));
        $UUIDS = ActivitypubActivity::save($this->activity, $targets);
        $instrument = $this->activity->get('instrument');
        $link = $instrument == 'follow' ? 'followers': 'contributors';
        ActivitypubLink::saveLinkForProject($link, $target,  $person, $UUIDS["activity"], $this->activity->get("instrument"));

        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
