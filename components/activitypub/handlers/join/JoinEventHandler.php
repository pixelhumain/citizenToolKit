<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\join;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Yii;

class JoinEventHandler extends AbstractHandler{
    public function __construct(Activity $activity, $payload=NULL){
        parent::__construct($activity);
        $this->payload = $payload;
    }
    protected function handleActivityFromClient(){
       $target = Type::createFromAnyValue($this->activity->get("attributedTo"));
        $participant= array($this->activity->get("actor"));
        ActivitypubActivity::save($this->activity, [$target], $participant);
        Request::post(
            $this->activity->get("actor"), 
            $target->get('inbox'),
            $this->activity->toArray(),
            true
        );
    }
    protected function handleActivityFromServer(){
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}