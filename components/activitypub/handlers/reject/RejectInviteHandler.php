<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\reject;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;

class RejectInviteHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        if (Utils::isMobilizonInstance($this->activity->get("object"))) {

            $object = Type::createFromAnyValue($this->activity->get("object"));
            $target = Type::createFromAnyValue($object->get("object"));
            ActivitypubActivity::save($this->activity, [], $this->payload);
            ActivitypubLink::deleteLink(
                "followers",
                $this->actor->get("preferredUsername"),
                $target,
                $this->payload
            );
            //federate activity
            Request::post(
                $this->actor->get("id"),
                $target->get("inbox"),
                $this->activity->toArray(),
                true
            );
        } else {
            $targets = [];
            $targetIds = [];
            $targetInboxes = [];
            if (is_array($this->activity->get("cc")))
                $targetIds = $this->activity->get("cc");
            foreach ($targetIds as $id) {
                if (isset($id)) {
                    $x = Type::createFromAnyValue($id);
                    $targetInboxes[] = $x->get('inbox');
                }
            }
            foreach ($targetIds as $id) {
                if ($id !== Utils::PUBLIC_INBOX) {
                    $targets[] = Type::createFromAnyValue($id);
                }
            }
            $object = Type::createFromAnyValue($this->activity->get("object"));
            $target = Type::createFromAnyValue($object->get("target"));
            $actor = Type::createFromAnyValue($this->activity->get("actor"));
            $UUIDS = ActivitypubActivity::save($this->activity, $targets);
            ActivitypubLink::deleteProjectLink("contributors", $target,  $actor, $UUIDS["activity"]);
            foreach ($targetInboxes as $inbox) {
                Request::post(
                    $this->actor->get("id"),
                    $inbox,
                    $this->activity->toArray(),
                    true
                );
            }
        }
    }

    protected function handleActivityFromServer()
    {
        $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
        $acceptActivity = Type::createFromAnyValue($this->activity->get("object"));
        $actor = Type::createFromAnyValue($this->activity->get("actor"));
        $target = Type::createFromAnyValue($this->activity->get("target"));
        $UUIDS = ActivitypubActivity::save($this->activity, []);
        ActivitypubLink::deleteProjectLink("contributors", $target, $actor, $UUIDS["activity"]);
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
