<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\reject;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Yii;

class RejectJoinHandler extends AbstractHandler
{
    public function __construct(Activity $activity)
    {
        parent::__construct($activity);
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== Utils::PUBLIC_INBOX) {
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $target = Type::createFromAnyValue($object->get("target"));
        $actor = Type::createFromAnyValue($object->get("actor"));
        $UUIDS = ActivitypubActivity::save($this->activity, $targets);
        ActivitypubLink::deleteProjectLink("contributors", $target,  $actor, $UUIDS["activity"]);
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        $object = $this->activity->get("object");
        $acceptActivity = Type::createFromAnyValue($object);
        $actor = Type::createFromAnyValue($acceptActivity->get("actor"));
        $target = Type::createFromAnyValue($this->activity->get("target"));
        $UUIDS = ActivitypubActivity::save($this->activity, []);


        if (Utils::isMobilizonInstance($this->activity->get("actor"))) {

            /**$objectId = null;
            $activityToArray = $acceptActivity->toArray();

            PHDB::insert('dump_ap',array('armel' => $activityToArray));
            if (is_object($object) && property_exists($object, 'object')) {
                $objectId = $object->get('object');
                $participant= Utils::removeIfExistInArray($activityToArray["payload"],array($object->get("actor")));
            }else{
                $participant= Utils::removeIfExistInArray($activityToArray["payload"],array($object->get("actor")));

                $objectId = $object;
            }
            if(ActivitypubLink::isElementExist(ActivitypubObject::COLLECTION,["object.id"=>$objectId])){
                PHDB::update(ActivitypubObject::COLLECTION, ["object.id"=>$objectId], [
                    '$set' => [
                        "payload" => count($participant) == 0 ? array() : $participant
                    ]
                ]);

            }**/
        }else{
            ActivitypubLink::deleteProjectLink("contributors", $target, $actor, $UUIDS["activity"]);

        }
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
