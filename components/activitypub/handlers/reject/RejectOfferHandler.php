<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\reject;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Yii;
use Person;
use Badge;
class RejectOfferHandler extends AbstractHandler
{
    public function __construct(Activity $activity)
    {
        parent::__construct($activity);
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== Utils::PUBLIC_INBOX) {
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $target = Type::createFromAnyValue($object->get("target"));
        $activty = Type::createFromAnyValue($object->get("object"));
        ActivitypubActivity::save($this->activity, $targets);
        $instrument = $this->activity->get('instrument');
        $entity = PHDB::findOne(Badge::COLLECTION, [
            "objectId" => $activty->get('id')
        ]);
        if(isset($entity['activitypub']['community']) && count($entity['activitypub']['community']) > 0) {
            $communities = $entity['activitypub']['community'];
            foreach($communities as $key => $value)
            {
                if($value['receptor'] == $target->get('id') && $value['object'] == $activty->get('id')){
                    $communities[$key]['revoke'] = true;
                    if(isset($instrument['revokeAuthor'])){
                        $communities[$key]['revokeAuthor'] = $instrument['revokeAuthor'];
                    }
                    if(isset($instrument['revokeReason'])){
                        $communities[$key]['revokeReason'] = $instrument['revokeReason'];
                    }
                }
            }
            PHDB::update(
                Badge::COLLECTION,
                [
                    "objectId" => $activty->get('id'),
                ],
                [
                    '$set' => [
                        'activitypub.community' => $communities
                    ]
                ]
            );
        }
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        $rejectActivity = Type::createFromAnyValue($this->activity->get("object"));
        $object = Type::createFromAnyValue($rejectActivity->get("object"));
        $target = Type::createFromAnyValue($rejectActivity->get('target'));
        $UUIDS = ActivitypubActivity::save($this->activity, []);
        $instrument = $this->activity->get('instrument');
        $entity = PHDB::findOne(Badge::COLLECTION, [
            "objectId" => $object->get('id')
        ]);
        if(isset($entity['activitypub']['community']) && count($entity['activitypub']['community']) > 0) {
            $communities = $entity['activitypub']['community'];
            foreach($communities as $key => $value)
            {
                if($value['receptor'] == $target->get('id') && $value['object'] == $object->get('id')){
                    $dumpArray = array('ici' => 'community');
                    Yii::app()->mongodb->selectCollection('dump_ap')->insert($dumpArray);
                    $communities[$key]['revoke'] = true;
                    if(isset($instrument['revokeAuthor'])){
                        $communities[$key]['revokeAuthor'] = $instrument['revokeAuthor'];
                    }
                    if(isset($instrument['revokeReason'])){
                        $communities[$key]['revokeReason'] = $instrument['revokeReason'];
                    }
                }
            }
            PHDB::update(
                Badge::COLLECTION,
                [
                    "objectId" => $object->get('id'),
                ],
                [
                    '$set' => [
                        'activitypub.community' => $communities
                    ]
                ]
            );
        }
        $user = PHDB::findOne(Person::COLLECTION, [
            'username' => $target->get('preferredUsername')
        ]);
        if(isset($user['activitypub']['badges'])) {
            $userBadges = $user['activitypub']['badges'];
            foreach($userBadges as $k => $v)
            {
                if($v['receptor'] == $target->get('id') && $v['object'] == $object->get('id')){
                    $userBadges[$k]['revoke'] = true;
                    if(isset($instrument['revokeAuthor'])){
                        $userBadges[$k]['revokeAuthor'] = $instrument['revokeAuthor'];
                    }
                    if(isset($instrument['revokeReason'])){
                        $userBadges[$k]['revokeReason'] = $instrument['revokeReason'];
                    }
                }
            }
            PHDB::update(
                Person::COLLECTION,
                [
                    'username' => $target->get('preferredUsername')
                ],
                [
                    '$set' => [
                        'activitypub.badges' => $userBadges
                    ]
                ]
            );
        }

        ActivityNotification::send($this->activity, $this->actor);
    }
}
