<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\delete;

use Badge;
use Event;
use MongoDate;
use News;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Project;

class DeleteTombstoneHandler extends AbstractHandler{
    public function __construct(Activity $activity){
        parent::__construct($activity);
    }

    protected function handleActivityFromClient(){
        $object = Type::createFromAnyValue($this->activity->get("object"));

        $targets = [];
        $targetIds = [];
        if(is_array($this->activity->get("to")))
            $targetIds = array_merge($this->activity->get("to"));
        if(is_array($this->activity->get("cc")))
            $targetIds = array_merge($this->activity->get("cc"));

        foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX){
                $targets[] = Type::createFromAnyValue($id);
            }
        }

        ActivitypubActivity::save($this->activity, $targets);
        ActivitypubObject::removeByObjectId($object->get("id"));

        $this->activity->set("object", $object);

        //group targets by domain name
        $targetInboxes = [];
        foreach($targets as $target){
            if(is_array($target->get("endpoints")) && isset($target->get("endpoints")["sharedInbox"])  && strpos($target->get("endpoints")["sharedInbox"], "@")!==false){
                if(!in_array($target->get("endpoints")["sharedInbox"], $targetInboxes))
                    $targetInboxes[] = $target->get("endpoints")["sharedInbox"];
            }else{
                $targetInboxes[] = $target->get("inbox");
            }
        }

        foreach($targetInboxes as $inbox){
            Request::post(
                $this->actor->get("id"),
                $inbox, $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer(){
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $res = PHDB::findOne(ActivitypubObject::COLLECTION, ["object.id"=>$object->get("id")]);
        if($res){
            $localObject = Type::createFromAnyValue($res["object"]);
            $collection = Person::COLLECTION;
            switch ($localObject->get("type")){
                case "Note" : $collection = News::COLLECTION; break;
                case "Event" : $collection = Event::COLLECTION; break;
                case "Project" : $collection = Project::COLLECTION; break;
                case "Badge" : $collection = Badge::COLLECTION; break;
            }
            PHDB::remove($collection, ["objectId" => $localObject->get("id")]);
            PHDB::remove(ActivitypubObject::COLLECTION, ["object.id" => $localObject->get("id")]);
        }
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
