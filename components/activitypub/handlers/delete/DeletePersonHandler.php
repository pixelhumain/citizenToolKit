<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\delete;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Rest;

class DeletePersonHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX){
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $target = Type::createFromAnyValue($this->activity->get("target"));
        $object = Type::createFromAnyValue($this->activity->get("object"));
        ActivitypubActivity::save($this->activity, $targets);
        $instrument = $this->activity->get("instrument");
        if($instrument != 'follow'){
            ActivitypubLink::deleteProjectLink("contributors",   $target, $object);
        }else{
            ActivitypubLink::deleteProjectLink("followers",   $target, $object);
        }
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }

    }

    protected function handleActivityFromServer()
    {
         //  $mocked_payload = self::mockPayload($this->activity->get("target"));
        $target = Type::createFromAnyValue($this->activity->get("target"));
        $object = Type::createFromAnyValue($this->activity->get("object"));
        if($target!=null){
            $instrument = $this->activity->get("instrument");
            if($instrument != 'follow'){
                ActivitypubLink::deleteProjectLink("contributors",   $target, $object);
            }else{
                ActivitypubLink::deleteProjectLink("followers",   $target, $object);
            }
        }else{
            $res = PHDB::findOne(ActivitypubObject::COLLECTION, ["object.id"=>$object->get("id")]);
            if($res){
                $localObject = Type::createFromAnyValue($res["object"]);
                PHDB::remove(ActivitypubObject::COLLECTION, ["object.id" => $localObject->get("id")]);
            }
        }

        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
