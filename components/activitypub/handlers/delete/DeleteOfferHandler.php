<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\delete;

use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Badge;
class DeleteOfferHandler extends AbstractHandler{
    public function __construct(Activity $activity){
        parent::__construct($activity);
    }

    protected function handleActivityFromClient(){
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = Type::createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== Utils::PUBLIC_INBOX) {
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $target = Type::createFromAnyValue($object->get("target"));
        $activty = Type::createFromAnyValue($object->get("object"));
        ActivitypubActivity::save($this->activity, $targets);
        $entity = PHDB::findOne(Badge::COLLECTION, [
            "objectId" => $activty->get('id')
        ]);
        if(isset($entity['activitypub']['community']) && count($entity['activitypub']['community']) > 0) {
            $communities = $entity['activitypub']['community'];
            foreach($communities as $key => $value)
            {
                if($value['receptor'] == $target->get('id') && $value['object'] == $object->get('id')){
                    unset($communities[$key]);
                }
            }
            PHDB::update(
                Badge::COLLECTION,
                [
                    "objectId" => $activty->get('id'),
                ],
                [
                    '$set' => [
                        'activitypub.community' => $communities
                    ]
                ]
            );
        }
        $user = PHDB::findOne(Person::COLLECTION, [
            'username' => $this->actor->get('preferredUsername')
        ]);
        if(isset($user['activitypub']['badges'])) {
            $userBadges = $user['activitypub']['badges'];
            foreach($userBadges as $key => $value)
            {
                if($value['receptor'] == $target->get('id') && $value['object'] == $activty->get('id')){
                    unset($userBadges[$key]);
                }
            }
            PHDB::update(
                Person::COLLECTION,
                [
                    'username' => $this->actor->get('preferredUsername')
                ],
                [
                    '$set' => [
                        'activitypub.badges' => $userBadges
                    ]
                ]
            );
        }
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer(){
        $refusedActivity = Type::createFromAnyValue($this->activity->get("object"));
        $object = Type::createFromAnyValue($refusedActivity->get("object"));
        $target = Type::createFromAnyValue($refusedActivity->get('target'));
        $entity = PHDB::findOne(Badge::COLLECTION, [
            "objectId" => $object->get('id')
        ]);
        if(isset($entity['activitypub']['community']) && count($entity['activitypub']['community']) > 0) {
            $communities = $entity['activitypub']['community'];
            foreach($communities as $key => $value)
            {
                if($value['receptor'] == $target->get('id') && $value['object'] == $object->get('id')){
                    unset($communities[$key]);
                }
            }
            PHDB::update(
                Badge::COLLECTION,
                [
                    "objectId" => $target->get('id'),
                ],
                [
                    '$set' => [
                        'activitypub.community' => $communities
                    ]
                ]
            );
        }
        $user = PHDB::findOne(Person::COLLECTION, [
            'username' => $target->get('preferredUsername')
        ]);
        if(isset($user['activitypub']['badges'])) {
            $userBadges = $user['activitypub']['badges'];
            foreach($userBadges as $k => $v)
            {
                if($v['receptor'] == $target->get('id') && $v['object'] == $object->get('id')){
                    unset($userBadges[$k]);
                }
            }
            PHDB::update(
                Person::COLLECTION,
                [
                    'username' => $target->get('preferredUsername')
                ],
                [
                    '$set' => [
                        'activitypub.badges' => $userBadges
                    ]
                ]
            );
        }
        ActivityNotification::send($this->activity, $this->actor);
    }
}
