<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\create;
use PHDB;
use MongoDate;
use Person;
use Project;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Preference;
use Yii;

class CreateHandler extends AbstractHandler
{
    private $pingUrl;
    public function __construct(Activity $activity,$pingUrl, $payload = NULL)
    {
        parent::__construct($activity);
        $this->payload = $payload;
        $this->pingUrl = $pingUrl;
    }
    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        if(is_array($this->activity->get("to")))
            $targetIds = array_merge($this->activity->get("to"));
        if(is_array($this->activity->get("cc")))
            $targetIds = array_merge($this->activity->get("cc"));
         
       foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX){
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        ActivitypubActivity::save($this->activity, $targets,[]);
        $targetInboxes = [];
        foreach($targets as $target){
           if(is_array($target->get("endpoints")) && isset($target->get("endpoints")["sharedInbox"]) && strpos($target->get("endpoints")["sharedInbox"], "@")!==false){
                if(!in_array($target->get("endpoints")["sharedInbox"], $targetInboxes))
                    $targetInboxes[] = $target->get("endpoints")["sharedInbox"];
            }else{
                $targetInboxes[] = $target->get("inbox");
            }
       }   
        foreach($targetInboxes as $inbox){
            Request::post(
                $this->actor->get('id'),
                $inbox,
                $this->activity->toArray(), 
                true
            );
        }
    }
    protected function handleActivityFromServer()
    {
       
            $targets = [];
            $to = $this->activity->get("to");
            
            if (is_array($to) && in_array(Utils::PUBLIC_INBOX, $to)){
                $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
            }
            $object = Type::createFromAnyValue($this->activity->get("object"));
            $UUIDS = ActivitypubActivity::save($this->activity, $targets,[]);
            if ($object->get("type") == "Project") {
                $data = Utils::parseToProject($object,$UUIDS["object"]);
                PHDB::insert(Project::COLLECTION, $data);
                ActivitypubLink::saveLinkProject("contributors", $object,  $this->actor, $UUIDS["activity"], $this->payload, false);
           }
            $this->pingTargets($targets);
            // notify user
            ActivityNotification::send($this->activity, $this->actor);
    }
    private function pingTargets($targets)
    {
        $targetIds = [];
        foreach ($targets as $target) {
            $user = PHDB::findOne(Person::COLLECTION, ["username" => $target->get("preferredUsername")]);
            if ($user && isset($user["preferences"]) && Preference::isActivitypubActivate($user["preferences"]))
                $targetIds[] = (string)$user["_id"];
        } 
        $curl = curl_init($this->pingUrl);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(["users" => $targetIds]));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_exec($curl);
    }

}
