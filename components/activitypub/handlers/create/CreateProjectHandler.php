<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\create;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use Yii;
class CreateProjectHandler extends CreateHandler{
    public function __construct(Activity $activity){
        parent::__construct($activity,Yii::app()->params['cows']["pingNewEventUrl"]);
    }
}