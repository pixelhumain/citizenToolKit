<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\create;

use MongoDate;
use MongoId;
use News;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\NotificationTransformer;
use Preference;
use Project;
use Yii;

class CreateNoteHandler extends AbstractHandler {
    private $payload;
    public function __construct(Activity $activity, $payload=NULL){
        parent::__construct($activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient(){
        $targets = [];
        $targetIds = [];
        if(is_array($this->activity->get("to")))
            $targetIds = array_merge($this->activity->get("to"));
        if(is_array($this->activity->get("cc")))
            $targetIds = array_merge($this->activity->get("cc"));
        foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX){
                $targets[] = Type::createFromAnyValue($id);
            }
        }

        ActivitypubActivity::save($this->activity, $targets, $this->payload);

        //group targets by domain name
        $targetInboxes = [];
        foreach($targets as $target){
            if(is_array($target->get("endpoints")) && isset($target->get("endpoints")["sharedInbox"])  && strpos($target->get("endpoints")["sharedInbox"], "@")!==false){
                if(!in_array($target->get("endpoints")["sharedInbox"], $targetInboxes))
                    $targetInboxes[] = $target->get("endpoints")["sharedInbox"];
            }else{
                $targetInboxes[] = $target->get("inbox");
            }
        }
        foreach($targetInboxes as $inbox){
            Request::post(
                $this->actor->get("id"),
                $inbox, $this->activity->toArray(),
                true
            );
        }
        if(isset($this->payload["newsId"]) && $this->payload["newsId"] != null){
            News::updateField($this->payload["newsId"], "objectUUID", $this->activity->get("object"), null);
        }

    }

    protected function handleActivityFromServer(){
        $targets = [];

        $to = $this->activity->get("to");
        if(is_array($to) && in_array(Utils::PUBLIC_INBOX, $to))
            $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));

        $object = Type::createFromAnyValue($this->activity->get("object"));

        $UUIDS = ActivitypubActivity::save($this->activity, $targets);
        $date = new MongoDate(time());
        $news = [
            "type"=>"activitypub",
            "author"=>$this->actor->get("id"),
            "created" => $date,
            "date" => $date,
            "sharedBy" => [
                "id" => $this->actor->get("id"),
                "updated" => $date,
            ],
            "updated" => $date,
            "objectUUID" => $UUIDS["object"],
            "objectId" => $object->get("id"),
            "scope" =>array('type' => 'restricted'),
        ];
        $news["fediTags"] = ActivitypubLink::getActorTags($this->activity->get('actor'));
        if(!empty($this->activity->get('target'))){
            $obj = Type::createFromAnyValue($this->activity->get('target'));
                $project = PHDB::findOne(Project::COLLECTION, [
                    "objectId" => $obj->get('id')
                ]);
                $news["target"] = array('id' =>(string) $project['_id'],"type" => $obj->get('type'));
        }
        $isNotAreplyTo = !$object->get("inReplyTo");
        if($isNotAreplyTo){

            PHDB::insert(News::COLLECTION,$news );

            // notify user
            if(count($targets)> 0){
                ActivityNotification::send($this->activity, $this->actor,$targets);
            }else{
                ActivityNotification::send($this->activity, $this->actor);
            }
            $this->pingTargets($targets);
        }
    }

    protected function pingTargets($targets){
        $targetIds = [];
        foreach($targets as $target){
            $user = PHDB::findOne(Person::COLLECTION, ["username" => $target->get("preferredUsername")]);
            if($user && isset($user["preferences"]) && Preference::isActivitypubActivate($user["preferences"]))
                $targetIds[] = (string)$user["_id"];
        }

        $coWs = Yii::app()->params['cows'];
     	$curl = curl_init($coWs["pingNewPostUrl"]);
        curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(["users" => $targetIds]));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_exec($curl);

    }

    private function mockPayload($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return array(
                "name" => $basename,
                "type" => "group"
            );
        } else if (strpos($url, 'projects') !== false) {
            return array(
                "name" => $basename,
                "type" => "project"
            );
        } else {
            return array(
                "name" => $basename,
                "type" => "person"
            );
        }
    }
    private function getTagByActivityActor($actor){

    }
}
