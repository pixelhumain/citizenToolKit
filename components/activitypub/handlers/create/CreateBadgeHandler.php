<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\create;
use Badge;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Yii;
class CreateBadgeHandler extends AbstractHandler {
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);
        $this->payload = $payload;
    }
    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        if(is_array($this->activity->get("to")))
            $targetIds = array_merge($this->activity->get("to"));
        if(is_array($this->activity->get("cc")))
            $targetIds = array_merge($this->activity->get("cc"));

        foreach($targetIds as $id){
            if($id !== Utils::PUBLIC_INBOX){
                $targets[] = Type::createFromAnyValue($id);
            }
        }
        ActivitypubActivity::save($this->activity, $targets,[]);
        $targetInboxes = [];
        foreach($targets as $target){
            if(is_array($target->get("endpoints")) && isset($target->get("endpoints")["sharedInbox"]) && str_contains($target->get("endpoints")[ "sharedInbox" ], "@")){
                if(!in_array($target->get("endpoints")["sharedInbox"], $targetInboxes))
                    $targetInboxes[] = $target->get("endpoints")["sharedInbox"];
            }else{
                $targetInboxes[] = $target->get("inbox");
            }
        }
        foreach($targetInboxes as $inbox){
            Request::post(
                $this->actor->get('id'),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }
    protected  function handleActivityFromServer(){
        $targets = [];
        $to = $this->activity->get("to");

        if (is_array($to) && in_array(Utils::PUBLIC_INBOX, $to)){
            $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
        }

        $object = Type::createFromAnyValue($this->activity->get("object"));
        $UUIDS = ActivitypubActivity::save($this->activity, $targets,[]);
        $data = Utils::parseToBadge($object);
        PHDB::insert(Badge::COLLECTION, $data);
        //$this->pingTargets($targets);
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}


