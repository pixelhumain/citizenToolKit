<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\AbstractActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;

abstract class AbstractHandler
{
    protected Activity $activity;
    protected ?AbstractActor $actor;
    protected ?bool $isFromClient;

    public function __construct(Activity $activity, ?bool $isFromClient = null)
    {
        $this->activity = $activity;
        $this->actor = Type::createFromAnyValue($activity->get("actor"));
        $this->isFromClient = $isFromClient;
    }

    public function handle()
    {
        if ($this->actor === null) {
           // l'activité n'a pas d'acteur donc pas la peine de continuer, ca permet de prevenir l'erreur
            return false;
        }
        if ($this->isFromClient === null) {
            $this->isFromClient = parse_url($this->actor->get("id"), PHP_URL_HOST) == Config::HOST();
        }

        if ($this->isFromClient) {
            $this->handleActivityFromClient();
        } else {
            $this->handleActivityFromServer();
        }
    }

    abstract protected function handleActivityFromClient();
    abstract protected function handleActivityFromServer();
}
