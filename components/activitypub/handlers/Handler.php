<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PHDB;

class Handler
{
    public static function handle(Activity $activity, $payload = null)
    {
        $object = Type::createFromAnyValue($activity->get("object"));
        if($object == null) return false;
        $activityType = $activity->get("type");
        $className = $activityType;
        if (in_array($object->get("type"), ["Person", "Group"]))
            $className .= "Person";
        else
            $className .= $object->get("type");
        $className .= "Handler";
        $class = __NAMESPACE__ . "\\" . strtolower($activityType) . "\\" . $className;
        if (!class_exists($class))
            return false;
        if($activity->get("actor") == null) return false;
        $handler = new $class($activity, $payload);
        $handler->handle();
    }
}
