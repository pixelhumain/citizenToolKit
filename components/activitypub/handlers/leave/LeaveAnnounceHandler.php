<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\leave;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Yii;

class LeaveAnnounceHandler extends AbstractHandler{
    public function __construct(Activity $activity, $payload=NULL){
        parent::__construct($activity);
        $this->payload = $payload;
    }
    protected function handleActivityFromClient(){
       $target = Type::createFromAnyValue($this->activity->get("attributedTo"));
       $participant= array($this->activity->get("actor"));
        ActivitypubActivity::save($this->activity, [$target],$participant);
        Request::post(
            $this->activity->get("actor"), 
            $target->get('inbox'),
            $this->activity->toArray(),
            true
        );
    }
    protected function handleActivityFromServer(){

        $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
        $objectId = $this->activity->get("object");
        $res = PHDB::findOne(ActivitypubObject::COLLECTION, ["object.id"=>$objectId]);
        $participant= Utils::removeIfExistInArray($res["payload"],array($this->activity->get("actor")));
        if(!$res) return null;
        PHDB::update(ActivitypubObject::COLLECTION, ["object.id"=>$objectId], [
             '$set' => [
               //  "object.participateCount" => count($participant),
                 "payload" => count($participant) == 0 ? array() : $participant
              ]
        ]);
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}