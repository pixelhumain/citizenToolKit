<?php
namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\leave;

use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;
use Yii;

class LeaveEventHandler extends AbstractHandler{
    public function __construct(Activity $activity, $payload=NULL){
        parent::__construct($activity);
        $this->payload = $payload;
    }
    protected function handleActivityFromClient(){
       $target = Type::createFromAnyValue($this->activity->get("attributedTo"));
       $participant= array($this->activity->get("actor"));
        ActivitypubActivity::save($this->activity, [],$participant);
        Request::post(
            $this->activity->get("actor"), 
            $target->get('inbox'),
            $this->activity->toArray(),
            true
        );
    }
    protected function handleActivityFromServer(){
        $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
        $object = $this->activity->get('object');
        if(is_object( $this->activity->get("object"))){
            $objectId = $object->get('object');
        }else{
            $objectId = $object;
        }
        $res = PHDB::findOne(ActivitypubObject::COLLECTION, ["object.id"=>$objectId]);
        $participant= Utils::removeIfExistInArray($res["payload"],array($this->activity->get("actor")));
        if(!$res) return null;
        PHDB::update(ActivitypubObject::COLLECTION, ["object.id"=>$objectId], [
             '$set' => [
               //  "object.participateCount" => count($participant),
                 "payload" => count($participant) == 0 ? array() : $participant
              ]
        ]);

        if(empty($this->activity->get('summary'))){
            $obj = Type::createFromAnyValue($objectId);
            $summary = Yii::t(
                "activitypub",
                "{who} leave participation in  '{what}'",
                array(
                    "{who}" =>$this->actor->get('preferredUsername'),
                    "{what}" => $obj->get("name")
                )
            );
            $this->activity->set('summary',$summary);
        }
        ActivitypubActivity::save($this->activity, $targets);
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}