<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\leave;

use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\AbstractHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\ActivityNotification;

class LeaveProjectHandler extends AbstractHandler
{
    public function __construct(Activity $activity, $payload = NULL)
    {
        parent::__construct($activity);

        $this->payload = $payload;
    }
    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        $actor = ActivitypubTranslator::coPersonToActor($user);
       
        if (is_array($this->activity->get("cc")))
            $targetIds = $this->activity->get("cc");

        foreach ($targetIds as $id) {
            if ($id !== $actor->get('id')) {
                $x = Type::createFromAnyValue($id);
                $targets[] = $x;
            }
        }
        $object = Type::createFromAnyValue($this->activity->get("object"));

       
        $targetInboxes = [];
        foreach ($targets as $t) {
            $targetInboxes[] = $t->get("inbox");
        }
     
        ActivitypubActivity::save($this->activity, []);
        $instrument = $this->activity->get("instrument");
        if($instrument != 'unfollow'){
            ActivitypubLink::deleteProjectLink("contributors",  $object, $actor);
        }else{
            ActivitypubLink::deleteProjectLink("followers",  $object, $actor);
        } 
       
        foreach ($targetInboxes as $inbox) {
            Request::post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }
    protected function handleActivityFromServer()
    {
        $targets = ActivitypubActor::getLocalFollowersOfExternalUser($this->actor->get("id"));
        $object = Type::createFromAnyValue($this->activity->get("object"));
        $actor = Type::createFromAnyValue($this->activity->get("actor"));
        ActivitypubActivity::save($this->activity, $targets);
        $instrument = $this->activity->get("instrument");
        if($instrument != 'unfollow'){
            ActivitypubLink::deleteProjectLink("contributors",  $object, $actor);
        }else{
            ActivitypubLink::deleteProjectLink("followers",  $object, $actor);
        }      
        // notify user
        ActivityNotification::send($this->activity, $this->actor);
    }
}
